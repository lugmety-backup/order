<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/11/2017
 * Time: 4:32 PM
 */

if(env("APP_ENV") == "production") {
    return array(
        'url' => 'https://api.lugmety.com/auth/v1/oauth/introspect',

        "restaurant_base_url" => "https://api.lugmety.com/restaurant/v1",

        "location_base_url" => "https://api.lugmety.com/location/v1",

        "oauth_base_url" => 'https://api.lugmety.com/auth/v1',

        'image_service_base_url' => 'https://api.lugmety.com/cdn/v1',//dont add / after v1

        "image_service_base_url_cdn" => 'https://cdn.lugmety.com',//cdn base url for image display

        "settings_url" => "https://api.lugmety.com/general/v1/public/settings/",

        "error_message_url" => "https://api.lugmety.com/general/v1/error-message/",

        "verifyPaymentFrontEndRedirect" => "https://lugmety.com/payment/paytabs/response",


//access token for admin
        'auth_access_token' => "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU5MzJjMmRiYjAzYzhmYjUwMDBlMDJjNDY2MjhkNTY1MGY2NWMxY2RjYTZmMjQ1MDQ0MWVlMTczZTU4NTAzNzkxNjk1MThlMWVmMjkyZjgzIn0.eyJhdWQiOiIyIiwianRpIjoiNTkzMmMyZGJiMDNjOGZiNTAwMGUwMmM0NjYyOGQ1NjUwZjY1YzFjZGNhNmYyNDUwNDQxZWUxNzNlNTg1MDM3OTE2OTUxOGUxZWYyOTJmODMiLCJpYXQiOjE0OTQyMjEwNjEsIm5iZiI6MTQ5NDIyMTA2MSwiZXhwIjoxNTI1NzU3MDYxLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.SJazOMGOnBJVsQJjiWxAdgwfpveSCY4x56b7Ma_-KX0aH4N6HSA1_7SPTDWk7syeAC86J_FV9vlMYvDtdLen0yqDiIWpdoHMGBTmHC7uY421ckLxZPkH4QRWKCr5nRKtZugrrtsRtmV-dsVDQp6HGncPchBR7qxsmaLftazbbZoII-lnLfCd4EVnZqQmxvfreJU5qCHGPuzJlLWjBw_iUD3GJ84A1xuf433EPMVRSHmUgleudxQQ8iXlrTjqvBmIhoS020c5f1FUQlK2YMqcTEvX9HhJarzPD64ECfBiFPrGYiDr7tTleWgBMn4fvsuQF0u6gwGE7pzH8ulp0sMElKAr-BPmwe8waVu0Zf2X3UYiNQ4KwKPZyS48I4KIC5ZzxHfMcLxsdzEVp_Tzs245c_WH65pSjV4V0yiN-1iMD0R4FvQ84otQbTePxJKSLsvkeFXic69cugVEfacSlDxX2nhnPEnZtDBlRXxsDzSHXZGlDGOrZ0GhP2hrkfx3J-1qXwANSTFGtFuUXLWwdPMFqX71PgaamB-A-5HmgkF7IX9noNZX3CLiY8yGtMDUSWGiPdASy9G9zfz0l2afFMv1JrxcGjbx4QuAxJ0DXGHcCuuK5dPNv4toErvEdLe9TyGkk-WbNXJm7JTvYnQvvIGzp60QOyC4oS587HxeJ-SAUEE",
        "google_apiKey" => "AIzaSyDoz-YDNS31TXHVeRHS_KL3h7IwZXJof2k",

        'paytabs_secret_key' => 'HtNpvsFz2MHm59fGITLwBJnfkBXtnAs53fuzIa9LWQ720mJJfufRdD6xf31CDtHHJchsmqdjhkliH41XzhOMVUkaYmNCQvkzHB14',

        'paytabs_email' => 'kabina.suwal92@gmail.com',

        'paytabs' => [
            'secret_key' => 'HtNpvsFz2MHm59fGITLwBJnfkBXtnAs53fuzIa9LWQ720mJJfufRdD6xf31CDtHHJchsmqdjhkliH41XzhOMVUkaYmNCQvkzHB14',
            'merchant_email' => 'kabina.suwal92@gmail.com',
            'cms_with_version' => 'Lumen 5.4.*',
            'site_url' => 'https://api.lugmety.com/order/v1',
            'return_url' => 'https://api.lugmety.com/order/v1/payment-method/verifyPayment'
        ],
    );
}
else {
    
    return array(
        'url' => 'http://api.stagingapp.io/auth/v1/oauth/introspect',
//    'url' => 'http://localhost:8888/oauth/introspect',
//   "restaurant_base_url" => "http://localhost:9999",
        "restaurant_base_url" => "http://api.stagingapp.io/restaurant/v1",
        "location_base_url" => "http://api.stagingapp.io/location/v1",
//    "location_base_url" => "http://localhost:9000",
//    "oauth_base_url" =>  'http://localhost:8888',
        "oauth_base_url" => 'http://api.stagingapp.io/auth/v1',
        'image_service_base_url' => 'http://api.stagingapp.io/cdn/v1',//dont add / after v1

        "image_service_base_url_cdn" => 'http://api.stagingapp.io/cdn/v1',//cdn base url for image display

        "settings_url" => "http://api.stagingapp.io/general/v1/public/settings/",
        "error_message_url" => "http://api.stagingapp.io/general/v1/error-message/",
        "verifyPaymentFrontEndRedirect" => "http://webapp.stagingapp.io/payment/paytabs/response",

//    'image_service_base_url'=>'http://localhost:5555',

//access token for admin
        'auth_access_token' => "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU5MzJjMmRiYjAzYzhmYjUwMDBlMDJjNDY2MjhkNTY1MGY2NWMxY2RjYTZmMjQ1MDQ0MWVlMTczZTU4NTAzNzkxNjk1MThlMWVmMjkyZjgzIn0.eyJhdWQiOiIyIiwianRpIjoiNTkzMmMyZGJiMDNjOGZiNTAwMGUwMmM0NjYyOGQ1NjUwZjY1YzFjZGNhNmYyNDUwNDQxZWUxNzNlNTg1MDM3OTE2OTUxOGUxZWYyOTJmODMiLCJpYXQiOjE0OTQyMjEwNjEsIm5iZiI6MTQ5NDIyMTA2MSwiZXhwIjoxNTI1NzU3MDYxLCJzdWIiOiIzIiwic2NvcGVzIjpbXX0.SJazOMGOnBJVsQJjiWxAdgwfpveSCY4x56b7Ma_-KX0aH4N6HSA1_7SPTDWk7syeAC86J_FV9vlMYvDtdLen0yqDiIWpdoHMGBTmHC7uY421ckLxZPkH4QRWKCr5nRKtZugrrtsRtmV-dsVDQp6HGncPchBR7qxsmaLftazbbZoII-lnLfCd4EVnZqQmxvfreJU5qCHGPuzJlLWjBw_iUD3GJ84A1xuf433EPMVRSHmUgleudxQQ8iXlrTjqvBmIhoS020c5f1FUQlK2YMqcTEvX9HhJarzPD64ECfBiFPrGYiDr7tTleWgBMn4fvsuQF0u6gwGE7pzH8ulp0sMElKAr-BPmwe8waVu0Zf2X3UYiNQ4KwKPZyS48I4KIC5ZzxHfMcLxsdzEVp_Tzs245c_WH65pSjV4V0yiN-1iMD0R4FvQ84otQbTePxJKSLsvkeFXic69cugVEfacSlDxX2nhnPEnZtDBlRXxsDzSHXZGlDGOrZ0GhP2hrkfx3J-1qXwANSTFGtFuUXLWwdPMFqX71PgaamB-A-5HmgkF7IX9noNZX3CLiY8yGtMDUSWGiPdASy9G9zfz0l2afFMv1JrxcGjbx4QuAxJ0DXGHcCuuK5dPNv4toErvEdLe9TyGkk-WbNXJm7JTvYnQvvIGzp60QOyC4oS587HxeJ-SAUEE",
        "google_apiKey" => "AIzaSyDoz-YDNS31TXHVeRHS_KL3h7IwZXJof2k",

        'paytabs_secret_key' => 'HtNpvsFz2MHm59fGITLwBJnfkBXtnAs53fuzIa9LWQ720mJJfufRdD6xf31CDtHHJchsmqdjhkliH41XzhOMVUkaYmNCQvkzHB14',

        'paytabs_email' => 'kabina.suwal92@gmail.com',

        'paytabs' => [
            'secret_key' => 'HtNpvsFz2MHm59fGITLwBJnfkBXtnAs53fuzIa9LWQ720mJJfufRdD6xf31CDtHHJchsmqdjhkliH41XzhOMVUkaYmNCQvkzHB14',
            'merchant_email' => 'kabina.suwal92@gmail.com',
            'cms_with_version' => 'Lumen 5.4.*',
            'site_url' => 'http://api.stagingapp.io/order/v1',
            'return_url' => 'http://api.stagingapp.io/order/v1/payment-method/verifyPayment'
        ],
    );
}