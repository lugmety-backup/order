# Lugmety Order Service
### Base Url for staging : http://api.stagingapp.io/order/v1
### Base Url for live : https://api.lugmety.com/order/v1
## Cron 
1. Run GET BASE_URL/cron/modify/pickup?hash=$2y$12$DVJGLbj4pNg62z98Zl6ytuWlMFu4rLMX1Ie.w6tFnAN.YwpZarYX2 to change delivery type pickup order status to delivered. 
2. RUN GET flush/dashboard/key to flush redis of admin dashboard stats