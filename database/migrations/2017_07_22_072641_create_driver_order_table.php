<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_order', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("user_id");
            $table->unsignedInteger("country_id");
            $table->unsignedInteger("city_id");
            $table->unsignedInteger("source_district_id")->nullable();//resturant district_id
            $table->unsignedInteger("destination_district_id")->nullable();//customer district_id obtained from shipping_id from order
            $table->unsignedInteger("restaurant_id");
            $table->unsignedInteger("order_id");
            $table->string("distance");
            $table->string("driver_picked_time");
            $table->float('price', 6, 2);
            $table->string("status");
            $table->dateTime('pickup_time');
            $table->text("order_history");
            $table->text("restaurant");
            $table->text("order");
            $table->timestamps();
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_order');
    }
}
