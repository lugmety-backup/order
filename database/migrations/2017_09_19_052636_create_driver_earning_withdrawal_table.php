<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverEarningWithdrawalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_earning_withdrawal', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("user_id");
            $table->unsignedInteger("admin_id")->nullable();
            $table->float("amount");
            $table->enum("status",["pending","approved","rejected"]);
            $table->text("comment");
            $table->engine = "InnoDB";
            $table->unsignedInteger("country_id")->nullable();
            $table->timestamp('date_for')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_earning_withdrawal');
    }
}
