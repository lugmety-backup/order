<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEarningRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('earning_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("country_id");
            $table->float("min_earning");
            $table->float("min_distance");
            $table->float("earning_per_km");
            $table->dateTime("start_date")->nullable();
            $table->dateTime("end_date")->nullable();
            $table->tinyInteger("is_default");
            $table->engine = "InnoDB";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('earning_rules');
    }
}
