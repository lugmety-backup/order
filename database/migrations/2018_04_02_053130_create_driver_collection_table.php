<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverCollectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_collections', function (Blueprint $table) {
            $table->increments('id');
            $table->engine = "InnoDB";
            $table->unsignedInteger("order_id");
            $table->unsignedInteger("country_id");
            $table->unsignedInteger("city_id");
            $table->unsignedInteger('driver_id');
            $table->unsignedInteger('team_id');
            $table->unsignedInteger('admin_id');
            $table->timestamp("leader_received_at")->nullable();
            $table->timestamp("admin_received_at")->nullable();
            $table->float("amount");
            $table->index(["order_id","driver_id","team_id","admin_id","country_id","city_id"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_collections');
    }
}
