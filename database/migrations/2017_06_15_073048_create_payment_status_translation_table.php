<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentStatusTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_status_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('payment_status_id');
            $table->string('lang_code');
            $table->string('name');
            $table->engine = "InnoDB";
            $table->foreign('payment_status_id')->references('id')->on('payment_status')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_status_translation');
    }
}
