<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('billing_id')->nullable();
            $table->unsignedInteger('shipping_id')->nullable();
            $table->string('payment_method');
            $table->unsignedInteger('country_id');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('district_id');
            $table->string('delivery_type');
            $table->float('amount', 8, 2);
            $table->boolean('is_seen')->default(false);
            $table->string("hash");
            $table->string("currency");
            $table->unsignedInteger('customer_id');
            $table->string('order_status');
            $table->string('payment_status');
            $table->string('delivery_status');
            $table->timestamp("order_date");//added for displaying ordrer date
            $table->string('payment_ref_id')->nullable();
            $table->float('site_commission_rate', 4, 2);
            $table->float('site_commission_amount', 6, 2);
            $table->dateTime('delivery_or_pickup_time')->nullable();
            $table->unsignedInteger('restaurant_id');
            $table->float('delivery_fee', 6, 2);
            $table->string('api_delivery_time');
            $table->string('threshold_delivery_time');
            $table->string('user_type');
            $table->float("logistics_fee");
            $table->float('site_tax_rate', 4, 2);
            $table->text('customer_note');
            $table->longText("extra_info");
            $table->text("customer_care_history");
            $table->string("admin_name");
            $table->unsignedInteger("admin_id")->nullable();
            $table->string('device_type',10);
            $table->unsignedInteger("rejection_reason_id")->nullable();
            $table->string("customer_name");
            $table->string("tax_type",10);
            $table->float("tax",10,2);
            $table->integer('customer_priority')->defaule(30);
            $table->unsignedInteger("machine_retry")->default(0);
//            $table->dateTime("last_machine_checked_date")->nullable();
            $table->string('delivery_time');
            $table->string("time_zone_utc",10);
            $table->string("app_version")->nullable();
            $table->string("os_version")->nullable();
            $table->string("model_name")->nullable();
            $table->tinyInteger('is_normal')->default(0);
            $table->softDeletes();
            $table->timestamps();
            $table->engine = "InnoDB";
            //$table->foreign('order_status_id')->references('id')->on('order_status')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
