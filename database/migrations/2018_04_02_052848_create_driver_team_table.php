<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_teams', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("captain_id");
            $table->unsignedInteger("created_by");
            $table->unsignedInteger("country_id");
            $table->unsignedInteger("city_id");
            $table->string("name");
            $table->text("description");
            $table->tinyInteger("status");
            $table->softDeletes();
            $table->index(["captain_id","created_by","country_id","city_id"]);
            $table->engine = "InnoDB";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_teams');
    }
}
