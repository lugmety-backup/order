<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverEarningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_earning', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("user_id");
            $table->unsignedInteger("order_id")->nullable();
            $table->unsignedInteger("country_id")->nullable();
            $table->float("amount");
//            $table->float('commission_rate')->default(5);
            $table->enum("type",["normal","deduction"]);
            $table->timestamp('date_for')->nullable();
            $table->text("comment");
            $table->engine = "InnoDB";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_earning');
    }
}
