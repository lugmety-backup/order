<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryStatusTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_status_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('delivery_status_id');
            $table->string('lang_code');
            $table->string('name');
            $table->engine = "InnoDB";
            $table->foreign('delivery_status_id')->references('id')->on('delivery_status')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_status_translation');
    }
}
