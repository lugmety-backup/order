<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartialRefundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partial_refunds', function (Blueprint $table) {
            $table->increments('id');
            $table->string("title");
            $table->text("comment");
            $table->float('amount', 8, 2);
            $table->emun("vat",["include_tax","do_not_include_tax"]);
            $table->enum("type",["plus","minus"]);
            $table->unsignedInteger("user_id");
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->engine = "InnoDB";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partial_refunds');
    }
}
