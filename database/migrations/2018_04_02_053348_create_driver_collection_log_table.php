<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverCollectionLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_collection_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("order_id");
            $table->unsignedInteger("user_id");
            $table->text("content");
            $table->enum("type",["driver","captain","admin"]);
            $table->engine = "InnoDB";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_collection_logs');
    }
}
