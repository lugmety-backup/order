<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCronInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cron_invoice', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("country_id");
            $table->text("restaurant_lists");
            $table->date("last_date");
            $table->tinyInteger("is_send_invoice");
            $table->engine = "InnoDB";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cron_invoice');
    }
}
