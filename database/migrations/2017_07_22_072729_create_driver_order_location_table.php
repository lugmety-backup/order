<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverOrderLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_order_location', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("driver_order_id");
            $table->unsignedInteger("driver_id");
//            $table->foreign("driver_order_id")->references('order_id')->on('driver_order')->onDelete("cascade");
            $table->double('latitude');
            $table->double('longitude');
            $table->timestamps();
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_order_location');
    }
}
