<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemAddons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_item_addons', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_item_id');
            $table->integer('item_total');
            $table->unsignedInteger('addon_id');
            $table->integer('quantity');
            $table->float('unit_price', 6, 2);
            $table->float('discount', 4, 2);
            $table->text('addon_raw_data');
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->foreign('order_item_id')->references('id')->on('order_items')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item_addons');
    }
}
