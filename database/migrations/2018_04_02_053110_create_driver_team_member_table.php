<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverTeamMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_team_members', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("driver_team_id");
            $table->foreign('driver_team_id')->references('id')->on('driver_teams');
            $table->unsignedInteger("driver_id");
            $table->index(["driver_team_id","driver_id"]);
            $table->softDeletes();
            $table->engine = "InnoDB";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_team_members');
    }
}
