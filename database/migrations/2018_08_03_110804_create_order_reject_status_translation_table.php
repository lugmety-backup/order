<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderRejectStatusTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_reject_status_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_reject_status_id');
            $table->string('lang_code');
            $table->string('name');

            $table->engine = "InnoDB";
            $table->foreign('order_reject_status_id')->references('id')->on('order_reject_status')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_reject_status_translation');
    }
}
