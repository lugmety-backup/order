<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingMethodTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_method_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shipping_method_id');
            $table->foreign('shipping_method_id')->references('id')->on('shipping_method')->onDelete('cascade');
            $table->string('lang_code', 20);
            $table->string('name', 255);
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_method_translation');
    }
}
