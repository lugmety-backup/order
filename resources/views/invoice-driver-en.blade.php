
<style type="text/css">
    body,p,h2{
        font-family: Arial;
    }
    table td{
        font-size: 14px;
    }
</style>
<div class="wrapper" style="max-width: 800px;margin: 0 auto;padding: 30px 0">
    <div class="header" style="width: 100%;display: inline-table;margin-bottom: 15px;">
        <div class="company-header" style="width: 50%;float: left">
            <img src="https://lugmety.com/assets/images/main-logo.png" alt="lugmety-logo" style="display: block;margin-bottom: 15px"/>
            <p style="color: #333;display: block;line-height: 1.6;font-size: 12px;margin-bottom: 0;margin-top: 5px">806, 8th floor, King's Road Tower</p>
            <p style="color: #333;display: block;line-height: 1.6;font-size: 12px;margin-bottom: 0;margin-top: 5px">Jeddah, K.S.A</p>
            <p style="color: #333;display: block;line-height: 1.6;font-size: 12px;margin-bottom: 0;margin-top: 5px">{{$data["lugmety_phone_number"]}}</p>
        </div>
        <div class="invoice-detail" style="float: right;text-align: right">
            <p style="color: #bf9f65;font-size: 16px;margin-bottom: 0;margin-top: 0;line-height: 1.8"><b>Invoice No: {{$data["invoice"]}}</b></p>
            <p style="color: #333;font-size: 16px;margin-bottom: 0;margin-top: 0;line-height: 1.8"><b style="color: #bf9f65">TIN:</b>{{$data["tin_from_setting"]}}</p>
            <p style="color: #333;font-size: 16px;margin-bottom: 0;margin-top: 0;line-height: 1.8">{{date('l jS \of F Y ')}} </p>
        </div>
    </div>
    <div class="table-wrapper" style="width: 100%;display: inline-table;margin: 15px 0;">
        <div class="table-top">
            <div class="bill-info" style="width: 50%;float: left;margin-bottom: 15px;">
                <p style="color: #999;margin-bottom: 0;font-size: 16px;margin-top: 5px"><b style="color:#bf9f65">Bill To: </b>{{$data["bill_to"]}}</p>
            </div>
            <div class="delivery-info" style="float: right;text-align: right">
                <p style="color: #999;font-size: 16px;margin-bottom: 0;margin-top: 5px"><b style="color:#bf9f65">FOR: </b>{{$data["bill_for_en"]}}</p>
            </div>
        </div>

    </div>
    <table class="StandardTable" style="width: 100%;border-spacing: 0;text-align: left">
        <thead style="-moz-border-radius: 10px;border-radius: 5px">
        <tr>
            <th style="width: 65%;background: #bf9f65;padding: 10px 20px;color: #fff;font-size: 18px">Description</th>
            <th style="background: #bf9f65;padding: 10px 20px;color: #fff;font-size: 18px">Amount</th>
        </tr>
        </thead>
        <tbody>
        <tr class="total-sales">
            <td style="color: #333;width: 65%;padding: 10px 20px;border-left: 1px solid #ddd;border-top: 0;border-right: 0;border-bottom: 1px solid #ddd;"><b>Total Earning</b></td>
            <td style="color: #333;padding: 10px 20px;border: 1px solid #ddd;border-top: 0"><b>{{$data["total_earning_en"]}}</b></td>
        </tr>
        <tr class="sales-detail-commision">
            <td style="text-align: right;width: 65%;border-right: 0;border-left: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 2px 20px"><b>Sub Total</b></td>
            <td style="border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px;padding-bottom: 10px"><b>{{$data["subtotal_en"]}}</b></td>
        </tr>

        </tr>
        <tr class="sales-detail-commision">
            <td style="text-align: right;width: 65%;border-right: 0;border-left: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 2px 20px"><b>LUGMETY.COM COMMISSION <i>({{$data["commission_rate_en"]}})</i> </b></td>
            <td style="border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px;padding-bottom: 10px"><b>{{$data["commission_amount_en"]}}</b></td>
        </tr>

        <tr class="sales-detail-commision">
            <td style="text-align: right;width: 65%;border-right: 0;border-left: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 2px 20px"><b>LUGMETY.COM Commission VAT <i>({{$data["commission_vat_en"]}})</i></b></td>
            <td style="border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px;padding-bottom: 10px"><b>{{$data["commission_vat_amount_en"]}}</b></td>
        </tr>
        @if($data["is_captain_driver"])
            <tr class="sales-detail-commision">
                <td style="text-align: right;width: 65%;border-right: 0;border-left: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 2px 20px"><b>Captain Salary</b></td>
                <td style="border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px;padding-bottom: 10px"><b>{{$data["captain_salary_en"]}}</b></td>
            </tr>
        @endif

        <tr class="sales-detail-commision">
            {{--<td style="text-align: right;padding-bottom: 20px !important;border-bottom: 1px solid #ddd;width: 65%;border-right: 0;border-left: 1px solid #ddd;border-top: 0;padding: 5px 20px 2px 20px"><b>LUGMETY.COM COMMISSION HELD IN CASH AT RESTAURANT [{{$data['lugmety_commission_rate']}}%]</b></td>--}}
            {{--<td style="border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px;padding-bottom: 10px"><b>{{$data["lugmety_cash_at_restaurant"]}}</b></td>--}}
        </tr>

        <tr class="sales-detail">
            <td style="text-align: right;padding: 15px;border-bottom: 1px solid #ddd ;color: #333;width: 65%;border-right: 0;border-left: 1px solid #ddd;border-top: 0;padding-bottom: 10px"><b>Grand Total</b></td>
            <td style="color: #333;border: 1px solid #ddd;border-top: 1px solid #ddd;border-bottom: 1px solid #ddd;padding: 12px 20px 0 20px;padding-bottom: 10px;"><b> {{$data["grand_total_en"]}}</b></td>
        </tr>
        </tbody>
    </table>
    <p style="margin-bottom: 30px;margin-top: 15px;color: #333;font-size: 14px;line-height: 1.6">{{$data["bank_info_english"]}}</p>
    <h2 style="color: #333333;font-size: 16px;">THANK YOU FOR YOUR BUSINESS!</h2>
</div>

