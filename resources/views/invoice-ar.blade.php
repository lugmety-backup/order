<html dir="rtl">
<head>
    <title>Invoice Template</title>
    <style type="text/css">
        body,p,h2{
            font-family: Arial;
        }
        table td{
            font-size: 14px;
        }
    </style>
</head>
<body dir="rtl">
<div class="wrapper" style="max-width: 800px;margin: 0 auto;padding: 30px 0">
    <div class="header" style="width: 100%;display: inline-table;margin-bottom: 15px;">
        <div class="company-header" style="width: 50%;float: right">
            <img src="https://lugmety.com/assets/images/main-logo.png" alt="lugmety-logo" style="display: block;margin-bottom: 15px"/>
            <p style="color: #333;display: block;line-height: 1.6;font-size: 12px;margin-bottom: 0;margin-top: 5px">806, 8th floor, King's Road Tower</p>
            <p style="color: #333;display: block;line-height: 1.6;font-size: 12px;margin-bottom: 0;margin-top: 5px">Jeddah, K.S.A</p>
            <p style="color: #333;display: block;line-height: 1.6;font-size: 12px;margin-bottom: 0;margin-top: 5px">{{$data["lugmety_phone_number"]}}</p>
        </div>
        <div class="invoice-detail" style="float: right;text-align: right">
            <p style="color: #bf9f65;font-size: 16px;margin-bottom: 0;margin-top: 0;line-height: 1.8"><b >رقم الفاتورة:</b>{{$data["invoice"]}}</p>
            <p style="color: #333;font-size: 16px;margin-bottom: 0;margin-top: 0;line-height: 1.8"><b dir="rtl" style="color: #bf9f65;">TIN:</b> {{$data["tin_from_setting"]}}</p>
            <p style="color: #333;font-size: 16px;margin-bottom: 0;margin-top: 0;line-height: 1.8">{{date('l jS \of F Y ')}}</p>
        </div>
    </div>
    <div class="table-wrapper" style="width: 100%;display: inline-table;margin: 15px 0;">
        <div class="table-top">
            <div class="bill-info" style="width: 50%;float: right;margin-bottom: 15px;">
                <p style="color: #999;margin-bottom: 0;font-size: 16px;margin-top: 5px"><b style="color:#bf9f65">إلى :</b>{{$data["restaurant_name_arabic"]}}</p>
                @if(!empty($data["tin"]))
                    <p style="color: #999;font-size: 16px;margin-bottom: 0;margin-top: 5px"><b style="color:#bf9f65;float:right;">TIN: </b> {{$data["tin"]}}</p>
                @endif
            </div>
            <div class="delivery-info" style="float: right;text-align: right">
                <p style="color: #999;font-size: 16px;margin-bottom: 0;margin-top: 5px"><b style="color:#bf9f65">قسم :</b>الخدمات اللوجستية للتوصيل عبر الإنترنت</p>
            </div>
        </div>

    </div>
    <table class="StandardTable" style="width: 100%;border-spacing: 0;text-align: right">
        <thead style="-moz-border-radius: 10px;border-radius: 5px">
        <tr>
            <th style="width: 65%;background: #bf9f65;padding: 10px 20px;color: #fff;font-size: 18px">الوصف </th>
            <th style="background: #bf9f65;padding: 10px 20px;color: #fff;font-size: 18px">المجموع</th>
        </tr>
        </thead>
        <tbody>
        <tr class="total-sales">
            <td style="color: #333;width: 65%;padding: 10px 20px;border-right: 1px solid #ddd;border-top: 0;border-right: 0;border-bottom: 1px solid #ddd;"><b>إجمال المبيعات </b></td>
            <td dir="ltr" style="color: #333;padding: 10px 20px;border: 1px solid #ddd;border-top: 0"><b>{{$data["total_sales_ar"]}} </b></td>
        </tr>
        <tr class="sales-detail">
            <td style="padding-top: 20px !important;color: #333;width: 65%;border-right: 0;padding: 5px 20px 2px 20px;border-right: 1px solid #ddd;border-top: 0;border-bottom: 0">الدفع عند الإستلام من المطعم </td>
            <td dir="ltr" style="padding-top: 20px !important;color: #333;border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px">{{$data["cash_on_pickup_ar"]}} </td>
        </tr>
        <tr class="sales-detail">
            <td style="color: #333;width: 65%;border-right: 0;padding: 5px 20px 2px 20px;border-right: 1px solid #ddd;border-top: 0;border-bottom: 0">الدفع عند التوصيل </td>
            <td dir="ltr" style="color: #333;border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px">{{$data["cash_on_delivery_ar"]}} </td>
        </tr>

        <tr class="sales-detail">
            <td style="color: #333;width: 65%;border-right: 0;padding: 5px 20px 2px 20px;border-right: 1px solid #ddd;border-top: 0;border-bottom: 0">الدفع بالبطاقة اللإتمانية عن طريق الأنترنت</td>
            <td dir="ltr" style="color: #333;border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px">{{$data["online_card_payments_ar"]}} </td>
        </tr>

        <tr class="sales-detail">
            <td style="padding-bottom: 20px !important;border-bottom: 1px solid #ddd;color: #333;width: 65%;border-right: 0;padding: 5px 20px 2px 20px;border-right: 1px solid #ddd;border-top: 0">سداد</td>
            <td dir="ltr" style="padding-bottom: 20px !important;border-bottom: 1px solid #ddd;color: #333;border: 1px solid #ddd;border-top: 0;padding: 5px 20px 0 20px">{{$data["sadad_ar"]}} </td>
        </tr>

        <tr class="sales-detail-commision">
            <td style="text-align: right;padding-top: 20px !important;width: 65%;border-right: 0;border-right: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 2px 20px"><b>مجموع بدون الدفع عند الإستلام من المطعم </b></td>
            <td style="padding-top: 20px !important;border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px;padding-bottom: 10px"><b dir="ltr">{{$data["subtotal_ar"]}} </b></td>
        </tr>
        {{--<tr class="sales-detail-commision">--}}

            {{--<td style="text-align: right;width: 65%;border-right: 0;border-right: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 2px 20px"><b dir="ltr">{{$data["card_issuer_fees_rate_ar"]}}</b></td>--}}
            {{--<td style="border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px;padding-bottom: 10px"><b dir="ltr">{{$data["card_issuer_fees_ar"]}} </b></td>--}}
        {{--</tr>--}}
        <tr class="sales-detail-commision">
            <td style="text-align: right;width: 65%;border-right: 0;border-right: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 2px 20px"><b dir="ltr">{{$data["lugmety_commission_rate_ar"]}} </b></td>
            <td style="border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px;padding-bottom: 10px"><b dir="ltr">{{$data["lugmety_commission_ar"]}} </b></td>
        </tr>

        <tr class="sales-detail-commision">
            <td style="text-align: right;padding-bottom: 20px !important;border-bottom: 1px solid #ddd;width: 65%;border-right: 0;border-right: 1px solid #ddd;border-top: 0;padding: 5px 20px 2px 20px"><b dir="ltr"> {{$data["lugmety_commision_vat_title_in_ar"]}} </b></td>
            <td style="border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px;padding-bottom: 10px"><b dir="ltr">{{$data["lugmety_commission_vat_ar"]}} </b></td>
        </tr>

        @if($data['is_empty_logistic_fee'])
            <tr class="sales-detail-commision">
                <td style="text-align: right;width: 65%;border-right: 0;border-left: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 2px 20px"><b dir="ltr">LUGMETY.COM LOGISTICS FEE</b></td>
                <td style="border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px;padding-bottom: 10px"><b dir="ltr">{{$data["lugmety_total_logistics_fee_ar"]}} </b></td>
            </tr>

            <tr class="sales-detail-commision">
                <td style="text-align: right;width: 65%;border-right: 0;border-left: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 2px 20px"><b dir="ltr">LUGMETY.COM LOGISTICS FEE VAT</b></td>
                <td style="border: 1px solid #ddd;border-top: 0;border-bottom: 0;padding: 5px 20px 0 20px;padding-bottom: 10px"><b dir="ltr">{{$data["lugmety_total_logistics_fee_vat_ar"]}}</b></td>
            </tr>
        @endif

        <tr class="sales-detail">
            <td style="text-align: right;padding: 15px;border-bottom: 1px solid #ddd !important;color: #333;width: 65%;border-right: 0;border-right: 1px solid #ddd;border-top: 0;padding-bottom: 10px"><b>مجموع</b></td>
            <td style="color: #333;border: 1px solid #ddd;border-top: 1px solid #ddd;border-bottom: 1px solid #ddd;padding: 12px 20px 0 20px;padding-bottom: 10px;"><b dir="ltr">{{$data["total_credit_ar"]}} </b></td>
        </tr>
        </tbody>
    </table>
    <p style="margin-bottom: 30px;margin-top: 15px;color: #333;font-size: 14px;line-height: 1.6">{{$data["bank_info_arabic"]}}</p>
    <h2 style="color: #333333;font-size: 16px;">شكرًا لك على عملك .</h2>

</div>
</body>
</html>