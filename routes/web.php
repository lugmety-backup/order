<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
/**
 * Cart routes group
 */
$app->get("machine/request",[
    "uses" => "MachineIngrationController@sendPrinterRequest"
]);
/**
 * remove dashboard redis key
 */
$app->get("flush/dashboard/key", function (){
    echo 'key'. '<br>';
    \Illuminate\Support\Facades\Artisan::call('flush:dashboard-key');
});


$app->get("cron/modify/pickup",[
    "uses" => "OrderController@updatePickedUpOrder"
]);

$app->get("driver/invoice",function (){
    return view('invoice-driver-en');
});

$app->get("update/order",[
    "uses" => "OrderController@updateCustomerName"
]);

$app->get("machine/response",[
    "uses" => "MachineIngrationController@printerResponseRequest"
]);
$app->post("send/invoice",[
    "uses" => "OrderController@getSpecificRestaurantInvoice"
]);


$app->post("ipn",[
    "uses" => "IpnController@paytabsIpn"
]);


$app->get("earning/{countryId:[0-9]+}/rule",[
    "uses" => "DriverController@getEarningRule"
]);

$app->post("machine/status",[
    "uses" => "MachineStatusController@getRestaurantsStatus"
]);

$app->post("order/setting",[
    "uses" => "OrderSettingController@createOrUpdateSetting"
]);

$app->group(['branch operator  group','middleware' => 'checkTokenPermission'], function () use ($app) {
    $app->get('branch/machine/request', [
        "uses" => "TabletMachineIntegrationController@sendPrinterRequest"
    ]);

    $app->get('branch/machine/response', [
        "uses" => "TabletMachineIntegrationController@printerResponseRequest"
    ]);
    $app->get('branch/order/list',[
        'middleware' => 'checkTokenPermission',
        "uses" => "OrderController@branchOperatorOrderList"
    ]);

    $app->get('branch-operator/order/list',[
        'middleware' => 'checkTokenPermission',
        "uses" => "OrderController@branchOperatorOrderListById"
    ]);

    $app->put('branch/update/order/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission',
        "uses" => "OrderController@branchOperatorUpdateOrder"
    ]);

});
$app->group(['driver  group'], function () use ($app) {
    $app->get('track/order/{id:[0-9]+}',[
        "uses" => "DriverController@trackYourOrder"
    ]);


    $app->get('assigned/drivers',[
        "uses" => "DriverController@getAllAssignedDriver"
    ]);


    $app->get("driver/summary",[
        'middleware' => 'checkTokenPermission:view-driver-summary',
        "uses" => "DriverController@getSummaryForAdmin"
    ]);

    $app->get("driver/invoice/report",[
        'middleware' => 'checkTokenPermission:view-driver-summary',
        "uses" => "DriverController@getSpecificDriverInvoiceReport"
    ]);


    $app->get('admin/driver/list/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:view-driver-order-list',
        "uses" => "DriverController@driverSpecificOrderListById"
    ]);

    $app->get('driver/list/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission',
        "uses" => "DriverController@driverSpecificOrderListByIdForUser"
    ]);

    $app->get("driver/statistics/dashboard",[
        'middleware' => 'checkTokenPermission',
        "uses" => "DriverController@driverDashBoard"
    ]);
    $app->get('restaurants/nearby', [
        'middleware' => 'checkTokenPermission',
        "uses" => "DriverController@restaurantOrderListForDriver"
    ]);

    $app->get('restaurant/{id:[0-9]+}/orders', [
        'middleware' => 'checkTokenPermission',
        "uses" => "DriverController@orderListForDriver"
    ]);


    $app->get('driver/order/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission',
        "uses" => "DriverController@orderSpecificForDriver"
    ]);

    $app->get('admin/order/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:view-driver-order',
        "uses" => "DriverController@getSpecificDetailOfDriverOrderForAdmin"
    ]);

    $app->get('driver/order/history', [
        'middleware' => 'checkTokenPermission',
        "uses" => "DriverController@getListForDriver"
    ]);

    $app->get("driver/collection/history",[
        "middleware" => 'checkTokenPermission',
        "uses" => "DriverController@getSpecificDriverCollectionDetail"
    ]);

    $app->get('admin/order/history', [
        'middleware' => 'checkTokenPermission:view-driver-order',
        "uses" => "DriverController@getListForDriverOrderForAdmin"
    ]);

    $app->post("driver/assign/order",[
        "middleware" => 'checkTokenPermission',
        "uses" => "DriverController@assignOrderToDriver"
    ]);

    $app->post("admin/assign/order",[
        "middleware" => 'checkTokenPermission:create-driver-order',
        "uses" => "DriverController@assignedOrderToDriverByAdmin"
    ]);

    $app->put('driver/order/{id:[0-9]+}',[
        "middleware" => 'checkTokenPermission',
        "uses" => "DriverController@updateAssignedOrderToDriver"
    ]);
//update assigned order
    $app->put("admin/order/{id:[0-9]+}",[
        "middleware" => 'checkTokenPermission:edit-driver-order',
        "uses" => "DriverController@updateAssignedOrderToDriverByAdmin"
    ]);

    $app->put("admin/revoke/order/{id:[0-9]+}",[
        "middleware" => 'checkTokenPermission:edit-driver-order',
        "uses" => "DriverController@stopAssigningOrderFromDriver"
    ]);


    //assign order to other driver
    $app->put("admin/reassign/order/{id:[0-9]+}",[
        "middleware" => 'checkTokenPermission:edit-driver-order',
        "uses" => "DriverController@updateAssignedOrderToOtherDriverByAdmin"
    ]);
});

//$app->get("check/pickup",[
//    "uses" => "OrderController@pickedUpCaseChecking"
//]);

$app->group([' captain driver team  group'], function () use ($app) {
//list the drivers by driver captain
    $app->get("captain/team",[
        "middleware" => 'checkTokenPermission',
        "uses" => "DriverTeamController@getDriversInfoForCaptain"
    ]);
//collect cash from driver by captain id by driver captain
    $app->put("collect/{collectionId:[0-9]+}/driver/amount",[
        "middleware" => 'checkTokenPermission',
        "uses" => "DriverTeamController@collectAmountByCaptainDriver"
    ]);


    //get the details of drivers for receivable collection
    $app->get("driver/{id:[0-9]+}/receivable/collection",[
        "middleware" => 'checkTokenPermission',
        "uses" => "DriverTeamController@getSpecificDriverCollectionDetail"
    ]);

    // get driver collected collections
    $app->get("drivers/collection/history",[
        "middleware" => 'checkTokenPermission',
        "uses" => "DriverTeamController@getDriverCollectionCollectedHistory"
    ]);

    //get the detail of driver
    $app->get("driver/{id:[0-9]+}/received/collection",[
        "middleware" => 'checkTokenPermission',
        "uses" => "DriverTeamController@getSpecificDriverCollectionCollectedDetail"
    ]);

});
$app->group([' admin driver team  group'], function () use ($app) {
// list all the teams with team members count
    $app->get("admin/team",[
        "middleware" => 'checkTokenPermission:view-team',
        "uses" => "DriverTeamController@getAllTeamInfo"
    ]);
//List all the active or non active members based on the team id
    $app->get("admin/team/{id:[0-9]+}/drivers",[
        "middleware" => 'checkTokenPermission:view-team',
        "uses" => "DriverTeamController@getAllDriversListOfTeam"
    ]);

    $app->get("admin/team/member/history",[
        "middleware" => 'checkTokenPermission:view-team',
        "uses" => "DriverTeamController@getAllDriverHistoryOfTeam"
    ]);

//get all cash orders for each teams
    $app->get("admin/cash/order",[
        "middleware" => 'checkTokenPermission:view-cash-order',
        "uses" => "DriverTeamController@getCashOrders"
    ]);
//get individual team dashboard stats
    $app->get("admin/team/dashboard/{id:[0-9]+}",[
        "middleware" => 'checkTokenPermission:view-team',
        "uses" => "DriverTeamController@getTeamDashboardStats"
    ]);
//gets the specific team members cash order history based on the passed driverId
    $app->get("admin/team/cash/order/detail",[
        "middleware" => 'checkTokenPermission:view-cash-order',
        "uses" => "DriverTeamController@getTeamCashOrderDetail"
    ]);
//
    $app->get("admin/teams/collection/history",[
        "middleware" => 'checkTokenPermission:view-collection-history',
        "uses" => "DriverTeamController@getTeamsCollectionHistory"
    ]);

    $app->get("export/admin/teams/collection/history",[
        "middleware" => 'checkTokenPermission:view-collection-history',
        "uses" => "DriverTeamController@exportTeamsCollectionHistory"
    ]);

    $app->get("admin/team/collection/history/detail",[
        "middleware" => 'checkTokenPermission:view-collection-history',
        "uses" => "DriverTeamController@collectionHistoryTeamDetail"
    ]);










//Get specific team details including active as well as inactive members
    $app->get("team/{id:[0-9]+}",[
        "middleware" => 'checkTokenPermission:view-team',
        "uses" => "DriverTeamController@getSpecificTeam"
    ]);

//get assigned drivers including captain driver
    $app->get("assigned/team/members",[
        "uses" => "DriverTeamController@getAssignedDrivers"
    ]);
//create new team excluding its member
    $app->post("create/team",[
        "middleware" => 'checkTokenPermission:create-team',
        "uses" => "DriverTeamController@createTeam"
    ]);
//add new team members based on team id
    $app->put("team/{id:[0-9]+}/member",[
        "middleware" => 'checkTokenPermission:update-team',
        "uses" => "DriverTeamController@addDrivers"
    ]);
//restore deleted team members
    $app->put("team/{id:[0-9]+}/member/{driverId:[0-9]+}",[
        "middleware" => 'checkTokenPermission:update-team',
        "uses" => "DriverTeamController@restoreTeamMember"
    ]);
// collect driver collection amount from driver captain
    $app->put("admin/collection/{id:[0-9]+}",[
        "middleware" => 'checkTokenPermission:update-driver-collection',
        "uses" => "DriverTeamController@receiveCollectionByAdmin"
    ]);
// receive all amount form driver captain for specific order
    $app->put("admin/receive/collection/{teamId:[0-9]+}",[
        "middleware" => 'checkTokenPermission:update-driver-collection',
        "uses" => "DriverTeamController@receiveAllCollectionByAdmin"
    ]);

// Update team basic info like name and description but wont change the captain id
    $app->put("team/{id:[0-9]+}",[
        "middleware" => 'checkTokenPermission:update-team',
        "uses" => "DriverTeamController@updateTeamInfo"
    ]);
//soft delete team members
    $app->delete("team/{id:[0-9]+}/member",[
        "middleware" => 'checkTokenPermission:delete-team',
        "uses" => "DriverTeamController@removeDrivers"
    ]);
//soft delete team
    $app->delete("team/{id:[0-9]+}",[
        "middleware" => 'checkTokenPermission:delete-team',
        "uses" => "DriverTeamController@deleteTeam"
    ]);
});

$app->group(['cart  group'], function () use ($app) {
    /**
     * Get the cart details using hash
     */
    //$app->post('cart}', ['uses' => 'CartController@index']);

    /**
     * Insert item into cart.
     */
    $app->post('cart/create', ['uses' => 'CartController@addToCart']);
    /**
     * Remove item from cart
     */
    $app->delete('cart/delete', ['uses' => 'CartController@delete']);

    $app->post('cart/assign-user',[
        'middleware' => 'checkTokenPermission',
        "uses" => "CartController@assignUserId"
    ]);

    $app->post('cart/overwrite',[
        'middleware' => 'checkTokenPermission',
        "uses" => "CartController@overwriteCart"
    ]);

    $app->post('cart/update-quantity', ['uses' => 'CartController@updateQuantity']);

    $app->get('cart/reorder/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'CartController@reorder']);

    $app->post('cart/item-count', [
        'uses' => 'CartController@itemCount']);

    $app->get('cart/user-cart',[
        'middleware' => 'checkTokenPermission',
        'uses' => 'CartController@getCartOfUser']);

    $app->post('cart/details',[
        'uses' => 'CartController@getCart']);

    $app->post('cart/overwrite-guest-cart',[
        'uses' => 'CartController@overwriteGuestCart']);

    $app->delete('cart/remove-cart',[
        'middleware' => 'checkTokenPermission',
        "uses" => "CartController@removeUserCart"
    ]);

    $app->delete('cart/remove-guest-cart',[
        "uses" => "CartController@removeGuestCart"
    ]);

});
$app->get("check/redis",[
    "uses" => "CartController@checkRedis"
]);

$app->group(['order  group'], function () use ($app) {


    $app->get('order/history',[
        'middleware' => 'checkTokenPermission',
        "uses" => "OrderController@publicIndex"
    ]);


    $app->get('datatable/order',[
        'middleware' => 'checkTokenPermission',
        "uses" => "OrderController@adminIndex"
    ]);

    $app->get('pagination/order',[
        'middleware' => 'checkTokenPermission',
        "uses" => "OrderController@adminIndexPagination"
    ]);

    $app->get('order/{id:[0-9]+}/check/{userId:[0-9]+}',[
        "uses" => "OrderController@checkOrderForReview"
    ]);


    $app->get('generate/monthly',[
        'middleware' => 'checkTokenPermission:view-restaurant-monthly-report',
        "uses" => "OrderController@generateAllOrdersReports"
    ]);

    $app->get("refund/{id:[0-9]+}",[
        'middleware' => 'checkTokenPermission:view-refund',
        "uses" => "PartialRefundController@getAllPartialRefund"
    ]);

    $app->post("refund",[
        'middleware' => 'checkTokenPermission:create-refund',
        "uses" => "PartialRefundController@createPartialRefund"
    ]);

    $app->delete("refund/{id:[0-9]+}",[
        'middleware' => 'checkTokenPermission:delete-refund',
        "uses" => "PartialRefundController@deleteRefund"
    ]);





    $app->post('order',[
        'middleware' => 'checkTokenPermission',
        "uses" => "OrderController@store"
    ]);


    $app->put('order/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission',
        "uses" => "OrderController@adminUpdate"
    ]);

    $app->put('assign/order/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:assign-order',
        "uses" => "OrderController@assignOrderToAdmin"
    ]);

    $app->delete('order/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:order-delete',
        "uses" => "OrderController@delete"
    ]);

//    $app->get('export-csv',[
//      //  'middleware' => 'checkTokenPermission:order-delete',
//        "uses" => "OrderController@exportCsv"
//    ]);

});

$app->group(['order item group '], function () use($app) {
    $app->get("order/{id:[0-9]+}/item",[
        "middleware" => "checkTokenPermission",
        "uses" => "OrderItemController@viewOrderItem"
    ]);

    $app->get("order/{id:[0-9]+}/item/v2",[
        "middleware" => "checkTokenPermission",
        "uses" => "OrderItemController@viewOrderItemV2"
    ]);
});

$app->group(['order comment group'], function () use ($app) {

    $app->get('order/{order_id:[0-9]+}/comment',[
        'middleware' => 'checkTokenPermission:view-order-comment',
        "uses" => "OrderCommentController@viewAllOrderCommentByOrder"
    ]);

    $app->post('order/comment',[
        'middleware' => 'checkTokenPermission:create-order-comment',
        "uses" => "OrderCommentController@createComment"
    ]);

    $app->put('order/comment/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:edit-order-comment',
        "uses" => "OrderCommentController@updateComment"
    ]);

    $app->delete('order/comment/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:delete-order-comment',
        "uses" => "OrderCommentController@deleteOrderComment"
    ]);
});

$app->group(['Admin Customer comment group'], function () use ($app) {

    $app->get('customer/comments',[
        'middleware' => 'checkTokenPermission:view-customer-comment',
        "uses" => "CustomerCommentController@getAllUserComments"
    ]);

    $app->post('customer/comment',[
        'middleware' => 'checkTokenPermission:create-customer-comment',
        "uses" => "CustomerCommentController@createComment"
    ]);

    $app->put('customer/comment/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:edit-customer-comment',
        "uses" => "CustomerCommentController@updateComment"
    ]);

    $app->delete('customer/comment/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:delete-customer-comment',
        "uses" => "CustomerCommentController@deleteOrderComment"
    ]);
});

$app->group(['order status group'], function () use ($app) {

    $app->get('order/status',[
        'middleware' => 'checkTokenPermission:view-order-status',
        "uses" => "OrderStatusController@viewAllOrder"
    ]);

    $app->get('order/status/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:view-order-status',
        "uses" => "OrderStatusController@viewSpecificOrder"
    ]);

    $app->post('order/status',[
        'middleware' => 'checkTokenPermission:create-order-status',
        "uses" => "OrderStatusController@createStatus"
    ]);

    $app->put('order/status/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:edit-order-status',
        "uses" => "OrderStatusController@updateStatus"
    ]);

    $app->delete('order/status/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:delete-order-status',
        "uses" => "OrderStatusController@deleteStatus"
    ]);

});

$app->group(['order reject status group'], function () use ($app) {

    $app->get('admin/order/reject/status',[
        'middleware' => 'checkTokenPermission:view-order-reject-status',
        "uses" => "OrderRejectStatusController@index"
    ]);

    $app->get('admin/order/reject/status/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:view-order-reject-status',
        "uses" => "OrderRejectStatusController@show"
    ]);

    $app->get('public/order/reject/status',[
        "uses" => "OrderRejectStatusController@publicIndex"
    ]);

    $app->get('public/order/reject/status/{id:[0-9]+}',[
        "uses" => "OrderRejectStatusController@publicShow"
    ]);

    $app->post('admin/order/reject/status',[
        'middleware' => 'checkTokenPermission:create-order-reject-status',
        "uses" => "OrderRejectStatusController@store"
    ]);

    $app->put('admin/order/reject/status/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:edit-order-reject-status',
        "uses" => "OrderRejectStatusController@update"
    ]);

    $app->delete('admin/order/reject/status/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:delete-order-reject-status',
        "uses" => "OrderRejectStatusController@delete"
    ]);

});

$app->group(['delivery status group'], function () use ($app) {

    $app->get('delivery/status',[
        'middleware' => 'checkTokenPermission',
        "uses" => "DeliveryStatusController@viewAllDelivery"
    ]);

    $app->get('delivery/status/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission',
        "uses" => "DeliveryStatusController@viewSpecificDelivery"
    ]);

    $app->post('delivery/status',[
        'middleware' => 'checkTokenPermission:create-delivery-status',
        "uses" => "DeliveryStatusController@createStatus"
    ]);

    $app->put('delivery/status/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:edit-delivery-status',
        "uses" => "DeliveryStatusController@updateStatus"
    ]);

    $app->delete('delivery/status/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:delete-delivery-status',
        "uses" => "DeliveryStatusController@deleteStatus"
    ]);

});

$app->group(['payment status group'], function () use ($app) {

    $app->get('payment/status',[
        'middleware' => 'checkTokenPermission:view-payment-status',
        "uses" => "PaymentStatusController@viewAllPayment"
    ]);

    $app->get('payment/status/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:view-payment-status',
        "uses" => "PaymentStatusController@viewSpecificPayment"
    ]);

    $app->post('payment/status',[
        'middleware' => 'checkTokenPermission:create-payment-status',
        "uses" => "PaymentStatusController@createStatus"
    ]);

    $app->put('payment/status/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:edit-payment-status',
        "uses" => "PaymentStatusController@updateStatus"
    ]);

    $app->delete('payment/status/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:delete-payment-status',
        "uses" => "PaymentStatusController@deleteStatus"
    ]);

});

$app->group(['Shipping Method route group'], function () use ($app) {

    $app->post('shipping/method/calculate',['middleware' => 'checkTokenPermission',
        'uses' => 'ShippingMethodController@calculateShipping'
    ]);

    $app->get('shipping/method',['middleware' => 'checkTokenPermission:view-shipping-method',
        'uses' => 'ShippingMethodController@adminIndex'
    ]);

    $app->get('shipping/method/{id}',['middleware' => 'checkTokenPermission:view-shipping-method',
        'uses' => 'ShippingMethodController@adminShow'
    ]);

    $app->get('public/shipping/method',[
        'uses' => 'ShippingMethodController@publicIndex'
    ]);

    $app->get('public/shipping/method/{id}',[
        'uses' => 'ShippingMethodController@publicShow'
    ]);

    $app->post('shipping/method',['middleware' => 'checkTokenPermission:create-shipping-method',
        'uses' => 'ShippingMethodController@store'
    ]);

    $app->put('shipping/method/{id}',['middleware' => 'checkTokenPermission:edit-shipping-method',
        'uses' => 'ShippingMethodController@update'
    ]);

    $app->delete('shipping/method/{id}',['middleware' => 'checkTokenPermission:delete-shipping-method',
        'uses' => 'ShippingMethodController@delete'
    ]);

});

/**
 * Payment method route group
 */
$app->group(['payment method group'], function () use ($app) {
    /**
     * Get list of payment methods for admin
     */
    $app->get('admin/payment-method', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'PaymentMethodController@index'
    ]);
    /**
     * gets payment methods details based on passed array or slugs
     */
    $app->post('payment-shipping-method', [
        'uses' => 'PaymentMethodController@getListOfPaymentAndShippingMethodDetails'
    ]);

    /**
     * Create new payment method
     */
    $app->post('admin/payment-method', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'PaymentMethodController@store'
    ]);

    /**
     * Update existing payment method
     */
    $app->put('admin/payment-method/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'PaymentMethodController@update'
    ]);
    /**
     * Delete specific payment method
     */

    $app->delete('admin/payment-method/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'PaymentMethodController@destroy'
    ]);

    /**
     * Get Specific payment method
     */
    $app->get('admin/payment-method/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'PaymentMethodController@show'
    ]);
    /**
     *
     *
     */
    $app->post('payment-method/order/{id:[0-9]+}',
        [
//            'middleware' => 'checkTokenPermission',
            'uses' => 'PaymentMethodController@updateOrder'

        ]);
    /**
     * Get public(active) payment methods.
     */
    $app->get('public/payment-method', [
        'uses' => 'PaymentMethodController@publicShowAll'
    ]);

    /**
     * Get specific public payment method.
     */
    $app->get('public/payment-method/{id:[0-9]+}', [

        'uses' => 'PaymentMethodController@publicShow'
    ]);
    /**
     * Verify payment via creditcard.
     */
    $app->post('payment-method/verifyPayment', [
//        'middleware' => 'checkTokenPermission',
        'uses' => 'PaymentMethodController@verifyPayment',
    ]);

//    $app->get('payment-method/getNotification', function ()
//    {
//        LogStoreHelper::storeLogInfo([
//            "status" => "200",
//
//
//        ]);
//    });

});


/**
 * Driver Points route
 */

$app->group(['Driver Points route group'], function () use ($app) {

    $app->get('driver/points',['middleware' => 'checkTokenPermission:view-driver-points',
        'uses' => 'DriverPointsController@index'
    ]);

    $app->get('driver/points/{id:[0-9]+}',['middleware' => 'checkTokenPermission:view-driver-points',
        'uses' => 'DriverPointsController@show'
    ]);

    $app->get('driver/points/user',['middleware' => 'checkTokenPermission:view-driver-points',
        'uses' => 'DriverPointsController@indexByDriverName'
    ]);

    $app->post('driver/points',['middleware' => 'checkTokenPermission:create-driver-points',
        'uses' => 'DriverPointsController@store'
    ]);

    $app->put('driver/points/{id:[0-9]+}',['middleware' => 'checkTokenPermission:edit-driver-points',
        'uses' => 'DriverPointsController@update'
    ]);

    $app->delete('driver/points/{id:[0-9]+}',['middleware' => 'checkTokenPermission:delete-driver-points',
        'uses' => 'DriverPointsController@delete'
    ]);

});

/**
 * Driver OrderLocation route
 */

$app->group(['Driver Order Location route group'], function () use ($app) {

    $app->get('driver/order/location',['middleware' => 'checkTokenPermission:view-driver-order-location',
        'uses' => 'DriverOrderLocationController@index'
    ]);

    $app->get('driver/order/location/{id:[0-9]+}',['middleware' => 'checkTokenPermission:view-driver-order-location',
        'uses' => 'DriverOrderLocationController@show'
    ]);

    $app->get('driver/order/location/user/{driver_order_id:[0-9]+}',['middleware' => 'checkTokenPermission',
        'uses' => 'DriverOrderLocationController@showByDriverOrderId'
    ]);

    $app->post('driver/order/location',[
        'middleware' => 'checkTokenPermission',
        'uses' => 'DriverOrderLocationController@storeOrUpdate'
    ]);

});

$app->group(['Experience Point Rules  route group'], function () use ($app) {

    $app->get('datatable/experience/point/rule',[
        'middleware' => 'checkTokenPermission:view-xp-rule',
        'uses' => 'ExperiencePointRulesController@getAllRules'
    ]);

    $app->get('experience/point/rule/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:view-xp-rule',
        'uses' => 'ExperiencePointRulesController@getSpecificRule'
    ]);

    $app->post('experience/point/rule',[
        'middleware' => 'checkTokenPermission:create-xp-rule',
        'uses' => 'ExperiencePointRulesController@createRules'
    ]);

    $app->put('experience/point/rule/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:edit-xp-rule',
        'uses' => 'ExperiencePointRulesController@updateRules'
    ]);

    $app->delete('experience/point/rule/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:delete-xp-rule',
        'uses' => 'ExperiencePointRulesController@deleteRules'
    ]);

});

$app->group(['Earning Rules  route group'], function () use ($app) {

    $app->get('datatable/earning/rule',[
        'middleware' => 'checkTokenPermission:view-earning-rule',
        'uses' => 'EarningRulesController@getAllRules'
    ]);

    $app->get('earning/rule/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:view-earning-rule',
        'uses' => 'EarningRulesController@getSpecificRule'
    ]);

    $app->post('earning/rule',[
        'middleware' => 'checkTokenPermission:create-earning-rule',
        'uses' => 'EarningRulesController@createRules'
    ]);

    $app->put('earning/rule/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:edit-earning-rule',
        'uses' => 'EarningRulesController@updateRules'
    ]);

    $app->delete('earning/rule/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:delete-earning-rule',
        'uses' => 'EarningRulesController@deleteRules'
    ]);

});


$app->group(['driver xp  route group'], function () use ($app) {

    $app->get('datatable/driver/xp',[
        'middleware' => 'checkTokenPermission:view-driver-xp',
        'uses' => 'DriverXpController@getAllDriverXpByAdmin'
    ]);

    $app->get('admin/driver/xp/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:view-driver-xp',
        'uses' => 'DriverXpController@getSpecificDriverXpAdmin'
    ]);

//driver
    $app->get('driver/xp',[
        'middleware' => 'checkTokenPermission',
        'uses' => 'DriverXpController@getAllDriverXp'
    ]);
//driver
    $app->get('driver/xp/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission',
        'uses' => 'DriverXpController@getSpecificDriverXp'
    ]);


    $app->post('driver/xp',[
        'middleware' => 'checkTokenPermission:create-driver-xp',
        'uses' => 'DriverXpController@createDriverXpByAdmin'
    ]);

    $app->put('driver/xp/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:edit-driver-xp',
        'uses' => 'DriverXpController@updateDriverXpByAdmin'
    ]);

//    $app->delete('driver/xp/{id:[0-9]+}',[
//        'middleware' => 'checkTokenPermission:delete-earning-rule',
//        'uses' => 'EarningRulesController@deleteRules'
//    ]);

});

$app->group(['driver earning withdrawl  route group'], function () use ($app) {

    $app->get('datatable/driver/withdrawal/request',[
        'middleware' => 'checkTokenPermission:view-withdrawal-request',
        'uses' => 'DriverEarningWithdrawlRequestController@getAllDriverWithdrawlRequest'
    ]);

    $app->get('driver/withdrawal/request/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:view-withdrawal-request',
        'uses' => 'DriverEarningWithdrawlRequestController@getSpecificDriverWithdrawlRequest'
    ]);

//driver
    $app->get('driver/request',[
        'middleware' => 'checkTokenPermission',
        'uses' => 'DriverEarningWithdrawlRequestController@getAllDriverRequest'
    ]);
//driver
    $app->get('driver/request/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission',
        'uses' => 'DriverEarningWithdrawlRequestController@getSpecificDriverRequest'
    ]);

    $app->post('admin/driver/withdrawal/request',[
        'middleware' => 'checkTokenPermission:create-driver-request',
        'uses' => 'DriverEarningWithdrawlRequestController@createWithdrawlRequestByAdmin'
    ]);


    $app->post('driver/withdrawal/request',[
        'middleware' => 'checkTokenPermission',
        'uses' => 'DriverEarningWithdrawlRequestController@createWithdrawlRequest'
    ]);

    $app->put('driver/withdrawal/request/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:edit-withdrawal-request',
        'uses' => 'DriverEarningWithdrawlRequestController@updateWithdrawlRequest'
    ]);

//    $app->delete('driver/xp/{id:[0-9]+}',[
//        'middleware' => 'checkTokenPermission:delete-earning-rule',
//        'uses' => 'EarningRulesController@deleteRules'
//    ]);

});


$app->group(['driver earning  route group'], function () use ($app) {

    $app->get('datatable/driver/earning',[
        'middleware' => 'checkTokenPermission:view-driver-earning',
        'uses' => 'DriverEarningController@getAllDriverEarningByAdmin'
    ]);

    $app->get('admin/driver/earning/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:view-driver-earning',
        'uses' => 'DriverEarningController@getSpecificDriverEarningAdmin'
    ]);

//driver
    $app->get('driver/earning',[
        'middleware' => 'checkTokenPermission',
        'uses' => 'DriverEarningController@getAllDriverEarning'
    ]);
//driver
    $app->get('driver/earning/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission',
        'uses' => 'DriverEarningController@getSpecificDriverEarning'
    ]);


    $app->post('driver/earning',[
        'middleware' => 'checkTokenPermission:create-driver-earning',
        'uses' => 'DriverEarningController@createDriverEarningByAdmin'
    ]);

    $app->put('driver/earning/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:edit-driver-earning',
        'uses' => 'DriverEarningController@updateDriverEarningByAdmin'
    ]);

//    $app->delete('driver/earning/{id:[0-9]+}',[
//        'middleware' => 'checkTokenPermission:delete-earning-rule',
//        'uses' => 'EarningRulesController@deleteRules'
//    ]);

});

$app->group(['Order Route for statistics group'], function () use ($app) {

    $app->get('order/customer/{customer_id:[0-9]+}/stats',[
        'middleware' => 'checkTokenPermission:view-customer-stats',
        "uses" => "OrderController@customerStats"
    ]);
//done
    $app->get('device/statistics/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:view-device-report',
        "uses" => "ReportController@orderingDeviceReport"
    ]);
    //
    $app->get('highest-spenders/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:view-high-spenders-report',
        "uses" => "ReportController@highestSpendingCustomers"
    ]);

    $app->post('avg/delivery-time/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:view-avg-delivery-time-stats',
        "uses" => "ReportController@avgDeliveryTime"
    ]);
    $app->post('restaurant-transaction-report/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:restaurant-transaction-report',
        "uses" => "ReportController@parentRestaurantReportForAdmin"
    ]);

    $app->post('parent-restaurant-report/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:admin-restaurant-report',
        "uses" => "ReportController@parentRestaurantReportForRestaurantAdmin"
    ]);

    $app->get('branch-restaurant-report/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:branch-restaurant-report',
        "uses" => "ReportController@branchRestaurantReportForBranchAdmin"
    ]);

    $app->get('driver-report/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:driver-deliveries-report',
        "uses" => "ReportController@driverReport"
    ]);
    $app->get('driver/most-earnings/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:driver-earning-report',
        "uses" => "ReportController@mostMoneyEarned"
    ]);
    $app->post('driver-comparision/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:driver-avg-time-report',
        "uses" => "ReportController@driverComparision"
    ]);

    $app->get('export-csv/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:export-sales-csv',
        "uses" => "ReportController@exportCsv"
    ]);
    $app->get('view-csv-data/{id:[0-9]+}',[
        'middleware' => 'checkTokenPermission:export-sales-csv',
        "uses" => "ReportController@viewStats"
    ]);

    $app->get('dashboard-stats',[
        "uses" => "ReportController@getDashboardReport"
    ]);
    $app->get('operator-dashboard-stats',[
        "middleware" => 'checkTokenPermission:operator-food-update',
        "uses" => "ReportController@dashboardStatForOperator"
    ]);

    $app->get('admin-dashboard-stats/{id:[0-9]+}',[
        "middleware" => 'checkTokenPermission:admin-main-dashboard',
        "uses" => "ReportController@AdminDashBoardReport"
    ]);
    $app->get('restaurant/admin-dashboard-stats',[
        "middleware" => 'checkTokenPermission:res-admin-main-dashboard',
        "uses" => "ReportController@ResAdminDashBoardReport"
    ]);

});

$app->get('export-orders', 'ExportController@exportOrder');
$app->post('export-driver', 'ExportController@exportDriverOrders');
$app->get('test/error/message','ExampleController@index');
$app->get('export-driver-pickup-time', 'ExportController@setPickupTime');
$app->get('export-order-tax', 'ExportController@setOrderTax');

$app->get('/flush-redis', function () use ($app) {
    Redis::flushall();
    return "success";
});


