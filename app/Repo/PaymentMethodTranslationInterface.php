<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 7/16/17
 * Time: 3:34 PM
 */

namespace App\Repo;


interface PaymentMethodTranslationInterface
{

    public function createPaymentMethodTranslation(array $attributes);

    public function deletePaymentMethodTranslation($id);

}