<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 12:18 PM
 */

namespace App\Repo;


interface OrderStatusInterface
{
    public function getAllOrderStatus();

    public function getSpecificOrderStatus($id);

    public function getAllEnabledOrderStatus();

    public function getSpecificOrderStatusEnabled($id);

    public function getOrderStatusBasedOnSlug($slug);

    public function createOrderStatus(array $request);

    public function updateOrderStatus($id, array $request);

    public function deleteOrderStatus($orderStatus);

    public function getActiveOrderStatus();

    public function getAllOrderStatusIncludingTranslation();

}