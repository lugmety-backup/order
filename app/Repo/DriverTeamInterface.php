<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/2/2018
 * Time: 12:00 PM
 */

namespace App\Repo;


interface DriverTeamInterface
{
    public function getSpecificDataByColumn($columnName, $value);

    public function getAllRecordsByColumn($columnName,$value);

    public function getSpecificDataByColumnActiveInActive($columnName, $value);

    public function getAllRecordsActiveByColumn($columnName,$value);

    public function getTeamIdBasedOnForDriver($countryId , $driverId);

    public function getAllTeamForCashOrdersAdmin(array $request,array $originalRequest);

    public function getAllTeamForAdmin(array $request);

    public function getAllTeamCollectionHistoryForAdmin(array $request,array $originalRequest);

    public function updateTeamRecord($id, array $request);

    public function updateTeamMember($id, array $request);

    public function createNewRecord(array $request);

    public function getCurrentActiveTeamId($captainId);

    public function checkDriverIsAssignedToOtherTeam($captainId, array $driverIds);

    public function getTeamAndTeamMembersByCaptainId($captainId);

    public function softDeleteById($id);

    public function softDeleteByCaptainId($id);
}