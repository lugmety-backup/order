<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface DriverOrderLocationInterface
{
    public function getAllDriverOrderLocation();

    public function getDriverOrderLocationByDriverOrderId($driverOrderId);

    public function getSpecificDriverOrderLocation($id);

    public function createDriverOrderLocation(array $attributes);

    public function updateDriverOrderLocation($id, array $attributes);

    public function deleteDriverOrderLocation($id);

}