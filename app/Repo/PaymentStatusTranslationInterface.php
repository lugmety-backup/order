<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/15/2017
 * Time: 1:26 PM
 */

namespace App\Repo;


interface PaymentStatusTranslationInterface
{
    public function getAllPaymentStatusTranslation();

    public function getSpecificPaymentStatusTranslation($id);

    public function getSpecificPaymentStatusTranslationByCodeAndId($langCode,$paymentMethodId);

    public function createPaymentStatusTranslation(array $request);

    public function updatePaymentStatusTranslation($id, array $request);

    public function deletePaymentStatusTranslation($id);

    public function deletePaymentStatusTranslationByPaymentStatusId($paymentId);
}