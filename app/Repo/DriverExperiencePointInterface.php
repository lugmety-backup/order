<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 11:31 AM
 */

namespace App\Repo;


interface DriverExperiencePointInterface
{

    public function getAllDriverXp($userId, $country_id,$limit);

    public function  getAllDriverXpForAdmin($country_id,$userId);

    public function viewSpecificXp($id);

    public function createDriverXp(array $request);

    public function updateDriverXp($data , array $request);

    public function deleteXp($data);

    public function getAllDriverXpNormal($user_id,$countryId);

    public function getAllDriverDeducted($user_id,$countryId);

}