<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 1/28/2018
 * Time: 12:06 PM
 */

namespace App\Repo;


interface PartialRefundInterface
{
    public function getAllPartialRefund($orderId);
    
    public function getSpecificPartialRefund($id);
    
    public function createRefund(array $request);
    
    public function updateRefund($data ,array $request);
    
    public function deleteRefund($id);

}