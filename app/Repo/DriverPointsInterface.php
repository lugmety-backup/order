<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface DriverPointsInterface
{
    public function getAllDriverPoints();

    public function getAllDriverPointsByUserId();

    public function getSpecificDriverPoints($id);

    public function createDriverPoints(array $attributes);

    public function updateDriverPoints($id, array $attributes);

    public function deleteDriverPoints($id);

}