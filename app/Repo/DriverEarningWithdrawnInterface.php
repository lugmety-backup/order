<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 11:32 AM
 */

namespace App\Repo;


interface DriverEarningWithdrawnInterface
{

    public function getAllWithdrawnRequest();

    public function getAllDriverWithdrawnRequest($driverId);

    public function getSpecificDriverWithdrawnRequest($id);

    public function createWithdrawnRequest(array $request);

    public function updateWithdrawnRequest($id,array $request);

    public function  getAllDriverWithdrawnReqestByDriver($status,$limit,$driverId,$countryId);

    public function getSpecificDriverWithdrawnReqestByDriver($id,$driverId,$countryId);
}