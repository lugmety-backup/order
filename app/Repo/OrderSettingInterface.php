<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 6/5/2018
 * Time: 11:55 AM
 */

namespace App\Repo;


interface OrderSettingInterface
{
    public function getSetting($type,$date);

    public function getSettingForInvoice($type,$date);

    public function createSetting(array $request);

    public function updateSetting($originalData, array $request);


}