<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/11/2017
 * Time: 11:01 AM
 */

namespace App\Repo;


interface ShippingMethodInterface
{
    public function getAllShippingMethod();

    public function getAllShippingMethodByStatus($status);

    public function getSpecificShippingMethodBySlug($slug);

    public function getSpecificShippingMethod($id);

    public function createShippingMethod(array $attributes);

    public function updateShippingMethod($id , array $attributes);

    public function deleteShippingMethod($id);

    public function getAllShippingMethodIncludingTranslation();

}