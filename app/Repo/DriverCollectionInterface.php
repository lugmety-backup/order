<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/2/2018
 * Time: 12:01 PM
 */

namespace App\Repo;


interface DriverCollectionInterface
{
    public function createCollection(array $request);

    public function updateCollection($id, array $request);

    public function getSpecificCollection($id);

    public function getSpecificCollectionByTeamIDAndCountryId($id,$teamId,$countryId);

    public function getSpecificDriverCollectionBasedOnTeamId(array $request,$driverId);

    public function getSpecificDriverCollectedCollectionBasedOnTeamId(array $request,$driverId);

    public function getSpecificDriverCollectedCollectionBasedOnTeamIdForDriver(array $request , $driverId);

    public function getSpecificDriverCollectedCollectionBasedOnTeamIdForDriverForAdmin(array $request, array $originalRequest);

    public function getSpecificDriverCollectedCollectionHistoryDetailBasedOnTeamIdForDriverForAdmin(array $request,$originalRequest);

    public function getSpecificTeamMemberOrderHistory(array $request,$originalRequest);

    public function updateALLDriverCaptainCollectedCollectionBasedOnTeamIdForAdmin(array $request,$teamId);

//        public function
}