<?php

/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 6/11/2017
 * Time: 10:16 PM
 */
namespace App\Repo;


interface OrderItemInterface
{

    public function getAllItem();

    public function getSpecificItem($id);

    public function createItem(array $attributes);

    public function updateItem($id, array $attributes);

    public function deleteItem($id);

    public function getItemByOrderId($orderId);

}