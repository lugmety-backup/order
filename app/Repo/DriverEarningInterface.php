<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 11:33 AM
 */

namespace App\Repo;


interface DriverEarningInterface
{
    public function getAllDriverEarning($userId, $limit, $countryId);

    public function  getAllDriverEarningForAdmin($userId);

    public function viewSpecificEarning($id);

    public function createDriverEarning(array $request);

    public function updateDriverEarning($data , array $request);

    public function deleteEarning($data);

    public function getSpecificDriverEarning($country_id,$driver_id,$date);

    public function getEarningHistoryOfDriver($driver_id,$country_id,$date);

    public function getAllDriverEarningNormal($user_id,$country_id,$date_for = null);

    public function getAllDriverEarningDeducted($user_id,$countryId, $date_for = null);

    public function getSpecificDriverEarningDeduction($country_id,$driver_id, $date);
}