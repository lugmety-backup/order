<?php

/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 6/11/2017
 * Time: 10:16 PM
 */
namespace App\Repo;
interface OrderItemAddonInterface
{
    public function getAllAddon();

    public function getSpecificAddon($id);

    public function createAddon(array $attributes);

    public function updateAddon($id, array $attributes);

    public function deleteAddon($id);
}