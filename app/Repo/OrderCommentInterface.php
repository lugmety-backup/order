<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 7:13 AM
 */

namespace App\Repo;


interface OrderCommentInterface
{

    public function getSpecificComment($id);

    public function getAllCommentsByOrderId($orderId,$limit);

    public function createComment(array $request);

    public function updateComment($id,array $request);

    public function deleteComment($comment);



}