<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/15/2017
 * Time: 1:26 PM
 */

namespace App\Repo;


interface PaymentStatusInterface
{
    public function getAllPaymentStatus();

    public function getAllEnabledPaymentStatus();

    public function getSpecificPaymentStatusEnabled($id);

    public function getSpecificPaymentStatus($id);

    public function getPaymentStatusBasedOnSlug($slug);

    public function createPaymentStatus(array $request);

    public function updatePaymentStatus($id, array $request);

    public function deletePaymentStatus($paymentMethod);

    public function getAllPaymentStatusIncludingTranslation();
}