<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 12/13/2017
 * Time: 11:54 AM
 */

namespace App\Repo;


interface MachineStatusInterface
{

    public function getSpecificMachineStatus($id);

    public function createMachineStatus(array $request);

    public function getAllMachineStatus(array $restaurantIds);

    public function updateMachineStatus($specificData,array $request );
}