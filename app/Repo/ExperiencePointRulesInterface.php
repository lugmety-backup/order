<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 11:30 AM
 */

namespace App\Repo;


interface ExperiencePointRulesInterface
{
    public function getAllRules($countryId);

    public function getSpecialRule($id);

    public function createRule(array $request);

    public function updateRule($request , array $data);

    public function deleteRule($id);
}