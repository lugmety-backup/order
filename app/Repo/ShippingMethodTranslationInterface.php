<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/15/2017
 * Time: 1:26 PM
 */

namespace App\Repo;


interface ShippingMethodTranslationInterface
{
    public function getAllShippingMethodTranslation($id);

    public function getAllShippingMethodTranslationByLang($id, $lang);

    public function getSpecificShippingMethodTranslation($id);

    public function createShippingMethodTranslation(array $request);

    public function updateShippingMethodTranslation($id, array $request);

    public function deleteShippingMethodTranslation($id);
}