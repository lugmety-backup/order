<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/12/2017
 * Time: 6:46 AM
 */

namespace App\Repo;


interface OrderInterface
{

    public function getAllOrders($orderStatus,$restaurantId, $countryId, $user_id);

    public function getAllOrdersPagination($orderStatus, $countryId, $userId, $limit,$orderBy, $filter);

    public function getAllOrdersBasedOnUserId($orderStatus, $restaurantIdForFilter,$countryId,$user_id);

    public function getOrdersStatsOfCustomer($customer_id, $country_id);

    public function getOrdersByRestaurantId($restaurantId, $orderStatus,$countryId,$userId);

    public function getOrdersByRestaurantAndOrderId($restaurantId , $orderId);

    public function getOrdersByRestaurantAndOrderIdAndValidate($restaurantId , $orderId);

    public function getAllRestaurantForDriver($driverLat,$driverLong, $driverCountryId, $distanceLimit, $limit);

    public function getAllOrdersForDriver($restaurantId, $driverCountryId ,$limit);

    public function getOrdersByOrderIdAndUserId($id , $userId);

    public function getAllOrdersCustomer($limit,$userId,$countryId);

    public function getSpecificOrder($id);

    public function createOrder(array $request);

    public function updateOrder($id,array $request);

    public function deleteOrderSoft($order);

    public function deleteOrderHard($order);

    public function getSpecificSoftDeletedOrNotSoftDeletedOrder($id);

    public function  getOrderDetailsForReorder($id, $userId);

    public function getOrderByOrderIdOnly($orderId);

    public function getOrderByListOfRestaurantIdAndCountry(array $restaurantId,$countryId,$orderStatus,$userId);

    public function getOrdersByRestaurantIdForBranchOperator($branch_id,$orderStatus, $user_id,$limit);

    public function getOrdersForCsv($restaurantId=null, $startDate=null,$endDate=null);

    public function getNewUserFromPassedUsers(array $users);

}