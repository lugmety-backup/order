<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/22/2017
 * Time: 2:21 PM
 */

namespace App\Repo;


interface DriverOrderInterface
{
    public function getAllDriverOrder();
    public function getDriverOrderBasedOnId($id);
    public function getAllDriverOrderForAdmin($status);
    public function getDriverOrderBasedOnIdAndUserId($id,$userId);
    public function getAllDriverOrderByUserId($userId, $country_id  ,$limit);
    public function checkUser($userId);
    public function createDriverOrder(array $request);
    public function updateDriverOrder($order,array $request);

    public function getAssignedDriverBasedOnOrderId($order_id);

    public function getLastSevenDaysRecordsIncludingCurrentDate($user_id,$country_id);


    public function getCompletedOrder($userId,$countryId);
}