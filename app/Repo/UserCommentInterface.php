<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 5/15/2018
 * Time: 2:16 PM
 */

namespace App\Repo;


interface UserCommentInterface
{
    public function getSpecificComment($id);

    public function getAllCommentsByUserId($userId,$limit);

    public function createComment(array $request);

    public function updateComment($id,array $request);

    public function deleteComment($comment);
}