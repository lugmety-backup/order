<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/2/2018
 * Time: 12:00 PM
 */

namespace App\Repo;


interface DriverTeamMemberInterface
{
    public function getAllMembersByTeamId($teamId);

    public function getAllTeamMembers();

    public function softDeleteByTeamIdAndDriverId($teamId , $driverId);

    public function deleteAllTeamMember($teamId);

    public function deleteSpecificTeamMembers($teamId, array $driverIds);

    public function getDriverCashCollectionHistoryByCaptain(array $request);

//        public function checkDriverIsAssignedToOtherCaptain(array $driverIds,$captainId);

}