<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 12:18 PM
 */

namespace App\Repo;


interface OrderRejectStatusInterface
{
    public function getAllOrderRejectStatus();

    public function getSpecificOrderRejectStatus($id);

    public function getAllEnabledOrderRejectStatus();

    public function getSpecificOrderRejectStatusEnabled($id);

    public function getOrderRejectStatusBasedOnSlug($slug);

    public function setDefaultRejectStatus($id);

    public function setOtherOrderRejectStatusToNonDefault();

    public function createOrderRejectStatus(array $request);

    public function updateOrderRejectStatus($id, array $request);

    public function deleteOrderRejectStatus($orderStatus);

    public function getActiveOrderRejectStatus();


}