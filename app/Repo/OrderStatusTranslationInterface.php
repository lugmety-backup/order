<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 12:21 PM
 */

namespace App\Repo;


interface OrderStatusTranslationInterface
{
    public function getAllOrderStatusTranslation();

    public function getSpecificOrderStatusTranslation($id);

    public function getSpecificOrderStatusTranslationByCodeAndId($langCode,$orderStatusId);

    public function createOrderStatusTranslation(array $request);

    public function updateOrderStatusTranslation($id, array $request);

    public function deleteOrderStatusTranslation($id);

    public function deleteOrderStatusTranslationByOrderStatusId($orderId);
}