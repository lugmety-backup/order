<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 3/11/2018
 * Time: 11:53 AM
 */

namespace App\Repo;


interface CronInvoiceInterface
{
    public function createInvoice(array $data);

    public function getInvoiceInfo($countryId);

    public function updateInvoiceInfo($data, array $request,$storeDate);

    public function checkMonthIsChanged($countryId, $date);

}