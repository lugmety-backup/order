<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 6/25/17
 * Time: 11:11 AM
 */

namespace App\Repo;


interface PaymentMethodInterface
{
    public function getAll();

    public function getSpecificPaymentMethod($id);

    public function getListOfPaymentMethod($slug);

    public function createPaymentMethod(array $attributes);

    public function updatePaymentMethod($id, array $attributes);

    public function deletePaymentMethod($id);

    public function getSpecificPaymentMethodBySlug($slug);

    public function getActivePaymentMethod();

    public function getSpecificActivePaymentMethod($id);

    public function getAllPaymentMethodIncludingTranslation();


}