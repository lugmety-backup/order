<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 6/25/17
 * Time: 11:14 AM
 */

namespace App\Repo\Eloquent;

use App\Models\PaymentMethod;
use App\Repo\PaymentMethodInterface;


class PaymentMethodRepo implements PaymentMethodInterface
{
    /**
     * @var PaymentMethod
     */
    private $method;

    /**
     * PaymentMethodRepo constructor.
     * @param PaymentMethod $method
     */
    public function __construct(PaymentMethod $method)
    {
        $this->method = $method;
    }

    /**
     * Get payment method list according to the value of $limit
     * @param $limit
     * @return mixed
     */
    public function getAll()
    {
        return $this->method->orderby('id','desc')->get();
    }

    /**
     * Get one specific payment method using its id.
     * @param $id
     */
    public function getSpecificPaymentMethod($id)
    {
        return $this->method->findOrFail($id);
    }

    public function getListOfPaymentMethod($slug){
        return $this->method->where([
                ['slug',$slug],
                ["status","1"]
            ]
        )->first();
    }

    /**
     * Create new payement method.
     * Insert the validated inputs in payment_method table.
     * @param array $attributes
     * @return mixed
     */
    public function createPaymentMethod(array $attributes)
    {
        $con= $this->method->create($attributes);
        return $con;
    }

    /**
     * Update existing payment method.
     * @param $id
     * @param array $attributes
     */
    public function updatePaymentMethod($id, array $attributes)
    {
        $this->method->findOrFail($id)->update($attributes);
        $con = $this->method->findOrFail($id);
        return $con;
    }

    /**
     * Delete existing payment method.
     * @param $id
     * @return mixed
     */
    public function deletePaymentMethod($id)
    {
        return $this->method->findOrFail($id)->delete();
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getSpecificPaymentMethodBySlug($slug)
    {
        return $this->method->where(
            [
                ['slug', $slug],
                ["status","1"]
            ])->first();
    }

    /**
     * @return mixed
     */
    public function getActivePaymentMethod()
    {
        return $this->method->where('status','1')->get();
    }

    /*
     *
     */
    public function getSpecificActivePaymentMethod($id)
    {
        return $this->method->where('status','1')->findOrFail($id);
    }

    public function getAllPaymentMethodIncludingTranslation()
    {
        return $this->method->with('paymentMethodTranslation')->get();
    }

}