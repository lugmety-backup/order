<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 6/5/2018
 * Time: 11:58 AM
 */

namespace App\Repo\Eloquent;


use App\Models\OrderSetting;
use App\Repo\OrderSettingInterface;

class OrderSettingRepo implements OrderSettingInterface
{
    /**
     * @var $setting
     */
    protected $setting;

    /**
     * OrderSettingRepo constructor.
     * @param OrderSetting $setting
     */
    public function __construct(OrderSetting $setting)
    {
        $this->setting = $setting;
    }

    /**
     * fetch setting based on date and the type
     * @param $type - the type of setting for eg driver means driver settings
     * @param $date - date means the setting to fetch for the passed date
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getSetting($type, $date)
    {
        return $this->setting->where([
            'type' => $type,
            'date_for' => $date
        ])->first();
    }

    public function getSettingForInvoice($type, $date)
    {
        $setting = $this->setting->where([
            ['type',$type],
            ['date_for',$date]
        ])->first();

        if(!$setting){
            $setting = $this->setting->where([
                ['type',$type]
            ])->orderBy('date_for','desc')->first();
            $this->createSetting([
                'type' => $type  ,
                'date_for' => $date,
                'data' => unserialize($setting->data)
            ]);
        }
        return $setting;

    }


    /**
     * create new setting
     * @param array $request
     * @return $this|\Illuminate\Database\Eloquent\Model
     */
    public function createSetting(array $request)
    {
       return $this->setting->create($request);
    }

    /**
     * update the existing setting
     * @param $originalData
     * @param array $request
     * @return mixed
     */
    public function updateSetting($originalData, array $request)
    {
       return $originalData->update($request);
    }

}