<?php

/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/22/2017
 * Time: 2:25 PM
 */

namespace App\Repo\Eloquent;
use App\Exceptions\DuplicateRecordException;
use App\Repo\DriverOrderInterface;
use App\Models\DriverOrder;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class DriverOrderRepo implements DriverOrderInterface
{
    protected $driverOrder;

    /**
     * DriverOrderRepo constructor.
     * @param $driverOrder
     */
    public function __construct(DriverOrder $driverOrder)
    {
        $this->driverOrder = $driverOrder;
    }


    public function getAllDriverOrder()
    {
        return $this->driverOrder->all();
    }

    public function getDriverOrderBasedOnId($id)
    {
        return $this->driverOrder->findOrFail($id);
    }

    public function getDriverOrderBasedOnIdAndUserId($id,$userId){
        return $this->driverOrder->where([
            ["id",$id],
            ['user_id',$userId],
            ['status',"!=",'reassigned']
        ])->firstOrFail();
    }

    public function getAllDriverOrderForAdmin($status){
        if(is_null($status)){
            $data =  $this->driverOrder->orderBy('updated_at', 'desc')->get();
        }
        else {
            $data = $this->driverOrder->where([
                ["status", $status]
            ])->orderBy('updated_at', 'desc')->get();
        }
//        $data = new Collection($data);
        return $data;
    }

    public function getAllDriverOrderByUserId($userId,$country_id,$limit){
        return  $this->driverOrder->where([
            ["user_id",$userId],
            ["country_id",$country_id],
            ['status',"!=",'reassigned']
        ])->orderBy('updated_at', 'desc')->paginate($limit);
    }

    public function checkAnotherDriverIsAssigned($userId,$restaurantId,$orderId){
        $assignedOrder = $this->driverOrder->where([
            ["restaurant_id",$restaurantId],
            ["status","assigned"],
            ['user_id','!=',$userId],
            ["order_id",$orderId]])->sharedLock()->first();
        if(!$assignedOrder){
//           $deleteRecord = $this->driverOrder->where([
//                ["country_id",$countryId],
//                ["status","assigned"],
//                ["user_id",$userId],
//                ["order_id",$orderId]])->first()->delete();
            return false;
        }
        else{
            return true;
        }

    }

    public function createDriverOrder(array $request)
    {
        $check_if_the_record_exist = $this->driverOrder->where([
            'order_id' => $request['order_id'],
            'user_id' => $request['user_id'],
            'status' => $request['status']
        ])->lockForUpdate()->first();
        if($check_if_the_record_exist){
            throw new DuplicateRecordException('Order is already assigned to driver.Please try again');
        }
        return $this->driverOrder->create($request);
    }

    public function checkUser($userId){
        return $this->driverOrder->where([
                ["user_id",$userId],
                ["status",'!=',"delivered"],
                ["status",'!=',"reassigned"]
            ]
        )->lockForUpdate()->first();
    }

    public function updateDriverOrder($order,array $request)
    {
        return $order->update($request);
    }

    public function getCompletedOrder($userId,$country_id)
    {
        return $this->driverOrder->where([
                ["user_id",$userId],
                ["status","delivered"],
                ["country_id",$country_id],
            ]
        )->count();
    }

    public function getTodayCompletedOrder($userId,$countryId){
        return $this->driverOrder->where([
                ["user_id",$userId],
                ["status","delivered"],
                ["country_id",$countryId],
                ["created_at" ,">=",Carbon::today()->toDateString()]
            ]
        )->count("id");
    }

    public function getDayCompletedOrder($userId,$day,$countryId){
        return $this->driverOrder->where([
                ["user_id",$userId],
                ["status","delivered"],
                ["country_id",$countryId],
            ]
        )->whereBetween("created_at",$day)->count("id");
    }


    public function getWeeklyCompletedOrder($userId){
        return $this->driverOrder->where([
                ["user_id",$userId],
                ["status","delivered"],
            ]
        )->whereBetween("created_at",[
            Carbon::parse('last sunday')->startOfDay(),
            Carbon::parse('next saturday')->endOfDay(),
        ])->count("id");
    }

    public function getDriverBasedOnOrderId($order_id,$type){
        if($type =="admin")return $this->driverOrder->select("id","status","user_id","order_id","order_history")->where("order_id",$order_id)->orderBy('status', 'ASC')->get();
        else return $this->driverOrder->select("id","status","user_id","order_id","order_history")->where([["order_id",$order_id],["status","assigned"]])->first();
    }

    public function getAllAssignedDriver(){
        return $this->driverOrder->where([
                ["status",'!=',"delivered"],
                ["status",'!=',"reassigned"]
            ]
        )->pluck('user_id')->values()->all();
    }

    public function getDriverOrderByOrderId($orderId)
    {
        return $this->driverOrder->where([
            ["order_id",$orderId],
            ["status","picked"]
        ])->first();
    }

    public function getLastSevenDaysRecordsIncludingCurrentDate($user_id,$country_id){
        $data = DB::select("
        SELECT tmp.dt,  src.order_count
            FROM (
                 SELECT CURDATE() - INTERVAL 7 DAY as dt UNION
                SELECT CURDATE() - INTERVAL 6 DAY UNION
                    SELECT CURDATE() - INTERVAL 5 DAY UNION
                    SELECT CURDATE() - INTERVAL 4 DAY UNION
                    SELECT CURDATE() - INTERVAL 3 DAY UNION
                    SELECT  CURDATE() - INTERVAL 2 DAY UNION
                    SELECT  CURDATE() - INTERVAL 1 DAY UNION
                    SELECT CURDATE()) as tmp
                left join (
                    SELECT
                        DATE(updated_at ) AS `Date`,
                         COUNT(*) AS `order_count`
                    FROM
                        driver_order
                    WHERE(
                        updated_at >=  CURDATE() - INTERVAL 7 DAY
                        AND
                        user_id = $user_id
                        AND
                        country_id = $country_id
                        AND
                        status = 'delivered'
                    )
                    GROUP BY
                        DATE(updated_at )
                ) as src on tmp.dt = src.`Date`
        ");
        return $data;
    }

    public function getAssignedDriverBasedOnOrderId($order_id)
    {
        return $this->driverOrder->where([
            ["status",'!=',"delivered"],
            ["status",'!=',"reassigned"],
            ['order_id',$order_id]
        ])->firstOrFail();
    }

    public function getTodayDeliveredOrder(){
        return $this->driverOrder->where([
            ["status","delivered"]
        ])
            ->whereDate('created_at', Carbon::today()->startOfDay())
            ->get();
    }


}