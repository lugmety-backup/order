<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 5/15/2018
 * Time: 2:17 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\UserCommentInterface;
use App\Models\CustomerComment;
class UserCommentRepo implements UserCommentInterface
{
    private $customer_comment;

    public function __construct(CustomerComment $comment)
    {
        $this->customer_comment = $comment;
    }

    public function getSpecificComment($id)
    {
        return $this->customer_comment->findOrFail($id);
    }

    public function getAllCommentsByUserId($userId, $limit)
    {
        return $this->customer_comment->where('user_id',$userId)->orderBy('updated_at','desc')->paginate($limit)->withPath('/customer/comments');
    }

    public function createComment(array $request)
    {
       return $this->customer_comment->create($request);
    }

    public function updateComment($id, array $request)
    {
        return $this->customer_comment->findOrFail($id)->update($request);
    }

    public function deleteComment($comment)
    {
        return $comment->delete();
    }


}