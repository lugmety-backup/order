<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/2/2018
 * Time: 12:02 PM
 */

namespace App\Repo\Eloquent;


use App\Models\DriverTeamMember;
use App\Repo\DriverTeamMemberInterface;
use Illuminate\Support\Facades\DB;

class DriverTeamMembersRepo implements DriverTeamMemberInterface
{
    protected $driverMember;

    public function __construct(DriverTeamMember $driverTeamMember)
    {
        $this->driverMember = $driverTeamMember;
    }

    public function getAllMembersByTeamId($teamId)
    {
        return $this->driverMember->where("driver_team_id",$teamId)->get();
    }

    public function getAllMembersByTeamIdSoftDeletedAsWell($teamId)
    {
        return $this->driverMember->where("driver_team_id",$teamId)->withTrashed()->pluck("driver_id")->values()->all();
    }

    public function getAllTeamMembers(){
        return $this->driverMember->
        orderBy("driver_id","desc")->get();
    }

    public function getDriversInfoForCaptain(array $request){
        $captain_id = $request["captain_id"];
        $startDate = $request["start_date"];
        $endDate = $request["end_date"];
        $countryId = $request["country_id"];
        $members = [];
        $members["list"] =  $this->driverMember->
        join("driver_teams","driver_team_members.driver_team_id","=","driver_teams.id")
            ->where("driver_teams.captain_id",$captain_id)
            ->whereNull("driver_teams.deleted_at")
            ->where("country_id",$countryId)
            ->select("driver_team_members.*" ,"driver_teams.country_id",
                DB::raw("
                (SELECT CASE WHEN sum(amount) IS NULL THEN 0 ELSE sum(amount) END  FROM driver_collections dc where dc.team_id=driver_team_members.driver_team_id AND  dc.driver_id=driver_team_members.driver_id AND dc.leader_received_at is null AND dc.country_id = '". $countryId . "' AND  (dc.created_at BETWEEN '". $startDate . "' AND '". $endDate . "')   ) as collectable_amount_form_driver
                 " ))->groupBy("driver_team_members.driver_id")->orderBy("driver_team_members.id","asc")->withTrashed()->get();

        $summary= DB::select("
        select 
        CASE WHEN sum( amount ) is NULL THEN 0 ELSE sum( amount ) END as total_receivable_amount  
        FROM driver_collections as dc 
        JOIN driver_teams as dt
        where dt.id =  dc.team_id AND  dc.leader_received_at is null
        AND dt.captain_id = '". $captain_id . "'
         AND dt.country_id = '". $countryId . "'
        AND dt.deleted_at is null 
        AND (dc.created_at BETWEEN '". $startDate . "' AND '". $endDate . "')
       
 ");
        $total_collected_amount= DB::select("
        select 
       CASE WHEN sum( amount ) is NULL THEN 0 ELSE sum( amount ) END as total_received_amount  
        FROM driver_collections as dc 
        JOIN driver_teams as dt
        where dt.id =  dc.team_id AND  dc.leader_received_at is not null
        AND dt.captain_id = '". $captain_id . "'
        AND dt.country_id = '". $countryId . "'
        AND dt.deleted_at is null
        AND (dc.created_at BETWEEN '". $startDate . "' AND '". $endDate . "')
 ");

        $members["summary"]["total_receivable_amount"] = $summary[0]->total_receivable_amount;
        $members["summary"]["total_collected_amount"] = $total_collected_amount[0]->total_received_amount;
        return $members;

    }


    public function getDriverCashCollectionHistoryByCaptain(array $request){
        $captain_id = $request["captain_id"];
        $startDate = $request["start_date"];
        $endDate = $request["end_date"];
        $countryId = $request["country_id"];
        $members = [];
        $members["list"] =  $this->driverMember->
        join("driver_teams","driver_team_members.driver_team_id","=","driver_teams.id")
            ->where("driver_teams.captain_id",$captain_id)
            ->whereNull("driver_teams.deleted_at")
            ->where("country_id",$countryId)
            ->select("driver_team_members.*" ,"driver_teams.country_id",
                DB::raw("
                (SELECT CASE WHEN sum(amount) IS NULL THEN 0 ELSE sum(amount) END  FROM driver_collections dc where dc.team_id=driver_team_members.driver_team_id AND  dc.driver_id=driver_team_members.driver_id AND dc.leader_received_at is not null AND dc.country_id = '". $countryId . "' AND  (dc.created_at BETWEEN '". $startDate . "' AND '". $endDate . "')   ) as collected_amount_form_driver
                 " ))->groupBy("driver_team_members.driver_id")->orderBy("driver_team_members.id","asc")->withTrashed()->get();
        return $members;
    }

    public function softDeleteByTeamIdAndDriverId($teamId, $driverId)
    {
        return $this->driverMember->where([
            ["driver_team_id",$teamId].
            ["driver_id",$driverId]
        ])->delete();
    }

    public function deleteSpecificTeamMembers($teamId, array $driverIds){
        return $this->driverMember->where("driver_team_id",$teamId)->whereIn("driver_id",$driverIds)->delete();
    }

    public function deleteAllTeamMember($teamId)
    {
        return $this->driverMember->where("driver_team_id",$teamId)->delete();
    }

//    public function checkDriverIsAssignedToOtherCaptain(array $driverIds, $teamId)
//    {
//       return $this->driverMember->where("")
//    }


}