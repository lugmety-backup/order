<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 12:27 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\OrderStatusTranslationInterface;
use App\Models\OrderStatusTranslation;
class OrderStatusTranslationRepo implements OrderStatusTranslationInterface
{

    protected $orderStatusTranslation;

    /**
     * OrderStatusTranslationRepo constructor.
     * @param $orderStatusTranslation
     */
    public function __construct(OrderStatusTranslation $orderStatusTranslation)
    {
        $this->orderStatusTranslation = $orderStatusTranslation;
    }

    public function getAllOrderStatusTranslation()
    {
        // TODO: Implement getAllOrderStatusTranslation() method.
    }

    public function getSpecificOrderStatusTranslation($id)
    {
        return $this->orderStatusTranslation->findOrFail($id);
    }

    public function createOrderStatusTranslation(array $request)
    {

        return $this->orderStatusTranslation->create($request);
    }

    public function getSpecificOrderStatusTranslationByCodeAndId($langCode,$orderStatusId){
        return $this->orderStatusTranslation->where([["lang_code",$langCode],["order_status_id",$orderStatusId]])->get();
    }

    public function updateOrderStatusTranslation($id, array $request)
    {
        // TODO: Implement updateOrderStatusTranslation() method.
    }

    public function deleteOrderStatusTranslation($id)
    {
        // TODO: Implement deleteOrderStatusTranslation() method.
    }

    public function deleteOrderStatusTranslationByOrderStatusId($orderId)
    {
       return $this->orderStatusTranslation->where("order_status_id",$orderId)->delete();
    }


}