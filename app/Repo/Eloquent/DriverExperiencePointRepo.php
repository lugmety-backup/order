<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 4:19 PM
 */

namespace App\Repo\Eloquent;


use App\Models\DriverExperiencePoint;
use App\Repo\DriverExperiencePointInterface;

class DriverExperiencePointRepo implements DriverExperiencePointInterface
{
    protected $driverXp;

    public function __construct(DriverExperiencePoint $ex)
    {
        $this->driverXp = $ex;
    }

    public function getAllDriverXpForSummary($userId)
    {
        return $this->driverXp->where("user_id",$userId)->get();
    }

    public function getAllDriverXp($userId,$country_id, $limit)
    {
        return $this->driverXp->where([["user_id",$userId],["type","normal"],["country_id",$country_id]])->paginate($limit);
    }

    public function viewSpecificXp($id)
    {
        return $this->driverXp->findOrFail($id);
    }

    public function createDriverXp(array $request)
    {
        return $this->driverXp->create($request);
    }

    public function updateDriverXp($id, array $request)
    {
        return $this->driverXp->findOrFail($id)->update($request);
    }

    public function getAllDriverXpForAdmin($userId,$countryId)
    {
        if(is_null($userId)) return $this->driverXp->where("country_id",$countryId)->get();
        return $this->driverXp->where([["user_id",$userId],["country_id",$countryId]])->get();
    }


    public function deleteXp($data)
    {
        // TODO: Implement deleteXp() method.
    }

    public function getAllDriverXpNormal($user_id,$countryId)
    {
        return $this->driverXp->where([
            ["user_id",$user_id],
            ["country_id",$countryId],
            ["type","normal"]
        ])->sum("experience_point");
    }

    public function getAllDriverDeducted($user_id,$countryId)
    {
        return $this->driverXp->where([
            ["user_id",$user_id],
            ["country_id",$countryId],
            ["type","deduction"]
        ])->sum("experience_point");
    }


}