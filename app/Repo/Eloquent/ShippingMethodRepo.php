<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 6/11/2017
 * Time: 3:19 PM
 */

namespace App\Repo\Eloquent;

use App\Repo\ShippingMethodInterface;
use App\Models\ShippingMethod;

class ShippingMethodRepo implements ShippingMethodInterface
{
    protected $shippingMethod;

    public function __construct(ShippingMethod $shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;
    }

    public function getSpecificShippingMethodBySlug($slug)
    {
        return $this->shippingMethod->where(
            [
                ['slug', $slug],
                ["status","1"]
            ])->get();
    }

    public function getSpecificShippingMethod($id)
    {
        return $this->shippingMethod->findOrFail($id);
    }

    public function getAllShippingMethodByStatus($status)
    {
        if($status == null){
            return $this->shippingMethod->all();
        }
        else{
            return $this->shippingMethod->where('status', $status)->get();
        }
    }

    public function getAllShippingMethod()
    {
        return $this->shippingMethod->all();
    }

    public function createShippingMethod(array $attributes)
    {
        return $this->shippingMethod->create($attributes);
    }

    public function updateShippingMethod($id, array $attributes)
    {
        return $this->shippingMethod->findOrFail($id)->update($attributes);
    }

    public function deleteShippingMethod($id)
    {
        return $this->shippingMethod->findOrFail($id)->delete();
    }

    public function getAllShippingMethodIncludingTranslation()
    {
        return $this->shippingMethod->with("shippingMethodTranslation")->get();
    }


}