<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/2/2018
 * Time: 12:02 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\DriverCollectionInterface;
use App\Models\DriverCollection;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DriverCollectionRepo implements DriverCollectionInterface
{
    protected $driverCollection;
    public function __construct(DriverCollection $driverCollection)
    {
        $this->driverCollection = $driverCollection;
    }

    public function createCollection(array $request)
    {
        return $this->driverCollection->create($request);
    }

    public function updateCollection($id, array $request)
    {
        return $this->driverCollection->findOrFail($id)->update($request);
    }

    public function getSpecificDriverCollectionBasedOnTeamId(array $request,$driverId)
    {

        $driverReceivableCollectionList["info"]=  $this->driverCollection->where([
            ["team_id",$request["team_id"]],
            ["driver_id",$driverId],
            ["country_id",$request["country_id"]]
        ])
            ->whereNull("leader_received_at")
            ->orderBy("created_at","desc")->paginate($request["limit"])->withPath("/driver/$driverId/receivable/collection")->appends($request);

        $collectionAmount["summary"]["total_collection_amount"] = $this->driverCollection->where([
            ["team_id",$request["team_id"]],
            ["driver_id",$driverId],
            ["country_id",$request["country_id"]]
        ])
            ->whereNotNull("leader_received_at")
            ->sum("amount");
//        dd($collectionAmount["total_collection_amount"]);
        if(!isset($collectionAmount["summary"]["total_collection_amount"])){
            $collectionAmount["summary"]["total_collection_amount"] =0;
        }
        $driverReceivableCollectionList["summary"] =$collectionAmount;
        return $driverReceivableCollectionList;
    }


    public function getSpecificDriverCollectedCollectionBasedOnTeamId(array $request,$driverId)
    {

        $driverReceivableCollectionList["info"]=  $this->driverCollection->where([
            ["team_id",$request["team_id"]],
            ["driver_id",$driverId],
            ["country_id",$request["country_id"]]
        ])
            ->whereNotNull("leader_received_at")
            ->orderBy("created_at","desc")->paginate($request["limit"])->withPath("/driver/$driverId/received/collection")->appends($request);

        $collectionAmount["summary"]["total_collection_amount"] = $this->driverCollection->where([
            ["team_id",$request["team_id"]],
            ["driver_id",$driverId],
            ["country_id",$request["country_id"]]
        ])
            ->whereNotNull("leader_received_at")
            ->sum("amount");
//        dd($collectionAmount["total_collection_amount"]);
        if(!isset($collectionAmount["summary"]["total_collection_amount"])){
            $collectionAmount["summary"]["total_collection_amount"] =0;
        }
        $driverReceivableCollectionList["summary"] =$collectionAmount;
        return $driverReceivableCollectionList;
    }

    public function getSpecificDriverCollectedCollectionBasedOnTeamIdForDriver(array $request, $driverId)
    {
        return  $this->driverCollection
            ->join("driver_teams","driver_teams.id","=","driver_collections.team_id")
            ->where([

                ["driver_id",$driverId],
                ["driver_collections.country_id",$request["country_id"]]
            ])
            ->select("driver_collections.*","driver_teams.name as team_name")
            ->whereNotNull("leader_received_at")
            ->orderBy("created_at","desc")->paginate($request["limit"])->withPath("/driver/collection/history")->appends($request);

    }

    public function getSpecificCollection($id)
    {
        return $this->driverCollection->findOrFail($id);
    }

    public function updateALLDriverCaptainCollectedCollectionBasedOnTeamIdForAdmin(array $request, $teamId)
    {
        $data['admin_received_at'] = $request['admin_received_at'];
        $data['admin_id'] = $request['admin_id'];
       return  $records = $this->driverCollection
            ->where([
                ['team_id',$teamId],
                ['admin_id',0]
            ])
            ->whereNotNull("leader_received_at")
            ->whereBetween("driver_collections.created_at",[$request["start_date"],$request["end_date"]])
            ->update($data);
    }


    public function getSpecificCollectionByTeamIDAndCountryId($id,$teamId,$countryId){
        return $this->driverCollection->where([
            ["team_id",$teamId],
            ["country_id",$countryId],
            ["id",$id]
        ])->firstOrFail();
    }


    public function getSpecificDriverCollectedCollectionBasedOnTeamIdForDriverForAdmin(array $request, array $originalRequest)
    {
//        dd($request);
        if(isset($request["driver_id"])) {

            $drivers = $this->driverCollection
                ->where([
                    ["driver_id", $request["driver_id"]],
                    ["team_id", $request["team_id"]]
                ]);
        }
        else{
            $drivers = $this->driverCollection
                ->where([
                    ["team_id", $request["team_id"]]
                ]);
        }
        return $drivers
            ->where("admin_id","=",0)
            ->join('driver_teams', 'driver_collections.team_id', '=', 'driver_teams.id')
            ->whereBetween("driver_collections.created_at",[$request["start_date"],$request["end_date"]])
            ->select("driver_collections.*","driver_teams.name")
            ->orderBy("leader_received_at","desc")->paginate($request["limit"])->withPath("/admin/team/cash/order/detail")->appends($originalRequest);

    }

    public function getSpecificDriverCollectedCollectionHistoryDetailBasedOnTeamIdForDriverForAdmin(array $request, $originalRequest)
    {
        if(isset($request["driver_id"])) {

            $drivers = $this->driverCollection
                ->where([
                    ["driver_id", $request["driver_id"]],
                    ["team_id", $request["team_id"]]
                ]);
        }
        else{
            $drivers = $this->driverCollection
                ->where([
                    ["team_id", $request["team_id"]]
                ]);
        }
        return $drivers->where("admin_id","!=",0)
            ->join('driver_teams', 'driver_collections.team_id', '=', 'driver_teams.id')
            ->whereBetween("driver_collections.created_at",[$request["start_date"],$request["end_date"]])
            ->select("driver_collections.*","driver_teams.name")
            ->orderBy("created_at","desc")->paginate($request["limit"])->withPath("/admin/team/collection/history/detail")->appends($originalRequest);
    }

    public function getSpecificTeamMemberOrderHistory(array $request, $originalRequest)
    {
        if(isset($request["driver_id"])) {

            $drivers = $this->driverCollection
                ->where([
                    ["driver_id", $request["driver_id"]],
                    ["team_id", $request["team_id"]]
                ]);
        }
        else{
            $drivers = $this->driverCollection
                ->where([
                    ["team_id", $request["team_id"]]
                ]);
        }
        return $drivers
            ->join('driver_teams', 'driver_collections.team_id', '=', 'driver_teams.id')
            ->whereBetween("driver_collections.created_at",[$request["start_date"],$request["end_date"]])
            ->select("driver_collections.*","driver_teams.name")
//
            ->orderBy("admin_received_at","asc")
            ->orderBy("leader_received_at","desc")
//            ->groupBy('admin_received_at')
            ->paginate($request["limit"])->withPath("/admin/team/member/history")->appends($originalRequest);
    }


}