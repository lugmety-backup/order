<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/22/2017
 * Time: 12:06 PM
 */

namespace App\Repo\Eloquent;


use App\Models\DeliveryStatusTranslation;
use App\Repo\DeliveryStatusTranslationInterface;

class DeliveryStatusTranslationRepo implements DeliveryStatusTranslationInterface
{
    protected $deliveryStatusTranslation;

    /**
     * DeliveryStatusTranslationRepo constructor.
     * @param $deliveryStatusTranslation
     */
    public function __construct(DeliveryStatusTranslation $deliveryStatusTranslation)
    {
        $this->deliveryStatusTranslation = $deliveryStatusTranslation;
    }

    public function getAllDeliveryStatusTranslation()
    {
        // TODO: Implement getAllDeliveryStatusTranslation() method.
    }

    public function getSpecificDeliveryStatusTranslation($id)
    {
        return $this->deliveryStatusTranslation->findOrFail($id);
    }

    public function createDeliveryStatusTranslation(array $request)
    {

        return $this->deliveryStatusTranslation->create($request);
    }

    public function getSpecificDeliveryStatusTranslationByCodeAndId($langCode,$deliveryStatusId){
        return $this->deliveryStatusTranslation->where([["lang_code",$langCode],["delivery_status_id",$deliveryStatusId]])->get();
    }

    public function updateDeliveryStatusTranslation($id, array $request)
    {
        // TODO: Implement updateDeliveryStatusTranslation() method.
    }

    public function deleteDeliveryStatusTranslation($id)
    {
        // TODO: Implement deleteDeliveryStatusTranslation() method.
    }

    public function deleteDeliveryStatusTranslationByDeliveryStatusId($deliveryId)
    {
        return $this->deliveryStatusTranslation->where("delivery_status_id",$deliveryId)->delete();
    }
}