<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/12/2017
 * Time: 6:49 AM
 */

namespace App\Repo\Eloquent;

use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Support\Facades\Input;
use App\Models\Orders;
use App\Repo\OrderInterface;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class OrderRepo implements OrderInterface
{
    protected $order;
    protected $slugify;

    /**
     * OrderRepo constructor.
     * @param $order
     */
    public function __construct(Orders $order, Slugify $slugify)
    {
        $this->order = $order;
        $this->slugify = $slugify;
    }


    /**
     * gets all orders
     * @return mixed
     */
    public function getAllOrders($orderStatus, $restaurantId, $countryId, $userId)
    {
        if (!$orderStatus && is_null($restaurantId)) $orders = $this->order->where("country_id", $countryId);
        elseif (is_null($restaurantId)) $orders = $this->order->where([["order_status", $orderStatus], ["country_id", $countryId]]);
        else   $orders = $this->order->where([["order_status", $orderStatus], ["restaurant_id", $restaurantId], ["country_id", $countryId]]);
        if (is_null($userId)) return $orders->orderBy("id", "desc")->get();
        else return $orders->where("customer_id", $userId)->orderBy("id", "desc")->get();
    }

    public function getAllOrdersPagination($orderStatus, $countryId, $userId,$limit,$orderBy, $filter)
    {
        $columnsList = Schema::getColumnListing("orders");
        $order = "id";
        foreach ($columnsList as $columnName){
            if(isset($orderBy["sort_column"])) {
                if ($columnName == $orderBy["sort_column"]) {
                    $order = $columnName;
                    break;
                }
            }
        }
        $orders = $this->order->where("country_id", $countryId);
        if(!is_null($filter['branch_id'])){

            if(is_array($filter['branch_id'])){
                $orders = $orders->whereIn("restaurant_id", $filter['branch_id']);
            }else
                $orders = $orders->where("restaurant_id", $filter['branch_id']);
        }
        if(!is_null($userId)) $orders = $orders->where("customer_id", $userId);
        if(!is_null($filter['admin_id'])) $orders = $orders->where("admin_id", $filter['admin_id']);
        if(!is_null($filter['delivery_type'])) $orders = $orders->where("delivery_type", $filter['delivery_type']);
        if(!is_null($filter['payment_method'])) $orders = $orders->where("payment_method", $filter['payment_method']);
        if(!is_null($filter['payment_status'])) $orders = $orders->where("payment_status", $filter['payment_status']);
        if(!is_null($filter['delivery_status'])) $orders = $orders->where("delivery_status", $filter['delivery_status']);
        if(!is_null($filter['order_status'])) $orders = $orders->where("order_status", $filter['order_status']);
        if(!is_null($filter['customer_name'])) $orders = $orders->where(function ($query) use ($filter){
            $query->where("customer_name", "like", "%" . $filter['customer_name'] . "%")
                ->orWhere("id", $filter['customer_name']);
        });
        if(!is_null($filter['device_type'])) $orders = $orders->where("device_type", $filter['device_type']);
        if(is_null($filter['start_date']) && is_null($filter['end_date'])){
            $firstDayOfMonth = Carbon::now()->startOfMonth()->toDateTimeString();
            $orders = $orders->where("created_at",">", $firstDayOfMonth);

        }else if(!is_null($filter['start_date']) && !is_null($filter['end_date'])) {
            $filter['start_date'] = Carbon::createFromFormat('Y-m-d', $filter['start_date'])->startOfDay()->toDateTimeString();
            $filter['end_date'] = Carbon::createFromFormat('Y-m-d', $filter['end_date'])->addDay()->startOfDay()->toDateTimeString();
            $firstDayOfMonth = Carbon::now()->startOfMonth();
            $orders = $orders->where("created_at", ">", $filter['start_date'])
                ->where("created_at", "<", $filter['end_date']);
        }elseif(!is_null($filter['start_date'])){
            $filter['start_date'] = Carbon::createFromFormat('Y-m-d', $filter['start_date'])->startOfDay()->toDateTimeString();
            $orders = $orders->where("created_at",">", $filter['start_date']);
        }elseif(!is_null($filter['end_date'])){
            $filter['end_date'] = Carbon::createFromFormat('Y-m-d', $filter['end_date'])->addDay()->startOfDay()->toDateTimeString();
            $orders = $orders->where("created_at","<", $filter['end_date']);
        }

        if(!is_null($filter["search"])) {
            $value ="%".strtolower($filter["search"])."%";
            $slugedValue = "%".$this->slugify->slugify($value)."%";
//            dd($columnsList);
            $orders = $orders->where(function ($query) use ($columnsList, $value, $slugedValue){
                foreach($columnsList as $key => $column)
                {
                    if ($column == "extra_info" ){
                        continue;
                    }
                    if ($column == "restaurant") {
                        $query->orWhere($column, "like",'%name";s:__:"'.$value.'address%');
                        continue;
                    }
                    if($key === 0){
                        $query->where($column, "like", $value);

                    }
                    else{
                        $query->orWhere($column, "like", $value)->orWhere($column, "like", $slugedValue);
                    }
                }

            });

        }
        return $orders->orderBy($order, $orderBy["sort_by"])->paginate($limit);
    }

    public function getAllOrdersBasedOnUserId($orderStatus, $restaurantId, $countryId, $user_id)
    {
        if (!$orderStatus && is_null($restaurantId)) return $this->order->where([["country_id", $countryId], ["customer_id", $user_id]])->orderBy("id", "desc")->get();
        elseif (is_null($restaurantId)) return $this->order->where([["order_status", $orderStatus], ["country_id", $countryId], ["customer_id", $user_id]])->orderBy("id", "desc")->get();
        else   return $this->order->where([["order_status", $orderStatus], ["restaurant_id", $restaurantId], ["country_id", $countryId], ["customer_id", $user_id]])->orderBy("id", "desc")->get();
    }

    public function getAllOrderList($min, $max)
    {
        return $this->order->where([["customer_id", ">=", $min], ["customer_id", "<=", $max]])->orderBy("customer_id", 'desc')->get();
    }

    public function getOrdersStatsOfCustomer($customer_id, $country_id)
    {

        if (is_null($country_id)) {
            $response = $this->order->selectRaw('country_id, sum(amount) as total_spent_amount')->where([["customer_id", $customer_id], ["payment_status", "completed"]])->groupBY('country_id')->groupBY('customer_id')->get();
            foreach ($response as $data) {
                $data['total_restaurants'] = count($this->order->distinct()->where([["customer_id", $customer_id], ["payment_status", "completed"], ["country_id", $data['country_id']]])->get(['restaurant_id']));
                $data['total_fulfilled_orders'] = count($this->order->where([["customer_id", $customer_id], ["payment_status", "completed"], ["country_id", $data['country_id']]])->get());

            }
            return $response;
        } else {
            $response = $this->order->selectRaw('country_id, sum(amount) as total_spent_amount')->where([["customer_id", $customer_id], ["payment_status", "completed"], ["country_id", $country_id]])->groupBY('country_id')->groupBY('customer_id')->get();
            foreach ($response as $data) {
                $data['total_restaurants'] = count($this->order->distinct()->where([["customer_id", $customer_id], ["payment_status", "completed"], ["country_id", $data['country_id']]])->get(['restaurant_id']));
                $data['total_fulfilled_orders'] = count($this->order->where([["customer_id", $customer_id], ["payment_status", "completed"], ["country_id", $data['country_id']]])->get());

            }
            return $response;
        }
    }

    private function _fixAmbiguousColumns($columns, $table, $alias, $columns_to_be_fixed = array("id", "city_id", "country_id", "district_id", "created_at", "distance", "updated_at", "restaurant_id"))
    {
        $new_column_names = [];
        //dd($columns);
        foreach ($columns as $col) {
            if (in_array($col, $columns_to_be_fixed)) {
                array_push($new_column_names, $table . '.' . $col . ' as ' . $alias . '_' . $col);
            } else {
                array_push($new_column_names, $col);
            }
        }

        return $new_column_names;
    }

    public function getAllRestaurantForDriver($driverLat, $driverLong, $driverCountryId, $distanceLimit, $limit)
    {
        /**
         * checks if driver_order table is empty or not
         */
        $driverOrder = DB::table('driver_order')->first();
        $circle_radius = 6371.393;
        $max_distance = $distanceLimit;
        // select column names from table using information table for all the tables
        // we will have a list of array of columns
        // remove id columns from both array.
        // append tablename infront of all coluumns for each columns for each table
        // combine array
        // do select
//        $columns = DB::getSchemaBuilder()->getColumnListing('orders');
        //removign id column
//        $new_columns = array_shift($columns);
        $page = Input::get('page', 1);
        $paginate = $limit;
        $offSet = ($page * $paginate) - $paginate;
        if ($driverOrder) {
            $data = DB::select(DB::raw('
                    SELECT SQL_CALC_FOUND_ROWS o.restaurant_id, COUNT(o.restaurant_id) as order_count FROM (SELECT orders.* , (' . $circle_radius . ' * acos(cos(radians(' . $driverLat . ')) * cos(radians(restaurant_latitude)) *
                    cos(radians(restaurant_longitude) - radians(' . $driverLong . ')) +
                    sin(radians(' . $driverLat . ')) * sin(radians(restaurant_latitude))))
                    AS driver_restaurant_distances
                    FROM orders 
                    LEFT JOIN driver_order ON orders.id = driver_order.order_id
                     WHERE (driver_order.order_id is NULL OR driver_order.status = "reassigned")
                     AND orders.order_status = "accepted"
                     AND orders.country_id = ' . $driverCountryId . '
                     AND orders.is_seen = true
                     AND orders.api_delivery_time <> ""
                      AND orders.delivery_time <> ""
                     AND orders.delivery_type != "pickup"
                     AND orders.delivery_status = "pending"
                     AND orders.payment_method != "cash-on-pickup") o
                     WHERE o.driver_restaurant_distances < ' . $max_distance . '
                     AND o.payment_status = "completed"
                     OR o.payment_method = "cash-on-delivery"
                     GROUP BY o.restaurant_id
                      ORDER BY driver_restaurant_distances
                      LIMIT ' . $offSet . ',' . $paginate . '
               '));
            $data["count"] = DB::select(DB::raw("SELECT FOUND_ROWS() AS count;"));
        } else {
            $data = DB::select(DB::raw('
                    SELECT SQL_CALC_FOUND_ROWS o.restaurant_id, COUNT(o.restaurant_id) as order_count FROM (SELECT orders.* , (' . $circle_radius . ' * acos(cos(radians(' . $driverLat . ')) * cos(radians(restaurant_latitude)) *
                    cos(radians(restaurant_longitude) - radians(' . $driverLong . ')) +
                    sin(radians(' . $driverLat . ')) * sin(radians(restaurant_latitude))))
                    AS driver_restaurant_distances
                    FROM orders 
                    WHERE orders.order_status = "accepted"
                     AND orders.country_id = ' . $driverCountryId . '
                     AND orders.is_seen = true
                     AND orders.api_delivery_time <> ""
                      AND orders.delivery_time <> ""
                      AND orders.delivery_type != "pickup"
                     AND orders.delivery_status = "pending"
                     AND orders.payment_method != "cash-on-pickup") o
                     WHERE o.driver_restaurant_distances < ' . $max_distance . '
                      AND o.payment_status = "completed"
                     OR o.payment_method = "cash-on-delivery"
                     GROUP BY o.restaurant_id
                      ORDER BY driver_restaurant_distances
                      LIMIT ' . $offSet . ',' . $paginate . '
                              
               '));
            $data["count"] = DB::select(DB::raw("SELECT FOUND_ROWS() AS count;"));
        }
        return $data;
//        $collection = new Collection($data);
//        $page = Input::get('page', 1);
//        $paginate = $limit;
//
//        $offSet = ($page * $paginate) - $paginate;
//        $itemsForCurrentPage = array_values(array_slice($data, $offSet, $paginate, true));
//        return $data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page);

    }

    public function getAllOrdersForDriver($restaurantId, $driverCountryId, $limit)
    {
        /**
         * checks if driver_order table is empty or not
         */
        $driverOrder = DB::table('driver_order')->first();
//        $circle_radius = 6371.393;
//        $max_distance = 10;
        // select column names from table using information table for all the tables
        // we will have a list of array of columns
        // remove id columns from both array.
        // append tablename infront of all coluumns for each columns for each table
        // combine array
        // do select
//        $columns = DB::getSchemaBuilder()->getColumnListing('orders');
        //removign id column
//        $new_columns = array_shift($columns);
        $page = Input::get('page', 1);
        $paginate = $limit;

        $offSet = ($page * $paginate) - $paginate;
        if ($driverOrder) {
            $data = DB::select(DB::raw('
                    SELECT SQL_CALC_FOUND_ROWS * FROM  (SELECT orders.* 
                    FROM orders 
                   LEFT JOIN driver_order ON orders.id = driver_order.order_id
                   WHERE (driver_order.order_id is NULL OR driver_order.status = "reassigned")
                     AND orders.order_status = "accepted"
                      AND orders.country_id = ' . $driverCountryId . '
                     AND orders.is_seen = true
                     AND orders.api_delivery_time <> ""
                      AND orders.delivery_time <> ""
                      AND orders.delivery_type != "pickup"
                      AND orders.delivery_status = "pending"
                     AND orders.restaurant_id = ' . $restaurantId . '
                     AND orders.payment_method != "cash-on-pickup") o
                     WHERE o.payment_status = "completed"
                     OR o.payment_method = "cash-on-delivery"
                      GROUP BY o.id
                     
                  
           Order BY o.delivery_time desc
                    
                                   
               '));
            $data["count"] = DB::select(DB::raw("SELECT FOUND_ROWS() AS total;"));


        } else {
            $data = DB::select(DB::raw('
                    SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT orders.* 
                    FROM orders 
                    WHERE orders.order_status = "accepted"
                     AND orders.country_id = ' . $driverCountryId . '
                     AND orders.is_seen = true
                     AND orders.api_delivery_time <> ""
                     AND orders.delivery_time <> ""
                      AND orders.delivery_type != "pickup"
                      AND orders.delivery_status = "pending"
                       AND orders.delivery_time <> ""
                     AND orders.restaurant_id = ' . $restaurantId . '
                     AND orders.payment_method != "cash-on-pickup") o
                     WHERE o.payment_status = "completed"
                     OR o.payment_method = "cash-on-delivery"
                        GROUP BY o.id
                      Order BY o.delivery_time desc
                    
                '));
            $data["count"] = DB::select(DB::raw("SELECT FOUND_ROWS() as total;"));
        }

        return $data;
//        $collection = new Collection($data);
//        $page = Input::get('page', 1);
//        $paginate = $limit;
//
//        $offSet = ($page * $paginate) - $paginate;
//        $itemsForCurrentPage = array_values(array_slice($data, $offSet, $paginate, true));
//        return $data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($data), $paginate, $page);

    }

    public function getAllReportOfRestaurant($restaurantId, $date)
    {
        $specificdate = Carbon::parse($date);

        return $this->order->where([["restaurant_id", $restaurantId], ["order_status", "accepted"]])->whereBetween("created_at", [
            $specificdate->startOfMonth()->toDateTimeString(),
            $specificdate->endOfMonth()->toDateTimeString()
        ])->orderBy("id", "desc")->get();
    }

    public function getAllReportOfRestaurantSales($restaurantId, $date)
    {
        $specificdate = Carbon::parse($date);

        return $this->order->where([["orders.restaurant_id", $restaurantId], ["order_status", "accepted"]])->whereBetween("orders.created_at", [
            $specificdate->startOfMonth()->toDateTimeString(),
            $specificdate->endOfMonth()->toDateTimeString()
        ])
            ->leftJoin('driver_order', 'orders.id', '=', 'driver_order.order_id')
            ->select('orders.id as id', 'order_date', 'orders.restaurant', 'customer_name', 'customer_note', 'delivery_type', 'payment_method', 'delivery_status', 'order_status', 'payment_status', 'device_type', 'currency', DB::raw('ROUND(amount - tax - delivery_fee,2) as sub_total'),'site_commission_amount','logistics_fee', 'delivery_fee', 'tax','amount' ,'driver_order.user_id as user_id',"site_tax_rate")
            ->orderBy("id", "desc")->get();
    }

    public function getAllReportOfRestaurantForNotification(array $request){
        return $this->order->where([["restaurant_id", $request["restaurant_id"]], ["order_status", "accepted"]])->whereBetween("created_at", [
            $request["start_date"],
            $request["end_date"]
        ])->orderBy("id", "desc")->get();
    }

    public function getOrdersByRestaurantId($restaurantId, $orderStatus, $countryId, $userId)
    {
        if ($orderStatus && !is_null($restaurantId)) $orders = $this->order->where([["restaurant_id", $restaurantId], ["order_status", $orderStatus], ["country_id", $countryId]]);
        elseif (!is_null($restaurantId)) $orders = $this->order->where([["restaurant_id", $restaurantId], ["country_id", $countryId]]);
        else $orders = $this->order->where([["order_status", $orderStatus], ["country_id", $countryId]]);
        if (is_null($userId)) return $orders->orderBy("id", "desc")->get();
        else return $orders->where("customer_id", $userId)->orderBy("id", "desc")->get();
    }

    public function getOrdersIdListByRestaurantIdForBranchOperator($branchId){
        return $orders = $this->order->where([
            ["restaurant_id", $branchId],
            ["order_status","pending"],
            ["pre_order",0],
//            ['is_seen',0],
            ["delivery_or_pickup_time",null]
        ])
            ->where(function ($query){
                $query->where([
                    ["payment_method" ,"creditcard"],
                    ["payment_status","completed"]
                ])->orWhere([
                    ["payment_method" ,"sadad"],
                    ["payment_status","completed"]
                ])
                    ->orWhere([
                        ["payment_method","!=" ,"creditcard"],
                        ["payment_method","!=","sadad"]
                    ]);
            })
            ->orderBy("id","asc")->pluck('id')->toArray();

    }

    public function getOrdersByRestaurantIdForBranchOperator($restaurantId, $orderStatus, $userId, $limit)
    {
        if ($orderStatus && !is_null($restaurantId)) $orders = $this->order->where([["restaurant_id", $restaurantId], ["order_status", $orderStatus]]);
        elseif (!is_null($restaurantId)) $orders = $this->order->where("restaurant_id", $restaurantId);
        else $orders = $this->order->where("order_status", $orderStatus);
        if (is_null($userId)) return $orders
            ->where(function ($query){
                $query->where([
                    ["payment_method" ,"creditcard"],
                    ["payment_status","completed"],
                    ["order_status","!=","pending"]
                ])->orWhere([
                    ["payment_method" ,"sadad"],
                    ["payment_status","completed"],
                    ["order_status","!=","pending"]
                ])
                    ->orWhere([
                        ["payment_method","!=" ,"creditcard"],
                        ["payment_method","!=","sadad"],
                        ["order_status","!=","pending"]
                    ]);
            })
            ->orderBy("id", "desc")->paginate($limit);
        else return $orders->where("customer_id", $userId)->orderBy("id", "desc")->paginate($limit);
    }


    public function getOrdersByRestaurantAndOrderIdAndValidate($restaurantId, $orderId)
    {
        return $this->order->where([
            ["restaurant_id", $restaurantId],
            ["id", $orderId],
            ["is_seen", true],
            ["order_status", "accepted"],
            ["delivery_type", "!=", "pickup"],
            ["delivery_status", "pending"],
            ["api_delivery_time", "!=", ""],
            ["payment_method", '!=', "cash-on-pickup"]
        ])->lockForUpdate()->first();
    }

    public function getOrdersByRestaurantAndOrderId($restaurantId, $orderId)
    {
        return $this->order->where([
            ["restaurant_id", $restaurantId],
            ["id", $orderId]
        ])->first();
    }

    public function getOrdersByOrderIdAndUserId($id, $userId)
    {
        return $this->order->where("customer_id", $userId
        )->findOrFail($id);
    }

    public function getOrderByOrderIdOnly($orderId){
        return $this->order->find($orderId);
    }

    public function getAllOrdersCustomer($limit, $userId, $countryId)
    {
        if (is_null($countryId)) {
            return $this->order->select("id", "order_date", "restaurant_id", "currency", "amount", "delivery_status", "delivery_type", "order_status", "payment_status", "restaurant","rejection_reason_id","user_type","delivery_fee")->where("customer_id", $userId)->orderBy("id", "desc")->paginate($limit);
        } else {
            return $this->order->select("id", "order_date", "restaurant_id", "currency", "amount", "delivery_status", "delivery_type", "order_status", "payment_status", "restaurant","rejection_reason_id","user_type","delivery_fee")->where([["customer_id", $userId], ["country_id", $countryId]])->orderBy("id", "desc")->paginate($limit);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getSpecificOrder($id)
    {
        return $this->order->findOrFail($id);
    }

    /**
     * @param array $request
     * @return mixed
     */
    public function createOrder(array $request)
    {
        return $this->order->create($request);
    }

    /**
     * @param $id
     * @param array $request
     * @return mixed
     */
    public function updateOrder($id, array $request)
    {
        $order = $this->order->findOrFail($id)->fill($request);
        return ($order->update()) ? $order : false;
    }

    public function updateOrderRestaurantAdmin($order, array $request)
    {
        $orderUpdate = $order->fill($request);
        return ($orderUpdate->update()) ? $orderUpdate : false;
    }

    /**
     * soft deletes passed collection order
     * @param $order
     * @return boolean
     */
    public function deleteOrderSoft($order)
    {
        return $order->delete();
    }

    /**
     * hard deletes passed collection order
     * @param $order
     * @return boolean
     */
    public function deleteOrderHard($order)
    {
        return $order->forceDelete();
    }

    public function getSpecificSoftDeletedOrNotSoftDeletedOrder($id)
    {
        return $this->order->withTrashed()->findOrFail($id);
    }


    public function getOrderDetailsForReorder($id, $userId)
    {
        $orderData = $this->order->findorfail($id);
        if ($orderData['customer_id'] == $userId && $orderData['order_status'] == 'accepted' && $orderData['payment_status'] == 'completed') {
            if ($orderData['delivery_type'] == "pickup") {
                $order = $orderData;
            } else {
                if ($orderData['delivery_status'] == "delivered") {
                    $order = $orderData;
                }
            }
            if (isset($order)) {
                $orderItems = unserialize($order['extra_info'])['items'];


                foreach ($orderItems as $index => $item) {
                    $items[$index]['food_id'] = $item['food_details']['item_id'];
                    $items[$index]['quantity'] = $item['food_details']['quantity'];
                    $items[$index]['size'] = $item['food_details']['size'];

                    foreach ($item['addons'] as $addon) {
                        $items[$index]['addon_categories'][] = [
                            'id' => $addon['category_id'],
                            'addons' => [$addon['addon_id']]
                        ];

                    }
                }
                $data['restaurant_id'] = $order['restaurant_id'];
                $data['items'] = $items;
                return $data;

            }
        }

        return false;
    }

    public function getOrderByListOfRestaurantIdAndCountry(array $restaurantId, $countryId, $orderStatus, $userId)
    {
        if ($orderStatus) $orders = $this->order->whereIn("restaurant_id", $restaurantId)->where([["country_id", $countryId], ["order_status", $orderStatus]]);
        else $orders = $this->order->whereIn("restaurant_id", $restaurantId)->where("country_id", $countryId);
        if (is_null($userId)) return $orders->orderBy("id", "desc")->get();
        else return $orders->where("customer_id", $userId)->orderBy("id", "desc")->get();
    }

    public function getOrdersForCsv($restaurantId = null, $startDate = null, $endDate = null)
    {
        if (!empty($restaurantId) && (empty($startDate) && empty($endDate))) {
            $orders = $this->order->select('id', 'customer_name', 'delivery_type', 'order_date', 'delivery_status','order_status','payment_status', 'site_commission_amount', 'amount', 'payment_method', 'restaurant','delivery_fee', 'site_tax_rate', 'customer_note')->where('restaurant_id', $restaurantId)->get();

        }
        elseif ((!empty($startDate) && !empty($endDate)) && !empty($restaurantId)) {
            $orders = $this->order->select('id', 'customer_name', 'delivery_type', 'order_date', 'delivery_status', 'order_status','payment_status','site_commission_amount', 'amount', 'payment_method', 'restaurant','delivery_fee', 'site_tax_rate', 'customer_note')->whereBetween('created_at', [$startDate, $endDate])->where('restaurant_id', $restaurantId)->get();

        } elseif ((!empty($startDate) && !empty($endDate)) && empty($restaurantId)) {
            $orders = $this->order->select('id', 'customer_name', 'delivery_type', 'order_date', 'delivery_status', 'order_status','payment_status','site_commission_amount', 'amount', 'payment_method', 'restaurant','delivery_fee', 'site_tax_rate', 'customer_note')->whereBetween('created_at', [$startDate, $endDate])->get();
        } else {
            return [];
        }
        foreach ($orders as $order) {
            $restaurant = unserialize($order->restaurant);

            $order->restaurant_name = $restaurant['translation']['en'];
            $order->delivery_status = str_replace("-", " ", ucfirst($order->delivery_status));
            $order->order_status = str_replace("-", " ", ucfirst($order->order_status));
            $order->payment_status = str_replace("-", " ", ucfirst($order->payment_status));
            $order->delivery_type = str_replace("-", " ", ucfirst($order->delivery_type));

            $order->payment_method = $order->payment_method == "creditcard" ? "Paytabs" : str_replace("-", " ", ucfirst($order->payment_method));
            $order->amount = number_format((float)round($order->amount,2), 2, '.', '');

            $order->sub_total = round(((100*$order->amount)/(100+$order->site_tax_rate))-$order->delivery_fee,2);

            $notes =  $order->orderComments()->select('comment','created_at')->get();
            if(!empty($notes))
            {

                $check = '';
                foreach ($notes as $note)
                {

                    $check .= $note['created_at']." ". $note['comment'].PHP_EOL;

                }
                $order->note = $check;
            }


        }

        return $orders->makeHidden('site_tax_rate')->toArray();
    }

    public function getNewUserFromPassedUsers(array $users)
    {
        if(count($this->order->whereIn('customer_id',[$users])->get()) == 0){ //checks if all of the records count is 0 i.e the passed user has orders or not , if not then assume them as a new user
            return $users;
        }
        return $this->order->whereIn('customer_id',[$users])->groupBy('customer_id')->havingRaw('COUNT(*) < 3')->get();
    }


}