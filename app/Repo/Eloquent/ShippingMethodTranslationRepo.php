<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/15/2017
 * Time: 1:22 PM
 */

namespace App\Repo\Eloquent;


use App\Models\ShippingMethodTranslation;
use App\Repo\ShippingMethodTranslationInterface;

/**
 * Class ShippingMethodTranslationRepo
 * @package App\Repo\Eloquent
 */
class ShippingMethodTranslationRepo implements ShippingMethodTranslationInterface
{
    /**
     * @var ShippingMethodTranslation
     */
    protected $ShippingMethodTranslation;

    /**
     * ShippingMethodTranslationRepo constructor.
     * @param $ShippingMethodTranslation
     */
    public function __construct(ShippingMethodTranslation $ShippingMethodTranslation)
    {
        $this->ShippingMethodTranslation = $ShippingMethodTranslation;
    }

    public function getAllShippingMethodTranslation($id)
    {
        return $this->ShippingMethodTranslation->where("shipping_method_id",$id)->get();
    }

    public function getAllShippingMethodTranslationByLang($id , $lang)
    {
        $ShippingMethodTranslation = $this->ShippingMethodTranslation->where([['lang_code', $lang],['shipping_method_id', $id]])->get();
        if ($ShippingMethodTranslation->count() == 0) {
            $ShippingMethodTranslation = $this->ShippingMethodTranslation->where([['lang_code', "en"],['shipping_method_id',$id]])->get();
        }
        return $ShippingMethodTranslation;
    }

    public function getSpecificShippingMethodTranslation($id)
    {
        return $this->ShippingMethodTranslation->findOrFail($id);
    }

    public function createShippingMethodTranslation(array $request)
    {
        return $this->ShippingMethodTranslation->create($request);
    }

    public function updateShippingMethodTranslation($id, array $request)
    {
        return $this->ShippingMethodTranslation->findOrFail($id)->update($request);
    }

    public function deleteShippingMethodTranslation($id)
    {
        return $this->ShippingMethodTranslation->findOrFail($id)->delete();
    }

}