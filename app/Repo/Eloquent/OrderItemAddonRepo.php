<?php

/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 6/11/2017
 * Time: 10:33 PM
 */
namespace App\Repo\Eloquent;

use App\Repo\OrderItemAddonInterface;
use App\Models\OrderItemAddon;


class OrderItemAddonRepo implements OrderItemAddonInterface
{

    private $addon;

    public function __construct(OrderItemAddon $addon)
    {
        $this->addon = $addon;
    }


    public function getAllAddon()
    {

        return $this->addon->get();
    }

    public function getSpecificAddon($id)
    {
        return $this->addon->findOrFail($id);
    }

    public function createAddon(array $attributes)
    {
        $con= $this->addon->create($attributes);
        return $con;
    }

    public function updateAddon($id, array $attributes)
    {

        $this->addon->findOrFail($id)->update($attributes);
        $con = $this->addon->findOrFail($id);
        return $con;
    }

    public function deleteAddon($id)
    {
        return $this->addon->findOrFail($id)->delete();
    }

}