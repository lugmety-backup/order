<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/22/2017
 * Time: 12:06 PM
 */

namespace App\Repo\Eloquent;


use App\Models\DeliveryStatus;
use App\Repo\DeliveryStatusInterface;

class DeliveryStatusRepo implements DeliveryStatusInterface
{
    protected $delivery;

    /**
     * DeliveryStatusRepo constructor.
     * @param $delivery
     */
    public function __construct(DeliveryStatus $delivery)
    {
        $this->delivery = $delivery;
    }

    public function getAllDeliveryStatus()
    {
        return $this->delivery->orderBy('id', "desc")->get();
    }

    public function getAllEnabledDeliveryStatus(){
        return $this->delivery->where("status",1)->orderBy('id', "desc")->get();
    }

    public function getSpecificDeliveryStatusEnabled($id)
    {
        return $this->delivery->where([
            ["id",$id],
            ["status",1]
        ])->firstOrFail();
    }

    public function getDeliveryStatusBasedOnSlug($slug){
        return $this->delivery->where("slug",$slug)->first();
    }

    public function getSpecificDeliveryStatus($id)
    {
        return $this->delivery->findOrFail($id);
    }

    public function setPendingSlugDefaultIfOtherSlugIsDefault()
    {

        $count = $this->delivery->where("is_default",1)->count();
        $checkPendingSlugIsPresent = $this->delivery->where('slug',"pending")->first();
        if($count == 0 && $checkPendingSlugIsPresent){
            $checkPendingSlugIsPresent->update(["is_default" => 1]);
        }
    }
    public function getDefaultDeliveryStatus(){
        $defaultDelivery = $this->delivery->where([
                ['is_default',"1"],
                ["status",1]
            ]
        )->first();
        if(!$defaultDelivery){
            return "pending";
        }
        else{
            return $defaultDelivery["slug"];
        }
    }

    public function setOtherDeliveryStatusToNonDefault(){

        $this->delivery->where("is_default",'=',1)->update(["is_default" => 0]);


    }

    public function createDeliveryStatus(array $request)
    {
        return $this->delivery->create($request);
    }

    public function updateDeliveryStatus($id, array $request)
    {
        return $this->delivery->findOrFail($id)->update($request);
    }

    public function deleteDeliveryStatus($delivery)
    {
        return $delivery->delete();

    }

    public function getActiveDeliveryStatus()
    {
        return $this->delivery->where('status', 1)->get();
    }

    public function getAllDeliveryStatusIncludingTranslation()
    {
        return $this->delivery->with('deliveryStatusTranslations')->get();
    }


}