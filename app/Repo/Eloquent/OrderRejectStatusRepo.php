<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 12:26 PM
 */

namespace App\Repo\Eloquent;


use App\Models\OrderRejectStatus;
use App\Repo\OrderRejectStatusInterface;

class OrderRejectStatusRepo implements OrderRejectStatusInterface
{
    protected $orderRejectStatus;

    /**
     * OrderRejectStatusRepo constructor.
     * @param $orderRejectStatus
     */
    public function __construct(OrderRejectStatus $orderRejectStatus)
    {
        $this->orderRejectStatus = $orderRejectStatus;
    }

    public function getAllOrderRejectStatus()
    {
        return $this->orderRejectStatus->orderBy('id', "desc")->get();
    }

    public function getAllEnabledOrderRejectStatus(){
        return $this->orderRejectStatus->where("status",1)->orderBy('id', "desc")->get();
    }

    public function getSpecificOrderRejectStatusEnabled($id)
    {
        return $this->orderRejectStatus->where([
            ["id",$id],
            ["status",1]
        ])->firstOrFail();
    }

    public function getOrderRejectStatusBasedOnSlug($slug){
        return $this->orderRejectStatus->where("slug",$slug)->first();
    }

    public function getSpecificOrderRejectStatus($id)
    {
        return $this->orderRejectStatus->findOrFail($id);
    }

   
    public function getDefaultOrderRejectStatus(){
        $defaultOrder = $this->orderRejectStatus->where([
                ['is_default',"1"],
                ["status",1]
            ]
        )->first();
    }


    public function setOtherOrderRejectStatusToNonDefault(){

        $this->orderRejectStatus->where("is_default",'=',1)->update(["is_default" => 0]);

    }

    public function setDefaultRejectStatus($id){

        $this->orderRejectStatus->findOrFail($id)->update(["is_default" => 1, "status" => 1]);

    }

    public function createOrderRejectStatus(array $request)
    {
        return $this->orderRejectStatus->create($request);
    }

    public function updateOrderRejectStatus($id, array $request)
    {
        return $this->orderRejectStatus->findOrFail($id)->update($request);
    }

    public function deleteOrderRejectStatus($orderRejectStatus)
    {
        return $orderRejectStatus->delete();

    }

    public function getActiveOrderRejectStatus()
    {
        return $this->orderRejectStatus->where('status', 1)->get();
    }



}