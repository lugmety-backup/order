<?php

/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 6/11/2017
 * Time: 10:33 PM
 */
namespace App\Repo\Eloquent;

use App\Repo\OrderItemInterface;
use App\Models\OrderItem;


class OrderItemRepo implements OrderItemInterface
{

    private $item;

    public function __construct(OrderItem $item)
    {
        $this->item = $item;
    }


    public function getAllItem()
    {

        return $this->item->get();
    }

    public function getSpecificItem($id)
    {
        return $this->item->findOrFail($id);
    }

    public function createItem(array $attributes)
    {
        $con = $this->item->create($attributes);
        return $con;
    }

    public function countOrderItem($orderId){
        return $this->item->where("order_id",$orderId)->sum('quantity');
    }

    public function updateItem($id, array $attributes)
    {

        $this->item->findOrFail($id)->update($attributes);
        $con = $this->item->findOrFail($id);
        return $con;
    }

    public function deleteItem($id)
    {
        return $this->item->findOrFail($id)->delete();
    }

    public function getItemByOrderId($orderId){
        return $this->item->where("order_id", $orderId)->get();
    }

}