<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 7/16/17
 * Time: 3:34 PM
 */

namespace App\Repo\Eloquent;

use App\Models\PaymentMethodTranslation;
use App\Repo\PaymentMethodTranslationInterface;

class PaymentMethodTranslationRepo implements PaymentMethodTranslationInterface
{
    private $translation;

    public  function __construct(PaymentMethodTranslation $translation)
    {
        $this->translation = $translation;
    }

    public function createPaymentMethodTranslation(array $attributes)
    {
        $con= $this->translation->create($attributes);
        return $con;
    }

    public function deletePaymentMethodTranslation($id)
    {

    }
}