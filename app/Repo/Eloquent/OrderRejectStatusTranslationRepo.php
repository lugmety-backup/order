<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 12:27 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\OrderRejectStatusTranslationInterface;
use App\Models\OrderRejectStatusTranslation;
class OrderRejectStatusTranslationRepo implements OrderRejectStatusTranslationInterface
{

    protected $orderRejectStatusTranslation;

    /**
     * OrderRejectStatusTranslationRepo constructor.
     * @param $orderRejectStatusTranslation
     */
    public function __construct(OrderRejectStatusTranslation $orderRejectStatusTranslation)
    {
        $this->orderRejectStatusTranslation = $orderRejectStatusTranslation;
    }

    public function getAllOrderRejectStatusTranslation()
    {
        // TODO: Implement getAllOrderRejectStatusTranslation() method.
    }

    public function getSpecificOrderRejectStatusTranslation($id)
    {
        return $this->orderRejectStatusTranslation->findOrFail($id);
    }

    public function createOrderRejectStatusTranslation(array $request)
    {
        return $this->orderRejectStatusTranslation->create($request);
    }

    public function getSpecificOrderRejectStatusTranslationByCodeAndId($langCode,$orderRejectStatusId){
        return $this->orderRejectStatusTranslation->where([["lang_code",$langCode],["order_status_id",$orderRejectStatusId]])->get();
    }

    public function updateOrderRejectStatusTranslation($id, array $request)
    {
        // TODO: Implement updateOrderRejectStatusTranslation() method.
    }

    public function deleteOrderRejectStatusTranslation($id)
    {
        // TODO: Implement deleteOrderRejectStatusTranslation() method.
    }

    public function deleteOrderRejectStatusTranslationByOrderRejectStatusId($id)
    {
       return $this->orderRejectStatusTranslation->where("order_reject_status_id",$id)->delete();
    }


}