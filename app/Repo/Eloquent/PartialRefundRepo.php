<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 1/28/2018
 * Time: 12:06 PM
 */

namespace App\Repo\Eloquent;


use App\Repo\PartialRefundInterface;
use App\Models\PartialRefund;
class PartialRefundRepo implements PartialRefundInterface
{
    
    protected $partialRefund;
    public function __construct(PartialRefund $partialRefund)
    {
        $this->partialRefund = $partialRefund;
    }

    public function getAllPartialRefund($orderId)
    {
        return $this->partialRefund->where("order_id",$orderId)->get();
    }

    public function getSpecificPartialRefund($id)
    {
        return $this->partialRefund->findOrFail($id);
    }

    public function createRefund(array $request)
    {
        return $this->partialRefund->create($request);
    }

    public function updateRefund($data, array $request)
    {
       return $data->update($request);
    }

    public function deleteRefund($id)
    {
       return $this->partialRefund->firstOrFail($id)->delete();
    }


}