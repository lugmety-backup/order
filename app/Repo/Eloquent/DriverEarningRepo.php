<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/20/2017
 * Time: 2:07 PM
 */

namespace App\Repo\Eloquent;


use App\Models\DriverEarning;
use App\Repo\DriverEarningInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DriverEarningRepo implements DriverEarningInterface
{
    protected $driverEarning;
    public function __construct(DriverEarning $ex)
    {
        $this->driverEarning = $ex;
    }

    public function getAllDriverEarning($userId, $limit,$countryId)
    {
        return $this->driverEarning->where([["user_id",$userId],["type" ,"normal"],["country_id",$countryId]])->paginate($limit);
    }

    public function viewSpecificEarning($id)
    {
        return $this->driverEarning->findOrFail($id);
    }

    public function createDriverEarning(array $request)
    {
        return $this->driverEarning->create($request);
    }

    public function updateDriverEarning($id, array $request)
    {
        return $this->driverEarning->findOrFail($id)->update($request);
    }

    public function getAllDriverEarningForAdmin($userId)
    {
        if(is_null($userId))  return $this->driverEarning->all();
        return $this->driverEarning->where("user_id",$userId)->get();
    }


    public function deleteEarning($data)
    {
        // TODO: Implement deleteEarning() method.
    }

    public function getAllDriverEarningNormal($user_id,$countryId,$date_for = null)
    {
        if (is_null($date_for)){
            return $this->driverEarning->where([
                ["user_id",$user_id],
                ["type","normal"],
                ["country_id",$countryId]
            ])->sum("amount");
        }
        else{
            $date = Carbon::parse($date_for)->tz('utc');
            $start_date = $date->startOfMonth()->startOfDay();
            $date = Carbon::parse($date_for)->tz('utc');
            $end_date = $date->endOfMonth()->endOfDay();

            return $this->driverEarning->where([
                ["user_id",$user_id],
                ["type","normal"],
                ["country_id",$countryId]
            ])
                ->whereBetween('date_for',[$start_date,$end_date])
                ->sum("amount");
        }

    }

    public function getSpecificDriverEarning($country_id,$driver_id, $date)
    {
        return $this->driverEarning
            ->where([
            ["user_id",$driver_id],
            ["type","normal"],
            ["country_id",$country_id]
        ])
            ->whereBetween('date_for',[$date['start_date'],$date['end_date']])
            ->get();
    }

    public function getSpecificDriverEarningDeduction($country_id,$driver_id, $date){
        return $this->driverEarning
            ->where([
                ["user_id",$driver_id],
                ["type","deduction"],
                ["country_id",$country_id]
            ])
            ->whereBetween('date_for',[$date['start_date'],$date['end_date']])
            ->get();
    }


    public function getAllDriverEarningDeducted($user_id,$countryId,$date_for = null)
    {
        if(is_null($date_for)){
            return $this->driverEarning->where([
                ["user_id",$user_id],
                ["type","deduction"],
                ["country_id",$countryId]
            ])->sum("amount");
        }
        else{
            $date = Carbon::parse($date_for)->tz('utc');
            $start_date = $date->startOfMonth()->startOfMonth();
            $date = Carbon::parse($date_for)->tz('utc');
            $end_date = $date->endOfMonth()->endOfDay();

            return $this->driverEarning->where([
                ["user_id",$user_id],
                ["type","deduction"],
                ["country_id",$countryId]
            ])
                ->whereBetween('date_for',[$start_date,$end_date])
                ->sum("amount");
        }

    }

    public function getEarningHistoryOfDriver($driver_id, $country_id, $date)
    {
        return $this->driverEarning->where([
            ["user_id",$driver_id],
            ["country_id",$country_id]
        ])
            ->whereBetween('date_for',[$date['start_date'],$date['end_date']])
            ->select('id','order_id','type','comment','amount')
            ->orderBy('created_at','desc')->get();
    }


    public function viewSpecificEarningByOrderIdAndUserId($orderId,$userId,$countryId){
        return $this->driverEarning->where([
            ["user_id",$userId],
            ["type","normal"],
            ["order_id",$orderId],
            ["country_id",$countryId]
        ])->sum("amount");
    }

    public function getTodayEarning(array $data){
        return $this->driverEarning->whereIn("order_id",$data)->pluck('order_id');
    }

    public function getCurrentEarning($user_id,$country_id){
        //todo optimize the code
      $data =  $this->driverEarning->select(DB::raw("select
  sum(case `type`
        when 'normal' then amount
        when 'deduction' then amount * -1 end) Total")->where([
          ["user_id",$user_id],
          ["country_id",$country_id]
      ]))->get();
      dd($data);
    }
}