<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/20/2017
 * Time: 5:28 PM
 */

namespace App\Repo\Eloquent;


use App\Models\DriverEarningWithdrawal;
use App\Repo\DriverEarningWithdrawnInterface;

class DriverEarningWithdrawnRepo implements DriverEarningWithdrawnInterface
{
    protected $withdrawnRequest;

    public function __construct(DriverEarningWithdrawal $earningWithdrawal)
    {
        $this->withdrawnRequest = $earningWithdrawal;
    }

    public function getAllWithdrawnRequest()
    {
        return $this->withdrawnRequest->all();
    }

    public function getAllDriverWithdrawnRequest($driverId)
    {
        return $this->withdrawnRequest->where("user_id",$driverId)->get();
    }

    public function getSpecificDriverWithdrawnRequest($id)
    {
        return $this->withdrawnRequest->findOrFail($id);
    }


    public function createWithdrawnRequest(array $request)
    {
        return $this->withdrawnRequest->create($request);
    }

    public function updateWithdrawnRequest($id, array $request)
    {
        return $this->withdrawnRequest->findOrFail($id)->update($request);
    }

    public function getAllDriverWithdrawnReqestByDriver($status, $limit,$driverId,$countryId)
    {
        if(!is_null($status)) return $this->withdrawnRequest->where([["status",$status],["user_id",$driverId],["country_id",$countryId]])->paginate($limit);
        else return $this->withdrawnRequest->where([["user_id",$driverId],["country_id",$countryId]])->paginate($limit);

    }

    public function getSpecificDriverWithdrawnReqestByDriver($id, $driverId,$countryId)
    {
        return $this->withdrawnRequest->where([
            ["id",$id],
            ["user_id",$driverId],
            ["country_id",$countryId]
        ])->firstOrFail();
    }

    public function getAllStatusdata($userId,$country_id){

        $paid = $this->withdrawnRequest->where([
            ["status","approved"],
            ["user_id",$userId],
            ["country_id",$country_id]
        ])->sum("amount");

        $pending = $this->withdrawnRequest->where([
            ["status","pending"],
            ["user_id",$userId],
            ["country_id",$country_id]
        ])->sum("amount");

        $rejected = $this->withdrawnRequest->where([
            ["status","rejected"],
            ["user_id",$userId],
            ["country_id",$country_id]
        ])->sum("amount");

        return [
            "paid" => $paid,
            "pending" => $pending,
            "rejected" => $rejected
        ];
    }


}