<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 12:26 PM
 */

namespace App\Repo\Eloquent;


use App\Models\OrderStatus;
use App\Repo\OrderStatusInterface;

class OrderStatusRepo implements OrderStatusInterface
{
    protected $orderStatus;

    /**
     * OrderStatusRepo constructor.
     * @param $orderStatus
     */
    public function __construct(OrderStatus $orderStatus)
    {
        $this->orderStatus = $orderStatus;
    }

    public function getAllOrderStatus()
    {
        return $this->orderStatus->orderBy('id', "desc")->get();
    }

    public function getAllEnabledOrderStatus(){
        return $this->orderStatus->where("status",1)->orderBy('id', "desc")->get();
    }

    public function getSpecificOrderStatusEnabled($id)
    {
        return $this->orderStatus->where([
            ["id",$id],
            ["status",1]
        ])->firstOrFail();
    }

    public function getOrderStatusBasedOnSlug($slug){
        return $this->orderStatus->where("slug",$slug)->first();
    }

    public function getSpecificOrderStatus($id)
    {
        return $this->orderStatus->findOrFail($id);
    }

    public function setPendingSlugDefaultIfOtherSlugIsDefault()
    {

        $count = $this->orderStatus->where("is_default",1)->count();
        $checkPendingSlugIsPresent = $this->orderStatus->where('slug',"pending")->first();
        if($count == 0 && $checkPendingSlugIsPresent){
            $checkPendingSlugIsPresent->update(["is_default" => 1]);
        }
    }
    public function getDefaultOrderStatus(){
        $defaultOrder = $this->orderStatus->where([
                ['is_default',"1"],
                ["status",1]
            ]
        )->first();
        if(!$defaultOrder){
            return "pending";
        }
        else{
            return $defaultOrder["slug"];
        }
    }

    public function setOtherOrderStatusToNonDefault(){

        $this->orderStatus->where("is_default",'=',1)->update(["is_default" => 0]);


    }

    public function createOrderStatus(array $request)
    {
        return $this->orderStatus->create($request);
    }

    public function updateOrderStatus($id, array $request)
    {
        return $this->orderStatus->findOrFail($id)->update($request);
    }

    public function deleteOrderStatus($orderStatus)
    {
        return $orderStatus->delete();

    }

    public function getActiveOrderStatus()
    {
        return $this->orderStatus->where('status', 1)->get();
    }

    public function getAllOrderStatusIncludingTranslation(){
        return $this->orderStatus->with('orderStatusTranslations')->get();
    }

}