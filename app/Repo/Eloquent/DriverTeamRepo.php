<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/2/2018
 * Time: 12:01 PM
 */

namespace App\Repo\Eloquent;


use App\Models\DriverTeam;
use App\Models\DriverTeamMember;
use App\Repo\DriverTeamInterface;
use Illuminate\Support\Facades\DB;

class DriverTeamRepo implements DriverTeamInterface
{
    protected $driverTeam;
    protected $driverMember;
    public function __construct(DriverTeam $driverTeam, DriverTeamMember $driverMember)
    {
        $this->driverTeam = $driverTeam;
        $this->driverMember = $driverMember;
    }

    public function getSpecificDataByColumn($columnName, $value)
    {
        return $this->driverTeam->where($columnName,$value)->first();
    }

    public function getAllRecordsByColumn($columnName, $value)
    {
        return $this->driverTeam->where($columnName,$value)->withTrashed()->get();
    }

    public function getSpecificDataByColumnActiveInActive($columnName, $value)
    {
        return $this->driverTeam->where($columnName,$value)->withTrashed()->first();
    }


    public function getCurrentActiveTeamId($captainId)
    {
        return $this->driverTeam->where("captain_id",$captainId)->first();
    }


    public function getAllRecordsActiveByColumn($columnName, $value)
    {
        return $this->driverTeam->where($columnName,$value)->get();
    }

    public function getTeamIdBasedOnForDriver($countryId , $driverId){
        return $this->driverTeam
            ->join("driver_team_members","driver_teams.id","=","driver_team_members.driver_team_id")
            ->whereNull(
                "driver_teams.deleted_at"
            )
            ->whereNull( "driver_team_members.deleted_at")
            ->where("country_id",$countryId)
            ->where("driver_team_members.driver_id",$driverId)
            ->select("driver_teams.*")->first();
    }

    public function getALlTeamsForAdmin(){
        return $this->driverTeam->all();
    }

    public function getAllTeamForAdmin(array $request){

            $driverTeams = $this->driverTeam
//                ->join("driver_team_members","driver_teams.id","=","driver_team_members.driver_team_id")
//                ->whereNull(
//                    "driver_teams.deleted_at"
//                )
//                ->whereNull( "driver_team_members.deleted_at")
//                ->leftJoin("driver_collections","driver_teams.id","=","driver_collections.team_id")
//                ->where("driver_collections.admin_id",0)
                ->where("driver_teams.country_id",$request["country_id"])
                ->where("driver_teams.city_id",$request["city_id"])
                ->leftJoin("driver_collections","driver_teams.id","=","driver_collections.team_id")
                ->select("driver_teams.*" ,
                    DB::raw('(SELECT COUNT(id) FROM driver_team_members dt WHERE dt.driver_team_id=driver_teams.id) as total_members,(SELECT CASE WHEN sum(amount) IS NULL THEN 0 ELSE sum(amount) END  FROM driver_collections dc where dc.team_id=driver_teams.id AND dc.admin_id =0 ) as collectable_amount  '))
                ->groupBy("driver_teams.id")->orderBy("driver_teams.id","desc")->withTrashed()->paginate($request["limit"]);
                return $driverTeams->withPath('/admin/team')->appends($request);
    }

    public function getAllTeamCollectionHistoryForAdmin(array $request,array $originalRequest){

        $driverTeams = $this->driverTeam
//                ->join("driver_team_members","driver_teams.id","=","driver_team_members.driver_team_id")
//                ->whereNull(
//                    "driver_teams.deleted_at"
//                )
//                ->whereNull( "driver_team_members.deleted_at")
//                ->leftJoin("driver_collections","driver_teams.id","=","driver_collections.team_id")
//                ->where("driver_collections.admin_id",0)
            ->where("driver_teams.country_id",$request["country_id"])
            ->where("driver_teams.city_id",$request["city_id"])
            ->leftJoin("driver_collections","driver_teams.id","=","driver_collections.team_id")
            ->select("driver_teams.*" ,
                DB::raw('(SELECT COUNT(id) FROM driver_team_members dt WHERE dt.driver_team_id=driver_teams.id) as total_members,(SELECT CASE WHEN sum(amount) IS NULL THEN 0 ELSE sum(amount) END  FROM driver_collections dc where dc.team_id=driver_teams.id AND dc.admin_id !=0 ) as collected_amount ,(SELECT count(order_id)  FROM driver_collections dc where dc.team_id=driver_teams.id AND dc.admin_id !=0 ) as total_collected_orders  '))
            ->groupBy("driver_teams.id")->orderBy("driver_teams.id","desc")->withTrashed()->paginate($request["limit"]);
        return $driverTeams->withPath('/admin/teams/collection/history')->appends($originalRequest);
    }

    public function getAllTeamForCashOrdersAdmin(array $request,array $originalRequest){
        $startDate = $request["start_date"];
        $endDate = $request["end_date"];
        $driverTeams["records"] = $this->driverTeam
//                ->join("driver_team_members","driver_teams.id","=","driver_team_members.driver_team_id")
//                ->whereNull(
//                    "driver_teams.deleted_at"
//                )
//                ->whereNull( "driver_team_members.deleted_at")
                ->leftJoin("driver_collections","driver_teams.id","=","driver_collections.team_id")
//                ->where("driver_collections.admin_id",0)
            ->where("driver_teams.country_id",$request["country_id"])
            ->where("driver_teams.city_id",$request["city_id"])
            ->whereBetween("driver_collections.created_at",[$startDate,$endDate])
            ->select("driver_teams.*" ,
                DB::raw("(SELECT CASE WHEN sum(amount) IS NULL THEN 0 ELSE sum(amount) END  FROM driver_collections dc where dc.team_id=driver_teams.id AND (dc.leader_received_at is null OR dc.admin_id = 0 ) AND  (dc.created_at BETWEEN '". $startDate . "' AND '". $endDate . "')  ) as collectable_amount_from_captain,  (SELECT  COUNT(order_id)  FROM driver_collections dc where dc.team_id=driver_teams.id AND dc.admin_id =0 AND  (dc.created_at BETWEEN '". $startDate . "' AND '". $endDate . "') ) as receivable_order,
                 (SELECT CASE WHEN sum(amount) IS NULL THEN 0 ELSE sum(amount) END  FROM driver_collections dc where dc.team_id=driver_teams.id AND dc.leader_received_at is not null AND dc.admin_id = 0 AND (dc.created_at BETWEEN '". $startDate . "' AND '". $endDate . "')  ) as collected_amount_from_driver,(SELECT  COUNT(order_id)  FROM driver_collections dc where dc.team_id=driver_teams.id AND dc.leader_received_at is not null AND dc.admin_id =0 AND  (dc.created_at BETWEEN '". $startDate . "' AND '". $endDate . "') ) as collecteable_order_by_admin
                 " ))
            ->groupBy("driver_teams.id")->orderBy("driver_teams.id","desc")->withTrashed()->paginate($request["limit"])->withPath('/admin/team')->appends($originalRequest);


         $summary= DB::select("
        select 
        sum(CASE WHEN amount IS NULL THEN 0 ELSE amount END )  as total_receivable_amount ,
        COUNT(order_id) as total_receivable_order  
        FROM driver_collections as dc 
        JOIN driver_teams as dt
        ON dt.id =  dc.team_id AND dc.admin_id = 0
        WHERE (dc.created_at BETWEEN '". $startDate . "' AND '". $endDate . "')
        AND dc.city_id = '". $request["city_id"] . "'
        AND  dc.country_id = '". $request["country_id"] . "'
 ");
        $total_collected_amount= DB::select("
        select
        sum(CASE WHEN amount IS NULL THEN 0 ELSE amount END )  as total_collected_amount
        FROM driver_collections as dc
        JOIN driver_teams as dt
        ON dt.id =  dc.team_id AND dc.admin_id != 0
        WHERE (dc.created_at BETWEEN '". $startDate . "' AND '". $endDate . "')
        AND dc.city_id = '". $request["city_id"] . "'
        AND  dc.country_id = '". $request["country_id"] . "'
 ");
        $summary = (array) $summary;
        if(!empty($summary[0]->total_receivable_order)){
            $driverTeams["summary"]["total_receivable_order"] = $summary[0]->total_receivable_order;
        }
        else{
            $driverTeams["summary"]["total_receivable_order"] = 0;
        }

        if(!empty($summary[0]->total_receivable_amount)){
            $driverTeams["summary"]["total_receivable_amount"] = $summary[0]->total_receivable_amount;
        }
        else{
            $driverTeams["summary"]["total_receivable_amount"] = 0;
        }

        if(!empty($total_collected_amount[0]->total_collected_amount)){
            $driverTeams["summary"]["total_collected_amount"] = $total_collected_amount[0]->total_collected_amount;
        }
        else{
            $driverTeams["summary"]["total_collected_amount"] = 0;
        }

        return $driverTeams;
    }

    public function getIndividualTeamDashboardStats($id){

       return  $driverTeams = $this->driverTeam
         ->where("id",$id)
//           ->leftJoin("driver_collections","driver_teams.id","=","driver_collections.team_id")
           ->select("driver_teams.*" ,
               DB::raw('
               (SELECT COUNT(id) FROM driver_team_members dt WHERE dt.driver_team_id=driver_teams.id and  dt.deleted_at IS NULL ) as total_active_members,
                (SELECT COUNT(order_id) FROM driver_collections dc WHERE dc.team_id=driver_teams.id) as total_orders,
                (SELECT CASE WHEN sum(amount) IS NULL THEN 0 ELSE sum(amount) END  FROM driver_collections dc where dc.team_id=driver_teams.id AND dc.admin_id !=0) as lifetime_collection,
               (SELECT CASE WHEN sum(amount) IS NULL THEN 0 ELSE sum(amount) END  FROM driver_collections dc where dc.team_id=driver_teams.id AND dc.admin_id =0 ) as collectable_amount  '))
           ->withTrashed()->first();
    }

    public function createNewRecord(array $request)
    {
        $teamInfo = $this->getCurrentActiveTeamId($request["captain_id"]);
        if($teamInfo){
            $teamInfo->driverTeamMembers()->delete();
            $teamInfo->delete();
        }
        $getCreatedTeamId = $this->driverTeam->create($request)->id;
        foreach ($request["drivers_id"] as $driverId){
            $this->driverMember->create([
                "driver_id" => $driverId,
                "driver_team_id" => $getCreatedTeamId
            ]);
        }
        return true;


    }

    public function updateTeamRecord($id, array $request)
    {
        $getTeam = $this->driverTeam->withTrashed()->findOrFail($id);
        $getTeam->update($request);
        return true;
    }


    public function updateTeamMember($id, array $request){
//            $getTeam = $this->driverTeam->findOrFail($id);
//            $getTeam->update($request);
//            if($request["type"] == "add"){
        foreach ($request["drivers_id"] as $driverId){
            $this->driverMember->create([
                "driver_id" => $driverId,
                "driver_team_id" => $id
            ]);
        }
        return true;
    }

    public function checkDriverIsAssignedToOtherTeam($captainId,array $driverId){
        return $this->driverTeam
            ->join("driver_team_members","driver_teams.id","=","driver_team_members.driver_team_id")
            ->where("driver_teams.captain_id","!=",$captainId)
            ->whereIn("driver_team_members.driver_id",$driverId)
            ->whereNull("driver_teams.deleted_at")
            ->whereNull("driver_team_members.deleted_at")->first();
    }


    public function getTeamAndTeamMembersByCaptainId($captainId)
    {
        return $this->driverTeam->where("captain_id",$captainId)->join("driver_team_members","driver_teams.id","=","driver_team_members.driver_team_id")->get();
    }

    public function softDeleteById($id)
    {
        $teamId = $this->driverTeam->findOrFail($id);
        $teamId->driverTeamMembers()->delete();
        return $teamId->delete();
    }

    public function softDeleteByCaptainId($id)
    {
        $teamId = $this->driverTeam->where("captain_id",$id)->firstOrFail();
        $teamId->driverTeamMembers()->delete();
        return $teamId->delete();
    }

}