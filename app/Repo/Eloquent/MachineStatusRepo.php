<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 12/13/2017
 * Time: 11:55 AM
 */

namespace App\Repo\Eloquent;

use App\Models\MachineStatus;
use App\Repo\MachineStatusInterface;

class MachineStatusRepo implements MachineStatusInterface
{
    protected $machineStatus;
    public function __construct(MachineStatus $machineStatus)
    {
        $this->machineStatus = $machineStatus;
    }

    public function getSpecificMachineStatus($restaurantId)
    {
       return $this->machineStatus->where("restaurant_id",$restaurantId)->first();
    }

    public function createMachineStatus(array $request)
    {
        return $this->machineStatus->create($request);
    }

    public function updateMachineStatus($specificData, array $request)
    {
       return $specificData->update($request);
    }

    public function getAllMachineStatus(array $restaurantIds)
    {
        return $this->machineStatus->whereIn("restaurant_id" ,$restaurantIds)->get();
    }


}