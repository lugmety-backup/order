<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/15/2017
 * Time: 1:20 PM
 */

namespace App\Repo\Eloquent;


use App\Models\PaymentStatus;
use App\Repo\PaymentStatusInterface;

/**
 * Class PaymentStatusRepo
 * @package App\Repo\Eloquent
 */
class PaymentStatusRepo implements PaymentStatusInterface
{

    /**
     * @var PaymentStatus
     */
    protected $paymentStatus;

    /**
     * PaymentStatusRepo constructor.
     * @param $paymentStatus
     */
    public function __construct(PaymentStatus $paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;
    }

    public function getAllPaymentStatus()
    {
        return $this->paymentStatus->orderby('id','desc')->get();
    }

    public function getPaymentStatusBasedOnSlug($slug){
        return $this->paymentStatus->where("slug",$slug)->first();
    }

    public function getAllEnabledPaymentStatus(){
        return $this->paymentStatus->where("status",1)->get();
    }

    public function getSpecificPaymentStatusEnabled($id)
    {
        return $this->paymentStatus->where([
            ["id",$id],
            ["status",1]
        ])->firstOrFail();
    }

    public function getSpecificPaymentStatus($id)
    {
        return $this->paymentStatus->findOrFail($id);
    }

    public function getDefaultPayment(){
        $defaultPayment = $this->paymentStatus->where(
            [
                ['is_default',1],
                ["status",1]
            ]
        )->first();
        if(!$defaultPayment){
            return "unpaid";
        }
        else{
            return $defaultPayment["slug"];
        }
    }

    public function setUnpaidSlugDefaultIfOtherSlugIsDefault()
    {
        $count = $this->paymentStatus->where("is_default",1)->count();
        $checkUnpaidSlugIsPresent = $this->paymentStatus->where('slug',"unpaid")->first();
        if($count == 0 &&  $checkUnpaidSlugIsPresent){
            $checkUnpaidSlugIsPresent->update(["is_default" => 1]);
        }
    }

    public function setOtherPaymentStatusToNonDefault(){
        $this->paymentStatus->where("is_default",'=',1)->update(["is_default" => 0]);
    }


    public function createPaymentStatus(array $request)
    {
        return $this->paymentStatus->create($request);
    }

    public function updatePaymentStatus($id, array $request)
    {
        return $this->paymentStatus->findOrFail($id)->update($request);
    }

    public function deletePaymentStatus($paymentMethod)
    {
        return $paymentMethod->delete();
    }

    public function getAllPaymentStatusIncludingTranslation()
    {
       return $this->paymentStatus->with('paymentStatusTranslations')->get();
    }


}