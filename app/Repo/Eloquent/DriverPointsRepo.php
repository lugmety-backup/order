<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 6/11/2017
 * Time: 3:19 PM
 */

namespace App\Repo\Eloquent;

use App\Models\DriverPoint;
use App\Repo\DriverPointsInterface;
use Illuminate\Support\Facades\DB;

class DriverPointsRepo implements DriverPointsInterface
{
    protected $driverPoints;
    /**
     * DriverPointsRepo constructor.
     */
    public function __construct(DriverPoint $driverPoints)
    {
        $this->driverPoints = $driverPoints;
    }

    public function getAllDriverPoints()
    {
        return $this->driverPoints->all();
    }

    public function getAllDriverPointsByUserId()
    {
        $result = DB::table('driver_points')
            ->select('user_id', DB::raw('SUM(point) as total_points'))
            ->groupBy('user_id')
            ->get();
        return $result;
    }

    public function getSpecificDriverPoints($id)
    {
        return $this->driverPoints->findOrFail($id);
    }

    public function createDriverPoints(array $attributes)
    {
        return $this->driverPoints->create($attributes);
    }

    public function updateDriverPoints($id, array $attributes)
    {
        return $this->driverPoints->findOrFail($id)->update($attributes);
    }

    public function deleteDriverPoints($id)
    {
        $driverPoints = $this->driverPoints->findOrFail($id);
        return $driverPoints->delete();
    }
}