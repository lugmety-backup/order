<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 11:35 AM
 */

namespace App\Repo\Eloquent;


use App\Models\ExperiencePointsRules;
use App\Repo\ExperiencePointRulesInterface;

class ExperiencePointRulesRepo implements ExperiencePointRulesInterface
{
    protected $experiencePointsRules;

    public function __construct(ExperiencePointsRules $experiencePointsRules)
    {
        $this->experiencePointsRules = $experiencePointsRules;
    }

    public function getAllRules($countryId)
    {
        return $this->experiencePointsRules->where("country_id",$countryId)->get();
    }

    public function getSpecialRule($id)
    {
       return $this->experiencePointsRules->findOrFail($id);
    }

    public function createRule(array $request)
    {
        return $this->experiencePointsRules->create($request);
    }

    public function updateRule($request,array $data)
    {
        return $request->update($data);
    }

    public  function unsetDefault(array $request){
        return $this->experiencePointsRules->where(
            "country_id",$request["country_id"]
        )->update(["is_default"=> 0]);
    }
    public function updateDefault(array $request,$id){
        return $this->experiencePointsRules->where([
            ["country_id",$request["country_id"]],
            ["id","!=",$id]
        ])->update(["is_default" =>  0]);
    }


    public function deleteRule($id)
    {
       return $this->experiencePointsRules->findOrFail($id)->delete();
    }

}