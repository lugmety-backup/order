<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 1:52 PM
 */

namespace App\Repo\Eloquent;


use App\Models\EarningRules;
use App\Repo\EarningRuleInterface;

class DriverEarningRuleRepo implements EarningRuleInterface
{
    protected $earningsRules;

    public function __construct(EarningRules $earningsRules)
    {
        $this->earningsRules = $earningsRules;
    }

    public function getAllRules($countryId)
    {
        return $this->earningsRules->where("country_id",$countryId)->get();
    }

    public function getSpecialRule($id)
    {
        return $this->earningsRules->findOrFail($id);
    }

    public function createRule(array $request)
    {
        return $this->earningsRules->create($request);
    }

    public function updateRule($request,array $data)
    {
        return $request->update($data);
    }

    public  function unsetDefault(array $request){
        return $this->earningsRules->where(
            "country_id",$request["country_id"]
        )->update(["is_default"=> 0]);
    }
    public function updateDefault(array $request,$id){
         $this->earningsRules->where([
            ["country_id",$request["country_id"]],
            ["id","!=",$id]
        ])->update(["is_default" =>  0]);

    }

     public function deleteRule($id)
    {
        return $this->earningsRules->findOrFail($id)->delete();
    }


}