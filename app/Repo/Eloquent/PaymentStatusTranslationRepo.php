<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/15/2017
 * Time: 1:22 PM
 */

namespace App\Repo\Eloquent;


use App\Models\PaymentStatusTranslation;
use App\Repo\PaymentStatusTranslationInterface;

/**
 * Class PaymentStatusTranslationRepo
 * @package App\Repo\Eloquent
 */
class PaymentStatusTranslationRepo implements PaymentStatusTranslationInterface
{
    /**
     * @var PaymentStatusTranslation
     */
    protected $paymentStatusTranslation;

    /**
     * PaymentStatusTranslationRepo constructor.
     * @param $paymentStatusTranslation
     */
    public function __construct(PaymentStatusTranslation $paymentStatusTranslation)
    {
        $this->paymentStatusTranslation = $paymentStatusTranslation;
    }

    public function getAllPaymentStatusTranslation()
    {
        // TODO: Implement getAllPaymentStatusTranslation() method.
    }

    public function getSpecificPaymentStatusTranslation($id)
    {
        return $this->paymentStatusTranslation->findOrFail($id);
    }

    public function getSpecificPaymentStatusTranslationByCodeAndId($langCode, $paymentMethodId)
    {
        return $this->paymentStatusTranslation->where([["lang_code",$langCode],["payment_status_id",$paymentMethodId]])->get();
    }

    public function createPaymentStatusTranslation(array $request)
    {
        return $this->paymentStatusTranslation->create($request);
    }

    public function updatePaymentStatusTranslation($id, array $request)
    {
        // TODO: Implement updatePaymentStatusTranslation() method.
    }

    public function deletePaymentStatusTranslation($id)
    {
        // TODO: Implement deletePaymentStatusTranslation() method.
    }

    public function deletePaymentStatusTranslationByPaymentStatusId($paymentId)
    {
        return $this->paymentStatusTranslation->where("payment_status_id",$paymentId)->delete();
    }


}