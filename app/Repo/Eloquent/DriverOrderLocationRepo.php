<?php
/**
 * Created by PhpStorm.
 * User: Prashant
 * Date: 6/11/2017
 * Time: 3:19 PM
 */

namespace App\Repo\Eloquent;

use App\Models\DriverOrderLocation;
use App\Repo\DriverOrderLocationInterface;
use Illuminate\Support\Facades\DB;

class DriverOrderLocationRepo implements DriverOrderLocationInterface
{
    protected $driverOrderLocation;
    /**
     * DriverOrderLocationRepo constructor.
     */
    public function __construct(DriverOrderLocation $driverOrderLocation)
    {
        $this->driverOrderLocation = $driverOrderLocation;
    }

    public function getAllDriverOrderLocation()
    {
        return $this->driverOrderLocation->all();
    }

    public function getDriverOrderLocationByDriverOrderId($driverOrderId)
    {
        $result = $this->driverOrderLocation->where('driver_order_id',$driverOrderId)->get();
        return $result;
    }

    public function getSpecificDriverOrderLocation($id)
    {
        return $this->driverOrderLocation->findOrFail($id);
    }

    public function createDriverOrderLocation(array $attributes)
    {
        return $this->driverOrderLocation->create($attributes);
    }

    public function updateDriverOrderLocation($id, array $attributes)
    {
        return $this->driverOrderLocation->findOrFail($id)->update($attributes);
    }

    public function deleteDriverOrderLocation($id)
    {
        $driverOrderLocation = $this->driverOrderLocation->findOrFail($id);
        return $driverOrderLocation->delete();
    }
}