<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 3/11/2018
 * Time: 12:00 PM
 */

namespace App\Repo\Eloquent;

use App\Models\CronInvoice;
use App\Repo\CronInvoiceInterface;

class CronInvoiceRepo implements CronInvoiceInterface
{
    protected  $cronInvoice;

    public function __construct(CronInvoice $cronInvoice)
    {
        $this->cronInvoice = $cronInvoice;
    }

    public function createInvoice(array $data)
    {
        $checkRecordExistForCountryId = $this->getInvoiceInfo($data["country_id"]);
        if($checkRecordExistForCountryId){
            if (count($this->checkMonthIsChanged($data["country_id"],$data["last_date"])) ==1){
                return $this->updateInvoiceInfo($checkRecordExistForCountryId,$data);

            }
            else{
                $data["is_send_invoice"] = 0;
                return $this->updateInvoiceInfo($checkRecordExistForCountryId,$data);
            }

        }

        else{
            return $this->cronInvoice->create($data);
        }
    }

    public function getInvoiceInfo($countryId)
    {
        return $this->cronInvoice->where("country_id",$countryId)->first();
    }

    public function updateInvoiceInfo($data, array $request,$storeDate = false)
    {
        if(!$storeDate && isset($request["last_date"])){
            unset($request["last_date"]);
        }
        return $data->update($request);
    }

    public function checkMonthIsChanged($countryId, $date)
    {
        return $this->cronInvoice->where([
            ["country_id", $countryId],
            ["last_date","<",$date]
        ])->first();
    }


}