<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 7:17 AM
 */

namespace App\Repo\Eloquent;

use App\Repo\OrderCommentInterface;
use App\Repo\OrderInterface;
use App\Models\OrderComment;
class OrderCommentRepo implements OrderCommentInterface
{
    protected $orderComment;

    /**
     * OrderCommentRepo constructor.
     * @param $orderComment
     */
    public function __construct(OrderComment $orderComment)
    {
        $this->orderComment = $orderComment;
    }

    public function getAllCommentsByOrderId($orderId,$limit){

        return $this->orderComment->where("order_id",$orderId)->latest()->paginate($limit);
    }

    public function getSpecificComment($id)
    {
        return $this->orderComment->findOrFail($id);
    }

    public function createComment(array $request)
    {
        return $this->orderComment->create($request);
    }

    public function updateComment($id, array $request)
    {
        return $this->orderComment->findOrFail($id)->update($request);
    }

    public function deleteComment($comment)
    {
        return $comment->delete();
    }

}