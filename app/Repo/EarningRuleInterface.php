<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 11:31 AM
 */

namespace App\Repo;


interface EarningRuleInterface
{
    public function getAllRules($countryId);

    public function getSpecialRule($id);

    public function createRule(array $request);

    public function updateRule($request , array $data);

    public function deleteRule($id);
}