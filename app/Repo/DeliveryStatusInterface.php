<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/22/2017
 * Time: 12:05 PM
 */

namespace App\Repo;


interface DeliveryStatusInterface
{
    public function getAllDeliveryStatus();

    public function getAllEnabledDeliveryStatus();

    public function getSpecificDeliveryStatusEnabled($id);

    public function getSpecificDeliveryStatus($id);

    public function getDeliveryStatusBasedOnSlug($slug);

    public function createDeliveryStatus(array $request);

    public function updateDeliveryStatus($id, array $request);

    public function deleteDeliveryStatus($orderStatus);

    public function getActiveDeliveryStatus();

    public function getAllDeliveryStatusIncludingTranslation();
}