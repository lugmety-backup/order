<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 12:21 PM
 */

namespace App\Repo;


interface OrderRejectStatusTranslationInterface
{
    public function getAllOrderRejectStatusTranslation();

    public function getSpecificOrderRejectStatusTranslation($id);

    public function getSpecificOrderRejectStatusTranslationByCodeAndId($langCode,$orderStatusId);

    public function createOrderRejectStatusTranslation(array $request);

    public function updateOrderRejectStatusTranslation($id, array $request);

    public function deleteOrderRejectStatusTranslation($id);

    public function deleteOrderRejectStatusTranslationByOrderRejectStatusId($id);
}