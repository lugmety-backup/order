<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/22/2017
 * Time: 12:06 PM
 */

namespace App\Repo;


interface DeliveryStatusTranslationInterface
{
    public function getAllDeliveryStatusTranslation();

    public function getSpecificDeliveryStatusTranslation($id);

    public function getSpecificDeliveryStatusTranslationByCodeAndId($langCode,$deliveryStatusId);

    public function createDeliveryStatusTranslation(array $request);

    public function updateDeliveryStatusTranslation($id, array $request);

    public function deleteDeliveryStatusTranslation($id);

    public function deleteDeliveryStatusTranslationByDeliveryStatusId($deliveryId);
}