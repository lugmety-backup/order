<?php

namespace App\Providers;

use App\Models\CronInvoice;
use App\Models\DeliveryStatus;
use App\Models\OrderRejectStatusTranslation;
use App\Models\Orders;
use App\Models\OrderStatus;
use App\Models\PaymentStatus;
use App\Repo\CronInvoiceInterface;
use App\Repo\DriverCollectionInterface;
use App\Repo\DriverTeamInterface;
use App\Repo\DriverTeamMemberInterface;
use App\Repo\Eloquent\CronInvoiceRepo;
use App\Repo\DeliveryStatusInterface;
use App\Repo\DeliveryStatusTranslationInterface;
use App\Repo\DriverEarningInterface;
use App\Repo\DriverEarningWithdrawnInterface;
use App\Repo\DriverExperiencePointInterface;
use App\Repo\DriverOrderLocationInterface;
use App\Repo\DriverPointsInterface;
use App\Repo\EarningRuleInterface;
use App\Repo\Eloquent\DeliveryStatusRepo;
use App\Repo\Eloquent\DeliveryStatusTranslationRepo;
use App\Repo\Eloquent\DriverCollectionRepo;
use App\Repo\Eloquent\DriverEarningRepo;
use App\Repo\Eloquent\DriverEarningRuleRepo;
use App\Repo\Eloquent\DriverEarningWithdrawnRepo;
use App\Repo\Eloquent\DriverExperiencePointRepo;
use App\Repo\Eloquent\DriverOrderLocationRepo;
use App\Repo\Eloquent\DriverPointsRepo;
use App\Repo\Eloquent\DriverTeamMembersRepo;
use App\Repo\Eloquent\DriverTeamRepo;
use App\Repo\Eloquent\ExperiencePointRulesRepo;
use App\Repo\Eloquent\MachineStatusRepo;
use App\Repo\Eloquent\OrderRejectStatusRepo;
use App\Repo\Eloquent\OrderRejectStatusTranslationRepo;
use App\Repo\Eloquent\OrderSettingRepo;
use App\Repo\Eloquent\PartialRefundRepo;
use App\Repo\Eloquent\UserCommentRepo;
use App\Repo\ExperiencePointRulesInterface;
use App\Repo\MachineStatusInterface;
use App\Repo\OrderRejectStatusInterface;
use App\Repo\OrderRejectStatusTranslationInterface;
use App\Repo\OrderSettingInterface;
use App\Repo\PartialRefundInterface;
use App\Repo\UserCommentInterface;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Repo\DriverOrderInterface;
use App\Repo\Eloquent\DriverOrderRepo;
use App\Repo\Eloquent\OrderCommentRepo;
use App\Repo\Eloquent\OrderRepo;
use App\Repo\Eloquent\OrderStatusRepo;
use App\Repo\Eloquent\OrderStatusTranslationRepo;
use App\Repo\Eloquent\PaymentMethodRepo;
use App\Repo\Eloquent\PaymentMethodTranslationRepo;
use App\Repo\Eloquent\PaymentStatusRepo;
use App\Repo\Eloquent\PaymentStatusTranslationRepo;
use App\Repo\Eloquent\ShippingMethodRepo;
use App\Repo\Eloquent\ShippingMethodTranslationRepo;
use App\Repo\OrderCommentInterface;
use App\Repo\OrderInterface;
use App\Repo\OrderStatusInterface;
use App\Repo\OrderStatusTranslationInterface;
use App\Repo\PaymentMethodInterface;
use App\Repo\PaymentMethodTranslationInterface;
use App\Repo\PaymentStatusInterface;
use App\Repo\PaymentStatusTranslationInterface;
use App\Repo\Eloquent\OrderItemAddonRepo;
use App\Repo\Eloquent\OrderItemRepo;
use App\Repo\OrderItemAddonInterface;
use App\Repo\OrderItemInterface;
use App\Repo\ShippingMethodInterface;
use App\Repo\ShippingMethodTranslationInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('roleChecker', 'App\Http\RoleCheckerFacade');
        $this->app->bind('remoteCall','App\Http\RemoteCallFacade');
        $this->app->bind('calculateDistance','App\Http\CalculateDistanceFacade');
        $this->app->bind('settings', 'App\Http\SettingsFacade');
        $this->app->bind(OrderInterface::class,OrderRepo::class);
        $this->app->bind(OrderCommentInterface::class,OrderCommentRepo::class);
        $this->app->bind(OrderStatusInterface::class,OrderStatusRepo::class);
        $this->app->bind(OrderStatusTranslationInterface::class,OrderStatusTranslationRepo::class);
        $this->app->bind(PaymentStatusTranslationInterface::class,PaymentStatusTranslationRepo::class);
        $this->app->bind(PaymentStatusInterface::class,PaymentStatusRepo::class);
        $this->app->bind('foodData', 'App\Http\FoodCheckerFacade');
        $this->app->bind(OrderItemAddonInterface::class,OrderItemAddonRepo::class);
        $this->app->bind(OrderItemInterface::class,OrderItemRepo::class);
        $this->app->bind(ShippingMethodInterface::class, ShippingMethodRepo::class);
        $this->app->bind(ShippingMethodTranslationInterface::class, ShippingMethodTranslationRepo::class);
        $this->app->bind(PaymentMethodInterface::class, PaymentMethodRepo::class);
        $this->app->bind(PaymentMethodTranslationInterface::class, PaymentMethodTranslationRepo::class);
        $this->app->bind(DeliveryStatusInterface::class,DeliveryStatusRepo::class);
        $this->app->bind(DeliveryStatusTranslationInterface::class,DeliveryStatusTranslationRepo::class);

        $this->app->bind(DriverOrderInterface::class,DriverOrderRepo::class);
        $this->app->bind(DriverPointsInterface::class, DriverPointsRepo::class);
        $this->app->bind(DriverOrderLocationInterface::class, DriverOrderLocationRepo::class);

        $this->app->bind(ExperiencePointRulesInterface::class,ExperiencePointRulesRepo::class);
        $this->app->bind(EarningRuleInterface::class,DriverEarningRuleRepo::class);
        $this->app->bind(DriverExperiencePointInterface::class,DriverExperiencePointRepo::class);
        $this->app->bind(DriverEarningInterface::class,DriverEarningRepo::class);
        $this->app->bind(DriverEarningWithdrawnInterface::class,DriverEarningWithdrawnRepo::class);

        $this->app->bind("imageUploader",'App\Http\ImageUploadFacade');


        $this->app->bind(MachineStatusInterface::class,MachineStatusRepo::class);
        $this->app->bind(PartialRefundInterface::class,PartialRefundRepo::class);
        $this->app->bind(OrderRejectStatusInterface::class,OrderRejectStatusRepo::class);
        $this->app->bind(OrderRejectStatusTranslationInterface::class,OrderRejectStatusTranslationRepo::class);

        $this->app->bind(CronInvoiceInterface::class,CronInvoiceRepo::class);

        $this->app->bind(DriverTeamInterface::class,DriverTeamRepo::class);
        $this->app->bind(DriverTeamMemberInterface::class,DriverTeamMembersRepo::class);
        $this->app->bind(DriverCollectionInterface::class,DriverCollectionRepo::class);
        $this->app->bind(UserCommentInterface::class,UserCommentRepo::class);

        $this->app->bind(OrderSettingInterface::class,OrderSettingRepo::class);







    }

    public function boot()
    {
        /**
         * takes the google cloud credential
         */
        $array = [
            "type" =>env('STACK_TYPE') ,
            "project_id" => env('STACK_PROJECT_ID'),
            "private_key_id" => env('STACK_PRIVATE_KEY_ID'),
            "private_key" => str_replace('\n', "\n", env('STACK_PRIVATE_KEY')),
            "client_email" => env('STACK_CLIENT_EMAIL'),
            "client_id" => env('STACK_CLIENT_ID'),
            "auth_uri" => env('STACK_AUTH_URI'),
            "token_uri" => env('STACK_TOKEN_URL'),
            "auth_provider_x509_cert_url" => env('STACK_AUTH_PROVIDER_x509_CERT_URL'),
            "client_x509_cert_url" => env('STACK_CLIENT_X509_CERT_URL')
        ];
        /**
         * the path to create and store the json
         */
        $path = storage_path().'/stackdriver.json';
        /**
         * open the file
         */
        $fp = fopen($path, 'w');
        /**
         * store array in json with pretty print
         */
        fwrite($fp, json_encode($array,JSON_PRETTY_PRINT ));
        fclose($fp);
        /*
         * checks if the assign order for driver is assigned
         */
        Validator::extend('status_validation', function($attribute, $value, $parameters, $validator) {
            $test=  'assigned' == $value; //in_array($value,["assigned"]);
            if(!$test){
                return false;
            }
            return true;
        });
        /**
         * validate status for admin and user update
         */
        Validator::extendImplicit('status_validation_update', function($attribute, $value, $parameters, $validator) {
            try{
                $validator->addReplacer('status_validation_update', function ($message, $attribute, $rule, $parameters)  {
                    /*
                     * replace :other and :value by $parameters[0] and $parameters[1]  in the message if present in $message
                     */
                    return str_replace(array(':other', ':value'), $parameters,$message);
                });

                if($parameters[0] == "order") OrderStatus::whereRaw("BINARY `slug`= ?",[$value])->firstOrFail();
                elseif ($parameters[0] == "payment") PaymentStatus::whereRaw("BINARY `slug`= ?",[$value])->firstOrFail();
                elseif($parameters[0] == "delivery") DeliveryStatus::whereRaw("BINARY `slug`= ?",[$value])->firstOrFail();
                else return false;
                return true;
            }
            catch(\Exception $ex) {
                return false;
            }
        });

        Validator::extendImplicit('amount_validation', function($attribute, $value, $parameters, $validator) {
            $validator->addReplacer('amount_validation', function ($message, $attribute, $rule, $parameters)  {

                return str_replace(array(':value'), $attribute,$message);
            });
            $getAllAttribute = $validator->getData();
            if(!(isset($getAllAttribute["order_id"]) && isset($getAllAttribute["type"]) && $getAllAttribute["type"] == "minus")){
                return true;
            }
            else{
                $getOrder = Orders::find($getAllAttribute["order_id"]);
                if(!$getOrder){
                    return true;
                }
                if($value > $getOrder->amount ){
                    return false;
                }
                else {
                    return true;
                }
            }



        });

        /*
         * checks if the assign order for driver is completed
         */

        Validator::extend('status_validation_driver_update', function($attribute, $value, $parameters, $validator) {
            $test = in_array($value,["completed","picked"]);
            if(!$test){
                return false;
            }
            return true;
        });

        Validator::extend('status_validation_driver_update_by_admin', function($attribute, $value, $parameters, $validator) {
            $test= 'reassigned' == $value; //in_array($value,["reassigned"]);
            if(!$test){
                return false;
            }
            return true;
        });
        /**
         *extendImplicit runs for the validation even if the field is empty or not present in input
         */
        Validator::extend('ignore_if', function($attribute, $value, $parameters, $validator) {
            $requestData = $validator->getData();
            $columnName = $parameters[0];
            /*
             * this will set custom message values
             */
            $validator->addReplacer('ignore_if', function ($message, $attribute, $rule, $parameters)  {
                /*
                 * replace :other and :value by $parameters[0] and $parameters[1]  in the message if present in $message
                 */
                return str_replace(array(':other', ':value'), $parameters,$message);
            });
            if($requestData["$columnName"] !== $parameters[1]){
                if(!isset($requestData["$attribute"])){
                    return false;
                }
                return true;
            }
            return true;

        });
    }


}
