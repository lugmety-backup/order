<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\IpnEvent' => [
            'App\Listeners\IpnListener',
        ],
        'App\Events\SendMessage' => [
            'App\Listeners\DispatchMessage',
        ],
        'App\Events\SendInvoiceMessage' => [
            'App\Listeners\DispatchInvoice',
        ],
    ];
}
