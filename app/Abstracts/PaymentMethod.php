<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 6/25/17
 * Time: 3:52 PM
 */

namespace App\Abstracts;


class PaymentMethod
{

    protected $title;
    protected $orderStatus;
    protected $paymentStatus;
    protected $paymentRefId;
    protected $ccFirstName;
    protected $ccLastName;
    protected $ccPhoneNumber;
    protected $phoneNumber;
    protected $email;
    protected $productsPerTitle;
    protected $unitPrice;
    protected $quantity;
    protected $otherCharges;
    protected $amount;
    protected $discount;
    protected $currency;
    protected $reference_no;
    protected $ipCustomer;
    protected $ipMerchant;
    protected $billingAddress;
    protected $city;
    protected $state;
    protected $postalCode;
    protected $country;
    protected $addressShipping;
    protected $stateShipping;
    protected $cityShipping;
    protected $msgLang;
    protected $countryShipping;
    protected $postalCodeShipping;
    protected $paymentReference;
    protected $redirectUrl;
    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return PaymentMethod
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * @param mixed $orderStatus
     * @return PaymentMethod
     */
    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * @param mixed $paymentStatus
     * @return PaymentMethod
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentRefId()
    {
        return $this->paymentRefId;
    }

    /**
     * @param mixed $paymentRefId
     * @return PaymentMethod
     */
    public function setPaymentRefId($paymentRefId)
    {
        $this->paymentRefId = $paymentRefId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCcFirstName()
    {
        return $this->ccFirstName;
    }

    /**
     * @param mixed $ccFirstName
     * @return PaymentMethod
     */
    public function setCcFirstName($ccFirstName)
    {
        $this->ccFirstName = $ccFirstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCcLastName()
    {
        return $this->ccLastName;
    }

    /**
     * @param mixed $ccLastName
     * @return PaymentMethod
     */
    public function setCcLastName($ccLastName)
    {
        $this->ccLastName = $ccLastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCcPhoneNumber()
    {
        return $this->ccPhoneNumber;
    }

    /**
     * @param mixed $ccPhoneNumber
     * @return PaymentMethod
     */
    public function setCcPhoneNumber($ccPhoneNumber)
    {
        $this->ccPhoneNumber = $ccPhoneNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $PhoneNumber
     * @return PaymentMethod
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return PaymentMethod
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductsPerTitle()
    {
        return $this->productsPerTitle;
    }

    /**
     * @param mixed $productsPerTitle
     * @return PaymentMethod
     */
    public function setProductsPerTitle($productsPerTitle)
    {
        $this->productsPerTitle = $productsPerTitle;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param mixed $unitPrice
     * @return PaymentMethod
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     * @return PaymentMethod
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOtherCharges()
    {
        return $this->otherCharges;
    }

    /**
     * @param mixed $otherCharges
     * @return PaymentMethod
     */
    public function setOtherCharges($otherCharges)
    {
        $this->otherCharges = $otherCharges;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return PaymentMethod
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discount
     * @return PaymentMethod
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $currency
     * @return PaymentMethod
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return PaymentMethod
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     * @return PaymentMethod
     */
    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param mixed $postalCode
     * @return PaymentMethod
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     * @return PaymentMethod
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddressShipping()
    {
        return $this->addressShipping;
    }

    /**
     * @param mixed $addressShipping
     * @return PaymentMethod
     */
    public function setAddressShipping($addressShipping)
    {
        $this->addressShipping = $addressShipping;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStateShipping()
    {
        return $this->stateShipping;
    }

    /**
     * @param mixed $stateShipping
     * @return PaymentMethod
     */
    public function setStateShipping($stateShipping)
    {
        $this->stateShipping = $stateShipping;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCityShipping()
    {
        return $this->cityShipping;
    }

    /**
     * @param mixed $cityShipping
     * @return PaymentMethod
     */
    public function setCityShipping($cityShipping)
    {
        $this->cityShipping = $cityShipping;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMsgLang()
    {
        return $this->msgLang;
    }

    /**
     * @param mixed $msgLang
     * @return PaymentMethod
     */
    public function setMsgLang($msgLang)
    {
        $this->msgLang = $msgLang;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReferenceNo()
    {
        return $this->reference_no;
    }

    /**
     * @param mixed $reference_no
     * @return PaymentMethod
     */
    public function setReferenceNo($reference_no)
    {
        $this->reference_no = $reference_no;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIpCustomer()
    {
        return $this->ipCustomer;
    }

    /**
     * @param mixed $ipCustomer
     * @return PaymentMethod
     */
    public function setIpCustomer($ipCustomer)
    {
        $this->ipCustomer = $ipCustomer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIpMerchant()
    {
        return $this->ipMerchant;
    }

    /**
     * @param mixed $ipMerchant
     * @return PaymentMethod
     */
    public function setIpMerchant($ipMerchant)
    {
        $this->ipMerchant = $ipMerchant;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @return mixed
     */
    public function getCountryShipping()
    {
        return $this->countryShipping;
    }

    /**
     * @param mixed $countryShipping
     * @return PaymentMethod
     */
    public function setCountryShipping($countryShipping)
    {
        $this->countryShipping = $countryShipping;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostalCodeShipping()
    {
        return $this->postalCodeShipping;
    }

    /**
     * @param mixed $postalCodeShipping
     * @return PaymentMethod
     */
    public function setPostalCodeShipping($postalCodeShipping)
    {
        $this->postalCodeShipping = $postalCodeShipping;
        return $this;
    }

    /**
     * @param mixed $billingAddress
     * @return PaymentMethod
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentReference()
    {
        return $this->paymentReference;
    }

    /**
     * @param mixed $paymentReference
     * @return PaymentMethod
     */
    public function setPaymentReference($paymentReference)
    {
        $this->paymentReference = $paymentReference;
        return $this;
    }

    public function getRedirectUrl(){
       return  $this->redirectUrl;
    }

    public function setRedirectUrl($returnUrl){
        $this->redirectUrl = $returnUrl;
        return $this;
    }

   }
