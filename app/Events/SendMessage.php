<?php

namespace App\Events;

use Illuminate\Support\Facades\Log;

class SendMessage extends Event
{
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $array;

    public function __construct($array)
    {


        $this->array = $array;

//        Log::info(serialize($this->array));


    }
}
