<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/5/2018
 * Time: 3:08 PM
 */

namespace App\Events;


class LogEvent extends Event
{
    public $message;

    public function __construct($message)
    {
        $this->message = $message;
    }
}