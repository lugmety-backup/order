<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 2/23/2018
 * Time: 4:58 PM
 */

namespace App\Events;


use Illuminate\Support\Facades\Log;

class SendInvoiceMessage extends Event
{
    public $message;

    public function __construct($message)
    {
        $this->message = $message;
    }
}