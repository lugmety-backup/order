<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 2/25/2018
 * Time: 12:17 PM
 */

namespace App\Listeners;


use App\Events\SendInvoiceMessage;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class DispatchInvoice implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * $tries holds no. of tries queue make before assigning a job as failed job
     * @var int
     */
    public $tries = 10;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $events;


    public function handle(SendInvoiceMessage $event)
    {
        $this->events = $event;



        try {
            \Amqp::publish('service1', serialize($this->events->message), [
                'queue' => 'invoice',
                'exchange_type' => 'direct',
                'exchange' => 'amq.direct',
            ]);
        }
        catch (\Exception $ex){
            $this->failed($ex->getMessage());
        }
    }

    public function failed($ex)
    {
        Log::info($ex);


    }
}