<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/5/2018
 * Time: 3:15 PM
 */

namespace App\Listeners;


use App\Events\LogEvent;
use Illuminate\Queue\InteractsWithQueue;

class DispatchLog
{
    use InteractsWithQueue;
    /**
     * $tries holds no. of tries queue make before assigning a job as failed job
     * @var int
     */
    public $tries = 10;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $events;


    public function handle(LogEvent $event)
    {
        $this->events = $event;



        try {
            \Amqp::publish('service1', serialize($this->events->message), [
                'queue' => 'logs',
                'exchange_type' => 'direct',
                'exchange' => 'amq.direct',
            ]);
        }
        catch (\Exception $ex){
            $this->failed($ex->getMessage());
        }
    }

    public function failed($ex)
    {
        Log::info($ex);


    }
}