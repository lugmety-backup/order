<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/22/2017
 * Time: 11:58 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DeliveryStatusTranslation extends Model
{
    protected $table = 'delivery_status_translation';

    public $timestamps = false;

    protected $fillable = [
        'delivery_status_id',
        'lang_code',
        'name'

    ];
}