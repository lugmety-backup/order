<?php

/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 6/11/2017
 * Time: 10:04 PM
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class OrderItemAddon extends Model
{
    protected $table = "order_item_addons";

    protected $fillable = ['order_item_id','item_total','addon_name','addon_id','quantity','unit_price','discount','addon_raw_data'];

    public function orderItem(){
        return $this->belongsTo('App\Models\OrderItem');
    }
}