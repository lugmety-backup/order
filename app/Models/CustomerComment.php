<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 5/15/2018
 * Time: 2:14 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CustomerComment extends Model
{
    protected $table = 'customer_comments';
    protected $fillable = ['comment','user_id','admin_id'];
}