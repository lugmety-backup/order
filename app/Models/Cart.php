<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 6/12/17
 * Time: 11:56 AM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Cart extends Model
{
    protected $table = "carts";

    protected $fillable = ['id','hash','user_id','cart_data'];

}