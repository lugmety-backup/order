<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 11:18 AM
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DriverEarning extends Model
{
    protected $table ="driver_earning";
    protected $fillable = ["user_id","order_id","amount","type","country_id","comment",'date_for'];
    public $earningStatusNEn = "Normal";
    public $earningStatusNAr = "عادي";
    public $earningStatusDEn = "Deduction";
    public $earningStatusDAr = "المستقطع";
    public $amountWithCurrency = "";



//    public function getAmountAttribute($value){
//        return number_format($value, 2,".","");
//    }

    public function setAmountAttribute($value){
        $this->attributes["amount"] = round($value,2);
    }

    public function getDateForAttribute($value){
        if(is_null($value) || empty($value)){
            return $value;
        }
        else{
            return Carbon::parse($value)->format('Y-m');
        }
    }

    public function getAmountAttribute($value){
        if(empty($this->amountWithCurrency)){
            return $value;
        }
        else{
            return $this->amountWithCurrency . "$value";
        }
    }

}
