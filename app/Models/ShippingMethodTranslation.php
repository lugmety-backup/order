<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:05 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingMethodTranslation extends Model
{

    protected $table='shipping_method_translation';
    protected $fillable=['shipping_method_id', 'lang_code','name'];

    public $timestamps = false;

    public function shippingMethod(){
        return $this->belongsTo('App\Models\ShippingMethod','shipping_method_id');
    }
}