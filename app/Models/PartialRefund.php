<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 1/28/2018
 * Time: 12:02 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PartialRefund extends Model
{
    protected $table = "partial_refunds";
    protected $fillable = ["title","amount","vat","type","user_id","order_id","comment"];

    public function order(){
        return $this->belongsTo('App\Models\Orders','order_id');
    }

}