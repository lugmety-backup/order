<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 12/13/2017
 * Time: 11:41 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MachineStatus extends Model
{
    protected $table = "machine_status";
    protected $fillable = ["restaurant_id","updated_at"];

}