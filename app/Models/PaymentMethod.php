<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 6/25/17
 * Time: 11:07 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
class PaymentMethod extends Model
{
    protected $table = "payment_method";

    protected $hidden = ["pivot", "paymentMethodTranslation"];

    protected $fillable = ['slug', 'class', 'type', 'order_status','icon', 'status'];

    public function paymentMethodTranslation()
    {
        return $this->hasMany('App\Models\PaymentMethodTranslation','payment_method_id');
    }

    public function getIconAttribute($value)
    {
        if (empty($value)) {
            return $value;
        }
//        $getpositionOfImageInUrl = strrpos($value, "/") + 1;
        //todo add from setting
        return Config::get("config.image_service_base_url_cdn").$value;

    }
}