<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 3/11/2018
 * Time: 11:01 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CronInvoice extends Model
{
    protected $table = "cron_invoice";
    protected $fillable = ["country_id","restaurant_lists","last_date","is_send_invoice"];
}