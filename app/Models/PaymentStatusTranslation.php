<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/15/2017
 * Time: 1:34 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PaymentStatusTranslation extends Model
{
    protected $table = "payment_status_translation";

    protected $fillable = ["payment_status_id","lang_code","name"];

    protected $hidden = ["payment_status_id"];

    public $timestamps = false;
    public function paymentStatus(){
        return $this->belongsTo('App\Models\PaymentStatus');
    }
}