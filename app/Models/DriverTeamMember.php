<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/2/2018
 * Time: 11:46 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DriverTeamMember extends Model
{
    use SoftDeletes;
    protected $table = "driver_team_members";
    protected $fillable = ["driver_team_id","driver_id"];

    protected $appends  =[
        "status"
    ];

    public function driverTeam(){
        return $this->belongsTo(DriverTeam::class);
    }
    public function getStatusAttribute(){
        if(is_null($this->getAttribute("deleted_at"))){
            return 1;
        }
        else{
            return 0;
        }
    }
}