<?php

/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 6/11/2017
 * Time: 10:03 PM
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class OrderItem extends Model
{
    protected $table = "order_items";

    protected $fillable = ['order_id','item_total','item_id','food_name','quantity','unit_price','discount','food_raw_data'];

    public function order(){
        return $this->belongsTo('App\Models\Orders','order_id');
    }
    public function OrderItemAddon(){
        return $this->hasMany('App\Models\OrderItemAddon','order_item_id');
    }



}