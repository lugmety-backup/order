<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/12/2017
 * Time: 4:14 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderRejectStatus extends Model
{
    protected $table ="order_reject_status";

    protected $fillable =["status","sort"];

    public function rejectStatusTranslation(){
        return $this->hasMany('App\Models\OrderRejectStatusTranslation','order_reject_status_id','id');
    }

}