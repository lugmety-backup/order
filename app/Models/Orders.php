<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/11/2017
 * Time: 5:24 PM
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Orders extends Model
{


    use SoftDeletes;
    public $setDefaultLanguage = "en";
    protected $table = "orders";
    public $hideRejection = false;
    protected $appends = [
        'rejection_reason',
    ];
    protected $fillable =[
        "order_date","extra_info","hash","delivery_status","billing_id","restaurant","shipping_id","payment_method","country_id","city_id","district_id","delivery_type","amount","currency",
        "is_seen","customer_id","order_status","payment_status","payment_ref_id","site_commission_rate","pre_order","customer_name",
        "site_commission_amount","delivery_or_pickup_time","restaurant_id","delivery_fee","api_delivery_time","threshold_delivery_time",
        "site_tax_rate","customer_note","delivery_time","distance","restaurant_latitude","machine_retry","restaurant_longitude","shipping_district_id","device_type","time_zone_utc",
        "tax_type","tax","rejection_reason_id","app_version","os_version","model_name","customer_care_history","admin_id","admin_name","user_type","logistics_fee",'customer_priority','is_normal'
    ];
    protected $hidden = ["deleted_at","hash","machine_retry","restaurant","delivery_time","time_zone_utc","customer_care_history","admin_id","admin_name"];
    protected $dates = ['deleted_at'];

    public function orderComments(){
        return $this->hasMany('App\Models\OrderComment',"order_id");
    }
    public function orderStatus(){
        return $this->belongsTo("App\Models\OrderStatus","order_status_id");
    }
    public function orderItem(){
        return $this->hasMany('App\Models\OrderItem',"order_id");
    }

    public function partialRefunds(){
        return $this->hasMany('App\Models\PartialRefund',"order_id");
    }

    public function getSiteCommissionAmountAttribute($value){
        return number_format((float)round($value,2), 2, '.', '');
    }

//    public function setOrderDateAttribute($value){
//        $now = Carbon::now($value);
//        $this->attributes["order_date"] =  $now;
//    }

//    public function setDeliveryOrPickupTimeAttribute($value){
//        $this->attributes["delivery_or_pickup_time"] = Carbon::parse($value)->format('H:i:s');
//    }


    public function getOrderDateAttribute($value){
        $carbon = new Carbon($value);
//        $addTime = $carbon->addHour(3);
        return $carbon->format("d M Y h:i A");
    }

    public function getRejectionReasonAttribute(){

        if(is_null($this->rejection_reason_id) || ($this->getOriginal("order_status") == "pending") ) return "";
        else{
            $orderRejectionStatus = new OrderRejectStatus();
            $reasons = $orderRejectionStatus->
            leftJoin("order_reject_status_translation","order_reject_status.id", "=" ,"order_reject_status_translation.order_reject_status_id")
                ->select("order_reject_status_translation.name")
                ->where("order_reject_status.id",$this->rejection_reason_id)
                ->whereIn("order_reject_status_translation.lang_code",[$this->setDefaultLanguage])
//                ->where(function($query){
//
//                    $query->where("order_reject_status_translation.lang_code",$this->setDefaultLanguage)
//                        ->orWhere("order_reject_status_translation.lang_code","en");
//                })
                ->get();

            if(count($reasons) !== 0){
                return $reasons[0]["name"];
            }
            else{
                return "";
            }
        }
    }




//    public function getAmountAttribute($value){
//        return printf("%2",$value);
//    }

}