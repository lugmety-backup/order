<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/22/2017
 * Time: 11:58 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DeliveryStatus extends Model
{
    protected $table ="delivery_status";

    protected $fillable =["status","is_default","slug"];

    public function deliveryStatusTranslations(){
        return $this->hasMany('App\Models\DeliveryStatusTranslation','delivery_status_id');
    }
}