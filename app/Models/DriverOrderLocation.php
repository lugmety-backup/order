<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/22/2017
 * Time: 2:14 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DriverOrderLocation extends Model
{
    protected $table = "driver_order_location";

    protected $fillable = ["driver_order_id","driver_id","latitude","longitude"];

    public function driverOrder(){
        return $this->belongsTo('App\Models\DriverOrder',"driver_order_id");
    }
}