<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 6/5/2018
 * Time: 11:53 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderSetting extends Model
{
    protected $table = 'order_settings';
    protected $fillable = ['type','data','date_for'];

    public function setDataAttribute($value){
        return $this->attributes['data'] = serialize($value);
    }
}