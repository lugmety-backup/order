<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/2/2018
 * Time: 11:46 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DriverCollection extends Model
{
    protected $table = "driver_collections";
    protected $fillable = ["order_id","driver_id","team_id","admin_id","leader_received_at","admin_received_at","amount","country_id","city_id"];


    public function driverTeam(){
        return $this->belongsTo(DriverTeam::class);
    }


}