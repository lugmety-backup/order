<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 11:16 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ExperiencePointsRules extends Model
{
    protected $table = "experience_point_rules";
    protected $fillable = ["country_id","min_xp","min_distance","xp_per_km","start_date","end_date","is_default"];
}