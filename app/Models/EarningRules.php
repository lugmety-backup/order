<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 11:16 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class EarningRules extends Model
{
    protected $table = "earning_rules";
    protected $fillable = ["country_id","min_earning","min_distance","earning_per_km","start_date","end_date","is_default"];


}