<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 11:17 AM
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DriverExperiencePoint extends Model
{
    protected $table = "driver_experience_point";
    protected $fillable = ["user_id","order_id","experience_point","country_id","type","comment"];
    public $xpStatusNEn = "Normal";
    public $xpStatusNAr = "عادي";
    public $xpStatusDEn = "Deduction";
    public $xpStatusDAr = "المستقطع";
    public function getCreatedAtAttribute($value){
        return Carbon::parse($value)->format("d-m-Y");
    }

//    public function getExperiencePointAttribute($value){
//        return ($value>0 ? $value." Points": $value." Point");
//    }

    public function setExperiencePointAttribute($value){
        $this->attributes["experience_point"] = round($value,2);
    }



}