<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/10/2017
 * Time: 1:05 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShippingMethod extends Model
{
    protected $table='shipping_method';
    protected $fillable=['slug', 'class', 'status', 'is_taxable', 'shipping_cost' , 'shipping_type' , 'dynamic_shipping_value'];

    public function shippingMethodTranslation(){
        return $this->hasMany('App\Models\ShippingMethodTranslation','shipping_method_id');
    }

}