<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/15/2017
 * Time: 1:34 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PaymentStatus extends Model
{
    protected $table ="payment_status";

    protected $fillable =["status","is_default","slug"];

    public function paymentStatusTranslations(){
        return $this->hasMany('App\Models\PaymentStatusTranslation','payment_status_id');
    }

}