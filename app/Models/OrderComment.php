<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 7:07 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderComment extends Model
{
    protected $table = "order_comments";

    protected $fillable = ["order_id","comment","user_id"];

    public function orders(){
        return $this->belongsTo("App\Models\Orders","order_id");
    }

}