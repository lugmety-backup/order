<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 12:07 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class OrderStatusTranslation extends Model
{
    protected $table = "order_status_translation";

    protected $fillable = ["order_status_id","lang_code","name"];

    protected $hidden = ["order_status_id"];

    public $timestamps = false;
    public function orderStaus(){
        return $this->belongsTo('App\Models\OrderStatus');
    }

    public function getNameAttribute($value){
        return ucwords($value);
    }
}