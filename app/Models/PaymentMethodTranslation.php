<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 7/16/17
 * Time: 3:30 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethodTranslation extends Model
{
    protected $table = 'payment_method_translation';

    public $timestamps = false;

    protected $fillable = [
        'payment_method_id',
        'lang',
        'name'

    ];

}