<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriverPoint extends Model
{
    protected $table="driver_points";

    protected $guarded = [
        'id'
    ];

    protected $fillable = [
        'user_id', 'point',
    ];

}
