<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/2/2018
 * Time: 11:46 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class DriverTeam extends Model
{
    use SoftDeletes;
    protected $table = "driver_teams";
    protected $fillable = ["captain_id","name","description","created_by","country_id","city_id"];
    protected $appends  =[
        "status"
    ];
    public function driverTeamMembers(){
        return $this->hasMany(DriverTeamMember::class,"driver_team_id");
    }

    public function driverCollections(){
        return $this->hasMany(DriverCollection::class,"team_id");
    }

    public function getStatusAttribute(){
       if(is_null($this->getAttribute("deleted_at"))){
           return 1;
       }
       else{
           return 0;
       }
    }


}