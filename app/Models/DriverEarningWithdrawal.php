<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 11:19 AM
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DriverEarningWithdrawal extends Model
{
    public $amountWithCurrency = "";
    protected $table = "driver_earning_withdrawal";
    protected $fillable = ["user_id","admin_id","amount","status","country_id","comment",'date_for'];

    //    public function getAmountAttribute($value){
//        return number_format($value, 2,".","");
//    }

    public function setAmountAttribute($value){
        $this->attributes["amount"] = round($value,2);
    }

    public function getAmountAttribute($value){
        if(empty($this->amountWithCurrency)){
            return $value;
        }
        else{
            return $this->amountWithCurrency . "$value";
        }
    }

    public function getDateForAttribute($value){
        if(is_null($value) || empty($value)){
            return $value;
        }
        else{
            return Carbon::parse($value)->format('Y-m');
        }
    }

}