<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/22/2017
 * Time: 2:06 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DriverOrder extends Model
{
    protected $table = 'driver_order';
    protected $fillable = ["user_id","country_id","city_id","order_id","source_district_id","destination_district_id","restaurant_id","distance","price","status","pickup_time","restaurant","order","order_history","driver_picked_time"];
    protected $hidden = ["driver_picked_time"];

    public function driverOrderLocation(){
        return $this->belongsTo('App\Models\DriverOrderLocation','driver_order_id');
    }
}