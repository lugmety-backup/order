<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/12/2017
 * Time: 4:14 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $table ="order_status";

    protected $fillable =["status","is_default","slug"];

    public function orderStatusTranslations(){
        return $this->hasMany('App\Models\OrderStatusTranslation','order_status_id');
    }

}