<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 6/14/17
 * Time: 4:14 PM
 */

namespace App\Http;

use RemoteCall;
use Illuminate\Support\Facades\Config;
class FoodCheckerFacade
{
    /**
     * Call remote restaurant services to validate the food items added in cart.
     * Fetch food details and related addons with their quantity and price.
     * @param $request
     * @return mixed
     */
    public function getFood($request)
    {

       $food = RemoteCall::store(Config::get("config.restaurant_base_url")."/cart", $request);

       if($food['status'] == 503)
       {
           $food['message'] =[
               'status' => 503,
               'message' => "Error Connecting to Service"
       ];
       }
        return ($food['message']);

    }

//    public function reorderCart($request)
//    {
//        $food = RemoteCall::store(Config::get("config.restaurant_base_url")."/cart/reorder", $request);
//
//        if($food['status'] == 503)
//        {
//            $food['message'] =[
//                'status' => 503,
//                'message' => "Error Connecting to Service"
//            ];
//        }
//        return ($food['message']);
//    }

public function getTaxAmount($request)
{
    $amount = RemoteCall::store(Config::get("config.location_base_url")."/calculate/tax", $request);
    if($amount['status'] == 503)
    {
        $amount['message'] =[
            'status' => 503,
            'message' => "Error Connecting to Service"
        ];
    }
    return ($amount['message']);

}
}