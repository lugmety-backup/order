<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/20/2017
 * Time: 5:34 PM
 */

namespace App\Http\Controllers;


use App\Events\SendMessage;
use App\Repo\DriverEarningInterface;
use App\Repo\DriverEarningWithdrawnInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use RoleChecker;

class DriverEarningWithdrawlRequestController extends Controller
{
    protected $driverEarningWithdrawl;

    protected $driverEarning;

    protected $log;

    public function __construct(DriverEarningWithdrawnInterface $driverEarningWithdrawn,\LogStoreHelper $log, DriverEarningInterface $driverEarning)
    {
        $this->driverEarning = $driverEarning;
        $this->driverEarningWithdrawl = $driverEarningWithdrawn;
        $this->log = $log;
    }

    /**
     * get all driver withdrawl request
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllDriverWithdrawlRequest(Request $request){
        try{
            $this->validate($request,[
                "user_id" => "sometimes|integer|min:1",
                "country_id" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            if($request->has("user_id"))  $allDriverWithdrawlRequest = $this->driverEarningWithdrawl->getAllDriverWithdrawnRequest($request->user_id);
            else  $allDriverWithdrawlRequest = $this->driverEarningWithdrawl->getAllWithdrawnRequest();
            /**
             * get location service base url from Config/config.php
             */
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$request->country_id";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if($countryInfo['status'] != 503){
                }
                else{
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails =  $countryInfo["message"]["data"];
            $currency = $shippingDetails["currency_symbol"];
            $allDriverWithdrawlRequest["currency_symbol"] = $currency;
            $allDriverWithdrawlRequest =  collect($allDriverWithdrawlRequest)->where("country_id",$request->country_id);
            foreach ($allDriverWithdrawlRequest as $earning){
                $earning->amountWithCurrency = $currency;
            }
            return \Datatables::of($allDriverWithdrawlRequest)->make(true);
        }catch (\Exception $ex){
            $this->log->storeLogInfo(["get all withdrawl request",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => "Error getting records"
            ],500);
        }
    }

    /**
     * get all driver request
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllDriverRequest(Request $request)
    {
        try {
            $limit = $request->get("limit", 20);
            $this->validate($request, [
                "limit" => "required|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            $limit = 20;
        }
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
            return response()->json([
                "status" => "403",
                "message" => "Only driver can view his/her withdrawal request"
            ], 403);
        }
        try {
            $driverId =\RoleChecker::getUser();
            $getDriverRequestHistory = $this->driverEarningWithdrawl->getAllDriverWithdrawnReqestByDriver(null, $limit, $driverId,$request->country_id);
            $getUserStatusInfo = $this->driverEarningWithdrawl->getAllStatusdata($driverId,$request->country_id);
            /**
             * get location service base url from Config/config.php
             */
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$request->country_id";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if($countryInfo['status'] != 503){
                }
                else{
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails =  $countryInfo["message"]["data"];
            $shippingDetails["currency_symbol"];
            $total = collect([
                "currency_symbol" => $shippingDetails["currency_symbol"],
                "current_balance" => $this->getCurrentEarning($driverId,$request->country_id),
                "paid" =>$getUserStatusInfo["paid"],
                "pending" => $getUserStatusInfo["pending"],
                "rejected" => $getUserStatusInfo["rejected"]
            ]);
            $getDriverRequestHistory = $total->merge($getDriverRequestHistory->withPath("/driver/request")->appends([ "limit" => $limit]));
            return response()->json([
                "status" => "200",
                "data" => $getDriverRequestHistory
            ], 200);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => "Error getting data"
            ],500);
        }
    }


    /**
     * get all driver request
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificDriverRequest(Request $request, $id)
    {
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
            return response()->json([
                "status" => "403",
                "message" => "Only driver can view his/her withdrawal request"
            ], 403);
        }
        try {
            $driverId =\RoleChecker::getUser();
            $getDriverRequestHistory = $this->driverEarningWithdrawl->getSpecificDriverWithdrawnReqestByDriver($id , $driverId,$request->country_id);
            /**
             * get location service base url from Config/config.php
             */
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$request->country_id";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if($countryInfo['status'] != 503){
                }
                else{
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails =  $countryInfo["message"]["data"];
            $getDriverRequestHistory["currency_symbol"] = $shippingDetails["currency_symbol"];
            return response()->json([
                "status" => "200",
                "data" => $getDriverRequestHistory
            ], 200);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Driver Request could not be found."
            ],404);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => "Error getting record"
            ],500);
        }
    }

    /**
     * gets specific withdrawl request record
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificDriverWithdrawlRequest($id){
        try{
            $specificRecord = $this->driverEarningWithdrawl->getSpecificDriverWithdrawnRequest($id);

            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$specificRecord->country_id";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if($countryInfo['status'] != 503){
                }
                else{
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails =  $countryInfo["message"]["data"];
            $userDatas["users"] = [$specificRecord->user_id];
            $oauthUrl = Config::get('config.oauth_base_url')."/review/users/list";
            $getUserDetail =  \RemoteCall::storeWithoutOauth($oauthUrl,$userDatas);
            if ($getUserDetail['status'] != 200) {
                if($getUserDetail['status'] == 503){
                    return response()->json(
                        $getUserDetail, $getUserDetail['status']);
                }
                return response()->json(
                    $getUserDetail["message"], $getUserDetail['status']);
            }
            $customer = $getUserDetail["message"]["data"][0];
            $specificRecord["name"] = implode(" ", array($customer['first_name'], $customer['middle_name'], $customer['last_name']));
            $specificRecord["mobile"] = implode("", array($customer['country_code'], $customer["mobile"]));
            $specificRecord->amountWithCurrency  = $shippingDetails["currency_symbol"];
            return response()->json([
                "status" => "200",
                "data" => $specificRecord
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Withdrawal request could not be found."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogInfo(["get all withdrawl request",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => "Error getting records"
            ],500);
        }
    }

    /**
     * create withdrawl request by driver
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createWithdrawlRequest(Request $request){
        try{
            $this->validate($request,[
                "amount" => "required|numeric|min:1",
                "country_id" =>"required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
            if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
                return response()->json([
                    "status" => "403",
                    "message" => "Only driver can create withdrawal request"
                ],403);
            }

            $driverId = \RoleChecker::getUser();
            //calling location service to get coutry id
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$request->country_id";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if($countryInfo['status'] != 503){
                }
                else{
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails =  $countryInfo["message"]["data"];
            $getCurrentBalance = $this->getCurrentEarning($driverId,$request->country_id);
            if( $request->amount > $getCurrentBalance){
                return response()->json([
                    "status" => "403",
                    "message" => "Requested amount should not be greater than your current balance."
                ],403);
            }
            $request = $request->all();
            $request["status"] = "pending";
            $request["user_id"] = $driverId;
            $createRequest = $this->driverEarningWithdrawl->createWithdrawnRequest($request);
            $this->log->storeLogInfo([
                "Driver Withdraw request",[
                    "status" => "200",
                    "message" => "Your request has been successfully created.",
                    "data" => $createRequest->toArray()
                ]
            ]);
            $array = [
                'service' => 'order service',
                'message' => 'payout created by admin',
                'data' => [
                    'user_details' => ['id' => $request["user_id"]],
                    'driver_id' => $request["user_id"],
                    "payouts" => $request,
                    "country_info" => $shippingDetails
                ]
            ];

            event(new SendMessage($array));
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Your request has been successfully created."
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogInfo(["withdrawl request",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ],500);
        }

    }

    /**
     * create withdrawl request by driver
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createWithdrawlRequestByAdmin(Request $request){
        try{
            $this->validate($request,[
                "amount" => "required|numeric|min:1",
                "user_id" => "required|integer|min:1",
                "status" => "required|in:pending,approved",
                "comment" => "sometimes|string",
                'date_for' => 'required|date_format:Y-m',
                "country_id" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        DB::beginTransaction();


        try{
            $adminID = \RoleChecker::getUser();

            //calling oauth service to check user has role driver
            $baseUrlForOauth = Config::get('config.oauth_base_url');
            $checkUserRoleIsDriver = \RemoteCall::getSpecific($baseUrlForOauth . "/user/$request->user_id/driver");
            if ($checkUserRoleIsDriver["status"] != 200) {
                return response()->json(
                    $checkUserRoleIsDriver["message"]
                , $checkUserRoleIsDriver["status"]);
            }

            //calling location service to get coutry id
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$request->country_id";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if($countryInfo['status'] != 503){
                }
                else{
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails =  $countryInfo["message"]["data"];

            $getCurrentBalance = $this->getCurrentEarning($request->user_id,$request->country_id,$request->date_for);
            if($request->status == "approved" && $request->amount > $getCurrentBalance){
                return response()->json([
                    "status" => "403",
                    "message" => "Requested amount should not be greater than current balance of the driver for requested date."
                ],403);
            }
            $data = $request->all();
            $data["admin_id"] = $adminID;
            $data["date_for"] = Carbon::parse($request->date_for)->tz('utc');

            $createRequest = $this->driverEarningWithdrawl->createWithdrawnRequest($data);


            //creating earning record for approved request
            if($request->status == "approved") {
                $createEarningRecord = $this->driverEarning->createDriverEarning([
                    "user_id" => $createRequest->user_id,
                    "amount" => $createRequest->amount,
                    "type" => "deduction",
                    'date_for' => Carbon::parse($request->date_for)->tz('utc') ,
                    "country_id" => $request->country_id
                ]);
            }
            $this->log->storeLogInfo([
                "Driver Withdraw request by admin",[
                    "status" => "200",
                    "message" => "Your request has been successfully created.",
                    "data" => $createRequest->toArray()
                ]
            ]);
            $array = [
                'service' => 'order service',
                'message' => 'payout created by admin',
                'data' => [
                    'user_details' => ['id' => $request["user_id"]],
                    'driver_id' => $request["user_id"],
                    "payouts" => $data,
                    "country_info" => $shippingDetails
                ]
            ];

            event(new SendMessage($array));

            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Your request has been successfully created."
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogInfo(["withdrawl request by admin",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ],500);
        }

    }


    /**
     * update withdrawl request by admin
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateWithdrawlRequest(Request $request, $id){
        try{
            $this->validate($request,[
                "status" => "required|in:pending,approved,rejected",
                "comment" => "sometimes|string",
                "country_id" => "required|integer|min:1",
                'date_for' => 'required|date_format:Y-m',
                "amount" =>"required|numeric|min:1"

            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
            $requests = $request->all();
            $requests["admin_id"] = \RoleChecker::getUser();
            $requests["status"] = $request->status;
            $requests["date_for"] = Carbon::parse($request->date_for)->tz('utc');
            $getRequest = $this->driverEarningWithdrawl->getSpecificDriverWithdrawnRequest($id);
//            if($getRequest->status == "approved"){
//                return response()->json([
//                    "status" => "403",
//                    "message" => "Approved request could not be changed."
//                ],403);
//            }

            //calling location service to get coutry id
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$request->country_id";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if($countryInfo['status'] != 503){
                }
                else{
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails =  $countryInfo["message"]["data"];

            $getCurrentBalance = $this->getCurrentEarning($getRequest->user_id,$request->country_id,$request->date_for);
            if($request->status == "approved" && $getRequest->amount > $getCurrentBalance){
                return response()->json([
                    "status" => "403",
                    "message" => "Requested amount should not be greater than current balance of the driver for requested date."
                ],403);
            }
            //update withdrawl request type
            $updateRequest = $this->driverEarningWithdrawl->updateWithdrawnRequest($id, $requests);

            //creating deduction in earning record for approved request when original status is not equal to request status
            if($request->status == "approved" && ($request->status != $getRequest->status )) {
                $createEarningRecord = $this->driverEarning->createDriverEarning([
                    "user_id" => $getRequest->user_id,
                    "amount" => $getRequest->amount,
                    "type" => "deduction",
                    'date_for' => Carbon::parse($request->date_for),
                    "country_id" => $request->country_id
                ]);
            }
            //create addition in previously deleted earnings when the original status is approved and the requested status is other than approved
            elseif (($getRequest->status == "approved") && ($request->status != $getRequest->status )){
                $createEarningRecord = $this->driverEarning->createDriverEarning([
                    "user_id" => $getRequest->user_id,
                    "amount" => $getRequest->amount,
                    "type" => "pending",
                    'date_for' =>  Carbon::parse($request->date_for),
                    "country_id" => $request->country_id
                ]);
            }
            //check if status is changed , if changed fire the event
            if($requests["status"] != $getRequest->status ){
                $array = [
                    'service' => 'order service',
                    'message' => 'payout status updated by admin',
                    'data' => [
                        'user_details' => ['id' => $getRequest["user_id"]],
                        'driver_id' => $getRequest["user_id"],
                        "payouts" => $requests,
                        "country_info" => $shippingDetails
                    ]
                ];
                event(new SendMessage($array));
            }
            DB::commit();
            $this->log->storeLogInfo([
                "Driver Withdraw request",[
                    "status" => "200",
                    "message" => "Withdrawal request has been updated successfully.",
                    "data" => $updateRequest
                ]
            ]);
            return response()->json([
                "status" => "200",
                "message" => "Withdrawal request has been updated successfully."
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogInfo(["withdrawl request",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => "Error creating request"
            ],500);
        }

    }

    /**
     * get current balance of the user
     * @param $user_id
     * @return mixed
     */
    private function getCurrentEarning($user_id,$country_id,$date_for = null){
        $normalEarning = $this->driverEarning->getAllDriverEarningNormal($user_id,$country_id,$date_for);
        $deductedEarning = $this->driverEarning->getAllDriverEarningDeducted($user_id,$country_id,$date_for);
        return $normalEarning - $deductedEarning;

    }

}
