<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 12/13/2017
 * Time: 12:44 PM
 */

namespace App\Http\Controllers;


use App\Repo\MachineStatusInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MachineStatusController extends Controller
{
    protected $machineStatus;

    function __construct(MachineStatusInterface $machineStatus)
    {
        $this->machineStatus = $machineStatus;
    }

    public function getRestaurantsStatus(Request $request){
        try{
            $this->validate($request,[
                "ids" => "required|array"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try{
             $getRestaurantMachineStatus = $this->machineStatus->getAllMachineStatus($request["ids"]);
            $allEmpty = false;
            if(count($getRestaurantMachineStatus) ==0){
                $allEmpty = true;
            }
            if($allEmpty === true){
                return response()->json([
                    "status" => "200",
                    "data" => [
                        "allEmpty" => $allEmpty,
                        "data" => []
                    ]
                ]);
            }
            $currentDate = Carbon::now('utc');
            foreach ($getRestaurantMachineStatus as $status ){
                $updatedAt = Carbon::parse($status["updated_at"]);
                $timeDiff = $currentDate->diffInMinutes($updatedAt);
                if($timeDiff > 6){
                    $status["status"] = false;
                }
                else {
                    $status["status"] = true;
                }
            }
            return response()->json([
                "status" => "200",
                "data" => [
                    "allEmpty" => $allEmpty,
                    "data" => $getRestaurantMachineStatus
                ]
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ],500);
        }
    }
}