<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 12/12/2017
 * Time: 11:26 AM
 */

namespace App\Http\Controllers;


use App\Events\SendMessage;
use App\Repo\OrderInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Redis;
class IpnController extends Controller
{
    protected $orders;

    public function __construct(OrderInterface $orders)
    {
        $this->orders = $orders;
    }

    public function paytabsIpn(Request $request){
        try{
            $url = storage_path("logs/ipn.log");
//            error_log('['.date("F j, Y, g:i a e O").']'.print_r($_REQUEST, TRUE), 3, $url);
            $payment_status = 'failed';
            $ipnLog = new Logger("ipn");
            $ipnLog->pushHandler(new StreamHandler(storage_path('logs/ipn.log')),Logger::INFO);
            $ipnLog->info("ipnLog",$request->all());
            $flag = false; //order is present or not flag
            //for web
            if($request->has("reference_id") && $request->has("response_code") ){
                $orderId = $request->get("reference_id");
                $responseCode = $request->get("response_code");
                $orderInfo = $this->orders->getSpecificOrder($orderId);
                if( in_array( $responseCode, array( 5001, 5002 ) ) ){
                    $payment_status = 'completed';
                }
                /**
                 *  refund case
                 */
                elseif ($responseCode == 5003){
                    $payment_status = 'completed';
                }
                $flag = true;
                if($payment_status == 'failed'){
                    $this->orders->updateOrder($orderId,["payment_status" => $payment_status,'order_status' => "cancelled",'delivery_status' => "cancelled"]);
                }
                else{
                    $this->orders->updateOrder($orderId,["payment_status" => $payment_status]);

                }
//                $this->orders->updateOrder($orderId,["payment_status" => $payment_status,'order_status' => "cancelled",'delivery_status' => "cancelled"]);
                $ipnLog->info("ipnLog",["Payment status updated successfully"]);
            }
            //for sdk
            elseif ($request->has("transaction_response_code") && $request->has("order_id") ){
                $orderId = $request->get("order_id");
                $responseCode = $request->get("response_code");
                $orderInfo = $this->orders->getSpecificOrder($orderId);
                if( in_array( $responseCode, array( 100,5001,5002) ) ){
                    $payment_status = 'completed';
                }
                /**
                 *  refund status
                 */
                elseif ($responseCode == 5003){
                    $payment_status = 'completed';
                }

                $flag = true;
                if($payment_status == 'failed'){
                    $this->orders->updateOrder($orderId,["payment_status" => $payment_status,'order_status' => "cancelled",'delivery_status' => "cancelled"]);
                }
                else{
                    $this->orders->updateOrder($orderId,["payment_status" => $payment_status]);

                }
                $ipnLog->info("ipnLogSDK",["Payment status updated successfully"]);
            }
            if($flag) {
                $orderInfo->makeVisible(["hash", "restaurant"]);
                $key = "identifier:" . $orderInfo["hash"];
                Redis::DEL($key);
                Redis::DEL("user-" . $orderInfo["customer_id"]);

                $updatedOrder = $orderInfo->toArray();

                $updatedOrder['extra_info'] = unserialize($updatedOrder['extra_info']);
                $updatedOrder['restaurant'] = unserialize($updatedOrder['restaurant']);

                //return $updatedOrder;
                $array = [
                    'service' => 'order service',
                    'message' => 'order created',
                    'data' => [
                        'user_details' => ['id' => $updatedOrder['customer_id']],
                        'order' => $updatedOrder,
                        'shipping_address' => isset($updatedOrder['extra_info']['shipping_address']) ? $updatedOrder['extra_info']['shipping_address'] : '',
                        'restaurant_id' => $updatedOrder['restaurant_id'],
                        'restaurant_name' => $this->restaurantTranslate($updatedOrder['restaurant']['translation'], "en"),
                        'parent_id' => $updatedOrder['restaurant']['parent_id'],

                    ]
                ];
                if ($payment_status !== "failed" && $updatedOrder['order_status'] == 'pending' ) {
                    event(new SendMessage($array));
                }
            }

        }

        catch (ModelNotFoundException $ex){
           Log::error($ex->getMessage());
            die();
        }
        catch (\Exception $ex){
            Log::error($ex->getMessage());
            die();
        }
    }


    private function restaurantTranslate($resData, $lang)
    {
        if (isset($resData['en'])) {
            $resName = isset($resData[$lang]) ? $resData[$lang] : $resData['en'];
        } else {
            $resName = '';
        }
        return $resName;
    }

}