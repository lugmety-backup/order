<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/12/2017
 * Time: 6:55 AM
 */

namespace App\Http\Controllers;

use App\Events\SendMessage;
use App\Http\Helper\OrderFilterHelper;
use App\Models\Orders;
use App\Models\OrderStatus;
use App\Repo\DeliveryStatusInterface;
use App\Repo\OrderItemAddonInterface;
use App\Repo\OrderItemInterface;
use App\Repo\PaymentMethodInterface;
use App\Repo\ShippingMethodInterface;
use App\Repo\ShippingMethodTranslationInterface;
use Carbon\Carbon;
use Chumper\Zipper\Zipper;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Repo\OrderInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use LogStoreHelper;
use Illuminate\Support\Facades\Config;
use Mpdf\Mpdf;
use Predis\Connection\ConnectionException;
use RemoteCall;
use Illuminate\Support\Facades\Input;
use Validator;
use RoleChecker;
use App\Repo\PaymentStatusInterface;
use App\Repo\OrderStatusInterface;
use Redis;
use CurrencyAttachHelper;
//use FoodData;
use CalculateDistance;
use Yajra\Datatables\Datatables;
use Settings;
use App\Http\Helpers\ExcelExportHelper;
use Excel;
use Spipu\Html2Pdf\Html2Pdf;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends Controller
{
    /**
     * @var OrderInterface
     */
    protected $order;
    /**
     * @var LogStoreHelper
     */
    protected $log;

    protected $dompdf;
    protected $slugify;
    protected $filerOrderHelper;

    /**
     * @var PaymentStatusInterface
     */
    protected $paymentStatus;

    /**
     * @var OrderStatusInterface
     */
    protected $orderStatus;

    protected $deliveryStatus;
    /**
     * @var OrderItemAddonInterface
     */
    protected $orderItemAddon;

    /**
     * @var OrderItemInterface
     */
    protected $orderItem;
    /**
     * OrderController constructor.
     * @param $order
     * @param $log
     */
    protected $shippingMethod;

    protected $shippingMethodTranslation;

    /**
     * @var CurrencyAttachHelper
     */
    protected $currencyAttachHelper;
    protected $paymentMethod;

    protected $allOrderStatus;
    protected $allPaymentStatus;
    protected $allPaymentMethod;
    protected $allShippingMethod;
    protected $allDeliveryStatus;
    protected $excel;

    public function __construct(OrderInterface $order,
                                LogStoreHelper $log,
                                PaymentMethodInterface $paymentMethod,
                                OrderItemAddonInterface $orderItemAddon,
                                ShippingMethodTranslationInterface $shippingMethodTranslation,
                                OrderItemInterface $orderItem,
                                OrderStatusInterface $orderStatus,
                                DeliveryStatusInterface $deliveryStatus,
                                PaymentStatusInterface $paymentStatus,
                                ExcelExportHelper $excel,
                                CurrencyAttachHelper $currencyAttachHelper,
                                Slugify $slugify,
                                OrderFilterHelper $filerOrderHelper,
                                ShippingMethodInterface $shippingMethod)
    {
        $this->order = $order;
        $this->log = $log;
        $this->paymentStatus = $paymentStatus;
        $this->orderStatus = $orderStatus;
        $this->orderItem = $orderItem;
        $this->orderItemAddon = $orderItemAddon;
        $this->shippingMethod = $shippingMethod;
        $this->currencyAttachHelper = $currencyAttachHelper;
        $this->deliveryStatus = $deliveryStatus;
        $this->paymentMethod = $paymentMethod;
        $this->shippingMethodTranslation = $shippingMethodTranslation;
        $this->excel = $excel;
        $this->slugify = $slugify;
        $this->filerOrderHelper = $filerOrderHelper;

    }

    /**
     * checks if order is present for passed user_id
     * @param $id
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkOrderForReview($id, $userId, Request $request)
    {
        $lang = Input::get('lang', 'en');
        try {
            $orderData = $this->order->getOrdersByOrderIdAndUserId($id, $userId);
            $orderData->makeHidden([
                "pre_order", "id", "billing_id", "restaurant", "shipping_id", "payment_method", "country_id", "city_id", "district_id", "delivery_type", "amount", "currency",
                "is_seen", "customer_id", "order_status", "payment_status", "payment_ref_id", "site_commission_rate",
                "site_commission_amount", "delivery_or_pickup_time", "delivery_fee", "api_delivery_time", "threshold_delivery_time",
                "site_tax_rate", "customer_note", "distance", "restaurant_latitude", "restaurant_longitude", "shipping_district_id"
            ]);
            return response()->json([
                "status" => "200",
                "data" => $orderData
            ]);
        } catch (\Exception $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('order-you-are-only-allowed-to-review-your-order',
                "You are only allowed to review your order.", 403, $lang);
//            return response()->json([
//                "status" => "403",
//                "message" => "You are only allowed to review your order only"
//            ], 403);
        }
    }


    /**
     * returns logged in user customer order history
     * @return \Illuminate\Http\JsonResponse
     */
    public function publicIndex(Request $request)
    {
        $userId = \RoleChecker::getUser();
        $lang = $request->get("lang", "en");
        try {

            $this->validate($request, [
                "country_id" => "sometimes|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "messsage" => $ex->response->original
            ], 422);
        }


        /**
         * if limit param is other than integer and validation fails then limit is set to 10
         */
        try {

            $this->validate($request, [
                "limit" => "required|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            $request->merge([
                "limit" => 10
            ]);
        }
        try {
            $countryId = $request->get("country_id", null);
            /**
             * gets orders based on limit and userId
             * */
            $customerOrders = $this->order->getAllOrdersCustomer($request['limit'], $userId, $countryId);
            $customerOrders->setDefaultLanguage = $lang;
            if (count($customerOrders) == 0) {
                return \Settings::getErrorMessageOrDefaultMessage('order-empty-order-list',
                    "Empty order list.", 404, $lang);
//                return response()->json([
//                    "status" => "404",
//                    "message" => "Empty order list"
//                ], 404);
            }
            $orderIdList["id"] = $customerOrders->pluck("id")->toArray();
            $restaurantUrl = Config::get("config.restaurant_base_url");
            $restaurantFullUrl = $restaurantUrl . "/review/list";
            $restaurantFromRemoteCall = RemoteCall::storeWithoutOauth($restaurantFullUrl, $orderIdList);
            if ($restaurantFromRemoteCall['status'] != 200) {
                if ($restaurantFromRemoteCall['status'] == 503) {
                    return response()->json(
                        $restaurantFromRemoteCall, $restaurantFromRemoteCall['status']);
                }
                return response()->json(
                    $restaurantFromRemoteCall["message"], $restaurantFromRemoteCall['status']);
            }
//
            foreach ($customerOrders as $customerOrder) {
                if(isset($customerOrder["user_type"] ) && ($customerOrder["user_type"] == "restaurant-customer") &&  (0 == $customerOrder['is_normal'])){
                    $customerOrder["amount"] = $customerOrder["amount"] - $customerOrder["delivery_fee"];
                    $customerOrder["delivery_fee"] = 0;
                }
                $customerOrder->setDefaultLanguage = $lang;
//                $orderStatusLanguage = $this->getOrderStatusTransaction($customerOrder["order_status"], $lang);
//                if ($orderStatusLanguage) {
//                    $customerOrder["order_status"] = $orderStatusLanguage;
//                }
//                $paymentStatusLanguage = $this->getPaymentStatusTransaction($customerOrder["payment_status"], $lang);
//                if ($paymentStatusLanguage) {
//                    $customerOrder["payment_status"] = $paymentStatusLanguage;
//                }
                $customerOrder["is_eligible"] = false;
                if ($customerOrder->order_status == "accepted") $customerOrder["is_eligible"] = true;
                $customerOrder["review"] = false;
                $customerOrder["review_id"] = null;
//                if($customerOrder["order_status"] != "accepted") continue;
                foreach ($restaurantFromRemoteCall["message"]["data"] as $review) {
                    if ($review["order_id"] == $customerOrder->id) {
                        $customerOrder["review_id"] = $review["id"];
                        $customerOrder["review"] = true;
                    }
                }
                $customerOrder->makeVisible("restaurant");
                $restaurantData = unserialize($customerOrder["restaurant"]);
                $customerOrder["restaurant_name"] = '';
                if ($restaurantData || is_null($restaurantData)) {
                    $customerOrder["restaurant_name"] = $this->restaurantTranslate($restaurantData["translation"], $lang);
                }
//                $customerOrder["amount"] = $this->currencyAttachHelper->attachCurrencyInAmount($customerOrder["currency"], $customerOrder["amount"]);
                $customerOrder->makeHidden(["restaurant"]);


            }
            return response()->json([
                "status" => "200",
                "data" => $customerOrders->appends(["limit" => $request["limit"]])->withPath('/order/history')
            ]);
        } catch (\Exception $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('order-error-viewing-data',
                "Error viewing data.", 500, $lang);
//            return response()->json([
//                "status" => "500",
//                "message" => "Error viewing data"
//            ], 500);
        }
    }

    /**
     * displays all order for admin
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminIndex(Request $request)
    {
        $lang = "en";
        $orderStatus = $request->get("order_status", false);
        try {
            $this->validate($request, [
                "restaurant_id" => "sometimes|integer|min:1",
                "country_id" => "required|integer|min:1",
                "user_id" => "sometimes|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $user_id = $request->get("user_id", null);
            $this->allOrderStatus = $this->orderStatus->getAllOrderStatusIncludingTranslation();
            $this->allPaymentStatus = $this->paymentStatus->getAllPaymentStatusIncludingTranslation();
            $this->allPaymentMethod = $this->paymentMethod->getAllPaymentMethodIncludingTranslation()->makeVisible("paymentMethodTranslation");
            $this->allShippingMethod = $this->shippingMethod->getAllShippingMethodIncludingTranslation();
            $this->allDeliveryStatus = $this->deliveryStatus->getAllDeliveryStatusIncludingTranslation();

            $restaurantIdForFilter = $request->get("restaurant_id");
            if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
                $ordersDatas = $this->order->getAllOrders($orderStatus, $restaurantIdForFilter, $request->country_id, $user_id);
                // get deliveryTime limit
                $deliveryTimeLimit = \Settings::getSettings('cash-on-delivery-limit');
                if ($deliveryTimeLimit["status"] != 200) {
                    return response()->json(
                        $deliveryTimeLimit["message"]
                        , $deliveryTimeLimit["status"]);
                }
                $timeLimitForCashOnDelivery = $deliveryTimeLimit["message"]["data"]["value"];

                $orders = $this->processDatatableRecord($ordersDatas, $lang,false,true,$timeLimitForCashOnDelivery);


            } /**
             * if user have role restaurant-admin or parent role restaurant-admin, then verification is done.
             * oauth response consists restaurant information.if user is restaurant-admin and is_restaurant_admin is false then user cant be able to view
             */
            elseif (RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();
                $flag = false;
                if (!is_null($restaurantInfo) && ($restaurantInfo["is_restaurant_admin"] == true) && !is_null($restaurantInfo["branch_id"])) {
                    if (is_null($restaurantIdForFilter)) {
                        $flag = true;
                        goto mainRestaurantorder;
                    }
                    if (!in_array($restaurantIdForFilter, $restaurantInfo["branch_id"])) {
                        return response()->json([
                            "status" => "403",
                            "message" => "You can view your restaurant orders only."
                        ], 403);
                    }

//                        foreach ($restaurantInfo["branch_id"] as $branch) {
                    mainRestaurantorder:
                    $orders = null;
                    if ($flag) $branchOrders = $this->order->getOrderByListOfRestaurantIdAndCountry($restaurantInfo["branch_id"], $request->country_id, $orderStatus, $user_id);
                    else $branchOrders = $this->order->getOrdersByRestaurantId($restaurantIdForFilter, $orderStatus, $request->country_id, $user_id);


                    $orders = $this->processDatatableRecord($branchOrders, $lang);

                } else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is false
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "You can view your restaurant orders only."
                    ], 403);
                }
            } elseif (RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();
                if (!is_null($restaurantInfo) && !is_null($restaurantInfo["branch_id"]) && ($restaurantInfo["is_restaurant_admin"] == false)) {
                    $orders = null;
                    if (is_null($restaurantIdForFilter)) goto mainBranchData;
                    else if (($restaurantInfo["branch_id"] != $restaurantIdForFilter)) {
                        return response()->json([
                            "status" => "403",
                            "message" => "You can view your restaurant orders only."
                        ], 403);
                    }
                    mainBranchData:

                    $orderDatas = $this->order->getOrdersByRestaurantId($restaurantInfo["branch_id"], $orderStatus, $request->country_id, $user_id);
                    $orders = $this->processDatatableRecord($orderDatas, $lang);

                } else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is true if you are branch-admin
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "You can view your restaurant orders only."
                    ], 403);
                }

            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "You are not allowed to view order"
                ], 403);
            }
//            $orders = new Collection($orders);
//        if (count($orders) == 0) {
//            return response()->json([
//                "status" => "404",
//                "message" => "Empty restaurant order"
//            ], 404);
//        }

            return $orders;
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => "error getting data"
            ], 500);
        }
    }

    public function adminIndexPagination(Request $request)
    {
        $lang = "en";
        $orderStatus = $request->get("order_status", false);
        $sortBy['sort_by'] = $request->get("sort_by", "desc");
        $limit = $request->get("limit");
        $user_id = ($request->has("user_id")) ? $request->user_id : null;
        try {
            $this->validate($request, [

                "search" => "sometimes|string",
                "country_id" => "required|integer|min:1",
                "sort_column" => "sometimes",
                "sort_by" =>"sometimes",

                "restaurant_id" => "sometimes|integer|min:1",
                "start_date" => "sometimes|date_format:Y-m-d",
                "end_date" => "sometimes|date_format:Y-m-d",
                "branch_id" => "sometimes|integer|min:1",
                "delivery_type" => "sometimes",
                "device_type" => "sometimes",
                "payment_method" => "sometimes",
                "delivery_status" => "sometimes",
                "payment_status" => "sometimes",
                "order_status" => "sometimes",
                "customer_id" => "sometimes",
                'admin_id' => 'sometimes'

            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $this->validate($request, [
                "limit" => "required|integer|min:1",
            ]);
        } catch (\Exception $ex) {
            $limit = 10;
        }

        try {
            $appends = [
                "restaurant_id" => ($request->has("restaurant_id")) ? $request->restaurant_id : null,
                "country_id" => $request->country_id,
                "user_id" => $user_id,
                "sort_by" => $sortBy['sort_by'],
                "limit" => $limit,
                "sort_column" => ($request->has("sort_column")) ? $request->sort_column : null
            ];
            $filter = [
                "search" => ($request->has("search")) ? $request->search : null,
                "start_date" => ($request->has("start_date")) ? $request->start_date : null,
                "end_date" => ($request->has("end_date")) ? $request->end_date : null,
                "restaurant_id" => ($request->has("restaurant_id")) ? $request->restaurant_id : null,
                "branch_id" => ($request->has("branch_id")) ? $request->branch_id : null,
                "delivery_type" => ($request->has("delivery_type")) ? $request->delivery_type : null,
                "device_type" => ($request->has("device_type")) ? $request->device_type : null,
                "payment_method" => ($request->has("payment_method")) ? $request->payment_method : null,
                "delivery_status" => ($request->has("delivery_status")) ? $request->delivery_status : null,
                "payment_status" => ($request->has("payment_status")) ? $request->payment_status : null,
                "order_status" => ($request->has("order_status")) ? $request->order_status : null,
                "customer_name" => ($request->has("customer_name")) ? $request->customer_name : null,
                'admin_id' => ($request->has("admin_id")) ? $request->admin_id : null,
            ];
            $appends = array_merge($appends,$filter);
            if($request->has("sort_column")){
                $sortBy['sort_column'] = $request->sort_column;
            }

            $user_id = $request->get("user_id", null);
            $this->allOrderStatus = $this->orderStatus->getAllOrderStatusIncludingTranslation();
            $this->allPaymentStatus = $this->paymentStatus->getAllPaymentStatusIncludingTranslation();
            $this->allPaymentMethod = $this->paymentMethod->getAllPaymentMethodIncludingTranslation()->makeVisible("paymentMethodTranslation");
            $this->allShippingMethod = $this->shippingMethod->getAllShippingMethodIncludingTranslation();
            $this->allDeliveryStatus = $this->deliveryStatus->getAllDeliveryStatusIncludingTranslation();

            if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
                if(is_null($filter['branch_id'])){
                    if(!is_null($filter['restaurant_id'])){
                        $restaurantFullUrl = Config::get("config.restaurant_base_url")."/restaurant/".$filter['restaurant_id']."/branch";
                        $restaruantBranches = RemoteCall::getSpecific($restaurantFullUrl);
                        if ($restaruantBranches['status'] != 200) {
                            return response()->json(
                                $restaruantBranches["message"], $restaruantBranches['status']);
                        }
                        if(count($restaruantBranches['message']['data']) == 0){
                            return response() -> json([
                                "status" => 404,
                                "message" => "No any orders to show."
                            ],404);
                        }
                        $restaraurantBranchIdArray = [];

                        foreach ($restaruantBranches['message']['data'] as $restaruantBranch){
                            $restaraurantBranchIdArray[] = $restaruantBranch['id'];
                        }
                        $filter['branch_id'] = $restaraurantBranchIdArray;
                    }
                }
                // get deliveryTime limit
                $deliveryTimeLimit = \Settings::getSettings('cash-on-delivery-limit');
                if ($deliveryTimeLimit["status"] != 200) {
                    return response()->json(
                        $deliveryTimeLimit["message"]
                        , $deliveryTimeLimit["status"]);
                }
                $timeLimitForCashOnDelivery = $deliveryTimeLimit["message"]["data"]["value"];

                $ordersDatas = $this->order->getAllOrdersPagination($orderStatus, $request->country_id, $user_id, $limit, $sortBy, $filter);
                $orders = $this->processDatatableRecordPagination($ordersDatas, $lang,false,true,$timeLimitForCashOnDelivery);


            } /**
             * if user have role restaurant-admin or parent role restaurant-admin, then verification is done.
             * oauth response consists restaurant information.if user is restaurant-admin and is_restaurant_admin is false then user cant be able to view
             */
            elseif (RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();
                if (!is_null($restaurantInfo) && ($restaurantInfo["is_restaurant_admin"] == true) && !is_null($restaurantInfo["restaurant_id"])) {
                    if(is_null($request['restaurant_id'])){
                        $request['restaurant_id'] = $restaurantInfo['restaurant_id'];
                    }else {
                        if ($request['restaurant_id'] != $restaurantInfo['restaurant_id']) {
                            return response()->json([
                                "status" => "403",
                                "message" => "You can view your restaurant orders only."
                            ], 403);
                        }
                    }
                    if(is_null($filter['branch_id'])){
                        $request['branch_id'] = $restaurantInfo['branch_id'];
                    }else{
                        if(!in_array($request['branch_id'],$restaurantInfo['branch_id'])){
                            return response()->json([
                                "status" => "403",
                                "message" => "You can view your restaurant orders only."
                            ], 403);
                        }
                    }
                    $ordersDatas = $this->order->getAllOrdersPagination($orderStatus, $request->country_id, $user_id, $limit, $sortBy, $filter);
                    $orders = $this->processDatatableRecordPagination($ordersDatas, $lang);

                } else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is false
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "You can view your restaurant orders only."
                    ], 403);
                }
            } elseif (RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();
                if (!is_null($restaurantInfo) && !is_null($restaurantInfo["branch_id"]) && ($restaurantInfo["is_restaurant_admin"] == false)) {

                    if(is_null($filter['restaurant_id'])){
                        $filter['restaurant_id'] = $restaurantInfo['restaurant_id'];
                    }else{
                        if($filter['restaurant_id'] != $restaurantInfo['restaurant_id']){
                            return response()->json([
                                "status" => "403",
                                "message" => "You can view your restaurant orders only."
                            ], 403);
                        }

                    }
                    if(is_null($filter['branch_id'])){
                        $filter['branch_id'] = $restaurantInfo['branch_id'];
                    }else{
                        if($filter['branch_id'] != $restaurantInfo['branch_id']){
                            return response()->json([
                                "status" => "403",
                                "message" => "You can view your restaurant orders only."
                            ], 403);
                        }
                    }
                    $ordersDatas = $this->order->getAllOrdersPagination($orderStatus, $request->country_id, $user_id, $limit, $sortBy, $filter);
                    $orders = $this->processDatatableRecordPagination($ordersDatas, $lang);

                } else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is true if you are branch-admin
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "You can view your restaurant orders only."
                    ], 403);
                }

            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "You are not allowed to view order"
                ], 403);
            }
//            $orders = new Collection($orders);
//        if (count($orders) == 0) {
//            return response()->json([
//                "status" => "404",
//                "message" => "Empty restaurant order"
//            ], 404);
//        }
            return $orders->withPath("/pagination/order")->appends($appends);
        } catch (\Exception $ex) {
            $this->log->storeLogError(['error listing orders',[
                'error_message' => $ex->getMessage(),
                'request' => $request->all()
            ]]);
            return response()->json([
                "status" => "500",
                "message" => "error getting data"
            ], 500);
        }
    }


    public function branchOperatorOrderList(Request $request)
    {
        $lang = Input::get("lang", "en");
        $orderStatus = $request->get("order_status", false);
        $limitOriginal = $limit = $request->get("limit");
        try {
            $this->validate($request, [
                "restaurant_id" => "sometimes|integer|min:1",
//                "country_id" => "required|integer|min:1",
                "user_id" => "sometimes|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $this->validate($request, [
                "limit" => "required|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            $limit = 10;
        }
        try {
            $user_id = $request->get("user_id", null);
            $this->allOrderStatus = $this->orderStatus->getAllOrderStatusIncludingTranslation();
            $this->allPaymentStatus = $this->paymentStatus->getAllPaymentStatusIncludingTranslation();
            $this->allPaymentMethod = $this->paymentMethod->getAllPaymentMethodIncludingTranslation()->makeVisible("paymentMethodTranslation");
            $this->allShippingMethod = $this->shippingMethod->getAllShippingMethodIncludingTranslation();
            $this->allDeliveryStatus = $this->deliveryStatus->getAllDeliveryStatusIncludingTranslation();

            $restaurantIdForFilter = $request->get("restaurant_id");
            $appends = [
                "limit" => $limit,
                "restaurant_id" => ($request->has("restaurant_id") ? $request->restaurant_id : null),
                "user_id" => ($request->has("user_id") ? $request->user_id : null)
            ];

            if (RoleChecker::hasRole("branch-operator") || (RoleChecker::get('parentRole') == "branch-operator")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();
                if (!is_null($restaurantInfo) && !is_null($restaurantInfo["branch_id"]) && ($restaurantInfo["is_restaurant_admin"] == false)) {
                    $orders = null;
                    if (is_null($restaurantIdForFilter)) goto mainBranchDataForBranchOperator;
                    else if (($restaurantInfo["branch_id"] != $restaurantIdForFilter)) {
                        return response()->json([
                            "status" => "403",
                            "message" => "You can view your restaurant orders only."
                        ], 403);
                    }
                    mainBranchDataForBranchOperator:

                    $orderDatas = $this->order->getOrdersByRestaurantIdForBranchOperator($restaurantInfo["branch_id"], $orderStatus, $user_id, $limit);
                    $orders = $this->processDatatableRecord($orderDatas, $lang, true);
                    $orders->withPath('/branch/order/list')->appends($appends);
                    return response()->json([
                        "status" => "200",
                        "data" => $orders
                    ]);


                } else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is true if you are branch-admin
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "You can view your restaurant orders only."
                    ], 403);
                }
            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "Only branch operator has access."
                ]);
            }


        } catch (\Exception $ex) {

            $this->log->storeLogError([
                "error listing order",
                [
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error viewing orders."
            ], 500);
        }
    }


    public function branchOperatorOrderListById()
    {
        try {
            if (RoleChecker::hasRole("branch-operator") || (RoleChecker::get('parentRole') == "branch-operator")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();
                if (!is_null($restaurantInfo) && !is_null($restaurantInfo["branch_id"]) && ($restaurantInfo["is_restaurant_admin"] == false)) {
                    $orders = [];
//get list of pending orders in single array for branch operator
                    $orderDatas = $this->order->getOrdersIdListByRestaurantIdForBranchOperator($restaurantInfo["branch_id"]);

                    $this->log->storeLogInfo([
                        "restaurant app pending order requesting",[
                            "restaurant_id" => $restaurantInfo["branch_id"],
                            "orders" => $orderDatas
                        ]
                    ]);
                    return response()->json([
                        "status" => "200",
                        "data" => $orderDatas
                    ]);


                } else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is true if you are branch-admin
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "You can view your restaurant orders only."
                    ], 403);
                }
            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "Only branch operator has access."
                ]);
            }

        } catch (\Exception $ex) {
            $this->log->storeLogError([
                "error listing order",
                [
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error viewing orders."
            ], 500);
        }
    }


    /**
     * Updates the order. Updates payment_status,order_status,is_seen(for restaurant-admin and branch admin) to 1
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function adminUpdate($id, Request $request)
    {
        DB::beginTransaction();
        try {

            /**
             * custom validation of order and payment status
             */
            $message = [
                "status_validation_update" => ":other status does not exist in :other status database.",
            ];
            if (RoleChecker::hasRole("customer")) {
                $this->validate($request, [
                    "payment_status" => "required|status_validation_update:payment"
                ], $message);
            } else {
                $this->validate($request, [
                    "order_status" => "required|status_validation_update:order",
                    "payment_status" => "required|status_validation_update:payment",
                    "delivery_status" => "required|status_validation_update:delivery",
                    "api_delivery_time" => "required_if:order_status,accepted|date_format:Y-m-d H:i:s"
                ], $message);
            }
//to-do custom validation

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);

        }
        try {
            /**
             * check if user has role admin or super-admin
             */
            if ($request->has('order_status') && $request->order_status == "accepted") {
                $getSetting = Settings::getSettings('threshold-delivery-time');
                if ($getSetting["status"] == "200") {
                    $getThresholdDeliveryTime = $getSetting["message"]["data"]["value"];
                } else {
                    return response()->json($getSetting["message"], $getSetting["status"]);
                }

            }
            $order = $this->order->getSpecificOrder($id);
            if ($request->payment_status == "failed") $request->merge(['order_status', "cancelled"]);;
            if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
                $request["is_seen"] = 1;
                if ($request->order_status == "accepted") {
                    $request->merge([
                        "delivery_time" => $this->calculateDeliveryTime($order, $request, $getThresholdDeliveryTime),
                        "threshold_delivery_time" => $getThresholdDeliveryTime
                    ]);
                    $updatedOrder = $this->order->updateOrder($id, $request->only("order_status", "delivery_status", "payment_status", "is_seen", "api_delivery_time", "delivery_time", "threshold_delivery_time"));
                } else {
                    $updatedOrder = $this->order->updateOrder($id, $request->only("order_status", "delivery_status", "payment_status", "is_seen"));
                }
//                $updatedOrder = $this->order->updateOrder($id, $request->only("order_status", "payment_status", "delivery_status"));
                /**
                 * check if user has role restaurant-admin.
                 */
            } elseif (RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")) {
                /**
                 * get restaurant info from oauth server
                 */
                $restaurantInfo = RoleChecker::getRestaurantInfo();
                /**
                 * checks if restaurant info is null, and is_restaurant_admin is true and order restaurant admin belongs to list of restaurantInfo branch id
                 */
                if (!is_null($restaurantInfo) && (($restaurantInfo["is_restaurant_admin"] == true) && in_array($order->restaurant_id, $restaurantInfo["branch_id"]))) {
                    $request["is_seen"] = 1;
                    if(!$this->filerOrderHelper->checkForOrderPermission($order, $request->all())){
                        return response()->json([
                            "status" => '403',
                            "message" => "Request unauthorized.Please contact customer support for further information."
                        ],403);
                    }

                    if ($request->order_status == "accepted") {
//                        $getThresholdDeliveryTime = "10";
                        $request->merge([
                            "delivery_time" => $this->calculateDeliveryTime($order, $request, $getThresholdDeliveryTime),
                            "threshold_delivery_time" => $getThresholdDeliveryTime
                        ]);
                        $updatedOrder = $this->order->updateOrderRestaurantAdmin($order, $request->only("order_status", "delivery_status", "payment_status", "is_seen", "api_delivery_time", "delivery_time", "threshold_delivery_time"));
                    } else {
                        $updatedOrder = $this->order->updateOrderRestaurantAdmin($order, $request->only("order_status", "delivery_status", "payment_status", "is_seen"));
                    }
                } else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is false
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "Forbidden"
                    ], 403);
                }
                /**
                 * checks if restaurant info is null, and is_restaurant_admin is false and order branch admin belongs to  restaurantInfo branch id
                 */
            } elseif (RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();

                if (!is_null($restaurantInfo) && !is_null($restaurantInfo["branch_id"]) && ($restaurantInfo["is_restaurant_admin"] == false) && in_array($order->restaurant_id, [$restaurantInfo["branch_id"]])) {
                    $request["is_seen"] = 1;
                    if(!$this->filerOrderHelper->checkForOrderPermission($order, $request->all())){
                        return response()->json([
                            "status" => '403',
                            "message" => "Request unauthorized.Please contact customer support for further information."
                        ],403);
                    }
                    if ($request->order_status == "accepted") {
                        $request->merge([
                            "delivery_time" => $this->calculateDeliveryTime($order, $request, $getThresholdDeliveryTime),
                            "threshold_delivery_time" => $getThresholdDeliveryTime
                        ]);
                        $updatedOrder = $this->order->updateOrderRestaurantAdmin($order, $request->only("order_status", "delivery_status", "payment_status", "is_seen", "api_delivery_time", "delivery_time", "threshold_delivery_time"));
                    } else {
                        $updatedOrder = $this->order->updateOrderRestaurantAdmin($order, $request->only("order_status", "delivery_status", "payment_status", "is_seen"));
                    }
                } else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is false
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "Forbidden"
                    ], 403);
                }
            } elseif (RoleChecker::hasRole("customer")) {
                return response()->json([
                    "status" => "200",
                    "message" => "Order updated successfully"
                ]);
            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "You are not allowed to update order"
                ], 403);
            }
            DB::commit();

            $order = $updatedOrder->toArray();
            $order['restaurant'] = unserialize($updatedOrder->restaurant);


            if (isset($request['order_status']) && $request['order_status'] == "accepted") {

                $order = $updatedOrder->makevisible(['delivery_time', 'restaurant', 'extra_info'])->toArray();
                $order['restaurant'] = unserialize($updatedOrder->restaurant);
                $order['extra_info'] = unserialize($updatedOrder->extra_info);

                $array = [
                    'service' => 'order service',
                    'message' => 'order accepted by restaurant',
                    'data' => [
                        'user_details' => ['id' => $updatedOrder['customer_id']],
                        'order' => $order,
                        'restaurant_id' => $updatedOrder['restaurant_id'],
                        'parent_id' => $order['restaurant']['parent_id'],
                        'country_id' => $order['restaurant']['country_id']
                    ]
                ];


                event(new SendMessage($array));
            }
            if (isset($request['order_status']) && $request['order_status'] == "completed") {
                $array = [
                    'service' => 'order service',
                    'message' => 'order completed',
                    'data' => [
                        'user_details' => ['id' => $updatedOrder['customer_id']],
                        'order' => $order,
                        'restaurant_id' => $updatedOrder['restaurant_id'],
                        'parent_id' => $order['restaurant']['parent_id'],
                    ]
                ];

                event(new SendMessage($array));
            }

            if (isset($request['order_status']) && $request['order_status'] == "rejected") {
                $array = [
                    'service' => 'order service',
                    'message' => 'order rejected',
                    'data' => [
                        'user_details' => ['id' => $updatedOrder['customer_id']],
                        'order' => $order,
                        'restaurant_id' => $updatedOrder['restaurant_id'],
                        'parent_id' => $order['restaurant']['parent_id'],
                    ]
                ];

                event(new SendMessage($array));
            }
            if (isset($request['payment_status']) && $request['payment_status'] == "completed" && ($order['payment_method'] == "creditcard" || $order['payment_method'] == "sadad")) {

                $array = [
                    'service' => 'order service',
                    'message' => 'payment successful',
                    'data' => [
                        'user_details' => ['id' => $updatedOrder['customer_id']],
                        'order' => $order,
                        'restaurant_id' => $updatedOrder['restaurant_id'],
                        'parent_id' => $order['restaurant']['parent_id'],
                    ]
                ];

                event(new SendMessage($array));

            }

            $this->log->storeLogInfo(array('Order Updated Successfully', [$updatedOrder]));
            return response()->json([
                "status" => "200",
                "message" => "Order updated successfully"
            ]);
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Order could not be found"
            ], 404);
        }

    }


    public function branchOperatorUpdateOrder($id, Request $request)
    {
        try {
            $order = $this->order->getSpecificOrder($id);
            $cashOnPickupFlag = true;
            $message = [
                "status_validation_update" => ":other status does not exist in :other status database.",
            ];
            if ($order->payment_method == "cash-on-pickup") {
                $this->validate($request, [
                    "order_status" => "required|status_validation_update:order",
                    "payment_status" => "required|status_validation_update:payment",
                    "delivery_status" => "required|status_validation_update:delivery",
                    "api_delivery_time" => "required_if:order_status,accepted|date_format:Y-m-d H:i:s"
                ], $message);
                if ($request->has("order_status") && ($request->order_status == "rejected")) {
                    $this->validate($request, [
                        "rejection_reason_id" => "required|integer|min:0",
                    ]);
                }
            } else {
                $cashOnPickupFlag = false;
                $this->validate($request, [
                    "order_status" => "required|status_validation_update:order",
//                    "payment_status" => "required|status_validation_update:payment",
//                    "delivery_status" => "required|status_validation_update:delivery",
                    "api_delivery_time" => "required_if:order_status,accepted|date_format:Y-m-d H:i:s"
                ], $message);

                if ($request->has("order_status") && ($request->order_status == "rejected")) {
                    $this->validate($request, [
                        "rejection_reason_id" => "required|integer|min:0",
                    ]);
                }
            }


        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Order could not be found"
            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);

        }


        DB::beginTransaction();
//            $notificationFlag = false;
        try {
//            $notificationFlag = false;
            if (RoleChecker::hasRole("branch-operator") || (RoleChecker::get('parentRole') == "branch-operator")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();
                if (!is_null($restaurantInfo) && !is_null($restaurantInfo["branch_id"]) && ($restaurantInfo["is_restaurant_admin"] == false) && in_array($order->restaurant_id, [$restaurantInfo["branch_id"]])) {
                    if(!$this->filerOrderHelper->checkForOrderPermission($order, $request->all())){
                        return response()->json([
                            "status" => '403',
                            "message" => "Request unauthorized.Please contact customer support for further information."
                        ],403);
                    }

                    if ($request->has('order_status') && $request->order_status == "accepted") {
                        $getSetting = Settings::getSettings('threshold-delivery-time');
                        if ($getSetting["status"] == "200") {
                            $getThresholdDeliveryTime = $getSetting["message"]["data"]["value"];
                        } else {
                            return response()->json($getSetting["message"], $getSetting["status"]);
                        }
                    }
                    $request["is_seen"] = 1;
                    if ($request->order_status == "accepted") {
                        if ($request->has("payment_status") && $request->payment_status == "failed") $request->merge(['order_status', "cancelled"]);;
                        $request->merge([
                            "delivery_time" => $this->calculateDeliveryTime($order, $request, $getThresholdDeliveryTime),
                            "threshold_delivery_time" => $getThresholdDeliveryTime
                        ]);
                        if ($cashOnPickupFlag) {
                            $updatedOrder = $this->order->updateOrderRestaurantAdmin($order, $request->only("order_status", "delivery_status", "payment_status", "api_delivery_time", "delivery_time", "threshold_delivery_time", "rejection_reason_id"));
                        } else {
                            $updatedOrder = $this->order->updateOrderRestaurantAdmin($order, $request->only("order_status", "is_seen", "api_delivery_time", "delivery_time", "threshold_delivery_time", "rejection_reason_id"));
                        }

                    } else {
                        if ($cashOnPickupFlag) {
                            $updatedOrder = $this->order->updateOrderRestaurantAdmin($order, $request->only("order_status", "delivery_status", "payment_status", "is_seen", "rejection_reason_id"));
                        } else {
                            $updatedOrder = $this->order->updateOrderRestaurantAdmin($order, $request->only("order_status", "is_seen", "rejection_reason_id"));

                        }

                    }
                    $order = $updatedOrder->makeVisible("restaurant", "extra_info", "delivery_time");
                    $order["restaurant"] = unserialize($order->restaurant);
                    $order['extra_info'] = unserialize($order['extra_info']);
                    if ($order["order_status"] == 'accepted') {
                        $message = 'order accepted by restaurant';
                        $country_id = $order['restaurant']['country_id'];
                        $array = [
                            'service' => 'order service',
                            'message' => $message,
                            'data' => [
                                'user_details' => ['id' => $order['customer_id']],
                                'order' => $order->toArray(),
                                'restaurant_id' => $order['restaurant_id'],
                                'parent_id' => $order["restaurant"]['parent_id'],
                                'country_id' => $country_id
                            ]
                        ];

                        event(new SendMessage($array));
                    } else if ($order["order_status"] == 'rejected') {
                        $message = 'order rejected';
//                        $notificationFlag = true;
                        $country_id = $order['restaurant']['country_id'];
                        $array = [
                            'service' => 'order service',
                            'message' => $message,
                            'data' => [
                                'user_details' => ['id' => $order['customer_id']],
                                'order' => $order->toArray(),
                                'restaurant_id' => $order['restaurant_id'],
                                'parent_id' => $order["restaurant"]['parent_id'],
                                'country_id' => $country_id
                            ]
                        ];

                        event(new SendMessage($array));
                    }

                    DB::commit();
                    return response()->json([
                        "status" => "200",
                        "message" => "Order updated successfully"
                    ]);
                } else {
                    /**
                     * this means there is some modification done in oauth restaurant_meta_table .
                     * and  is_restaurant_admin is true if you are branch-admin
                     * */
                    return response()->json([
                        "status" => "403",
                        "message" => "You can view your restaurant orders only."
                    ], 403);
                }
            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "Only branch operator has access."
                ]);
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ], 500);
        }
    }

    /**
     * creates order and order item
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $foodDatasFromRedis = '';
        $lang = Input::get("lang", "en");
        $payload['redirect_url'] = $request["redirect_url"];
        try {

            if (!RoleChecker::hasRole("customer") && !RoleChecker::hasRole("restaurant-customer")) {
                return \Settings::getErrorMessageOrDefaultMessageWithPayload('order-only-customer-can-place-order',
                    "Only customer can place order.", 403, $lang, $payload);
//                return response()->json([
//                    "status" => "403",
//                    "message" => "Only customer can place order",
//                    "data" => ["redirect_url" => $request["redirect_url"]]
//                ], 403);
            }
            /**
             * Start of database transaction
             */
            DB::beginTransaction();

            /**
             * Validation of input request
             */
            $message = [
                "ignore_if" => "The :attribute field is required when :other field value is not a :value."
            ];
            if ($request->has("delivery_or_pickup_time") && strtolower($request->delivery_or_pickup_time) == "asap") {
                $this->validate($request, [
                    "redirect_url" => "required|url",
                    "hash" => "required",
                    "delivery_type" => "required|alpha-dash|",
                    "shipping_id" => "ignore_if:delivery_type,pickup|integer|min:1",
                    "payment_method" => "required|alpha-dash",
                    "pre_order" => "integer|min:0|max:1",
                    "delivery_or_pickup_time" => "required_if:pre_order,1",
                    "restaurant_id" => "required|integer|min:1",
//                "use_sdk" => "boolean"
                ], $message);
            } else {
                $this->validate($request, [
                    "redirect_url" => "required|url",
                    "hash" => "required",
                    "delivery_type" => "required|alpha-dash|",
                    "shipping_id" => "ignore_if:delivery_type,pickup|integer|min:1",
                    "payment_method" => "required|alpha-dash",
                    "pre_order" => "integer|min:0|max:1",
                    "delivery_or_pickup_time" => "required_if:pre_order,1|date_format:Y-m-d h:i A",
                    "restaurant_id" => "required|integer|min:1",
//                "use_sdk" => "boolean"
                ], $message);
            }
        } /**
         * Displays validation error messages
         */
        catch (\Exception $ex) {

            $this->log->storeLogError(array("Order Error validation", [
                "status" => "200",
                "message" => $ex->response->original,
            ]
            ));
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original,
//                "data" => ["redirect_url" =>$request["redirect_url"]]
            ], 422);
        }
//       if($request->has("delivery_or_pickup_time")) $request->delivery_or_pickup_time = Carbon::parse( $request->delivery_or_pickup_time)->format('H:i:s');
        /**
         * checking mobile_sdk
         */
        if ($request->has("delivery_or_pickup_time") && $request->delivery_or_pickup_time == "ASAP") {
            $request->merge(array('delivery_or_pickup_time' => strtolower($request->delivery_or_pickup_time)));
        }

        if (!$request->has("pre_order")) {
            $request->merge(["pre_order", 0]);
        }
        $request['pre_order'] = $request->get("pre_order", 0);
        $use_sdk = $request->get("use_sdk", false);
        if (!$use_sdk) goto skipSdkCheck;
        if (!($use_sdk && ($request->payment_method === "creditcard" || $request->payment_method === "sadad"))) {
//            return response()->json([
//                "status" => "403",
//                "message" => "Payment method should be either credit card or sadad to use sdk"
//            ], 403);
            return \Settings::getErrorMessageOrDefaultMessage('order-payment-method-should-be-either-credit-card-or-sadad',
                "Payment method should be either credit card or sadad to use sdk.", 403, $lang);

        }


        /**
         * call rolechecker faced to get user id
         */

        skipSdkCheck:
        try {
            $userId = \RoleChecker::getUser();
            $key = "identifier:" . $request["hash"];
            $foodDatasFromRedis = Redis::hgetall($key);
            if (empty($foodDatasFromRedis) || $foodDatasFromRedis["user_id"] != $userId) {
                throw new \Exception();
            }
            $subtotal = $foodDatasFromRedis["total_price"];
            if ($foodDatasFromRedis["restaurant_id"] != $request->restaurant_id) {
//                return response()->json([
//                    "status" => "403",
//                    "message" => "Restaurant Id does not match"
//                ], 403);
                return \Settings::getErrorMessageOrDefaultMessage('order-restaurant-id-does-not-match',
                    "Restaurant Id does not match.", 403, $lang);
            }
        } catch (ConnectionException $ex) {
            $this->log->storeLogError(array("Redis Connection Error", [
                "status" => "503",
                "message" => "Error Connecting Redis Server",
                "data" => ["redirect_url" => $request["redirect_url"]]
            ]));
            return \Settings::getErrorMessageOrDefaultMessageWithPayload('order-error-connecting-redis-server',
                "Error Connecting Redis Server.", 503, $lang, $payload);
//            return response()->json([
//                "status" => "503",
//                "message" => "Error Connecting Redis Server",
//                "data" => ["redirect_url" => $request["redirect_url"]]
//            ], 503);
        } catch (\Exception $ex) {
            //TODO: redirect to previous url

            $this->log->storeLogError(array("Redis key and user checking", [
                "status" => "403",
                "message" => "Key or user_id does not match",
                "data" => [
                    'request' => $request->all(),
                    'redis_data' => $foodDatasFromRedis
                ]
            ]));
            return \Settings::getErrorMessageOrDefaultMessageWithPayload('order-key-or-user-id-does-not-match',
                'Your session has expired. Please logout and login again.', 403, $lang, $payload);
//            return response()->json([
//                "status" => "403",
//                "message" => "Key or user_id does not match",
//                "data" => ["redirect_url" => $request["redirect_url"]]
//            ], 403);
        }
        try {
            $restaurantTranslation = unserialize($foodDatasFromRedis["restaurant_name"]);
            $foodDatasFromRedis = unserialize($foodDatasFromRedis["cart_data"]);

        } catch (\Exception $ex) {
            return \Settings::getErrorMessageOrDefaultMessageWithPayload('order-error-in-unserializing',
                "Error in unserializing.", 403, $lang, $payload);
//            return response()->json([
//                "status" => 403,
//                "message" => "Error in unserializing",
//                "data" => ["redirect_url" => $request["redirect_url"]]
//
//            ], 403);
        }
        $callFoodService["restaurant_id"] = $request["restaurant_id"];
        $orderItems["items"] = [];
        $foodIds = [];
        foreach ($foodDatasFromRedis as $foodData) {
            $orderItemAddon = [];
            if (!empty($foodData["addons"])) {
                foreach ($foodData["addons"] as $addon) {
                    $addons[] = $addon["id"];
                    $orderItemAddon[] = [
                        "addon_id" => $addon["id"],
                        "addon_name" => serialize($addon["name"]),
                        "unit_price" => $addon["price"],
                        "category_id" => $addon['category_id'],
                        "category_name" => $addon['category_name']
                    ];
                }
            } else {
                $addons = [];
                $orderItemAddon = [];
            }
            $orderItems["items"][] = [

                "food_details" => [
                    "total_unit_price" => $foodData["total_unit_price"],
                    "item_id" => $foodData["id"],
                    "food_name" => $foodData["name"],
                    "unit_price" => $foodData["price"],
                    "quantity" => $foodData["quantity"],
                    "image_url" => $foodData["image_url"],
                    "special_instruction" => $foodData['special_instruction'],
                    "size" => $foodData['size_slug'],
                    "size_title" => $foodData['size_title']
                ],

                "addons" => $orderItemAddon,
            ];
            $foodIds[] = $foodData["id"];
            $callFoodService["items"][] = [
                "id" => $foodData["id"],
                "addons" => $addons,
                "quantity" => $foodData["quantity"]
            ];
        }
        //food ids
        $foodRequest["ids"] = array_unique(array_values($foodIds));
        $order["pre_order"] = $request->get("pre_order", 0);
        if ($request->has("delivery_or_pickup_time") && $order["pre_order"] == 1) {
            $order = $request->except("api_delivery_time", "threshold_delivery_time");
            $order["delivery_or_pickup_time"] = ($request->delivery_or_pickup_time == "asap") ? $request->delivery_or_pickup_time : Carbon::parse($request->delivery_or_pickup_time)->format('Y-m-d H:i:s');
            $order["pre_order"] = ($request->delivery_or_pickup_time == "asap") ? 0 : 1;
        } else {
            $order = $request->except("api_delivery_time", "threshold_delivery_time", "delivery_or_pickup_time");
        }
        /**
         * this method will returns default Order status if not found will return pending
         * */
        $order["order_status"] = $this->orderStatus->getDefaultOrderStatus();
        /**
         * this method will returns default Payment status if not found will return unpaid
         * */
        $order["payment_status"] = $this->paymentStatus->getDefaultPayment();

        /**
         * identifying user type 10 for vip,20 for new, 30 for normal
         */
        $user_priority = RoleChecker::get('customer_priority'); //get user priority

        switch ($user_priority){ //checks user priority
            case 10 : {
                $order["customer_priority"] = $user_priority; // sets user priority to vip
                break;
            }
            default : { //if the priority is not 10 then checking if the user is new or normal
                switch (count($this->order->getNewUserFromPassedUsers([$userId]))) { //checks and counts the user is new or not
                    case 1 : {
                        $order["customer_priority"] = 20; // if new then  set to 20
                        break;
                    }
                    default : {
                        $order["customer_priority"] = 30; //default set to normal user
                        break;
                    }
                }
                break;
            }
        }
        /**
         * this method will returns default delivery status if not found will return pending
         * */
        $order["delivery_status"] = $this->deliveryStatus->getDefaultDeliveryStatus();
        $order["machine_retry"] = 0;
        $deviceType = RoleChecker::get("device_type");
        $order["device_type"] = ($deviceType) ? $deviceType : "";
        $restaurantId = $request["restaurant_id"];
        $shippingId = $request["shipping_id"];
        $is_normal = RoleChecker::get('is_normal'); //this variable is used only for restaurant customer to check if the user is set to normal or not
        /**
         * get restaurant base url from Config/config.php
         */
        if(RoleChecker::hasRole("restaurant-customer")){
            $is_restaurant_customer = true;
            $order['is_normal'] = $is_normal;
        }
        else{
            $is_restaurant_customer = false;
            $order['is_normal'] = 1;
        }
        $restaurantBaseUrl = Config::get("config.restaurant_base_url");
        $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$restaurantId/detail?is_restaurant_customer=$is_restaurant_customer&user_id=$userId&lang=" . $lang;
        if (isset($order["delivery_or_pickup_time"]) && isset($order["pre_order"]) && $order["pre_order"] == 1) {
            $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$restaurantId/detail" . "?is_restaurant_customer=$is_restaurant_customer&user_id=$userId&pre_order=" . $order["pre_order"] . "&lang=" . $lang . "&delivery_time=" . $order["delivery_or_pickup_time"];
        }

        /**
         *calling restaurant service to check the restaurant exist or not if exist fetch the data
         */


        $restaurantdata = RemoteCall::getSpecific($restaurantFullUrl);
        if ($restaurantdata['status'] != 200) {
            if ($restaurantdata['status'] == 503) {
                $restaurantdata["data"]["redirect_url"] = $request["redirect_url"];
                return response()->json(
                    $restaurantdata, $restaurantdata['status']);
            }
            $restaurantdata["message"]["data"]["redirect_url"] = $request["redirect_url"];
            return response()->json(
                $restaurantdata["message"], $restaurantdata['status']);
        }
        //calling restaurant service to check if one of the items is out of stock or inactive
        $foodChecker = RemoteCall::storeWithoutOauth("$restaurantBaseUrl/check/foods?lang=$lang",$foodRequest);
        if ($foodChecker['status'] != 200) {
            if ($foodChecker['status'] == 503) {
                $foodChecker["data"]["redirect_url"] = $request["redirect_url"];
                return response()->json(
                    $foodChecker, $foodChecker['status']);
            }
            $foodChecker["message"]["data"]["redirect_url"] = $request["redirect_url"];
            return response()->json(
                $foodChecker["message"], $foodChecker['status']);
        }


        unset($restaurantdata["message"]["data"]["logo"]);
        unset($restaurantdata["message"]["data"]["cover_image"]);

        /*
         * checking if the delivery zone exist or not for restaurant
         */
        if ($order["delivery_type"] !== "pickup" && !isset($restaurantdata["message"]["data"]["delivery_zone"])) {
            return \Settings::getErrorMessageOrDefaultMessageWithPayload('order-delivery-zone-does-not-exist-for-this-restaurant',
                "Delivery zone does not exist for this restaurant.", 404, $lang, $payload);
//            return response()->json([
//                "status" => "404",
//                "message" => "Delivery zone does not exist for this restaurant",
//                "data" => ["redirect_url" => $request["redirect_url"]]
//            ], 404);
        }
        $order["site_commission_rate"] = $restaurantdata["message"]["data"]["commission_rate"];
        $siteCommissionRate = $order["site_commission_rate"] * 0.01 * $subtotal;
        $order["site_commission_amount"] = round($siteCommissionRate, 2);
        $order["customer_id"] = $userId;
        /**
         * attach country_id and city_id in country array to pass to location service
         * which returns currency,tax_percent
         */

        $country_id = $restaurantdata["message"]["data"]["country_id"];
        $city_id = $restaurantdata["message"]["data"]["city_id"];
        $district_id = $restaurantdata["message"]["data"]["district_id"];
//        $order["order_date"] = $restaurantdata["message"]["data"]["timezone"];
        $order["order_date"] = Carbon::now($restaurantdata["message"]["data"]["timezone"]);
        $order["delivery_or_pickup_time"] = ($request["delivery_or_pickup_time"] == "asap" || $request["pre_order"] == 0 || !isset($request["delivery_or_pickup_time"])) ? null : $restaurantdata["message"]["data"]["delivery_time"];
        $order["country_id"] = $country_id;
        $order["city_id"] = $city_id;
        $order["district_id"] = $district_id;

        /**
         *
         * calling oauth server to get user details
         * */
        $oauthBaseUrl = Config::get("config.oauth_base_url");
        $oauthfullUrl = "$oauthBaseUrl/user/details/$userId";// user/details/{id} route of oauth
        $getUserDetail = RemoteCall::getSpecific($oauthfullUrl);//calls user/details/{id} route of oauth
        if ($getUserDetail['status'] != 200) {
            if ($getUserDetail['status'] == 503) {
                $countryInfo["data"]["redirect_url"] = $request["redirect_url"];
                return response()->json(
                    $getUserDetail, $getUserDetail['status']);
            }
            $getUserDetail["message"]["data"]["redirect_url"] = $request["redirect_url"];
            return response()->json(
                $getUserDetail["message"], $getUserDetail['status']);
        }

        /**
         * get location service base url from Config/config.php
         */
        $countryBaseUrl = Config::get("config.location_base_url");
        $countryFullUrl = "$countryBaseUrl/country/$country_id/city/$city_id";
        /**
         * calling to location service
         */
        $deliverFee = [];
        if ($order["delivery_type"] !== "pickup") {
            $deliverFee["address_id"] = $shippingId;
            $deliverFee["delivery_zone"] = $restaurantdata["message"]["data"]["delivery_zone"];
        }
        $countryInfo = RemoteCall::store($countryFullUrl, $deliverFee);
        /**
         * if location service gives status message other than zero then display the message
         */

        if ($countryInfo['status'] != 200) {
            if ($countryInfo['status'] != 503) {
                $countryInfo["message"]["data"]["redirect_url"] = $request["redirect_url"];
            } else {
                $countryInfo["data"]["redirect_url"] = $request["redirect_url"];
                return response()->json(
                    $countryInfo, $countryInfo['status']);
            }
            return response()->json(
                $countryInfo["message"], $countryInfo['status']);
        }
        $restaurantdata["message"]["data"]["translation"] = $restaurantTranslation;

        $order["restaurant"] = serialize($restaurantdata["message"]["data"]);
        $order["currency"] = $countryInfo["message"]["data"]["currency"];
        $order["site_tax_rate"] = $countryInfo["message"]["data"]["tax_percentage"];
        $userDetails = $getUserDetail["message"]["data"];
        $order["customer_name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($userDetails['first_name'], $userDetails['middle_name'], $userDetails['last_name']))));
        $shippingAndCartData["items"] = $orderItems["items"];
        if (isset($countryInfo["message"]["data"]["shipping_details"])) {
            $shippingAndCartData["shipping_details"] = $countryInfo["message"]["data"]["shipping_details"];
        }
        $order["extra_info"] = serialize($shippingAndCartData);
        $timeZone = $restaurantdata["message"]["data"]["timezone"];
        //timeZone conversion to utc
        $timeZoneInISO = Carbon::now($timeZone)->toIso8601String();
        $timeZOneInArray = explode("+", $timeZoneInISO);
        if (isset($timeZOneInArray[1])) {
            $timeZoneInUtc = "+" . $timeZOneInArray[1];
        } else {
            $timeZOneInArray = explode("-", $timeZone);
            $timeZoneInUtc = "-" . $timeZOneInArray[1];
        }
        $order["time_zone_utc"] = $timeZoneInUtc;
        if ($order["delivery_type"] != "pickup") {
            $order["shipping_district_id"] = $countryInfo["message"]["data"]["shipping_district_id"];
            $order["restaurant_latitude"] = $restaurantdata["message"]["data"]["latitude"];
            $order["restaurant_longitude"] = $restaurantdata["message"]["data"]["longitude"];
            $googleResponse = CalculateDistance::getDistance($order["restaurant_latitude"], $order["restaurant_longitude"], $countryInfo["message"]["data"]["latitude"], $countryInfo["message"]["data"]["longitude"]);
            $payment["distance"] = $order["distance"] = $googleResponse["distance"];
            if ($order["pre_order"] == "1" && !is_null($order["delivery_or_pickup_time"])) {
                $order["delivery_time"] = Carbon::createFromFormat("Y-m-d H:i:s", $order["delivery_or_pickup_time"], $timeZone)->addSeconds($googleResponse["duration_seconds"]);

            } else {
                $order["delivery_time"] = Carbon::now($timeZone)->addSeconds($googleResponse["duration_seconds"]);
            }
        } else {
            $countryInfo["message"]["data"]["latitude"] = $order["restaurant_latitude"] = $restaurantdata["message"]["data"]["latitude"];
            $countryInfo["message"]["data"]["longitude"] = $order["restaurant_longitude"] = $restaurantdata["message"]["data"]["longitude"];
            $payment["distance"] = $order["distance"] = 0;
        }

        $payment["location"] = $countryInfo["message"]["data"];


        $payment["restaurant"] = [
//            "latitude" => $restaurantdata["message"]["data"]["latitude"],
//            "longitude" => $restaurantdata["message"]["data"]["longitude"],
            "shipping_method" => $request["delivery_type"]
        ];

        if(RoleChecker::hasRole("restaurant-customer") && 0 == $is_normal){
            $order["user_type"] = "restaurant-customer";
            $order["logistics_fee"] = $restaurantdata["message"]["data"]["logistics_fee"];
        }
        else{
            $order["user_type"] = "customer";
        }



        /**
         * calculated shipping cost
         */
        $order["delivery_fee"] = 0;
        if(!RoleChecker::hasRole("restaurant-customer")){
            $cost = $this->calculateShipping($payment);
            if ($cost->original["status"] != "200") {
                $cost->original["data"]["redirect_url"] = $request["redirect_url"];
                return response()->json($cost->original, $cost->original["status"]);
            }
            $order["delivery_fee"] = $cost->original["data"]['cost'];
        }else{
            $cost = $restaurantdata["message"]["data"]["delivery_fee"];

            $order["delivery_fee"] = $cost;
        }
//        dd($cost);
//        dd($cost->original["data"]['cost']);
        $order["tax_type"] = $restaurantdata["message"]["data"]["tax_type"];
        $extraCharge = 0;
        if ($order["tax_type"] !== "none") {
            $extraCharge = ($subtotal) * $order["site_tax_rate"] * 0.01;
        }
//        else{
//            $extraCharge = ($order["delivery_fee"] + $subtotal) * $order["site_tax_rate"] * 0.01;
//        }

        $order["tax"] = $extraCharge;
        $order["amount"] = round($subtotal + $order["delivery_fee"] + $extraCharge, 2);

        try {
            $orders = $this->order->createOrder($order);

            $this->log->storeLogInfo(array("Order Creation", [$orders]));

            /**
             * Creation of order items
             */
            $paymentToPayment = [];
            $firstItemCheck = true;
            $dataForMobileSdk = [];// for mobile sdk
            foreach ($orderItems["items"] as $orderItem) {
                if ($firstItemCheck) {
                    $foodName = $this->restaurantTranslate($orderItem["food_details"]["food_name"], $lang);
                    $orderItem["food_details"]["food_name"] = serialize($orderItem["food_details"]["food_name"]);
                    $paymentToPayment["products_per_title"] = $foodName;
                    $dataForMobileSdk["pt_product_name"] = $foodName;
                    $paymentToPayment["unit_price"] = $orderItem["food_details"]["total_unit_price"];
                    $paymentToPayment["quantity"] = $orderItem["food_details"]["quantity"];
                } else {
                    $foodName = $this->restaurantTranslate($orderItem["food_details"]["food_name"], $lang);
                    $orderItem["food_details"]["food_name"] = serialize($orderItem["food_details"]["food_name"]);
                    $paymentToPayment["products_per_title"] .= " || " . $foodName;
                    $paymentToPayment["unit_price"] .= " || " . $orderItem["food_details"]["total_unit_price"];
                    $paymentToPayment["quantity"] .= " || " . $orderItem["food_details"]["quantity"];
                }
                $firstItemCheck = false;
                $orderItem["food_details"]["food_raw_data"] = serialize($orderItem["food_details"]);
                $orderItem["food_details"]["order_id"] = $orders->id;
                $creatingOrderItem = $this->orderItem->createItem($orderItem["food_details"]);
                $this->log->storeLogInfo(array("Order Item Creation", [$creatingOrderItem]));
                foreach ($orderItem["addons"] as $addon) {
                    if (empty($addon)) continue;
                    $addon["order_item_id"] = $creatingOrderItem->id;
                    $addon["addon_raw_data"] = serialize($addon);
                    $creatingOrderItemAddon = $this->orderItemAddon->createAddon($addon);
                    $this->log->storeLogInfo(array("Order Item Addon Creation", [$creatingOrderItemAddon]));
                }
            }
            /**
             * data fir paytabs
             */
            $paymentToPayment["cc_phone_number"] = $payment["location"]["calling_code"];
            if (empty($getUserDetail["message"]["data"]["address_line_1"]) || is_null($getUserDetail["message"]["data"]["address_line_1"])) {
                $getUserDetail["message"]["data"]["address_line_1"] = $getUserDetail["message"]["data"]["country"];
            }
            if (empty($getUserDetail["message"]["data"]["city"]) || is_null($getUserDetail["message"]["data"]["city"])) {
                $getUserDetail["message"]["data"]["city"] = "Jeddah";
            }
            if ($order["delivery_type"] != "pickup") {
                $paymentToPayment["address_shipping"] = $payment["location"]["address_name"];
                $paymentToPayment["city"] = $paymentToPayment["state"] = $getUserDetail["message"]["data"]["city"];
                $paymentToPayment["city_shipping"] = $paymentToPayment["state_shipping"] = explode(",", $payment["location"]["address_name"])[1];
            } else {
                $paymentToPayment["address_shipping"] = $getUserDetail["message"]["data"]["address_line_1"];
                $paymentToPayment["city"] = $paymentToPayment["state"] = $paymentToPayment["city_shipping"] = $paymentToPayment["state_shipping"] = $getUserDetail["message"]["data"]["city"];
            }

            $paymentToPayment["msg_lang"] = "English";
            $paymentToPayment["amount"] = $order["amount"];
            $paymentToPayment["other_charges"] = $extraCharge + $order["delivery_fee"];
            $paymentToPayment["discount"] = 0;
            $paymentToPayment["payment_method"] = $request["payment_method"];
            $paymentToPayment["title"] = "Order #$orders->id";
            $paymentToPayment["reference_no"] = $orders->id;
            $paymentToPayment["postal_code"] = $paymentToPayment["postal_code_shipping"] = "11564";
            $paymentToPayment["currency"] = $payment["location"]["currency_code"];
            $paymentToPayment["cc_first_name"] = $getUserDetail["message"]["data"]["first_name"];
            $paymentToPayment["cc_last_name"] = $getUserDetail["message"]["data"]["last_name"];
            $paymentToPayment["email"] = $getUserDetail["message"]["data"]["email"];
            $paymentToPayment["phone_number"] = $getUserDetail["message"]["data"]["mobile"];
            $paymentToPayment["ip_customer"] = $getUserDetail["message"]["data"]["ip_address"];
            /**
             * in the case the iso code of saudi arabia is ksa which is not valid iso code for paytabs .
             * so if ksa or KSA is found it will be converted into SAU
             * only in the case for saudi arabia
             */
            if (($payment["location"]["code"] == "KSA") || ($payment["location"]["code"] == "ksa")) {
                $payment["location"]["code"] = "SAU";
            }
            $paymentToPayment["country"] = $paymentToPayment["country_shipping"] = $payment["location"]["code"];


            $paymentToPayment["billing_address"] = $getUserDetail["message"]["data"]["address_line_1"];
            $paymentToPayment["ip_merchant"] = "127.0.0.0";

            /**
             * data for mobile sdk
             */
            $data = \Settings::getSettings('paytabs-email');
            if ($data["status"] != 200) {
                return response()->json(
                    $data["message"]
                    , $data["status"]);
            }
            $dataForMobileSdk["id"] = $orders->id;
            $dataForMobileSdk["pt_merchant_email"] = $data['message']['data']['value'];
//            $dataForMobileSdk["pt_merchant_email"] =Config::get('config.paytabs_email');

            $data = \Settings::getSettings('paytabs-secret-key');
            if ($data["status"] != 200) {
                return response()->json(
                    $data["message"]
                    , $data["status"]);
            }
            $dataForMobileSdk["pt_secret_key"] = $data['message']['data']['value'];
//            $dataForMobileSdk["pt_secret_key"] =Config::get('config.paytabs_secret_key');

            $dataForMobileSdk["pt_transaction_title"] = implode(" ", array($getUserDetail["message"]["data"]["first_name"], $getUserDetail["message"]["data"]["middle_name"], $getUserDetail["message"]["data"]['last_name']));
            $dataForMobileSdk["pt_amount"] = $order["amount"];
            $dataForMobileSdk["pt_customer_phone_number"] = $paymentToPayment["phone_number"];
            $dataForMobileSdk["pt_currency_code"] = $paymentToPayment["currency"];
            $dataForMobileSdk["pt_customer_email"] = $paymentToPayment["email"];
            $dataForMobileSdk["pt_order_id"] = $orders->id;
            $dataForMobileSdk["pt_address_billing"] = trim(preg_replace("/[^ \w]+/", "", $getUserDetail["message"]["data"]["address_line_1"]));
            $dataForMobileSdk["pt_city_billing"] = $dataForMobileSdk["pt_state_billing"] = $getUserDetail["message"]["data"]["city"];
            $dataForMobileSdk["pt_country_billing"] = $payment["location"]["code"];

            $dataForMobileSdk["pt_postal_code_billing"] = str_replace("+", "00", $getUserDetail["message"]["data"]["country_code"]);
            $dataForMobileSdk["pt_address_shipping"] = trim(preg_replace("/[^ \w]+/", "", $paymentToPayment["address_shipping"]));
            if ($order["delivery_type"] == "pickup") {
                $dataForMobileSdk["pt_city_shipping"] = $dataForMobileSdk["pt_state_shipping"] = $dataForMobileSdk["pt_city_billing"];
                $dataForMobileSdk["pt_country_shipping"] = $dataForMobileSdk["pt_country_billing"];
                $dataForMobileSdk["pt_postal_code_shipping"] = $dataForMobileSdk["pt_postal_code_billing"];
            }
            else {
                $dataForMobileSdk["pt_city_shipping"] = $dataForMobileSdk["pt_state_shipping"] = explode(",", $paymentToPayment["address_shipping"])[1];
                $dataForMobileSdk["pt_country_shipping"] = $paymentToPayment["country"];
                $dataForMobileSdk["pt_postal_code_shipping"] = $paymentToPayment["postal_code"];
            }
            if ($use_sdk) goto useSdk;
            $proxy = Request::create(
                "/payment-method/order/$orders->id",
                'post',
                $paymentToPayment
            );
            $responseFromPayment = \App::dispatch($proxy);
            if ($responseFromPayment->original["status"] != "200") {
                $responseFromPayment->original["data"]["redirect_url"] = $request["redirect_url"];
                return response()->json($responseFromPayment->original, $responseFromPayment->original["status"]);
            } else {
                DB::commit();

                $order = $orders->toArray();
                if ($order['delivery_type'] != "pickup") {

                    $order = $orders->makevisible('delivery_time')->toArray();
                    // dd($order);
                }

                $order['sub_total'] = $subtotal;
                $order['restaurant'] = $restaurantdata["message"]["data"];
                $order['extra_info'] = unserialize($order['extra_info']);
                if ($responseFromPayment->original["data"]["payment_type"] == "non-redirected") {
                    $array = [
                        'service' => 'order service',
                        'message' => 'order created',
                        'data' => [
                            'user_details' => $getUserDetail['message']['data'],
                            'order' => $order,
                            'shipping_address' => $countryInfo['message']['data'],
                            'restaurant_id' => $restaurantdata['message']['data']['id'],
                            'restaurant_name' => $this->restaurantTranslate($restaurantTranslation, "en"),
                            'parent_id' => $restaurantdata["message"]["data"]['parent_id'],
                        ]
                    ];
//return $array;
                    event(new SendMessage($array));
                }


                useSdk:
                if ($use_sdk) {
                    DB::commit();
                    $order = $orders->toArray();
                    $order['sub_total'] = $subtotal;
                    $order['items'] = $orderItems["items"];
                    $order['restaurant'] = $restaurantdata["message"]["data"];
                    $order['extra_info'] = unserialize($order['extra_info']);
                    return \Settings::getErrorMessageOrDefaultMessageWithPayload('order-order-created-successfully',
                        "Order created successfully.", 200, $lang, $dataForMobileSdk);
//                    return response()->json([
//                        "status" => "200",
//                        "message" => "Order created successfully",
//                        "data" => $dataForMobileSdk
//                    ]);
                }

                if ($responseFromPayment->original["data"]["payment_type"] != "redirected") {
                    Redis::DEL($key);
                    $key = "user-" . $userId;
                    if (Redis::exists($key)) {
                        Redis::DEL($key);
                    }
                    $payload_data['id'] = $orders->id;
                    $payload_data['payment_type'] = $responseFromPayment->original["data"]["payment_type"];
                    return \Settings::getErrorMessageOrDefaultMessageWithPayload('order-order-created-successfully',
                        "Order created successfully.", 200, $lang, $payload_data);
//                    return response()->json([
//                        "status" => "200",
//                        "message" => "Order created successfully",
//                        "data" => ["id" => $orders->id, "payment_type" => $responseFromPayment->original["data"]["payment_type"]]
//                    ], 200);
                } else {
                    if (isset($responseFromPayment->original["data"]["p_id"])) {
//                        Redis::DEL($key);
//                        $key= "user-".$userId;
//                        if (Redis::exists($key)) {
//                            Redis::DEL($key);
//                        }
                        $payload_data["id"] = $orders->id;
                        $payload_data["payment_type"] = $responseFromPayment->original["data"]["payment_type"];
                        $payload_data["p_id"] = $responseFromPayment->original["data"]["p_id"];
                        $payload_data["payment_url"] = $responseFromPayment->original["data"]["payment_url"];
                        return \Settings::getErrorMessageOrDefaultMessageWithPayload('order-order-created-successfully',
                            "Order created successfully.", 200, $lang, $payload_data);
//                        return response()->json([
//                            "status" => "200",
//                            "message" => "Order created successfully",
//                            "data" => [
//                                "id" => $orders->id,
//                                "payment_type" => $responseFromPayment->original["data"]["payment_type"],
//                                "p_id" => $responseFromPayment->original["data"]["p_id"],
//                                "payment_url" => $responseFromPayment->original["data"]["payment_url"]
//                            ]
//                        ], 200);
                    }
                }
            }
        } catch (QueryException $ex) {
            $this->log->storeLogError(array("Error in query", [
                "status" => "500",
                "message" => "Error in Query",
                "data" => ["redirect_url" => $request["redirect_url"]]
            ]));
            return \Settings::getErrorMessageOrDefaultMessageWithPayload('order-error-in-query',
                "Error in Query.", 500, $lang, $payload);
//            return response()->json([
//                "status" => "500",
//                "message" => "Error in Query",
//                "data" => ["redirect_url" => $request["redirect_url"]]
//            ], 500);
        }

    }

    public function getSpecificRestaurantInvoice(Request $request)
    {
        set_time_limit(0);
        try {
            $this->validate($request, [
                "start_date" => "required|date_format:Y-m-d H:i:s",
                "end_date" => "required|date_format:Y-m-d H:i:s|after:start_date",
                "restaurant_id" => "required|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
//todo adding lugmety_commission_vat which calculates sums the total commission vat. exactly coppy from generate all report and add new field in notificcation blade
        try {
            $bankInfoEnglish = \Settings::getSettings('invoice-bank-info-english');
            if ($bankInfoEnglish["status"] != 200) {
                return response()->json(
                    $bankInfoEnglish["message"]
                    , $bankInfoEnglish["status"]);
            }

            $bankInfoArabic = \Settings::getSettings('invoice-bank-info-arabic');
            if ($bankInfoArabic["status"] != 200) {
                return response()->json(
                    $bankInfoArabic["message"]
                    , $bankInfoArabic["status"]);
            }

            $tinNumberFromSetting = \Settings::getSettings('invoice-lugmety-tin');
            if ($tinNumberFromSetting["status"] != 200) {
                return response()->json(
                    $tinNumberFromSetting["message"]
                    , $tinNumberFromSetting["status"]);
            }
            $phoneNumber = \Settings::getSettings('invoice-lugmety-phone-number');
            if ($phoneNumber["status"] != 200) {
                return response()->json(
                    $phoneNumber["message"]
                    , $phoneNumber["status"]);
            }
            $results = $this->order->getAllReportOfRestaurantForNotification($request->all());
            $ordersForCsv = $this->order->getAllReportOfRestaurantSales($request->restaurant_id, $request->start_date);

            $results->makeVisible(["restaurant", "extra_info"]);
            $sales = array(
                'creditcard' => 0,
                'cash-on-pickup' => 0,
                'cash-on-delivery' => 0,
                'sadad' => 0,
                "cash-on-delivery-completed" => 0
            );
            $total_amount = 0;
            $site_commission_amount = 0;
            if (count($results) !== 0) {
                $site_commission_rate = $results[0]["site_commission_rate"];
                $site_tax_rate = $results[0]["site_tax_rate"];
                /**
                 * calling restaurant service to get restaurant detail
                 */
                $restaurantId = $results[0]->restaurant_id;
                $restaurantBaseUrl = Config::get("config.restaurant_base_url");
                $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$restaurantId/detail?lang=en";

                $tin = "";
                /**
                 *calling restaurant service to check the restaurant exist or not if exist fetch the data
                 */
                $cc_commision_rate = 2.5;
                $restaurantdata = RemoteCall::getSpecific($restaurantFullUrl);
                if ($restaurantdata['status'] == 200) {
                    $tin = $restaurantdata["message"]["data"]["tin"];
                    $cc_commision_rate = $restaurantdata["message"]["data"]["cc_commision_rate"];
                }

                $restaurantInfo = unserialize($results[0]["restaurant"]);
                $currency = $results[0]["currency"];
                $date = Carbon::parse($request->start_date)->format("Y-m");
                $RestaurantNameInAr = (isset($restaurantInfo["translation"]["ar"])) ? $restaurantInfo["translation"]["ar"] : "";
                $billTo = $restaurantInfo["name"];
                $invoice = strtoupper(substr($restaurantInfo["name"], 0, 4)) . "-" . $restaurantInfo["id"] . "-" . $date;
                if (isset($restaurantInfo["tin"]) && $restaurantdata['status'] != 200) {
                    $tin = $restaurantInfo["tin"];
                }
                foreach ($results as $order) {
                    if ($order['payment_method'] != "cash-on-pickup" && $order["payment_status"] != "completed") {
                        continue;
                    }
                    if ($order["delivery_type"] == "home-delivery" && $order["payment_status"] == "completed") {
                        $deliveryFee = $order['delivery_fee'];// * (1 + $order['site_tax_rate'] * 0.01);
                        $amount = $order['amount'] - $deliveryFee;
//                        $amount = $order['amount'] ;
                        $$site_commission_amount += ($order['site_commission_amount']* (1 + $order['site_tax_rate'] * 0.01));
                        $total_amount = $total_amount + $amount;
                        $sales[$order['payment_method']] = $sales[$order['payment_method']] + $amount;
                    } else {
                        if ($order["payment_method"] == "cash-on-pickup" && $order["payment_status"] == "completed") {
                            $sales["cash-on-delivery-completed"] = $sales["cash-on-delivery-completed"] + $order['amount'];
                        }
                        $site_commission_amount += ($order['site_commission_amount']* (1 + $order['site_tax_rate'] * 0.01));
                        $total_amount = $total_amount + $order['amount'];

                        $sales[$order['payment_method']] = $sales[$order['payment_method']] + $order['amount'];
                    }

                }
//                $cashOnPickupCompleted = round($sales['cash-on-delivery-completed'], 2);
//                $cashOnPickUp = $cashOnPickupCompleted;//round($sales['cash-on-pickup'], 2);
                $cashOnPickUp =round($sales['cash-on-pickup'], 2);
                $subTotal = round($total_amount - $cashOnPickUp, 2);
                $lumetryCommisionAtCash = round($site_commission_amount, 2);
                $paytabs = round($sales['creditcard'], 2);
                $card_issuer_fees = round($cc_commision_rate * 0.01 * $paytabs, 2);

                $cashOnDelivery = round($sales['cash-on-delivery'], 2);
//            $lugmety_cash_at_restaurant = round(($cashOnPickupCompleted) * ($site_commission_rate / 100), 2);
                $lugmety_cash_at_restaurant = round(($cashOnPickUp) * ($site_commission_rate / 100), 2);
                $sadad = round($sales["sadad"], 2);
                // where 2.5 is credit card processing fee
                $total_credit = round($subTotal - $lumetryCommisionAtCash - $lugmety_cash_at_restaurant - $card_issuer_fees, 2);
                if ($results[0]["country_id"] == 1) {
                    $currency1 = "ريال";
                } else {
                    $currency1 = $currency;
                }

                $filename = $billTo;
                $filename = "$filename-$invoice";
                $data = array(
                    "invoice" => $invoice,
                    'tin' => "",
                    "bill_to" => "$billTo",
                    "restaurant_name_arabic" => $RestaurantNameInAr,
                    "for" => "Online Delivery Logistic Services",
                    "total_sales" => $currency . "$total_amount",
                    "total_sales_ar" => "$total_amount " . $currency1,
                    'cash_on_pickup' => $currency . "$cashOnPickUp",
                    'cash_on_pickup_ar' => "$cashOnPickUp " . $currency1,
                    'cash_on_delivery' => $currency . "$cashOnDelivery",
                    'cash_on_delivery_ar' => "$cashOnDelivery " . $currency1,
                    'online_card_payments' => $currency . "$paytabs",
                    'online_card_payments_ar' => "$paytabs " . $currency1,
                    "subtotal" => $currency . "$subTotal",
                    "subtotal_ar" => "$subTotal " . $currency1,
                    "sadad" => $currency . "$sadad",
                    "sadad_ar" => "$sadad " . $currency1,
                    "lugmety_phone_number" => $phoneNumber["message"]["data"]["value"],
                    "card_issuer_fees_rate" => $cc_commision_rate . "%",
                    "card_issuer_fees_rate_ar" => "( % " . str_replace(".", " , ", "$cc_commision_rate") . " ) " . "عمولة بطاقة الإئتمان",
                    "card_issuer_fees" => $currency . "$card_issuer_fees",
                    "card_issuer_fees_ar" => "$card_issuer_fees " . $currency1,
                    "lugmety_commission" => $currency . "$lumetryCommisionAtCash",
                    "lugmety_commission_ar" => "$lumetryCommisionAtCash " . $currency1,
                    'lugmety_commission_rate' => "$site_commission_rate",
                    'lugmety_commission_rate_ar' => "( % " . str_replace(".", " , ", "$site_commission_rate") . " ) " . "عمولة لقمتي",
                    "lugmety_commission_held_restaurant_ar" => "( % " . str_replace(".", " , ", "$site_commission_rate") . " ) " . "عمولة لقمتي المقبوضة كاش من المطعم",
                    "lugmety_cash_at_restaurant" => $currency . "$lugmety_cash_at_restaurant",
                    "lugmety_cash_at_restaurant_ar" => "$lugmety_cash_at_restaurant " . $currency1,
                    "total_credit" => $currency . "$total_credit",
                    "total_credit_ar" => "$total_credit " . $currency1,
                    "bank_info_english" => $bankInfoEnglish["message"]["data"]["value"],
                    "bank_info_arabic" => $bankInfoArabic["message"]["data"]["value"],
                    "tin_from_setting" => $tinNumberFromSetting["message"]["data"]["value"],
                    "filename" => $this->slugify->slugify($filename)

                );

                $delivery_fee_sum = 0;
                $sub_total_sum = 0;
                $grand_total_sum = 0;
                $commission_sum = 0;
                $tax_sum = 0;
                $total = [];

                $userId = array_unique(array_filter($ordersForCsv->pluck('user_id')->toArray()));
                //return $userId;

                $userDatas["users"] = $userId;
                if (empty($userId) || is_null($userId)) {
                    $userDatasFromOauth = [];
                    goto skipCallingOauth;
                }
                $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list";
                $getUserDetailsForReview = \RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);

                if ($getUserDetailsForReview["status"] != "200") {
                    if ($getUserDetailsForReview['status'] == 503) {
                        return response()->json(
                            $getUserDetailsForReview, $getUserDetailsForReview['status']);
                    }
                    return response()->json(
                        $getUserDetailsForReview["message"], $getUserDetailsForReview['status']);
                }
                $userDatasFromOauth = $getUserDetailsForReview["message"]["data"];
                skipCallingOauth:

                foreach ($ordersForCsv as $key => $order) {
//                    if ($order['payment_method'] != "cash-on-pickup" && $order["payment_status"] != "completed") {
//                        $ordersForCsv->forget($key);
//                    }
                    $restaurant = unserialize($order->restaurant);
//                    $comission_amount =  $order->site_commission_amount;
                    $order->restaurant = $restaurant['translation']['en'];
                    $order->device_type = ucwords($order->device_type);
                    $order->delivery_type = str_replace("-", " ", ucwords($order->delivery_type));

                    $order->payment_method = $order->payment_method == "creditcard" ? "Paytabs" : str_replace("-", " ", ucwords($order->payment_method));
                    $order->order_status = str_replace("-", " ", ucwords($order->order_status));
                    $order->delivery_status = str_replace("-", " ", ucwords($order->delivery_status));
                    $order->payment_status = str_replace("-", " ", ucwords($order->payment_status));
//                    unset($order["site_commission_amount"]);
//                    $order->site_commission_amount = $comission_amount * (1 + $order->site_tax_rate * 0.01);

                    $order->amount = number_format((float)round($order->amount, 2), 2, '.', '');

                    $order->sub_total = round($order->sub_total, 2);
                    $order->tax = number_format((float)round($order->tax, 2), 2, '.', '');
                    $order->hideRejection = true;
                    unset($order["site_tax_rate"]);

                    $notes = $order->orderComments()->select('comment', 'created_at')->get();
                    if (!empty($notes)) {

                        $check = '';
                        foreach ($notes as $note) {

                            $check .= $note['created_at'] . " " . $note['comment'] . PHP_EOL;

                        }
                        $order->note = $check;
                    }
                    if (!empty($notes)) {

                        $check = '';
                        foreach ($notes as $note) {

                            $check .= $note['created_at'] . " " . $note['comment'] . PHP_EOL;

                        }
                        $order->note = $check;
                    }

                    if ($order->delivery_type == 'Home delivery') {
                        foreach ($userDatasFromOauth as $driver) {
                            if ($driver['id'] == $order->user_id) {

                                $order->driver_name = preg_replace('/\s+/', ' ', trim(implode(" ", array($driver['first_name'], $driver['middle_name'], $driver['last_name']))));

                                break;
                            } else {
                                $order->driver_name = "";
                            }
                        }
                    } else {
                        $order->driver_name = "";
                    }
                    $order->makeVisible('restaurant');
                    unset($order->site_tax_rate, $order->user_id);

                    $delivery_fee_sum += $order->delivery_fee;
                    $sub_total_sum += $order->sub_total;
                    $grand_total_sum += $order->amount;
                    $commission_sum += $order->site_commission_amount;
                    $tax_sum += $order->tax;


                }


                $total = [
                    $delivery_fee_sum,
                    $sub_total_sum,
                    $grand_total_sum,
                    $commission_sum,
                    $tax_sum,
                    $ordersForCsv[0]['currency']

                ];

                $data["orders"] = $ordersForCsv;
                $data["total"] = $total;


                return response()->json([
                    "status" => "200",
                    "data" => $data
                ]);

            } else {
                return response()->json([
                    "status" => "404",
                    "message" => "Record(s) could not be found for passed parameters"
                ], 404);
            }
        } catch (\Exception $ex) {
            $this->log->storeLogError([
                "Error downloading invoice",
                [
                    "message" => $ex->getMessage(),
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error downloading invoice"
            ], 500);
        }


    }

    public function generateAllOrdersReports(Request $request)
    {
        try {
            $this->validate($request, [
                "restaurant_id" => "required|integer|min:1",
                "month" => "required|digits:2|min:01|max:12",
                "year" => "required|digits:4"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $date = Carbon::createFromDate($request->year, $request->month, 1, 'utc');

            $bankInfoEnglish = \Settings::getSettings('invoice-bank-info-english');
            if ($bankInfoEnglish["status"] != 200) {
                return response()->json(
                    $bankInfoEnglish["message"]
                    , $bankInfoEnglish["status"]);
            }
            $bankInfoArabic = \Settings::getSettings('invoice-bank-info-arabic');
            if ($bankInfoArabic["status"] != 200) {
                return response()->json(
                    $bankInfoArabic["message"]
                    , $bankInfoArabic["status"]);
            }

            $tinNumberFromSetting = \Settings::getSettings('invoice-lugmety-tin');
            if ($tinNumberFromSetting["status"] != 200) {
                return response()->json(
                    $tinNumberFromSetting["message"]
                    , $tinNumberFromSetting["status"]);
            }
            $phoneNumber = \Settings::getSettings('invoice-lugmety-phone-number');
            if ($phoneNumber["status"] != 200) {
                return response()->json(
                    $phoneNumber["message"]
                    , $phoneNumber["status"]);
            }
            $results = $this->order->getAllReportOfRestaurant($request->restaurant_id, $date);

            $ordersForCsv = collect($this->order->getAllReportOfRestaurantSales($request->restaurant_id, $date));

            $results->makeVisible(["restaurant", "extra_info"]);
            $sales = array(
                'creditcard' => 0,
                'cash-on-pickup' => 0,
                'cash-on-delivery' => 0,
                'sadad' => 0,
                "cash-on-delivery-completed" => 0
            );
            $total_amount = 0;
            if (count($results) !== 0) {
                $site_commission_rate = $results[0]["site_commission_rate"];
                $site_tax_rate = $results[0]["site_tax_rate"];
                /**
                 * calling restaurant service to get restaurant detail
                 */
                $restaurantId = $results[0]->restaurant_id;
                $restaurantBaseUrl = Config::get("config.restaurant_base_url");
                $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$restaurantId/detail?lang=en";

                $tin = "";
                /**
                 *calling restaurant service to check the restaurant exist or not if exist fetch the data
                 */
                $cc_commision_rate = 0;
                $restaurantdata = RemoteCall::getSpecific($restaurantFullUrl);
                if ($restaurantdata['status'] == 200) {
                    $tin = $restaurantdata["message"]["data"]["tin"];
                    $cc_commision_rate = $restaurantdata["message"]["data"]["cc_commision_rate"];
                }

                $restaurantInfo = unserialize($results[0]["restaurant"]);
                $currency = $results[0]["currency"];
                $date = $request->year . "-" . $request->month;
                $RestaurantNameInAr = (isset($restaurantInfo["translation"]["ar"])) ? $restaurantInfo["translation"]["ar"] : "";
                $billTo = (isset($restaurantInfo["translation"]["en"])) ? $restaurantInfo["translation"]["en"] : $restaurantInfo["name"];
                $invoice = strtoupper(substr($billTo, 0, 4)) . "-" . $restaurantInfo["id"] . "-" . $date;
//                if (isset($restaurantInfo["tin"]) && $restaurantdata['status'] != 200) {
//                    $tin = $restaurantInfo["tin"];
//                }
                $total_commission = 0;
                $total_commission_vat = 0;
                $logistics_fee = 0;
                $logistics_fee_vat = 0;
                foreach ($results as $order) {
                    $paytabs = 0;
                    $lugmety_cash_at_restaurant_excluding_vat = 0;
                    if ($order['payment_method'] != "cash-on-pickup" && $order["payment_status"] != "completed") {
                        continue;
                    }
                    if ($order["delivery_type"] == "home-delivery" && $order["payment_status"] == "completed") {
                        $deliveryFee = $order['delivery_fee'];// * (1 + $order['site_tax_rate'] * 0.01);
                        $amount = $order['amount'] - $deliveryFee;
                        if($order['payment_method'] == 'cash-on-pickup'){
                            $lugmety_cash_at_restaurant_excluding_vat = $order['amount'] * ($order['site_commission_rate'] * 0.01);
                        }
                        if($order['payment_method'] == 'creditcard'){
                            $paytabs = $amount;
                        }
                        $commission = $order['site_commission_amount'] + $lugmety_cash_at_restaurant_excluding_vat +  ($cc_commision_rate * 0.01 * $paytabs);
                        $total_commission += $commission;
                        $total_commission_vat += ($commission * $order['site_tax_rate'] * 0.01);


                        $total_amount = $total_amount + $amount;
                        if($order["user_type"] == "restaurant-customer" && (0 == $order['is_normal'])){
                            $logistics_fee += $order["logistics_fee"];
                            $logistics_fee_vat += $logistics_fee * $order['site_tax_rate'] * 0.01;
                        }
                        $sales[$order['payment_method']] = round(($sales[$order['payment_method']] + $amount),2);
                    } else {
                        if ($order["payment_method"] == "cash-on-pickup" && $order["payment_status"] == "completed") {
                            $sales["cash-on-delivery-completed"] = round($sales["cash-on-delivery-completed"] + $order['amount'],2);
                        }
                        if($order["user_type"] == "restaurant-customer" && (0 == $order['is_normal'])){
                            $logistics_fee += $order["logistics_fee"];
                            $logistics_fee_vat += $logistics_fee * $order['site_tax_rate'] * 0.01;
                        }
                        if($order['payment_method'] == 'cash-on-pickup'){
                            $lugmety_cash_at_restaurant_excluding_vat = $order['amount'] * ($order['site_commission_rate'] * 0.01);
                        }
                        if($order['payment_method'] == 'creditcard'){
                            $paytabs = $order["amount"];
                        }
                        $commission = $order['site_commission_amount'] + $lugmety_cash_at_restaurant_excluding_vat +  ($cc_commision_rate * 0.01 * $paytabs);
                        $total_commission += $commission;
                        $total_commission_vat += ($commission * $order['site_tax_rate'] * 0.01);
                        $total_amount = $total_amount + $order['amount'];

                        $sales[$order['payment_method']] = round(($sales[$order['payment_method']] + $order['amount']),2);
                    }

                }



//                $cashOnPickupCompleted = round($sales['cash-on-delivery-completed'], 2);
//                $cashOnPickUp = $cashOnPickupCompleted;//round($sales['cash-on-pickup'], 2);
                $sadad = round($sales["sadad"], 2);
                $cashOnPickUp = round($sales['cash-on-pickup'], 2);
                $subTotal = round($sadad + $sales['cash-on-delivery'] + $sales['creditcard']);
                $paytabs = round($sales['creditcard'], 2);
//                $card_issuer_fees = round($cc_commision_rate * 0.01 * $paytabs, 2);
                $cashOnDelivery = round($sales['cash-on-delivery'], 2);
//            $lugmety_cash_at_restaurant = round(($cashOnPickupCompleted) * ($site_commission_rate / 100), 2);


                // where 2.5 is credit card processing fee
                $total_credit = round($subTotal - $total_commission - $total_commission_vat  - $logistics_fee - $logistics_fee_vat, 2);

                if ($results[0]["country_id"] == 1) {
                    $currency1 = "ريال";
                } else {
                    $currency1 = $currency;
                }
                $is_empty_logistic_fee = false;
                if($logistics_fee > 0){
                    $is_empty_logistic_fee = true;
                }
                $total_amount = number_format((float)$total_amount,2);
                $filename = $billTo;
                $filename = "$filename-$invoice";
                $cashOnPickUp = number_format((float)$cashOnPickUp,2);
                $cashOnDelivery = number_format((float)$cashOnDelivery,2);
                $paytabs = number_format((float)$paytabs,2);
                $sadad = number_format((float)$sadad,2);
                $total_credit = number_format((float)$total_credit,2);
                $logistics_fee = number_format($logistics_fee,2);
                $logistics_fee_vat = number_format($logistics_fee_vat,2);
                $total_commission_vat = number_format($total_commission_vat,2);
                $total_commission = number_format($total_commission,2);
                $data = array(
                    "invoice" => $invoice,
                    'tin' => $tin,
                    "bill_to" => "$billTo",
                    "restaurant_name_arabic" => $RestaurantNameInAr,
                    "for" => "Online Delivery Logistic Services",
                    "total_sales" => $currency . "$total_amount",
                    "total_sales_ar" => "$total_amount " . $currency1,
                    'cash_on_pickup' => $currency . "$cashOnPickUp",
                    'cash_on_pickup_ar' => "$cashOnPickUp " . $currency1,
                    'cash_on_delivery' => $currency . "$cashOnDelivery",
                    'cash_on_delivery_ar' => "$cashOnDelivery " . $currency1,
                    'online_card_payments' => $currency . "$paytabs",
                    'online_card_payments_ar' => "$paytabs " . $currency1,

                    "subtotal" => $currency . "$subTotal",
                    "subtotal_ar" => "$subTotal " . $currency1,
                    "sadad" => $currency . "$sadad",
                    "sadad_ar" => "$sadad " . $currency1,
                    "lugmety_phone_number" => $phoneNumber["message"]["data"]["value"],

                    "lugmety_commission" => $currency . "$total_commission",
                    "lugmety_commision_vat_title_in_ar" => "القيمة المُضافة",
                    "lugmety_commission_vat_en" =>$currency. $total_commission_vat,
                    "lugmety_commission_vat_ar" => $total_commission_vat . "".$currency1,
                    "lugmety_commission_ar" => "$total_commission " . $currency1,

                    'lugmety_commission_rate' => "$site_commission_rate",
                    'lugmety_commission_rate_ar' => "( % " . str_replace(".", " , ", "$site_commission_rate") . " ) " . "عمولة لقمتي",
                    "lugmety_commission_held_restaurant_ar" => "( % " . str_replace(".", " , ", "$site_commission_rate") . " ) " . "عمولة لقمتي المقبوضة كاش من المطعم",

                    "total_credit" => $currency . "$total_credit",
                    "total_credit_ar" => "$total_credit " . $currency1,
                    "bank_info_english" => $bankInfoEnglish["message"]["data"]["value"],
                    "bank_info_arabic" => $bankInfoArabic["message"]["data"]["value"],
                    "tin_from_setting" => $tinNumberFromSetting["message"]["data"]["value"],

                    'lugmety_total_logistics_fee_en' => $currency . $logistics_fee,
                    'lugmety_total_logistics_fee_ar' =>  $logistics_fee . $currency1,
                    'lugmety_total_logistics_fee_vat_en' => $currency . $logistics_fee_vat,
                    'lugmety_total_logistics_fee_vat_ar' => $logistics_fee_vat . $currency1,
                    'is_empty_logistic_fee' => $is_empty_logistic_fee


                );

                //csv
                $delivery_fee_sum = 0;
                $sub_total_sum = 0;
                $grand_total_sum = 0;
                $commission_sum = 0;
                $tax_sum = 0;
                $logistics_fee_sum = 0;
                $total = [];

                $userId = array_unique(array_filter($ordersForCsv->pluck('user_id')->toArray()));
                //return $userId;

                $userDatas["users"] = $userId;
                if (empty($userId) || is_null($userId)) {
                    $userDatasFromOauth = [];
                    goto skipCallingOauth;
                }
                $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list";
                $getUserDetailsForReview = \RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);

                if ($getUserDetailsForReview["status"] != "200") {
                    if ($getUserDetailsForReview['status'] == 503) {
                        return response()->json(
                            $getUserDetailsForReview, $getUserDetailsForReview['status']);
                    }
                    return response()->json(
                        $getUserDetailsForReview["message"], $getUserDetailsForReview['status']);
                }
                $userDatasFromOauth = $getUserDetailsForReview["message"]["data"];
                skipCallingOauth:

                foreach ($ordersForCsv as $key => $order) {
//                    if ($order['payment_method'] != "cash-on-pickup" && $order["payment_status"] != "completed") {
//                        $ordersForCsv->forget($key);
//                    }
                    $restaurant = unserialize($order->restaurant);
                    $comission_amount =  $order->site_commission_amount;
                    $order->restaurant = $restaurant['translation']['en'];
                    $order->device_type = ucwords($order->device_type);
                    $order->delivery_type = str_replace("-", " ", ucwords($order->delivery_type));

                    $order->payment_method = $order->payment_method == "creditcard" ? "Paytabs" : str_replace("-", " ", ucwords($order->payment_method));
                    $order->order_status = str_replace("-", " ", ucwords($order->order_status));
                    $order->delivery_status = str_replace("-", " ", ucwords($order->delivery_status));
                    $order->payment_status = str_replace("-", " ", ucwords($order->payment_status));
                    $order->site_commission_amount = $comission_amount;

                    $order->amount = number_format((float)round($order->amount, 2), 2, '.', '');
//                    $order->hideRejection = true;
                    $order->sub_total = round($order->sub_total, 2);
                    $order->tax = number_format((float)round($order->tax, 2), 2, '.', '');

                    unset($order["site_tax_rate"]);
                    $order->addHidden("rejection_reason");
                    $notes = $order->orderComments()->select('comment', 'created_at')->get();
                    if (!empty($notes)) {

                        $check = '';
                        foreach ($notes as $note) {

                            $check .= $note['created_at'] . " " . $note['comment'] . PHP_EOL;

                        }
                        $order->note = $check;
                    }
                    if (!empty($notes)) {

                        $check = '';
                        foreach ($notes as $note) {

                            $check .= $note['created_at'] . " " . $note['comment'] . PHP_EOL;

                        }
                        $order->note = $check;
                    }

                    if ($order->delivery_type == 'Home delivery') {
                        foreach ($userDatasFromOauth as $driver) {
                            if ($driver['id'] == $order->user_id) {

                                $order->driver_name = preg_replace('/\s+/', ' ', trim(implode(" ", array($driver['first_name'], $driver['middle_name'], $driver['last_name']))));

                                break;
                            } else {
                                $order->driver_name = "";
                            }
                        }
                    } else {
                        $order->driver_name = "";
                    }
                    $order->makeVisible('restaurant');
                    unset($order->site_tax_rate, $order->user_id);

                    $delivery_fee_sum += $order->delivery_fee;
                    $sub_total_sum += $order->sub_total;
                    $grand_total_sum += $order->amount;
                    $commission_sum += $order->site_commission_amount;
                    $tax_sum += $order->tax;
                    $logistics_fee_sum += $order->logistics_fee;


                }


                $total = [
                    $delivery_fee_sum,
                    $sub_total_sum,
                    $grand_total_sum,
                    $commission_sum,
                    $tax_sum,
                    $ordersForCsv[0]['currency'],
                    $logistics_fee_sum

                ];

                $fileNameInSlug = $this->slugify->slugify($filename);
                $this->excel->createSalesReport($ordersForCsv, $total, $fileNameInSlug);
                ob_clean();
                $html2pdf = new Mpdf(['tempDir' => storage_path() . "/exports"]);
                $content = view("invoice-en")->with(["data" => $data]);

                $html2pdf->autoScriptToLang = true;
                $html2pdf->autoLangToFont = true;
                $html2pdf->WriteHTML($content);
                $html2pdf->output(storage_path() . "/exports/invoice-en" . ".pdf");
                ob_clean();
                $html2pdf1 = new Mpdf(['tempDir' => storage_path() . "/exports"]);
                $html2pdf1->autoScriptToLang = true;
                $html2pdf1->autoLangToFont = true;
                $contentInArabic = view("invoice-ar")->with(["data" => $data]);
                $html2pdf1->writeHTML($contentInArabic);

                $html2pdf1->output(storage_path() . "/exports/invoice-ar" . ".pdf");
                if (is_dir(storage_path() . '/exports/ttfontdata')) {
                    array_map('unlink', glob(storage_path() . '/exports/ttfontdata/*'));
                    rmdir(storage_path() . '/exports/ttfontdata');
                }
                $zipper = new Zipper();
                $files = glob(storage_path() . '/exports/*');
                $zipper->make(storage_path("/exports/$fileNameInSlug.zip"))->add($files)->close();
                if (file_exists(storage_path("/exports/$fileNameInSlug.zip"))) {
                    array_map('unlink', glob(storage_path() . '/exports/*.pdf'));
                    array_map('unlink', glob(storage_path() . '/exports/*.csv'));
                    return response()->download(storage_path("/exports/$fileNameInSlug.zip"))->deleteFileAfterSend(true);
                }

            } else {
                return response()->json([
                    "status" => "404",
                    "message" => "Record(s) could not be found for passed parameters"
                ], 404);
            }
        } catch (\Exception $ex) {
            $this->log->storeLogError([
                "Error downloading invoice",
                [
                    "message" => $ex->getMessage(),
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error downloading invoice"
            ], 500);
        }

    }

    public function assignOrderToAdmin($id){
        try{
            $adminId = RoleChecker::getUser();
            $adminName = RoleChecker::getUserName();
            $getOrder = $this->order->getSpecificOrder($id);
            $updateOrder["admin_name"] = $adminName;
            if($adminId == $getOrder->admin_id){
                return response()->json([
                    "status" => "409",
                    "message" => "You have already assigned to this order."
                ],409);
            }
            $updateOrder["admin_id"] = $adminId;
            if(empty($getOrder->customer_care_history)){
                $customerCareHistory[0] =[
                    "admin_id" => $adminId,
                    "date_utc" => Carbon::now('utc')->toDateTimeString()
                ];
                $updateOrder["customer_care_history"] = serialize($customerCareHistory);
            }
            else{
                $customerCareHistory = unserialize($getOrder->customer_care_history);
                $countIndex = count($customerCareHistory);
                $customerCareHistory[$countIndex] = [
                    "admin_id" => $adminId,
                    "date_utc" => Carbon::now('utc')->toDateTimeString()
                ];
                $updateOrder["customer_care_history"] = serialize($customerCareHistory);
            }
            $this->order->updateOrder($id,$updateOrder);
            return response()->json([
                "status" => "200",
                "message" => "You have successfully assigned to this order."
            ]);

        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Order could not be found."
            ],404);
        }

        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ],500);
        }
    }


    /**
     * deletes specified order softly or permanent way
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            DB::beginTransaction();
            /**
             * this method will display specific order wheather the order is soft deleted or not
             * @param $id
             * @returns $orders
             * */
            $order = $this->order->getSpecificSoftDeletedOrNotSoftDeletedOrder($id);
            /**
             * if the deleted_at row of order table is  null then the order is soft deleted
             *
             * */
            if (is_null($order->deleted_at)) {
                /**
                 * this deleteOrderSoft method will use soft delete parent row if the soft delete features
                 * is used */
                $this->order->deleteOrderSoft($order);
                $this->log->storeLogInfo(array("Order Deleted", [
                    "status" => "404",
                    "message" => "Order soft Deleted",
                    "data" => $order
                ]));
            } /**
             * if the deleted_at row of order table is not null then the order and all its associated child
             * rows are hard deleted
             * */
            else {
                /**
                 * hard delete this will delete all its child rows as well its parent row
                 */
                $this->order->deleteOrderHard($order);
                $this->log->storeLogInfo(array("Order Deleted", [
                    "status" => "404",
                    "message" => "Order hard Deleted",
                    "data" => $order
                ]));

            }

            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Order Deleted Successfully"
            ], 200);
        } catch (ModelNotFoundException $ex) {
            $this->log->storeLogError(array("Error in query", [
                "status" => "404",
                "message" => "Requested Order not found"
            ]));

            return response()->json([
                "status" => "404",
                "message" => "Requested Order not found"
            ], 404);
        } catch (\Exception $ex) {

            $this->log->storeLogError(array("Error in query", [
                "status" => "500",
                "message" => "Error Deleting order"
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error Deleting order "
            ], 500);
        }
    }


    private function getOrderStatusTransaction($orderStatus, $allStatus, $lang)
    {
        /**
         * gets order status from order status table based on slug order_status from order table
         */

        $orderStatus = collect($allStatus)->where("slug", $orderStatus)->first();

        /*
         * if order_status is not present then the value stored in order.order_status column in displayed
         */
        if (!$orderStatus) return false;
        $orderStatus = $orderStatus->toArray();
//        dd($orderStatus["order_status_translations"]);
        $orderLanguageTransation = $orderStatus["order_status_translations"];
        $orderLanguage = collect($orderLanguageTransation);
        $orderLanguage = $orderLanguage->where("lang_code", $lang)->first();
        /**
         *
         */
        if (!$orderLanguage) {
            $orderLanguage = $orderLanguage->where("lang_code", "en")->first();
            if (!$orderLanguage) return false;
        }
        return $orderLanguage["name"];

    }

    private function getPaymentStatusTransaction($paymentStatus, $allStatus, $lang)
    {
        /**
         * gets payment status from payment status table based on slug payment_status from payment table
         */
        $paymentStatus = collect($allStatus)->where("slug", $paymentStatus)->first();
        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (!$paymentStatus) return false;
        $paymentStatus = $paymentStatus->toArray();
        $languageTransation = $paymentStatus["payment_status_translations"];
        $language = collect($languageTransation);
        $paymentLanguage = $language->where("lang_code", $lang)->first();
        /**
         *
         */
        if (!$paymentLanguage) {
            $paymentLanguage = $language->where("lang_code", "en")->first();
            if (!$paymentLanguage) return false;
        }
        return $paymentLanguage["name"];

    }

    private function calculateShipping($request)
    {
        try {
            $shippingMethod = $this->shippingMethod->getSpecificShippingMethodBySlug($request["restaurant"]["shipping_method"]);
            if ($shippingMethod->count() == 0) {
                throw new ModelNotFoundException();
            }
            $class = $shippingMethod[0]["class"];


            $name = "\\App\\Http\\Controllers\\ShippingClass\\" . $class;

            if (!class_exists($name)) {
                throw new \Exception();
            }
            $shipping = new  $name;
            if ($shippingMethod[0]['is_taxable'] == 1) {
                //$shipping->setIsTaxable(1);
                // $shipping->setTaxPercentage($request['tax_percentage']);
            }
            if ($shippingMethod[0]['shipping_type'] == "dynamic_distance") {
                $shipping->setMinDistance($request['location']['min_distance']);
                $shipping->setMinCost($request['location']['min_cost']);
                $shipping->setPerCost($request['location']['per_cost']);
                $shipping->setDistance($request['distance']);
            } else if ($shippingMethod[0]['shipping_type'] == "fixed") {
                $shipping->setShippingCost($shippingMethod[0]['shipping_cost']);
            }
            return response()->json([
                'status' => '200',
                'data' => $shipping->index()
            ], 200);

        } catch (ModelNotFoundException $ex) {
            $this->log->storeLogInfo(array("Shipping Method", [
                'status' => '404',
                'message' => 'Shipping Method  Not found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => 'Shipping Method  Not found'
            ], 404);
        } catch (\Exception $ex) {
            $this->log->storeLogInfo(array("Shipping Method", [
                'status' => '404',
                'message' => 'Shipping Method Class Not found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => 'Shipping Method Class Not found'
            ], 404);
        }

    }

    private function calculateShippingForRestaurantCustomer($request)
    {
        try {
            $shippingMethod = $this->shippingMethod->getSpecificShippingMethodBySlug($request["restaurant"]["shipping_method"]);
            if ($shippingMethod->count() == 0) {
                throw new ModelNotFoundException();
            }
            $class = $shippingMethod[0]["class"];
            if($class == "RateByDistanceShippingMethod") $class = "FlatRateShippingMethod";

            $name = "\\App\\Http\\Controllers\\ShippingClass\\" . $class;

            if (!class_exists($name)) {
                throw new \Exception();
            }
            $shipping = new  $name;
            if ($shippingMethod[0]['is_taxable'] == 1) {
                //$shipping->setIsTaxable(1);
                // $shipping->setTaxPercentage($request['tax_percentage']);
            }
            if ($shippingMethod[0]['shipping_type'] == "dynamic_distance") {
                $getSetting = Settings::getSettings('customer-restaurant-fixed-delivery-rate');
                if ($getSetting["status"] == "200") {
                    $deliveryAmount = (int)$getSetting["message"]["data"]["value"];
                } else {
                    $this->log->storeLogInfo(array("Shipping Method", [
                        'status' => '404',
                        'message' => 'Delivery Rate  Not found'
                    ]));
                    return response()->json([
                        'status' => '404',
                        'message' => 'Delivery Rate  Not found'
                    ], 404);
                }
                $shipping->setShippingCost($deliveryAmount);
            } else if ($shippingMethod[0]['shipping_type'] == "fixed") {
                $shipping->setShippingCost($shippingMethod[0]['shipping_cost']);
            }
            return response()->json([
                'status' => '200',
                'data' => $shipping->index()
            ], 200);

        } catch (ModelNotFoundException $ex) {
            $this->log->storeLogInfo(array("Shipping Method", [
                'status' => '404',
                'message' => 'Shipping Method  Not found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => 'Shipping Method  Not found'
            ], 404);
        } catch (\Exception $ex) {
            $this->log->storeLogInfo(array("Shipping Method", [
                'status' => '404',
                'message' => 'Shipping Method Class Not found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => 'Shipping Method Class Not found'
            ], 404);
        }

    }

    /**
     * this endpoints will change the delivery status of delivery type pickup to delivered if the payment status is completed
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePickedUpOrder(Request $request){

        try{
            $this->validate($request,[
                'hash' => 'required|string'
            ]);
        }

        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try {
            //$2y$12$DVJGLbj4pNg62z98Zl6ytuWlMFu4rLMX1Ie.w6tFnAN.YwpZarYX2
            if(!password_verify('pickup#',$request->hash)){
                return response()->json([
                    'status' => '403',
                    'message' => 'Invalid Hash'
                ],403);
            }
            set_time_limit(0);
            $getAllNonDeliveredPickupOrder = Orders::
            where([
                ["order_status", "accepted"],
                ["delivery_type", "pickup"],
                ["payment_method", "cash-on-pickup"],
                ["api_delivery_time", "!=", ""],
                ["delivery_status","!=","delivered"],
            ])
                ->orWhere([
                    ["order_status", "accepted"],
                    ["delivery_type", "pickup"],
                    ["api_delivery_time", "!=", ""],
                    ["payment_method", "creditcard"],
                    ["payment_status", "completed"],
                    ["delivery_status","!=","delivered"],
                ])->orderBy("id","desc")->get();

            Log::info("Pickup cron",[
                "total_order" => count($getAllNonDeliveredPickupOrder)
            ]);

            foreach ($getAllNonDeliveredPickupOrder as $order) {
                if ($order->payment_method == "creditcard") {
                    $apiDeliveryTime = Carbon::parse($order->api_delivery_time, $order->time_zone_utc);
                    $currentTime = Carbon::now($order->time_zone_utc);
                    $timeDifference = $apiDeliveryTime->diffInMinutes($currentTime);
                    if ($timeDifference >= 10) {
                        $order->update([
                            "delivery_status" => "delivered"
                        ]);
                    }
                } else {
                    $apiDeliveryTime = Carbon::parse($order->api_delivery_time, $order->time_zone_utc);
                    $currentTime = Carbon::now($order->time_zone_utc);
                    $timeDifference = $apiDeliveryTime->diffInMinutes($currentTime);
                    if ($timeDifference >= 10) {
                        $order->update([
                            "delivery_status" => "delivered",
                            "payment_status" => "completed"
                        ]);
                    }
                }
            }
            DB::commit();

            return response()->json([
                'status' => '200',
                'message' => 'Successfully completed pickup cron'
            ]);
        }
        catch (\Exception $ex){
            Log::error("Pickup cron error",[
                "total_order" => count($getAllNonDeliveredPickupOrder),
                "message" => $ex->getMessage()
            ]);
        }

    }


    public function updateCustomerName(Request $request)
    {
        $min_userId = $request->min_user;
        $max_userId = $request->max_user;
        set_time_limit(300000);
        $getAllOrders = $this->order->getAllOrderList($min_userId, $max_userId);
        $getALlUserList["users"] = array_values(array_unique($getAllOrders->pluck("customer_id")->all()));
        $fullUrl = $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list";
        $getUserDetailsForReview = \RemoteCall::storeWithoutOauth($oauthUrl, $getALlUserList);
        if ($getUserDetailsForReview["status"] != "200") {
            if ($getUserDetailsForReview['status'] == 503) {
                return response()->json(
                    $getUserDetailsForReview, $getUserDetailsForReview['status']);
            }
            return response()->json(
                $getUserDetailsForReview["message"], $getUserDetailsForReview['status']);
        }
        $userDatasFromOauth = collect($getUserDetailsForReview["message"]["data"])->sortByDesc("id");
        foreach ($getAllOrders as $order) {
            if (!empty($order->customer_name)) continue;
            foreach ($userDatasFromOauth as $user) {
                if ($order["customer_id"] == $user["id"]) {
                    $customerName = preg_replace('/\s+/', ' ', trim(implode(" ", array($user['first_name'], $user['middle_name'], $user['last_name']))));
                    $order->update(["customer_name" => $customerName]);
                    break;
                } else {
                    continue;
                }
            }
        }

    }

    public function checkRedis()
    {
        $hash = sha1(uniqid());
        $key = "identifier:" . "1234";
        try {
            Redis::hmset($key, [
                'hash' => $hash,
                'created_at' => time(),
                'updated_at' => time()
            ]);
            return response()->json([
                "status" => "200",
                "message" => "redis is  running"

            ], 200);
        } catch (\RedisException $e) {
            return response()->json([
                "status" => "503",
                "message" => "redis is no running"

            ], 503);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "503",
                "message" => "redis is no running"

            ], 503);
        }
    }

//        try
//        {
//            $redis = \Redis::instance();
//
//        }
//        catch(\RedisException $e)
//        {
//            return response()->json([
//                "status" => "503",
//                "message" => "redis is no running"
//
//            ],503);
//        }
//    }

    private function checkOrderStatus($order)
    {

        if (($order->delivery_type == "creditcard") || ($order->delivery_type == "sadad")) {
            if ($order->payment_status == "completed") {
                return true;
            }
        } else if ($order->delivery_type == "cash-on-delivery") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Translate restaurant name into passed param $lang
     * @param $resData
     * @param $lang
     * @return string
     */
    private function restaurantTranslate($resData, $lang)
    {
        if (isset($resData['en'])) {
            $resName = isset($resData[$lang]) ? $resData[$lang] : $resData['en'];
        } else {
            $resName = '';
        }
        return $resName;
    }

    private function getDeliveryStatusTransaction($deliveryStatus, $allStatus, $lang)
    {
        /**
         * gets payment status based on slug payment_status from payment table
         */
        $deliveryStatus = collect($allStatus)->where("slug", $deliveryStatus)->first();
        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (!$deliveryStatus) return false;
        $deliveryStatus = $deliveryStatus->toArray();
        $language = collect($deliveryStatus["delivery_status_translations"]);
        $deliveryLanguage = $language->where("lang_code", $lang)->first();
        /**
         *
         */
        if (!$deliveryLanguage) {
            $deliveryLanguage = $language->where("lang_code", "en")->first();
            if (!$deliveryLanguage) return false;
        }
        return $deliveryLanguage["name"];

    }

    private function getPaymentMethodTranslation($paymentMethod, $allStatus, $lang)
    {
        /**
         * gets payment method based on slug payment_status from payment table
         */
        $payment_status = collect($allStatus)->where("slug", $paymentMethod)->first();

        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (!$payment_status) return false;
        $payment_status = $payment_status->toArray();
        $language = collect($payment_status["payment_method_translation"]);
        $deliveryLanguage = $language->where("lang", $lang)->first();
        /**
         *
         */
        if (!$deliveryLanguage) {
            $deliveryLanguage = $language->where("lang", "en")->first();
            if (!$deliveryLanguage) return false;
        }
        return $deliveryLanguage["name"];
    }

    private function getDeliverytMethodTranslation($deliveryMethod, $allStatus, $lang)
    {
        /**
         * gets payment status based on slug payment_status from payment table
         */
        $deliveryStatus = collect($allStatus)->where("slug", $deliveryMethod)->first();
        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (!$deliveryStatus) return false;
        $deliveryStatus = $deliveryStatus->toArray();
        $language = collect($deliveryStatus["shipping_method_translation"]);
        $deliveryLanguage = $language->where("lang_code", $lang)->first();
        /**
         *
         */
        if (!$deliveryLanguage) {
            $deliveryLanguage = $language->where("lang_code", "en")->first();
            if (!$deliveryLanguage) return false;
        }
        return $deliveryLanguage["name"];
    }

    public function customerStats(Request $request, $customer_id)
    {

//        $userId = \RoleChecker::getUser();
//        $lang = Input::get("lang", "en");
        $request['country_id'] = Input::get("country_id", null);
        /**
         * if limit param is other than integer and validation fails then limit is set to 10
         */
        if (!is_null($request['country_id'])) {
            $rules = [
                "country_id" => "integer|min:1"];
            try {
                $validator = $this->validate($request, $rules);
            } catch (\Exception $ex) {
                $request['country_id'] = null;
            }
        }

        /**
         * gets orders based on limit and userId
         * */
        $customerOrders = $this->order->getOrdersStatsOfCustomer($customer_id, $request['country_id']);
        foreach ($customerOrders as $customerOrder) {
            $districts['districts'] = [];
            $districts['lang'] = $request['lang'];
            $districts['country_id'] = $customerOrder['country_id'];
            $url = Config::get("config.location_base_url");
            $country = RemoteCall::getSpecific($url . "/public/country/" . $customerOrder['country_id']);
            if ($country['message']['status'] == "200") {
                $countryData = $country['message']['data'];
                $customerOrder['currency_code'] = $countryData['currency_code'];
                $customerOrder['currency_symbol'] = $countryData['currency_symbol'];
                foreach ($countryData['translation'] as $translate) {
                    if ($translate['lang'] == "en") {
                        $customerOrder['name'] = $translate['name'];
                    }
                }
            }


        }

        return response()->json([
            "status" => "200",
            "data" => $customerOrders
        ]);
    }

    /**
     * process datatable object
     * @param $ordersDatas
     * @param $lang
     * @return mixed
     */
    private function processDatatableRecord($ordersDatas, $lang, $is_branch_operator = false,$is_admin= false,$timeLimit ="")
    {
//        $datatables = app('datatables');
//        $datatable = $datatables->eloquent($ordersDatas)->make(true);
//        $orders = [];
//        $orders_dt = $datatable->getData();
//                return $da
//        if( count( $orders_dt->data ) ){
        $currentTimeForRespectiveCountry = Carbon::now($ordersDatas[0]["time_zone_utc"]);

        $addTimeForCurrentTime = $currentTimeForRespectiveCountry->addMinute($timeLimit);
        foreach ($ordersDatas as $order) {
            $order->setDefaultLanguage = $lang;
            if (count($order) == 0) continue;
//                $order = json_decode(json_encode($order), true);
            $refunds = $order->partialRefunds;
            if (count($refunds) == 0) {
                $order["is_adjustment"] = false;
            } else {
                $order["is_adjustment"] = true;
            }
            $order->makeVisible(["restaurant","admin_id","admin_name","delivery_time"]);
            if($is_admin && $order["delivery_type"] == "home-delivery" && !empty($order["delivery_time"])){
                $deliveryTime = Carbon::parse($order["delivery_time"],$ordersDatas[0]["time_zone_utc"]);
                if(($addTimeForCurrentTime->lte($deliveryTime) || $addTimeForCurrentTime->gte($deliveryTime)) && ($order["delivery_status"] == "picked" || $order["delivery_status"] == "assigned" )){
                    $order["is_critical_delivery_order"] = true;
                }
            }
            $orderStatusLanguage = $this->getOrderStatusTransaction($order["order_status"], $this->allOrderStatus, $lang);
            if ($orderStatusLanguage) {
                $order["order_status"] = $orderStatusLanguage;
            }
            $paymentStatusLanguage = $this->getPaymentStatusTransaction($order["payment_status"], $this->allPaymentStatus, $lang);
            if ($paymentStatusLanguage) {
                $order["payment_status"] = $paymentStatusLanguage;
            }
            $deliveryStatusLanguage = $this->getDeliveryStatusTransaction($order["delivery_status"], $this->allDeliveryStatus, $lang);
            if ($deliveryStatusLanguage) {
                $order["delivery_status"] = $deliveryStatusLanguage;
            }
            $paymentMethod = $this->getPaymentMethodTranslation($order["payment_method"], $this->allPaymentMethod, $lang);
            if ($paymentMethod) {
                $order["payment_method"] = $paymentMethod;
            }
            $shippingMethod = $this->getDeliverytMethodTranslation($order["delivery_type"], $this->allShippingMethod, $lang);
            if ($shippingMethod) {
                $order["delivery_type"] = $shippingMethod;
            }
//

//                $order["restaurant"] = str_replace("&quot;",'"',$order["restaurant"]);

            $restaurantData = unserialize($order["restaurant"]);
            $order["main_restaurant_id"] = $restaurantData["parent_id"];
            $order["order_date"] = Carbon::parse($order->created_at)->tz($restaurantData["timezone"]);
            $order["restaurant_name"] = '';
            if ($restaurantData || is_null($restaurantData)) {
                $order["restaurant_name"] = $this->restaurantTranslate($restaurantData["translation"], $lang);
            }
//            unset($order["restaurant"],$order["extra_info"]);
            $order->makeHidden(["restaurant", "extra_info"]);
            $order["amount"] = number_format((float)round($order["amount"], 2), 2, '.', '');

            $order["amount"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $order["amount"]);
//                $orders[] = (object)$order;
        }
        if ($is_branch_operator) {
            return $ordersDatas;
        }
        return Datatables::of($ordersDatas)->make(true);
    }

    private function processDatatableRecordPagination($ordersDatas, $lang, $is_branch_operator = false,$is_admin=false,$timeLimit= "")
    {
        $currentTimeForRespectiveCountry = Carbon::now($ordersDatas[0]["time_zone_utc"]);


        $addTimeForCurrentTime = $currentTimeForRespectiveCountry->addMinute($timeLimit);
        foreach ($ordersDatas as $order) {

            $order->makeVisible(["restaurant","admin_id","admin_name","delivery_time"]);

            $order->setDefaultLanguage = $lang;
            $order["is_critical_delivery_order"] = false;
            if (count($order) == 0) continue;
//                $order = json_decode(json_encode($order), true);
            $refunds = $order->partialRefunds;
            if (count($refunds) == 0) {
                $order["is_adjustment"] = false;
            } else {
                $order["is_adjustment"] = true;
            }
            if($is_admin && $order["delivery_type"] == "home-delivery" && !empty($order["delivery_time"])){
                $deliveryTime = Carbon::parse($order["delivery_time"],$ordersDatas[0]["time_zone_utc"]);
                if(($addTimeForCurrentTime->gte($deliveryTime) || $currentTimeForRespectiveCountry->gt($deliveryTime)) && ($order["delivery_status"] == "picked" || $order["delivery_status"] == "assigned" )){
                    $order["is_critical_delivery_order"] = true;
                }
            }

            $orderStatusLanguage = $this->getOrderStatusTransaction($order["order_status"], $this->allOrderStatus, $lang);
            if ($orderStatusLanguage) {
                $order["order_status"] = $orderStatusLanguage;
            }
            $paymentStatusLanguage = $this->getPaymentStatusTransaction($order["payment_status"], $this->allPaymentStatus, $lang);
            if ($paymentStatusLanguage) {
                $order["payment_status"] = $paymentStatusLanguage;
            }
            $deliveryStatusLanguage = $this->getDeliveryStatusTransaction($order["delivery_status"], $this->allDeliveryStatus, $lang);
            if ($deliveryStatusLanguage) {
                $order["delivery_status"] = $deliveryStatusLanguage;
            }
            $paymentMethod = $this->getPaymentMethodTranslation($order["payment_method"], $this->allPaymentMethod, $lang);
            if ($paymentMethod) {
                $order["payment_method"] = $paymentMethod;
            }
            $shippingMethod = $this->getDeliverytMethodTranslation($order["delivery_type"], $this->allShippingMethod, $lang);
            if ($shippingMethod) {
                $order["delivery_type"] = $shippingMethod;
            }
//

//                $order["restaurant"] = str_replace("&quot;",'"',$order["restaurant"]);

            $restaurantData = unserialize($order["restaurant"]);
            $order["main_restaurant_id"] = $restaurantData["parent_id"];
            $order["order_date"] = Carbon::parse($order->created_at)->tz($restaurantData["timezone"]);
            $order["restaurant_name"] = '';
            if ($restaurantData || is_null($restaurantData)) {
                $order["restaurant_name"] = $this->restaurantTranslate($restaurantData["translation"], $lang);
            }
//            unset($order["restaurant"],$order["extra_info"]);
            if(isset($order["user_type"] ) && ($order["user_type"] == "restaurant-customer") && (0 == $order['is_normal'])){
                $order["amount"] = $order["amount"] - $order["delivery_fee"];
                $order["delivery_fee"] = 0;
            }
            $order->makeHidden(["restaurant", "extra_info"]);
            $order["amount"] = number_format((float)round($order["amount"], 2), 2, '.', '');

            $order["amount"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $order["amount"]);
//                $orders[] = (object)$order;
        }
        if ($is_branch_operator) {
            return $ordersDatas;
        }
        return $ordersDatas;
    }

    private function calculateDeliveryTime($order, $request, $getThresholdDeliveryTime)
    {

        if($order["delivery_type"] == "pickup"){
            $getThresholdDeliveryTime = 0;
        }
        $order->makeVisible(["restaurant", "delivery_time"]);
        $timeZone = unserialize($order->restaurant)["timezone"];
        if (empty($order["delivery_time"]) || is_null($order["delivery_time"])) {
            $oldDeliveryTime = Carbon::createFromFormat("Y-m-d H:i:s", $request->api_delivery_time, $timeZone)->addMinutes($getThresholdDeliveryTime);
        } else {
            $oldDeliveryTime = Carbon::createFromFormat("Y-m-d H:i:s", $order["delivery_time"], $timeZone);

        }
//        if ($order->pre_order == "1" && !is_null($order->delivery_or_pickup_time)) {
//
////            $oldDeliveryTime = Carbon::createFromFormat("Y-m-d H:i:s",$order["delivery_or_pickup_time"],$timeZone);
//            if (empty($order["threshold_delivery_time"]) || is_null($order["threshold_delivery_time"])) {
//                $final_delivery_time = $oldDeliveryTime->addMinutes($getThresholdDeliveryTime);
//            } else {
//                $oldDeliveryTime = $oldDeliveryTime->subMinutes($order["threshold_delivery_time"]);
//                $final_delivery_time = $oldDeliveryTime->addMinutes($getThresholdDeliveryTime);
//            }
//        } else {
//            if ((empty($order["threshold_delivery_time"]) || is_null($order["threshold_delivery_time"])) && (empty($order["api_delivery_time"]) || is_null($order["api_delivery_time"]))) {
//                $newApiDeliveryTime = Carbon::createFromFormat("Y-m-d H:i:s", $request->api_delivery_time, $timeZone);
//                $difference = $oldDeliveryTime->diffInMinutes($newApiDeliveryTime, false);
//                $final_delivery_time = $oldDeliveryTime->addMinutes($difference)->addMinutes($getThresholdDeliveryTime);
//
//
//            } else {
        $thresholdDeliveryTime = (empty($order["threshold_delivery_time"]) || is_null($order["threshold_delivery_time"])) ? 0 : $order["threshold_delivery_time"];
        $removingPreviousThresholdDeliveryTime = $oldDeliveryTime->subMinutes($thresholdDeliveryTime);
        if (empty($order["api_delivery_time"]) || is_null($order["api_delivery_time"])) {
            $oldDeliveryTime = $removingPreviousThresholdDeliveryTime;
        } else {
            $oldApiDeliveryTime = Carbon::createFromFormat("Y-m-d H:i:s", $order["api_delivery_time"], $timeZone);
            $differenceWithPreviousApiDeliveryTime = $removingPreviousThresholdDeliveryTime->diffInMinutes($oldApiDeliveryTime, false);
            $oldDeliveryTime = $removingPreviousThresholdDeliveryTime->addMinutes($differenceWithPreviousApiDeliveryTime);
        }
        $newApiDeliveryTime = Carbon::createFromFormat("Y-m-d H:i:s", $request->api_delivery_time, $timeZone);
        $differenceWithNewApiDeliveryTime = $oldDeliveryTime->diffInMinutes($newApiDeliveryTime, false);
        $final_delivery_time = $oldDeliveryTime->addMinutes($differenceWithNewApiDeliveryTime)->addMinutes($getThresholdDeliveryTime);

//            }

//        }
        return $final_delivery_time;
    }

//    public function pickedUpCaseChecking()
//    {
//        set_time_limit(0);
//        DB::enableQueryLog();
//        $order = new Orders();
//        $getAllNonDeliveredPickupOrder = $order
//            ->where([
//                ["order_status", "accepted"],
//                ["delivery_type", "pickup"],
//                ["payment_method", "cash-on-pickup"],
//                ["api_delivery_time", "!=", ""],
//                ["delivery_status","!=","delivered"],
//            ])
//            ->orWhere([
//                ["order_status", "accepted"],
//                ["delivery_type", "pickup"],
//                ["api_delivery_time", "!=", ""],
//                ["payment_method", "creditcard"],
//                ["payment_status", "completed"],
//                ["delivery_status","!=","delivered"],
//            ])->orderBy("id","desc")->get();
//        return DB::getQueryLog();
//        foreach ($getAllNonDeliveredPickupOrder as $order) {
//            if($order->id != 66){
//                continue;
//            }
//            if ($order->payment_method == "creditcard") {
//                $apiDeliveryTime = Carbon::parse($order->api_delivery_time, $order->time_zone_utc);
//                $currentTime = Carbon::now($order->time_zone_utc);
//                $timeDifference = $apiDeliveryTime->diffInMinutes($currentTime);
//                if ($timeDifference >= 10) {
//                    $order->update([
//                        "delivery_status" => "delivered"
//                    ]);
//                }
//            } else {
//                $apiDeliveryTime = Carbon::parse($order->api_delivery_time, $order->time_zone_utc);
//                $currentTime = Carbon::now($order->time_zone_utc);
//                $timeDifference = $apiDeliveryTime->diffInMinutes($currentTime);
//                if ($timeDifference >= 10) {
//                    $order->update([
//                        "delivery_status" => "delivered",
//                        "payment_status" => "completed"
//                    ]);
//                }
//            }
//        }
//    }



    public function exportCsv()
    {
        $request['restaurant_id'] = Input::get("restaurant_id");

        $request['start_date'] = Input::get('start_date');
        $request['end_date'] = Input::get('end_date');

        if (!empty($request['start_date']) && !empty($request['end_date'])) {
            $request['start_date'] = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
            $request['end_date'] = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000

        }

        $order = $this->order->getOrdersForCsv($request['restaurant_id'], $request['start_date'], $request['end_date']);

        if (!empty($order)) {
            Excel::create('order', function ($excel) use ($order) {
                $excel->sheet('mySheet', function ($sheet) use ($order) {
                    $sheet->fromArray($order);
                    $sheet->cell("A1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Order ID");
                    });
                    $sheet->cell("B1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Customer");
                    });
                    $sheet->cell("C1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Delivery Type");
                    });

                    $sheet->cell("D1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Order Date");
                    });
                    $sheet->cell("E1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Delivery Status");
                    });

                    $sheet->cell("F1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Order Status");
                    });

                    $sheet->cell("G1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Payment Status");
                    });
                    $sheet->cell("H1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Commission");
                    });

                    $sheet->cell("I1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Grand Total");
                    });

                    $sheet->cell("J1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Payment Method");
                    });

                    $sheet->cell("K1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Delivery Fee");
                    });


                    $sheet->cell("L1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Customer Note");
                    });

                    $sheet->cell("M1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Restaurant");
                    });

                    $sheet->cell("N1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Sub Total");
                    });

                    $sheet->cell("O1", function ($cell) use ($order) {
                        $cell->setFont(array(

                            'size' => '10',
                            'bold' => true,
                        ));

                        $cell->setValue("Note");
                    });
                });
            })->export('csv');

        } else {
            return response()->json([
                "status" => "400",
                "message" => "No record found"
            ], 400);
        }


    }


}