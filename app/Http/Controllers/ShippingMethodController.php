<?php

namespace App\Http\Controllers;


use App\Http\Controllers\ShippingClass\FixedRateShippingMethod;
use App\Repo\ShippingMethodInterface;
use App\Repo\ShippingMethodTranslationInterface;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use LogStoreHelper;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Yajra\Datatables\Facades\Datatables;
use Validator;

class ShippingMethodController extends Controller
{
    /**
     * @var EmailTemplateInterface
     */
    private $shippingMethod;
    /**
     * @var LogStoreHelper
     */
    private $logStoreHelper;

    private $slugify;

    protected $flatRate;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ShippingMethodInterface $shippingMethod,
                                ShippingMethodTranslationInterface $shippingMethodTranslation,
                                LogStoreHelper $logStoreHelper,
                                Slugify $slugify, FixedRateShippingMethod $flatRate)
    {
        $this->shippingMethod = $shippingMethod;
        $this->shippingMethodTranslation = $shippingMethodTranslation;
        $this->logStoreHelper = $logStoreHelper;
        $this->slugify = $slugify;
        $this->flatRate = $flatRate;
    }

//    public function showBySlug($slug){
//
//        $shippingMethod = $this->shippingMethod->getSpecificShippingMethodBySlug($slug);
//        $class = $shippingMethod[0]["class"];
//        $name = "\\App\\Http\\Controllers\\ShippingClass\\".$class;
//        try{
//           if(!class_exists($name))
//               throw new \Exception();
//            $shipping = new  $name;
//            return $shipping->index($shippingMethod[0]);
//        }
//        catch (\Exception $ex){
//            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
//                'status'=>'404',
//                'message'=>'Shipping Method Class Not found'
//            ]));
//            return response()->json([
//                'status'=>'404',
//                'message'=>'Shipping Method Class Not found'
//            ],404);
//        }
//    }

    public function calculateShipping(Request $request){
        $shippingMethod = $this->shippingMethod->getSpecificShippingMethodBySlug($request['slug']);

        $class = $shippingMethod[0]["class"];

        $name = "\\App\\Http\\Controllers\\ShippingClass\\".$class;
        try{
            if(!class_exists($name)){
                throw new \Exception();
            }
            $shipping = new  $name;
            if($shippingMethod[0]['is_taxable'] == 1){
                $shipping->setIsTaxable(1);
                $shipping->setTaxPercentage($request['tax_percentage']);
            }
            //if($shippingMethod['shipping_type']="dynamic_distance"){
                $shipping->setMinCost($request['min_cost']);
                $shipping->setPerCost($request['per_cost']);
                $shipping->setSourceLongitude($request['source_longitude']);
                $shipping->setSourceLatitude($request['source_latitude']);
                $shipping->setShippingId($request['shipping_id']);
                $shipping->setDestinationLongitude($request['destination_longitude']);
                $shipping->setDestinationLatitude($request['destination_latitude']);
            //}
            return $shipping->index();
        }
        catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=>'Shipping Method Class Not found'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>'Shipping Method Class Not found'
            ],404);
        }
    }

    public function adminIndex(Request $request)
    {
        $request['lang'] = Input::get('lang', 'en');

        /**
         * Validating the given request
         */
        try{
            $this->validate($request, [
                'status'=>'sometimes|integer|min:0|max:1'
            ]);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }

        $request['status'] = Input::get('status', null);

        try{
            $shippingMethods = $this->shippingMethod->getAllShippingMethodByStatus($request['status']);
            if(count($shippingMethods) ==0){
                throw new \Exception();
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=> 'Shipping Method could not be found'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Shipping Method could not be found"
            ],404);
        }
        /**
         * Listing Translation of shipping_methods
         */
        foreach ($shippingMethods as $shippingMethod) {
            try {
                $shippingMethodsTranslation = $this->shippingMethodTranslation->getAllShippingMethodTranslationByLang($shippingMethod['id'],$request['lang']);
                if ($shippingMethodsTranslation->count() == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }
            $shippingMethod['lang_code'] = $shippingMethodsTranslation[0]['lang_code'];
            $shippingMethod['name'] = $shippingMethodsTranslation[0]['name'];
        }


        $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
            'status'=>'200',
            'data'=> $shippingMethods
        ]));
        return response()->json([
            'status'=>'200',
            'data'=> $shippingMethods
        ],200);
    }

    public function publicIndex(Request $request)
    {
        $request['lang'] = Input::get('lang', 'en');


        try{
            $shippingMethods = $this->shippingMethod->getAllShippingMethodByStatus(1);
            $shippingMethods->makeHidden(["class", "status", "created_at", "updated_at"]);
            if(count($shippingMethods) ==0){
                throw new \Exception();
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=> 'Shipping Method could not be found'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Shipping Method could not be found"
            ],404);
        }
        /**
         * Listing Translation of shipping_methods
         */
        foreach ($shippingMethods as $shippingMethod) {
            try {
                $shippingMethodsTranslation = $this->shippingMethodTranslation->getAllShippingMethodTranslationByLang($shippingMethod['id'],$request['lang']);
                if ($shippingMethodsTranslation->count() == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }
            $shippingMethod['lang_code'] = $shippingMethodsTranslation[0]['lang_code'];
            $shippingMethod['name'] = $shippingMethodsTranslation[0]['name'];
        }


        $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
            'status'=>'200',
            'data'=> $shippingMethods
        ]));
        return response()->json([
            'status'=>'200',
            'data'=> $shippingMethods
        ],200);
    }

    public function publicShow($id ,Request $request)
    {
        try{
            $shippingMethod = $this->shippingMethod->getSpecificShippingMethod($id);
            $shippingMethod->makeHidden(["class", "status", "created_at", "updated_at"]);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=> 'Shipping Method could not be found'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Shipping Method could not be found"
            ],404);
        }

        try {
            $shippingMethodsTranslation = $this->shippingMethodTranslation->getAllShippingMethodTranslationByLang($shippingMethod['id'],$request['lang']);
            if ($shippingMethodsTranslation->count() == 0) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Default language english not found in database"
            ], 404);
        }
        $shippingMethod['lang_code'] = $shippingMethodsTranslation[0]['lang_code'];
        $shippingMethod['name'] = $shippingMethodsTranslation[0]['name'];

        $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
            'status'=>'200',
            'data'=> $shippingMethod
        ]));
        return response()->json([
            'status'=>'200',
            'data'=> $shippingMethod
        ],200);
    }

    public function adminShow($id ,Request $request)
    {
        try{
            $shippingMethod = $this->shippingMethod->getSpecificShippingMethod($id);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=> 'Shipping Method could not be found'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Shipping Method could not be found"
            ],404);
        }

        try {
            $shippingMethodsTranslation = $this->shippingMethodTranslation->getAllShippingMethodTranslation($shippingMethod['id']);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Translation Not found"
            ], 404);
        }
        $shippingMethod['translation'] = $shippingMethodsTranslation;

        $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
            'status'=>'200',
            'data'=> $shippingMethod
        ]));
        return response()->json([
            'status'=>'200',
            'data'=> $shippingMethod
        ],200);
    }

    public function store(Request $request)
    {
        /**
         * Slugifying the slug
         */
        $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
        $message =[
            "slug.required" => "The slug may not only contain numbers,special characters and should not be empty.",
            "shipping_type.in" => "The shipping type must be fixed, dynamic_distance or dynamic_order_percentage"
        ];
        /**
         * Validating the given request
         */
        try{
            $this->validate($request, [
                'slug'=>'required|unique:shipping_method',
                'class'=>'required',
                'status'=>'required|integer|min:0|max:1',
                'is_taxable' => 'required|integer|min:0|max:1',
                'shipping_cost' => 'required|numeric|min:0',
                'shipping_type' => 'required|in:fixed,dynamic_distance,dynamic_order_percentage',
                'dynamic_shipping_value' => 'integer| min:0',
                "translation" => "required|array",
                "translation.en" => "required"
            ],$message);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }
        $request=$request->all();

        /**
         * If status is not given, default is set to 1
         */
        $request['status'] = Input::get('status', 1);
        
        DB::beginTransaction();
        /**
         * Creating Shipping Method
         */
        try{
            $shippingMethod = $this->shippingMethod->createShippingMethod($request);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=> 'Shipping Method could not be created'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Shipping Method could not be created"
            ],404);
        }

        foreach ($request['translation'] as $key=> $translation){
            $translation['shipping_method_id'] = $shippingMethod->id;
            $translation['lang_code'] = $key;

            $rules = [
                "lang_code" => 'required|alpha',
                "name" => "required",
            ];

            $validator = Validator::make($translation, $rules);
            if ($validator->fails()) {
                $error = $validator->errors();
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Shipping Method Translation', [
                    'status' => '422',
                    "message" => [$key => $error]
                ]));
                return response()->json([
                    'status' => '422',
                    "message" => [$key => $error]
                ],422);
            }

            /**
             * slugifying the lang code
             */
            $translation['lang_code']=$this->slugify->slugify($translation['lang_code'],['regexp' => '/([^A-Za-z]|-)+/']);
            if(empty($translation['lang_code'])){
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Restaurant Translation', [
                    'status' => '422',
                    "message" => ["lang_code"=>"The lang_code may not only contain numbers,special characters."]
                ]));
                return response()->json([
                    "status"=>"422",
                    "message"=>["lang_code"=>"The lang_code may not only contain numbers,special characters."]
                ],422);
            }


            try{
                $createTranslation = $this->shippingMethodTranslation->createShippingMethodTranslation($translation);
            }catch (\Exception $ex)
            {
                DB::rollBack();
                $this->logStoreHelper->storeLogInfo(array("Shipping Method Translation", [
                    "status" => "404",
                    "message" => ["Shipping Method Translation could not be created"]
                ]));
                return response()->json([
                    "status"=>"404",
                    "message"=>["Shipping Method Translation could not be created"]
                ],404);
            }

            $this->logStoreHelper->storeLogInfo(array("Shipping Method Translation", [
                "status" => "200",
                "message" => "Shipping Method Translation Created Successfully",
                "data" => $createTranslation
            ]));
        }

        DB::commit();
        
        $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
            'status'=>'200',
            'message'=> 'Shipping Method created successfully'
        ]));
        return response()->json([
            'status'=>'200',
            'message'=> "Shipping Method created successfully"
        ],200);

    }

    public function update($id,Request $request)
    {
        try{
            $shippingMethod = $this->shippingMethod->getSpecificShippingMethod($id);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=> 'Requested Shipping Method could not be found'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> 'Requested Shipping Method could not be found'
            ],404);
        }
        /**
         * Slugifying the slug
         */
        $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
        $message =[
            "slug.required" => "The slug may not only contain numbers,special characters and should not be empty.",
            "shipping_type.in" => "The shipping type must be fixed, dynamic_distance or dynamic_order_percentage"
        ];
        /**
         * Validating the given request
         */
        try{
            $this->validate($request, [
                'slug'=>'required|unique:shipping_method,slug,'.$id,
                'class'=>'required',
                'status'=>'required|integer|min:0|max:1',
                'is_taxable' => 'required|integer|min:0|max:1',
                'shipping_cost' => 'required|numeric|min:0',
                'shipping_type' => 'required|in:fixed,dynamic_distance,dynamic_order_percentage',
                'dynamic_shipping_value' => 'integer| min:0 | max:100',
                "translation" => "required|array",
                "translation.en" => "required"
            ],$message);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }
        $request=$request->all();

        /**
         * Updating Shipping Method
         */
        DB::beginTransaction();
        try{
        $shipping = $this->shippingMethod->updateShippingMethod($id, $request);
        }catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=> 'Shipping Method could not be updated'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Shipping Method could not be updated"
            ],404);
        }


            $deleteTranslation=$shippingMethod->shippingMethodTranslation()->delete();
        try{}catch (\Exception $ex){
            DB::rollBack();
            $this->logStoreHelper->storeLogInfo(array("Page",[
                'status'=>'404',
                'message'=>"Page could not be deleted for update"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Page could not be deleted for update"
            ],404);
        }

        foreach ($request['translation'] as $key=> $translation) {
            $translation['shipping_method_id'] = $shippingMethod->id;
            $translation['lang_code'] = $key;

            $rules = [
                "lang_code" => 'required|alpha',
                "name" => "required",
            ];

            $validator = Validator::make($translation, $rules);
            if ($validator->fails()) {
                $error = $validator->errors();
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Shipping Method Translation', [
                    'status' => '422',
                    "message" => [$key => $error]
                ]));
                return response()->json([
                    'status' => '422',
                    "message" => [$key => $error]
                ], 422);
            }

            /**
             * slugifying the lang code
             */
            $translation['lang_code'] = $this->slugify->slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
            if (empty($translation['lang_code'])) {
                DB::rollBack();
                $this->logStoreHelper->storeLogError(array('Restaurant Translation', [
                    'status' => '422',
                    "message" => ["lang_code" => "The lang_code may not only contain numbers,special characters."]
                ]));
                return response()->json([
                    "status" => "422",
                    "message" => ["lang_code" => "The lang_code may not only contain numbers,special characters."]
                ], 422);
            }


            try {
                $createTranslation = $this->shippingMethodTranslation->createShippingMethodTranslation($translation);
            } catch (\Exception $ex) {
                DB::rollBack();
                $this->logStoreHelper->storeLogInfo(array("Shipping Method Translation", [
                    "status" => "404",
                    "message" => ["Shipping Method Translation could not be created"]
                ]));
                return response()->json([
                    "status" => "404",
                    "message" => ["Shipping Method Translation could not be created"]
                ], 404);
            }
        }

        DB::commit();
        $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
            'status'=>'200',
            'message'=> 'Shipping Method updated successfully'
        ]));
        return response()->json([
            'status'=>'200',
            'message'=> "Shipping Method updated successfully"
        ],200);

    }

    public function delete($id){
        try{
            $shippingMethod = $this->shippingMethod->deleteShippingMethod($id);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=> 'Shipping Method could not be found'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Shipping Method could not be found"
            ],404);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
                'status'=>'404',
                'message'=> 'Shipping Method could not be deleted'
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Shipping Method could not be deleted"
            ],404);
        }
        $this->logStoreHelper->storeLogInfo(array("Shipping Method",[
            'status'=>'200',
            'message'=> 'Shipping Method deleted successfully'
        ]));
        return response()->json([
            'status'=>'200',
            'message'=> "Shipping Method deleted successfully"
        ],200);
    }

    public function test(Request $request){
        $data = \Settings::getSettings($request['slug']);
        if ($data["status"] != 200) {
            return response()->json(
                $data["message"]
                , $data["status"]);
        }
        $dataForMobileSdk["pt_secret_key"] = $data['message']['data']['value'];
        return $dataForMobileSdk["pt_secret_key"];
    }

}
