<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 10/12/17
 * Time: 12:07 PM
 */

namespace App\Http\Controllers;

use App\Models\Orders;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use RemoteCall;
use Illuminate\Support\Facades\Config;
use App\Repo\OrderInterface;
class ExportController extends Controller
{

    private $db_ext;
    private $db_ext1;
    private $db_ext2;
    private $order;
    public function __construct(OrderInterface $order)
    {
        $this->db_ext = DB::connection('mysql_external');
        $this->db_ext1 = DB::connection('mysql_external1');
        $this->db_ext2 = DB::connection('mysql_external2');
        $this->order = $order;

    }

    public function exportOrder()
    {

        try {
            set_time_limit(300000);

            $oldOrders = $this->db_ext->table('orders')
                ->join('order_items', 'order_items.order_id', '=', 'orders.id')
                ->leftJoin('restaurants', 'restaurants.id', '=', 'order_items.restaurant_id')
                ->leftJoin('order_comment', 'order_comment.order_id', '=', 'orders.id')
                ->select('orders.id as orders_id', 'orders.*', 'orders.created_at as order_created_at', 'orders.updated_at as order_updated_at', 'order_items.id as item_id', 'order_items.*', 'order_items.created_at as item_created_at', 'order_items.updated_at as item_updated_at', 'order_comment.id as comment_id', 'order_comment.*', 'order_comment.created_at as comment_created_at', 'order_comment.updated_at as comment_updated_at', 'orders.user_id as order_user_id', 'restaurants.name', 'restaurants.ar_name', 'order_comment.user_id as comment_user_id')
                ->get();


            //DB::beginTransaction();
            foreach ($oldOrders as $order) {
                echo $order->orders_id . "<br>";
                try {


                    if (empty($order->order_user_id)) {
                        continue;
                    }
                    $items = [];

                    //echo ($order->orders_id)."<br>";

                    $cart_data = unserialize($order->cart_data);
                    try {
                        //echo $order->orders_id."<br>";
                        foreach ($cart_data as $key => $data) {
                            foreach ($data as $item) {

                                $cart = [];
                                $cart = [

                                    'total_unit_price' => $item['food_price'],
                                    'item_id' => $item['food_id'],
                                    'food_name' => [
                                        'en' => $item['food_name'],

                                    ],
                                    'unit_price' => $item['food_price'],
                                    'size' => '',
                                    'size_title' => '',
                                    'quantity' => $item['qty'],
                                    'image_url' => '',
                                    'special_instruction' => ''
                                ];

                                $addons = [];
                                if (count($item['addons']) > 0) {
                                    //return "hi";
                                    foreach ($item['addons'] as $add) {

                                        $addoncheck = $this->db_ext->table('foods')->join('food_addons', 'foods.id', '=', 'food_addons.food_id')->where([
                                            ['foods.id', '=', $item['food_id']],
                                            ['foods.name', '=', $add['addon_name']],
                                        ])->value('foods.id');
                                        //return $addoncheck;

                                        if (count($addoncheck) <= 0) {
                                            $addoncheck = $this->db_ext->table('food_addonscategories')
                                                ->join('addons_addonscategories', 'food_addonscategories.addonscategories_id', '=', 'addons_addonscategories.addonscategories_id')
                                                ->join('foods', 'foods.id', '=', 'addons_addonscategories.addon_id')
                                                ->where([
                                                    ['food_addonscategories.food_id', '=', $item['food_id']],
                                                    ['foods.name', '=', $add['addon_name']],
                                                ])->value('foods.id');


                                            if (count($addoncheck) <= 0) {
                                                $addoncheck = $this->db_ext->table('foods')->where([
                                                    ['foods.food_or_addon', '=', 'A'],
                                                    ['foods.name', '=', $add['addon_name']],
                                                ])->value('id');

                                                if ($addoncheck > 0) {
                                                    $addon = [];

                                                    $addon['addon_id'] = $addoncheck;
                                                    $addon['addon_name'] = [
                                                        'en' => $add['addon_name'],

                                                    ];
                                                    $addon['unit_price'] = $add['addon_price'];
                                                    //$cart['addons'] = $addons;

                                                    $addons[] = $addon;
                                                }

                                            }
                                        }

                                    }

                                } else {
                                    $addons = [];
                                }

                                $dt['food_details'] = $cart;
                                $dt['addons'] = $addons;
                                $items[] = $dt;


                            }

                            $extra_info['items'] = $items;


                        }
                        // return $items;

                    } catch (\Exception $ex) {
                        continue;
                    }


                    //return Config::get("config.restaurant_base_url") . "/public/restaurant/branch/" . $order->restaurant_id . "/detail";
                    $restaurantdata = RemoteCall::getSpecific(Config::get("config.restaurant_base_url") . "/export/restaurant/branch/" . $order->restaurant_id . "/detail?delivery_time=" . $order->delivery_or_pickup_time . "lang=ar");


                    if ($restaurantdata['status'] != 200) {

                        continue;
//                    $restaurantdata['re'] = $order->restaurant_id;
//                    return response()->json(
//
//                        $restaurantdata, $restaurantdata['status']);

                    }


                    $restaurant = $restaurantdata['message']['data'];
                    $restaurant['translation'] = [
                        'en' => $order->name,
                        'ar' => $order->ar_name

                    ];


                    if ($order->payment_method == 'Paytabs') {
                        $order->payment_method = 'creditcard';
                        if (!empty($order->payment_data)) {
                            $payment_data = unserialize($order->payment_data);
                            if (gettype($payment_data) == 'array') {
                                $ref_id = isset($payment_data['payment_reference']) ? $payment_data['payment_reference'] : '';

                            } else {
                                $ref_id = isset($payment_data->transaction_id) ? $payment_data->transaction_id : '';
                            }
                        }
                    } elseif ($order->payment_method == 'Cash On Pick Up') {
                        $order->payment_method = 'cash-on-pickup';
                    } else {
                        $order->payment_method = str_slug($order->payment_method);
                    }
                    $tim = '';
                    if (!empty($order->delivery_or_pickup_time)) {
                        $time = explode(' ', $order->delivery_or_pickup_time);

                        $hm = explode(':', $time[0]);

                        if ($time[1] == 'PM') {
                            $hm[0] = $hm[0] + 12;


                        }
                        $time = Carbon::createFromTime($hm[0], $hm[1]);

                        $tim = explode(' ', $order->order_created_at)[0] . " " . explode(' ', $time)[1];


                    }

                    $insertOrder['id'] = $order->orders_id;
                    $insertOrder['billing_id'] = '';
                    $insertOrder['shipping_id'] = '';
                    $insertOrder['payment_method'] = str_slug($order->payment_method);
                    $insertOrder['country_id'] = $restaurant['country_id'];
                    $insertOrder['city_id'] = $restaurant['city_id'];
                    $insertOrder['district_id'] = $restaurant['district_id'];
                    $insertOrder['delivery_type'] = $order->delivery_type == 'Pick Up' ? 'pickup' : str_slug($order->delivery_type);
                    $insertOrder['amount'] = $order->total_price;
                    $insertOrder['is_seen'] = $order->seen;
                    $insertOrder['currency'] = $order->currency;
                    $insertOrder['customer_id'] = empty($order->order_user_id) ? '' : $order->order_user_id;
                    $insertOrder['order_status'] = str_slug($order->order_status);
                    $insertOrder['payment_status'] = str_slug($order->payment_status);
                    $insertOrder['payment_ref_id'] = isset($ref_id) ? $ref_id : '';
                    $insertOrder['site_commission_rate'] = $order->site_commission_rate;
                    $insertOrder['site_commission_amount'] = $order->site_commission;
                    $insertOrder['delivery_or_pickup_time'] = $tim;
                    $insertOrder['restaurant_id'] = $order->restaurant_id;
                    $insertOrder['restaurant'] = serialize($restaurant);
                    $insertOrder['delivery_fee'] = empty($order->delivery_price) ? '' : $order->delivery_price;
                    $insertOrder['api_delivery_time'] = empty($order->api_delivery_time) ? '' : $order->api_delivery_time;
                    $insertOrder['threshold_delivery_time'] = empty($order->threshold_delivery_time) ? '' : $order->threshold_delivery_time;
                    $insertOrder['site_tax_rate'] = $order->site_tax_rate;
                    $insertOrder['customer_note'] = '';
                    $insertOrder['delivery_status'] = str_slug($order->is_delivered);
                    $insertOrder['order_date'] = $order->order_created_at;
                    $insertOrder['created_at'] = $order->order_created_at;
                    $insertOrder['updated_at'] = $order->order_updated_at;
                    $insertOrder['deleted_at'] = null;
                    $insertOrder['distance'] = '';
                    $insertOrder['restaurant_latitude'] = $restaurant['latitude'];
                    $insertOrder['restaurant_longitude'] = $restaurant['longitude'];
                    $insertOrder['shipping_district_id'] = '';
                    $insertOrder['hash'] = '';
                    $insertOrder['pre_order'] = 0;
                    $insertOrder['extra_info'] = serialize($extra_info);

                    DB::table('orders')->insert($insertOrder);


                    $this->insertOrderItems($extra_info, $order->orders_id);

                    if (!empty($order->comment_id)) {

                        $insertOrderComment['id'] = $order->comment_id;
                        $insertOrderComment['order_id'] = $order->orders_id;
                        $insertOrderComment['comment'] = $order->comment;
                        $insertOrderComment['user_id'] = $order->comment_user_id;
                        $insertOrderComment['created_at'] = $order->comment_created_at;
                        $insertOrderComment['updated_at'] = $order->comment_updated_at;
                        DB::table('order_comments')->insert($insertOrderComment);
                    }
                } catch (QueryException $ex) {
                    continue;

                } catch (\Exception $exception) {
                    continue;
                }


            }
            //DB::commit();
            return response()->json([

                'message' => 'data successfully migrated',
                'status' => '200'

            ], 200);
        } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }
    }


    private function insertOrderItems($extra_info, $orderId)
    {

        foreach ($extra_info as $key => $items) {

            foreach ($items as $cart) {

                $insert['order_id'] = $orderId;
                $cart['food_details']['food_name'] = serialize($cart['food_details']['food_name']);
                $insert['item_id'] = $cart['food_details']['item_id'];
                $insert['food_name'] = $cart['food_details']['food_name'];
                $insert['quantity'] = $cart['food_details']['quantity'];
                $insert['unit_price'] = $cart['food_details']['unit_price'];


                $insert['food_raw_data'] = serialize($cart['food_details']);
                $id = DB::table('order_items')->insertGetId($insert);
                if (count($cart['addons']) > 0) {

                    foreach ($cart['addons'] as $addon) {
                        //print_r($addon);
                        $insertAddon['order_item_id'] = $id;
                        $insertAddon['addon_id'] = $addon['addon_id'];
                        $insertAddon['addon_name'] = serialize($addon['addon_name']);
                        $insertAddon['unit_price'] = $addon['unit_price'];
                        $insertAddon['addon_raw_data'] = serialize(array_except($insertAddon, ['addon_raw_data']));
                        // return $insertAddon;
                        DB::table('order_item_addons')->insert($insertAddon);
                    }

                }
            }
        }


        return true;
    }

    public function exportDriverOrders(\Illuminate\Http\Request $request)
    {

        set_time_limit(3000);
        $fileD = fopen($request->file, "r");
        $column = fgetcsv($fileD);
        while (!feof($fileD)) {
            $rowData[] = fgetcsv($fileD);
        }
        //DB::beginTransaction();
        $i=0;
        foreach ($rowData as $key => $value) {

            try {
                $oldDetails = $this->db_ext1->table('lugm_orderdetails')
                    ->join('lugm_login', 'lugm_orderdetails.loginid', '=', 'lugm_login.loginid')
                    ->where([
                        ['lugm_login.user_id', $value[1]],
                        ['lugm_login.usertype', $value[6]]
                    ])->get();
                if (count($oldDetails) <= 0) continue;

                $insert = [];
                $insert['first_name'] = $value[9];
                $insert['middle_name'] = '';
                $insert['last_name'] = '';
                $insert['email'] = $value[13];
                $insert['password'] = app('hash')->make($value[3]);

                $insert['mobile'] = !empty($value[12]) ? $value[12] : '123';
                $insert['birthday'] = explode(" ", Carbon::parse($value[16]))[0];
                //echo $value[16];


                $insert['status'] = ($value[19] == "Approved") ? 1 : 0;
                $insert['facebook_id'] = null;
                $insert['google_id'] = null;
                $insert['country'] = 'Saudi Arabia';
                $insert['country_code'] = '+966';
                $insert['city'] = $value[14];
                $insert['created_at'] = $value[18];
                $insert['updated_at'] = $value[24];

                $id = $this->db_ext2->table('users')->insertGetId($insert);
                $this->db_ext2->table('role_user')->insert([
                    'user_id' => $id,
                    'role_id' => 6

                ]);


                foreach ($oldDetails as $detail) {
                    //dd($detail);
                    try {
                        $order = DB::table('orders')->find($detail->orderid);
                        $order = json_decode(json_encode($order), True);
                        //dd(array_except($order, ['created_at', 'updated_at', 'restaurant']));
                        $insertDetail = [];
                        $insertDetail['user_id'] = $id;
                        $insertDetail['driver_country_id'] = $order['country_id'];
                        $insertDetail['city_id'] = $order['city_id'];
                        $insertDetail['source_district_id'] = $order['district_id'];
                        $insertDetail['destination_district_id'] = $order['shipping_district_id'];
                        $insertDetail['restaurant_id'] = $order['restaurant_id'];
                        $insertDetail['distance'] = $detail->distance;
                        $insertDetail['price'] = $detail->price;
                        $insertDetail['status'] = $detail->status;
                        $insertDetail['pickup_time'] = empty($order['delivery_or_pickup_time']) ? '' : $order['delivery_or_pickup_time'];

                        $insertDetail['restaurant'] = $order['restaurant'];
                        $insertDetail['order'] = serialize(array_except($order, ['created_at', 'updated_at', 'restaurant']));
                        $insertDetail['created_at'] = $detail->created;
                        $insertDetail['updated_at'] = $detail->modified;
                        $insertDetail['order_id'] = $detail->orderid;
                        $insertDetail['country_id'] = 1;

                        $driver_id = DB::table('driver_order')->insertGetId($insertDetail);

                        if (!empty($detail->google_map_coordinate)) {
                            $coordinates = explode(",", $detail->geo);
                        }

                        DB::table('driver_order_location')->insert([
                            'driver_order_id' => $driver_id,
                            'created_at' => $detail->created,
                            'updated_at' => $detail->created,
                            'latitude' => isset($coordinates[0]) ? ($coordinates[0]) : 24.774265,
                            'longitude' => isset($coordinates[1]) ? ($coordinates[1]) : 46.738586

                        ]);

                        DB::table('driver_points')->insert([
                            'user_id' => $id,
                            'created_at' => $detail->created,
                            'updated_at' => $detail->created,
                            'point' => $detail->points

                        ]);
                        DB::table('driver_experience_point')->insert([
                            'user_id' => $id,
                            'order_id' => $order['id'],
                            'created_at' => $detail->created,
                            'updated_at' => $detail->created,
                            'experience_point' => $detail->points

                        ]);



                    }catch (\Exception $ex) {
                        $i++;
                        $email[] = $value[13];
                        continue;
//                        return response()->json([
//
//                            'message' => $detail->orderid,
//
//                            'status' => '400'
//
//                        ], 400);
                    }
                }

            } catch (\Exception $ex) {
                $email[] = $value[13];
                continue;
//                return response()->json([
//
//                    'message' => $ex->getMessage(),
//
//                    'status' => '400'
//
//                ], 400);
            }

        }
       // DB::commit();
        try { } catch (\Exception $ex) {
            return response()->json([

                'message' => $ex->getMessage(),
                'status' => '400'

            ], 400);
        }
        return response()->json([

            'message' => 'data successfully migrated',
            'data' => $email,
            'status' => '200'

        ], 200);

    }

    public function setPickupTime()
    {
        $driverHistory = DB::table('driver_order')
            ->select("id","order_history","restaurant")
            ->where([
                ["order_history","<>",""],
                ["status","<>","assigned"]
            ])->get();

       foreach ($driverHistory as $history){
           $id = $history->id;
           $orderHistory = unserialize($history->order_history);
           $restaurantData = unserialize($history->restaurant);
           $timeZone = (isset($restaurantData["timezone"])) ?$restaurantData["timezone"]:"Asia/Riyadh";
           if(isset($orderHistory["picked"])){
                $pickupTime = Carbon::parse($orderHistory["picked"]["date"]);
                $pickupTime = Carbon::createFromFormat('Y-m-d H:i:s',$pickupTime,$timeZone)->tz('utc');
              $update = DB::table("driver_order")
                  ->where('id', $id)
                  ->update(['driver_picked_time' => $pickupTime]);
           }
       }
       return response()->json([
           "status" => "200",
           "message" => "Successfully exported driver picked time."
       ]);

    }

    public function setOrderTax(){
            set_time_limit(30000);
           $allOrders = Orders::orderBy("id","desc")->get();

            foreach ($allOrders as $key => $order){
                $orderItems = $order->orderItem()->get();

                $orderItemCount = $orderItems->count();
                if ( $orderItemCount== 0) {
                   $tax = 0;
                   $order->update(["tax" => $tax]);
                   continue;
                }
                /**
                 * setting @param $total to 0 by default
                 */

                $total = 0;

                foreach ($orderItems as $orderItem) {
                    $itemTotal = 0;
                    $addonTotal = 0;
                    $orderItemAddons = $orderItem->OrderItemAddon()->get();
                    /**
                     * loops through array of addons to calculate addon total
                     */
                    $tempArray = [];
                    foreach ($orderItemAddons as $orderItemAddon) {

                        $tempArray[] = $orderItemAddon;
                        $addonTotal += $orderItemAddon["unit_price"];
                    }
                    $orderItem["addons"] = $tempArray;
                    $total += $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                }
                $tax = round($order->amount - $order->delivery_fee - $total,2);
               $order->update(["tax" => $tax]);
            }
    }

}