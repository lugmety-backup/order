<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 6/12/17
 * Time: 12:06 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Redis;
use LogStoreHelper;
use RemoteCall;
use Illuminate\Support\Facades\Config;
use FoodData;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use RoleChecker;
use App\Repo\OrderInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CartController extends Controller
{
    /**
     * @var LogStoreHelper
     */
    protected $logStoreHelper, $order;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LogStoreHelper $logStoreHelper, OrderInterface $order)
    {

        $this->logStoreHelper = $logStoreHelper;
        $this->order = $order;
    }


    /**
     * @param $hash
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * Add the given food and food addon to the cart.
     * If hash is present in request, update the related key.
     * Else create new key using unique id.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function addToCart(Request $request)
    {
        $lang = Input::get('lang', 'en');

        try {
            $message = [
                "hash.required" => "Hash field cannot be empty.",

            ];
            $this->validate($request, [

                "hash" => "sometimes|required",

            ], $message);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }

        try {
            /**
             * Validate the given request.
             * If cart_data is not available, throw validation error else proceed.
             */
            $lang = Input::get("lang", "en");


            $userId = null;
            /**
             * if request has token, extract user id from token.
             * Check if user has hash.
             *
             */
            $oldData = [];
            if ($request->has("token")) {
                /**
                 * Get user id using token using getUserId function.
                 * If status code is other than 200, throw error message.
                 */
                $userDetails = $this->getUserId($request['token']);
                if ($userDetails['status'] != 200) {

                    $this->logStoreHelper->storeLogError([
                        "Token Validation Error",
                        [
                            'status' => $userDetails['status'],
                            "message" => $userDetails['message']
                        ]

                    ]);

                    return response()->json(
                        $userDetails['message'], $userDetails['status']);
                }
                /**
                 * Use user id to get hash related to that user id.
                 */
                $userId = $userDetails['message']["user_id"];
                $oldHash = Redis::get("user-" . $userId);
                /**
                 * If user has previous hash, then get all data related to hash.
                 */
                if (isset($oldHash) && !empty($oldHash)) {
                    if (Redis::exists("identifier:" . $oldHash)) {
                        $hashData = Redis::hgetall("identifier:" . $oldHash);
                        /**
                         * Format the data types and other output data using responseFormat function.
                         */

                        $hashData = $this->responseFormat($hashData, $lang);

                        /**
                         * If request has hash, check if the input has is same as previous hash.
                         * If same, proceed.
                         * Else,send error message.
                         */
                        if ($hashData['restaurant_id'] == $request['restaurant_id']) {

                            if (!$request->has('hash')) {
                                $request['hash'] = $oldHash;
                            }
                            if ($request->has("hash") && ($oldHash != $request['hash'])) {

                                $oldData = $hashData['cart_data'];

                            } else {

                            }
                        } else {
                            try {
                                $error_data = \Settings::getErrorMessage('order-you-have-existing-cart');
                                if ($error_data["status"] != 200) {
                                    throw new \Exception();
                                }
                                $error_message = $error_data['message'][$lang];
                                return response()->json([
                                    "status" => '409',
                                    "message" => $error_message,
                                    "data" => $hashData
                                ], 409);
                            } catch (\Exception $ex) {
                                return response()->json([
                                    "status" => '409',
                                    "message" => "You have existing cart.",
                                    "data" => $hashData
                                ], 409);
                            }


                        }
                    } else {
                        Redis::del("identifier:" . $oldHash);
                        Redis::del("user-" . $userId);
                    }


                }
            }
            /**
             * If hash is provided, check if both related restaurant id matches.
             * If restaurant id matches, proceed.
             * Else throw error message.
             */
            if ($request->has('hash')) {
                $key = "identifier:" . $request['hash'];

                if (Redis::exists($key)) {
                    if (Redis::hget($key, 'restaurant_id') != $request['restaurant_id']) {

                        try {
                            $error_data = \Settings::getErrorMessage('order-you-cannot-add-food-of-another-restaurant-from-here');
                            if ($error_data["status"] != 200) {
                                throw new \Exception();
                            }
                            $error_message = $error_data['message'][$lang];
                            return response()->json([
                                "status" => '409',
                                "message" => $error_message,
                                "data" => $hashData
                            ], 409);
                        } catch (\Exception $ex) {
                            return response()->json([
                                "status" => '403',
                                "message" => "You cannot add food of another restaurant from here.",
                            ], 403);
                        }
                    }
                    $request['key'] = $key;
                } else {
                    $request['hash'] = '';
//                    return response()->json([
//                        "status" => "404",
//                        "message" => "Requested hash not found"
//                    ], 404);
                }

            }
            /**
             * Validation for the input given.
             * If validation fails, throw appropriate error message, else continue.
             */
            try {
                $this->validate($request, [

                    "restaurant_id" => "required|integer",
                    "items" => "required|array",
                ]);

            } catch (\Exception $e) {

                return response()->json([
                    "status" => "422",
                    "message" => $e->response->original
                ], 422);
            }
            foreach ($request['items'] as $item) {

                $rules = [
                    "food_id" => "required|integer",
                    "quantity" => 'required|integer|min:1',
                    "addons" => 'array',
                ];

                $validator = Validator::make($item, $rules);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError([
                        "Validation Error",
                        [
                            'status' => '422',
                            "message" => $error
                        ]

                    ]);

                    return response()->json([
                        'status' => '422',
                        "message" => $error
                    ], 422);
                }
            }

            /**
             * Send items to restaurant service to validate.
             * Display error message if the status code is other than 200.
             * Else proceed.
             */
            $cartDetails = FoodData::getFood($request->all());

            if ($cartDetails['status'] != 200) {


                return response()->json([
                    "status" => $cartDetails['status'],
                    "message" => $cartDetails['message']
                ], $cartDetails['status']);
            }

            /**
             * If the hash is present, update the data having key "identifier:" $hash.
             * Else create new key and insert into redis db.
             */

            $request['user_id'] = $userId;
            /**
             * if request has hash, check if the current restaurant id is same as restaurant id saved in hash.
             * If same, proceed.
             * Else, throw error message.
             */
            if ($request->has('hash')) {

                $key = "identifier:" . $request['hash'];

                $request['key'] = $key;
                /**
                 * Merge previous data if
                 */
                $cartDetail = array_merge($cartDetails['data'], $oldData);

                $data = $this->update($request, $cartDetail);

            } else {
                $data = $this->create($request, $cartDetails['data']);
            }
            if (isset($data['status'])) {
                $this->logStoreHelper->storeLogError([
                    "Token Validation Error",
                    [
                        'status' => $data['status'],
                        "message" => $data['message']
                    ]

                ]);

                return response()->json(
                    $data['message'], $data['status']);
            }

            $getResponse = $this->responseFormat($data, $lang);

            /**
             * Store into lumen.log
             */
            $this->logStoreHelper->storeLogInfo([
                'Cart',
                [
                    "status" => "200",
                    "message" => "Data inserted successfully",
                    "data" => $getResponse
                ]
            ]);
            try {
                $error_data = \Settings::getErrorMessage('order-cart-data-inserted-successfully');
                if ($error_data["status"] != 200) {
                    throw new \Exception();
                }
                $error_message = $error_data['message'][$lang];
                return response()->json([
                    "status" => "200",
                    "message" => $error_message,
                    "data" => $getResponse
                ], 200);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "200",
                    "message" => "Data inserted successfully.",
                    "data" => $getResponse
                ], 200);
            }


        } catch (\Exception $ex) {

            $this->logStoreHelper->storeLogError([
                'Cart',
                [
                    "status" => "500",
                    "message" => $ex->getMessage()
                ]
            ]);

            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ], 500);
        }


    }


    /**
     * Get user id from the oauth service using remote call facade.
     * @param $accessToken
     * @return \Illuminate\Http\JsonResponse
     */
    private function getUserId($accessToken)
    {
        $accessTokenHeader = "Bearer $accessToken";
        $url = Config::get("config.url");
        $body["token"] = $accessToken;
        $userDetails = RemoteCall::oauthCall($url, $accessTokenHeader, $body);

        return $userDetails;
    }

    /**
     * Create new key to store cart data.
     * Generate new hash key for  storing using "identifier:" prefix and a
     * @param $request
     * @param $cartDetail
     * @return array
     */
    public function create($request, $cartDetail)
    {
        /**
         * Generate random string
         */
        $hash = sha1(uniqid() . str_random(10));
        $key = "identifier:" . $hash;
        $total = 0;
        /**
         * Get all the cart details as food data and add all sub_total to get total price.
         */

        foreach ($cartDetail['cartData'] as $foodData) {
            $data[] = $foodData;
            $total += $foodData['sub_total'];

        }
        if ($cartDetail['restaurant_tax_type'] == "inclusive") {
            $taxData['data']['amount'] = $this->calculateTaxForInclusive($cartDetail['tax_rate'], $total);
            $taxData['data']['tax_percentage'] = $cartDetail['tax_rate'];

        } elseif ($cartDetail['restaurant_tax_type'] == "none") {
            $taxData['data']['amount'] = 0;
            $taxData['data']['tax_percentage'] = 0;
        } else {
            $taxData = $this->calculateTax($cartDetail['restaurant_city_id'], $total);
            if ($taxData['status'] != 200) {

                return $taxData;
            }

        }


        /**
         * Store data in redis database.
         */
        Redis::hmset($key, [
            'hash' => $hash,
            'user_id' => $request['user_id'],
            'cart_data' => serialize($data),
            'total_price' => $total,
            'restaurant_id' => (integer)$request['restaurant_id'],
            'restaurant_name' => serialize($cartDetail['restaurant']),
            'restaurant_slug' => $cartDetail['restaurant_slug'],
            'city_id' => $cartDetail['restaurant_city_id'],
            'currency' => $cartDetail['currency'],
            'tax_amount' => $taxData['data']['amount'],
            'tax_percentage' => $taxData['data']['tax_percentage'],
            'grand_total' => $total + $taxData['data']['amount'],
            'tax_type' => $cartDetail['restaurant_tax_type'],
            'created_at' => time(),
            'updated_at' => time()
        ]);
        Log::info("Cart Controller create", [
            'hash' => $hash,
            'user_id' => $request['user_id']]);

        $data = Redis::hgetall($key);
        /**
         * If user is logged in and user id is present, set a key value pair using userid and hash key.
         */
        if (!empty($request['user_id'])) {
            Redis::set("user-" . $request['user_id'], $hash);
        }

        return $data;

    }

    /**
     * Update the key related to the requested data items
     * @param $request
     * @param $cartDetail
     * @return array
     */
    public function update($request, $cartDetail)
    {

        /**
         * unset $data array to ensure it does not contain garbage value.
         * Use request to get hash and get key to fetch data from redis db.
         */
        unset($data);


        $hashData = Redis::hgetall($request['key']);

        /**
         * Get cart_data stored in the given key from redis db and assign to $cartData
         */
        $cartData = unserialize($hashData['cart_data']);


        /**
         * Get total_price stored in given key from redis db and assign to $totalPrice
         */

        // $totalPrice = $hashData['total_price'];

        /**
         * $cartDetail is array of input containing food and its respective addons.
         *
         */
        $update = false;
        if (!empty($cartData)) {
            /**
             * Use response from restaurant service for for-each operation.
             */
            foreach ($cartDetail['cartData'] as $detail) {

                /**
                 * Use cart details stored in redis hash.
                 */

                foreach ($cartData as $index => $food) {


                    /**
                     * Check if id from input matches the unserialized food data.
                     * If data does not match, check another row.
                     *  If matches, do further operations.
                     */
                    if (($detail['id'] == $food['id']) && ($detail['size_slug'] == $food['size_slug']) && ($detail['special_instruction'] == $food['special_instruction'])) {

                        if (!empty($detail['addons']) && !empty($food['addons'])) {
                            unset($addonIds);
                            unset($detailAddonIds);
                            foreach ($food['addons'] as $addon) {

                                $addonIds[] = $addon['id'];

                            }

                            foreach ($detail['addons'] as $detailAddon) {

                                $detailAddonIds[] = $detailAddon['id'];
                            }

                            /**
                             * If $addonIds and $detail['addons'] are identical, unset the related index.
                             * Subtract $food['sub_total'] from the cart['total_price'] and break the operation.
                             */
                            if ($detailAddonIds == $addonIds) {
                                /**
                                 * Update current cart data with restaurant service.
                                 * Replace food price with food price from response.
                                 */
                                $food['price'] = $detail['price'];
                                /**
                                 * Subtract food['sub_total'] from total_price obtained from cart. It is to minimize the calculation error in case price is updated.
                                 */
                                $hashData['total_price'] -= $food['sub_total'];
                                /**
                                 * Add quantity from restaurant service response and cart to get final quantity of food
                                 */
                                $food['quantity'] += $detail['quantity'];
                                /**
                                 * Replace total_unit_price with total_unit_price from response.
                                 */
                                $food['total_unit_price'] = $detail['total_unit_price'];
                                /**
                                 * Calculate total price of specific food for computed quantity and store in $food['sub_total']
                                 */
                                $food['sub_total'] = $food['quantity'] * $detail['total_unit_price'];

                                $food['special_instruction'] = isset($detail['special_instruction']) ? $detail['special_instruction'] : '';
                                /**
                                 * Update array related to given food
                                 */
                                $cartData[$index] = $food;
                                /**
                                 * Add total price of specific food to the grand total of the cart and store in $total_price.
                                 */
                                $hashData['total_price'] += $food['sub_total'];
                                /**
                                 * Set update flag to true.
                                 */
                                $update = true;
                                break;
                            }

                        } elseif (empty($detail['addons']) && empty($food['addons'])) {
                            /**
                             * Update current cart data with restaurant service.
                             * Replace food price with food price from response.
                             */
                            $food['price'] = $detail['price'];
                            /**
                             * Subtract food['sub_total'] from total_price obtained from cart. It is to minimize the calculation error in case price is updated.
                             */
                            $hashData['total_price'] -= $food['sub_total'];
                            /**
                             * Add quantity from restaurant service response and cart to get final quantity of food
                             */
                            $food['quantity'] += $detail['quantity'];
                            /**
                             * Replace total_unit_price with total_unit_price from response.
                             */
                            $food['total_unit_price'] = $detail['total_unit_price'];
                            /**
                             * Calculate total price of specific food for computed quantity and store in $food['sub_total']
                             */
                            $food['sub_total'] = $food['quantity'] * $detail['total_unit_price'];
                            /**
                             * Add total price of specific food to the grand total of the cart and store in $total_price.
                             */
                            $hashData['total_price'] += $food['sub_total'];


                            $food['special_instruction'] = isset($detail['special_instruction']) ? $detail['special_instruction'] : '';
                            /**
                             * Update array related to given food
                             */
                            $cartData[$index] = $food;
                            /**
                             * Set update flag to true.
                             */
                            $update = true;
                            break;
                        } else {
                            continue;
                        }


                    }

                }

                /**
                 * If $update is set to false, serialize $detail and add to $cartData.
                 */
                if (!$update) {

                    $cartData[] = $detail;
                    $hashData['total_price'] += $detail['sub_total'];
                }
                /**
                 * Add sub_total of $detail to $totalPrice.
                 */

            }

        } /**
         * Incase cartdata is empty, Insert data into cart.
         */
        else {
            foreach ($cartDetail['cartData'] as $foodData) {
                $cartData[] = $foodData;
                $hashData['total_price'] += $foodData['sub_total'];

            }
        }

        /**
         * If user id in hash is empty and user id is present, set a key value pair using userid and hash key.
         */
        if (empty($hashData['user_id']) && !empty($request['user_id'])) {

            $userId = $request['user_id'];
            Redis::set("user-" . $request['user_id'], $request['hash']);

        } else {
            $userId = $hashData['user_id'];
        }

        Log::info("Cart Controller update", [
            'hash' => $request['hash'],
            'user_id' => $request['user_id']]);

        if (!isset($hashData['tax_type'])) {
            $hashData['tax_type'] = "exclusive";

        }
        if ($hashData['tax_type'] == "inclusive") {
            $taxData['data']['amount'] = $this->calculateTaxForInclusive($hashData['tax_percentage'], $hashData['total_price']);
            $taxData['data']['tax_percentage'] = $hashData['tax_percentage'];

        } elseif ($hashData['tax_type'] == "none") {
            $taxData['data']['amount'] = 0;
            $taxData['data']['tax_percentage'] = 0;
        } else {
            $taxData = $this->calculateTax($hashData['city_id'], $hashData['total_price']);
            if ($taxData['status'] != 200) {

                return $taxData;
            }

        }

        /**
         * Store updated data to the given key.
         */
        Redis::hmset($request['key'], [
            'hash' => $request['hash'],
            'user_id' => $userId,
            'cart_data' => serialize($cartData),
            'total_price' => $hashData['total_price'],
            'restaurant_name' => serialize($cartDetail['restaurant']),
            'restaurant_slug' => $cartDetail['restaurant_slug'],
            'currency' => $cartDetail['currency'],
            'tax_amount' => $taxData['data']['amount'],
            'tax_percentage' => $taxData['data']['tax_percentage'],
            'grand_total' => $hashData['total_price'] + $taxData['data']['amount'],
            'tax_type' => $hashData['tax_type'],
            'updated_at' => time()

        ]);

        $data = Redis::hgetall($request['key']);


        return $data;
    }

    /**
     * Remove item from the given cart
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        $lang = Input::get('lang');
        /**
         * Validation for the input given.
         * If validation fails, throw appropriate error message, else continue.
         */
        try {
            $this->validate($request, [

                "hash" => "required",
                "items" => "required|array",
            ]);

        } catch (\Exception $e) {

            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }


        /**
         * Use request to get hash and get key to fetch data from redis db.
         * Check if $key exists.
         */
        $key = "identifier:" . $request['hash'];

        if (Redis::exists($key)) {
            try {

                $cartDetails = $request['items'];


                /**
                 * Get all the data related to the given key from redis db.
                 * Unserialize the data of $cart['cart_data'] and assign to $cartData.
                 */
                $cart = Redis::hgetall($key);
                $cartData = unserialize($cart['cart_data']);
                /**
                 * Check if $cartData contains the data given from input.
                 * If yes, unset the index containing the input data.
                 */
                foreach ($cartDetails as $detail) {

                    foreach ($cartData as $index => $food) {
                        $rules = [
                            "size" => "present",
                        ];

                        $validator = Validator::make($detail, $rules);
                        if ($validator->fails()) {
                            $error = $validator->errors();
                            $this->logStoreHelper->storeLogError([
                                "Validation Error",
                                [
                                    'status' => '422',
                                    "message" => $error
                                ]

                            ]);
                            return response()->json([
                                'status' => '422',
                                "message" => $error
                            ], 422);
                        }

                        /**
                         * Check if id from input matches the unserialized food data.
                         * If data does not match, check another row.
                         *  If matches, do further operations.
                         */
                        if (($detail['id'] == $food['id']) && ($detail['size'] == $food['size_slug']) && ($detail['special_instruction'] == $food['special_instruction'])) {
                            $rules = [
                                "addons" => "present|array",
                            ];

                            $validator = Validator::make($detail, $rules);
                            if ($validator->fails()) {
                                $error = $validator->errors();
                                $this->logStoreHelper->storeLogError([
                                    "Validation Error",
                                    [
                                        'status' => '422',
                                        "message" => $error
                                    ]

                                ]);
                                return response()->json([
                                    'status' => '422',
                                    "message" => $error
                                ], 422);
                            }


                            if (!empty($detail['addons']) && !empty($food['addons'])) {
                                /**
                                 * Unset $addonIds to ensure no garbage value is present before operation.
                                 */
                                unset($addonIds);
                                /**
                                 * Get all the addons from the food array.
                                 */
                                foreach ($food['addons'] as $addon) {

                                    $addonIds[] = $addon['id'];
                                }

                                //dd($detail['addons'] == $addonIds);
                                /**
                                 * If $addonIds and $detail['addons'] are identical, unset the related index.
                                 * Subtract $food['sub_total'] from the cart['total_price'] and break the operation.
                                 */
                                if ($detail['addons'] == $addonIds) {

                                    unset($cartData[$index]);
                                    $cart['total_price'] -= $food['sub_total'];

                                    break;
                                }

                            } elseif (empty($detail['addons']) && empty($food['addons'])) {
                                /**
                                 * If addons are not present, unset related index.
                                 * Subtract $food['sub_total'] from the cart['total_price'] and break the operation.
                                 */
                                unset($cartData[$index]);
                                $cart['total_price'] -= $food['sub_total'];

                                break;
                            } else {
                                continue;
                            }

                        }

                    }

                }


                if (!isset($cart['tax_type'])) {
                    $cart['tax_type'] = "exclusive";

                }
                if ($cart['tax_type'] == "inclusive") {
                    $taxData['data']['amount'] = $this->calculateTaxForInclusive($cart['tax_percentage'], $cart['total_price']);
                    $taxData['data']['tax_percentage'] = $cart['tax_percentage'];

                } elseif ($cart['tax_type'] == "none") {
                    $taxData['data']['amount'] = 0;
                    $taxData['data']['tax_percentage'] = 0;
                } else {
                    $taxData = $this->calculateTax($cart['city_id'], $cart['total_price']);
                    if ($taxData['status'] != 200) {

                        return $taxData;
                    }

                }

                /**
                 * Store updated data of given key.
                 */

                if (count($cartData) <= 0) {
                    Redis::del("identifier:" . $request['hash']);
                    if (!empty($cart['user_id'])) {
                        Redis::del("user-" . $cart['user_id']);
                    }
                    $cart['cart_data'] = [];
                    $cart['total_price'] = '0';
                    $cart['tax_amount'] = '0';
                    $cart['grand_total'] = '0';
                    return response()->json([
                        "status" => "200",
                        "message" => "Data deleted successfully",
                        "data" => array_except($cart, ['hash'])
                    ], 200);
                }

                Redis::hmset($key, [
                    'hash' => $request['hash'],
                    'user_id' => $cart['user_id'],
                    'cart_data' => serialize(array_values($cartData)),
                    'total_price' => $cart['total_price'],
                    'tax_amount' => $taxData['data']['amount'],
                    'tax_type' => $cart['tax_type'],
                    'tax_percentage' => $taxData['data']['tax_percentage'],
                    'grand_total' => $cart['total_price'] + $taxData['data']['amount'],
                    'updated_at' => time()

                ]);

                $data = Redis::hgetall($key);

                $getResponse = $this->responseFormat($data, $lang);
                try {
                    $error_data = \Settings::getErrorMessage('order-data-deleted-successfully');
                    if ($error_data["status"] != 200) {
                        throw new \Exception();
                    }
                    $error_message = $error_data['message'][$lang];
                    return response()->json([
                        "status" => "200",
                        "message" => $error_message,
                        "data" => $getResponse
                    ], 200);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "200",
                        "message" => "Data deleted successfully.",
                        "data" => $getResponse
                    ], 200);
                }


            } catch (\Exception $ex) {
                $this->logStoreHelper->storeLogError([
                    'Cart',
                    [
                        "status" => "500",
                        "message" => $ex->getMessage()
                    ]
                ]);
                return response()->json([
                    "status" => "500",
                    "message" => $ex->getMessage(),
                ], 500);
            }
        } else {
            try {
                $error_data = \Settings::getErrorMessage('order-requested-hash-not-found');
                if ($error_data["status"] != 200) {
                    throw new \Exception();
                }
                $error_message = $error_data['message'][$lang];
                return response()->json([
                    "status" => "404",
                    "message" => $error_message,
                ], 404);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Requested hash not found.",
                ], 404);
            }

        }
    }

    /**
     * If  user logs in for cart checkout assign user with the given cart hash.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignUserId(Request $request)
    {

        $lang = Input::get('lang', 'en');
        try {
            $this->validate($request, [

                "hash" => "required"
            ]);

        } catch (\Exception $e) {

            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }
        /**
         * Get user id from RoleChecker facade.
         */
        try {
            $user = RoleChecker::getUser();

            $key = "identifier:" . $request['hash'];
            /**
             * Check if the given key exists in redis db.
             * If key exists, proceed.
             * Else throw error message.
             */
            if (Redis::exists($key)) {

                $newHashData = Redis::hgetall($key);
                if (!empty($newHashData['user_id']) && $newHashData['user_id'] != $user) {
                    try {
                        $error_data = \Settings::getErrorMessage('order-cart-is-already-registered');
                        if ($error_data["status"] != 200) {
                            throw new \Exception();
                        }
                        $error_message = $error_data['message'][$lang];
                        return response()->json([
                            "status" => "403",
                            "message" => $error_message,
                        ], 403);
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "403",
                            "message" => "Cart is already registered.",
                        ], 403);
                    }

                }


                $oldHash = Redis::get("user-" . $user);
                /**
                 * If user has previous hash, then get all data related to hash.
                 */
                if (isset($oldHash) && !empty($oldHash)) {
                    if (Redis::exists("identifier:" . $oldHash)) {


                        $oldHashData = Redis::hgetall("identifier:" . $oldHash);


                        /**
                         * If request has hash, check if the input has is same as previous hash.
                         * If same, proceed.
                         * Else, throw error message.
                         */
                        if ($oldHashData['restaurant_id'] != $newHashData['restaurant_id']) {

                            $oldHashData = $this->responseFormat($oldHashData, $lang);
                            return response()->json([
                                "status" => "409",
                                "message" => "You have existing cart",
                                "data" => $oldHashData
                            ], 409);

                        } else {
                            if ($oldHash != $request['hash']) {

                                $cartDetail = [
                                    'cartData' => unserialize($oldHashData['cart_data']),
                                    'restaurant' => unserialize($oldHashData['restaurant_name']),
                                    'restaurant_slug' => $oldHashData['restaurant_slug'],
                                    'currency' => $oldHashData['currency'],
                                ];
                                $request['key'] = $key;
                                $this->update($request, $cartDetail);

                            }


                        }
                    } else {
                        Redis::del("identifier:" . $oldHash);
                        Redis::del("user-" . $user);
                    }
                }
                /**
                 *
                 */

                Redis::set("user-" . $user, $request['hash']);
                Redis::hmset($key, [
                    'user_id' => $user,

                ]);

                $data = Redis::hgetall($key);
                $data = $this->responseFormat($data, $lang);
                try {
                    $error_data = \Settings::getErrorMessage('order-user-assigned-to-the-cart-successfully');
                    if ($error_data["status"] != 200) {
                        throw new \Exception();
                    }
                    $error_message = $error_data['message'][$lang];
                    return response()->json([
                        "status" => "200",
                        "message" => $error_message,
                        "data" => $data
                    ], 200);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "200",
                        "message" => "User assigned to the cart successfully.",
                        "data" => $data
                    ], 200);
                }


            } else {
                try {
                    $error_data = \Settings::getErrorMessage('order-requested-hash-not-found');
                    if ($error_data["status"] != 200) {
                        throw new \Exception();
                    }
                    $error_message = $error_data['message'][$lang];
                    return response()->json([
                        "status" => "404",
                        "message" => $error_message
                    ], 404);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Requested hash not found."
                    ], 404);
                }

            }

        } catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError([
                'Cart',
                [
                    "status" => "500",
                    "message" => $ex->getMessage()
                ]
            ]);
            try {
                $error_data = \Settings::getErrorMessage('order-something-went-wrong');
                if ($error_data["status"] != 200) {
                    throw new \Exception();
                }
                $error_message = $error_data['message'][$lang];
                return response()->json([
                    "status" => "500",
                    "message" => $error_message
                ], 500);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "500",
                    "message" => "Something went wrong. Please try again later."
                ], 500);
            }

        }

    }

    /**
     * THis function overwrites if overwrite request is 1, else removes the recent hash if present and set old hash as active hash.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function overwriteCart(Request $request)
    {
        $lang = Input::get('lang', 'en');
        try {

            $this->validate($request, [
                "overwrite" => "required|integer|min:0|max:1",
            ]);

        } catch (\Exception $e) {

            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }
        /**
         * Get id of logged in user.
         * Check if user id has previous key. If yes, check overwrite request.
         * If overwrite request is 1, delete 'user-userid' key and set new 'user-userid' for new hash and delete old hash key.
         * If new hash is not provided set hashData empty.
         */
        try {
            $userId = RoleChecker::getUser();
            if (empty(Redis::get("user-" . $userId))) {
                try {
                    $error_data = \Settings::getErrorMessage('order-cart-related-to-user-not-found');
                    if ($error_data["status"] != 200) {
                        throw new \Exception();
                    }
                    $error_message = $error_data['message'][$lang];
                    return response()->json([
                        "status" => "404",
                        "message" => $error_message
                    ], 404);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Cart related to user not found."
                    ], 404);
                }

            }
            $key = ("identifier:" . Redis::get("user-" . $userId));


            try {
                if (Redis::exists($key)) {
                    $overwrite = $request['overwrite'];
                    if ($overwrite == 1) {

                        if ($request->has("recent_hash") && Redis::exists("identifier:" . $request['recent_hash'])) {
                            if (Redis::get("user-" . $userId) != $request['recent_hash']) {
                                Redis::del($key);
                                Redis::del("user-" . $userId);
                            }
                            $new_key = "identifier:" . $request['recent_hash'];

                            Redis::set("user-" . $userId, $request['recent_hash']);
                            Redis::hmset($new_key, [
                                'user_id' => $userId,
                            ]);

                            $hashData = Redis::hgetall($new_key);
                            $hashData = $this->responseFormat($hashData, $lang);

                        } elseif (!$request->has("recent_hash") && $request->has("restaurant_id") && $request->has("items")) {
                            try {
                                $this->validate($request, [

                                    "restaurant_id" => "required|integer",
                                    "items" => "required|array",
                                ]);

                            } catch (\Exception $e) {

                                $this->logStoreHelper->storeLogError([
                                    "Validation Error",
                                    [
                                        'status' => '422',
                                        "message" => $e->response->original
                                    ]

                                ]);
                                return response()->json([
                                    "status" => "422",
                                    "message" => $e->response->original
                                ], 422);
                            }
                            foreach ($request['items'] as $item) {

                                $rules = [
                                    "food_id" => "required|integer",
                                    "quantity" => 'required|integer|min:1',
                                    "addons" => 'array',
                                ];

                                $validator = Validator::make($item, $rules);
                                if ($validator->fails()) {
                                    $error = $validator->errors();
                                    $this->logStoreHelper->storeLogError([
                                        "Validation Error",
                                        [
                                            'status' => '422',
                                            "message" => $error
                                        ]

                                    ]);
                                    return response()->json([
                                        'status' => '422',
                                        "message" => $error
                                    ], 422);
                                }
                            }

                            /**
                             * Send items to restaurant service to validate.
                             * Display error message if the status code is other than 200.
                             * Else proceed.
                             */
                            $cartDetails = FoodData::getFood($request->all());

                            if ($cartDetails['status'] != 200) {


                                return response()->json([
                                    "status" => $cartDetails['status'],
                                    "message" => $cartDetails['message']
                                ], $cartDetails['status']);
                            }


                            Redis::del($key);
                            Redis::del("user-" . $userId);

                            /**
                             * Send details to create function and add data to redis db.
                             */
                            $request['user_id'] = $userId;
                            $data = $this->create($request, $cartDetails['data']);

                            $data = $this->responseFormat($data, $lang);


                            /**
                             * Store into lumen.log
                             */
                            $this->logStoreHelper->storeLogInfo([
                                'Cart',
                                [
                                    "status" => "200",
                                    "message" => "Cart is overwritten successfully",
                                    "data" => $data
                                ]
                            ]);
                            try {
                                $error_data = \Settings::getErrorMessage('order-current-cart-data');
                                if ($error_data["status"] != 200) {
                                    throw new \Exception();
                                }
                                $error_message = $error_data['message'][$lang];
                                return response()->json([
                                    "status" => "200",
                                    "message" => $error_message,
                                    "data" => $data
                                ], 200);
                            } catch (\Exception $ex) {
                                return response()->json([
                                    "status" => "200",
                                    "message" => "Current Cart Data",
                                    "data" => $data
                                ], 200);
                            }

                        } else {
                            Redis::del($key);
                            Redis::del("user-" . $userId);
                            $hashData = new \stdClass();
                        }

                        try {
                            $error_data = \Settings::getErrorMessage('order-current-cart-data');
                            if ($error_data["status"] != 200) {
                                throw new \Exception();
                            }
                            $error_message = $error_data['message'][$lang];
                            $message = $error_message;
                        } catch (\Exception $ex) {
                            $message = "Current Cart Data";
                        }


                    } /**
                     * Else get all data of old hash.
                     * If new hash key is given, delete it.
                     *
                     */ else {
                        $hashData = Redis::hgetall($key);

                        $hashData = $this->responseFormat($hashData, $lang);


                        if ($request->has("recent_hash") && Redis::exists("identifier:" . $request['recent_hash']) && $key != "identifier:" . $request['recent_hash']) {

                            Redis::del("identifier:" . $request['recent_hash']);
                        }

                        try {
                            $error_data = \Settings::getErrorMessage('order-data-of-previous-cart');
                            if ($error_data["status"] != 200) {
                                throw new \Exception();
                            }
                            $error_message = $error_data['message'][$lang];
                            $message = $error_message;
                        } catch (\Exception $ex) {
                            $message = "Data of previous cart.";
                        }

                    }

                    return response()->json([
                        "status" => "200",
                        "message" => $message,
                        "data" => $hashData
                    ], 200);
                } else {
                    Redis::del($key);
                    Redis::del("user-" . $userId);

                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                try {
                    $error_data = \Settings::getErrorMessage('order-requested-hash-not-found');
                    if ($error_data["status"] != 200) {
                        throw new \Exception();
                    }
                    $error_message = $error_data['message'][$lang];
                    return response()->json([
                        "status" => "404",
                        "message" => $error_message
                    ], 404);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Requested hash not found."
                    ], 404);
                }

            }

        } catch (\Exception $exception) {
            $this->logStoreHelper->storeLogError([
                'Cart',
                [
                    "status" => "500",
                    "message" => $exception->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ], 500);

        }

    }

    /**
     * Update quantity of specific food.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateQuantity(Request $request)
    {
        $lang = Input::get('lang', 'en');

        /**
         * Validation for the input given.
         * If validation fails, throw appropriate error message, else continue.
         */
        try {
            $this->validate($request, [

                "hash" => "required",
                "item" => "required|array",
                "item.id" => "required|integer",
                "item.quantity" => "required|integer",
                "item.addons" => "array",
                //"item.size" => "present"
            ]);

        } catch (\Exception $e) {

            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }

        $rules = [
            "size" => "present"
        ];
        $validator = Validator::make($request['item'], $rules);

        if ($validator->fails()) {
            $error = $validator->errors();
            $this->logStoreHelper->storeLogError([
                'Validation Error',
                [
                    'status' => '422',
                    "message" => $error
                ]]);
            return response()->json([
                'status' => '422',
                "message" => $error
            ], 422);
        }

        /**
         * Use request to get hash and get key to fetch data from redis db.
         * Check if $key exists.
         */
        try {
            $key = "identifier:" . $request['hash'];

            if (Redis::exists($key)) {


                /**
                 * unset $data array to ensure it doesnot contain garbage value.
                 * Use request to get hash and get key to fetch data from redis db.
                 */
                unset($data);


                $hashData = Redis::hgetall($key);
                /**
                 * Get cart_data stored in the given key from redis db and assign to $cartData
                 */
                $cartData = unserialize($hashData['cart_data']);

                /**
                 * Get total_price stored in given key from redis db and assign to $totalPrice
                 */

                $totalPrice = $hashData['total_price'];

                /**
                 * $cartDetail is array of input containing food and its respective addons.
                 *
                 */

                $item = array_except($request['item'], ['quantity']);


                foreach ($cartData as $index => $food) {


                    /**
                     * Check if id from input matches the unserialized food data.
                     * If data does not match, check another row.
                     *  If matches, do further operations.
                     */
                    if (($item['id'] == $food['id']) && ($item['size'] == $food['size_slug']) && ($item['special_instruction'] == $food['special_instruction'])) {


                        if ((isset($item['addons']) && !empty($item['addons'])) && !empty($food['addons'])) {

                            unset($addonIds);
                            foreach ($food['addons'] as $addon) {

                                $addonIds[] = $addon['id'];
                            }

                            /**
                             * If $addonIds and $detail['addons'] are identical, do further operations .
                             * Subtract $food['sub_total'] from the cart['total_price'] and break the operation.
                             */
                            if ($item['addons'] == $addonIds) {

                                $food['quantity'] += $request['item']['quantity'];

                                $totalPrice -= $food['sub_total'];


                                /**
                                 * If food quantity is less than 1, delete the food.
                                 */
                                if ($food['quantity'] < 1) {
                                    unset($cartData[$index]);
                                } else {
                                    $food['sub_total'] = $food['quantity'] * $food['total_unit_price'];
                                    $cartData[$index] = $food;
                                    $totalPrice += $food['sub_total'];
                                    break;
                                }

                            }

                        } /**
                         * If $addonIds and $detail['addons'] are empty, do further operations .
                         * Subtract $food['sub_total'] from the cart['total_price'] and break the operation.
                         */
                        elseif (empty($item['addons']) && empty($food['addons'])) {
                            $food['quantity'] += $request['item']['quantity'];
                            $totalPrice -= $food['sub_total'];

                            if ($food['quantity'] < 1) {
                                unset($cartData[$index]);
                            } else {
                                $food['sub_total'] = $food['quantity'] * $food['total_unit_price'];
                                $cartData[$index] = $food;
                                $totalPrice += $food['sub_total'];
                                break;
                            }

                        }


                    }

                }
                if (!isset($hashData['tax_type'])) {
                    $hashData['tax_type'] = "exclusive";

                }
                if ($hashData['tax_type'] == "inclusive") {
                    $taxData['data']['amount'] = $this->calculateTaxForInclusive($hashData['tax_percentage'],$totalPrice);
                    $taxData['data']['tax_percentage'] = $hashData['tax_percentage'];

                } elseif ($hashData['tax_type'] == "none") {
                    $taxData['data']['amount'] = 0;
                    $taxData['data']['tax_percentage'] = 0;
                }else
                 {
                    $taxData = $this->calculateTax($hashData['city_id'], $totalPrice);
                    if ($taxData['status'] != 200) {

                        return $taxData;
                    }

                }

                if (count($cartData) <= 0) {
                    Redis::del("identifier:" . $request['hash']);
                    if (!empty($cart['user_id'])) {
                        Redis::del("user-" . $cart['user_id']);
                    }
                    $hashData['cart_data'] = [];
                    $hashData['cart_data'] = [];
                    $hashData['total_price'] = '0';
                    $hashData['tax_amount'] = '0';
                    $hashData['grand_total'] = '0';
                    try {
                        $error_data = \Settings::getErrorMessage('order-quantity-updated-successfully');
                        if ($error_data["status"] != 200) {
                            throw new \Exception();
                        }
                        $error_message = $error_data['message'][$lang];
                        return response()->json([
                            "status" => "200",
                            "message" => $error_message,
                            "data" => array_except($hashData, ['hash'])
                        ], 200);
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "200",
                            "message" => "Quantity updated successfully.",
                            "data" => array_except($hashData, ['hash'])
                        ], 200);
                    }

                }

                /**
                 * Store updated data to the given key.
                 */
                Redis::hmset($key, [
                    'hash' => $request['hash'],
                    'user_id' => $hashData['user_id'],
                    'cart_data' => serialize($cartData),
                    'total_price' => $totalPrice,
                    'tax_amount' => $taxData['data']['amount'],
                    'tax_percentage' => $taxData['data']['tax_percentage'],
                    'grand_total' => $totalPrice + $taxData['data']['amount'],
                    'tax_type' => $hashData['tax_type'],
                    'updated_at' => time()

                ]);

                $data = Redis::hgetall($key);

                $data = $this->responseFormat($data, $lang);
                try {
                    $error_data = \Settings::getErrorMessage('order-quantity-updated-successfully');
                    if ($error_data["status"] != 200) {
                        throw new \Exception();
                    }
                    $error_message = $error_data['message'][$lang];
                    return response()->json([
                        "status" => "200",
                        "message" => $error_message,
                        "data" => $data
                    ], 200);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "200",
                        "message" => "Quantity updated successfully.",
                        "data" => $data
                    ], 200);
                }


            } else {
                try {
                    $error_data = \Settings::getErrorMessage('order-requested-hash-not-found');
                    if ($error_data["status"] != 200) {
                        throw new \Exception();
                    }
                    $error_message = $error_data['message'][$lang];
                    return response()->json([
                        "status" => "404",
                        "message" => $error_message
                    ], 404);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Requested hash not found."
                    ], 404);
                }
            }
        } catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError([
                'Cart',
                [
                    "status" => "500",
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage(),

            ], 500);

        }
    }

    /**
     * Reorder the cart user previously selected.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder($id)
    {
        try {
            $lang = Input::get("lang", "en");

            $userId = RoleChecker::getUser();

            $oldHash = Redis::get("user-" . $userId);

            /**
             * If user has previous hash, then get all data related to hash.
             */
            if (isset($oldHash) && !empty($oldHash)) {

                $hashData = Redis::hgetall("identifier:" . $oldHash);

                $hashData = $this->responseFormat($hashData, $lang);
                /**
                 * If request has hash, check if the input has is same as previous hash.
                 * If same, proceed.
                 * Else,send error message.
                 */
                try {
                    $error_data = \Settings::getErrorMessage('order-you-have-existing-cart');
                    if ($error_data["status"] != 200) {
                        throw new \Exception();
                    }
                    $error_message = $error_data['message'][$lang];
                    return response()->json([
                        "status" => "409",
                        "message" => $error_message,
                        "data" => $hashData
                    ], 409);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "409",
                        "message" => "You have existing cart.",
                        "data" => $hashData
                    ], 409);
                }

            }
            /**
             * Fetch successful order of the user and get restaurant_id and items he ordered.
             * Pass request to remote restaurant service and validate data
             * If response is other than 200, send error message along with status code.
             * Else create new hash key and store cart details.
             *
             */
            $itemDetails = $this->order->getOrderDetailsForReorder($id, $userId);
            //return $itemDetails;


            if (!$itemDetails) {
                try {
                    $error_data = \Settings::getErrorMessage('order-your-order-is-not-successfully-processed-yet');
                    if ($error_data["status"] != 200) {
                        throw new \Exception();
                    }
                    $error_message = $error_data['message'][$lang];
                    return response()->json([
                        "status" => "403",
                        "message" => $error_message,
                    ], 403);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "403",
                        "message" => "Your order is not successfully processed yet.",
                    ], 403);
                }

            }
            $request['restaurant_id'] = $itemDetails['restaurant_id'];
            $request['items'] = $itemDetails['items'];

            $cartDetails = FoodData::getFood($request);

            if ($cartDetails['status'] != 200) {
                return response()->json([
                    "status" => $cartDetails['status'],
                    "message" => $cartDetails['message']
                ], $cartDetails['status']);
            }
            $request['user_id'] = $userId;
            /**
             * Pass cartDetails['data'] to create function and store response in $data.
             *
             */
            $data = $this->create($request, $cartDetails['data']);

            $data = $this->responseFormat($data, $lang);
            try {
                $error_data = \Settings::getErrorMessage('order-reordered');
                if ($error_data["status"] != 200) {
                    throw new \Exception();
                }
                $error_message = $error_data['message'][$lang];
                return response()->json([
                    "status" => "200",
                    "message" => $error_message,
                    "data" => $data
                ], 200);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "200",
                    "message" => "Reordered.",
                    "data" => $data
                ], 200);
            }


        } catch (ModelNotFoundException $ex) {
            try {
                $error_data = \Settings::getErrorMessage('order-requested-order-not-found');
                if ($error_data["status"] != 200) {
                    throw new \Exception();
                }
                $error_message = $error_data['message'][$lang];
                return response()->json([
                    "status" => "404",
                    "message" => $error_message,
                ], 404);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Requested order not found.",
                ], 404);
            }

        } catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError([
                'Cart',
                [
                    "status" => "500",
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage(),

            ], 500);

        }

    }

    /**
     * Count no of items of a cart
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ItemCount(Request $request)
    {
        $lang = Input::get('lang', 'en');
        /**
         * Validation for the input given.
         * If validation fails, throw appropriate error message, else continue.
         */
        try {
            $this->validate($request, [

                "hash" => "required",
            ]);

        } catch (\Exception $e) {

            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }

        /**
         * Use request to get hash and get key to fetch data from redis db.
         * Check if $key exists.
         */
        try {
            $key = "identifier:" . $request['hash'];

            if (Redis::exists($key)) {

                /**
                 * Get count of cart_data array stored in the given key from redis db and assign to $cartCount
                 */
                $cartCount = count(unserialize(Redis::hget($key, 'cart_data')));
                try {
                    $error_data = \Settings::getErrorMessage('order-items-in-cart');
                    if ($error_data["status"] != 200) {
                        throw new \Exception();
                    }
                    $error_message = $error_data['message'][$lang];
                    return response()->json([
                        "status" => "200",
                        "message" => $error_message,
                        "data" => $cartCount
                    ], 200);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "200",
                        "message" => "Items in cart.",
                        "data" => $cartCount
                    ], 200);
                }


            } else {
                try {
                    $error_data = \Settings::getErrorMessage('order-requested-hash-not-found');
                    if ($error_data["status"] != 200) {
                        throw new \Exception();
                    }
                    $error_message = $error_data['message'][$lang];
                    return response()->json([
                        "status" => "404",
                        "message" => $error_message
                    ], 404);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Requested hash not found."
                    ], 404);
                }
            }
        } catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError([
                'Cart',
                [
                    "status" => "500",
                    "message" => $ex->getMessage()
                ]
            ]);

            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage(),

            ], 500);
        }
    }

    /**
     * Get cart of a user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCartOfUser()
    {
        $lang = Input::get('lang', 'en');
        try {
            $userId = RoleChecker::getUser();
            $hash = Redis::get("user-" . $userId);

            if (!empty($hash) && Redis::exists("identifier:" . $hash)) {

                $hashData = Redis::hgetall("identifier:" . $hash);

                $hashData = $this->responseFormat($hashData, $lang);

            } else {

                $hashData = new \stdClass();
            }
            return response()->json([
                "status" => "200",
                "data" => $hashData,

            ], 200);
        } catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError([
                'Cart',
                [
                    "status" => "500",
                    "message" => $ex->getMessage()
                ]
            ]);

            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage(),
            ], 500);

        }

    }

    /**
     * @param $foodData
     * @param $lang
     * @return array
     */
    private function foodTranslate($foodData, $lang)
    {

        foreach ($foodData as $cartData) {
            if (isset($cartData['name']['en'])) {
                $cartData['name'] = isset($cartData['name'][$lang]) ? $cartData['name'][$lang] : $cartData['name']['en'];
            } else {
                $cartData['name'] = '';
            }


            if (!empty($cartData['addons'])) {
                unset($addonCheck);
                foreach ($cartData['addons'] as $adon) {
                    if (isset($adon['name']['en'])) {
                        $adon['name'] = isset($adon['name'][$lang]) ? $adon['name'][$lang] : $adon['name']['en'];
                    } else {
                        $adon['name'] = '';
                    }
                    if (isset($adon['category_name']['en'])) {
                        $adon['category_name'] = isset($adon['category_name'][$lang]) ? $adon['category_name'][$lang] : $adon['category_name']['en'];
                    } else {
                        $adon['category_name'] = '';
                    }


                    $addonCheck[] = $adon;
                }
                $cartData['addons'] = $addonCheck;
            }
            $title = '';
            if (!empty($cartData['size_slug'])) {
                if (isset($cartData['size_title']['en'])) {
                    $title = isset($cartData['size_title'][$lang]) ? $cartData['size_title'][$lang] : $cartData['size_title']['en'];
                } else {
                    $title = '';
                }

            }
            $cartData['size_title'] = $title;

            $check[] = $cartData;


        }

        return $check;
    }

    /**
     * @param $resData
     * @param $lang
     * @return string
     */
    private function restaurantTranslate($resData, $lang)
    {
        if (isset($resData['en'])) {
            $resName = isset($resData[$lang]) ? $resData[$lang] : $resData['en'];
        } else {
            $resName = '';
        }
        return $resName;
    }

    /**
     * Get cart details of a requested hash of cart
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCart(Request $request)
    {
        $lang = Input::get('lang', 'en');
        try {
            $this->validate($request, [

                "hash" => "required",

            ]);

        } catch (\Exception $e) {

            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }
        try {
            $hash = $request['hash'];

            if (!empty($hash) && Redis::exists("identifier:" . $hash)) {

                $hashData = Redis::hgetall("identifier:" . $hash);
                if (empty($hashData['user_id'])) {
                    $hashData = $this->responseFormat($hashData, $lang);

                    return response()->json([
                        "status" => "200",
                        "data" => $hashData,

                    ], 200);
                }

            }
            try {
                $error_data = \Settings::getErrorMessage('order-requested-hash-not-found');
                if ($error_data["status"] != 200) {
                    throw new \Exception();
                }
                $error_message = $error_data['message'][$lang];
                return response()->json([
                    "status" => "404",
                    "message" => $error_message
                ], 404);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Requested hash not found."
                ], 404);
            }


        } catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError([
                'Cart',
                [
                    "status" => "500",
                    "message" => $ex->getMessage()
                ]
            ]);

            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage(),
            ], 500);

        }
    }

    /**
     * Overwrite option for guest user to add different restaurant cart.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function overwriteGuestCart(Request $request)
    {
        $request['user_id'] = null;
        $lang = Input::get('lang', 'en');
        /**
         * Validate request and send error message in case validation fails.
         * Else continue.
         */
        try {

            $this->validate($request, [
                "overwrite" => "required|integer|min:0|max:1",
                "hash" => "required"
            ]);

        } catch (\Exception $e) {

            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }

        try {
            /**
             * Check if given hash exists in redis database.
             * If exists continue, else throw error message.
             */
            if (Redis::exists("identifier:" . $request['hash']) && empty(Redis::hget("identifier:" . $request['hash'], 'user_id'))) {
                /**
                 * If overwrite = 1, check the optional requests validity.
                 * Overwrite existing hash data with new data and delete the hash key from redis.
                 */
                if ($request['overwrite'] == 1) {

                    $data = new \stdClass();
                    if ($request->has('restaurant_id') || $request->has('items')) {
                        /**
                         * Validation for the input given.
                         * If validation fails, throw appropriate error message, else continue.
                         */
                        try {
                            $this->validate($request, [

                                "restaurant_id" => "required|integer",
                                "items" => "required|array",
                            ]);

                        } catch (\Exception $e) {

                            $this->logStoreHelper->storeLogError([
                                "Validation Error",
                                [
                                    'status' => '422',
                                    "message" => $e->response->original
                                ]

                            ]);
                            return response()->json([
                                "status" => "422",
                                "message" => $e->response->original
                            ], 422);
                        }
                        foreach ($request['items'] as $item) {

                            $rules = [
                                "food_id" => "required|integer",
                                "quantity" => 'required|integer|min:1',
                                "addons" => 'array',
                            ];

                            $validator = Validator::make($item, $rules);
                            if ($validator->fails()) {
                                $error = $validator->errors();
                                $this->logStoreHelper->storeLogError([
                                    "Validation Error",
                                    [
                                        'status' => '422',
                                        "message" => $error
                                    ]

                                ]);
                                return response()->json([
                                    'status' => '422',
                                    "message" => $error
                                ], 422);
                            }
                        }

                        /**
                         * Send items to restaurant service to validate.
                         * Display error message if the status code is other than 200.
                         * Else proceed.
                         */
                        $cartDetails = FoodData::getFood($request->all());

                        if ($cartDetails['status'] != 200) {


                            return response()->json([
                                "status" => $cartDetails['status'],
                                "message" => $cartDetails['message']
                            ], $cartDetails['status']);
                        }
                        /**
                         * Send details to create function and add data to redis db.
                         */
                        $data = $this->create($request, $cartDetails['data']);

                        $data = $this->responseFormat($data, $lang);


                    }
                    Redis::del("identifier:" . $request['hash']);
                    /**
                     * Store into lumen.log
                     */
                    $this->logStoreHelper->storeLogInfo([
                        'Cart',
                        [
                            "status" => "200",
                            "message" => "Data overwritten successfully",
                            "data" => $data
                        ]
                    ]);
                    try {
                        $error_data = \Settings::getErrorMessage('order-cart-is-overwritten-successfully');
                        if ($error_data["status"] != 200) {
                            throw new \Exception();
                        }
                        $error_message = $error_data['message'][$lang];
                        return response()->json([
                            "status" => "200",
                            "message" => $error_message,
                            "data" => $data
                        ], 200);
                    } catch (\Exception $ex) {
                        return response()->json([
                            "status" => "200",
                            "message" => "Cart is overwritten successfully.",
                            "data" => $data
                        ], 200);
                    }


                } /**
                 * If overwrite = 0, return cart data to user.
                 */
                else {

                    $hashData = Redis::hgetall("identifier:" . $request['hash']);

                    $hashData = $this->responseFormat($hashData, $lang);

                    return response()->json([
                        "status" => "200",
                        "message" => "Current Cart Data",
                        "data" => $hashData,
                    ], 200);

                }

            } else {
                try {
                    $error_data = \Settings::getErrorMessage('order-requested-hash-not-found');
                    if ($error_data["status"] != 200) {
                        throw new \Exception();
                    }
                    $error_message = $error_data['message'][$lang];
                    return response()->json([
                        "status" => "404",
                        "message" => $error_message
                    ], 404);
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Requested hash not found."
                    ], 404);
                }
            }

        } catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError([
                'Cart',
                [
                    "status" => "500",
                    "message" => $ex->getMessage()
                ]
            ]);

            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage(),
            ], 500);
        }

    }

    private function responseFormat($data, $lang)
    {
        $data['cart_data'] = (empty(unserialize($data['cart_data'])) ? unserialize($data['cart_data']) : $this->foodTranslate(unserialize($data['cart_data']), $lang));
        $data['total_price'] = (string)round($data['total_price'], 2);

        $data['restaurant_id'] = (integer)($data['restaurant_id']);
        $data['restaurant_name'] = $this->restaurantTranslate(unserialize($data['restaurant_name']), $lang);
        $data['user_id'] = empty($data['user_id']) ? "" : (integer)$data['user_id'];
        $data['currency'] = isset($data['currency']) ? $data['currency'] : '';
        $data['tax_amount'] = isset($data['tax_amount']) ? (string)(round($data['tax_amount'], 2)) : '0';

        $data['grand_total'] = isset($data['grand_total']) ? (string)(round($data['grand_total'], 2)) : '0';
        $data['tax_percentage'] = isset($data['tax_percentage']) ? (string)(round($data['tax_percentage'], 2)) : '0';
        $data['created_at'] = date("Y-m-d H:i:s", $data['created_at']);
        $data['updated_at'] = date("Y-m-d H:i:s", $data['updated_at']);

        return $data;
    }

    public function removeUserCart()
    {
        try {

            $userId = RoleChecker::getUser();
            $hash = Redis::get("user-" . $userId);

            if (!empty($hash) && Redis::exists("identifier:" . $hash)) {

                Redis::del("user-" . $userId);
                Redis::del("identifier:" . $hash);
                return response()->json([
                    "status" => "200",
                    "message" => "Cart removed"
                ], 200);

            } else {
                return response()->json([
                    "status" => "404",
                    "message" => "Cart not found"
                ], 404);
            }
        } catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError([
                'Cart',
                [
                    "status" => "500",
                    "message" => $ex->getMessage()
                ]
            ]);

            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage(),
            ], 500);
        }


    }

    private function calculateTax($cityId, $totalAmount)
    {

        $request['city_id'] = (integer)$cityId;
        $request['amount'] = $totalAmount;

        $taxData = FoodData::getTaxAmount($request);
        return $taxData;
    }

    public function removeGuestCart(Request $request)
    {
        /**
         * Validate request and send error message in case validation fails.
         * Else continue.
         */
        try {

            $this->validate($request, [

                "hash" => "required"
            ]);

        } catch (\Exception $e) {

            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }

        try {

            if (Redis::exists("identifier:" . $request['hash']) && empty(Redis::hget("identifier:" . $request['hash'], 'user_id'))) {

                Redis::del("identifier:" . $request['hash']);
                return response()->json([
                    "status" => "200",
                    "message" => "Cart removed"
                ], 200);

            } else {
                return response()->json([
                    "status" => "404",
                    "message" => "Cart not found"
                ], 404);
            }
        } catch (\Exception $ex) {
            $this->logStoreHelper->storeLogError([
                'Cart',
                [
                    "status" => "500",
                    "message" => $ex->getMessage()
                ]
            ]);

            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage(),
            ], 500);
        }

    }

    public function calculateTaxForInclusive($taxRate, $total)
    {
        $taxAmount = $total * ($taxRate * 0.01);

        return $taxAmount;

    }

}