<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 12:17 PM
 */

namespace App\Http\Controllers;

use App\Repo\OrderRejectStatusTranslationInterface;
use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repo\OrderRejectStatusInterface;
use LogStoreHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Validator;
use RoleChecker;
use Yajra\Datatables\Datatables;

/**
 * Class OrderRejectStatusController
 * @package App\Http\Controllers
 */
class OrderRejectStatusController extends Controller
{
    /**
     * @var OrderRejectStatusInterface
     */
    protected $orderRejectStatus;
    /**
     * @var OrderRejectStatusTranslationInterface
     */
    protected $orderRejectStatusTranslation;

    /**
     * @var Slugify
     */
    protected $slugify;

    /**
     * @var LogStoreHelper
     */
    protected $logStoreHelper;

    /**
     * OrderRejectStatusController constructor.
     * @param $orderRejectStatus
     * @param $orderRejectStatusTranslation
     */
    public function __construct(LogStoreHelper $logStoreHelper, Slugify $slugify, OrderRejectStatusInterface $orderRejectStatus, OrderRejectStatusTranslationInterface $orderRejectStatusCreated)
    {
        $this->orderRejectStatus = $orderRejectStatus;
        $this->slugify = $slugify;
        $this->logStoreHelper = $logStoreHelper;
        $this->orderRejectStatusTranslation = $orderRejectStatusCreated;
    }

    public function index()
    {
        try {
            if (RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin") || RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")) {
                $orderRejectStatuses = $this->orderRejectStatus->getAllEnabledOrderRejectStatus();
            } else {
                $orderRejectStatuses = $this->orderRejectStatus->getAllOrderRejectStatus();
            }
            if (!$orderRejectStatuses->first()) {
                throw  new \Exception();
            }
            foreach ($orderRejectStatuses as $key => $orderStatus) {
                $orderRejectStatusTranslation = $orderStatus->rejectStatusTranslation()->where('lang_code', "en")->get();

                try {
                    if ($orderRejectStatusTranslation->count() == 0) {
                        throw new \Exception();
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }
                /*
                 * attach name and lang_code in $restaurant variable
                 * */

                $orderStatus['name'] = $orderRejectStatusTranslation[0]['name'];
                $orderStatus['lang_code'] = $orderRejectStatusTranslation[0]['lang_code'];
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Order Reject Status"
            ], 404);
        }


        return Datatables::of($orderRejectStatuses)->make(true);
//        return response()->json([
//            "status" => "200",
//            "data" => $orderRejectStatuses
//        ]);

    }

    public function publicIndex()
    {
        $request['lang'] = Input::get('lang', 'en');
        try {
            $orderRejectStatuses = $this->orderRejectStatus->getAllEnabledOrderRejectStatus();

            if (!$orderRejectStatuses->first()) {
                throw  new \Exception();
            }
            $orderRejectStatuses = collect($orderRejectStatuses)->sortBy("sort")->values();
            foreach ($orderRejectStatuses as $key => $orderStatus) {
                $orderRejectStatusTranslation = $orderStatus->rejectStatusTranslation()->where('lang_code', $request['lang'])->get();

                try {
                    if ($orderRejectStatusTranslation->count() == 0) {
                        $orderRejectStatusTranslation = $orderStatus->rejectStatusTranslation()->where('lang_code', $request['lang'])->get();
                        if ($orderRejectStatusTranslation->count() == 0) {
                            throw new \Exception();
                        }
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }
                /*
                 * attach name and lang_code in $restaurant variable
                 * */

                $orderStatus['name'] = $orderRejectStatusTranslation[0]['name'];
                $orderStatus['lang_code'] = $orderRejectStatusTranslation[0]['lang_code'];
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Order Reject Status"
            ], 404);
        }

        return response()->json([
            "status" => "200",
            "data" => $orderRejectStatuses
        ], 200);


    }

    /**
     * Returns specific order reject status
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {

        $request['lang'] = Input::get("lang", 'en');
        try {
            $specificOrderRejectStatus = $this->orderRejectStatus->getSpecificOrderRejectStatusEnabled($id);
            $this->logStoreHelper->storeLogError(array("Order Reject Status", [
                "status" => "200",
                "data" => $specificOrderRejectStatus
            ]));

            $orderRejectStatusTranslation = $specificOrderRejectStatus->rejectStatusTranslation()->get();

            try {
                $orderRejectStatusTranslation = $specificOrderRejectStatus->rejectStatusTranslation()->get();
                if ($orderRejectStatusTranslation->count() == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }
            /*
             * attach name and lang_code in $restaurant variable
             * */

            $specificOrderRejectStatus['translation'] = $orderRejectStatusTranslation;

            return response()->json([
                "status" => "200",
                "data" => $specificOrderRejectStatus
            ], 200);
        } catch (ModelNotFoundException $exception) {
            $this->logStoreHelper->storeLogInfo(array("Order Reject Status", [
                "status" => "404",
                "message" => "Request Order Reject Status does not exist"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Request Order Reject Status does not exist"
            ], 404);
        }
    }

    public function publicShow($id)
    {
        $request['lang'] = Input::get("lang", 'en');
        try {
            $specificOrderRejectStatus = $this->orderRejectStatus->getSpecificOrderRejectStatusEnabled($id);
            $this->logStoreHelper->storeLogError(array("Order Reject Status", [
                "status" => "200",
                "data" => $specificOrderRejectStatus
            ]));

            $orderRejectStatusTranslation = $specificOrderRejectStatus->rejectStatusTranslation()->where('lang_code', $request['lang'])->get();

            try {
                if ($orderRejectStatusTranslation->count() == 0) {
                    $orderRejectStatusTranslation = $specificOrderRejectStatus->rejectStatusTranslation()->where('lang_code', "en")->get();
                    if ($orderRejectStatusTranslation->count() == 0) {
                        throw new \Exception();
                    }
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }
            /*
             * attach name and lang_code in $restaurant variable
             * */

            $specificOrderRejectStatus['name'] = $orderRejectStatusTranslation[0]['name'];
            $specificOrderRejectStatus['lang_code'] = $orderRejectStatusTranslation[0]['lang_code'];

            return response()->json([
                "status" => "200",
                "data" => $specificOrderRejectStatus
            ], 200);
        } catch (ModelNotFoundException $exception) {
            $this->logStoreHelper->storeLogInfo(array("Order Reject Status", [
                "status" => "404",
                "message" => "Request Order Reject Status does not exist"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Request Order Reject Status does not exist"
            ], 404);
        }
    }

    /**
     * Create order reject status.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                "status" => "integer|min:0|max:1",
                "sort" => "integer",
                "translation" => "required|array"
            ]);
        } catch (\Exception $exception) {
            $this->logStoreHelper->storeLogError(array("Order Reject Status ", [
                "status" => '422',
                "message" => $exception->response->original
            ]));

            return response()->json([
                "status" => "422",
                "message" => $exception->response->original
            ], 422);
        }
        $request['sort'] = Input::get('sort', 1);

        try {
            DB::beginTransaction();
            $orderRejectStatus = $request->all();
            $orderRejectStatus["status"] = $request->input('status', 1);

            $orderRejectStatusCreated = $this->orderRejectStatus->createOrderRejectStatus($orderRejectStatus);

            foreach ($orderRejectStatus['translation'] as $key => $translation) {
                $translation['order_reject_status_id'] = $orderRejectStatusCreated->id;
                $translation['lang_code'] = $key;
                $orderStatusId = $orderRejectStatusCreated->id;
                $rules = [
                    "lang_code" => 'required|alpha',
                    "name" => "required"
                ];
                $translation['lang_code'] = $this->slugify->slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
                if (empty($translation['lang_code'])) {
                    return response()->json([
                        "status" => "422",
                        "message" => ["translation" => ["lang_code" => "The lang_code may not only contain numbers,special characters."]]
                    ], 422);
                }
                $validator = Validator::make($translation, $rules);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError(array('Order Status Translation', [
                        'status' => '422',
                        "message" => ["translation" => [$key => $error]]
                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => ["translation" => [$key => $error]]
                    ], 422);
                }
                /*
                 * checking if the same order_status_id id have existing lang_code or not
                 * */
                try {
                    $createTranslation = $this->orderRejectStatusTranslation->createOrderRejectStatusTranslation($translation);
                    $this->logStoreHelper->storeLogInfo(array("Order Status Translation", [
                        "status" => "200",
                        "message" => "Order Status Translation Created Successfully",
                        "data" => $createTranslation
                    ]));

                } catch (\Exception $ex) {
                    $this->logStoreHelper->storeLogError(array("Order Status", [
                        "status" => "409",
                        "message" => "Language translation for given Order Reject Status already exist"
                    ]));
                    return response()->json([
                        "status" => "409",
                        "message" => "Language translation for given Order Reject Status already exist"
                    ], 409);
                }
            }

            $this->logStoreHelper->storeLogInfo(array("Order Reject Status", [
                "status" => "200",
                "message" => "Order Reject Status Created Successfully",
                "data" => $orderRejectStatusCreated
            ]));
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Order Reject Status created successfully"
            ]);

        } catch (\Exception $exception) {
            return response()->json([
                "status" => "500",
                "message" => "Error Creating Order status"
            ], 500);
        }
    }

    /**
     * update order reject status.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        try {

            $this->validate($request, [
                "status" => "integer|min:0|max:1",
                "sort" => "integer",
                "translation" => "required|array"
            ]);
        } catch (\Exception $exception) {
            $this->logStoreHelper->storeLogError(array("Order Reject Status ", [
                "status" => '422',
                "message" => $exception->response->original
            ]));

            return response()->json([
                "status" => "422",
                "message" => $exception->response->original
            ], 422);
        }
        $request['sort'] = Input::get('sort', 1);

        try {
            DB::beginTransaction();
            /**
             * checking order reject status exist for update
             */
            try {
                $specificOrderRejectStatus = $this->orderRejectStatus->getSpecificOrderRejectStatus($id);
            } catch (ModelNotFoundException $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Order Reject Status could not be found"
                ], 404);
            }
            $orderRejectStatus = $request->all();
            $orderRejectStatusUpdated = $this->orderRejectStatus->updateOrderRejectStatus($id, $orderRejectStatus);
            $this->orderRejectStatusTranslation->deleteOrderRejectStatusTranslationByOrderRejectStatusId($id);

            foreach ($orderRejectStatus['translation'] as $key => $translation) {
                $translation['order_reject_status_id'] = $id;
                $translation['lang_code'] = $key;
                $orderStatusId = $id;
                $rules = [
                    "lang_code" => 'required|alpha',
                    "name" => "required"
                ];
                $translation['lang_code'] = $this->slugify->slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
                if (empty($translation['lang_code'])) {
                    return response()->json([
                        "status" => "422",
                        "message" => ["translation" => ["lang_code" => "The lang_code may not only contain numbers,special characters."]]
                    ], 422);
                }
                $validator = Validator::make($translation, $rules);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError(array('Order Status Translation', [
                        'status' => '422',
                        "message" => ["translation" => [$key => $error]]
                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => ["translation" => [$key => $error]]
                    ], 422);
                }
                /*
                 * checking if the same order_status_id id have existing lang_code or not
                 * */
                try {
                    $createTranslation = $this->orderRejectStatusTranslation->createOrderRejectStatusTranslation($translation);
                    $this->logStoreHelper->storeLogInfo(array("Order Status Translation", [
                        "status" => "200",
                        "message" => "Order Status Translation Created Successfully",
                        "data" => $createTranslation
                    ]));

                } catch (\Exception $ex) {
                    $this->logStoreHelper->storeLogError(array("Order Status", [
                        "status" => "409",
                        "message" => "Language translation for given Order Reject Status already exist"
                    ]));
                    return response()->json([
                        "status" => "409",
                        "message" => "Language translation for given Order Reject Status already exist"
                    ], 409);
                }
            }

            $this->logStoreHelper->storeLogInfo(array("Order Reject Status", [
                "status" => "200",
                "message" => "Order Reject Status update Successfully",
                "data" => [$id, $orderRejectStatusUpdated]
            ]));
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Order Reject Status Updated successfully"
            ]);


        } catch (\Exception $exception) {
            return response()->json([
                "status" => "500",
                "message" => "Error Updating Order status"
            ], 500);
        }
    }


    /**
     * Deletes specific order reject status and its child i.e order reject status translation
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {

            DB::beginTransaction();
            /*
             * checking order reject status exists or not
             * */
            $specificOrderRejectStatus = $this->orderRejectStatus->getSpecificOrderRejectStatus($id);

            try {
                /**
                 * this will deletes order reject status and its child table order reject status translation
                 */
                $this->orderRejectStatus->deleteOrderRejectStatus($specificOrderRejectStatus);
                DB::Commit();
                $this->logStoreHelper->storeLogInfo(array("Order Reject Status", [
                    "status" => "200",
                    "message" => "Order Reject Status Deleted Successfully",
                    "data" => $specificOrderRejectStatus
                ]));
                return response()->json([
                    "status" => "200",
                    "message" => "Order Reject Status Deleted Successfully"
                ]);
            } catch (\Exception $ex) {
                $this->logStoreHelper->storeLogError(array("Order Reject Status", [
                    "status" => "500",
                    "message" => "Error Deleting Order Reject Status"
                ]));
                return response()->json([
                    "status" => "500",
                    "message" => "Error Deleting Order Reject Status"
                ], 500);
            }
        } catch (ModelNotFoundException $exception) {
            $this->logStoreHelper->storeLogError(array("Order Reject Status", [
                "status" => "404",
                "message" => "Request Order does not exist"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Request Order does not exist"
            ], 404);
        }
    }


}