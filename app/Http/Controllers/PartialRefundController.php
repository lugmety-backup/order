<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 1/28/2018
 * Time: 12:10 PM
 */

namespace App\Http\Controllers;


use App\Repo\OrderInterface;
use App\Repo\PartialRefundInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PartialRefundController extends Controller
{
    protected $partialRefund;
    protected $order;
    protected $log;

    public function __construct(PartialRefundInterface $partialRefund,OrderInterface $order, \LogStoreHelper $logStoreHelper)
    {
        $this->partialRefund = $partialRefund;
        $this->order = $order;
        $this->log = $logStoreHelper;
    }

    public function getAllPartialRefund($orderId){
        try{
            $refunds = $this->partialRefund->getAllPartialRefund($orderId);
            return response()->json([
                "status" => "200",
                "data" => $refunds
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => "Error listing partial refund"
            ],500);
        }
    }

    public function createPartialRefund(Request $request){
        try{
            $message = [
                "amount.amount_validation" => "The deduction :value should not exceeds the order amount.",
                "type.in" => "The type value must be either plus or minus.",
                "vat.in" => "The type value must be either include_tax or do_not_include_tax."
            ];
            $this->validate($request,[
                "title" => "required|string",
                "type" => "required|in:plus,minus",
                "order_id" => "required|integer|min:1",
                "vat" => "required|in:include_tax,do_not_include_tax",
                "amount" => "required|between:0,999999.99|amount_validation",
                "comment" => "sometimes|string",

            ],$message);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => '422',
                "message" => $ex->response->original
            ],422);
        }


        DB::beginTransaction();
        try{
            if($request->type == "plus"){
                $flag = 1;

            }
            else{
                $flag = -1;
            }
            $request->merge([
                "user_id" => \RoleChecker::getUser()
            ]);
             $getOrder = $this->order->getSpecificOrder($request->order_id);
             $originalOrder = $getOrder->toArray();
            if($getOrder->tax_type == "none"){
                $commisionAmount = $getOrder->site_commission_amount  + ($flag * $request->amount * $getOrder->site_commission_rate * 0.01);
                $updateOrder =  $getOrder->update([
                    "amount" => $getOrder->amount + ($flag * $request->amount),
                    "site_commission_amount" => $commisionAmount
                ]);
            }
            elseif ($request->vat == "do_not_include_tax"){
                $commisionAmount = $getOrder->site_commission_amount  + ($flag * $request->amount * $getOrder->site_commission_rate * 0.01);
                $updateOrder =   $getOrder->update([
                    "amount" => $getOrder->amount + ($flag * $request->amount),
                    "site_commission_amount" => $commisionAmount
                ]);
            }
            else{
                $amount = $request->amount;
                $tax = $getOrder->site_tax_rate * 0.01 * $request->amount;
                $commisionAmount = $getOrder->site_commission_amount  + ($flag * $request->amount * $getOrder->site_commission_rate * 0.01);
                $total = $getOrder->amount + ($flag * $amount) + $flag * $tax;

                $updateOrder =  $getOrder->update([
                    "amount" => $total,
                    "tax" => $getOrder->tax + ($flag * $tax),
                    "site_commission_amount" => $commisionAmount
                ]);
            }
            //create refunds
            $createRefund = $this->partialRefund->createRefund($request->all());
            $getOrderUpdated = $this->order->getSpecificOrder($request->order_id);
            $this->log->storeLogInfo([
                "Refund creation",
                [
                    "order_id" => $getOrder->id,
                    'original_order_record' => $originalOrder,
                    'request' => $request->all(),
                    'refund_id' => $createRefund->id,
                    'is_order_updated' => $updateOrder,
                    'updated_order' => $getOrderUpdated->toArray()

                ]]);
            DB::commit();
            return response()->json([
                "status" => '200',
                "message" => "Adjustment created successfully."
            ]);

        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Order could not be found in database."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "Refund creation",
                [
                    "message" => $ex->getMessage()
                ]

            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error creating adjustment."
            ]);
        }
    }

    public function deleteRefund($id){
        try{
            DB::beginTransaction();
            $getRefundDetail = $this->partialRefund->getSpecificPartialRefund($id);
             $getOrder = $getRefundDetail->order()->first();
            $originalOrder = $getOrder->toArray();
            if($getRefundDetail->type == "plus"){
                $flag = -1;

            }
            else{
                $flag = 1;
            }
            if($getOrder->tax_type == "none"){
                $commisionAmount = $getOrder->site_commission_amount  + ($flag * $getRefundDetail->amount * $getOrder->site_commission_rate * 0.01);
                $updateOrder =   $getOrder->update([
                    "amount" => $getOrder->amount + ($flag * $getRefundDetail->amount),
                    "site_commission_amount" => $commisionAmount
                ]);
            }
            elseif ($getRefundDetail->vat == "do_not_include_tax"){
                $commisionAmount = $getOrder->site_commission_amount  + ($flag * $getRefundDetail->amount * $getOrder->site_commission_rate * 0.01);
                $updateOrder =  $getOrder->update([
                    "amount" => $getOrder->amount + ($flag * $getRefundDetail->amount),
                    "site_commission_amount" => $commisionAmount
                ]);
            }
            else{
                $commisionAmount = $getOrder->site_commission_amount  + ($flag * $getRefundDetail->amount * $getOrder->site_commission_rate * 0.01);
                $amount = $getRefundDetail->amount;
                $tax = $getOrder->site_tax_rate * 0.01 * $getRefundDetail->amount;

                $total = $getOrder->amount + ($flag * $amount) + $flag * $tax;

                $updateOrder =   $getOrder->update([
                    "amount" => $total,
                    "tax" => $getOrder->tax + ($flag * $tax),
                    "site_commission_amount" => $commisionAmount
                ]);
            }
            $getOrderUpdated = $getRefundDetail->order()->first();
            $deleted = $getRefundDetail->delete();

            $this->log->storeLogInfo([
                "Refund deletion",
                [
                    "order_id" => $getOrder->id,
                    'original_order_record' => $originalOrder,
                    'refund_id' => $getRefundDetail->id,
                    'refund_deleted' => $deleted,
                    'is_order_updated' => $updateOrder,
                    'updated_order' => $getOrderUpdated->toArray()

                ]]);
            DB::commit();
            return response()->json([
                "status" => '200',
                "message" => "Adjustment deleted successfully."
            ]);
        }

        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Adjustment could not be found"
            ],404);
        }

        catch (\Exception $ex){
            $this->log->storeLogError([
                "Refund creation",
                [
                    "message" => $ex->getMessage()
                ]

            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error deleting adjustment."
            ]);
        }
    }




}