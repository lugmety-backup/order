<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/20/2017
 * Time: 2:09 PM
 */

namespace App\Http\Controllers;


use App\Repo\DriverEarningInterface;
use App\Repo\OrderInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class DriverEarningController extends Controller
{
    protected $driverEarning;
    protected  $log;
    protected $order;
    public function __construct(OrderInterface $order,DriverEarningInterface $experiencePoint, \LogStoreHelper $logStoreHelper)
    {
        $this->driverEarning = $experiencePoint;
        $this->log = $logStoreHelper;
        $this->order = $order;
    }

    /**
     * return all driver earning for logged in driver
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllDriverEarning(Request $request){
        try{
            $lang = $request->get("lang","en");
            $limit = $request->get("limit",20);
            $this->validate($request,[
                "limit" => "required|integer|min:1",
            ]) ;
        }
        catch (\Exception $ex){
            $limit = 20;
        }
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1",
            ]) ;
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            if(!(\RoleChecker::hasRole("driver") || \RoleChecker::hasRole('driver-captain') )){
                return response()->json([
                    "status" => "403",
                    "message" => "Only driver has access"
                ],403);
            }
            $userId = \RoleChecker::getUser();
            $getAllDriverEarning = $this->driverEarning->getAllDriverEarning($userId, $limit,$request->country_id);
            foreach ($getAllDriverEarning as $earning){

                if($earning["type"] == "normal" && $lang == "en"){
                    $earning["type_label"] = $earning->earningStatusNEn;
                }
                elseif($earning["type"] == "normal" && $lang == "ar"){
                    $earning["type_label"] = $earning->earningStatusNAr;
                }
                elseif($earning["type"] == "deduction" && $lang == "ar"){
                    $earning["type_label"] = $earning->earningStatusDAr;
                }
                elseif($earning["type"] == "deduction" && $lang == "en"){
                    $earning["type_label"] = $earning->earningStatusDEn;
                }
                else{
                    $earning["type_label"] = ucfirst($earning->type);
                }
            }

            if(count($getAllDriverEarning) == 0){
                return response()->json([
                    "status" => "404",
                    "message" => "Empty earning"
                ],404);
            }
            /**
             * get location service base url from Config/config.php
             */
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$request->country_id";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if($countryInfo['status'] != 503){
                }
                else{
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails =  $countryInfo["message"]["data"];
            $currency = $shippingDetails["currency_symbol"];

            $getEarningInfo = $this->getTotalEarning($userId, $request->country_id);
            $total = collect(
                ["total_earnings" =>$getEarningInfo["total_earnings"],
                    "available" => $getEarningInfo[0],
                    "currency" => $currency
                ]);
            $getAllDriverEarning = $total->merge($getAllDriverEarning->withPath("/driver/earning")->appends(['limit' => $limit,"country_id"=> $request->country_id]));
            return response()->json([
                "status" => "200",
                "data" => $getAllDriverEarning
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(array("Driver xp view all",[
                $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error getting record"
            ],500);
        }
    }

    /**
     * return all driver earning for passed user_id .This route is for admin
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function getAllDriverEarningByAdmin(Request $request){
        try{
            $this->validate($request,[
                "user_id" => "required|integer|min:1",
                "country_id" => "required|integer|min:1"
            ]) ;
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            $userId = $request->get("user_id",null);
            $getAllDriverEarning = $this->driverEarning->getAllDriverEarningForAdmin($userId);
            $getAllDriverEarning = collect($getAllDriverEarning)->where("country_id",$request->country_id)->values()->all();
            /**
             * get location service base url from Config/config.php
             */
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$request->country_id";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if($countryInfo['status'] != 503){
                }
                else{
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails =  $countryInfo["message"]["data"];
            $currency = $shippingDetails["currency_symbol"];
//            $getAllDriverEarning = new Collection($getAllDriverEarning);
            foreach ($getAllDriverEarning as $earning){
               $earning->amountWithCurrency = $currency;
            }
            return \Datatables::of($getAllDriverEarning)->make(true);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(array("Driver xp view all",[
                $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error getting record"
            ],500);
        }
    }


    /**
     * returns specific xp data for logged in user
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificDriverEarning($id, Request $request){
        try{
            $lang = $request->get("lang","ar");
            try{
                $this->validate($request,[
                    "country_id" => "required|integer|min:1"
                ]);
            }
            catch (\Exception $ex){
                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ],422);
            }

            if(!(\RoleChecker::hasRole("driver") || \RoleChecker::hasRole('driver-captain') )){
                return response()->json([
                    "status" => "403",
                    "message" => "Only driver has access"
                ],403);
            }
            $userId = \RoleChecker::getUser();
            $getDriverEarning = $this->driverEarning->viewSpecificEarning($id);
            if($getDriverEarning["type"] == "normal" && $lang == "en"){
                $earning["type_label"] = $getDriverEarning->earningStatusNEn;
            }
            elseif($getDriverEarning["type"] == "normal" && $lang == "ar"){
                $getDriverEarning["type_label"] = $getDriverEarning->earningStatusNAr;
            }
            elseif($getDriverEarning["type"] == "deduction" && $lang == "ar"){
                $getDriverEarning["type_label"] = $getDriverEarning->earningStatusDAr;
            }
            elseif($getDriverEarning["type"] == "deduction" && $lang == "en"){
                $getDriverEarning["type_label"] = $getDriverEarning->earningStatusDEn;
            }
            else{
                $getDriverEarning["type_label"] = ucfirst($getDriverEarning->type);
            }
            if($getDriverEarning->country_id != $request->country_id){
                return response()->json([
                    "status" => "403",
                    "message" => "Country id did not  match"
                ],403);
            }
            if($getDriverEarning->user_id !== $userId){
                return response()->json([
                    "status" => "403",
                    "message" => "You can view your earnings only."
                ],403);
            }
            $orderNull = false;
            $getDriverEarning = $getDriverEarning->toArray();
            if(is_null($getDriverEarning["order_id"]))  {
                $orderNull = true;
                /**
                 * get location service base url from Config/config.php
                 */
                $countryBaseUrl = Config::get("config.location_base_url");
                //todo get restaurant country location
                $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$request->country_id";

                /**
                 * calling to location service
                 */
                $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
                /**
                 * if location service gives status message other than zero then display the message
                 */

                if ($countryInfo['status'] != 200) {
                    if($countryInfo['status'] != 503){
                    }
                    else{
                        return response()->json(
                            $countryInfo, $countryInfo['status']);
                    }
                    return response()->json(
                        $countryInfo["message"], $countryInfo['status']);
                }

                $shippingDetails =  $countryInfo["message"]["data"];
                $timezone = $shippingDetails["timezone"];
                $countryName = $shippingDetails["translation"]["lang"];
                goto skipDetails;
            }
            $orderDetails = $this->order->getSpecificOrder($getDriverEarning["order_id"]);


            $restaurantBaseUrl = Config::get("config.restaurant_base_url");
            $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$orderDetails->restaurant_id/detail?lang=$lang";

            /**
             *calling restaurant service to check the restaurant exist or not if exist fetch the data
             */
            $restaurantdata = \RemoteCall::getSpecific($restaurantFullUrl);
            if ($restaurantdata['status'] != 200) {
                if($restaurantdata['status'] == 503){
                    return response()->json(
                        $restaurantdata, $restaurantdata['status']);
                }
                return response()->json(
                    $restaurantdata["message"], $restaurantdata['status']);
            }
            $restaurantDetails =   $restaurantdata["message"]["data"];
            /**
             * get location service base url from Config/config.php
             */
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/$orderDetails->customer_id/shipping/$orderDetails->shipping_id";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if($countryInfo['status'] != 503){
                }
                else{
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails =  $countryInfo["message"]["data"];
            $timezone = $shippingDetails["country"]["timezone"];
            $countryName = $shippingDetails["translation"]["lang"];

            /**
             *
             * calling oauth server to get user details
             * */
            $userDatas["users"] = [$orderDetails->customer_id];
            $oauthUrl = Config::get('config.oauth_base_url')."/review/users/list";
            $getUserDetail =  \RemoteCall::storeWithoutOauth($oauthUrl,$userDatas);
            if ($getUserDetail['status'] != 200) {
                if($getUserDetail['status'] == 503){
                    return response()->json(
                        $getUserDetail, $getUserDetail['status']);
                }
                return response()->json(
                    $getUserDetail["message"], $getUserDetail['status']);
            }
            $countryFirstName = explode(" ",$shippingDetails["translation"]["name"]);
            if(strpos(strtolower($restaurantDetails["address"]),strtolower($countryFirstName[0])) !== false){
                $order["restaurant_information"]["address"] = $restaurantDetails["address"];
            }
            else{
                $order["restaurant_information"]["address"] = implode(",",[$restaurantDetails["address"],$shippingDetails["translation"]["name"]]);
            }
            $customer = $getUserDetail["message"]["data"][0];
            $getDriverEarning["customer_information"]["name"] = implode(" ", array($customer['first_name'], $customer['middle_name'], $customer['last_name']));
            $getDriverEarning["customer_information"]["mobile"] = implode("", array($customer['country_code'], $customer["mobile"]));
            $getDriverEarning["customer_information"]["house_no"] = $shippingDetails["house_number"];
            $getDriverEarning["customer_information"]["address"] = implode(",", array($shippingDetails['address_name'], $shippingDetails['street'], $shippingDetails['extra_direction']));
            $getDriverEarning["customer_information"]["zip_code"] = $shippingDetails["zip_code"];
            $getDriverEarning["restaurant_information"]["name"] = $restaurantDetails["name"];
            $getDriverEarning["restaurant_information"]["address"] = $restaurantDetails["address"];
            $getDriverEarning["restaurant_information"]["phone_number"] = $restaurantDetails["phone_no"];
            skipDetails:
            if($lang == "ar"){
                $getDriverEarning["delivery_information"]["delivery_status"] = "منجز";
            }
            else{
                $getDriverEarning["delivery_information"]["delivery_status"] = "COMPLETED";
            }

            $convertToCountryTimeZone = Carbon::parse($getDriverEarning["created_at"])->timezone($timezone);
            $getDriverEarning["delivery_information"]["time"] = $convertToCountryTimeZone->format("h:i A");
            $getDriverEarning["delivery_information"]["date"] = $convertToCountryTimeZone->format("Y-m-d");
            return response()->json([
                "status" => "200",
                "data" => $getDriverEarning
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Empty earning"
            ],404);
        }
//        catch (\Exception $ex){
//            $this->log->storeLogError(array("Driver earning view specific",[
//                $ex->getMessage()
//            ]));
//            return response()->json([
//                "status" => "500",
//                "message" => "Error getting data"
//            ],500);
//        }
    }

    /**
     * returns specific xp data .Only user that has permission can use this route
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificDriverEarningAdmin($id, Request $request){

        try{
            $getDriverEarning = $this->driverEarning->viewSpecificEarning($id);

            $country_id = $getDriverEarning->country_id;
            $orderNull = false;
            $getDriverEarning = $getDriverEarning->toArray();
            $orderNull = true;
            /**
             * get location service base url from Config/config.php
             */
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$country_id";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if($countryInfo['status'] != 503){
                }
                else{
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails =  $countryInfo["message"]["data"];
            $timezone = $shippingDetails["timezone"];
            $countryName = $shippingDetails["translation"]["lang"];
            $getDriverEarning["amount"] = implode("",[$shippingDetails["currency_symbol"],$getDriverEarning["amount"]]);
            /**
             *
             * calling oauth server to get user details
             * */
            $userDatas["users"] = [$getDriverEarning["user_id"]];
            $oauthUrl = Config::get('config.oauth_base_url')."/review/users/list";
            $getUserDetail =  \RemoteCall::storeWithoutOauth($oauthUrl,$userDatas);
            if ($getUserDetail['status'] != 200) {
                if($getUserDetail['status'] == 503){
                    return response()->json(
                        $getUserDetail, $getUserDetail['status']);
                }
                return response()->json(
                    $getUserDetail["message"], $getUserDetail['status']);
            }
            $customer = $getUserDetail["message"]["data"][0];
            $getDriverEarning["name"] = implode(" ", array($customer['first_name'], $customer['middle_name'], $customer['last_name']));
            $getDriverEarning["mobile"] = implode("", array($customer['country_code'], $customer["mobile"]));
            $convertToCountryTimeZone = Carbon::parse($getDriverEarning["created_at"])->timezone($timezone);
            return response()->json([
                "status" => "200",
                "data" => $getDriverEarning
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Empty earning"
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(array("Driver xp view specific",[
                $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" =>  "Error getting data"
            ],500);
        }
    }

    /**
     * creating new xp for passed user id. Only user that has permission has access to create earning
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createDriverEarningByAdmin(Request $request){
        try{
            $this->validate($request,[
                "user_id" => "required|integer|min:1",
                "order_id" => "sometimes|integer|min:1",
                "amount" => "required|numeric|min:0",
                "type" => "required|in:normal,deduction",
                "comment" => "sometimes|string",
                'date_for' => 'required|date_format:Y-m',
                "country_id" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            DB::beginTransaction();
            //calling oauth service to check user has role driver
            $baseUrlForOauth = Config::get('config.oauth_base_url');
            $checkUserRoleIsDriver = \RemoteCall::getSpecific($baseUrlForOauth . "/user/$request->user_id/driver");
            if ($checkUserRoleIsDriver["status"] != 200) {
                return response()->json([
                    $checkUserRoleIsDriver["message"]
                ], $checkUserRoleIsDriver["status"]);
            }
            $getCurrentBalance = $this->getTotalEarning($request->user_id,$request->country_id,$request->date_for)[0];
            if($request->type =="deduction" && $request->amount > $getCurrentBalance){
                return response()->json([
                    "status" => "403",
                    "message" => "Requested amount should not be greater than current balance of the driver for requested date."
                ],403);
            }
            $request->merge([
                'date_for' => Carbon::parse($request->date_for)->tz('utc')
            ]);
            $createDriverEarning = $this->driverEarning->createDriverEarning($request->all());
            DB::commit();
            $this->log->storeLogInfo([
                "driver xp creation by admin",[
                    "status" => "200",
                    "message" => "Driver earning created successfully",
                    "data" => $createDriverEarning->toArray()
                ]
            ]);
            return response()->json([
                "status" => "200",
                "message" => "Driver earning created successfully"
            ]);
        }
        catch (\Exception $ex){

            $this->log->storeLogInfo([
                "driver xp creation by admin",[
                    $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error creating driver earning"
            ]);
        }
    }

    /**
     * creating new xp for passed user id. Only user that has permission has access to create earning
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateDriverEarningByAdmin(Request $request, $id){
        try{
            $this->validate($request,[
                "user_id" => "required|integer|min:1",
                "order_id" => "sometimes|integer|min:1",
                "amount" => "required|numeric|min:0",
                "type" => "required|in:normal,deduction",
                "comment" => "sometimes|string",
                'date_for' => 'required|date_format:Y-m',
                "country_id" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            DB::beginTransaction();
            $getCurrentBalance = $this->getTotalEarning($request->user_id,$request->country_id,$request->date_for)[0];
            if($request->type =="deduction" && $request->amount > $getCurrentBalance){
                return response()->json([
                    "status" => "403",
                    "message" => "Requested amount should not be greater than current balance of the driver for requested date."
                ],403);
            }
            $request->merge([
                'date_for' => Carbon::parse($request->date_for)->tz('utc')
            ]);
            $createDriverEarning = $this->driverEarning->updateDriverEarning($id,$request->except("user_id","order_id"));
            DB::commit();
            $this->log->storeLogInfo([
                "driver xp creation by admin",[
                    "status" => "200",
                    "message" => "Driver earning updated successfully",
//                    "data" => $createDriverEarning->toArray()
                ]
            ]);
            return response()->json([
                "status" => "200",
                "message" => "Driver earning updated successfully"
            ]);
        }
        catch (\Exception $ex){

            $this->log->storeLogInfo([
                "driver xp creation by admin",[
                    $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error creating driver earning"
            ]);
        }
    }

    private function getTotalEarning($user_id,$countryId,$date_for = null){
        $normalEarning = $this->driverEarning->getAllDriverEarningNormal($user_id,$countryId,$date_for);
        $deductedEarning = $this->driverEarning->getAllDriverEarningDeducted($user_id,$countryId,$date_for);
        return [
            $normalEarning - $deductedEarning,
            "total_earnings" => $normalEarning,
            "deducted_earnings" => $deductedEarning
        ];

    }

}