<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 12:17 PM
 */

namespace App\Http\Controllers;

use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repo\OrderStatusTranslationInterface;
use App\Repo\OrderStatusInterface;
use LogStoreHelper;
use Illuminate\Support\Facades\DB;
use Validator;
use RoleChecker;
use Yajra\Datatables\Datatables;

/**
 * Class OrderStatusController
 * @package App\Http\Controllers
 */
class OrderStatusController extends Controller
{
    /**
     * @var OrderStatusInterface
     */
    protected $orderStatus;
    /**
     * @var OrderStatusTranslationInterface
     */
    protected $orderStatusTranslation;

    /**
     * @var Slugify
     */
    protected $slugify;

    /**
     * @var LogStoreHelper
     */
    protected $logStoreHelper;
    /**
     * OrderStatusController constructor.
     * @param $orderStatus
     * @param $orderStatusTranslation
     */
    public function __construct(LogStoreHelper $logStoreHelper,Slugify $slugify,OrderStatusInterface $orderStatus, OrderStatusTranslationInterface $orderStatusTranslation)
    {
        $this->orderStatus = $orderStatus;
        $this->orderStatusTranslation = $orderStatusTranslation;
        $this->slugify = $slugify;
        $this->logStoreHelper = $logStoreHelper;
    }

    public function viewAllOrder(){
        try {
            if(RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")|| RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")){
                $orderStatuses  = $this->orderStatus->getAllEnabledOrderStatus();
            }
            else {
                $orderStatuses = $this->orderStatus->getAllOrderStatus();
            }
            if (!$orderStatuses->first()) {
                throw  new \Exception();
            }
        }
        catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Order Status"
            ],404);
        }
        foreach ($orderStatuses as $key => $orderStatus) {
            $orderStatusTranslation = $orderStatus->orderStatusTranslations()->where('lang_code', "en")->get();

            try {
                if ($orderStatusTranslation->count() == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }

            /*
             * attach name and lang_code in $restaurant variable
             * */

            $orderStatus['name'] = $orderStatusTranslation[0]['name'];
            $orderStatus['lang_code'] = $orderStatusTranslation[0]['lang_code'];
        }
         $orderStatuses = collect($orderStatuses)->sortBy('name')->values()->all();
        return Datatables::of($orderStatuses)->make(true);
//        return response()->json([
//            "status" => "200",
//            "data" => $orderStatuses
//        ]);

    }

    /**
     * Returns specific order and order translation
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewSpecificOrder($id){
       
        try{
            /*
             * checking order status exists or not
           * */
            if(RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")|| RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")){
                $specificOrderStatus  = $this->orderStatus->getSpecificOrderStatusEnabled($id);
            }
            else {
                $specificOrderStatus = $this->orderStatus->getSpecificOrderStatus($id);
            }
            $specificOrderStatus["translation"] = $specificOrderStatus->orderStatusTranslations()->get();

            $this->logStoreHelper->storeLogError(array("Order Status", [
                "status" => "200",
                "data" => $specificOrderStatus
            ]));

            return response()->json([
                "status" => "200",
                "data" => $specificOrderStatus
            ],200);
        }
        catch (ModelNotFoundException $exception){
            $this->logStoreHelper->storeLogInfo(array("Order Status", [
                "status" => "404",
                "message" => "Request Order Status does not exist"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Request Order Status does not exist"
            ],404);
        }
    }

    /**
     * Create order status.only permitted user can create order status.if input request is_default is 1 then will set other whose status is 1 to 0
     * and will set slug with name pending to 1 if there is no is_default is 1 and slug name pending is present
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createStatus(Request $request){
        try{
           
            $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
            /**
             * custom validation of slug
             */
            $message =[
                "slug.required" => "The slug may not only contain numbers,special characters and should not be empty."
            ];

            $this->validate($request,[
                "status" => "integer|min:0|max:1",
                "is_default" => "integer|min:0|max:1",
                "slug" => "required|unique:order_status,slug",
                "translation" => "required|array"
            ],$message);
        }
        catch (\Exception $exception){
            $this->logStoreHelper->storeLogError(array("Order Status ",[
                "status"=>'422',
                "message"=>$exception->response->original
            ]));

            return response()->json([
                "status" => "422",
                "message" => $exception->response->original
            ],422);
        }

        try{
            DB::beginTransaction();
            $orderStatus = $request->all();
            $orderStatus["is_default"] =$request->input('is_default',0);
            $orderStatus["status"] =$request->input('status',1);
            if(isset($orderStatus["is_default"]) && $orderStatus["is_default"] == 1){
                /**
                 * if input request is_default is 1 then this function will set other whose status is 1 to 0
                 */
                $this->orderStatus->setOtherOrderStatusToNonDefault();
            }
            else{
                /**
                 *  this function will set slug with name pending to 1 if there is no is_default is 1 and slug name pending is present
                 */
                $this->orderStatus->setPendingSlugDefaultIfOtherSlugIsDefault();
            }

            $orderStatusCreated =$this->orderStatus->createOrderStatus($orderStatus);
            $this->logStoreHelper->storeLogInfo(array("Order Status", [
                "status" => "200",
                "message" => "Restaurant Created Successfully",
                "data" => $orderStatusCreated
            ]));
            foreach ($orderStatus['translation'] as $key => $translation) {
                $translation['order_status_id'] = $orderStatusCreated->id;
                $translation['lang_code'] = $key;
                $orderStatusId = $orderStatusCreated->id;
                $rules = [
                    "lang_code" => 'required|alpha',
                    "name" => "required"
                ];
                $translation['lang_code'] = $this->slugify->slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
                if (empty($translation['lang_code'])) {
                    return response()->json([
                        "status" => "422",
                        "message" => ["translation"=> ["lang_code" => "The lang_code may not only contain numbers,special characters."]]
                    ], 422);
                }
                $validator = Validator::make($translation, $rules);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError(array('Order Status Translation', [
                        'status' => '422',
                        "message" => ["translation"=>[$key => $error]]
                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => ["translation"=>[$key => $error]]
                    ], 422);
                }
                /*
                 * checking if the same order_status_id id have existing lang_code or not
                 * */
                try {
                    $checkExistingTranslation = $this->orderStatusTranslation
                        ->getSpecificOrderStatusTranslationByCodeAndId($translation["lang_code"], $orderStatusId);
                    switch ($checkExistingTranslation->first()) {
                        case !null: {
                            throw new \Exception();
                            break;
                        }
                        default: {
                            $createTranslation = $this->orderStatusTranslation->createOrderStatusTranslation($translation);
                            $this->logStoreHelper->storeLogInfo(array("Order Status Translation", [
                                "status" => "200",
                                "message" => "Order Status Translation Created Successfully",
                                "data" => $createTranslation
                            ]));
                        }
                    }
                } catch (\Exception $ex) {
                    $this->logStoreHelper->storeLogError(array("Order Status", [
                        "status" => "409",
                        "message" => "Language translation for given Order Status already exist"
                    ]));
                    return response()->json([
                        "status" => "409",
                        "message" => "Language translation for given Order Status  already exist"
                    ], 409);
                }
            }
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Order Status created successfully"
            ]);

        }
        catch (\Exception $exception){
            return response()->json([
                "status" => "500",
                "message" => "Error Creating Order status"
            ],500);
        }
    }

    /**
     * update order status.only permitted user can create order status.if input request is_default is 1 then will set other whose status is 1 to 0
     * and will set slug with name pending to 1 if there is no is_default is 1 and slug name pending is present
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus($id,Request $request){
        try{
            $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
            /**
             * custom validation of slug
             */
            $message =[
                "slug.required" => "The slug may not only contain numbers,special characters and should not be empty."
            ];

            $this->validate($request,[
                "status" => "integer|min:0|max:1",
                "is_default" => "integer|min:0|max:1",
                "slug" => "required|unique:order_status,slug,".$id,
                "translation" => "required|array"
            ],$message);
        }
        catch (\Exception $exception){
            $this->logStoreHelper->storeLogError(array("Order Status ",[
                "status"=>'422',
                "message"=>$exception->response->original
            ]));

            return response()->json([
                "status" => "422",
                "message" => $exception->response->original
            ],422);
        }

        try{
            DB::beginTransaction();
            /**
             * checking order status exist for update
             */
            try{
                $specificOrderStatus  = $this->orderStatus->getSpecificOrderStatus($id);
                /**
                 * deletes the all order status translation whose order_status_id is @param $id
                 */
                $this->orderStatusTranslation->deleteOrderStatusTranslationByOrderStatusId($specificOrderStatus->id);
            }
            catch (ModelNotFoundException $ex){
                return response()->json([
                    "status" => "404",
                    "message" => "Order Status could not be found"
                ],404);
            }
            $orderStatus = $request->all();
            if(isset($orderStatus["is_default"]) && $orderStatus["is_default"] == 1){
                /**
                 * if input request is_default is 1 then this function will set other whose status is 1 to 0
                 */
                $this->orderStatus->setOtherOrderStatusToNonDefault();
            }
            else{
                /**
                 *  this function will set slug with name pending to 1 if there is no is_default is 1 and slug name pending is present
                 */
                $this->orderStatus->setPendingSlugDefaultIfOtherSlugIsDefault();
            }
            /**
             * checks if the order status slug belongs to list of slugs list that are fixed
             * if belongs it  will not update requested slug
             */
            if($this->checkOrderStatusSlug($specificOrderStatus->slug)) {
                unset($orderStatus["slug"]);
            }
            $orderStatusUpdated = $this->orderStatus->updateOrderStatus($id, $orderStatus);
            $this->logStoreHelper->storeLogInfo(array("Order Status", [
                "status" => "200",
                "message" => "Order Status update Successfully",
                "data" => [$id,$orderStatusUpdated]
            ]));
            foreach ($orderStatus['translation'] as $key => $translation) {
                $translation['order_status_id'] = $id;
                $translation['lang_code'] = $key;
                $orderStatusId = $id;
                $rules = [
                    "lang_code" => 'required|alpha',
                    "name" => "required"
                ];
                $translation['lang_code'] = $this->slugify->slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
                if (empty($translation['lang_code'])) {
                    return response()->json([
                        "status" => "422",
                        "message" => ["lang_code" => "The lang_code may not only contain numbers,special characters."]
                    ], 422);
                }
                $validator = Validator::make($translation, $rules);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError(array('Order Status Translation', [
                        'status' => '422',
                        "message" => ["translation"=>[$key => $error]]
                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => ["translation"=>[$key => $error]]
                    ], 422);
                }
                /*
                 * checking if the same order_status_id id have existing lang_code or not
                 * */
                try {
                    $checkExistingTranslation = $this->orderStatusTranslation
                        ->getSpecificOrderStatusTranslationByCodeAndId($translation["lang_code"], $orderStatusId);
                    switch ($checkExistingTranslation->first()) {
                        case !null: {
                            throw new \Exception();
                            break;
                        }
                        default: {
                            $createTranslation = $this->orderStatusTranslation->createOrderStatusTranslation($translation);
                            $this->logStoreHelper->storeLogInfo(array("Order Status Translation", [
                                "status" => "200",
                                "message" => "Order Status Translation updated Successfully",
                                "data" => $createTranslation
                            ]));
                        }
                    }
                } catch (\Exception $ex) {
                    $this->logStoreHelper->storeLogError(array("Order Status", [
                        "status" => "409",
                        "message" => "Language translation for given Order Status  already exist"
                    ]));
                    return response()->json([
                        "status" => "409",
                        "message" => "Language translation for given Order Status already exist"
                    ], 409);
                }
            }
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Order Status Updated successfully"
            ]);

        }
        catch (\Exception $exception){
            return response()->json([
                "status" => "500",
                "message" => "Error Updating Order status"
            ],500);
        }
    }


    /**
     * Deletes specific order status and its child i.e order status translation
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteStatus($id){
        try{
           
            DB::beginTransaction();
            /*
             * checking order status exists or not
             * */
            $specificOrderStatus  = $this->orderStatus->getSpecificOrderStatus($id);
            /**
             * checks if the slug belongs to list of slugs list that are fixed
             * if belongs it wont allow to delete
             */
            if($this->checkOrderStatusSlug($specificOrderStatus->slug)){
                return response()->json([
                    "status" => "403",
                    "message" => "Requested order status slug is not allowed to delete"
                ],403);
            }
            try{
                /**
                 * this will deletes order status and its child table order status translation
                 */
                $this->orderStatus->deleteOrderStatus($specificOrderStatus);
                DB::Commit();
                $this->logStoreHelper->storeLogInfo(array("Order Status", [
                    "status" => "200",
                    "message" => "Order Status Deleted Successfully",
                    "data" =>$specificOrderStatus
                ]));
                return response()->json([
                    "status" => "200",
                    "message" => "Order Status Deleted Successfully"
                ]);
            }
            catch (\Exception $ex){
                $this->logStoreHelper->storeLogError(array("Order Status", [
                    "status" => "500",
                    "message" => "Error Deleting Order Status"
                ]));
                return response()->json([
                    "status" => "500",
                    "message" => "Error Deleting Order Status"
                ],500);
            }
        }
        catch (ModelNotFoundException $exception){
            $this->logStoreHelper->storeLogError(array("Order Status", [
                "status" => "404",
                "message" => "Request Order does not exist"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Request Order does not exist"
            ],404);
        }
    }

    /**
     * checks if the slug matches with list of slugs
     * @param $slug
     * @return bool
     */
    private function checkOrderStatusSlug($slug){
        $orderStatusToCheck =["pending","accepted","cancelled","rejected","processing"];
        if(in_array($slug, $orderStatusToCheck)){
            return true;
        }
        return false;
    }


}