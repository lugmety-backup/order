<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 1:57 PM
 */

namespace App\Http\Controllers;

use App\Repo\EarningRuleInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
class EarningRulesController extends Controller
{

    protected $earningRule;
    protected $log;
    public function __construct(EarningRuleInterface $earningRule,\LogStoreHelper $log)
    {
        $this->earningRule = $earningRule;
        $this->log = $log;
    }

    /**
     * returns all country rules based on @param country_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllRules(Request $request)
    {
        try {
            try {
                $this->validate($request, [
                    "country_id" => "required|integer|min:1"
                ]);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ], 422);
            }
            $getRules = $this->earningRule->getAllRules($request->country_id);

            $getCountryRules = new Collection($getRules);

            return Datatables::of($getCountryRules)->make(true);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["all xp rules",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => "Error getting data"
            ],500);
        }
    }

    /**
     * return specified xp rules
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificRule($id){
        try{
            $getSpecificRule = $this->earningRule->getSpecialRule($id);
            return response()->json([
                "status" => "200",
                "data" => $getSpecificRule
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Rule could not be found."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["specific xp rules",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => "Error getting data"
            ],500);
        }
    }

    /**
     * created new rules for xp
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRules(Request $request){
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1",
                "min_earning" => "required|min:0|numeric",
                "min_distance" => "required|min:0|numeric",
                "earning_per_km" => "required|min:0|numeric",
                "is_default" => "sometimes|boolean",
                "start_date" => "required_if:is_default,0|date_format:Y-m-d H:i:s",
                "end_date" => "required_if:is_default,0|date_format:Y-m-d H:i:s|after:start_date",

            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
            $request["is_default"] = $request->get("is_default",0);
            /**
             * if is_default is 1 then it will set other 0
             */
            $request = $request->all();
            if(isset($request["is_default"]) && ($request["is_default"] == 1)){
                $unset = $this->earningRule->unsetDefault($request);
            }

            $createRules = $this->earningRule->createRule($request);
            $this->log->storeLogInfo(["xp rules creation",
                    [
                        "status" => "200",
                        "message" => "Experience rule created successfully",
                        "data" => $createRules->toArray()
                    ]]
            );
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Experience rule created successfully",
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["create all xp rules",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => "Error creating earning rule"
            ],500);
        }
    }

    /**
     * updates the specified $id rule
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateRules(Request $request, $id){
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1",
                "min_earning" => "required|min:0|numeric",
                "min_distance" => "required|min:0|numeric",
                "earning_per_km" => "required|min:0|numeric",
                "is_default" => "sometimes|boolean",
                "start_date" => "required_if:is_default,0|date_format:Y-m-d H:i:s",
                "end_date" => "required_if:is_default,0|date_format:Y-m-d H:i:s|after:start_date",

            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
            $getRules = $this->earningRule->getSpecialRule($id);
            $request = $request->all();

           if($getRules->is_default==1 && isset($request["is_default"]) && ($request["is_default"] == 0)){
               return response()->json([
                   "status" => "403",
                   "message" => "Default rule could not be changed to non default"
               ],403);
           }

            /**
             * if is_default is 1 then it will set other 0 and current id to 1
             */
            if(isset($request["is_default"]) && ($request["is_default"] == 1)){
                $updateIsDefault = $this->earningRule->updateDefault($request,$id);
                $request["start_date"] = null;
                $request["end_date"] = null;
            }

            $createRules = $this->earningRule->updateRule($getRules,$request);
            $this->log->storeLogInfo(["xp rules update",[$createRules]]);
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Experience rule updated successfully"
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Rule could not be found."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["update xp rules",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => "Error updating earning rule"
            ],500);
        }
    }

    /**
     * deletes rules based on id
     * @param $id
     */
    public function deleteRules($id){
        try{
            $getRules = $this->earningRule->getSpecialRule($id);
            if($getRules->is_default ==1){
                return response()->json([
                    "status" => "403",
                    "message" => "Default rule cannot be deleted.Please change it and try again"
                ],403);
            }
            DB::beginTransaction();
            /**
             * deletes the rule
             */
            $deletesRecord = $this->earningRule->deleteRule($id);
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Rule deleted successfully"
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Rule could not be found."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError("update xp rules",[$ex->getMessage()]);
            return response()->json([
                "status" => "500",
                "message" => "Error deleting earning rule"
            ],500);
        }
    }
}