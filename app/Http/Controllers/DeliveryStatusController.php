<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 12:17 PM
 */

namespace App\Http\Controllers;

use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repo\DeliveryStatusTranslationInterface;
use App\Repo\DeliveryStatusInterface;
use LogStoreHelper;
use Illuminate\Support\Facades\DB;
use Validator;
use RoleChecker;
use Yajra\Datatables\Datatables;

/**
 * Class DeliveryStatusController
 * @package App\Http\Controllers
 */
class DeliveryStatusController extends Controller
{
    /**
     * @var DeliveryStatusInterface
     */
    protected $deliveryStatus;
    /**
     * @var DeliveryStatusTranslationInterface
     */
    protected $deliveryStatusTranslation;

    /**
     * @var Slugify
     */
    protected $slugify;

    /**
     * @var LogStoreHelper
     */
    protected $logStoreHelper;
    /**
     * DeliveryStatusController constructor.
     * @param $deliveryStatus
     * @param $deliveryStatusTranslation
     */
    public function __construct(LogStoreHelper $logStoreHelper,Slugify $slugify,DeliveryStatusInterface $deliveryStatus, DeliveryStatusTranslationInterface $deliveryStatusTranslation)
    {
        $this->deliveryStatus = $deliveryStatus;
        $this->deliveryStatusTranslation = $deliveryStatusTranslation;
        $this->slugify = $slugify;
        $this->logStoreHelper = $logStoreHelper;
    }

    public function viewAllDelivery(){
        try {
            if(RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")|| RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")){
                $deliveryStatuses = $this->deliveryStatus->getAllEnabledDeliveryStatus();
            }
            else {
                $deliveryStatuses = $this->deliveryStatus->getAllDeliveryStatus();
            }
            if (!$deliveryStatuses->first()) {
                throw  new \Exception();
            }
        }
        catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Delivery Status"
            ],404);
        }
        foreach ($deliveryStatuses as $key => $deliveryStatus) {
            $deliveryStatusTranslation = $deliveryStatus->deliveryStatusTranslations()->where('lang_code', "en")->get();

            try {
                if ($deliveryStatusTranslation->count() == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }
            /*
             * attach name and lang_code in $restaurant variable
             * */

            $deliveryStatus['name'] = $deliveryStatusTranslation[0]['name'];
            $deliveryStatus['lang_code'] = $deliveryStatusTranslation[0]['lang_code'];
        }
        return Datatables::of($deliveryStatuses)->make(true);
//        return response()->json([
//            "status" => "200",
//            "data" => $deliveryStatuses
//        ]);

    }

    /**
     * Returns specific delivery and delivery translation
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewSpecificDelivery($id){

        try{
            /*
             * checking delivery status exists or not
           * */
            if(RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")|| RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")){
                $specificDeliveryStatus  = $this->deliveryStatus->getSpecificDeliveryStatusEnabled($id);
            }
            else {
                $specificDeliveryStatus = $this->deliveryStatus->getSpecificDeliveryStatus($id);
            }
            $specificDeliveryStatus["translation"] = $specificDeliveryStatus->deliveryStatusTranslations()->get();

            $this->logStoreHelper->storeLogError(array("Delivery Status", [
                "status" => "200",
                "data" => $specificDeliveryStatus
            ]));

            return response()->json([
                "status" => "200",
                "data" => $specificDeliveryStatus
            ],200);
        }
        catch (ModelNotFoundException $exception){
            $this->logStoreHelper->storeLogInfo(array("Delivery Status", [
                "status" => "404",
                "message" => "Request Delivery Status does not exist"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Request Delivery Status does not exist"
            ],404);
        }
        catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("Delivery Status", [
                "status" => "500",
                "message" => $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error in Query"
            ],500);
        }
    }

    /**
     * Create delivery status.only permitted user can create delivery status.if input request is_default is 1 then will set other whose status is 1 to 0
     * and will set slug with name pending to 1 if there is no is_default is 1 and slug name pending is present
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createStatus(Request $request){
        try{

            $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
            /**
             * custom validation of slug
             */
            $message =[
                "slug.required" => "The slug may not only contain numbers,special characters and should not be empty."
            ];

            $this->validate($request,[
                "status" => "integer|min:0|max:1",
                "is_default" => "integer|min:0|max:1",
                "slug" => "required|unique:delivery_status,slug",
                "translation" => "required|array"
            ],$message);
        }
        catch (\Exception $exception){
            $this->logStoreHelper->storeLogError(array("Delivery Status ",[
                "status"=>'422',
                "message"=>$exception->response->original
            ]));

            return response()->json([
                "status" => "422",
                "message" => $exception->response->original
            ],422);
        }

        try{
            DB::beginTransaction();
            $deliveryStatus = $request->all();
            $deliveryStatus["is_default"] =$request->input('is_default',0);
            $deliveryStatus["status"] =$request->input('status',1);
            if(isset($deliveryStatus["is_default"]) && $deliveryStatus["is_default"] == 1){
                /**
                 * if input request is_default is 1 then this function will set other whose status is 1 to 0
                 */
                $this->deliveryStatus->setOtherDeliveryStatusToNonDefault();
            }
            else{
                /**
                 *  this function will set slug with name pending to 1 if there is no is_default is 1 and slug name pending is present
                 */
                $this->deliveryStatus->setPendingSlugDefaultIfOtherSlugIsDefault();
            }

            $deliveryStatusCreated =$this->deliveryStatus->createDeliveryStatus($deliveryStatus);
            $this->logStoreHelper->storeLogInfo(array("Delivery Status", [
                "status" => "200",
                "message" => "Restaurant Created Successfully",
                "data" => $deliveryStatusCreated
            ]));
            foreach ($deliveryStatus['translation'] as $key => $translation) {
                $translation['delivery_status_id'] = $deliveryStatusCreated->id;
                $translation['lang_code'] = $key;
                $deliveryStatusId = $deliveryStatusCreated->id;
                $rules = [
                    "lang_code" => 'required|alpha',
                    "name" => "required"
                ];
                $translation['lang_code'] = $this->slugify->slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
                if (empty($translation['lang_code'])) {
                    return response()->json([
                        "status" => "422",
                        "message" => ["translation"=> ["lang_code" => "The lang_code may not only contain numbers,special characters."]]
                    ], 422);
                }
                $validator = Validator::make($translation, $rules);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError(array('Delivery Status Translation', [
                        'status' => '422',
                        "message" => ["translation"=>[$key => $error]]
                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => ["translation"=>[$key => $error]]
                    ], 422);
                }
                /*
                 * checking if the same delivery_status_id id have existing lang_code or not
                 * */
                try {
                    $checkExistingTranslation = $this->deliveryStatusTranslation
                        ->getSpecificDeliveryStatusTranslationByCodeAndId($translation["lang_code"], $deliveryStatusId);
                    switch ($checkExistingTranslation->first()) {
                        case !null: {
                            throw new \Exception();
                            break;
                        }
                        default: {
                            $createTranslation = $this->deliveryStatusTranslation->createDeliveryStatusTranslation($translation);
                            $this->logStoreHelper->storeLogInfo(array("Delivery Status Translation", [
                                "status" => "200",
                                "message" => "Delivery Status Translation Created Successfully",
                                "data" => $createTranslation
                            ]));
                        }
                    }
                } catch (\Exception $ex) {
                    $this->logStoreHelper->storeLogError(array("Delivery Status", [
                        "status" => "409",
                        "message" => "Language translation for given Delivery Status already exist"
                    ]));
                    return response()->json([
                        "status" => "409",
                        "message" => "Language translation for given Delivery Status  already exist"
                    ], 409);
                }
            }
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Delivery Status created successfully"
            ]);

        }
        catch (\Exception $exception){
            return response()->json([
                "status" => "500",
                "message" => "Error Creating Delivery status"
            ],500);
        }
    }

    /**
     * update delivery status.only permitted user can create delivery status.if input request is_default is 1 then will set other whose status is 1 to 0
     * and will set slug with name pending to 1 if there is no is_default is 1 and slug name pending is present
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus($id,Request $request){
        try{
            $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
            /**
             * custom validation of slug
             */
            $message =[
                "slug.required" => "The slug may not only contain numbers,special characters and should not be empty."
            ];

            $this->validate($request,[
                "status" => "integer|min:0|max:1",
                "is_default" => "integer|min:0|max:1",
                "slug" => "required|unique:delivery_status,slug,".$id,
                "translation" => "required|array"
            ],$message);
        }
        catch (\Exception $exception){
            $this->logStoreHelper->storeLogError(array("Delivery Status ",[
                "status"=>'422',
                "message"=>$exception->response->original
            ]));

            return response()->json([
                "status" => "422",
                "message" => $exception->response->original
            ],422);
        }

        try{
            DB::beginTransaction();
            /**
             * checking delivery status exist for update
             */
            try{
                $specificDeliveryStatus  = $this->deliveryStatus->getSpecificDeliveryStatus($id);
                /**
                 * deletes the all delivery status translation whose delivery_status_id is @param $id
                 */
                $this->deliveryStatusTranslation->deleteDeliveryStatusTranslationByDeliveryStatusId($specificDeliveryStatus->id);
            }
            catch (ModelNotFoundException $ex){
                return response()->json([
                    "status" => "404",
                    "message" => "Delivery Status could not be found"
                ],404);
            }
            $deliveryStatus = $request->all();
            if(isset($deliveryStatus["is_default"]) && $deliveryStatus["is_default"] == 1){
                /**
                 * if input request is_default is 1 then this function will set other whose status is 1 to 0
                 */
                $this->deliveryStatus->setOtherDeliveryStatusToNonDefault();
            }
            else{
                /**
                 *  this function will set slug with name pending to 1 if there is no is_default is 1 and slug name pending is present
                 */
              $this->deliveryStatus->setPendingSlugDefaultIfOtherSlugIsDefault();
            }
            /**
             * checks if the delivery status slug belongs to list of slugs list that are fixed
             * if belongs it  will not update requested slug
             */
            if($this->checkDeliveryStatusSlug($specificDeliveryStatus->slug)) {
                unset($deliveryStatus["slug"]);
            }
            $deliveryStatusUpdated = $this->deliveryStatus->updateDeliveryStatus($id, $deliveryStatus);
            $this->logStoreHelper->storeLogInfo(array("Delivery Status", [
                "status" => "200",
                "message" => "Delivery Status update Successfully",
                "data" => [$id,$deliveryStatusUpdated]
            ]));
            foreach ($deliveryStatus['translation'] as $key => $translation) {
                $translation['delivery_status_id'] = $id;
                $translation['lang_code'] = $key;
                $deliveryStatusId = $id;
                $rules = [
                    "lang_code" => 'required|alpha',
                    "name" => "required"
                ];
                $translation['lang_code'] = $this->slugify->slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
                if (empty($translation['lang_code'])) {
                    return response()->json([
                        "status" => "422",
                        "message" => ["lang_code" => "The lang_code may not only contain numbers,special characters."]
                    ], 422);
                }
                $validator = Validator::make($translation, $rules);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError(array('Delivery Status Translation', [
                        'status' => '422',
                        "message" => ["translation"=>[$key => $error]]
                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => ["translation"=>[$key => $error]]
                    ], 422);
                }
                /*
                 * checking if the same delivery_status_id id have existing lang_code or not
                 * */
                try {
                    $checkExistingTranslation = $this->deliveryStatusTranslation
                        ->getSpecificDeliveryStatusTranslationByCodeAndId($translation["lang_code"], $deliveryStatusId);
                    switch ($checkExistingTranslation->first()) {
                        case !null: {
                            throw new \Exception();
                            break;
                        }
                        default: {
                            $createTranslation = $this->deliveryStatusTranslation->createDeliveryStatusTranslation($translation);
                            $this->logStoreHelper->storeLogInfo(array("Delivery Status Translation", [
                                "status" => "200",
                                "message" => "Delivery Status Translation updated Successfully",
                                "data" => $createTranslation
                            ]));
                        }
                    }
                } catch (\Exception $ex) {
                    $this->logStoreHelper->storeLogError(array("Delivery Status", [
                        "status" => "409",
                        "message" => "Language translation for given Delivery Status  already exist"
                    ]));
                    return response()->json([
                        "status" => "409",
                        "message" => "Language translation for given Delivery Status already exist"
                    ], 409);
                }
            }
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Delivery Status Updated successfully"
            ]);

        }
        catch (\Exception $exception){
            return response()->json([
                "status" => "500",
                "message" => "Error Updating Delivery status"
            ],500);
        }
    }


    /**
     * Deletes specific delivery status and its child i.e delivery status translation
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteStatus($id){
        try{

            DB::beginTransaction();
            /*
             * checking delivery status exists or not
             * */
            $specificDeliveryStatus  = $this->deliveryStatus->getSpecificDeliveryStatus($id);
            /**
             * checks if the slug belongs to list of slugs list that are fixed
             * if belongs it wont allow to delete
             */
            if($this->checkDeliveryStatusSlug($specificDeliveryStatus->slug)){
                return response()->json([
                    "status" => "403",
                    "message" => "Requested delivery status slug is not allowed to delete"
                ],403);
            }
            try{
                /**
                 * this will deletes delivery status and its child table delivery status translation
                 */
                $this->deliveryStatus->deleteDeliveryStatus($specificDeliveryStatus);
                DB::Commit();
                $this->logStoreHelper->storeLogInfo(array("Delivery Status", [
                    "status" => "200",
                    "message" => "Delivery Status Deleted Successfully",
                    "data" =>$specificDeliveryStatus
                ]));
                return response()->json([
                    "status" => "200",
                    "message" => "Delivery Status Deleted Successfully"
                ]);
            }
            catch (\Exception $ex){
                $this->logStoreHelper->storeLogError(array("Delivery Status", [
                    "status" => "500",
                    "message" => "Error Deleting Delivery Status"
                ]));
                return response()->json([
                    "status" => "500",
                    "message" => "Error Deleting Delivery Status"
                ],500);
            }
        }
        catch (ModelNotFoundException $exception){
            $this->logStoreHelper->storeLogError(array("Delivery Status", [
                "status" => "404",
                "message" => "Request Delivery does not exist"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Request Delivery does not exist"
            ],404);
        }
    }

    /**
     * checks if the slug matches with list of slugs
     * @param $slug
     * @return bool
     */
    private function checkDeliveryStatusSlug($slug){
        $deliveryStatusToCheck =["pending","assigned","reassigned","picked","delivered","cancelled"];
        if(in_array($slug, $deliveryStatusToCheck)){
            return true;
        }
        return false;
    }


}