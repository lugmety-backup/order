<?php

namespace App\Http\Controllers\ShippingClass;


use App\Abstracts\ShippingMethod;
use Illuminate\Support\Facades\Config;
use RoleChecker;

class FlatRateShippingMethod extends ShippingMethod
{
    private $shipping;

    /**
     * FlatRateShippingMethod constructor.
     */
    public function __construct()
    {

    }

    public function index()
    {
        $taxAndOtherInfo["cost"] = $this->getShippingCost();
        return $taxAndOtherInfo;
    }


}
