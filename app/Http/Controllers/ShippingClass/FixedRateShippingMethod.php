<?php

namespace App\Http\Controllers\ShippingClass;


use App\Abstracts\ShippingMethod;

class FixedRateShippingMethod extends ShippingMethod
{
    private $shipping;
    /**
     * FlatRateShippingMethod constructor.
     */
    public function __construct()
    {
    }

    public function index(){
        $cost = $this->getShippingCost();
        $taxAndOtherInfo["cost"] = $cost;
        return $taxAndOtherInfo;
        }
}
