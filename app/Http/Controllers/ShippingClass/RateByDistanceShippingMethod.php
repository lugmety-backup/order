<?php

namespace App\Http\Controllers\ShippingClass;


use App\Abstracts\ShippingMethod;
use Illuminate\Support\Facades\Config;
use RoleChecker;

class RateByDistanceShippingMethod extends ShippingMethod
{
    private $shipping;
    /**
     * FlatRateShippingMethod constructor.
     */
    public function __construct()
    {

    }

    public function index(){
        $distance = $this->getDistance();
        $minCost=$this->getMinCost();
        $perCost=$this->getPerCost();
        $minDistance = $this->getMinDistance();
        if($distance <= $minDistance){
            $cost = $minCost;
        }
        else{
            $cost = round($minCost + ($distance-$minDistance)*$perCost,2);
        }
        $taxAndOtherInfo["cost"] = $cost;
        return $taxAndOtherInfo;
    }


}
