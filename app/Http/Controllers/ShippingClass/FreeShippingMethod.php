<?php

namespace App\Http\Controllers\ShippingClass;


use App\Abstracts\ShippingMethod;

class FreeShippingMethod extends ShippingMethod
{
    /**
     * FreeShippingMethod constructor.
     */
    public function __construct($shippingMethod)
    {
        $this->shipping_cost = $shippingMethod['shipping_cost'];
    }

    public function index(){
        $taxAndOtherInfo["cost"] = 0;
        return $taxAndOtherInfo;
    }
}
