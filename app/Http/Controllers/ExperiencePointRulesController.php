<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 11:45 AM
 */

namespace App\Http\Controllers;


use App\Repo\ExperiencePointRulesInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ExperiencePointRulesController extends Controller
{
    protected $experiencePointRules;
    protected $log;
    public function __construct(ExperiencePointRulesInterface $experiencePointRules,\LogStoreHelper $log)
    {
        $this->experiencePointRules = $experiencePointRules;
        $this->log = $log;
    }

    /**
     * returns all country rules based on @param country_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllRules(Request $request)
    {
        try {
            try {
                $this->validate($request, [
                    "country_id" => "required|integer|min:1"
                ]);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ], 422);
            }
            $getRules = $this->experiencePointRules->getAllRules($request->country_id);

            $getCountryRules = new Collection($getRules);

            return Datatables::of($getCountryRules)->make(true);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["all xp rules",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => "Error getting data"
            ],500);
        }
    }

    /**
     * return specified xp rules
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificRule($id){
        try{
            $getSpecificRule = $this->experiencePointRules->getSpecialRule($id);
            return response()->json([
                "status" => "200",
                "data" => $getSpecificRule
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Rule could not be found."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["specific xp rules",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => "Error getting data"
            ],500);
        }
    }

    /**
     * created new rules for xp
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRules(Request $request){
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1",
                "min_xp" => "required|min:0|numeric",
                "min_distance" => "required|min:0|numeric",
                "xp_per_km" => "required|min:0|numeric",
                "is_default" => "sometimes|boolean",
                "start_date" => "required_if:is_default,0|date_format:Y-m-d H:i:s",
                "end_date" => "required_if:is_default,0|date_format:Y-m-d H:i:s|after:start_date",

            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
            $request["is_default"] = $request->get("is_default",0);
            /**
             * if is_default is 1 then it will set other 0
             */
            $request = $request->all();
            if(isset($request["is_default"]) && ($request["is_default"] == 1)){
                $unset = $this->experiencePointRules->unsetDefault($request);
            }

            $createRules = $this->experiencePointRules->createRule($request);
            $this->log->storeLogInfo(["xp rules creation",
                [
                    "status" => "200",
                    "message" => "Experience rule created successfully",
                    "data" => $createRules->toArray()
                ]]
            );
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Experience rule created successfully",
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["create all xp rules",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => "Error creating experience rule"
            ],500);
        }
    }

    /**
     * updates the specified $id rule
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateRules(Request $request, $id){
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1",
                "min_xp" => "required|min:0|numeric",
                "min_distance" => "required|min:0|numeric",
                "xp_per_km" => "required|min:0|numeric",
                "is_default" => "sometimes|boolean",
                "start_date" => "required_if:is_default,0|date_format:Y-m-d H:i:s",
                "end_date" => "required_if:is_default,0|date_format:Y-m-d H:i:s|after:start_date",

            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
            $getRules = $this->experiencePointRules->getSpecialRule($id);
            $request = $request->all();

            if($getRules->is_default==1 && isset($request["is_default"]) && ($request["is_default"] == 0)){
                return response()->json([
                    "status" => "403",
                    "message" => "Default rule could not be changed to non default"
                ],403);
            }

            /**
             * if is_default is 1 then it will set other 0 and current id to 1
             */
            if(isset($request["is_default"]) && ($request["is_default"] == 1)){
                $updateIsDefault = $this->experiencePointRules->updateDefault($request,$id);
                $request["start_date"] = null;
                $request["end_date"] = null;

            }
            $createRules = $this->experiencePointRules->updateRule($getRules,$request);
            $this->log->storeLogInfo(["xp rules update",[$createRules]]);
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Experience rule updated successfully"
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Rule could not be found."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(["update xp rules",[$ex->getMessage()]]);
            return response()->json([
                "status" => "500",
                "message" => "Error updating experience rule"
            ],500);
        }
    }

    /**
     * deletes rules based on id
     * @param $id
     */
    public function deleteRules($id){
        try{
            $getRules = $this->experiencePointRules->getSpecialRule($id);
            if($getRules->is_default ==1){
                return response()->json([
                    "status" => "403",
                    "message" => "Default rule cannot be deleted.Please change it and try again"
                ],403);
            }
            DB::beginTransaction();
            /**
             * deletes the rule
             */
            $deletesRecord = $this->experiencePointRules->deleteRule($id);
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Rule deleted successfully"
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Rule could not be found."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError("update xp rules",[$ex->getMessage()]);
            return response()->json([
                "status" => "500",
                "message" => "Error deleting experience rule"
            ],500);
        }
    }

}