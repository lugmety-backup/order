<?php

namespace App\Http\Controllers;

use App\Repo\DriverOrderLocationInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use LogStoreHelper;
use Validator;
use RoleChecker;
/**
 * Class DriverOrderLocationController
 * @package App\Http\Controllers
 */
class DriverOrderLocationController extends Controller
{
    /**
     * @var DriverOrderLocationInterface
     */
    private $driverOrderLocation;
    /**
     * @var LogStoreHelper
     */
    private $logStoreHelper;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(DriverOrderLocationInterface $driverOrderLocation,
                                LogStoreHelper $logStoreHelper)
    {
        $this->driverOrderLocation = $driverOrderLocation;
        $this->logStoreHelper = $logStoreHelper;
    }

    /**
     * Display all the Driver Order Location
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        try{
            $driverOrderLocation = $this->driverOrderLocation->getAllDriverOrderLocation();
            if(count($driverOrderLocation) == 0){
                $this->logStoreHelper->storeLogInfo(array("DriverOrderLocation",[
                    'status'=>'404',
                    'message'=>'Empty Record'
                ]));
                return response()->json([
                    'status' => '404',
                    'message' => "Empty Record"
                ], 404);
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverOrderLocation",[
                'status'=>'404',
                'message'=>'Driver Order Location could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Driver Order Location could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("DriverOrderLocation",[
            'status'=>'200',
            'data'=>$driverOrderLocation
        ]));
        return response()->json([
            'status' => '200',
            'data' => $driverOrderLocation
        ], 200);
    }


    /**
     * Display all the Driver Order Location
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showByDriverOrderId($driver_order_id){
        try{
            $driverOrderLocation = $this->driverOrderLocation->getDriverOrderLocationByDriverOrderId($driver_order_id);
            if(count($driverOrderLocation) == 0){
                $this->logStoreHelper->storeLogInfo(array("DriverOrderLocation",[
                    'status'=>'404',
                    'message'=>'Empty Record'
                ]));
                return response()->json([
                    'status' => '404',
                    'message' => "Empty Record"
                ], 404);
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverOrderLocation",[
                'status'=>'404',
                'message'=>'Driver Order Location could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Driver Order Location could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("DriverOrderLocation",[
            'status'=>'200',
            'data'=>$driverOrderLocation
        ]));
        return response()->json([
            'status' => '200',
            'data' => $driverOrderLocation
        ], 200);
    }

    /**
     * Display specific Driver Point with given id.
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        try{
            $driverOrderLocation = $this->driverOrderLocation->getSpecificDriverOrderLocation($id);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverOrderLocation",[
                'status'=>'404',
                'message'=>'Driver Order Location could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Driver Order Location could not be found"
            ], 404);
        }


        $this->logStoreHelper->storeLogInfo(array("DriverOrderLocation",[
            'status'=>'200',
            'data'=>$driverOrderLocation
        ]));
        return response()->json([
            'status' => '200',
            'data' => $driverOrderLocation
        ], 200);
    }


    /**
     * Create new DriverOrderLocation.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeOrUpdate(Request $request)
    {
        /**
         * Validating the request
         */
        try{
            $this->validate($request, [
                'driver_order_id'=>'required|integer|min:1',
                'latitude'=>'required|numeric',
                'longitude'=>'required|numeric',
            ]);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverOrderLocation",[
                'status'=>'422',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }
        if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
            return response()->json([
                "status" => "403",
                "message" => "Only driver can create or update his/her location"
            ],403);
        }
        $request=$request->all();
        $request["driver_id"] = \RoleChecker::getUser();
        $driver = $this->driverOrderLocation->getDriverOrderLocationByDriverOrderId($request['driver_order_id']);
        if(count($driver) == 0){
            try{
                $driverOrderLocation = $this->driverOrderLocation->createDriverOrderLocation($request);
            }catch (\Exception $ex){
                $this->logStoreHelper->storeLogInfo(array("DriverOrderLocation",[
                    'status'=>'404',
                    'message'=>"Driver OrderLocation could not be created"
                ]));
                return response()->json([
                    'status'=>'404',
                    'message'=>"Driver OrderLocation could not be created"
                ],404);
            }
        }else{
            try{
                $this->driverOrderLocation->updateDriverOrderLocation($driver[0]['id'], $request);
            }catch (\Exception $ex){
                $this->logStoreHelper->storeLogInfo(array("DriverOrderLocation",[
                    'status'=>'404',
                    'message'=>"Driver OrderLocation could not be updated"
                ]));
                return response()->json([
                    'status'=>'404',
                    'message'=>"Driver OrderLocation could not be updated"
                ],404);
            }
        }

        $this->logStoreHelper->storeLogInfo(array("DriverOrderLocation",[
            'status'=>'200',
            'message'=>"Driver Order Location created successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Driver Order Location created successfully"
        ],200);

    }


}
