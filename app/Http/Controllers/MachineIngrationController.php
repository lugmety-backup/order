<?php

namespace App\Http\Controllers;

use App\Events\SendMessage;
use App\Restaurant;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

//use Auth;
//use Event;
//use App\Events\OrderWasConfirmed;
use Illuminate\Support\Facades\Config;
use Log;
use App\Models\Orders;
use Settings;
use App\Repo\MachineStatusInterface;
class MachineIngrationController extends Controller
{

    public $machineStatus;
    public $log = true;
    protected $orders;

    public function __construct(Orders $orders, MachineStatusInterface $machineStatus)
    {
        $this->orders = $orders;
        $this->machineStatus = $machineStatus;
    }

    public function sendPrinterRequest()
    {
        try {
            if (isset($_GET['a'])) {
                $resID = $_GET['a'];
            }
            if (isset($_GET['k'])) {
                $machineKey = $_GET['k'];
            }
            /**
             * update or create machine request timestamp to track machine is active or not
             */
            $getMachineStatus = $this->machineStatus->getSpecificMachineStatus($resID);
            if($getMachineStatus){
                $now = Carbon::now('utc');
                $updateMachineStatus = $this->machineStatus->updateMachineStatus($getMachineStatus,["updated_at" => $now]);
            }
            else{
                $createMachineStatus = $this->machineStatus->createMachineStatus(["restaurant_id" => $resID]);
            }

            /**
             * get machine retries limit from setting
             */
//            $retriesLimit = 0;
            $getSetting = Settings::getSettings('machine-retry-limit');
            if ($getSetting["status"] == "200") {
                $retriesLimit = (int)$getSetting["message"]["data"]["value"];
            } else {
                $retriesLimit = 2;
            }
            /**
             * get latest record if exist
             */
            $orderToBeUpdated = $order = $this->orders->latest()->where([
                ['order_status', 'pending'],
                ["restaurant_id", $resID],
                ["machine_retry", "<=", $retriesLimit]
            ])->first();
            if($orderToBeUpdated) {
                $order->makeVisible('restaurant');
                $restaurant = unserialize($order->restaurant);
            }
            else{
                die();
            }
            if ($restaurant && ($restaurant["machine_ref_key"] == $machineKey)) {

                /**
                 * subtract current date time by 1 hour
                 */
                $currentDateSubtractedByHour = Carbon::now('utc')->subHour();
                /**
                 * converting above obtained date time into restaurant country timezone
                 */
                $currentDateSubtractedByHourInTimeZone = $currentDateSubtractedByHour->tz($restaurant['timezone'])->format("Y-m-d H:i:s");
                /**
                 * gets the pre_order record whose pre_order date is less than hour and select the most priority order .i.e selcect older record
                 */
                $getPreorder = $this->orders->where([
                    ['order_status', 'pending'],
                    ["restaurant_id", $resID],
                    ["pre_order", 1],
                    ["machine_retry", "<=", $retriesLimit],
                    ["delivery_or_pickup_time", "!=", null],
                    ["delivery_or_pickup_time", ">=", $currentDateSubtractedByHourInTimeZone]
                ])->orderBy('delivery_or_pickup_time', 'ASC')->first();

                if ($getPreorder) {
                    $orderToBeUpdated = $order = $getPreorder;
                }
                $orderToBeUpdated->update(["machine_retry" => $order->machine_retry, "last_machine_checked_date" => Carbon::now("utc")]);

                if ($order && $order->payment_method == 'creditcard') {
                    if ($order->payment_status != 'completed') {
                        $order = null;
                    }
                }


                if ($order) {
                    $this->log($order);

                    return $this->printOrder($restaurant, $order);


                } else {
                    $this->log('no order yet');

                }

            } else {
                $this->log('machine credentials did not match');
            }
        } catch (\Exception $e) {
            $this->log('machine print request error: ' . $e->getMessage());

        }

    }


    public function printerResponseRequest()
    {
        try {
            $this->log('machine callback: ');
            $this->log(json_encode($_REQUEST));

            if (isset($_GET['a'])) {
                $ResID = $_GET['a'];
            }
            if (isset($_GET['o'])) {
                $orderID = $_GET['o'];
            }
            if (isset($_GET['ak'])) {
                $acceptedOrRejected = ucfirst(strtolower($_GET['ak']));
            }
            if (isset($_GET['m'])) {
                $descOfAcceptingOrRejecting = $_GET['m'];
            }
            if (isset($_GET['dt'])) {
                $operationTimeStamp = strtotime(($_GET['dt']));
                $operationTime = date("Y-m-d H:i:s", $operationTimeStamp);
            }
            if (isset($_GET['k'])) {
                $machineKey = $_GET['k'];
            }
            $order = $this->orders->where([
                ['id', $orderID],
                ["restaurant_id", $ResID]
            ])->first();
            if($order) {
                $order->makeVisible('restaurant');
                $restaurant = unserialize($order->restaurant);
            }
            else{
                die();
            }
            if ($restaurant && ($restaurant["machine_ref_key"] == $machineKey)) {
                if ($order) {
                    $getSetting = Settings::getSettings('threshold-delivery-time');
                    if ($getSetting["status"] == "200") {
                        $getThresholdDeliveryTime = $getSetting["message"]["data"]["value"];
                    } else {
                        $getThresholdDeliveryTime = "30";
                    }
                    $previousOrderStatus = ucfirst($order->order_status);

//                    if($order->pre_order == "1" && !is_null($order->delivery_or_pickup_time)){
//                        $final_delivery_time = Carbon::parse($order->delivery_or_pickup_time)->addMinutes($getThresholdDeliveryTime);
//                    }
//                    else {
//                        $final_delivery_time = Carbon::parse($order->api_delivery_time)->addMinutes($getThresholdDeliveryTime);
//                    }


                    if ($acceptedOrRejected == 'Accepted') {
                        $final_delivery_time = $this->calculateDeliveryTime($order, $operationTime, $getThresholdDeliveryTime);
//                        foreach ($order->orderItems as $item) {
                        $order->update([
                            'order_status' => 'accepted',
                            'api_delivery_time' => $operationTime,
                            'is_seen' => "1",
                            "delivery_time" => $final_delivery_time,
                            "threshold_delivery_time" => $getThresholdDeliveryTime
                        ]);
//                        }
                    } else {
//                        foreach ($order->orderItems as $item) {
                        $order->update([
                            'order_status' => 'rejected',
                            'is_seen' => "1",
//                            "delivery_time" => $final_delivery_time,
//                            "threshold_delivery_time" => $getThresholdDeliveryTime
                        ]);
//                        }
                    }

                    if ($previousOrderStatus != $acceptedOrRejected) {
                        $order = $this->orders->where([
                            ['id', $orderID],
                            ["restaurant_id", $ResID]
                        ])->first();
                        $order->makeVisible('restaurant');
                        $order->makeVisible('extra_info');
                        $order->makeVisible('delivery_time');
                        $order["restaurant"] = unserialize($order->restaurant);
                        $order['extra_info'] = unserialize($order['extra_info']);
                        if ($acceptedOrRejected == 'Accepted' || $acceptedOrRejected == 'Rejected') {
                            if ($acceptedOrRejected == 'Accepted') {
                                $message = 'order accepted by restaurant';
                                $country_id = $order['restaurant']['country_id'];
                            } else {
                                $message = 'order rejected';
                                $country_id = '';
                            }
                            $array = [
                                'service' => 'order service',
                                'message' => $message,
                                'data' => [
                                    'user_details' => ['id' => $order['customer_id']],
                                    'order' => $order->toArray(),
                                    'restaurant_id' => $order['restaurant_id'],
                                    'parent_id' => $restaurant['parent_id'],
                                    'country_id' => $country_id
                                ]
                            ];

                            event(new SendMessage($array));
                        }
                    }

                } else {
                    $this->log('invalid order no');
//                    return 'invalid order no';
                }
            } else {
                $this->log('machine credentials did not match');
//            return "credentials did not match";
            }
        } catch (\Exception $e) {
            $this->log('machine callback error: ' . $e->getMessage());
        }

    }


    public function printOrder($restaurant, $order)
    {
        $taxAmount = $order->site_tax_rate;
        $grandTotal = $totalWithTax = $order->amount;
        $orderData = [];
        $orderData['Restaurant Id'] = $restaurant["id"];
        $orderData['Order Type'] = ($order->delivery_type == 'dynamic-distance') ? 1 : 2;
        $orderData['Order No'] = $order->id;
        $orderData['Order Items'] = '';
        $orderData['Requested For'] = date('H:i d-m-Y', strtotime($order->created_at));
        $orderData['Last Order No'] = '';
        $orderData['Payment Status'] = ($order->payment_status == 'completed') ? 6 : 7;

        $orderData['Payment Method'] = $order->payment_method;
        $orderData['Delivery Charge'] = $order->currency . '' . number_format((float)$order->delivery_fee, 2, '.', '');


//        $orderData['CC + Handling Fees'] = $order->currency.''.number_format((float)20, 2, '.', '');
        $orderData['Total Amount'] = $order->currency . '' . number_format((float)$grandTotal, 2, '.', '');
        $orderData['Customer Type'] = ($order->customer_id) ? 4 : 5;
        $userDetails = $this->oauthCall($order->customer_id, $order->payment_method);
        $orderData['Customer Comments'] = $order->customer_note;
        if ($order->delivery_type !== "pickup") {
            $orderData['Customer Address'] = $userDetails[1];
        } else {
            $orderData['Customer Address'] = $this->customerAddress($order);
        }

        $orderData['Customer Name'] = $userDetails[0]["name"];
        $orderData['Customer Phone'] = $userDetails[0]["mobile"];
        $total = 0;
//        $orderData['Customer Name'] = $order->order->user->first_name.' '. $order->order->user->last_name;
//        $orderData['Customer Phone'] = $order->order->user->mobile;
        foreach ($order->orderItem as $cart) {
            $addonTotal = 0;
            $itemTotal = 0;
            $foodDetails = unserialize($cart->food_raw_data);
            $food_size = (isset($foodDetails["size_title"])) ? $this->languageTranslate($foodDetails["size_title"]) : ';';
            $special_instruction = (isset($foodDetails["special_instruction"])) ? $foodDetails["special_instruction"] : ';';
            $orderData['Order Items'] .= $cart['quantity'] . ';' . $this->languageTranslate(unserialize($foodDetails["food_name"])) . ';' . $order->currency . '' . number_format((float)$cart['unit_price'] * $cart['quantity'], 2, '.', '') . "|" . $food_size . ';;|';
            if (count($addons = $cart->OrderItemAddon) > 0) {
                foreach ($addons as $addon) {
                    $addonTotal += $addon["unit_price"];
                    $orderData['Order Items'] .= $cart['quantity'] . ';' . $this->languageTranslate(unserialize($addon["addon_name"])) . ';' . $order->currency . '' . number_format((float)$addon['unit_price'] * $cart['quantity'], 2, '.', '') . "|" . $special_instruction . ';;|';
                }
            }
//            else{
//                $orderData['Order Items'] .= ";;;|";
//            }
//            $itemTotal = $cart["quantity"] * ($cart["unit_price"] + $addonTotal);
            /**
             * calculates total price for this order
             */
            $total += $cart["quantity"] * ($cart["unit_price"] + $addonTotal);
        }
        $taxAmount =  $order->tax;
        $orderData['Tax'] = $order->currency . '' . number_format((float)$taxAmount, 2, '.', '');
        return $order = "#" . $orderData['Restaurant Id'] .
            "*" . $orderData['Order Type'] .
            "*" . $orderData['Order No'] .
            "*" . $orderData['Order Items'] .
            "*" . $orderData['Delivery Charge'] .
            "*" . $orderData['Tax'] .
//            ";".$orderData['CC + Handling Fees'].
            ";" . $orderData['Total Amount'] .
            ";" . $orderData['Customer Name'] .
            ";" . $orderData['Customer Address'] .
            ";" . $orderData['Customer Phone'] .
            ";" . $orderData['Payment Method'] .
            ";*" . $orderData['Customer Comments'] . "#";

        return $order;
    }


    public function log($message)
    {
        if ($this->log == true) {
            Log::info($message);
        }
    }

    /**
     * get shipping address of user
     * @param $order
     * @return string
     */
    public function customerAddress($order)
    {

        $locationBaseUrl = Config::get('config.location_base_url');
        $fullUrl = $locationBaseUrl . "/machine/integration/shipping/address/$order->shipping_id";
        $getShippingAddress = \RemoteCall::getSpecificWithoutToken($fullUrl);
        if ($getShippingAddress["status"] == 200) {

            $shippingAddress = $getShippingAddress["message"]["data"];
            $customerAddress = $shippingAddress["street"]
                . ', ' . $shippingAddress["district_name"]
                . ', ' . $shippingAddress["city_name"];
        } else {
            $customerAddress = "";
        }
        return $customerAddress;
    }

    /**
     * Translate restaurant name into passed param $lang
     * @param $resData
     * @param $lang
     * @return string
     */
    private function languageTranslate($resData)
    {
        $lang = "en";
        if (isset($resData['en'])) {
            $resName = isset($resData[$lang]) ? $resData[$lang] : $resData['en'];
        } else {
            $resName = ';';
        }
        return $resName;
    }

    private function oauthCall($userId, $order)
    {
        $fullUrl = Config::get("config.oauth_base_url") . "/machine/user/$userId/detail";
        $oauthResponse = \RemoteCall::getSpecificWithoutToken($fullUrl);
        if ($oauthResponse["status"] == 200) {
            $oauth = $oauthResponse["message"]["data"];
            $userData = [];
            $user['name'] = $oauth["first_name"];
            if (!empty($oauth["middle_name"])) {
                $user['name'] .= " " . $oauth["middle_name"];
            }
            $user["name"] .= " " . $oauth["last_name"];
            $user["mobile"] = $oauth["mobile"];
            if ($order == "creditcard") {
                $customerAddress = $oauth["address_line_1"]
                    . ', ' . $oauth["city"]
                    . ', ' . $oauth["country"];
            } else {
                $customerAddress = $oauth["country"];
            }
            return [$user, $customerAddress];
        } else {
            return [[
                "name" => "",
                "mobile" => ""
            ], ""
            ];
        }


    }

    /**
     * returns delivery_time for pre order and normal order
     * @param $order
     * @param $request api_delivery_time obtained from machine
     * @param $getThresholdDeliveryTime
     * @return static
     */
    private function calculateDeliveryTime($order, $request, $getThresholdDeliveryTime)
    {
        $order->makeVisible(["restaurant", "delivery_time"]);
        if($order["delivery_type"] == "pickup"){
            $getThresholdDeliveryTime = 0;
        }
        $timeZone = unserialize($order->restaurant)["timezone"];
        if (empty($order["delivery_time"]) || is_null($order["delivery_time"])) {
            $oldDeliveryTime = Carbon::createFromFormat("Y-m-d H:i:s", $request, $timeZone)->addMinutes($getThresholdDeliveryTime);
        } else {
            $oldDeliveryTime = Carbon::createFromFormat("Y-m-d H:i:s", $order["delivery_time"], $timeZone);

        }
//        if ($order->pre_order == "1" && !is_null($order->delivery_or_pickup_time)) {
//
////            $oldDeliveryTime = Carbon::createFromFormat("Y-m-d H:i:s",$order["delivery_or_pickup_time"],$timeZone);
//            if (empty($order["threshold_delivery_time"]) || is_null($order["threshold_delivery_time"])) {
//                $final_delivery_time = $oldDeliveryTime->addMinutes($getThresholdDeliveryTime);
//            } else {
//                $oldDeliveryTime = $oldDeliveryTime->subMinutes($order["threshold_delivery_time"]);
//                $final_delivery_time = $oldDeliveryTime->addMinutes($getThresholdDeliveryTime);
//            }
//        } else {
//            if ((empty($order["threshold_delivery_time"]) || is_null($order["threshold_delivery_time"])) && (empty($order["api_delivery_time"]) || is_null($order["api_delivery_time"]))) {
//                $newApiDeliveryTime = Carbon::createFromFormat("Y-m-d H:i:s", $request, $timeZone);
//                $difference = $oldDeliveryTime->diffInMinutes($newApiDeliveryTime, false);
//                $final_delivery_time = $oldDeliveryTime->addMinutes($difference)->addMinutes($getThresholdDeliveryTime);
//
//
//            } else {
                $thresholdDeliveryTime = (empty($order["threshold_delivery_time"]) || is_null($order["threshold_delivery_time"])) ? 0 : $order["threshold_delivery_time"];
                $removingPreviousThresholdDeliveryTime = $oldDeliveryTime->subMinutes($thresholdDeliveryTime);
                if (empty($order["api_delivery_time"]) || is_null($order["api_delivery_time"])) {
                    $oldDeliveryTime = $removingPreviousThresholdDeliveryTime;
                } else {
                    $oldApiDeliveryTime = Carbon::createFromFormat("Y-m-d H:i:s", $order["api_delivery_time"], $timeZone);
                    $differenceWithPreviousApiDeliveryTime = $removingPreviousThresholdDeliveryTime->diffInMinutes($oldApiDeliveryTime, false);
                    $oldDeliveryTime = $removingPreviousThresholdDeliveryTime->addMinutes($differenceWithPreviousApiDeliveryTime);
                }
                $newApiDeliveryTime = Carbon::createFromFormat("Y-m-d H:i:s", $request, $timeZone);
                $differenceWithNewApiDeliveryTime = $oldDeliveryTime->diffInMinutes($newApiDeliveryTime, false);
                $final_delivery_time = $oldDeliveryTime->addMinutes($differenceWithNewApiDeliveryTime)->addMinutes($getThresholdDeliveryTime);

//            }
//
//        }
        return $final_delivery_time;
    }

}
