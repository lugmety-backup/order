<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 6/5/2018
 * Time: 12:09 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use LogStoreHelper;
use App\Repo\OrderSettingInterface;
class OrderSettingController extends Controller
{
    /**
     * @var LogStoreHelper
     */
    protected  $log;
    /**
     * @var OrderSettingInterface
     */
    protected $orderSetting;

    /**
     * OrderSettingController constructor.
     * @param LogStoreHelper $logStoreHelper
     * @param OrderSettingInterface $orderSetting
     */
    public function __construct(LogStoreHelper $logStoreHelper, OrderSettingInterface $orderSetting)
    {
        $this->log = $logStoreHelper;
        $this->orderSetting = $orderSetting;
    }

    /**
     * create or update setting
     * @param array $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createOrUpdateSetting(Request $request){
        try{
            $this->validate($request,[
                'hash' => 'required|string',
                'data' => 'required|array',
                'date_for' => 'required|date_format:Y-m',
                'type' => 'required|string'
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
            if(!password_verify('Nepal123$',$request->hash)){
                return response()->json([
                    'status' => '403',
                    'message' => 'forbidden'
                ],403);
            }

            /**
             * fetch the setting based on the type and the date
             */
            $getSetting = $this->orderSetting->getSetting($request->type,$request->date_for);
            /**
             * update the original setting
             */
            if($getSetting){
                $this->orderSetting->updateSetting($getSetting,$request->all());
                DB::commit();
                $this->log->storeLogInfo(['update original order setting',[
                    'request' =>  $request->all(),
                    'original_setting' => $getSetting
                ]]);
            }

            /**
             * create new setting
             */
            else{
                $this->orderSetting->createSetting($request->all());
                DB::commit();
                $this->log->storeLogInfo(['new order setting',[
                    'request' =>  $request->all()
                ]]);
            }
            return response()->json([
                'status' => '200',
                'message' => 'Setting created successfully'
            ],200);

        }
        catch (\Exception $ex){
            $this->log->storeLogError([' Error in order setting',[
                'request' =>  $request->all(),
                'message' => $ex->getMessage()
            ]]);
            return response()->json([
                'status' => '500',
                'message' => 'Error in creating setting'
            ],500);
        }
    }

}