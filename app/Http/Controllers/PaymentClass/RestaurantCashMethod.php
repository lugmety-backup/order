<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/24/2018
 * Time: 5:35 PM
 */

namespace App\Http\Controllers\PaymentClass;


use App\Abstracts\PaymentMethod;

class RestaurantCashMethod extends PaymentMethod
{
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $request['order_status'] = $this->getOrderStatus();
        $request['payment_status'] = $this->getPaymentStatus();
        $request['payment_ref_id'] = $this->getPaymentRefId();

        return $request;
    }
}