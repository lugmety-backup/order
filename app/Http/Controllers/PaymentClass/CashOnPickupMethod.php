<?php

/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 6/26/17
 * Time: 2:36 PM
 */

namespace App\Http\Controllers\PaymentClass;

use App\Abstracts\PaymentMethod;

class CashOnPickupMethod extends PaymentMethod
{
    /**
     * CashOnPickupMethod constructor.\
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function index()
    {
        $request['order_status'] = $this->getOrderStatus();
        $request['payment_status'] = $this->getPaymentStatus();
        $request['payment_ref_id'] = $this->getPaymentRefId();

        return $request;

    }

}