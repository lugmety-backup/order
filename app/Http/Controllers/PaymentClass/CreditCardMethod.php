<?php

/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 6/26/17
 * Time: 12:19 PM
 */
namespace App\Http\Controllers\PaymentClass;

use App\Abstracts\PaymentMethod;
use RemoteCall;
use Illuminate\Support\Facades\Config;

class CreditCardMethod extends PaymentMethod
{
    /**
     * CreditCardMethod constructor.
     */
    public function __construct()
    {

    }

    /**
     * Calls paytabs service api to create pay page and redirection url  for payment.
     * @return mixed
     */
    public function createPayPage()
    {
        $data = \Settings::getSettings('paytabs-email');
        if ($data["status"] != 200) {
            return response()->json(
                $data["message"]
                , $data["status"]);
        }
        $request['merchant_email'] = $data['message']['data']['value'];
//        $request['merchant_email'] = Config::get('config.paytabs.merchant_email');

        $data = \Settings::getSettings('paytabs-secret-key');
        if ($data["status"] != 200) {
            return response()->json(
                $data["message"]
                , $data["status"]);
        }
        $request['secret_key'] = $data['message']['data']['value'];
//        $request['secret_key'] = Config::get('config.paytabs.secret_key');

        $data = \Settings::getSettings('paytabs-cms-with-version');
        if ($data["status"] != 200) {
            return response()->json(
                $data["message"]
                , $data["status"]);
        }
        $request['cms_with_version'] = $data['message']['data']['value'];
//        $request['cms_with_version'] = Config::get('config.paytabs.cms_with_version');

        $data = \Settings::getSettings('paytabs-site-url');
        if ($data["status"] != 200) {
            return response()->json(
                $data["message"]
                , $data["status"]);
        }
        $request['site_url'] = $data['message']['data']['value'];
//        $request['site_url'] = Config::get('config.paytabs.site_url');

        $request['return_url'] = $this->getRedirectUrl();
        $request['title'] = $this->getTitle();
        $request['cc_first_name'] = $this->getCcFirstName();
        $request['cc_last_name'] = $this->getCcLastName();
        $request['cc_phone_number'] = $this->getCcPhoneNumber();
        $request['phone_number'] = $this->getPhoneNumber();
        $request['email'] = $this->getEmail();
        $request['products_per_title'] = $this->getProductsPerTitle();
        $request['unit_price'] = $this->getUnitPrice();
        $request['quantity'] = $this->getQuantity();
        $request['other_charges'] = $this->getOtherCharges();
        $request['amount'] = $this->getAmount();
        $request['discount'] = $this->getDiscount();
        $request['currency'] = $this->getCurrency();
        $request['reference_no'] = $this->getReferenceNo();
        $request['ip_customer'] = $this->getIpCustomer();
        $request['ip_merchant'] = $this->getIpMerchant();
        $request['billing_address'] = $this->getBillingAddress();
        $request['city'] = $this->getCity();
        $request['postal_code'] = $this->getPostalCode();
        $request['country'] = $this->getCountry();
        $request['state'] = $this->getState();
        $request['address_shipping'] = $this->getAddressShipping();
        $request['state_shipping'] = $this->getStateShipping();
        $request['msg_lang'] = $this->getMsgLang();
        $request['city_shipping'] = $this->getCityShipping();
        $request['country_shipping'] = $this->getCountryShipping();
        $request['postal_code_shipping'] = $this->getPostalCodeShipping();

        return RemoteCall::store("https://www.paytabs.com/apiv2/create_pay_page", $request);

    }

    /**
     * Calls paytabs service api to verify the transaction of the user after creating pay page.
     * @return mixed
     */
    public function verifyPayment($use_sdk)
    {
        $data = \Settings::getSettings('paytabs-email');
        if ($data["status"] != 200) {
            return response()->json(
                $data["message"]
                , $data["status"]);
        }
        $request['merchant_email'] = $data['message']['data']['value'];
//        $request['merchant_email'] = Config::get('config.paytabs.merchant_email');

        $data = \Settings::getSettings('paytabs-secret-key');
        if ($data["status"] != 200) {
            return response()->json(
                $data["message"]
                , $data["status"]);
        }
        $request['secret_key'] = $data['message']['data']['value'];
//        $request['secret_key'] = Config::get('config.paytabs.secret_key');
        if($use_sdk){
            $request['transaction_id'] = $this->getPaymentReference();

            return RemoteCall::store("https://www.paytabs.com/apiv2/verify_payment_transaction", $request);
        }
        else{
            $request['payment_reference'] = $this->getPaymentReference();

            return RemoteCall::store("https://www.paytabs.com/apiv2/verify_payment", $request);
        }

    }

}