<?php

namespace App\Http\Controllers;

use App\Repo\DriverPointsInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use LogStoreHelper;
use Validator;

/**
 * Class DriverPointsController
 * @package App\Http\Controllers
 */
class DriverPointsController extends Controller
{
    /**
     * @var DriverPointsInterface
     */
    private $driverPoints;
    /**
     * @var LogStoreHelper
     */
    private $logStoreHelper;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(DriverPointsInterface $driverPoints,
                                LogStoreHelper $logStoreHelper)
    {
        $this->driverPoints = $driverPoints;
        $this->logStoreHelper = $logStoreHelper;
    }

    /**
     * Display all the Driver Points
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        try{
            $driverPoints = $this->driverPoints->getAllDriverPoints();
            if(count($driverPoints) == 0){
                $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                    'status'=>'404',
                    'message'=>'Empty Record'
                ]));
                return response()->json([
                    'status' => '404',
                    'message' => "Empty Record"
                ], 404);
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                'status'=>'404',
                'message'=>'Driver Points could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Driver Points could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
            'status'=>'200',
            'data'=>$driverPoints
        ]));
        return response()->json([
            'status' => '200',
            'data' => $driverPoints
        ], 200);
    }


    /**
     * Display all the Driver Points
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexByDriverName(){
        try{
            $driverPoints = $this->driverPoints->getAllDriverPointsByUserId();
            if(count($driverPoints) == 0){
                $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                    'status'=>'404',
                    'message'=>'Empty Record'
                ]));
                return response()->json([
                    'status' => '404',
                    'message' => "Empty Record"
                ], 404);
            }
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                'status'=>'404',
                'message'=>'Driver Points could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Driver Points could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
            'status'=>'200',
            'data'=>$driverPoints
        ]));
        return response()->json([
            'status' => '200',
            'data' => $driverPoints
        ], 200);
    }

    /**
     * Display specific Driver Point with given id.
     * @param $slug
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        try{
            $driverPoints = $this->driverPoints->getSpecificDriverPoints($id);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                'status'=>'404',
                'message'=>'Driver Points could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Driver Points could not be found"
            ], 404);
        }


        $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
            'status'=>'200',
            'data'=>$driverPoints
        ]));
        return response()->json([
            'status' => '200',
            'data' => $driverPoints
        ], 200);
    }


    /**
     * Create new DriverPoints.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        /**
         * Validating the request
         */
        try{
            $this->validate($request, [
                'user_id'=>'required|integer|min:1',
                'point'=>'required|numeric',
            ]);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                'status'=>'422',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }
        $request=$request->all();

        try{
            $driverPoints = $this->driverPoints->createDriverPoints($request);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                'status'=>'404',
                'message'=>"Driver Points could not be created"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Driver Points could not be created"
            ],404);
        }
        $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
            'status'=>'200',
            'message'=>"Driver Points created successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Driver Points created successfully"
        ],200);

    }

    /**
     * Edit Specific Driver Points
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        /**
         * Checking if the given DriverPoint with given id is available or not
         */
        try{
            $driverPoints = $this->driverPoints->getSpecificDriverPoints($id);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                'status'=>'404',
                'message'=>"Driver Points could not be found"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=> "Driver Points could not be found"
            ],404);
        }
        /**
         * Validating the request
         */
        try{
            $this->validate($request, [
                'user_id'=>'required|integer|min:1',
                'point'=>'required|numeric',
            ]);
        } catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                'status'=>'422',
                'message'=>'Validation Error',
                'data' => $ex->response->original
            ]));
            return response()->json([
                'status'=>'422',
                'message'=>$ex->response->original
            ],422);
        }

        $request=$request->all();

        try{
            $this->driverPoints->updateDriverPoints($id, $request);

         }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                'status'=>'404',
                'message'=>"Driver Points could not be updated"
            ]));
            return response()->json([
                'status'=>'404',
                'message'=>"Driver Points could not be updated"
            ],404);
        }
        $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
            'status'=>'200',
            'message'=>"Driver Points updated successfully"
        ]));
        return response()->json([
            'status'=>'200',
            'message'=>"Driver Points updated successfully"
        ],200);
    }

    /**
     * Delete Specific Driver Points.
     *
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        try{
            $driverPoints = $this->driverPoints->getSpecificDriverPoints($id);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                'status'=>'404',
                'message'=>'Driver Points could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Driver Points could not be found"
            ], 404);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                'status'=>'404',
                'message'=>'Driver Points could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Driver Points could not be found"
            ], 404);
        }
        try{
            $this->driverPoints->deleteDriverPoints($id);
        }catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                'status'=>'404',
                'message'=>'Driver Points could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Driver Points could not be found"
            ], 404);
        }catch (\Exception $ex){
            $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
                'status'=>'404',
                'message'=>'Driver Points could not be found'
            ]));
            return response()->json([
                'status' => '404',
                'message' => "Driver Points could not be found"
            ], 404);
        }
        $this->logStoreHelper->storeLogInfo(array("DriverPoints",[
            'status'=>'200',
            'message'=> "Driver Points Deleted Successfully"
        ]));
        return response()->json([
            'status' => '200',
            'message' => "Driver Points Deleted Successfully"
        ], 200);
    }

    public function test(Request $request){
        $distance = \CalculateDistance::getDistance(80,20,80,20);
        return $distance;
    }
}
