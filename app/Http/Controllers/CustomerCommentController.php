<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 5/15/2018
 * Time: 2:26 PM
 */

namespace App\Http\Controllers;


use App\Repo\UserCommentInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use RoleChecker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
class CustomerCommentController extends Controller
{
    protected $user_comment;

    /**
     * @var LogStoreHelper
     */
    protected $logStoreHelper;

    public function __construct(UserCommentInterface $comment,\LogStoreHelper $logStoreHelper)
    {
        $this->user_comment = $comment;
        $this->logStoreHelper = $logStoreHelper;
    }

    /**
     * list specific user comments commented by admin
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllUserComments(Request $request){
        try {
            /**
             * if limit param is other than integer and validation fails then limit is set to 10
             */
            $this->validate($request,[
                'limit' => 'required|integer|min:1',
            ]);
        }
        catch (\Exception $ex){
            $request->merge([
                'limit' => 10
            ]);
        }

        try {
            /**
             * if limit param is other than integer and validation fails then limit is set to 10
             */
            $this->validate($request,[
                'user_id' => 'required|integer|min:1',
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }

        try {

            $comments = $this->user_comment->getAllCommentsByUserId($request->user_id, $request["limit"]);
            $userData = [];
            foreach ($comments as $comment) {
                $userData[] = $comment["admin_id"];
            }
            $userDatas["users"] = array_unique($userData);
            $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list";
            $getUserDetailsForReview = \RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);
            if ($getUserDetailsForReview["status"] != "200") {
                throw new \Exception($getUserDetailsForReview["message"]["message"]);
            }
            $userDatasFromOauth = $getUserDetailsForReview["message"]["data"];
            if (empty($userDatasFromOauth)) {
                throw new \Exception("empty admin record");
            }
            foreach ($comments as $key => $comment) {

                $comment["comment_at"] = Carbon::createFromFormat('Y-m-d H:i:s', $comment["updated_at"])->diffForHumans();
                $comment->makeHidden(["created_at", "updated_at"]);
                foreach ($userDatasFromOauth as $userData) {
                    if ($comment->admin_id !== $userData["id"]) continue;
                    $comment["name"] = implode(" ", array($userData["first_name"], $userData["middle_name"], $userData["last_name"]));
                }
                if (!isset($comment["name"])) unset($comments[$key]);
            }
            return response()->json([
                "status" => "200",
                "data" => $comments->appends([
                    "user_id" => $request->user_id,
                    'limit' => ($request->has('limit'))?$request->limit:null
                ])
            ]);
        }

        catch (\Exception $ex){
            $this->logStoreHelper->storeLogError(["listing user comments",[
                'data' => $request->all(),
                'message' => $ex->getMessage()
            ]]);
            return response()->json([
                "status" => "200",
                "data" => []
            ]);
        }
    }

    /**
     * create user comment
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createComment(Request $request){
        try {
            $this->validate($request, [
                "user_id" => "required|integer|min:1",
                "comment" => "required",
            ]);

        }catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try {
            DB::beginTransaction();
            $request["admin_id"] = RoleChecker::getUser();

            $comment = $this->user_comment->createComment($request->all());
            $this->logStoreHelper->storeLogInfo(array("User Comment Creation",[
                "status" => "200",
                "message" => "User Comment created successfully",
                "data" => $comment
            ]));
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "User Comment created successfully"
            ]);
        }catch (\Exception $ex){

            $this->logStoreHelper->storeLogError(array("User Comment Creation",[
                "status" => "500",
                'data' => $request->all(),
                "message" => "Error Creating user comments"
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error Creating user comment"
            ],500);
        }
    }

    /**
     * Updating previously created comment
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateComment($id,Request $request){
        try {
            $this->validate($request, [
                "comment" => "required",
            ]);

        }catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try {
            $userId = RoleChecker::getUser();
            DB::beginTransaction();
            /**
             * checks if user_id matches with the order comment to be updated
             */
            if($userId != $this->user_comment->getSpecificComment($id)->admin_id){

                $this->logStoreHelper->storeLogError(array("Order Comment Creation",[
                    "status" => "403",
                    "message" => "You can update your comment only"
                ]));
                return response()->json([
                    "status" => "403",
                    "message" => "You can update your comment only"
                ],403);
            }
            $comment = $this->user_comment->updateComment($id,$request->all());
            $this->logStoreHelper->storeLogInfo(array("Updating Order Comment",[
                "status" => "200",
                "message" => "User Comment update successfully",
                "data" => $comment
            ]));
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "User Comment updated successfully."
            ]);
        }
        catch (ModelNotFoundException $exception){
            $this->logStoreHelper->storeLogError(array("Order Comment Creation",[
                "status" => "404",
                "message" => "Requested User Comment not found"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Requested User Comment not found"
            ],404);
        }
        catch (\Exception $ex){

            $this->logStoreHelper->storeLogError(array("User Comment Update",[
                "status" => "500",
                'data' => $request->all(),
                "message" => "Error Updating user comment"
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error Updating User Comment"
            ],500);
        }
    }

    /**
     * deletes previously created comment
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteOrderComment($id){
        try{
            DB::beginTransaction();
            $userId = RoleChecker::getUser();
            $comment = $this->user_comment->getSpecificComment($id);

            /**
             * checks if user_id matches with the order comment to be deleted
             */

            if($userId != $comment->admin_id){
                $this->logStoreHelper->storeLogError(array("User Comment Deletion",[
                    "status" => "403",
                    "message" => "You can delete your comment only"
                ]));
                return response()->json([
                    "status" => "403",
                    "message" => "You can delete your comment only"
                ],403);
            }
            //deletes the order comment
            $this->user_comment->deleteComment($comment);
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "User Comment Deleted Successfully"
            ],200);
        }
        catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogError(array("Error in query",[
                "status" => "404",
                "message" => "Requested User Comment not found"
            ]));

            return response()->json([
                "status" => "404",
                "message" => "Requested User Comment not found"
            ],404);
        }
        catch (\Exception $ex){

            $this->logStoreHelper->storeLogError(array("Error deleting user comment",[
                "status" => "500",
                "message" => $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error Deleting user comment"
            ],500);
        }
    }


}