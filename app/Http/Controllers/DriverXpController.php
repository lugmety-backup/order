<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/19/2017
 * Time: 4:32 PM
 */

namespace App\Http\Controllers;


use App\Repo\DriverExperiencePointInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use RoleChecker;
class DriverXpController extends Controller
{
    protected $driverXp;
    protected  $log;
    public function __construct(DriverExperiencePointInterface $experiencePoint, \LogStoreHelper $logStoreHelper)
    {
        $this->driverXp = $experiencePoint;
        $this->log = $logStoreHelper;
    }

    /**
     * return all driver experience point for logged in driver
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllDriverXp(Request $request){
        try{
            $limit = $request->get("limit",20);
            $this->validate($request,[
                "limit" => "required|integer|min:1"
            ]) ;
        }
        catch (\Exception $ex){
            $limit = 20;
        }
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1"
            ]) ;
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
                return response()->json([
                    "status" => "403",
                    "message" => "Only driver has access"
                ],403);
            }
            $userId = \RoleChecker::getUser();
            $getAllDriverXp = $this->driverXp->getAllDriverXp($userId,$request->country_id, $limit);
            if(count($getAllDriverXp) == 0){
                return response()->json([
                    "status" => "404",
                    "message" => "Empty experience point"
                ],404);
            }

            $getXpInfo = $this->getTotalXp($userId,$request->country_id);
            $total = collect(
                ["total_xp" =>$getXpInfo["total_xp"],
//                   "available" =>$getXpInfo[0]
                ]);
            $getAllDriverXp = $total->merge($getAllDriverXp->withPath("/driver/xp")->appends(['limit' => $limit]));


            return response()->json([
                "status" => "200",
                "data" => $getAllDriverXp
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(array("Driver xp view all",[
                $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error getting record"
            ],500);
        }
    }

    /**
     * return all driver experience point for passed user_id .This route is for admin
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function getAllDriverXpByAdmin(Request $request){
        try{
            $this->validate($request,[
                "user_id" => "sometimes|integer|min:1",
                "country_id" => "required|integer|min:1"
            ]) ;
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            $userId = $request->get("user_id",null);
            $getAllDriverXp = $this->driverXp->getAllDriverXpForAdmin($userId,$request->country_id);
            foreach ($getAllDriverXp as $key=> $xp){
                $getAllDriverXp[$key]["created_date"] = $xp->getOriginal('created_at');
            }

            $getAllDriverXp = new Collection($getAllDriverXp->toArray());
            return \Datatables::of($getAllDriverXp)->make(true);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(array("Driver xp view all",[
                $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error getting record"
            ],500);
        }
    }


    /**
     * returns specific xp data for logged in user
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificDriverXp($id,Request $request){
        try{
            try{
                $this->validate($request,[
                    "country_id" => "required|integer|min:1"
                ]) ;
            }
            catch (\Exception $ex){
                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ],422);
            }
            if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
                return response()->json([
                    "status" => "403",
                    "message" => "Only driver has access"
                ],403);
            }
            $userId = \RoleChecker::getUser();
            $getDriverXp = $this->driverXp->viewSpecificXp($id);
            if($getDriverXp->country_id != $request->country_id){
                return response()->json([
                    "status" => "403",
                    "message" => "Country id did not  match"
                ],403);
            }
            if($getDriverXp->user_id !== $userId){
                return response()->json([
                    "status" => "403",
                    "message" => "You can view your experience point only "
                ],403);
            }
            return response()->json([
                "status" => "200",
                "data" => $getDriverXp
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Empty experience point"
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(array("Driver xp view specific",[
                $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error getting record"
            ],500);
        }
    }

    /**
     * returns specific xp data .Only user that has permission can use this route
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificDriverXpAdmin($id,Request $request){
        try{
            $getDriverXp = $this->driverXp->viewSpecificXp($id);
            $getDriverXp->created_date =  $getDriverXp->getOriginal('created_at');
            $userDatas["users"] = [$getDriverXp["user_id"]];
            $oauthUrl = Config::get('config.oauth_base_url')."/review/users/list";
            $getUserDetail =  \RemoteCall::storeWithoutOauth($oauthUrl,$userDatas);
            if ($getUserDetail['status'] != 200) {
                if($getUserDetail['status'] == 503){
                    return response()->json(
                        $getUserDetail, $getUserDetail['status']);
                }
                return response()->json(
                    $getUserDetail["message"], $getUserDetail['status']);
            }
            $customer = $getUserDetail["message"]["data"][0];
            $getDriverXp["name"] = implode(" ", array($customer['first_name'], $customer['middle_name'], $customer['last_name']));
            $getDriverXp["mobile"] = implode("", array($customer['country_code'], $customer["mobile"]));
            return response()->json([
                "status" => "200",
                "data" => $getDriverXp
            ]);
        }
        catch (ModelNotFoundException $ex){
            return response()->json([
                "status" => "404",
                "message" => "Empty experience point"
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError(array("Driver xp view specific",[
                $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error getting record"
            ],500);
        }
    }

    /**
     * creating new xp for passed user id. Only user that has permission has access to create experience point
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createDriverXpByAdmin(Request $request){
        try{
            $this->validate($request,[
                "user_id" => "required|integer|min:1",
                "order_id" => "sometimes|integer|min:1",
                "experience_point" => "required|numeric|min:0",
                "type" => "required|in:normal,deduction",
                "comment" => "sometimes|string",
                "country_id" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            DB::beginTransaction();
            //calling oauth service to check user has role driver
            $baseUrlForOauth = Config::get('config.oauth_base_url');
            $checkUserRoleIsDriver = \RemoteCall::getSpecific($baseUrlForOauth . "/user/$request->user_id/driver");
            if ($checkUserRoleIsDriver["status"] != 200) {
                return response()->json([
                    $checkUserRoleIsDriver["message"]
                ], $checkUserRoleIsDriver["status"]);
            }

            $getCurrentXp = $this->getTotalXp($request->user_id, $request->country_id)[0];
            if($request->type == "deduction" && $request->experience_point > $getCurrentXp){
                return response()->json([
                    "status" => "403",
                    "message" => "Requested experience point should not be greater than current experience point of the driver."
                ],403);
            }
            $createDriverXp = $this->driverXp->createDriverXp($request->all());
            DB::commit();
            $this->log->storeLogInfo([
                "driver xp creation by admin",[
                    "status" => "200",
                    "message" => "Driver experience point created successfully",
                    "data" => $createDriverXp->toArray()
                ]
            ]);
            return response()->json([
                "status" => "200",
                "message" => "Driver experience point created successfully"
            ]);
        }
        catch (\Exception $ex){

            $this->log->storeLogInfo([
                "driver xp creation by admin",[
                    $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error creating driver experience point"
            ]);
        }
    }

    /**
     * creating new xp for passed user id. Only user that has permission has access to create experience point
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateDriverXpByAdmin(Request $request, $id){
        try{
            $this->validate($request,[
                "user_id" => "required|integer|min:1",
                "order_id" => "sometimes|integer|min:1",
                "experience_point" => "required|numeric|min:0",
                "type" => "required|in:normal,deduction",
                "comment" => "sometimes|string",
                "country_id" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            DB::beginTransaction();
            $getCurrentXp = $this->getTotalXp($request->user_id,$request->country_id)[0];
            if($request->type == "deduction" && $request->experience_point > $getCurrentXp){
                return response()->json([
                    "status" => "403",
                    "message" => "Requested experience point should not be greater than current experience point of the driver."
                ],403);
            }
            $createDriverXp = $this->driverXp->updateDriverXp($id,$request->except("user_id","order_id"));
            DB::commit();
            $this->log->storeLogInfo([
                "driver xp creation by admin",[
                    "status" => "200",
                    "message" => "Driver experience point updated successfully",
                ]
            ]);
            return response()->json([
                "status" => "200",
                "message" => "Driver experience point updated successfully"
            ]);
        }
        catch (\Exception $ex){

            $this->log->storeLogInfo([
                "driver xp creation by admin",[
                    $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error creating driver experience point"
            ]);
        }
    }

    /**
     * return current xp
     * @param $user_id
     * @return mixed
     */
    private function getTotalXp($user_id,$countryId){
        $normalXp = $this->driverXp->getAllDriverXpNormal($user_id,$countryId);
        $deductedXp = $this->driverXp->getAllDriverDeducted($user_id,$countryId);

        return [
            $normalXp - $deductedXp,
            "total_xp" => $normalXp,
            "deducted_xp" => $deductedXp
        ];

    }

}