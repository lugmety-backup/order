<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/22/2017
 * Time: 2:32 PM
 */

namespace App\Http\Controllers;


use App\Repo\DriverOrderInterface;
use Illuminate\Http\Request;
use LogStoreHelper;
class DriverOrderController extends Controller
{
    protected $driverOrder;
    protected $logStoreHelper;

    /**
     * DriverOrderController constructor.
     * @param $driverOrder
     * @param $logStoreHelper
     */
    public function __construct(DriverOrderInterface $driverOrder, LogStoreHelper $logStoreHelper)
    {
        $this->driverOrder = $driverOrder;
        $this->logStoreHelper = $logStoreHelper;
    }

    public function createDriverOrder(Request $request){
        try{
            $message =[
                "status_validation" => "Input status must have value assigned"
            ];
            $this->validate($request,[
            "status" => "required|status_validation",
            "order_id" =>"required|integer|min:0"
            ],$message);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
    }



}