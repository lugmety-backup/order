<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/15/2017
 * Time: 2:12 PM
 */

namespace App\Http\Controllers;

use Cocur\Slugify\Slugify;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repo\PaymentStatusInterface;
use App\Repo\PaymentStatusTranslationInterface;
use LogStoreHelper;
use Illuminate\Support\Facades\DB;
use Validator;
use RoleChecker;
use Yajra\Datatables\Datatables;

class PaymentStatusController extends Controller
{
    /**
     * @var PaymentStatusInterface
     */
    protected $paymentStatus;
    /**
     * @var PaymentStatusTranslationInterface
     */
    protected $paymentStatusTranslation;

    /**
     * @var Slugify
     */
    protected $slugify;

    /**
     * @var LogStoreHelper
     */
    protected $logStoreHelper;
    /**
     * PaymentStatusController constructor.
     * @param $paymentStatus
     * @param $paymentStatusTranslation
     */
    public function __construct(LogStoreHelper $logStoreHelper,Slugify $slugify,PaymentStatusInterface $paymentStatus, PaymentStatusTranslationInterface $paymentStatusTranslation)
    {
        $this->paymentStatus = $paymentStatus;
        $this->paymentStatusTranslation = $paymentStatusTranslation;
        $this->slugify = $slugify;
        $this->logStoreHelper = $logStoreHelper;
    }

    public function viewAllPayment(){
        try {
            if(RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")|| RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")){
                $paymentStatuses = $this->paymentStatus->getAllEnabledPaymentStatus();
            }
            else {
                $paymentStatuses = $this->paymentStatus->getAllPaymentStatus();
            }
            if (!$paymentStatuses->first()) {
                throw  new \Exception();
            }
        }
        catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Empty Payment Status "
            ],404);
        }
        foreach ($paymentStatuses as $key => $paymentStatus) {
            /*
         * Get restaurant Translation based on @param lang
         * if empty record is obtained default en lang is returned
         * */
            $paymentStatusTranslation = $paymentStatus->paymentStatusTranslations()->where('lang_code', "en")->get();

            try {
                if ($paymentStatusTranslation->count() == 0) {
                    throw new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }
            /*
             * attach name and lang_code in $restaurant variable
             * */

            $paymentStatus['name'] = $paymentStatusTranslation[0]['name'];
            $paymentStatus['lang_code'] = $paymentStatusTranslation[0]['lang_code'];
        }
        return Datatables::of($paymentStatuses)->make(true);
//        return response()->json([
//            "status" => "200",
//            "data" => $paymentStatuses
//        ]);

    }

    /**
     * Returns specific order and order translation
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewSpecificPayment($id){
        try{
            /*
             * checking order status exists or not
           * */
            if(RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")|| RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")){
                $specificPaymentStatus  = $this->paymentStatus->getSpecificPaymentStatusEnabled($id);
            }
            else {
                $specificPaymentStatus = $this->paymentStatus->getSpecificPaymentStatus($id);
            }

            $specificPaymentStatus["translation"] = $specificPaymentStatus->paymentStatusTranslations()->get();

            $this->logStoreHelper->storeLogError(array("Payment Status", [
                "status" => "200",
                "data" => $specificPaymentStatus
            ]));

            return response()->json([
                "status" => "200",
                "data" => $specificPaymentStatus
            ],200);
        }
        catch (ModelNotFoundException $exception){
            $this->logStoreHelper->storeLogInfo(array("Payment Status", [
                "status" => "404",
                "message" => "Request Payment Status does not exist"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Request Payment Status does not exist"
            ],404);
        }
    }

    /**
     * Create order status.only permitted user can create order status.if input request is_default is 1 then will set other whose status is 1 to 0
     * and will set slug with name unpaid to 1 if there is no is_default is 1 and slug name unpaid is present
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createStatus(Request $request){
        try{

            $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
            /**
             * custom validation of slug
             */
            $message =[
                "slug.required" => "The slug may not only contain numbers,special characters and should not be empty."
            ];

            $this->validate($request,[
                "status" => "integer|min:0|max:1",
                "is_default" => "integer|min:0|max:1",
                "slug" => "required|unique:payment_status,slug",
                "translation" => "required|array"
            ],$message);
        }
        catch (\Exception $exception){
            $this->logStoreHelper->storeLogError(array("Payment Status ",[
                "status"=>'422',
                "message"=>$exception->response->original
            ]));

            return response()->json([
                "status" => "422",
                "message" => $exception->response->original
            ],422);
        }

        try{
            DB::beginTransaction();
            $paymentStatus = $request->all();
            $paymentStatus["is_default"] =$request->input('is_default',0);
            $paymentStatus["status"] = $request->input('status',1);
            if(isset($paymentStatus["is_default"]) && $paymentStatus["is_default"] == 1){
                /**
                 * if input request is_default is 1 then this function will set other whose status is 1 to 0
                 */
                $this->paymentStatus->setOtherPaymentStatusToNonDefault();
            }
            else{
                /**
                 *  this function will set slug with name pending to 1 if there is no is_default is 1 and slug name unpaid is present
                 */
                $this->paymentStatus->setUnpaidSlugDefaultIfOtherSlugIsDefault();
            }

            $paymentStatusCreated =$this->paymentStatus->createPaymentStatus($paymentStatus);
            $this->logStoreHelper->storeLogInfo(array("Payment Status", [
                "status" => "200",
                "message" => "Restaurant Created Successfully",
                "data" => $paymentStatusCreated
            ]));
            foreach ($paymentStatus['translation'] as $key => $translation) {
                $translation['payment_status_id'] = $paymentStatusCreated->id;
                $translation['lang_code'] = $key;
                $paymentStatusId = $paymentStatusCreated->id;
                $rules = [
                    "lang_code" => 'required|alpha',
                    "name" => "required"
                ];
                $translation['lang_code'] = $this->slugify->slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
                if (empty($translation['lang_code'])) {
                    return response()->json([
                        "status" => "422",
                        "message" => ["translation" =>["lang_code" => "The lang_code may not only contain numbers,special characters."]]
                    ], 422);
                }
                $validator = Validator::make($translation, $rules);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError(array('Payment Status Translation', [
                        'status' => '422',
                        "message" => ["translation"=>[$key => $error]]
                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => ["translation"=>[$key => $error]]
                    ], 422);
                }
                /*
                 * checking if the same payment_status_id id have existing lang_code or not
                 * */
                try {
                    $checkExistingTranslation = $this->paymentStatusTranslation
                        ->getSpecificPaymentStatusTranslationByCodeAndId($translation["lang_code"], $paymentStatusId);
                    switch ($checkExistingTranslation->first()) {
                        case !null: {
                            throw new \Exception();
                            break;
                        }
                        default: {
                            $createTranslation = $this->paymentStatusTranslation->createPaymentStatusTranslation($translation);
                            $this->logStoreHelper->storeLogInfo(array("Payment Status Translation", [
                                "status" => "200",
                                "message" => "Payment Status Translation Created Successfully",
                                "data" => $createTranslation
                            ]));
                        }
                    }
                } catch (\Exception $ex) {
                    $this->logStoreHelper->storeLogError(array("Payment Status", [
                        "status" => "409",
                        "message" => "Language translation for given Payment status  already exist"
                    ]));
                    return response()->json([
                        "status" => "409",
                        "message" =>"Language translation for given Payment status  already exist"
                    ], 409);
                }
            }
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Payment Status created successfully"
            ]);

        }
        catch (\Exception $exception){
            return response()->json([
                "status" => "500",
                "message" => "Error Creating Payment status"
            ],500);
        }
    }

    /**
     * update order status.only only permitted user can create order status.if input request is_default is 1 then will set other whose status is 1 to 0
     * and will set slug with name pending to 1 if there is no is_default is 1 and slug name pending is present
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus($id,Request $request){
        try{
            if(!(RoleChecker::hasRole("admin") || RoleChecker::hasRole("super-admin"))){
                return response()->json([
                    "status" => "403",
                    "message" => "Only admin or super admin can view order comment"
                ],403);
            }

            $request["slug"] = $this->slugify->slugify($request['slug'],['regexp' => '/([^A-Za-z]|-)+/']);
            /**
             * custom validation of slug
             */
            $message =[
                "slug.required" => "The slug may not only contain numbers,special characters and should not be empty."
            ];

            $this->validate($request,[
                "status" => "integer|min:0|max:1",
                "is_default" => "integer|min:0|max:1",
                "slug" => "required|unique:payment_status,slug,".$id,
                "translation" => "required|array"
            ],$message);
        }
        catch (\Exception $exception){
            $this->logStoreHelper->storeLogError(array("Payment Status ",[
                "status"=>'422',
                "message"=>$exception->response->original
            ]));

            return response()->json([
                "status" => "422",
                "message" => $exception->response->original
            ],422);
        }

        try{
            DB::beginTransaction();
            /**
             * checking order status exist for update
             */
            try{
                $specificPaymentStatus  = $this->paymentStatus->getSpecificPaymentStatus($id);
                /**
                 * deletes the all order status translation whose payment_status_id is @param $id
                 */
                $this->paymentStatusTranslation->deletePaymentStatusTranslationByPaymentStatusId($specificPaymentStatus->id);
            }
            catch (ModelNotFoundException $ex){
                return response()->json([
                    "status" => "404",
                    "message" => "Payment Status could not be found"
                ],404);
            }
            $paymentStatus = $request->all();
            if(isset($paymentStatus["is_default"]) && $paymentStatus["is_default"] == 1){
                /**
                 * if input request is_default is 1 then this function will set other whose status is 1 to 0
                 */
                $this->paymentStatus->setOtherPaymentStatusToNonDefault();
            }
            else{
                /**
                 *  this function will set slug with name pending to 1 if there is no is_default is 1 and slug name pending is present
                 */
                $this->paymentStatus->setUnpaidSlugDefaultIfOtherSlugIsDefault();
            }
            /**
             * checks if the order status slug belongs to list of slugs list that are fixed
             * if belongs it  will not update requested slug
             */
            if($this->checkPaymentStatusSlug($specificPaymentStatus->slug)) {
                unset($paymentStatus["slug"]);
            }
            $paymentStatusUpdated =$this->paymentStatus->updatePaymentStatus($id,$paymentStatus);

            $this->logStoreHelper->storeLogInfo(array("Payment Status", [
                "status" => "200",
                "message" => "Restaurant update Successfully",
                "data" => [$id,$paymentStatusUpdated]
            ]));
            foreach ($paymentStatus['translation'] as $key => $translation) {
                $translation['payment_status_id'] = $id;
                $translation['lang_code'] = $key;
                $paymentStatusId = $id;
                $rules = [
                    "lang_code" => 'required|alpha',
                    "name" => "required"
                ];
                $translation['lang_code'] = $this->slugify->slugify($translation['lang_code'], ['regexp' => '/([^A-Za-z]|-)+/']);
                if (empty($translation['lang_code'])) {
                    return response()->json([
                        "status" => "422",
                        "message" => ["translation" =>["lang_code" => "The lang_code may not only contain numbers,special characters."]]
                    ], 422);
                }
                $validator = Validator::make($translation, $rules);
                if ($validator->fails()) {
                    $error = $validator->errors();
                    $this->logStoreHelper->storeLogError(array('Payment Status Translation', [
                        'status' => '422',
                        "message" => ["translation"=>[$key => $error]]
                    ]));
                    return response()->json([
                        'status' => '422',
                        "message" => ["translation"=>[$key => $error]]
                    ], 422);
                }
                /*
                 * checking if the same payment_status_id id have existing lang_code or not
                 * */
                try {
                    $checkExistingTranslation = $this->paymentStatusTranslation
                        ->getSpecificPaymentStatusTranslationByCodeAndId($translation["lang_code"], $paymentStatusId);
                    switch ($checkExistingTranslation->first()) {
                        case !null: {
                            throw new \Exception();
                            break;
                        }
                        default: {
                            $createTranslation = $this->paymentStatusTranslation->createPaymentStatusTranslation($translation);
                            $this->logStoreHelper->storeLogInfo(array("Payment Status Translation", [
                                "status" => "200",
                                "message" => "Payment Status Translation Created Successfully",
                                "data" => $createTranslation
                            ]));
                        }
                    }
                } catch (\Exception $ex) {
                    $this->logStoreHelper->storeLogError(array("Payment Status", [
                        "status" => "409",
                        "message" => "Language translation for given Payment status  already exist"
                    ]));
                    return response()->json([
                        "status" => "409",
                        "message" => "Language translation for given Payment Status  already exist"
                    ], 409);
                }
            }
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Payment Status Updated successfully"
            ]);

        }
        catch (\Exception $exception){
            return response()->json([
                "status" => "500",
                "message" => "Error Updating Payment status"
            ],500);
        }
    }


    /**
     * Deletes specific order status and its child i.e order status translation
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteStatus($id){
        try{
            if(!(RoleChecker::hasRole("admin") || RoleChecker::hasRole("super-admin"))){
                return response()->json([
                    "status" => "403",
                    "message" => "Only admin or super admin can view order comment"
                ],403);
            }
            DB::beginTransaction();
            /*
             * checking order status exists or not
             * */
            $specificPaymentStatus  = $this->paymentStatus->getSpecificPaymentStatus($id);
            try{
                /**
                 * checks if the slug belongs to list of slugs list that are fixed
                 * if belongs it wont allow to delete
                 */
                if($this->checkPaymentStatusSlug($specificPaymentStatus->slug)){
                    return response()->json([
                        "status" => "403",
                        "message" => "Requested payment status slug is not allowed to delete"
                    ],403);
                }
                /**
                 * this will deletes order status and its child table order status translation
                 */
                $this->paymentStatus->deletePaymentStatus($specificPaymentStatus);
                DB::Commit();
                $this->logStoreHelper->storeLogInfo(array("Payment Status", [
                    "status" => "200",
                    "message" => "Payment Status Deleted Successfully",
                    "data" =>$specificPaymentStatus
                ]));
                return response()->json([
                    "status" => "200",
                    "message" => "Payment Status Deleted Successfully"
                ]);
            }
            catch (\Exception $ex){
                $this->logStoreHelper->storeLogError(array("Payment Status", [
                    "status" => "500",
                    "message" => "Error Deleting Payment Status"
                ]));
                return response()->json([
                    "status" => "500",
                    "message" => "Error Deleting Payment Status"
                ],500);
            }
        }
        catch (ModelNotFoundException $exception){
            $this->logStoreHelper->storeLogError(array("Payment Status", [
                "status" => "404",
                "message" => "Request Payment does not exist"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Request Payment does not exist"
            ],404);
        }
    }

    /**
     * checks if the slug matches with list of slugs
     * @param $slug
     * @return bool
     */
    private function checkPaymentStatusSlug($slug){
        $orderStatusToCheck =["pending","refunded","on-hold","cancelled","failed","processing","completed"];
        if(in_array($slug, $orderStatusToCheck)){
            return true;
        }
        return false;
    }
}