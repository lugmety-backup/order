<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 12/18/17
 * Time: 12:51 PM
 */

namespace App\Http\Controllers;


use App\Models\Orders;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repo\OrderInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\In;
use RemoteCall;
use RoleChecker;
use Excel;
use App\Models\Orders as OrderModel;

class ReportController extends Controller
{
    private $order;
    private $order_model;

    public function __construct(OrderInterface $order, OrderModel $order_model)
    {
        $this->order = $order;
        $this->order_model = $order_model;

    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * Orders count for devices used per country
     */
    public function orderingDeviceReport($id, Request $request)
    {
        try {
            $this->validate($request, [
                "start_date" => "required|date_format:Y-m-d",
                "end_date" => "required|date_format:Y-m-d|after:start_date",

            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $startDate = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
            $endDate = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000

            $device = DB::table('orders')
                ->select(DB::raw('count(*) as count,device_type'))
                ->where('country_id', $id)
                ->where('device_type', "<>", "")
                ->whereBetween('created_at', [$startDate, $endDate])
                ->groupBy('device_type')
                ->get();


            return response()->json([
                "status" => "200",
                "data" => $device
            ], 200);

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);

        }


    }

    public function highestSpendingCustomers($id, Request $request)
    {
        try {
            $this->validate($request, [
                "start_date" => "required|date_format:Y-m-d",
                "end_date" => "required|date_format:Y-m-d|after:start_date",


            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }

        $startDate = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
        $endDate = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000

        $users = $web = DB::table('orders')
            ->select('customer_id', 'customer_name', 'currency', DB::raw('SUM(amount) as total,COUNT(DISTINCT id) AS order_count, SUM(tax) as vat_amount, COUNT(DISTINCT restaurant_id) AS res_count'))
            ->whereBetween('created_at', [$startDate, $endDate])
            ->where([
                ['country_id', $id],
                ['order_status', "accepted"],
                ["payment_status", "completed"],
                ['is_seen', 1],
            ])
            ->groupBy('customer_id')
            ->orderBy('total', 'desc')
            ->limit(10)->get();
        if (count($users) > 0) {
            return response()->json([
                "status" => 200,
                "data" => $users
            ], 200);
        } else {
            return response()->json([
                "status" => 204,
                "data" => $users
            ], 204);
        }


    }

    public function avgDeliveryTime($id, Request $request)
    {

        try {
            $this->validate($request, [
                "is_all" => "required|in:yes,no",
                "restaurant_id" => "required_if:is_all,no|array",
                "start_date" => "required|date_format:Y-m-d",
                "end_date" => "required|date_format:Y-m-d|after:start_date",
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            if ($request->is_all == "yes") {

                $startDate = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
                $endDate = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000

                $restaurants = DB::table('orders')
                    ->whereBetween('created_at', [$startDate, $endDate])
                    ->where([
                        ['country_id', $id],
                        ["delivery_type", "<>", "pickup"],
                        ["delivery_time", "<>", ""],
                        ['order_status', "accepted"],
                        ["delivery_status", "delivered"],
                        ["payment_status", "completed"],
                        ['is_seen', 1],
                        ["api_delivery_time", "<>", ""],
                    ])
                    ->select('restaurant_id', DB::raw('avg(TIMESTAMPDIFF(MINUTE,CONVERT_TZ (created_at, "+00:00",time_zone_utc),delivery_time)) as deliveryTime'))
                    ->groupBy("restaurant_id")
                    ->orderBy("deliveryTime", 'desc')->get();


            } else {

                $startDate = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
                $endDate = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000

                $restaurants = DB::table('orders')
                    ->whereBetween('created_at', [$startDate, $endDate])
                    ->where([
                        ["delivery_type", "<>", "pickup"],
                        ["delivery_time", "<>", ""],
                        ["country_id", $id],
                        ['order_status', "accepted"],
                        ["delivery_status", "delivered"],
                        ["payment_status", "completed"],
                        ['is_seen', 1],
                        ["api_delivery_time", "<>", ""]
                    ])->whereIn('restaurant_id', $request['restaurant_id'])
                    ->select('restaurant_id', DB::raw('avg(TIMESTAMPDIFF(MINUTE,CONVERT_TZ (created_at, "+00:00",time_zone_utc),delivery_time)) as deliveryTime'))
                    ->groupBy("restaurant_id")
                    ->orderBy("deliveryTime", 'desc')->get();


            }

            if (count($restaurants) > 0) {
                return response()->json([
                    "status" => "200",
                    "data" => $restaurants
                ], 200);
            } else {
                return response()->json([
                    "status" => "204",
                    "data" => $restaurants
                ], 204);
            }


        } catch (\Exception $ex) {

            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);

        }
    }

//    public function transactionReport($id, Request $request)
//    {
//        try {
//            $this->validate($request, [
//                "is_all" => "required|in:yes,no",
//                "restaurant_id" => "required_if:is_all,no|array",
//                "start_date" => "sometimes|date_format:Y-m-d",
//                "end_date" => "required_with:start_date|date_format:Y-m-d|after:start_date",
//            ]);
//        } catch (\Exception $ex) {
//            return response()->json([
//                "status" => "422",
//                "message" => $ex->response->original
//            ], 422);
//        }
//        try {
//            if ($request->is_all == "yes") {
//                if ($request->has('start_date') && $request->has('end_date')) {
//                    $startDate = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
//                    $endDate = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000
//
//                    $restaurants = DB::table('orders')
//                        ->whereBetween('created_at', [$startDate, $endDate])
//                        ->where([
//                            ['country_id', $id],
//                            ["delivery_type", "<>", "pickup"],
//                            ["delivery_time", "<>", ""],
//                            ['order_status', "accepted"],
//                            ["delivery_status", "delivered"],
//                            ["payment_status", "completed"],
//                            ['is_seen', 1],
//                            ["api_delivery_time", "<>", ""]
//                        ])
//                        ->select('restaurant_id', DB::raw('sum(amount) as total_amount,count(id) as total_orders'))
//                        ->groupBy('restaurant_id')
//                        ->orderBy("deliveryTime", 'desc')->get();
//                } else {
//
//
//                    $restaurants = DB::table('orders')
//                        ->where([
//                            ['country_id', $id],
//                            ["delivery_type", "<>", "pickup"],
//                            ["delivery_time", "<>", ""],
//                            ['order_status', "accepted"],
//                            ["delivery_status", "delivered"],
//                            ["payment_status", "completed"],
//                            ['is_seen', 1],
//                            ["api_delivery_time", "<>", ""]
//                        ])
//                        ->select('restaurant_id', DB::raw('sum(amount) as total_amount,count(id) as total_orders'))
//                        ->groupBy('restaurant_id')
//                        ->orderBy("deliveryTime", 'desc')->get();
//                }
//
//            } else {
//                if ($request->has('start_date') && $request->has('end_date')) {
//                    $startDate = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
//                    $endDate = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000
//
//                    $restaurants = DB::table('orders')
//                        ->whereBetween('created_at', [$startDate, $endDate])
//                        ->where([
//                            ['country_id', $id],
//                            ["delivery_type", "<>", "pickup"],
//                            ["delivery_time", "<>", ""],
//                            ['order_status', "accepted"],
//                            ["delivery_status", "delivered"],
//                            ["payment_status", "completed"],
//                            ['is_seen', 1],
//                            ["api_delivery_time", "<>", ""]
//                        ])
//                        ->select('restaurant_id', DB::raw('sum(amount) as total_amount,count(id) as total_orders'))
//                        ->groupBy('restaurant_id')
//                        ->orderBy("deliveryTime", 'desc')->get();
//                } else {
//                    $restaurants = DB::table('orders')
//                        ->whereIn('restaurant_id', $request['restaurant_id'])
//                        ->where([
//                            ['country_id', $id],
//                            ["delivery_type", "<>", "pickup"],
//                            ["delivery_time", "<>", ""],
//                            ['order_status', "accepted"],
//                            ["delivery_status", "delivered"],
//                            ["payment_status", "completed"],
//                            ['is_seen', 1],
//                            ["api_delivery_time", "<>", ""]
//                        ])
//                        ->select('restaurant_id', DB::raw('sum(amount) as total_amount,count(id) as total_orders'))
//                        ->groupBy('restaurant_id')
//                        ->orderBy("deliveryTime", 'desc')->get();
//                }
//
//
//            }
//            return response()->json([
//                "status" => "200",
//                "data" => $restaurants
//            ], 200);
//        } catch (\Exception $ex) {
//            return response()->json([
//                "status" => "400",
//                "data" => $ex->getMessage()
//            ], 400);
//        }
//
//    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     *
     */
    public function parentRestaurantReportForAdmin($id, Request $request)
    {

        try {
            $this->validate($request, [
                "main_restaurant" => "required|boolean",
                "restaurant_id" => "required|array",
                "start_date" => "sometimes|date_format:Y-m-d",
                "end_date" => "required_with:start_date|date_format:Y-m-d|after:start_date",

            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $check = [];
            $startDate = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
            $endDate = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000

            if ($request->main_restaurant == true) {
                $fullUrl = Config::get('config.restaurant_base_url') . "/get-restaurant-report";
                $restaurantData = RemoteCall::store($fullUrl, $request->all());
                if ($restaurantData["status"] != "200") {
                    return response()->json([
                        "status" => $restaurantData["status"],
                        "message" => $restaurantData["message"]["message"],
                    ], $restaurantData["status"]);
                }

                $restaurantList = $restaurantData["message"]['data'];


                if (!empty($restaurantList)) {


                    $check = $this->getMainRestaurantInfo($id, $restaurantList, $startDate, $endDate);


                }

                return response()->json([
                    "status" => "200",
                    "data" => $check
                ]);
            } else {
                $orders = DB::table('orders')
                    ->whereIn('restaurant_id', $request->restaurant_id)
                    ->whereBetween('created_at', [$startDate, $endDate])
                    ->where([

                        ['order_status', "accepted"],
                        ["payment_status", "completed"],
                        ['is_seen', 1],

                    ])
                    ->select('restaurant_id', "currency", "restaurant", DB::raw('sum(amount) as total_amount,count(id) as total_orders'))
                    ->groupBy('restaurant_id')
                    ->get();
                if (count($orders) == 0) {
                    $order1 = [];
                } else {
                    $order = json_decode((string)$orders, true);
                    $order1 = $this->getRestaurantBranchOrderInfo($order);
                }
                return response()->json([
                    "status" => "200",
                    "data" => $order1
                ], 200);

            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => $ex->getMessage()
            ], 404);
        }


    }

    public function parentRestaurantReportForRestaurantAdmin($id, Request $request)
    {

        try {
            $this->validate($request, [
                "main_restaurant" => "required|boolean",
                "restaurant_id" => "required_if:main_restaurant,false|array",
                "start_date" => "sometimes|date_format:Y-m-d",
                "end_date" => "required_with:start_date|date_format:Y-m-d|after:start_date",
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            if (RoleChecker::hasRole("restaurant_admin" || (RoleChecker::get('parentRole') == "restaurant-admin"))) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();


                if (!is_null($restaurantInfo) && ($restaurantInfo["is_restaurant_admin"] == true) && !is_null($restaurantInfo["branch_id"])) {

                    if ($request->main_restaurant == true && $restaurantInfo["restaurant_id"] == $id) {
                        $rest["restaurant_id"] = [$id];
                        $fullUrl = Config::get('config.restaurant_base_url') . "/get-restaurant-report";
                        $restaurantData = RemoteCall::store($fullUrl, $rest);
                        if ($restaurantData["status"] != "200") {
                            return response()->json([
                                "status" => $restaurantData["status"],
                                "message" => $restaurantData["message"]["message"],
                            ], $restaurantData["status"]);
                        }
                        $check = [];
                        $restaurantList = $restaurantData["message"]['data'];
                        if (!empty($restaurantList)) {

                            $startDate = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
                            $endDate = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000

                            $check = $this->getMainRestaurantInfo(null, $restaurantList, $startDate, $endDate);

                        }
                        return response()->json([
                            "status" => "200",
                            "data" => $check
                        ], 200);
                    } elseif ($request->main_restaurant == false && count(array_diff($request->restaurant_id, $restaurantInfo['branch_id'])) == 0) {
                        $orders = DB::table('orders')
                            ->whereIn('restaurant_id', $request->restaurant_id)
                            ->where([

                                ['order_status', "accepted"],
                                ["payment_status", "completed"],
                                ['is_seen', 1],
                            ])
                            ->select('restaurant_id', "currency", "restaurant", DB::raw('sum(amount) as total_amount,count(id) as total_orders'))
                            ->groupBy('restaurant_id')
                            ->get();
                        if (count($orders) == 0) {
                            $order1 = [];
                        } else {
                            $order = json_decode((string)$orders, true);
                            $order1 = $this->getRestaurantBranchOrderInfo($order);
                        }
                        return response()->json([
                            "status" => "200",
                            "data" => $order1
                        ]);

                    } else {
                        return response()->json([
                            "status" => "403",
                            "message" => "Forbidden"
                        ], 403);
                    }

                } else {
                    return response()->json([
                        "status" => "403",
                        "message" => "Forbidden"
                    ], 403);
                }
            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "Forbidden"
                ], 403);
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);
        }


    }


    public function branchRestaurantReportForBranchAdmin($id, Request $request)
    {
        try {
            $this->validate($request, [
                "start_date" => "required|date_format:Y-m-d",
                "end_date" => "required|date_format:Y-m-d|after:start_date",

            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {


            if (RoleChecker::hasRole("branch-admin") || (RoleChecker::get('parentRole') == "branch-admin")) {
                $restaurantInfo = RoleChecker::getRestaurantInfo();

                if (!is_null($restaurantInfo) && !is_null($restaurantInfo["branch_id"]) && ($restaurantInfo["branch_id"] == $id) && ($restaurantInfo["is_restaurant_admin"] == false)) {

                    $startDate = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
                    $endDate = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000

                    $order = DB::table("orders")
                        ->select("restaurant_id", "restaurant", "currency", DB::raw('sum(amount) as total_amount, count(id) as orders_count
            '))
                        ->whereBetween('created_at', [$startDate, $endDate])
                        ->where([
                            ['order_status', "accepted"],
                            ["payment_status", "completed"],
                            ['is_seen', 1],
                            ["restaurant_id", $id]
                        ])
                        ->groupBy('restaurant_id')
                        ->get();


                    if (count($order) == 0) {
                        $order = [];
                    } else {
                        $order = json_decode((string)$order, true);

                        $order = $this->getRestaurantBranchOrderInfo($order);
                    }
                    return response()->json([
                        "status" => "200",
                        "data" => $order
                    ]);

                } else {
                    return response()->json([
                        "status" => "403",
                        "message" => "Forbidden"
                    ], 403);
                }
            } else {
                return response()->json([
                    "status" => "403",
                    "message" => "Forbidden"
                ], 403);
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);
        }
    }


    private function getMainRestaurantInfo($countryId, $restaurantList, $startDate, $endDate)
    {
        if (!empty($countryId)) {
            foreach ($restaurantList as $key1 => $list) {
                $restaurants = DB::table('orders')
                    ->whereIn('restaurant_id', $list['branch_list'])
                    ->where([
                        ['country_id', $countryId],
                        ['order_status', "accepted"],
                        ["payment_status", "completed"],
                        ['is_seen', 1],

                    ])
                    ->select('restaurant_id', "currency", DB::raw('sum(amount) as total_amount,count(id) as total_orders'))
                    ->groupBy('restaurant_id')
                    ->get();
                $restaurants = json_decode((string)$restaurants, true);
                if (!empty($restaurants)) {
                    $sum = 0;
                    foreach ($restaurants as $key => $restaurant) {
                        //$restaurants[$key]['total_amount'] = $restaurant['total_amount'];
                        $restaurants[$key]['name'] = $list['branch_name'][$restaurant['restaurant_id']];
                        $restaurants[$key]['parent_name'] = $list['parent_name'];
                        $restaurants[$key]['parent_id'] = $key1;
                        $restaurants[$key]['total_amount'] = $restaurant['total_amount'];


                    }
                    $check[] = $restaurants;
                }
            }
        } else {
            foreach ($restaurantList as $key1 => $list) {
                $restaurants = DB::table('orders')
                    ->whereIn('restaurant_id', $list['branch_list'])
                    ->whereBetween('created_at', [$startDate, $endDate])
                    ->where([
                        ['order_status', "accepted"],
                        ["payment_status", "completed"],
                        ['is_seen', 1],

                    ])
                    ->select('restaurant_id', "currency", DB::raw('sum(amount) as total_amount,count(id) as total_orders'))
                    ->groupBy('restaurant_id')
                    ->get();
                $restaurants = json_decode((string)$restaurants, true);
                if (!empty($restaurants)) {
                    $sum = 0;
                    foreach ($restaurants as $key => $restaurant) {
                        $restaurants[$key]['name'] = $list['branch_name'][$restaurant['restaurant_id']];
                        $restaurants[$key]['parent_name'] = $list['parent_name'];
                        $restaurants[$key]['parent_id'] = $key1;
                        $restaurants[$key]['total_amount'] = $restaurant['total_amount'];


                    }
                    $check[] = $restaurants;
                }
            }
        }

        return $check;
    }

    private function getRestaurantBranchOrderInfo($orders)
    {
        foreach ($orders as $key => $order) {
            $restaurant = unserialize($orders[$key]["restaurant"]);
            $orders[$key]["total_amount"] = $order["total_amount"];
            unset($orders[$key]["restaurant"]);
            $orders[$key]["restaurant_name"] = $restaurant["translation"]["en"];

        }
        return $orders;
    }

    public function driverReport($id, Request $request)
    {
        try {
            $this->validate($request, [
                "start_date" => "required|date_format:Y-m-d",
                "end_date" => "required|date_format:Y-m-d|after:start_date",

            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $startDate = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
            $endDate = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000

            $mostDeliveriesByDriver = DB::table('driver_order')
                ->select(DB::raw('count(*) as count,user_id'))
                ->where('status', "=", "delivered")
                ->where('country_id', $id)
                ->whereBetween('created_at', [$startDate, $endDate])
                ->groupBy('user_id')
                ->orderBy('count', "desc")
                ->limit(10)
                ->get();

            $moneyEarned = DB::table('driver_earning')
                ->select(DB::raw('sum(amount) as total_amount,user_id'))
                ->where('type', "=", "normal")
                ->where('country_id', $id)
                ->whereBetween('created_at', [$startDate, $endDate])
                ->groupBy('user_id')
                ->orderBy('total_amount', "desc")
                ->limit(10)
                ->get();

            $avgDelivery = DB::table('driver_order')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->where('status', "=", "delivered")
                ->where('country_id', $id)
                ->where('driver_picked_time', '<>', "")
                ->select(DB::raw('avg(TIMESTAMPDIFF(MINUTE,driver_picked_time,updated_at)) as delivery_time, COUNT(*) as total_order_delivered, COUNT(DISTINCT user_id) AS driver_count'))
                ->first();
            $avgDelivery = json_decode(json_encode($avgDelivery), TRUE);

            if (count($mostDeliveriesByDriver) <= 0 && count($moneyEarned) <= 0 && $avgDelivery['total_order_delivered'] <= 0) {
                return response()->json([
                    "status" => "204",
                    "data" => [
                    ]
                ], 204);
            }
            $avgDelivery['delivery_time'] = is_null($avgDelivery['delivery_time']) ? (float)0 : (float)$avgDelivery['delivery_time'];

            $userId1 = $mostDeliveriesByDriver->count() > 0 ? $mostDeliveriesByDriver->pluck("user_id")->toArray() : [];
            $userId2 = count($moneyEarned) > 0 ? $moneyEarned->pluck("user_id")->toArray() : [];

            $userId = array_unique(array_merge($userId1, $userId2));


            $userDatas["users"] = $userId;

            $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list";
            $getUserDetailsForReview = \RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);
            if ($getUserDetailsForReview["status"] != "200") {
                if ($getUserDetailsForReview['status'] == 503) {
                    return response()->json(
                        $getUserDetailsForReview, $getUserDetailsForReview['status']);
                }
                return response()->json(
                    $getUserDetailsForReview["message"], $getUserDetailsForReview['status']);
            }
            $userDatasFromOauth = $getUserDetailsForReview["message"]["data"];
            if ($mostDeliveriesByDriver->count() > 0) {
                foreach ($mostDeliveriesByDriver as $key => $delivery) {
                    foreach ($userDatasFromOauth as $driver) {
                        if ($driver['id'] == $delivery->user_id) {
                            $delivery->driver_name = preg_replace('/\s+/', ' ', trim(implode(" ", array($driver['first_name'], $driver['middle_name'], $driver['last_name']))));

                            break;
                        }
                    }
                    if (!isset($delivery->driver_name)) {
                        unset($mostDeliveriesByDriver[$key]);
                    }

                }
                $mostDeliveriesByDriver = collect($mostDeliveriesByDriver)->take(5)->all();

            } else {
                $mostDeliveriesByDriver = [];
            }

            if (count($moneyEarned) > 0) {
                foreach ($moneyEarned as $key => $money) {
                    foreach ($userDatasFromOauth as $driver) {
                        if ($driver['id'] == $money->user_id) {
                            $money->driver_name = preg_replace('/\s+/', ' ', trim(implode(" ", array($driver['first_name'], $driver['middle_name'], $driver['last_name']))));

                            break;
                        }
                    }
                    if (!isset($money->driver_name)) {
                        unset($moneyEarned[$key]);
                    }

                }
                $moneyEarned = collect($moneyEarned)->take(5)->all();

            } else {
                $moneyEarned = [];
            }

            $currency = DB::table('orders')
                ->where('country_id', $id)->pluck('currency')->first();


            return response()->json([
                "status" => "200",
                "data" => [
                    "most_deliveries" => $mostDeliveriesByDriver,
                    "most_earned" => $moneyEarned,
                    "avg_delivery" => $avgDelivery,
                    "currency" => $currency

                ]
            ], 200);


        } catch (\Exception $ex) {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);

        }
    }


    public function driverComparision($id, Request $request)
    {

        try {
            $this->validate($request, [
                "driver_id" => "required|array",
                "start_date" => "sometimes|date_format:Y-m-d",
                "end_date" => "required_with:start_date|date_format:Y-m-d|after:start_date",

            ]);

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);

        }
        try {

            $startDate = Carbon::parse($request['start_date'])->startOfDay();  //2016-09-29 00:00:00.000000
            $endDate = Carbon::parse($request['end_date'])->endOfDay(); //2016-09-29 23:59:59.000000

            $mostDeliveriesByDriver = DB::table('driver_order')
                ->select(DB::raw('count(*) as count,user_id'))
                ->where('status', "=", "delivered")
                ->where('country_id', $id)
                ->whereIn('user_id', $request['driver_id'])
                ->whereBetween('created_at', [$startDate, $endDate])
                ->groupBy('user_id')
                ->orderBy('count', "desc")
                ->get();

            $moneyEarned = DB::table('driver_earning')
                ->select(DB::raw('sum(amount) as total_amount,user_id'))
                ->where('type', "=", "normal")
                ->where('country_id', $id)
                ->whereIn('user_id', $request['driver_id'])
                ->whereBetween('created_at', [$startDate, $endDate])
                ->groupBy('user_id')
                ->orderBy('total_amount', "desc")
                ->get();


            $avgDelivery = DB::table('driver_order')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->where('status', "=", "delivered")
                ->where('country_id', $id)
                ->whereIn('user_id', $request['driver_id'])
                ->where('driver_picked_time', '<>', "")
                ->select('user_id', DB::raw('avg(TIMESTAMPDIFF(MINUTE,driver_picked_time,updated_at)) as deliveryTime'))
                ->groupBy("user_id")
                ->orderBy("deliveryTime", 'asc')
                ->get();

            if (count($mostDeliveriesByDriver) <= 0 && count($moneyEarned) <= 0 && count($avgDelivery) <= 0) {
                return response()->json([
                    "status" => "204",
                    "data" => [
                    ]
                ], 204);
            }

            $userId1 = $mostDeliveriesByDriver->count() > 0 ? $mostDeliveriesByDriver->pluck("user_id")->toArray() : [];
            $userId2 = count($moneyEarned) > 0 ? $moneyEarned->pluck("user_id")->toArray() : [];
            $userId3 = count($avgDelivery) > 0 ? $avgDelivery->pluck("user_id")->toArray() : [];

            $userId = array_unique(array_merge($userId1, $userId2, $userId3));


            $userDatas["users"] = $userId;

            $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list";

            $getUserDetailsForReview = \RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);

            if ($getUserDetailsForReview["status"] != "200") {
                if ($getUserDetailsForReview['status'] == 503) {
                    return response()->json(
                        $getUserDetailsForReview, $getUserDetailsForReview['status']);
                }
                return response()->json(
                    $getUserDetailsForReview["message"], $getUserDetailsForReview['status']);
            }
            $userDatasFromOauth = $getUserDetailsForReview["message"]["data"];

            foreach ($userDatasFromOauth as $driver) {
                $name = preg_replace('/\s+/', ' ', trim(implode(" ", array($driver['first_name'], $driver['middle_name'], $driver['last_name']))));
                $user[$driver['id']]['order_count'] = 0;
                $user[$driver['id']]['money_earned'] = '0';
                $user[$driver['id']]['avg_delivery_time'] = "0";

            }

            if ($mostDeliveriesByDriver->count() > 0) {
                foreach ($mostDeliveriesByDriver as $key => $delivery) {
                    foreach ($userDatasFromOauth as $driver) {
                        if ($driver['id'] == $delivery->user_id) {
                            $user[$delivery->user_id]['driver_name'] = preg_replace('/\s+/', ' ', trim(implode(" ", array($driver['first_name'], $driver['middle_name'], $driver['last_name']))));

                            break;
                        }
                    }
                    if (!isset($user[$delivery->user_id]['driver_name'])) {
                        unset($user[$delivery->user_id]);
                    } else {
                        $user[$delivery->user_id]['order_count'] = $delivery->count;

                    }


                }
            }

            if (count($moneyEarned) > 0) {
                foreach ($moneyEarned as $key => $money) {
                    foreach ($userDatasFromOauth as $driver) {
                        if ($driver['id'] == $money->user_id) {
                            $user[$money->user_id]['driver_name'] = preg_replace('/\s+/', ' ', trim(implode(" ", array($driver['first_name'], $driver['middle_name'], $driver['last_name']))));
                            break;

                        }
                    }
                    if (!isset($user[$money->user_id]['driver_name'])) {
                        unset($moneyEarned[$key]);
                    } else {
                        $user[$money->user_id]['money_earned'] = number_format((float)round($money->total_amount, 2), 2, '.', '');
                    }

                }


            }
            if (count($avgDelivery) > 0) {
                foreach ($avgDelivery as $key => $time) {
                    foreach ($userDatasFromOauth as $driver) {
                        if ($driver['id'] == $time->user_id) {
                            $user[$time->user_id]['driver_name'] = preg_replace('/\s+/', ' ', trim(implode(" ", array($driver['first_name'], $driver['middle_name'], $driver['last_name']))));

                            break;
                        }
                    }
                    if (!$user[$time->user_id]['driver_name']) {
                        unset($avgDelivery[$key]);
                    } else {
                        $user[$time->user_id]['avg_delivery_time'] = number_format((float)round($time->deliveryTime, 2), 2, '.', '');
                    }

                }

            }
            $user['currency'] = DB::table('orders')
                ->where('country_id', $id)->pluck('currency')->first();


            return response()->json([
                "status" => "200",
                "data" => $user
            ], 200);

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => $ex->getMessage()
            ], 400);
        }


    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * under construction
     */

    public function exportCsv($countryId)
    {

        try {
            $restaurantId = Input::get("branch_id");
            $parentId = Input::get("restaurant_id");
            if (!empty($restaurantId) && !empty($parentId)) {

                return response()->json([
                    "status" => "422",
                    "message" => "You cannot select both parent and branch restaurant at once.",
                ], 422);

            }
            if (!empty($restaurantId)) {
                if (!$this->checkAuth($restaurantId, null)) {
                    return response()->json([
                        "status" => "403",
                        "message" => "forbidden",
                    ], 403);
                }

            } elseif (!empty($parentId)) {
                if (!$this->checkAuth(null, $parentId)) {
                    return response()->json([
                        "status" => "403",
                        "message" => "forbidden",
                    ], 403);
                }

            } else {
                if (!$this->checkAuth(null, null)) {
                    return response()->json([
                        "status" => "403",
                        "message" => "forbidden",
                    ], 403);
                }
            }

            $orderStatus = Input::get('order_status');
            $paymentStatus = Input::get('payment_status');
            $deliveryStatus = Input::get('delivery_status');
            $deliveryMethod = Input::get('delivery_method');
            $startDate1 = Input::get('start_date');
            $endDate1 = Input::get('end_date');
            $paymentMethod = Input::get('payment_method');
            $deviceType = Input::get("device_type");

            if (!empty($startDate1) && !empty($endDate1)) {
                $startDate = Carbon::parse($startDate1)->startOfDay();  //2016-09-29 00:00:00.000000
                $endDate = Carbon::parse($endDate1)->endOfDay(); //2016-09-29 23:59:59.000000
                if ($startDate->gt($endDate)) {
                    return response()->json([
                        "status" => "422",
                        "message" => "The field start_date cannot be greater than end_date",
                    ], 422);
                }
                $dateRange = [$startDate, $endDate];
            } else {
                return response()->json([
                    "status" => "422",
                    "message" => "The fields start_date and end_date are required",
                ], 422);
            }

            $delivery_fee_sum = 0;
            $sub_total_sum = 0;
            $grand_total_sum = 0;
            $commission_sum = 0;
            $tax_sum = 0;
            $logistics_fee = 0;
            $branches = [];
            if (!empty($parentId)) {
                $restaurantId = null;
                $rest["restaurant_id"] = [$parentId];
                $fullUrl = Config::get('config.restaurant_base_url') . "/get-restaurant-report";
                $restaurantData = RemoteCall::store($fullUrl, $rest);
                if ($restaurantData["status"] != "200") {
                    return response()->json([
                        "status" => $restaurantData["status"],
                        "message" => $restaurantData["message"]["message"],
                    ], $restaurantData["status"]);
                }

                $restaurantList = $restaurantData["message"]['data'];
                if (count($restaurantList) > 0) {
                    $branches = $restaurantList[$parentId]['branch_list'];
                } else {
                    return response()->json([
                        "status" => "204",
                        "message" => "No record found"
                    ], 204);
                }

            }

            $orders = $this->order_model
                ->when($restaurantId, function ($query) use ($restaurantId) {
                    return $query->where('orders.restaurant_id', $restaurantId);

                })
                ->when($countryId, function ($query) use ($countryId) {
                    return $query->where('orders.country_id', $countryId);

                })
                ->when($orderStatus, function ($query) use ($orderStatus) {
                    return $query->where('order_status', $orderStatus);

                })
                ->when($paymentStatus, function ($query) use ($paymentStatus) {
                    return $query->where('payment_status', $paymentStatus);

                })
                ->when($deliveryStatus, function ($query) use ($deliveryStatus) {
                    return $query->where('delivery_status', $deliveryStatus);

                })
                ->when($deliveryMethod, function ($query) use ($deliveryMethod) {
                    return $query->where('delivery_type', $deliveryMethod);

                })
                ->when($dateRange, function ($query) use ($dateRange) {
                    return $query->whereBetween('orders.created_at', $dateRange);

                })
                ->when($deviceType, function ($query) use ($deviceType) {
                    return $query->where('device_type', $deviceType);

                })
                ->when($paymentMethod, function ($query) use ($paymentMethod) {
                    return $query->where('payment_method', $paymentMethod);

                })
                ->when($parentId, function ($query) use ($branches) {
                    return $query->whereIn('orders.restaurant_id', $branches);

                })
                ->leftJoin('driver_order', 'orders.id', '=', 'driver_order.order_id')
                ->select('orders.id as id', 'order_date', 'orders.restaurant', 'customer_name', 'customer_note', 'delivery_type', 'payment_method', 'delivery_status', 'order_status', 'payment_status', 'device_type', 'currency', DB::raw('ROUND(amount - tax - delivery_fee,2) as sub_total'),'site_commission_amount', 'logistics_fee','delivery_fee', 'tax','amount' ,'driver_order.user_id as user_id',"site_tax_rate")->orderBy('id', 'desc')
                ->get();


            if ($orders->count() > 0) {

                $userId = array_unique(array_filter($orders->pluck('user_id')->toArray()));
                //return $userId;

                $userDatas["users"] = $userId;
                if(empty($userId) || is_null($userId)) {
                    $userDatasFromOauth = [];
                    goto skipCallingOauth;
                }
                $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list";
                $getUserDetailsForReview = \RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);

                if ($getUserDetailsForReview["status"] != "200") {
                    if ($getUserDetailsForReview['status'] == 503) {
                        return response()->json(
                            $getUserDetailsForReview, $getUserDetailsForReview['status']);
                    }
                    return response()->json(
                        $getUserDetailsForReview["message"], $getUserDetailsForReview['status']);
                }
                $userDatasFromOauth = $getUserDetailsForReview["message"]["data"];
                skipCallingOauth:


                foreach (collect($orders) as $order) {
                    $restaurant = unserialize($order->restaurant);
//                    $comission_amount = 0;
                    $comission_amount = $order->site_commission_amount;
                    $order->restaurant = $restaurant['translation']['en'];
                    $order->device_type = ucwords($order->device_type);
                    $order->delivery_type = str_replace("-", " ", ucwords($order->delivery_type));

                    $order->payment_method = $order->payment_method == "creditcard" ? "Paytabs" : str_replace("-", " ", ucwords($order->payment_method));
                    $order->order_status = str_replace("-", " ", ucwords($order->order_status));
                    $order->delivery_status = str_replace("-", " ", ucwords($order->delivery_status));
                    $order->payment_status = str_replace("-", " ", ucwords($order->payment_status));
                    $order->site_commission_amount = $comission_amount;
                    $order->amount = number_format((float)round($order->amount, 2), 2, '.', '');
                    $order->addHidden("rejection_reason");
                    $order->sub_total = round($order->sub_total, 2);
                    $order->tax = number_format((float)round($order->tax, 2), 2, '.', '');

                    $notes = $order->orderComments()->select('comment', 'created_at')->get();
                    if (!empty($notes)) {

                        $check = '';
                        foreach ($notes as $note) {

                            $check .= $note['created_at'] . " " . $note['comment'] . PHP_EOL;

                        }
                        $order->note = $check;
                    }
                    if (!empty($notes)) {

                        $check = '';
                        foreach ($notes as $note) {

                            $check .= $note['created_at'] . " " . $note['comment'] . PHP_EOL;

                        }
                        $order->note = $check;
                    }

                    if ($order->delivery_type == 'Home delivery') {
                        foreach ($userDatasFromOauth as $driver) {
                            if ($driver['id'] == $order->user_id) {

                                $order->driver_name = preg_replace('/\s+/', ' ', trim(implode(" ", array($driver['first_name'], $driver['middle_name'], $driver['last_name']))));

                                break;
                            } else {
                                $order->driver_name = "";
                            }
                        }
                    } else {
                        $order->driver_name = "";
                    }
                    $order->makeVisible('restaurant');
                    unset($order->site_tax_rate, $order->user_id,$order->site_tax_rate);

                    $delivery_fee_sum += $order->delivery_fee;
                    $sub_total_sum += $order->sub_total;
                    $grand_total_sum += $order->amount;
                    $commission_sum += $order->site_commission_amount;
                    $tax_sum += $order->tax;
                    $logistics_fee += $order->logistics_fee;


                }


                $orders = json_decode(json_encode($orders), True);

                $total = [
                    $delivery_fee_sum,
                    $sub_total_sum,
                    $grand_total_sum,
                    $commission_sum,
                    $tax_sum,
                    $orders[0]['currency'],
                    $logistics_fee

                ];
                Excel::create('sales_report_' . $startDate1 . "_" . $endDate1, function ($excel) use ($orders, $total) {
                    $excel->sheet('mySheet', function ($sheet) use ($orders, $total) {
                        $sheet->fromArray($orders);
                        $sheet->cell("A1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Order ID");
                        });
                        $sheet->cell("B1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Order Date");
                        });
                        $sheet->cell("C1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Restaurant");
                        });

                        $sheet->cell("D1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Customer");
                        });
                        $sheet->cell("E1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Customer Note");
                        });

                        $sheet->cell("F1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Delivery Type");
                        });

                        $sheet->cell("G1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Payment Method");
                        });
                        $sheet->cell("H1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Delivery Status");
                        });

                        $sheet->cell("I1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Order Status");
                        });

                        $sheet->cell("J1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Payment Status");
                        });

                        $sheet->cell("K1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Device Type");
                        });


                        $sheet->cell("L1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Currency");
                        });
                        $sheet->cell("M1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Sub Total");
                        });

                        $sheet->cell("N1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Commission");
                        });


                        $sheet->cell("O1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Logistics Fee");
                        });

                        $sheet->cell("P1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Delivery Fee");
                        });

                        $sheet->cell("Q1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Tax Amount");
                        });
                        $sheet->cell("R1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Grand Total");
                        });
                        $sheet->cell("S1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Note");
                        });

                        $sheet->cell("T1", function ($cell) use ($orders) {
                            $cell->setFont(array(

                                'size' => '10',
                                'bold' => true,
                            ));

                            $cell->setValue("Driver");
                        });

                        $sheet->rows(array(
                            array(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null),
                            array("Total", null, null, null, null, null, null, null, null, null, null, $total[5], $total[1], $total[3],$total[6], $total[0], $total[4], $total[2], null)
                        ));


                    });
                })->export('csv');

            } else {
                return response()->json([
                    "status" => "204",
                    "message" => "No record found"
                ], 204);
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);
        }


    }

    public function viewStats($countryId)
    {
        try {

            $restaurantId = Input::get("branch_id");
            $parentId = Input::get("restaurant_id");
            if (!empty($restaurantId) && !empty($parentId)) {

                return response()->json([
                    "status" => "403",
                    "message" => "You cannot select both parent and branch restaurant at once.",
                ]);

            }
            if (!empty($restaurantId)) {
                if (!$this->checkAuth($restaurantId, null)) {
//                    dd($this->checkAuth($restaurantId, null));
                    return response()->json([
                        "status" => "403",
                        "message" => "forbidden",
                    ]);
                }

            } elseif (!empty($parentId)) {
                if (!$this->checkAuth(null, $parentId)) {
                    return response()->json([
                        "status" => "403",
                        "message" => "forbidden",
                    ]);
                }

            } else {
                if (!$this->checkAuth(null, null)) {
                    return response()->json([
                        "status" => "403",
                        "message" => "forbidden",
                    ]);
                }
            }

            $orderStatus = Input::get('order_status');
            $paymentStatus = Input::get('payment_status');
            $deliveryStatus = Input::get('delivery_status');
            $deliveryMethod = Input::get('delivery_method');
            $startDate = Input::get('start_date');
            $endDate = Input::get('end_date');
            $paymentMethod = Input::get('payment_method');
            $deviceType = Input::get("device_type");

            if (!empty($startDate) && !empty($endDate)) {
                $startDate = Carbon::parse($startDate)->startOfDay();  //2016-09-29 00:00:00.000000
                $endDate = Carbon::parse($endDate)->endOfDay(); //2016-09-29 23:59:59.000000
                if ($startDate->gt($endDate)) {
                    return response()->json([
                        "status" => "422",
                        "message" => "The field start_date cannot be greater than end_date",
                    ], 422);
                }
                $dateRange = [$startDate, $endDate];
            } else {
                return response()->json([
                    "status" => "422",
                    "message" => "The fields start_date and end_date are required",
                ], 422);
            }

            $branches = [];
            if (!empty($parentId)) {
                $restaurantId = null;
                $rest["restaurant_id"] = [$parentId];
                $fullUrl = Config::get('config.restaurant_base_url') . "/get-restaurant-report";
                $restaurantData = RemoteCall::store($fullUrl, $rest);
                if ($restaurantData["status"] != "200") {
                    return response()->json([
                        "status" => $restaurantData["status"],
                        "message" => $restaurantData["message"]["message"],
                    ], $restaurantData["status"]);
                }

                $restaurantList = $restaurantData["message"]['data'];
                $branches = $restaurantList[$parentId]['branch_list'];
            }

            $orders = $this->order_model
                ->when($restaurantId, function ($query) use ($restaurantId) {
                    return $query->where('restaurant_id', $restaurantId);

                })
                ->when($countryId, function ($query) use ($countryId) {
                    return $query->where('country_id', $countryId);

                })
                ->when($orderStatus, function ($query) use ($orderStatus) {
                    return $query->where('order_status', $orderStatus);

                })
                ->when($paymentStatus, function ($query) use ($paymentStatus) {
                    return $query->where('payment_status', $paymentStatus);

                })
                ->when($deliveryStatus, function ($query) use ($deliveryStatus) {
                    return $query->where('delivery_status', $deliveryStatus);

                })
                ->when($deliveryMethod, function ($query) use ($deliveryMethod) {
                    return $query->where('delivery_type', $deliveryMethod);

                })
                ->when($dateRange, function ($query) use ($dateRange) {
                    return $query->whereBetween('created_at', $dateRange);

                })
                ->when($deviceType, function ($query) use ($deviceType) {
                    return $query->where('device_type', $deviceType);

                })
                ->when($paymentMethod, function ($query) use ($paymentMethod) {
                    return $query->where('payment_method', $paymentMethod);

                })
                ->when($parentId, function ($query) use ($branches) {
                    return $query->whereIn('restaurant_id', $branches);

                })
                ->select('device_type', 'currency', 'site_commission_amount', 'amount', 'delivery_fee', 'tax','site_tax_rate','logistics_fee')
                ->get();

            if ($orders->count() > 0) {
                $delivery_fee_sum = 0;
                $grand_total_sum = 0;
                $commission_sum = 0;
                $logistic_sum = 0;
                $tax_sum = 0;
                $device['ios'] = 0;
                $device['web'] = 0;
                $device['android'] = 0;
                $order_count = 0;


                foreach ($orders as $order) {
                    $order_count++;
                    if ($order->device_type == "ios") {
                        $device['ios']++;

                    } elseif ($order->device_type == "web") {
                        $device['web']++;
                    } elseif ($order->device_type == "android") {
                        $device['android']++;
                    } else {

                    }

                    $order->amount = number_format((float)round($order->amount, 2), 2, '.', '');
                    $order->delivery_fee = number_format((float)round($order->delivery_fee, 2), 2, '.', '');
                    $commission_sum += ($order->site_commission_amount * (1 + $order->site_tax_rate * 0.01));
                    $logistic_sum += ($order->logistics_fee * (1 + $order->site_tax_rate * 0.01));
                    unset($order->site_tax_rate);
                    $delivery_fee_sum += $order->delivery_fee;
                    $grand_total_sum += $order->amount;

                    $tax_sum += $order->tax;


                }

                $total = [
                    "order_count" => $order_count,
                    "total_delivery_fee" => number_format((float)round($delivery_fee_sum, 2), 2, '.', ''),
                    "total_grand_total" => number_format((float)round($grand_total_sum, 2), 2, '.', ''),
                    "total_commission" => number_format((float)round($commission_sum, 2), 2, '.', ''),
                    "total_logistic_fee" =>  number_format((float)round($logistic_sum, 2), 2, '.', ''),
                    "total_tax" => number_format((float)round($tax_sum, 2), 2, '.', ''),
                    "device_type" => $device,
                    "currency" => $orders[0]['currency']
                ];

                return response()->json([
                    "status" => "200",
                    "data" => $total
                ], 200);


            } else {
                return response()->json([
                    "status" => "204",
                    "message" => "No record found"
                ], 204);
            }
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);
        }

    }

    private function checkAuth($restaurantId = null, $parentId = null)
    {
        $restaurantInfo = RoleChecker::getRestaurantInfo();
        if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
            return true;
        } elseif (RoleChecker::hasRole('restaurant-admin') || (RoleChecker::get('parentRole') == 'restaurant-admin')) {
            if ($restaurantInfo['is_restaurant_admin'] == "1") {
                if (in_array($restaurantId, $restaurantInfo["branch_id"])) {
                    return true;
                } elseif ($restaurantInfo["restaurant_id"] == $parentId) {
                    return true;
                }
            }
        } elseif (RoleChecker::hasRole('branch-admin') || (RoleChecker::get('parentRole') == 'branch-admin')) {
            if ($restaurantInfo["branch_id"] == $restaurantId) {
                return true;
            }
        }


        return false;
    }

    /**
     * Dashboard report for admin and super admin.
     * Contains overall stats for orders, revenue, tax, commission, restaurant and branch count, usrs count a
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDashboardReport()
    {
        try {
            $fullUrl = Config::get('config.restaurant_base_url') . "/get-count";

            $restaurantData = RemoteCall::getSpecificWithoutToken($fullUrl);
            if ($restaurantData["status"] != "200") {
                return response()->json([
                    "status" => $restaurantData["status"],
                    "message" => $restaurantData["message"]["message"],
                ], $restaurantData["status"]);
            }


            $fullUrl = Config::get('config.oauth_base_url') . "/user/role-group";

            $userData = RemoteCall::getSpecificWithoutToken($fullUrl);

            if ($userData["status"] != "200") {
                return response()->json([
                    "status" => $userData["status"],
                    "message" => $userData["message"]["message"],
                ], $userData["status"]);
            }


            if (count($restaurantData['message']['data']) > 0) {
                if (count($restaurantData['message']['data']['restaurant_count']) > 0) {
                    foreach ($restaurantData['message']['data']['restaurant_count'] as $restaurantDatum) {
                        $resList[$restaurantDatum['country_id']] = $restaurantDatum['restaurant_count'];

                        $countryId[] = $restaurantDatum['country_id'];
                    }
                }
                if (count($restaurantData['message']['data']['branch_count']) > 0) {
                    foreach ($restaurantData['message']['data']['branch_count'] as $branchData) {
                        $branchList[$branchData['country_id']] = $restaurantDatum['restaurant_count'];
                        $countryId[] = $branchData['country_id'];
                    }
                }

            }

            if (count($userData['message']['data']) > 0) {
                foreach ($userData['message']['data'] as $user) {
                    $userList[$user['country_id']][$user['display_name']] = $user['count'];
                    $countryId[] = $user['country_id'];
                }
            }
            DB::enableQueryLog();
            $orders = $this->order_model
                ->select(DB::raw('SUM(amount) as total_amount, currency, SUM(tax) as total_tax, SUM(delivery_fee) as total_delivery_fee, count(*) as total_orders, country_id, device_type'))
                ->where([
                    ['order_status', "accepted"],
                    ["payment_status", "completed"],
                    ['is_seen', 1],
                ])->groupBy(['country_id', 'device_type'])
                ->where('device_type', "<>", "")
                ->get();

            if ($orders->count() > 0) {

                foreach ($orders as $order) {
                    $orderData[$order['country_id']][$order['device_type']] = $order;
                    $countryId[] = $order['country_id'];
                }

                foreach ($orderData as $ke => $oo) {


                    $totalTax[$ke] = 0;
                    $totalDelivery[$ke] = 0;
                    $totalOrders[$ke] = 0;
                    $totalAmount[$ke] = 0;
                    foreach ($oo as $check) {


                        $orderDevice[$ke][$check['device_type']] = $check['total_orders'];
                        $totalTax[$ke] += $check['total_tax'];
                        $totalDelivery[$ke] += $check['total_delivery_fee'];
                        $totalOrders[$ke] += $check['total_orders'];
                        $totalAmount[$ke] += $check['total_amount'];
                        $currency[$ke] = $check['currency'];

                    }
                }

            }


            $countryId = array_unique($countryId);
            if (count($countryId) > 0) {
                $request['country_id'] = $countryId;
                $fullUrl = Config::get('config.location_base_url') . "/country-name";
                $countryData = \RemoteCall::store($fullUrl, $request);
                if ($countryData["status"] != "200") {
                    return response()->json([
                        "status" => $countryData["status"],
                        "message" => $countryData["message"]["message"],
                    ], $countryData["status"]);
                }
            }

            foreach ($countryData['message']['data'] as $key => $value) {
                $data[] = [
                    'country' => $value,
                    'restaurant_count' => isset($resList[$key]) ? $resList[$key] : 0,
                    'branch_count' => isset($branchList[$key]) ? $branchList[$key] : 0,
                    'user_role' => isset($userList[$key]) ? $userList[$key] : [],
                    "total_amount" => isset($totalAmount[$key]) ? $totalAmount[$key] : 0,
                    "currency" => isset($currency[$key]) ? $currency[$key] : "",
                    "total_tax" => isset($totalTax[$key]) ? $totalTax[$key] : 0,
                    "total_delivery_fee" => isset($totalDelivery[$key]) ? $totalDelivery[$key] : 0,
                    "total_orders" => isset($totalOrders[$key]) ? $totalOrders[$key] : 0,
                    "device_type" => isset($orderDevice[$key]) ? $orderDevice[$key] : 0,


                ];
            }


            return response()->json([
                "status" => "200",
                "$data" => $data
            ], 200);


        } catch (\Exception $ex) {
            return response()->json([
                "status" => "400",
                "message" => $ex->getMessage()
            ], 400);
        }

    }

    /**
     * For dashboard stats by country id.
     * @param $countryId
     * @return \Illuminate\Http\JsonResponse
     */

    public function getDashboardReportByCountry($countryId)
    {
        $fullUrl = Config::get('config.restaurant_base_url') . "/get-count/" . $countryId;
        $restaurantData = RemoteCall::getSpecificWithoutToken($fullUrl);
        if ($restaurantData["status"] != "200") {
            return response()->json([
                "status" => $restaurantData["status"],
                "message" => $restaurantData["message"]["message"],
            ], $restaurantData["status"]);
        }

        $fullUrl = Config::get('config.oauth_base_url') . "/user/role-group/" . $countryId;

        $userData = RemoteCall::getSpecificWithoutToken($fullUrl);

        if ($userData["status"] != "200") {
            return response()->json([
                "status" => $userData["status"],
                "message" => $userData["message"]["message"],
            ], $userData["status"]);
        }


        $orders = $this->order_model
            ->select('device_type', 'currency', 'site_commission_amount', 'amount', 'site_tax_rate', 'delivery_fee')
            ->where([
                ['order_status', "accepted"],
                ["payment_status", "completed"],
                ['is_seen', 1],
            ])
            ->get();

        if ($orders->count() > 0) {
            $delivery_fee_sum = 0;
            $sub_total_sum = 0;
            $grand_total_sum = 0;
            $commission_sum = 0;
            $tax_sum = 0;
            $device['ios'] = 0;
            $device['web'] = 0;
            $device['android'] = 0;
            $order_count = 0;


            foreach ($orders as $order) {
                $order_count++;
                if ($order->device_type == "ios") {
                    $device['ios']++;

                } elseif ($order->device_type == "web") {
                    $device['web']++;
                } elseif ($order->device_type == "android") {
                    $device['android']++;
                } else {

                }

                $order->amount = number_format((float)round($order->amount, 2), 2, '.', '');
                $order->delivery_fee = number_format((float)round($order->delivery_fee, 2), 2, '.', '');
                $order->sub_total = number_format((float)round(((100 * $order->amount) / (100 + $order->site_tax_rate)) - $order->delivery_fee, 2), 2, '.', '');
                $order->tax_amount = number_format((float)round(($order->sub_total + $order->delivery_fee) * ($order->site_tax_rate / 100), 2), 2, '.', '');

                unset($order->site_tax_rate);

                $delivery_fee_sum += $order->delivery_fee;
                $sub_total_sum += $order->sub_total;
                $grand_total_sum += $order->amount;
                $commission_sum += $order->site_commission_amount;
                $tax_sum += $order->tax_amount;

            }

            $total = [
                "order_count" => $order_count,
                "total_delivery_fee" => number_format((float)round($delivery_fee_sum, 2), 2, '.', ''),
                "total_sub_total" => number_format((float)round($sub_total_sum, 2), 2, '.', ''),
                "total_grand_total" => number_format((float)round($grand_total_sum, 2), 2, '.', ''),
                "total_commission" => number_format((float)round($commission_sum, 2), 2, '.', ''),
                "total_tax" => number_format((float)round($tax_sum, 2), 2, '.', ''),
                "device_type" => $device,
                "currency" => $orders[0]['currency'],
                "restaurant_count" => $restaurantData['message']['data']['restaurant_count'],
                "branch_count" => $restaurantData['message']['data']['branch_count'],
                "users" => $userData['message']['data']
            ];

            return response()->json([
                "status" => "200",
                "data" => $total
            ], 200);


        } else {
            return response()->json([
                "status" => "204",
                "message" => "No record found"
            ], 204);
        }

    }


    /**
     * Dashboard Stats for branch operators
     * @param $restaurantId
     */

    public function dashboardStatForOperator()
    {
        try {
            $restaurantInfo = RoleChecker::getRestaurantInfo();
            if (RoleChecker::hasRole('branch-operator')) {
                $orders = DB::table('orders')
                    ->select(DB::raw('count(*) as count,SUM(amount) as total_revenue, restaurant_id,currency'))
                    ->where([
                        ['order_status', "accepted"],
                        ["payment_status", "completed"],
                        ['is_seen', 1],
                        ['restaurant_id', $restaurantInfo['branch_id']]

                    ])
                    ->first();


                $orders2 = DB::table('orders')
                    ->select(DB::raw('count(*) as count, order_status'))
                    ->where([
                        ['restaurant_id', $restaurantInfo['branch_id']]

                    ])
                    ->groupBy('order_status')
                    ->pluck('count', 'order_status');

                return response()->json([
                    "status" => "200",
                    "data" => [
                        "total_revenue" => empty($orders->total_revenue) ? 0 : $orders->total_revenue,
                        "currency" => $orders->currency,
                        "orders" => [
                            "total_orders" => array_sum($orders2->toArray()),
                            "total_accepted_orders" => isset($orders2['accepted']) ? $orders2['accepted'] : 0,
                            "total_rejected_orders" => isset($orders2['rejected']) ? $orders2['rejected'] : 0,


                        ]
                    ]

                ]);
            }
        } catch (\Exception $exception) {
            return response()->json([
                "status" => "400",
                "message" => $exception->getMessage()
            ]);
        }


    }

    public function AdminDashBoardReport($id)
    {
        try {
            /**
             * checks if the key exist in redis . if exist then fetch the redis key data otherwise fetch the data by sql query and http request and set to redis
             */

            top:
            if(! \Redis::exists('admin-dashboard-stats')){
                $day = Carbon::now();

                $month = Carbon::parse($day)->format('Y-m');
                $year = Carbon::parse($day)->format('Y');
                $yesterday_date = Carbon::parse($day)->subDay(1)->format('Y-m-d');

                $query = DB::table('orders')
                    ->select('country_id','currency','time_zone_utc', DB::raw('SUM(amount) as revenue,COUNT(DISTINCT id) AS fulfilled_orders , SUM(site_commission_amount * (1 + site_tax_rate * 0.01)) as commission, SUM(delivery_fee) as delivery_fee , SUM(logistics_fee * (1 + site_tax_rate * 0.01)) as logistic_fee'))
                    ->where([
                        ['country_id', $id],
                        ['order_status', "accepted"],
                        ["payment_status", "completed"],
                        ['is_seen', 1],
                    ]);


                $ldFulfilled = $query->get();

                $today = Carbon::now($ldFulfilled[0]->time_zone_utc)->toFormattedDateString();
                $startDate = new Carbon('first day of this month');
                $startDate = $startDate->toFormattedDateString();

                $mdFulfilled = $query->where('created_at', 'like', $month . '%')->get();

                $previous_day_fulfilled = $query->where('created_at', 'like', $yesterday_date . '%')->get();

                $query1 = DB::table('orders')
                    ->where([
                        ['country_id', $id],
                    ]);
                $ld_total_orders = $query1->count();


                $md_total_orders = $query1->where('created_at', 'like', $month . '%')->count();
                $previous_day_total_orders = $query1->where('created_at', 'like', $yesterday_date . '%')->count();

                $pendingOrders = DB::table('orders')
                    ->select(DB::raw('COUNT(DISTINCT id) AS pending_orders'))
                    ->where([
                        ['country_id', $id],
                        ['order_status', "pending"],
                        ['created_at', 'like', $month . '%']
                    ])->value('pending_orders');

                $yesterday_pending_orders = DB::table('orders')
                    ->select(DB::raw('COUNT(DISTINCT id) AS pending_orders'))
                    ->where([
                        ['country_id', $id],
                        ['order_status', "pending"],
                        ['created_at', 'like', $yesterday_date . '%']
                    ])->value('pending_orders');

                $dataForGraph = DB::table('orders')
                    ->select(DB::raw('COUNT(DISTINCT id) AS total,Day(created_at) as day'))
                    ->where([
                        ['country_id', $id],
                        ['created_at', 'like', $month . '%']
                    ])->groupBy('day')
                    ->pluck('total', 'day');

                $dataForMonthlyGraph = DB::table('orders')
                    ->select(DB::raw('COUNT(DISTINCT id) AS total,Month(created_at) as month'))
                    ->where([
                        ['country_id', $id],
                        ['created_at', 'like', $year . '%']
                    ])->groupBy('month')
                    ->pluck('total', 'month');


                $fullUrl = Config::get('config.oauth_base_url') . "/dashboard-user-count/" . $id;

                $userData = RemoteCall::getSpecificWithoutToken($fullUrl);

                if ($userData["status"] != "200") {
                    return response()->json([
                        "status" => $userData["status"],
                        "message" => $userData["message"]["message"],
                    ], $userData["status"]);
                }
                $auth = $userData['message']['data'];

                $ltd = [
                    "revenue" => is_null($ldFulfilled[0]->revenue) ? 0 : $ldFulfilled[0]->revenue,
                    'fulfilled_orders' => is_null($ldFulfilled[0]->fulfilled_orders) ? 0 : $ldFulfilled[0]->fulfilled_orders,
                    'commission' => is_null($ldFulfilled[0]->commission) ? 0 : $ldFulfilled[0]->commission,
                    "logistic_fee" => is_null($ldFulfilled[0]->logistic_fee) ? 0 : $ldFulfilled[0]->logistic_fee,
                    'delivery_fee' => is_null($ldFulfilled[0]->delivery_fee) ? 0 : $ldFulfilled[0]->delivery_fee,
                    'customer' => isset($auth['ld_users']['customer']) ? $auth['ld_users']['customer'] : 0,
                    'driver' => isset($auth['ld_users']['driver']) ? $auth['ld_users']['driver'] : 0,
                    'total_orders' => $ld_total_orders

                ];

                $mtd = [
                    "revenue" => is_null($mdFulfilled[0]->revenue) ? 0 : $mdFulfilled[0]->revenue,
                    'fulfilled_orders' => $mdFulfilled[0]->fulfilled_orders,
                    'pending_orders' => $pendingOrders,
                    'commission' => is_null($mdFulfilled[0]->commission) ? 0 : $mdFulfilled[0]->commission,
                    'delivery_fee' => is_null($mdFulfilled[0]->delivery_fee) ? 0 : $mdFulfilled[0]->delivery_fee,
                    "logistic_fee" => is_null($mdFulfilled[0]->logistic_fee) ? 0 : $mdFulfilled[0]->logistic_fee,
                    'customer' => isset($auth['md_users']['customer']) ? $auth['md_users']['customer'] : 0,
                    'driver' => isset($auth['md_users']['driver']) ? $auth['md_users']['driver'] : 0,
                    'total_orders' => $md_total_orders

                ];

                $yesterday = [
                    "revenue" => is_null($previous_day_fulfilled[0]->revenue) ? 0 : $previous_day_fulfilled[0]->revenue,
                    'fulfilled_orders' => $previous_day_fulfilled[0]->fulfilled_orders,
                    'pending_orders' => $yesterday_pending_orders,
                    'commission' => is_null($previous_day_fulfilled[0]->commission) ? 0 : $previous_day_fulfilled[0]->commission,
                    'delivery_fee' => is_null($previous_day_fulfilled[0]->delivery_fee) ? 0 : $previous_day_fulfilled[0]->delivery_fee,
                    "logistic_fee" => is_null($previous_day_fulfilled[0]->logistic_fee) ? 0 : $previous_day_fulfilled[0]->logistic_fee,
                    'customer' => isset($auth['yesterday_users']['customer']) ? $auth['yesterday_users']['customer'] : 0,
                    'driver' => isset($auth['yesterday_users']['driver']) ? $auth['yesterday_users']['driver'] : 0,
                    'total_orders' => $previous_day_total_orders
                ];

                $data = [
                    "date_string" => $startDate.' to '.$today,
                    'country_id' => $id,
                    "currency" => $ldFulfilled[0]->currency,
                    "ltd" => $ltd,
                    "mtd" => $mtd,
                    'yesterday' => $yesterday,
                    "mtd_progress" => $dataForGraph,
                    'monthly_progress' => $dataForMonthlyGraph

                ];

                \Redis::set('admin-dashboard-stats',serialize($data));
            }
            else{
                $get_redis_data =  \Redis::get('admin-dashboard-stats');
                $data = unserialize($get_redis_data);
                if((!isset($data['country_id']) || ($data['country_id'] != $id))){
                    \Redis::del('admin-dashboard-stats');
                    goto top;
                }
            }

            return response()->json([
                "status" => "200",
                "data" => $data
            ], 200);

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => "something went wrong."
            ], 500);
        }
    }

    public function ResAdminDashBoardReport()
    {
        try {
            $day = Carbon::now();

            $month = Carbon::parse($day)->format('Y-m');

            $restaurantInfo = RoleChecker::getRestaurantInfo();
            if (RoleChecker::hasRole('branch-admin')) {
                $restaurantInfo['branch_id'] = [$restaurantInfo['branch_id']];

            } else {
                $branchCount = count($restaurantInfo['branch_id']);
            }

            $query = DB::table('orders')
                ->whereIn('restaurant_id', $restaurantInfo['branch_id']);

            $check = $query->select('currency','time_zone_utc')->get();

            $today = Carbon::now($check[0]->time_zone_utc)->toFormattedDateString();
            $startDate = new Carbon('first day of this month');
            $startDate = $startDate->toFormattedDateString();

            $totalOrders = $query->select(DB::raw('COUNT(DISTINCT id) AS count'))->where('created_at', 'like', $month . '%')->get();

            $fulfilledOrders = $query
                ->select('currency', DB::raw('SUM(amount) as revenue,COUNT(DISTINCT id) AS count'))
                ->where('created_at', 'like', $month . '%')
                ->where([
                    ['order_status', "accepted"],
                    ["payment_status", "completed"],
                    ['is_seen', 1],
                ])->get();

            $dataForGraph = DB::table('orders')
                ->select(DB::raw('COUNT(DISTINCT id) AS total,Day(created_at) as day'))
                ->whereIn('restaurant_id', $restaurantInfo['branch_id'])
                ->where([
                    ['created_at', 'like', $month . '%']
                ])->groupBy('day')
                ->pluck('total', 'day');


            $data = [
                "date_string" => $startDate.' to '.$today,
                "currency" => $check[0]->currency,
                "total_orders" => $totalOrders[0]->count,
                "revenue" => is_null($fulfilledOrders[0]->revenue) ? 0 : $fulfilledOrders[0]->revenue,
                "fulfilled_orders" => is_null($fulfilledOrders[0]->count) ? 0 : $fulfilledOrders[0]->count,
                "orders" => $dataForGraph
            ];
            if (isset($branchCount)) {
                $data["total_branch"] = $branchCount;
            }

            return response()->json([
                "status" => "200",
                "data" => $data
            ], 200);


        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => "Something went wrong."
            ], 500);
        }
    }

}