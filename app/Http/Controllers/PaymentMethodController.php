<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 6/25/17
 * Time: 11:31 AM
 */

namespace App\Http\Controllers;

use App\Events\IpnEvent;
use App\Events\SendMessage;
use App\Repo\ShippingMethodInterface;
use App\Repo\ShippingMethodTranslationInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redis;
use LogStoreHelper;
use App\Repo\PaymentMethodInterface;
use App\Repo\OrderStatusInterface;
use App\Repo\OrderStatusTranslationInterface;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cocur\Slugify\Slugify;
use RoleChecker;
use App\Repo\OrderInterface;
use App\Repo\PaymentMethodTranslationInterface;
use ImageUploader;
use Yajra\Datatables\Datatables;
use Settings;
class PaymentMethodController extends Controller
{
    private $paymentMethod, $orderStatus, $shippingMethod, $shippingMethodTranslation, $orderStatusTranslation, $logStoreHelper, $slugify, $order, $paymentMethodTranslation;


    /**
     * PaymentMethodController constructor.
     * @param LogStoreHelper $logStoreHelper
     * @param PaymentMethodInterface $paymentMethod
     * @param OrderStatusTranslationInterface $orderStatusTranslation
     * @param OrderStatusInterface $orderStatus
     */
    public function __construct(LogStoreHelper $logStoreHelper, PaymentMethodInterface $paymentMethod, OrderStatusTranslationInterface $orderStatusTranslation, OrderStatusInterface $orderStatus, Slugify $slugify, OrderInterface $order, PaymentMethodTranslationInterface $paymentMethodTranslation, ShippingMethodTranslationInterface $shippingMethodTranslation, ShippingMethodInterface $shippingMethod)
    {
        $this->paymentMethod = $paymentMethod;
        $this->orderStatus = $orderStatus;
        $this->orderStatusTranslation = $orderStatusTranslation;
        $this->logStoreHelper = $logStoreHelper;
        $this->slugify = $slugify;
        $this->order = $order;
        $this->paymentMethodTranslation = $paymentMethodTranslation;
        $this->shippingMethod = $shippingMethod;
        $this->shippingMethodTranslation = $shippingMethodTranslation;
    }

    /**
     * Return the list of payment methods from the payment_method table.
     * Check if user is authorized to view the list.
     * If yes, display paginated list of payment methods, else display error message.
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if ($this->checkAuth()) {

            $methods = $this->paymentMethod->getAll();
            foreach ($methods as $key => $method) {
                /**
                 * Get category Translation based on @param lang
                 * if empty record is obtained default en lang is returned
                 *If default en lang is not available, throw exception
                 * */
                $methodTranslation = $method->paymentMethodTranslation()->where('lang', 'en')->get();
                try {
                    if ($methodTranslation->count() == 0) {

                        throw new \Exception();

                    }

                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }

                /*
                 * attach name and lang_code in $category variable
                 * */
                $method['name'] = $methodTranslation[0]['name'];
                $method['lang'] = $methodTranslation[0]['lang'];
            }

            try {
                if (!$methods->first()) {
                    throw  new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Record"
                ], 404);
            }
            return Datatables::of($methods)->make(true);
//            return response()->json([
//                'status' => "200",
//                "data" => $methods
//            ], 200);

        } else {
            return response()->json([
                "status" => "401",
                "message" => "Only Authorized personnel can view payment method"
            ], 401);
        }
    }

    public function getListOfPaymentAndShippingMethodDetails(Request $request)
    {
        try {
            $this->validate($request, [
                "slugs" => "required|array",
                "slugs.payment" => "sometimes|array",
                "slugs.shipping" => "sometimes|array",
                "lang" => "required|alpha"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        $paymentList = [];
        if (isset($request->slugs['payment'])) {
            foreach ($request->slugs['payment'] as $slug) {
                $paymentMethod = $this->paymentMethod->getSpecificPaymentMethodBySlug($slug);
                if (!$paymentMethod) continue;

                $methodTranslation = $paymentMethod->paymentMethodTranslation()->where('lang', $request['lang'])->get();
                if ($methodTranslation->count() == 0) {
                    $methodTranslation = $paymentMethod->paymentMethodTranslation()->where('lang', 'en')->get();
                }
                if ($methodTranslation->count() == 0) continue;
                $paymentMethod['name'] = $methodTranslation[0]['name'];

                $paymentList[] = $paymentMethod->makeHidden(["order_status", "class", "type", "created_at", "updated_at", "status"]);

            }
        }

        $shippingList = [];
        if (isset($request->slugs['shipping'])) {
            foreach ($request->slugs['shipping'] as $slug) {
                $shippingMethod = $this->shippingMethod->getSpecificShippingMethodBySlug($slug);
                if (count($shippingMethod) == 0) continue;

                $methodTranslation = $this->shippingMethodTranslation->getAllShippingMethodTranslationByLang($shippingMethod[0]['id'], $request['lang']);
                if (count($methodTranslation) == 0) continue;
                $shippingMethod[0]['name'] = $methodTranslation[0]['name'];
                $shipping = $shippingMethod[0];

                $shippingList[] = $shipping->makeHidden(["class", "is_taxable", "shipping_cost", "dynamic_shipping_value", "shipping_type", "created_at", "updated_at", "status"]);
            }
        }

        $list['shipping'] = $shippingList;
        $list['payment'] = $paymentList;

        return response()->json([
            "status" => "200",
            "data" => $list
        ], 200);
    }

    /**
     * Store the payment method in payment_method.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if ($this->checkAuth()) {
            /**
             * Validate the request sent from frontend
             * If validation fails, return relevant error messages.
             * Else, store payment method into payment_method table.
             */
            $orderStatus = $this->orderStatus->getActiveOrderStatus();

            if ($orderStatus->count() > 0) {
                unset($slugList);
                foreach ($orderStatus as $order) {
                    $slugList[] = $order['slug'];
                }
            } else {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Active Order Status"
                ], 404);
            }


            try {
                $request['slug'] = $this->slugify->slugify($request['slug'], ['regexp' => '/([^A-Za-z]|-)+/']);

                $message = [
                    "slug.required" => "The slug should contain letters only.",
                    "translation.en.required" => "English title is required."
                ];
                $this->validate($request, [
                    "slug" => "required|unique:payment_method,slug",
                    "class" => "required",
                    "icon" => "required",
                    "type" => "required|in:redirected,non-redirected",
//                    "order_status" => "required|in:" . implode($slugList, ","),
                    "status" => "required|integer|min:0|max:1",
                    "translation" => "required|array",
                    "translation.en" => "required"

                ], $message);

            } catch (\Exception $e) {

                return response()->json([
                    "status" => "422",
                    "message" => $e->response->original
                ], 422);
            }


            try {
                /**
                 * Start DB transaction to ensure the operation is reversed in case not successfully committed.
                 *
                 */

                DB::beginTransaction();
                $request['slug'] = $this->slugify->slugify($request['slug']);
                $request['status'] = (string)$request['status'];
                //uploading image
                $imageUpload = $this->saveImage($request['icon'], "icon");
                if ($imageUpload["status"] != 200) {
                    return response()->json(
                        $imageUpload["message"]
                        , $imageUpload["status"]);
                }
                $request['icon'] = $imageUpload["message"]["data"];
                $method = $this->paymentMethod->createPaymentMethod($request->all());

                foreach ($request['translation'] as $key => $translation) {
                    $translation['payment_method_id'] = $method->id;
                    $translation['lang'] = $key;
                    /**
                     * Validate lang and name fields.
                     * lang field is required and must be alphabets.
                     * name field is required.
                     * If validation fails, abort with validation error message.
                     * Else insert data in payment_method_translation table.
                     */
                    $rules = [
                        "lang" => 'required|alpha',
                        "name" => "required"
                    ];
                    $translation['lang'] = $this->slugify->slugify($translation['lang']);

                    $validator = Validator::make($translation, $rules);
                    if ($validator->fails()) {
                        $error = $validator->errors();
                        $this->logStoreHelper->storeLogError([
                            "Validation Error", [
                                'status' => '422',
                                "message" => [$key => $error]
                            ]
                        ]);
                        return response()->json([
                            'status' => '422',
                            "message" => [$key => $error]
                        ], 422);
                    }
                    /**
                     * Insert data into category_translation
                     */

                    $createTranslation[] = $this->paymentMethodTranslation->createPaymentMethodTranslation($translation);


                }
//                $method['translation'] = $createTranslation;
//                dd($createTranslation);

                DB::commit();

                $this->logStoreHelper->storeLogInfo([
                    "PaymentMethod",
                    [
                        "status" => "200",
                        "message" => "Payment Method Created Successfully",
                        //"data" => $method
                    ]
                ]);
                return response()->json([
                    "status" => "200",
                    "message" => "Payment Method Created Successfully",
                    //"data" => $method
                ], 200);

            } catch (\Exception $e) {
                return response()->json([
                    "status" => "422",
                    //"message" => "There was error in creating payment method"
                    "message" => $e->getMessage()

                ], 422);
            }
        } else {
            return response()->json([
                "status" => "401",
                "message" => "Only Authorized personnel can create payment method"
            ], 401);
        }

    }

    /**
     * Delete payment method from payment_method table.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if ($this->checkAuth()) {
            try {

                $this->paymentMethod->deletePaymentMethod($id);

                return response()->json([
                    "status" => "200",
                    "message" => "Requested Payment Method Deleted Successfully"
                ], 200);

            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Requested Payment Method could not be found"
                ], 404);

            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "422",
                    "message" => "There were problem deleting Payment Method"
                ], 422);
            }

        } else {
            return response()->json([
                "status" => "401",
                "message" => "Only Authorized personnel can delete payment method"
            ], 401);
        }

    }

    /**
     * Display specific payment method for given id.
     * Check if user is aut
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        if ($this->checkAuth()) {
            try {

                $method = $this->paymentMethod->getSpecificPaymentMethod($id);
                $method['translation'] = $method->paymentMethodTranslation;

            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Requested Payment Method could not be found"
                ], 404);
            }

            return response()->json([
                "status" => "200",
                "data" => $method
            ], 200);
        } else {
            return response()->json([
                "status" => "401",
                "message" => "Only Authorized personnel can view payment method"
            ], 401);
        }
    }

    /**
     * Update specific payment method for given id with the request provided.
     * If payment method of given id, return error message.
     * Else update the payment method and return success message.
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        if ($this->checkAuth()) {
            try {
                $method = $this->paymentMethod->getSpecificPaymentMethod($id);

                try {
                    $orderStatus = $this->orderStatus->getActiveOrderStatus();

                    if ($orderStatus->count() > 0) {
                        unset($slugList);
                        foreach ($orderStatus as $order) {
                            $slugList[] = $order['slug'];
                        }
                    } else {
                        return response()->json([
                            "status" => "404",
                            "message" => "Empty Active Order Status"
                        ], 404);
                    }

                    /**
                     * Validate the request provided.
                     * If validation fails, return error message.
                     * Else update the specific payment method in payment_method and return success message.
                     */
                    $request['slug'] = $this->slugify->slugify($request['slug'], ['regexp' => '/([^A-Za-z]|-)+/']);

                    $message = [
                        "slug.required" => "The slug should contain letters only.",
                        "translation.en.required" => "English title is required."
                    ];

                    $this->validate($request, [
                        "slug" => "required|unique:payment_method,slug," . $id,
                        "class" => "required",
                        "icon" => "required",
                        "type" => "required|in:redirected,non-redirected",
//                        "order_status" => "required|in:" . implode($slugList, ","),
                        "status" => "required|integer|min:0|max:1",

                    ], $message);

                } catch (\Exception $e) {
                    return response()->json([
                        "status" => "422",
                        "message" => $e->response->original
                    ]);
                }

                DB::beginTransaction();

                $request['status'] = (string)$request['status'];

                //uploading image
                $imageUpload = $this->saveImage($request['icon'], "icon");
                if ($imageUpload["status"] != 200) {
                    return response()->json(
                        $imageUpload["message"]
                        , $imageUpload["status"]);
                }
                $request['icon'] = $imageUpload["message"]["data"];

                $method = $this->paymentMethod->updatePaymentMethod($id, $request->all());

                $method->paymentMethodTranslation()->delete();

                foreach ($request['translation'] as $key => $translation) {
                    $translation['payment_method_id'] = $method->id;
                    $translation['lang'] = $key;
                    /**
                     * Validate lang and name fields.
                     * lang field is required and must be alphabets.
                     * name field is required.
                     * If validation fails, abort with validation error message.
                     * Else insert data in payment_method_translation table.
                     */
                    $rules = [
                        "lang" => 'required|alpha',
                        "name" => "required"
                    ];
                    $translation['lang'] = $this->slugify->slugify($translation['lang']);

                    $validator = Validator::make($translation, $rules);
                    if ($validator->fails()) {
                        $error = $validator->errors();
                        $this->logStoreHelper->storeLogError([
                            "Validation Error", [
                                'status' => '422',
                                "message" => [$key => $error]
                            ]
                        ]);
                        return response()->json([
                            'status' => '422',
                            "message" => [$key => $error]
                        ], 422);
                    }
                    /**
                     * Insert data into category_translation
                     */

                    $createTranslation[] = $this->paymentMethodTranslation->createPaymentMethodTranslation($translation);


                }
                $method['translation'] = $createTranslation;

                $this->logStoreHelper->storeLogInfo([
                    "Payment Method", [
                        "status" => "200",
                        "message" => "Payment Method Updated Successfully",
                        "data" => $method
                    ]
                ]);

                DB::commit();
                return response()->json([
                    "status" => "200",
                    "message" => "Payment Method updated Successfully",
                    'data' => $method
                ], 200);

            } catch (\Exception $e) {
                return response()->json([
                    "status" => "404",
                    "message" => $e->getMessage()
                ], 404);
            }

        } else {
            return response()->json([
                "status" => "401",
                "message" => "Only Authorized personnel can update payment method"
            ], 401);
        }

    }

    /**
     * Check if the logged in user is authorized.
     * Return true, if authorized, else return false.
     * @return bool
     */
    private function checkAuth()
    {
        return (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) ? true : false;

    }

    /**
     * Fetch all active payment methods to display for public.
     * @return \Illuminate\Http\JsonResponse
     */
    public function publicShowAll()
    {
        $request['lang'] = Input::get("lang", "en");

        /**
         * Hide cash on delivery & cash on pickup if filter is true;
         * show cash as payment method if filter is true
         */
        $payment_filter = Input::get("filter", false);
        $request['filter'] = ($payment_filter === 'true' ) ? true : false;
        try {

            $methods = $this->paymentMethod->getActivePaymentMethod();

            $forget_payment_method_keys = [];

            foreach ($methods as $key => $method) {
                if( $request['filter'] === true && ( $method['slug'] === 'cash-on-delivery' || $method['slug'] === 'cash-on-pickup' ) ){
                    $forget_payment_method_keys[] = $key;
                }
                $method->makeHidden(["class", "order_status", "status", "created_at", "updated_at"]);
                /**
                 * Get payment method Translation based on @param lang
                 * if empty record is obtained default en lang is returned
                 *If default en lang is not available, throw exception
                 * */
                $methodTranslation = $method->paymentMethodTranslation()->where('lang', $request['lang'])->get();
                try {
                    if ($methodTranslation->count() == 0) {
                        if ($request['lang'] == 'en') {
                            throw new \Exception();
                        } else {
                            $methodTranslation = $method->paymentMethodTranslation()->where('lang', 'en')->get();
                            if ($methodTranslation->count() == 0) {
                                throw new \Exception();
                            }
                        }

                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Default language english not found in database"
                    ], 404);
                }


                /*
                 * attach name and lang_code in $category variable
                 * */
                $method['name'] = $methodTranslation[0]['name'];
                $method['lang'] = $methodTranslation[0]['lang'];

            }

            try {
                if (!$methods->first()) {
                    throw  new \Exception();
                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Record"
                ], 404);
            }

            /**
             * Hide cash on delivery & cash on pickup if filter is true;
             * show cash as payment method if filter is true
             */
            if( $request['filter'] === true && count( $forget_payment_method_keys ) ){
                foreach( $forget_payment_method_keys as $forget_payment_method ){
                    $methods->forget( $forget_payment_method );
                }
                $methods = $methods->values()->all();

                $payment_method_cash = array(
                    'id' => 0,
                    'slug' => 'cash',
                    'type' => 'non-redirected',
                    'icon' => '',
                    'name' => ($request['lang'] === 'ar') ? 'كاش': 'Cash',
                    'lang' => $request['lang']
                );

                array_unshift( $methods, $payment_method_cash);
            }

            return response()->json([
                'status' => "200",
                "data" => $methods
            ], 200);

        } catch (\Exception $ex) {

            return response()->json([
                "status" => "404",
                "message" => "something went wrong"
            ], 404);
        }
    }

    /**
     * Fetch specific payment method for public.
     * Only active method is displayed.
     * If th method is inactive, it is considered 404 error.
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function publicShow($id)
    {
        try {
            $request['lang'] = Input::get("lang", "en");

            $method = $this->paymentMethod->getSpecificActivePaymentMethod($id);
            $method->makeHidden(["class", "order_status", "status", "created_at", "updated_at"]);
            /**
             * Get payment method Translation based on @param lang
             * if empty record is obtained default en lang is returned
             *If default en lang is not available, throw exception
             * */
            $methodTranslation = $method->paymentMethodTranslation()->where('lang', $request['lang'])->get();
            try {
                if ($methodTranslation->count() == 0) {
                    if ($request['lang'] == 'en') {
                        throw new \Exception();
                    } else {
                        $methodTranslation = $method->paymentMethodTranslation()->where('lang', 'en')->get();
                        if ($methodTranslation->count() == 0) {
                            throw new \Exception();
                        }
                    }

                }
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Default language english not found in database"
                ], 404);
            }


            /*
             * attach name and lang_code in $category variable
             * */
            $method['name'] = $methodTranslation[0]['name'];
            $method['lang'] = $methodTranslation[0]['lang'];


        } catch (\Exception $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Requested Payment Method could not be found"
            ], 404);
        }

        return response()->json([
            "status" => "200",
            "data" => $method
        ], 200);
    }

    /**
     * This is for the payment function after the customer checkout the order.
     * Currently four payment methods are processed.
     * The id of order and request parameter are sent.
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateOrder($id, Request $request)
    {

        try {
            /**
             * Fetch the order using order id.
             * Get the payment method using slug of payment stored in orders table.
             * Get namespace of class of payment method.
             *
             */

            $paymentMethod = $this->paymentMethod->getSpecificPaymentMethodBySlug($request['payment_method']);
            if (!$paymentMethod) {
                return response()->json([
                    "status" => "404",
                    "message" => "Payment Method not found."
                ], 404);
            }

            $class = $paymentMethod["class"];
            $namespace = "\\App\\Http\\Controllers\\PaymentClass\\" . $class;
            if (!class_exists($namespace)) {
                throw new ModelNotFoundException();
            }
            $payment = new  $namespace;

            /**
             * If payment method is non-redirected, update the orders row for specific order id.
             */
            if ($paymentMethod['type'] == "non-redirected") {

                return response()->json([
                    "status" => "200",
                    "message" => "non-redirected",
                    "data" => [
                        "payment_type" => "non-redirected",
                    ]
                ], 200);

                /**
                 * If payment method is redirected type, send required request parameters to the related service.
                 * If the class is creditcard, redirect to paytabs api.
                 * Else if the class is sadad, redirect to sadad api.
                 * Return the response of the api.
                 */
            } elseif ($paymentMethod['type'] == "redirected") {

                if ($class == "CreditCardMethod") {
                    $payment->setTitle($request['title']);
                    $payment->setCcFirstName($request['cc_first_name']);
                    $payment->setCcLastName($request['cc_last_name']);
                    $payment->setCcPhoneNumber($request['cc_phone_number']);
                    $payment->setPhoneNumber($request['phone_number']);
                    $payment->setEmail($request['email']);
                    $payment->setProductsPerTitle($request['products_per_title']);
                    $payment->setUnitPrice($request['unit_price']);
                    $payment->setQuantity($request['quantity']);
                    $payment->setOtherCharges($request['other_charges']);
                    $payment->setAmount($request['amount']);
                    $payment->setDiscount($request['discount']);
                    $payment->setCurrency($request['currency']);
                    $payment->setReferenceNo($request['reference_no']);
                    $payment->setIpCustomer($request['ip_customer']);
                    $payment->setIpMerchant($request['ip_merchant']);
                    $payment->setBillingAddress($request['billing_address']);
                    $payment->setCity($request['city']);
                    $payment->setPostalCode($request['postal_code']);
                    $payment->setCountry($request['country']);
                    $payment->setState($request['state']);
                    $payment->setAddressShipping($request['address_shipping']);
                    $payment->setStateShipping($request['state_shipping']);
                    $payment->setCityShipping($request['city_shipping']);
                    $payment->setCountryShipping($request['country_shipping']);
                    $payment->setPostalCodeShipping($request['postal_code_shipping']);
                    $payment->setMsgLang($request['msg_lang']);

                    $data = \Settings::getSettings('paytabs-return-url');
                    if ($data["status"] != 200) {
                        return response()->json(
                            $data["message"]
                            , $data["status"]);
                    }
                    $paytabs_return_url = $data['message']['data']['value'];
                    $returnUrl = $paytabs_return_url . "?payment_method=creditcard&order_id=$id";

//                    $returnUrl =  Config::get('config.paytabs.return_url')."?payment_method=creditcard&order_id=$id";
                    $payment->setRedirectUrl($returnUrl);
                    $payPage = $payment->createPayPage();
                    //return $payPage;
                    /**
                     * If response_code is 4012 set $status to 200 else 422.
                     */
                    //$status = ($payPage['message']['response_code'] == 4012)? 200:422 ;

                    $status = 422;
                    $paymentId = '';
                    $payment_url = '';

                    if ($payPage['message']['response_code'] == 4012) {
                        $order['payment_ref_id'] = $payPage['message']['p_id'];
                        $payment_url = $payPage['message']['payment_url'];

                        if ($this->order->updateOrder($id, $order)) {
                            $status = 200;
                            $paymentId = $payPage['message']['p_id'];
                        }
                    }

                    if (isset($payPage['message']['result'])) {
                        $message = $payPage['message']['result'];

                    } elseif (isset($payPage['message']['details'])) {
                        $message = $payPage['message']['details'];
                    } else {
                        $message = 'So';
                    }
                    return response()->json([
                        "status" => $status,
                        "message" => $message,
                        "data" => [
                            "payment_type" => "redirected",
                            "p_id" => $paymentId,
                            "payment_url" => $payment_url
                        ]
                    ], $status);

                }
            }


        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Class not found."
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "422",
                "message" => "Something went wrong. Please try again later."
                //"message" => $e->getMessage()
            ], 422);
        }
    }

    /**
     * This function is used to verify if the redirected payment methods are successful or not.
     * @param Request $request
     * @return mixed
     */
    public function verifyPayment(Request $request)
    {
        DB::beginTransaction();
        try {

            $this->validate($request, [
                "payment_method" => "required",
            ]);

        } catch (\Exception $e) {
            return response()->json([
                "status" => "422",
                "message" => $e->response->original
            ], 422);
        }
        $paytabStatus = "";
        $flag = false;
        try {
            if (trim($request['payment_method']) == 'creditcard') {
                try {
                    if ($request->has("use_sdk")) {
                        $this->validate($request, [
                            "transaction_id" => "required"

                        ]);
                    } else {
                        $this->validate($request, [
                            "payment_reference" => "required"

                        ]);
                    }
                } catch (\Exception $e) {
                    return response()->json([
                        "status" => "422",
                        "message" => $e->response->original
                    ], 422);
                }
                $namespace = "\\App\\Http\\Controllers\\PaymentClass\\CreditCardMethod";

                $payment = new $namespace;
                $sdk = false;
                if ($request->has("use_sdk")) {
                    $payment->setPaymentReference($request["transaction_id"]);
                    $sdk = true;
                } else {
                    $payment->setPaymentReference($request["payment_reference"]);
                }

                $verify = $payment->verifyPayment($sdk);
                if ($verify['message']['response_code'] == 100) {
                    //$order['order_status'] = "processed";
                    $order['payment_status'] = "completed";
                    $paytabStatus = "completed";
                    $orderId = (isset($verify['message']["reference_no"])) ? $verify['message']["reference_no"] : $verify['message']["order_id"];
                    $updatedOrder = $this->order->updateOrder($orderId, $order);

//                    $updatedOrder->makeVisible(["hash", "restaurant"]);
//                    $key = "identifier:" . $updatedOrder["hash"];
//                    Redis::DEL($key);
//                    Redis::DEL("user-" . $updatedOrder["customer_id"]);
//
//                    $updatedOrder = $updatedOrder->toArray();
//
//                    $updatedOrder['extra_info'] = unserialize($updatedOrder['extra_info']);
//                    $updatedOrder['restaurant'] = unserialize($updatedOrder['restaurant']);
//
//                    //return $updatedOrder;
//                    $array = [
//                        'service' => 'order service',
//                        'message' => 'order created',
//                        'data' => [
//                            'user_details' => ['id' => $updatedOrder['customer_id']],
//                            'order' => $updatedOrder,
//                            'shipping_address' => isset($updatedOrder['extra_info']['shipping_address']) ? $updatedOrder['extra_info']['shipping_address'] : '',
//                            'restaurant_id' => $updatedOrder['restaurant_id'],
//                            'restaurant_name' => $this->restaurantTranslate($updatedOrder['restaurant']['translation'], "en"),
//                            'parent_id' => $updatedOrder['restaurant']['parent_id'],
//
//                        ]
//                    ];
//
//                    event(new SendMessage($array));

//                $array = [
//                    'service' => 'order service',
//                    'message' => 'payment successful',
//                    'data' => [
//                        'user_details' => ['id' => $updatedOrder['customer_id']],
//                        'order' => $updatedOrder,
//                        'shipping_address' => isset($updatedOrder['extra_info']['shipping_details']) ? $updatedOrder['extra_info']['shipping_details'] : '',
//                        'restaurant_id' => $updatedOrder['restaurant_id'],
//                        'restaurant_name' => $this->restaurantTranslate($updatedOrder['restaurant']['translation'], "en"),
//                        'parent_id' => $updatedOrder['restaurant']['parent_id'],
//
//                    ]
//                ];
//                event(new SendMessage($array));

                    $status = 200;


                } else {
                    $order['payment_status'] = "failed";
                    $paytabStatus = "failed";
                    $errorMessage = $verify["message"]["result"];
                    $flag = true;
                    $orderId = (isset($verify['message']["reference_no"])) ? $verify['message']["reference_no"] : $verify['message']["order_id"];

                    $updatedOrder = $this->order->updateOrder($orderId, ["payment_status" => "failed",'order_status' => "cancelled",'delivery_status' => "cancelled"]);
//                    $updatedOrder->makeVisible(["hash", "restaurant"]);
//                    $updatedOrder = $updatedOrder->toArray();
//                    if ($updatedOrder) {
//                        $updatedOrder['restaurant'] = unserialize($updatedOrder['restaurant']);
//                        $updatedOrder['extra_info'] = unserialize($updatedOrder['extra_info']);
//
////                    $array = [
////                        'service' => 'order service',
////                        'message' => 'payment failed',
////                        'data' => [
////                            'user_details' => ['id' => $updatedOrder['customer_id']],
////                            'order' => $updatedOrder,
////                            'shipping_address' => isset($updatedOrder['extra_info']['shipping_details']) ? $updatedOrder['extra_info']['shipping_details'] : '',
////                            'restaurant_id' => $updatedOrder['restaurant_id'],
////                            'restaurant_name' => $this->restaurantTranslate($updatedOrder['restaurant']['translation'], "en"),
////                            'parent_id' => $updatedOrder['restaurant']['parent_id'],
////
////                        ]
////                    ];
////                    event(new SendMessage($array));
//
//                        $status = 422;
//
//
//                    }

                }
//            $baseUrl = Config::get("config.verifyPaymentFrontEndRedirect");
                $data = \Settings::getSettings('verify-payment-front-end-redirect-url');
                if ($data["status"] != 200) {
                    return response()->json(
                        $data["message"]
                        , $data["status"]);
                }
                $baseUrl = $data['message']['data']['value'];
                DB::commit();

                if ($flag) {
                    if ($request->has("use_sdk")) {
                        return response()->json([
                            "status" => "422",
                            "message" => $errorMessage
                        ], 422);
                    }
                    return redirect()->to("$baseUrl?status=$paytabStatus&message=$errorMessage");
                } else {
                    if ($request->has("use_sdk")) {
                        return response()->json([
                            "status" => "200",
                            "message" => "Payment completed successfully"
                        ], 200);
                    }
                    return redirect()->to("$baseUrl?status=$paytabStatus");
                }

//            $paymentId = $verify['message']['pt_invoice_id'];
//            return response()->json([
//                "status" => $status,
//                "message" => $verify['message']['result'],
//                "data" => [
//
//                    "payment_ref_id" => $paymentId
//                ]
//            ], $status);
//

            } else {
//            return redirect()->to('http://localhost:8080/payment/paytabs/response');
                $this->logStoreHelper->storeLogError("class not found", [
                    "status" => 404,
                    "message" => "Class not found"
                ]);
//            return redirect()->to("http://localhost:8080/payment/paytabs/response?status= $paytabStatus");
                return response()->json([
                    "status" => 404,
                    "message" => "Class not found",
                ], 404);
            }
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "404",
                "message" => $ex->getMessage()
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "500",
                "message" => "Error"
            ]);

        }
    }

    /**
     * Upload the image to folder of the given path and returns the path of the saved image
     * @param $image
     * @param $type
     * @return string
     */
    protected function saveImage($image, $type)
    {
        $imaged['avatar'] = $image;
        $imaged["type"] = $type;
        $imaged["service"] = "food";
        $imageUpload = ImageUploader::upload($imaged);
        return $imageUpload;
    }
    private function restaurantTranslate($resData, $lang)
    {
        if (isset($resData['en'])) {
            $resName = isset($resData[$lang]) ? $resData[$lang] : $resData['en'];
        } else {
            $resName = '';
        }
        return $resName;
    }

}