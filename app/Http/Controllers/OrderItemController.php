<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/21/2017
 * Time: 11:12 AM
 */

namespace App\Http\Controllers;

use App\Repo\DeliveryStatusInterface;
use App\Repo\OrderInterface;
use App\Repo\OrderItemAddonInterface;
use App\Repo\OrderItemInterface;
use App\Repo\OrderStatusInterface;
use App\Repo\PaymentMethodInterface;
use App\Repo\PaymentStatusInterface;
use App\Repo\ShippingMethodInterface;
use App\Repo\ShippingMethodTranslationInterface;
use Carbon\Carbon;
use CurrencyAttachHelper;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use RemoteCall;
use RoleChecker;

/**
 * Class OrderItemController
 * @package App\Http\Controllers
 */
class OrderItemController extends Controller
{


    /**
     * @var OrderInterface
     */
    protected $order;

    /**
     * @var OrderItemInterface
     */
    protected $orderItem;
    /**
     * @var OrderItemInterface
     */
    protected $orderItemAddon;
    /**
     * @var CurrencyAttachHelper
     */
    protected $currencyAttachHelper;
    /**
     * @var PaymentStatusInterface
     */
    protected $paymentStatus;
    /**
     * @var OrderStatusInterface
     */
    protected $orderStatus;

    protected $deliveryStatus;

    protected $shippingMethod;

    protected $shippingMethodTranslation;
    protected $paymentMethod;

    /**
     * OrderItemController constructor.
     * @param OrderInterface $order
     * @param PaymentStatusInterface $paymentStatus
     * @param OrderStatusInterface $orderStatus
     * @param CurrencyAttachHelper $currencyAttachHelper
     * @param OrderItemInterface $orderItem
     * @param OrderItemAddonInterface $orderItemAddon
     */
    public function __construct(OrderInterface $order,
                                PaymentMethodInterface $paymentMethod,
                                ShippingMethodInterface $shippingMethod,
                                ShippingMethodTranslationInterface $shippingMethodTranslation,
                                PaymentStatusInterface $paymentStatus,
                                OrderStatusInterface $orderStatus,
                                CurrencyAttachHelper $currencyAttachHelper,
                                OrderItemInterface $orderItem,
                                DeliveryStatusInterface $deliveryStatus,
                                OrderItemAddonInterface $orderItemAddon)
    {
        $this->orderItem = $orderItem;
        $this->orderItemAddon = $orderItem;
        $this->order = $order;
        $this->currencyAttachHelper = $currencyAttachHelper;
        $this->paymentStatus = $paymentStatus;
        $this->orderStatus = $orderStatus;
        $this->deliveryStatus = $deliveryStatus;
        $this->paymentMethod = $paymentMethod;
        $this->shippingMethod = $shippingMethod;
        $this->shippingMethodTranslation = $shippingMethodTranslation;
    }

    /**
     * return specific order items details
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewOrderItem($id)
    {
        $partialRefundResponse = [];
        $lang = Input::get("lang", "en");
        $showCategoryFlag = false;
        /**
         * Checks if user role admin or super admin
         *
         * delivery address, restaurant address, user detail including phone, email, order detail
         */
        $orderItemsToshow["item_count"] = 0;
        if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
            try {
                try {
                    $order = $this->order->getSpecificOrder($id);
                    $order->makeVisible("restaurant");
                    $restaurantData = unserialize($order->restaurant);
                    $order["order_date"] = Carbon::parse($order->created_at)->tz($restaurantData["timezone"]);
                    $order->setDefaultLanguage = $lang;
                    $order->makeVisible("delivery_time");
                    $delivery_time = $this->getDeliveryTime($order);
//                    unset( $order["delivery_time"]);
                    $order["delivery_time"] = $delivery_time;
                    $orderStatusLanguage = $this->getOrderStatusTransaction($order["order_status"], $lang);
                    $orderStatusSlugLanguage = $this->getOrderStatusSlugTransaction($order["order_status"]);
                    if ($orderStatusLanguage) {
                        $order["order_status"] = $orderStatusLanguage;
                        $order["order_status_slug"] = $orderStatusSlugLanguage;
                    }
                    $paymentStatusLanguage = $this->getPaymentStatusTransaction($order["payment_status"], $lang);
                    $paymentStatusSlugLanguage = $this->getPaymentStatusSlugTransaction($order["payment_status"]);

                    if ($paymentStatusLanguage) {
                        $order["payment_status"] = $paymentStatusLanguage;
                        $order["payment_status_slug"] = $paymentStatusSlugLanguage;
                    }
                    $deliveryStatusLanguage = $this->getDeliveryStatusTransaction($order["delivery_status"], $lang);
                    $deliveryStatusSlugLanguage = $this->getDeliveryStatusSlugTransaction($order["delivery_status"]);

                    if ($deliveryStatusLanguage) {
                        $order["delivery_status"] = $deliveryStatusLanguage;
                        $order["delivery_status_slug"] = $deliveryStatusSlugLanguage;
                    }
                    $paymentMethod = $this->getPaymentMethodTranslation($order["payment_method"], $lang);
                    if ($paymentMethod) {
                        $order["payment_method"] = $paymentMethod;
                    }
                    $shippingMethod = $this->getDeliverytMethodTranslation($order["delivery_type"], $lang);
                    if ($shippingMethod) {
                        $order["delivery_type"] = $shippingMethod;
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Order could not found"
                    ], 404);
                }
                /**
                 * gets order items based on order id from order item table
                 */
                $orderItems = $order->orderItem()->get();

                $orderItemCount = $orderItems->count();
                if ($orderItemCount == 0) {
                    throw  new \Exception();
                }
                /**
                 * setting @param $total to 0 by default
                 */

                $total = 0;

                foreach ($orderItems as $orderItem) {
                    $itemTotal = 0;
                    $addonTotal = 0;
                    /**
                     * gets order Item addon
                     */

                    $orderItemsToshow["restaurant_name"] = '';
                    $orderItemsToshow["phone_no"] = null;
                    $orderItemsToshow["mobile_no"] = null;
                    $orderItemsToshow["address"] = null;
                    $orderItemsToshow["order"] = $order;
                    /**
                     * get restaurant base url from Config/config.php
                     */
                    $restaurantId = $order->restaurant_id;
                    $restaurantBaseUrl = Config::get("config.restaurant_base_url");
                    $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$restaurantId/detail?lang=" . $lang;

                    /**
                     *calling restaurant service to check the restaurant exist or not if exist fetch the data
                     */

                    $restaurantData = RemoteCall::getSpecific($restaurantFullUrl);

                    if ($restaurantData['status'] != 200) {
                        $order->makeVisible("restaurant");
                        $restaurantData = unserialize($order["restaurant"]);
                        if ($restaurantData || is_null($restaurantData)) {
                            $orderItemsToshow["restaurant_name"] = $this->restaurantTranslate($restaurantData["translation"], $lang);
                            $orderItemsToshow["phone_no"] = $restaurantData["phone_no"];
                            $orderItemsToshow["mobile_no"] = $restaurantData["mobile_no"];
                            $orderItemsToshow["address"] = $restaurantData["address"] ?: null;
                            $orderItemsToshow["additional_contact"] = $restaurantData['additional_contact']?:[];
                        }
                        $order->makeHidden("restaurant", "extra_info");
                    } else {
                        $orderItemsToshow["restaurant_name"] = $restaurantData["message"]["data"]["name"];
                        $orderItemsToshow["phone_no"] = $restaurantData["message"]["data"]["phone_no"];
                        $orderItemsToshow["mobile_no"] = $restaurantData["message"]["data"]["mobile_no"];
                        $orderItemsToshow["address"] = $restaurantData["message"]["data"]["address"] ?: null;
                        $orderItemsToshow["additional_contact"] = $restaurantData["message"]["data"]['additional_contact']?:[];
                    }

                    /**
                     *calling location service to get shipping address data
                     */
                    $locationBaseUrl = Config::get("config.location_base_url");
                    $locationFullUrl = "$locationBaseUrl/public/shipping/address/" . $order->shipping_id . "?lang=" . $lang;
                    $locationData = RemoteCall::getSpecific($locationFullUrl);
                    if ($locationData['status'] != 200) {
                        $orderItemsToshow["shipping_address"] = null;
                    } else {
                        $shippingList['address_name'] = $locationData['message']['data']['address_name'];
                        $shippingList['street'] = $locationData['message']['data']['street'];
                        $shippingList['longitude'] = $locationData['message']['data']['longitude'];
                        $shippingList['latitude'] = $locationData['message']['data']['latitude'];
                        $shippingList['extra_direction'] = $locationData['message']['data']['extra_direction'];
                        $shippingList['type'] = $locationData['message']['data']['type'];
                        $shippingList['zip_code'] = $locationData['message']['data']['zip_code'];
                        $shippingList['floor_number'] = $locationData['message']['data']['floor_number'];
                        $shippingList['office_name'] = $locationData['message']['data']['office_name'];
                        $shippingList['district_name'] = $locationData['message']['data']['district_name'];
                        $shippingList['city_name'] = $locationData['message']['data']['city_name'];
                        $shippingList['country_name'] = $locationData['message']['data']['country_name'];
                        $orderItemsToshow["shipping_address"] = $shippingList;
                    }

                    /**
                     *calling oauth service to get shipping address data
                     */
                    $oauth_base_url = Config::get("config.oauth_base_url");
                    $userFullUrl = "$oauth_base_url/machine/user/" . $order->customer_id . "/detail";
                    $userData = RemoteCall::getSpecific($userFullUrl);
                    if ($userData['status'] != 200) {
                        $orderItemsToshow["user_detail"] = null;
                    } else {
                        $orderItemsToshow['user_detail'] = $userData['message']['data'];
                    }

                    $orderItemAddons = $orderItem->OrderItemAddon()->get();
                    /**
                     * loops through array of addons to calculate addon total
                     */
                    $tempArray = [];
                    foreach ($orderItemAddons as $orderItemAddon) {

                        $addonName = unserialize($orderItemAddon["addon_name"]);
                        $orderItemAddon["addon_name"] = $this->restaurantTranslate($addonName, $lang);
                        $tempArray[] = $orderItemAddon;
                        $addonTotal += $orderItemAddon["unit_price"];
                    }
                    $orderItem["addons"] = $tempArray;
                    /**
                     * gets specific food total price by adding addons unit cost and food unit cost
                     */
                    $orderItemsToshow["item_count"] += $orderItem["quantity"];
                    $itemTotal = $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                    $orderItem["unit_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItem["unit_price"]);
                    $orderItem["item_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $itemTotal);
                    $orderItem["item_price"] = $itemTotal;
                    $foodName = unserialize($orderItem["food_name"]);
                    $foodDetails = unserialize($orderItem->food_raw_data);
                    $orderItem["food_name"] = $this->restaurantTranslate($foodName, $lang);
                    $orderItem["is_adjustment"] = false;
                    $orderItem["special_instruction"] = (isset($foodDetails["special_instruction"])) ? $foodDetails["special_instruction"] : '';
                    $orderItem["food_size"] = (isset($foodDetails["size_title"])) ? $this->restaurantTranslate($foodDetails["size_title"], $lang) : "";
                    $orderItemsToshow["items"][] = $orderItem;
                    /**
                     * calculates total price for this order
                     */
                    $total += $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                }

//                if(isset($order["tax_type"]) && $order["tax_type"] != 'exclusive'){
//                    $orderItemsToshow["tax"] = round(($order["delivery_fee"] + $total) * $order->site_tax_rate * 0.01,2);
//                    $deliveryFeeTax = round(($order["delivery_fee"]) * $order->site_tax_rate * 0.01,2);
//                    $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
//                    $orderItemsToshow["subtotal"] = $total * (1 - $order->site_tax_rate * 0.01);;
//                    $orderItemsToshow["total"] = round($total + $deliveryFeeTax + $order->delivery_fee ,2);
//                }
//                else{
//                }
                $refunds = $order->partialRefunds;
                if (count($refunds) == 0) {
                    goto skipGettingUserInfo;
                }

                $userDatas["users"] = array_values($refunds->pluck("user_id")->all());
                $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list?lang=$lang";
                $getUserDetailsForReview = RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);
                if ($getUserDetailsForReview["status"] != "200") {
                    return response()->json([
                        "status" => $getUserDetailsForReview["status"],
                        "message" => $getUserDetailsForReview["message"]["message"]
                    ], $getUserDetailsForReview["status"]);
                }
                $partialRefundResponse = [];
                foreach ($refunds as $refund) {

                    $flag = true;
                    if ($refund->type == "minus") {
                        $flag = false;
                    }
                    $amount = ($flag) ? $refund->amount : -1 * $refund->amount;
                    $total += $amount;
                    $price = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $refund->amount);
                    $price = ($flag) ? $price : "-" . $price;
                    $refund = [
                        "id" => 0,
                        "adjustment_id" => $refund->id,
                        "order_id" => $refund->order_id,
//                            "user_id" => $refund->user_id,
                        "item_id" => 0,
//                        "id" => $refund->id,
                        "user_id" => $refund->user_id,
                        "food_name" => $refund->title,
                        "quantity" => 1,
                        "unit_price" => $amount,
                        "addons" => [],
                        "is_adjustment" => true,
                        "unit_price_currency" => $price,
                        "item_price_currency" => $price,
                        "item_price" => $amount,
                        "special_instruction" => "",
                        "food_size" => "",
                        "type" => $refund->type,
                        "vat" => $refund->vat,
                        "comment" => $refund->comment,
                        "food_image_url" => "null",
                        "size" => ""
                    ];
                    $refund["name"] = "";
                    foreach ($getUserDetailsForReview["message"]["data"] as $userDetails) {
                        if ($userDetails["id"] == $refund["user_id"]) {
                            $refund["name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($userDetails['first_name'], $userDetails['middle_name'], $userDetails['last_name']))));
                        }
                    }
                    $partialRefundResponse[] = $refund;

                }

//                $refunds->makeHidden(["user_id","comment"]);
                skipGettingUserInfo:
                $orderItemsToshow["items"] = array_merge($orderItemsToshow["items"], $partialRefundResponse);
//                $orderItemsToshow["partial_refund"] = $refunds;
                $orderItemsToshow["rejection_reason_id"] = $order->rejection_reason_id;
                $orderItemsToshow["rejection_reason"] = $order->rejection_reason;
                $orderItemsToshow["subtotal"] = $total;
                $orderItemsToshow["tax"] = round($order["tax"], 2);
                if(isset($order["user_type"]) &&  ($order["user_type"] == "restaurant-customer") && (0 == $order['is_normal'])){
                    $orderItemsToshow["delivery_charge"] = 0;
                    $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] , 2);
                }
                else {
                    $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
                    $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] + $order->delivery_fee, 2);
                }
                $orderItemsToshow["subtotal_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $total);
                $orderItemsToshow["tax_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItemsToshow["tax"]);
                $orderItemsToshow["delivery_charge_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"],  $orderItemsToshow["delivery_charge"]);
                $orderItemsToshow["total_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItemsToshow["total"]);
                return response()->json([
                    "status" => "200",
                    "data" => $orderItemsToshow
                ]);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Order Item not found"
                ], 404);
            }
        }
        elseif (RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")) {
            $restaurantInfo = RoleChecker::getRestaurantInfo();
            if (!is_null($restaurantInfo) && ($restaurantInfo["is_restaurant_admin"] == true)) {
                if (!is_null($restaurantInfo["branch_id"])) {
                    try {
                        $branchOrder = $this->order->getSpecificOrder($id);
                    } catch (ModelNotFoundException $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Order not found"
                        ], 404);
                    }
                    /**
                     * checks if restaurant id matches with the restaurant info which is returned from oauth server
                     */
                    $array = array_intersect([$branchOrder["restaurant_id"]], $restaurantInfo["branch_id"]);
                    if (empty($array)) {
                        return response()->json([
                            "status" => "403",
                            "message" => "You can view your restaurant order item only"
                        ], 403);
                    }
                    $order = $branchOrder;
                    $order->setDefaultLanguage = $lang;
                    $order->makeVisible("restaurant");
                    $restaurantData = unserialize($order->restaurant);
                    $order["order_date"] = Carbon::parse($order->created_at)->tz($restaurantData["timezone"]);
                    $order->makeVisible("delivery_time");
                    $delivery_time = $this->getDeliveryTime($order);
//                    unset( $order["delivery_time"]);
                    $order["delivery_time"] = $delivery_time;
                    $orderStatusLanguage = $this->getOrderStatusTransaction($order["order_status"], $lang);
                    $orderStatusSlugLanguage = $this->getOrderStatusSlugTransaction($order["order_status"]);
                    if ($orderStatusLanguage) {
                        $order["order_status"] = $orderStatusLanguage;
                        $order["order_status_slug"] = $orderStatusSlugLanguage;
                    }
                    $paymentStatusLanguage = $this->getPaymentStatusTransaction($order["payment_status"], $lang);
                    $paymentStatusSlugLanguage = $this->getPaymentStatusSlugTransaction($order["payment_status"]);

                    if ($paymentStatusLanguage) {
                        $order["payment_status"] = $paymentStatusLanguage;
                        $order["payment_status_slug"] = $paymentStatusSlugLanguage;
                    }
                    $deliveryStatusLanguage = $this->getDeliveryStatusTransaction($order["delivery_status"], $lang);
                    $deliveryStatusSlugLanguage = $this->getDeliveryStatusSlugTransaction($order["delivery_status"]);

                    if ($deliveryStatusLanguage) {
                        $order["delivery_status"] = $deliveryStatusLanguage;
                        $order["delivery_status_slug"] = $deliveryStatusSlugLanguage;
                    }
                    $paymentMethod = $this->getPaymentMethodTranslation($order["payment_method"], $lang);
                    if ($paymentMethod) {
                        $order["payment_method"] = $paymentMethod;
                    }
                    $shippingMethod = $this->getDeliverytMethodTranslation($order["delivery_type"], $lang);
                    if ($shippingMethod) {
                        $order["delivery_type"] = $shippingMethod;
                    }
                    $orderItems = $order->orderItem()->get();
                    $orderItemsCount = $orderItems->count();
                    $total = 0;
                    if ($orderItemsCount == 0) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Empty Order Item"
                        ], 404);
                    }
                    foreach ($orderItems as $orderItem) {
                        $itemTotal = 0;
                        $addonTotal = 0;
                        /**
                         * gets order Item addon
                         */

                        $orderItemsToshow["restaurant_name"] = '';
                        $orderItemsToshow["phone_no"] = null;
                        $orderItemsToshow["mobile_no"] = null;
                        $orderItemsToshow["order"] = $order;
//                        $orderItemsToshow["unit_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItem["unit_price"]);

                        /**
                         * get restaurant base url from Config/config.php
                         */
                        $restaurantId = $order->restaurant_id;
                        $restaurantBaseUrl = Config::get("config.restaurant_base_url");
                        $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$restaurantId/detail?lang=" . $lang;

                        /**
                         *calling restaurant service to check the restaurant exist or not if exist fetch the data
                         */

                        $restaurantData = RemoteCall::getSpecific($restaurantFullUrl);


                        if ($restaurantData['status'] != 200) {
                            $order->makeVisible("restaurant");
                            $restaurantData = unserialize($order["restaurant"]);
                            if ($restaurantData || is_null($restaurantData)) {
                                $orderItemsToshow["restaurant_name"] = $this->restaurantTranslate($restaurantData["translation"], $lang);;
                                $orderItemsToshow["phone_no"] = $restaurantData["phone_no"];
                                $orderItemsToshow["mobile_no"] = $restaurantData["mobile_no"];
                                $orderItemsToshow["address"] = $restaurantData["address"] ?: null;
                            }
                            $order->makeHidden("restaurant", "extra_info");
                        } else {
                            $orderItemsToshow["restaurant_name"] = $restaurantData["message"]["data"]["name"];
                            $orderItemsToshow["phone_no"] = $restaurantData["message"]["data"]["phone_no"];
                            $orderItemsToshow["mobile_no"] = $restaurantData["message"]["data"]["mobile_no"];
                            $orderItemsToshow["address"] = $restaurantData["message"]["data"]["address"] ?: null;
                        }
                        /**
                         *calling location service to get shipping address data
                         */
                        $locationBaseUrl = Config::get("config.location_base_url");
                        $locationFullUrl = "$locationBaseUrl/public/shipping/address/" . $order->shipping_id . "?lang=" . $lang;
                        $locationData = RemoteCall::getSpecific($locationFullUrl);
                        if ($locationData['status'] != 200) {
                            $orderItemsToshow["shipping_address"] = null;
                        } else {
                            $shippingList['address_name'] = $locationData['message']['data']['address_name'];
                            $shippingList['street'] = $locationData['message']['data']['street'];
                            $shippingList['longitude'] = $locationData['message']['data']['longitude'];
                            $shippingList['latitude'] = $locationData['message']['data']['latitude'];
                            $shippingList['extra_direction'] = $locationData['message']['data']['extra_direction'];
                            $shippingList['type'] = $locationData['message']['data']['type'];
                            $shippingList['zip_code'] = $locationData['message']['data']['zip_code'];
                            $shippingList['floor_number'] = $locationData['message']['data']['floor_number'];
                            $shippingList['office_name'] = $locationData['message']['data']['office_name'];
                            $shippingList['district_name'] = $locationData['message']['data']['district_name'];
                            $shippingList['city_name'] = $locationData['message']['data']['city_name'];
                            $shippingList['country_name'] = $locationData['message']['data']['country_name'];
                            $orderItemsToshow["shipping_address"] = $shippingList;
                        }

                        /**
                         *calling oauth service to get shipping address data
                         */
                        $oauth_base_url = Config::get("config.oauth_base_url");
                        $userFullUrl = "$oauth_base_url/machine/user/" . $order->customer_id . "/detail";
                        $userData = RemoteCall::getSpecific($userFullUrl);
                        if ($userData['status'] != 200) {
                            $orderItemsToshow["user_detail"] = null;
                        } else {
                            $orderItemsToshow['user_detail'] = $userData['message']['data'];
                        }

                        $orderItemAddons = $orderItem->OrderItemAddon()->get();
                        $tempArray = [];
                        foreach ($orderItemAddons as $orderItemAddon) {

                            $addonName = unserialize($orderItemAddon["addon_name"]);
                            $orderItemAddon["addon_name"] = $this->restaurantTranslate($addonName, $lang);
                            $tempArray[] = $orderItemAddon;
                            $addonTotal += $orderItemAddon["unit_price"];
                        }
                        $orderItem["addons"] = $tempArray;
                        $orderItemsToshow["item_count"] += $orderItem["quantity"];
                        $itemTotal = $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                        $orderItem["item_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $itemTotal);
                        $orderItem["unit_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItem["unit_price"]);
                        $orderItem["item_price"] = $itemTotal;
                        $foodName = unserialize($orderItem["food_name"]);
                        $orderItem["food_name"] = $this->restaurantTranslate($foodName, $lang);
                        $orderItem["is_adjustment"] = false;
                        $foodDetails = unserialize($orderItem->food_raw_data);
                        $orderItem["special_instruction"] = (isset($foodDetails["special_instruction"])) ? $foodDetails["special_instruction"] : '';
                        $orderItem["food_size"] = (isset($foodDetails["size_title"])) ? $this->restaurantTranslate($foodDetails["size_title"], $lang) : "";
                        $orderItemsToshow["items"][] = $orderItem;
                        $total += $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                    }


//                    if(isset($order["tax_type"]) && $order["tax_type"] != 'exclusive'){
//                        $orderItemsToshow["tax"] = round(($order["delivery_fee"] + $total) * $order->site_tax_rate * 0.01,2);
//                        $deliveryFeeTax = round(($order["delivery_fee"]) * $order->site_tax_rate * 0.01,2);
//                        $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
//                        $orderItemsToshow["subtotal"] = $total * (1 - $order->site_tax_rate * 0.01);;
//                        $orderItemsToshow["total"] = round($total + $deliveryFeeTax + $order->delivery_fee ,2);
//                    }
//                    else{

//                    }
                    $refunds = $order->partialRefunds;
                    $partialRefundResponse = [];
                    foreach ($refunds as $refund) {
                        $flag = true;
                        if ($refund->type == "minus") {
                            $flag = false;
                        }
                        $amount = ($flag) ? $refund->amount : -1 * $refund->amount;
                        $total += $amount;
                        $price = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $refund->amount);
                        $price = ($flag) ? $price : "-" . $price;
                        $refund = [
                            "id" => 0,
                            "adjustment_id" => $refund->id,
                            "order_id" => $refund->order_id,
//                            "user_id" => $refund->user_id,
                            "item_id" => 0,
                            "food_name" => $refund->title,
                            "quantity" => 1,
                            "unit_price" => $amount,
                            "addons" => [],
                            "is_adjustment" => true,
                            "unit_price_currency" => $price,
                            "item_price_currency" => $price,
                            "item_price" => $amount,
                            "special_instruction" => "",
                            "food_size" => "",
                            "type" => $refund->type,
                            "vat" => $refund->vat,
                            "food_image_url" => "null",
                            "size" => ""
//                            "comment" => $refund->comment
                        ];
                        $partialRefundResponse[] = $refund;

                    }
                    $refunds->makeHidden(["user_id", "comment"]);
                    $orderItemsToshow["items"] = array_merge($orderItemsToshow["items"], $partialRefundResponse);
//                    $orderItemsToshow["partial_refund"] = $refunds;
                    $orderItemsToshow["rejection_reason_id"] = $order->rejection_reason_id;
                    $orderItemsToshow["rejection_reason"] = $order->rejection_reason;
                    $orderItemsToshow["subtotal"] = $total;
                    $orderItemsToshow["tax"] = round($order["tax"], 2);
                    if(isset($order["user_type"]) &&  ($order["user_type"] == "restaurant-customer")  && (0 == $order['is_normal'])){
                        $orderItemsToshow["delivery_charge"] = 0;
                        $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] , 2);
                    }
                    else {
                        $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
                        $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] + $order->delivery_fee, 2);
                    }
                    $orderItemsToshow["subtotal_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $total);
                    $orderItemsToshow["tax_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItemsToshow["tax"]);
                    $orderItemsToshow["delivery_charge_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"],  $orderItemsToshow["delivery_charge"]);
                    $orderItemsToshow["total_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItemsToshow["total"]);
                    return response()->json([
                        "status" => "200",
                        "data" => $orderItemsToshow
                    ]);


                }
            } else {
                /**
                 * this means there is some modification done in oauth restaurant_meta_table .
                 * and  is_restaurant_admin is false
                 * */
                return response()->json([
                    "status" => "403",
                    "message" => "Forbidden"
                ], 403);
            }
        }
        elseif (RoleChecker::hasRole("branch-admin") || RoleChecker::hasRole("branch-operator") || (RoleChecker::get('parentRole') == "branch-admin" || RoleChecker::get('parentRole') == "branch-operator")) {
            $restaurantInfo = RoleChecker::getRestaurantInfo();
            if (!is_null($restaurantInfo) && !is_null($restaurantInfo["branch_id"]) && ($restaurantInfo["is_restaurant_admin"] == false)) {
                /*
                 * gets order based on restaurant id and order id
                 */
//                if(RoleChecker::hasRole("branch-operator") || RoleChecker::get('parentRole') == "branch-operator"){
//                    $showCategoryFlag = true;
//                }

                $order = $this->order->getOrdersByRestaurantAndOrderId($restaurantInfo["branch_id"], $id);
                if (count($order) == 0) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Empty Order Item"
                    ], 404);
                }
                $order->setDefaultLanguage = $lang;
                $order->makeVisible("restaurant");
                $restaurantData = unserialize($order->restaurant);
                $order["order_date"] = Carbon::parse($order->created_at)->tz($restaurantData["timezone"]);
                $order->makeVisible("delivery_time");
                $delivery_time = $this->getDeliveryTime($order);
//                    unset( $order["delivery_time"]);
                $order["delivery_time"] = $delivery_time;
                //TODO
                $orderStatusLanguage = $this->getOrderStatusTransaction($order["order_status"], $lang);
                $orderStatusSlugLanguage = $this->getOrderStatusSlugTransaction($order["order_status"]);
                if ($orderStatusLanguage) {
                    $order["order_status"] = $orderStatusLanguage;
                    $order["order_status_slug"] = $orderStatusSlugLanguage;
                }
                $paymentStatusLanguage = $this->getPaymentStatusTransaction($order["payment_status"], $lang);
                $paymentStatusSlugLanguage = $this->getPaymentStatusSlugTransaction($order["payment_status"]);

                if ($paymentStatusLanguage) {
                    $order["payment_status"] = $paymentStatusLanguage;
                    $order["payment_status_slug"] = $paymentStatusSlugLanguage;
                }
                $deliveryStatusLanguage = $this->getDeliveryStatusTransaction($order["delivery_status"], $lang);
                $deliveryStatusSlugLanguage = $this->getDeliveryStatusSlugTransaction($order["delivery_status"]);

                if ($deliveryStatusLanguage) {
                    $order["delivery_status"] = $deliveryStatusLanguage;
                    $order["delivery_status_slug"] = $deliveryStatusSlugLanguage;
                }
                $paymentMethod = $this->getPaymentMethodTranslation($order["payment_method"], $lang);
                $order["payment_method_slug"] = $order["payment_method"];
                if ($paymentMethod) {
                    $order["payment_method"] = $paymentMethod;
                }
                $shippingMethod = $this->getDeliverytMethodTranslation($order["delivery_type"], $lang);
                if ($shippingMethod) {
                    $order["delivery_type"] = $shippingMethod;
                }
                if (!$order) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Order not found"
                    ], 404);
                }
                $orderItems = $order->orderItem()->get();
                $orderItemsCount = $orderItems->count();
                $total = 0;
                if ($orderItemsCount == 0) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Empty Order Item"
                    ], 404);
                }
                foreach ($orderItems as $orderItem) {
                    $itemTotal = 0;
                    $addonTotal = 0;
                    /**
                     * gets order Item addon
                     */

                    $orderItemsToshow["restaurant_name"] = '';
                    $orderItemsToshow["phone_no"] = null;
                    $orderItemsToshow["mobile_no"] = null;
                    $orderItemsToshow["order"] = $order;
//                    $orderItemsToshow["unit_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItem["unit_price"]);

                    /**
                     * get restaurant base url from Config/config.php
                     */
                    $restaurantId = $order->restaurant_id;
                    $restaurantBaseUrl = Config::get("config.restaurant_base_url");
                    $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$restaurantId/detail?lang=" . $lang;

                    /**
                     *calling restaurant service to check the restaurant exist or not if exist fetch the data
                     */

                    $restaurantData = RemoteCall::getSpecific($restaurantFullUrl);


                    if ($restaurantData['status'] != 200) {
                        $order->makeVisible("restaurant");
                        $restaurantData = unserialize($order["restaurant"]);
                        if ($restaurantData || is_null($restaurantData)) {
                            $orderItemsToshow["restaurant_name"] = $this->restaurantTranslate($restaurantData["translation"], $lang);;
                            $orderItemsToshow["phone_no"] = $restaurantData["phone_no"];
                            $orderItemsToshow["mobile_no"] = $restaurantData["mobile_no"];
                            $orderItemsToshow["address"] = $restaurantData["address"] ?: null;
                        }
                        $order->makeHidden("restaurant", "extra_info");
                    } else {
                        $orderItemsToshow["restaurant_name"] = $restaurantData["message"]["data"]["name"];
                        $orderItemsToshow["phone_no"] = $restaurantData["message"]["data"]["phone_no"];
                        $orderItemsToshow["mobile_no"] = $restaurantData["message"]["data"]["mobile_no"];
                        $orderItemsToshow["address"] = $restaurantData["message"]["data"]["address"] ?: null;
                    }
                    /**
                     *calling location service to get shipping address data
                     */
                    $locationBaseUrl = Config::get("config.location_base_url");
                    $locationFullUrl = "$locationBaseUrl/public/shipping/address/" . $order->shipping_id . "?lang=" . $lang;
                    $locationData = RemoteCall::getSpecific($locationFullUrl);
                    if ($locationData['status'] != 200) {
                        $orderItemsToshow["shipping_address"] = null;
                    } else {
                        $shippingList['address_name'] = $locationData['message']['data']['address_name'];
                        $shippingList['street'] = $locationData['message']['data']['street'];
                        $shippingList['longitude'] = $locationData['message']['data']['longitude'];
                        $shippingList['latitude'] = $locationData['message']['data']['latitude'];
                        $shippingList['extra_direction'] = $locationData['message']['data']['extra_direction'];
                        $shippingList['type'] = $locationData['message']['data']['type'];
                        $shippingList['zip_code'] = $locationData['message']['data']['zip_code'];
                        $shippingList['floor_number'] = $locationData['message']['data']['floor_number'];
                        $shippingList['office_name'] = $locationData['message']['data']['office_name'];
                        $shippingList['district_name'] = $locationData['message']['data']['district_name'];
                        $shippingList['city_name'] = $locationData['message']['data']['city_name'];
                        $shippingList['country_name'] = $locationData['message']['data']['country_name'];
                        $orderItemsToshow["shipping_address"] = $shippingList;
                    }

                    /**
                     *calling oauth service to get shipping address data
                     */
                    $oauth_base_url = Config::get("config.oauth_base_url");
                    $userFullUrl = "$oauth_base_url/machine/user/" . $order->customer_id . "/detail";
                    $userData = RemoteCall::getSpecific($userFullUrl);
                    if ($userData['status'] != 200) {
                        $orderItemsToshow["user_detail"] = null;
                    } else {
                        $orderItemsToshow['user_detail'] = $userData['message']['data'];
                    }
                    $orderItemAddons = $orderItem->OrderItemAddon()->get();
                    $tempArray = [];
                    foreach ($orderItemAddons as $orderItemAddon) {

                        $addonName = unserialize($orderItemAddon["addon_name"]);
                        $orderItemAddon["addon_name"] = $this->restaurantTranslate($addonName, $lang);
                        $tempArray[] = $orderItemAddon;
                        $addonTotal += $orderItemAddon["unit_price"];
                    }
                    $orderItem["addons"] = $tempArray;
                    $orderItemsToshow["item_count"] += $orderItem["quantity"];
                    $itemTotal = $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                    $orderItem["item_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $itemTotal);
                    $orderItem["unit_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItem["unit_price"]);
                    $orderItem["item_price"] = $itemTotal;
                    $foodName = unserialize($orderItem["food_name"]);
                    $orderItem["food_name"] = $this->restaurantTranslate($foodName, $lang);
                    $orderItem["is_adjustment"] = false;
                    $foodDetails = unserialize($orderItem->food_raw_data);
                    $orderItem["food_size"] = (isset($foodDetails["size_title"])) ? $this->restaurantTranslate($foodDetails["size_title"], $lang) : "";
                    $orderItem["special_instruction"] = (isset($foodDetails["special_instruction"])) ? $foodDetails["special_instruction"] : '';
//                    $orderItem["addons"] = $orderItemAddons;
                    $orderItemsToshow["items"][] = $orderItem;
                    $total += $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                }


//                if(isset($order["tax_type"]) && $order["tax_type"] != 'exclusive'){
//                    $orderItemsToshow["tax"] = round(($order["delivery_fee"] + $total) * $order->site_tax_rate * 0.01,2);
//                    $deliveryFeeTax = round(($order["delivery_fee"]) * $order->site_tax_rate * 0.01,2);
//                    $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
//                    $orderItemsToshow["subtotal"] = $total * (1 - $order->site_tax_rate * 0.01);;
//                    $orderItemsToshow["total"] = round($total + $deliveryFeeTax + $order->delivery_fee ,2);
//                }
//                else{
//                }
                $partialRefundResponse = [];
                $refunds = $order->partialRefunds;
                foreach ($refunds as $refund) {
                    $flag = true;
                    if ($refund->type == "minus") {
                        $flag = false;
                    }
                    $price = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $refund->amount);
                    $amount = ($flag) ? $refund->amount : -1 * $refund->amount;
                    $total += $amount;
                    $price = ($flag) ? $price : "-" . $price;
                    $refund = [
                        "id" => 0,
                        "adjustment_id" => $refund->id,
                        "order_id" => $refund->order_id,
//                            "user_id" => $refund->user_id,
                        "item_id" => 0,
                        "food_name" => $refund->title,
                        "quantity" => 1,
                        "unit_price" => $amount,
                        "addons" => [],
                        "is_adjustment" => true,
                        "unit_price_currency" => $price,
                        "item_price_currency" => $price,
                        "item_price" => $amount,
                        "special_instruction" => "",
                        "food_size" => "",
                        "type" => $refund->type,
                        "vat" => $refund->vat,
                        "food_image_url" => "null",
                        "size" => ""
//                            "comment" => $refund->comment
                    ];
                    $partialRefundResponse[] = $refund;

                }
                $refunds->makeHidden(["user_id", "comment"]);
                if (!$showCategoryFlag) {
                    goto skipOrderingByCategories;
                }
                $foodIdList["food_ids"] = collect($orderItemsToshow["items"])->pluck("item_id")->unique()->all();
                //caling restaurant service for getting categories

                $getFoodWithCategories = RemoteCall::storeWithoutOauth($restaurantBaseUrl . "/foods/category", $foodIdList);
                if ($getFoodWithCategories["status"] != "200") {
                    return response()->json([
                        "status" => $getFoodWithCategories["status"],
                        "message" => $getFoodWithCategories["message"]["message"]
                    ], $getFoodWithCategories["status"]);
                }
                $orderItemsToshowTempValue["items"] = array_merge($orderItemsToshow["items"], $partialRefundResponse);
                $orderItemsToshow["items"] = [];
                foreach ($orderItemsToshowTempValue["items"] as $item) {
                    if ($item["is_adjustment"]) {
                        $orderItemsToshow["items"]["adjustment"][] = $item;
                    } else {
                        foreach ($getFoodWithCategories["message"]["data"] as $food) {
                            if ($food["id"] == $item["item_id"]) {
                                $orderItemsToshow["items"][$food["name"]][] = $item;
                            }
                        }
                    }
                }
//                    $orderItemsToshow["partial_refund"] = $refunds;
                skipOrderingByCategories:
                $orderItemsToshow["rejection_reason_id"] = $order->rejection_reason_id;
                $orderItemsToshow["rejection_reason"] = $order->rejection_reason;
                $orderItemsToshow["subtotal"] = $total;
                $orderItemsToshow["tax"] = round($order["tax"], 2);
                if(isset($order["user_type"]) &&  ($order["user_type"] == "restaurant-customer")  && (0 == $order['is_normal'])){
                    $orderItemsToshow["delivery_charge"] = 0;
                    $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] , 2);
                }
                else {
                    $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
                    $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] + $order->delivery_fee, 2);
                }
                $orderItemsToshow["subtotal_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $total);
                $orderItemsToshow["tax_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItemsToshow["tax"]);
                $orderItemsToshow["delivery_charge_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"],  $orderItemsToshow["delivery_charge"]);
                $orderItemsToshow["total_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItemsToshow["total"]);
                return response()->json([
                    "status" => "200",
                    "data" => $orderItemsToshow
                ]);

            } else {
                /**
                 * this means there is some modification done in oauth restaurant_meta_table .
                 * and  is_restaurant_admin is true if you are branch-admin
                 * */
                return response()->json([
                    "status" => "403",
                    "message" => "Forbidden."
                ], 403);
            }
        }
        elseif (RoleChecker::hasRole("customer") || RoleChecker::hasRole("restaurant-customer")) {
            try {
                $order = $this->order->getSpecificOrder($id);
                $order->setDefaultLanguage = $lang;
                $order->makeVisible("restaurant");
                $restaurantData = unserialize($order->restaurant);
                $order["order_date"] = Carbon::parse($order->created_at)->tz($restaurantData["timezone"]);
                $order->makeVisible("delivery_time");
                $delivery_time = $this->getDeliveryTime($order);
                $orderItemsToshow["delivery_time"] = $delivery_time;
                $orderItemsToshow["pre_order"] = $order->pre_order;
                $extraInfo = unserialize($order["extra_info"]);
                $orderStatusLanguage = $this->getOrderStatusTransaction($order["order_status"], $lang);
                $orderStatusSlugLanguage = $this->getOrderStatusSlugTransaction($order["order_status"]);
                $customerOrder["order_status"] = $order["order_status"];
                $customerOrder["payment_status"] = $order["payment_status"];

                if ($orderStatusLanguage) {
                    $customerOrder["order_status"] = $orderStatusLanguage;
                }
                if ($orderStatusSlugLanguage) {
                    $customerOrder["order_status_slug"] = $orderStatusSlugLanguage;
                }
                $paymentStatusLanguage = $this->getPaymentStatusTransaction($order["payment_status"], $lang);
                $paymentStatusSlugLanguage = $this->getPaymentStatusSlugTransaction($order["payment_status"]);
                if ($paymentStatusLanguage) {
                    $customerOrder["payment_status"] = $paymentStatusLanguage;
                }
                if ($paymentStatusSlugLanguage) {
                    $customerOrder["payment_status_slug"] = $paymentStatusSlugLanguage;
                }
                $customerOrder["delivery_status"] = $order["delivery_status"];
                $deliveryStatusLanguage = $this->getDeliveryStatusTransaction($order["delivery_status"], $lang);
                $deliveryStatusSlugLanguage = $this->getDeliveryStatusSlugTransaction($order["delivery_status"]);
                if ($deliveryStatusLanguage) {
                    $customerOrder["delivery_status"] = $deliveryStatusLanguage;
                }
                if ($deliveryStatusSlugLanguage) {
                    $customerOrder["delivery_status_slug"] = $deliveryStatusSlugLanguage;
                }
                $customerOrder["delivery_method"] = $order["delivery_type"];
                $customerOrder["delivery_method_name"] = $this->getDeliverytMethodTranslation($order["delivery_type"], $lang);

                $customerOrder["payment_method"] = $order["payment_method"];
                $customerOrder["payment_method_name"] = $this->getPaymentMethodTranslation($order["payment_method"], $lang);
                /**
                 * checks if the order belongs to logged in user or not
                 */
                if ($order->customer_id != RoleChecker::getuser()) {
                    return response()->json([
                        "status" => "403",
                        "message" => "You are only allowed to view your order item only"
                    ], 403);
                }
            } catch (ModelNotFoundException $exception) {
                return response()->json([
                    "status" => "404",
                    "message" => "Order not found "
                ], 404);
            }
            $orderItems = $order->orderItem()->get();
            $orderItemsCount = $orderItems->count();
            $total = 0;
            if ($orderItemsCount == 0) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Order Item"
                ], 404);
            }
            $orderItemsToshow["item_count"] = 0;
            foreach ($orderItems as $orderItemKey => $orderItem) {
                $itemTotal = 0;
                $addonTotal = 0;
                /**
                 * gets order Item addon
                 */

                $orderItemsToshow["restaurant_name"] = '';
                $orderItemsToshow["phone_no"] = null;
                $orderItemsToshow["mobile_no"] = null;


                /**
                 * get restaurant base url from Config/config.php
                 */
                $restaurantId = $order->restaurant_id;
                $restaurantBaseUrl = Config::get("config.restaurant_base_url");
                $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$restaurantId/detail?lang=" . $lang;

                /**
                 *calling restaurant service to check the restaurant exist or not if exist fetch the data
                 */

                $restaurantData = RemoteCall::getSpecific($restaurantFullUrl);

                if ($restaurantData['status'] != 200) {
                    $order->makeVisible("restaurant");
                    $restaurantData = unserialize($order["restaurant"]);
                    if ($restaurantData || is_null($restaurantData)) {
                        $orderItemsToshow["restaurant_name"] = $this->restaurantTranslate($restaurantData["translation"], $lang);;
                        $orderItemsToshow["phone_no"] = $restaurantData["phone_no"];
                        $orderItemsToshow["mobile_no"] = $restaurantData["mobile_no"];
                        $orderItemsToshow["restaurant_slug"] = $restaurantData["slug"];
                        $orderItemsToshow["address"] = $restaurantData["address"] ?: null;
                        $orderItemsToshow['created_at'] = Carbon::parse($order["created_at"])->tz($restaurantData["timezone"])->format("Y-m-d h:i A");
                    }
                    $order->makeHidden("restaurant", "extra_info");
                } else {
                    $orderItemsToshow["restaurant_name"] = $restaurantData["message"]["data"]["name"];
                    $orderItemsToshow["phone_no"] = $restaurantData["message"]["data"]["phone_no"];
                    $orderItemsToshow["mobile_no"] = $restaurantData["message"]["data"]["mobile_no"];
                    $orderItemsToshow["address"] = $restaurantData["message"]["data"]["address"] ?: null;
                    $orderItemsToshow["restaurant_slug"] = $restaurantData["message"]["data"]["slug"];
//                    return $order["created_at"];
                    $orderItemsToshow['created_at'] = Carbon::parse($order["created_at"])->tz($restaurantData["message"]["data"]["timezone"])->format("Y-m-d h:i A");;

                }

                /**
                 *calling location service to get shipping address data
                 */
                $locationBaseUrl = Config::get("config.location_base_url");
                $locationFullUrl = "$locationBaseUrl/public/shipping/address/" . $order->shipping_id . "?lang=" . $lang;
                $locationData = RemoteCall::getSpecific($locationFullUrl);
                if ($locationData['status'] != 200) {
                    $orderItemsToshow["shipping_address"] = null;
                } else {
                    $shippingList['address_name'] = $locationData['message']['data']['address_name'];
                    $shippingList['street'] = $locationData['message']['data']['street'];
                    $shippingList['longitude'] = $locationData['message']['data']['longitude'];
                    $shippingList['latitude'] = $locationData['message']['data']['latitude'];
                    $shippingList['extra_direction'] = $locationData['message']['data']['extra_direction'];
                    $shippingList['type'] = $locationData['message']['data']['type'];
                    $shippingList['zip_code'] = $locationData['message']['data']['zip_code'];
                    $shippingList['floor_number'] = $locationData['message']['data']['floor_number'];
                    $shippingList['office_name'] = $locationData['message']['data']['office_name'];
                    $shippingList['district_name'] = $locationData['message']['data']['district_name'];
                    $shippingList['city_name'] = $locationData['message']['data']['city_name'];
                    $shippingList['country_name'] = $locationData['message']['data']['country_name'];
                    $orderItemsToshow["shipping_address"] = $shippingList;
                }

                /**
                 *calling oauth service to get shipping address data
                 */
                $oauth_base_url = Config::get("config.oauth_base_url");
                $userFullUrl = "$oauth_base_url/machine/user/" . $order->customer_id . "/detail";
                $userData = RemoteCall::getSpecific($userFullUrl);
                if ($userData['status'] != 200) {
                    $orderItemsToshow["user_detail"] = null;
                } else {
                    $orderItemsToshow['user_detail'] = $userData['message']['data'];
                }

                $orderItemAddons = $orderItem->OrderItemAddon()->get();
                $tempArray = [];

                foreach ($orderItemAddons as $addonKey => $orderItemAddon) {

                    $addonName = unserialize($orderItemAddon["addon_name"]);
                    if (isset($extraInfo["items"][$orderItemKey]["addons"][$addonKey])) {
                        $orderItemAddon["category_id"] = (isset($extraInfo["items"][$orderItemKey]["addons"][$addonKey]["category_id"])) ? $extraInfo["items"][$orderItemKey]["addons"][$addonKey]["category_id"] : "";
                        if (isset($extraInfo["items"][$orderItemKey]["addons"][$addonKey]["category_name"])) {
                            $orderItemAddon["category_name"] = $this->restaurantTranslate($extraInfo["items"][$orderItemKey]["addons"][$addonKey]["category_name"], $lang);
                        } else {
                            $orderItemAddon["category_name"] = $orderItemAddon["category_name"] = "";
                        }

                    } else {
                        $orderItemAddon["category_id"] = $orderItemAddon["category_name"] = "";
                    }
                    $orderItemAddon["addon_name"] = $this->restaurantTranslate($addonName, $lang);
                    $tempArray[] = $orderItemAddon;
                    $addonTotal += $orderItemAddon["unit_price"];
                }
                $orderItem["addons"] = $tempArray;
                $orderItemsToshow["item_count"] += $orderItem["quantity"];
                $itemTotal = $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                $orderItem["item_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $itemTotal);
                $orderItem["item_price"] = $itemTotal;
                $foodName = unserialize($orderItem["food_name"]);
                $orderItem["food_name"] = $this->restaurantTranslate($foodName, $lang);
                $orderItem["is_adjustment"] = false;
                $orderItem["food_image_url"] = "";
                $foodDetails = unserialize($orderItem->food_raw_data);
                $orderItem["food_size"] = (isset($foodDetails["size_title"])) ? $this->restaurantTranslate($foodDetails["size_title"], $lang) : "";
                $orderItem["special_instruction"] = (isset($foodDetails["special_instruction"])) ? $foodDetails["special_instruction"] : '';
                $orderItem["size"] = (isset($foodDetails["size_title"])) ? $this->restaurantTranslate($foodDetails["size_title"], $lang) : '';
                if (isset($foodDetails["image_url"]))
                    $orderItem["food_image_url"] = $foodDetails["image_url"];
//                $orderItem["addons"] = $orderItemAddons;
                $orderItemsToshow["items"][] = $orderItem;

                $total += $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
            }


//            if(isset($order["tax_type"]) && $order["tax_type"] != 'exclusive'){
//                $orderItemsToshow["tax"] = round(($order["delivery_fee"] + $total) * $order->site_tax_rate * 0.01,2);
//                $deliveryFeeTax = round(($order["delivery_fee"]) * $order->site_tax_rate * 0.01,2);
//                $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
//                $orderItemsToshow["subtotal"] = $total * (1 - $order->site_tax_rate * 0.01);;
//                $orderItemsToshow["total"] = round($total + $deliveryFeeTax + $order->delivery_fee ,2);
//            }
//            else{

//            }
            $refunds = $order->partialRefunds;
            $partialRefundResponse = [];
            foreach ($refunds as $refund) {
                $flag = true;
                if ($refund->type == "minus") {
                    $flag = false;
                }
                $price = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $refund->amount);
                $amount = ($flag) ? $refund->amount : -1 * $refund->amount;
                $total += $amount;
                $price = ($flag) ? $price : "-" . $price;
                $refund = [
                    "id" => 0,
                    "adjustment_id" => $refund->id,
                    "order_id" => $refund->order_id,
//                            "user_id" => $refund->user_id,
                    "item_id" => 0,
                    "food_name" => $refund->title,
                    "quantity" => 1,
                    "unit_price" => $amount,
                    "addons" => [],
                    "is_adjustment" => true,
                    "unit_price_currency" => $price,
                    "item_price_currency" => $price,
                    "item_price" => $amount,
                    "special_instruction" => "",
                    "food_size" => "",
                    "type" => $refund->type,
                    "vat" => $refund->vat,
                    "food_image_url" => "null",
                    "size" => ""
//                            "comment" => $refund->comment
                ];
                $partialRefundResponse[] = $refund;

            }
            $refunds->makeHidden(["user_id", "comment"]);
            $orderItemsToshow["items"] = array_merge($orderItemsToshow["items"], $partialRefundResponse);
            $orderItemsToshow["rejection_reason"] = $order->rejection_reason;
//                    $orderItemsToshow["partial_refund"] = $refunds;
            $orderItemsToshow["subtotal"] = $total;
            $orderItemsToshow["currency"] = $order["currency"];
            $orderItemsToshow["tax"] = round($order["tax"], 2);

            if(RoleChecker::hasRole("restaurant-customer") && ( 0 == $order['is_normal'])){
                $orderItemsToshow["delivery_charge"] = 0;
                $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] , 2);
            }
            else {
                $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
                $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] + $order->delivery_fee, 2);
            }

            $orderItemsToshow["order_status"] = $customerOrder["order_status"];
            $orderItemsToshow["payment_status"] = $customerOrder["payment_status"];
            $orderItemsToshow["delivery_status"] = $customerOrder["delivery_status"];
            $orderItemsToshow["order_status_slug"] = $customerOrder["order_status_slug"];
            $orderItemsToshow["payment_status_slug"] = $customerOrder["payment_status_slug"];
            $orderItemsToshow["delivery_status_slug"] = $customerOrder["delivery_status_slug"];
            $orderItemsToshow["delivery_method_slug"] = $customerOrder["delivery_method"];
            $orderItemsToshow["delivery_method_name"] = ($customerOrder["delivery_method_name"]) ? $customerOrder["delivery_method_name"] : "";

            $orderItemsToshow["payment_method_slug"] = $customerOrder["payment_method"];
            $orderItemsToshow["payment_method_name"] = ($customerOrder["payment_method_name"]) ? $customerOrder["payment_method_name"] : "";
            return response()->json([
                "status" => "200",
                "data" => $orderItemsToshow
            ]);
        } else {
            return response()->json([
                "status" => "403",
                "message" => "You are not allowed to view order item"
            ], 403);
        }
    }

    public function viewOrderItemV2($id)
    {
        $partialRefundResponse = [];
        $lang = Input::get("lang", "en");
        $showCategoryFlag = false;
        /**
         * Checks if user role admin or super admin
         *
         * delivery address, restaurant address, user detail including phone, email, order detail
         */
        $orderItemsToshow["item_count"] = 0;
        if (RoleChecker::hasRole('admin') || RoleChecker::hasRole('super-admin')) {
            try {
                try {
                    $order = $this->order->getSpecificOrder($id);
                    $order->makeVisible("restaurant");
                    $restaurantData = unserialize($order->restaurant);
                    $order["order_date"] = Carbon::parse($order->created_at)->tz($restaurantData["timezone"]);
                    $order->setDefaultLanguage = $lang;
                    $order->makeVisible("delivery_time");
                    $delivery_time = $this->getDeliveryTime($order);
//                    unset( $order["delivery_time"]);
                    $order["delivery_time"] = $delivery_time;
                    $orderStatusLanguage = $this->getOrderStatusTransaction($order["order_status"], $lang);
                    $orderStatusSlugLanguage = $this->getOrderStatusSlugTransaction($order["order_status"]);
                    if ($orderStatusLanguage) {
                        $order["order_status"] = $orderStatusLanguage;
                        $order["order_status_slug"] = $orderStatusSlugLanguage;
                    }
                    $paymentStatusLanguage = $this->getPaymentStatusTransaction($order["payment_status"], $lang);
                    $paymentStatusSlugLanguage = $this->getPaymentStatusSlugTransaction($order["payment_status"]);

                    if ($paymentStatusLanguage) {
                        $order["payment_status"] = $paymentStatusLanguage;
                        $order["payment_status_slug"] = $paymentStatusSlugLanguage;
                    }
                    $deliveryStatusLanguage = $this->getDeliveryStatusTransaction($order["delivery_status"], $lang);
                    $deliveryStatusSlugLanguage = $this->getDeliveryStatusSlugTransaction($order["delivery_status"]);

                    if ($deliveryStatusLanguage) {
                        $order["delivery_status"] = $deliveryStatusLanguage;
                        $order["delivery_status_slug"] = $deliveryStatusSlugLanguage;
                    }
                    $paymentMethod = $this->getPaymentMethodTranslation($order["payment_method"], $lang);
                    if ($paymentMethod) {
                        $order["payment_method"] = $paymentMethod;
                    }
                    $shippingMethod = $this->getDeliverytMethodTranslation($order["delivery_type"], $lang);
                    if ($shippingMethod) {
                        $order["delivery_type"] = $shippingMethod;
                    }
                } catch (\Exception $ex) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Order could not found"
                    ], 404);
                }
                /**
                 * gets order items based on order id from order item table
                 */
                $orderItems = $order->orderItem()->get();

                $orderItemCount = $orderItems->count();
                if ($orderItemCount == 0) {
                    throw  new \Exception();
                }
                /**
                 * setting @param $total to 0 by default
                 */

                $total = 0;

                foreach ($orderItems as $orderItem) {
                    $itemTotal = 0;
                    $addonTotal = 0;
                    /**
                     * gets order Item addon
                     */

                    $orderItemsToshow["restaurant_name"] = '';
                    $orderItemsToshow["phone_no"] = null;
                    $orderItemsToshow["mobile_no"] = null;
                    $orderItemsToshow["address"] = null;
                    $orderItemsToshow["order"] = $order;
                    /**
                     * get restaurant base url from Config/config.php
                     */
                    $restaurantId = $order->restaurant_id;
                    $restaurantBaseUrl = Config::get("config.restaurant_base_url");
                    $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$restaurantId/detail?lang=" . $lang;

                    /**
                     *calling restaurant service to check the restaurant exist or not if exist fetch the data
                     */

                    $restaurantData = RemoteCall::getSpecific($restaurantFullUrl);


                    if ($restaurantData['status'] != 200) {
                        $order->makeVisible("restaurant");
                        $restaurantData = unserialize($order["restaurant"]);
                        if ($restaurantData || is_null($restaurantData)) {
                            $orderItemsToshow["restaurant_name"] = $this->restaurantTranslate($restaurantData["translation"], $lang);
                            $orderItemsToshow["phone_no"] = $restaurantData["phone_no"];
                            $orderItemsToshow["mobile_no"] = $restaurantData["mobile_no"];
                            $orderItemsToshow["address"] = $restaurantData["address"] ?: null;
                        }
                        $order->makeHidden("restaurant", "extra_info");
                    } else {
                        $orderItemsToshow["restaurant_name"] = $restaurantData["message"]["data"]["name"];
                        $orderItemsToshow["phone_no"] = $restaurantData["message"]["data"]["phone_no"];
                        $orderItemsToshow["mobile_no"] = $restaurantData["message"]["data"]["mobile_no"];
                        $orderItemsToshow["address"] = $restaurantData["message"]["data"]["address"] ?: null;
                    }

                    /**
                     *calling location service to get shipping address data
                     */
                    $locationBaseUrl = Config::get("config.location_base_url");
                    $locationFullUrl = "$locationBaseUrl/public/shipping/address/" . $order->shipping_id . "?lang=" . $lang;
                    $locationData = RemoteCall::getSpecific($locationFullUrl);
                    if ($locationData['status'] != 200) {
                        $orderItemsToshow["shipping_address"] = null;
                    } else {
                        $shippingList['address_name'] = $locationData['message']['data']['address_name'];
                        $shippingList['street'] = $locationData['message']['data']['street'];
                        $shippingList['longitude'] = $locationData['message']['data']['longitude'];
                        $shippingList['latitude'] = $locationData['message']['data']['latitude'];
                        $shippingList['extra_direction'] = $locationData['message']['data']['extra_direction'];
                        $shippingList['type'] = $locationData['message']['data']['type'];
                        $shippingList['zip_code'] = $locationData['message']['data']['zip_code'];
                        $shippingList['floor_number'] = $locationData['message']['data']['floor_number'];
                        $shippingList['office_name'] = $locationData['message']['data']['office_name'];
                        $shippingList['district_name'] = $locationData['message']['data']['district_name'];
                        $shippingList['city_name'] = $locationData['message']['data']['city_name'];
                        $shippingList['country_name'] = $locationData['message']['data']['country_name'];
                        $orderItemsToshow["shipping_address"] = $shippingList;
                    }

                    /**
                     *calling oauth service to get shipping address data
                     */
                    $oauth_base_url = Config::get("config.oauth_base_url");
                    $userFullUrl = "$oauth_base_url/machine/user/" . $order->customer_id . "/detail";
                    $userData = RemoteCall::getSpecific($userFullUrl);
                    if ($userData['status'] != 200) {
                        $orderItemsToshow["user_detail"] = null;
                    } else {
                        $orderItemsToshow['user_detail'] = $userData['message']['data'];
                    }

                    $orderItemAddons = $orderItem->OrderItemAddon()->get();
                    /**
                     * loops through array of addons to calculate addon total
                     */
                    $tempArray = [];
                    foreach ($orderItemAddons as $orderItemAddon) {

                        $addonName = unserialize($orderItemAddon["addon_name"]);
                        $orderItemAddon["addon_name"] = $this->restaurantTranslate($addonName, $lang);
                        $tempArray[] = $orderItemAddon;
                        $addonTotal += $orderItemAddon["unit_price"];
                    }
                    $orderItem["addons"] = $tempArray;
                    /**
                     * gets specific food total price by adding addons unit cost and food unit cost
                     */
                    $orderItemsToshow["item_count"] += $orderItem["quantity"];
                    $itemTotal = $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                    $orderItem["unit_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItem["unit_price"]);
                    $orderItem["item_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $itemTotal);
                    $orderItem["item_price"] = $itemTotal;
                    $foodName = unserialize($orderItem["food_name"]);
                    $foodDetails = unserialize($orderItem->food_raw_data);
                    $orderItem["food_name"] = $this->restaurantTranslate($foodName, $lang);
                    $orderItem["is_adjustment"] = false;
                    $orderItem["special_instruction"] = (isset($foodDetails["special_instruction"])) ? $foodDetails["special_instruction"] : '';
                    $orderItem["food_size"] = (isset($foodDetails["size_title"])) ? $this->restaurantTranslate($foodDetails["size_title"], $lang) : "";
                    $orderItemsToshow["items"][] = $orderItem;
                    /**
                     * calculates total price for this order
                     */
                    $total += $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                }

//                if(isset($order["tax_type"]) && $order["tax_type"] != 'exclusive'){
//                    $orderItemsToshow["tax"] = round(($order["delivery_fee"] + $total) * $order->site_tax_rate * 0.01,2);
//                    $deliveryFeeTax = round(($order["delivery_fee"]) * $order->site_tax_rate * 0.01,2);
//                    $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
//                    $orderItemsToshow["subtotal"] = $total * (1 - $order->site_tax_rate * 0.01);;
//                    $orderItemsToshow["total"] = round($total + $deliveryFeeTax + $order->delivery_fee ,2);
//                }
//                else{
//                }
                $refunds = $order->partialRefunds;
                if (count($refunds) == 0) {
                    goto skipGettingUserInfo;
                }

                $userDatas["users"] = array_values($refunds->pluck("user_id")->all());
                $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list?lang=$lang";
                $getUserDetailsForReview = RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);
                if ($getUserDetailsForReview["status"] != "200") {
                    return response()->json([
                        "status" => $getUserDetailsForReview["status"],
                        "message" => $getUserDetailsForReview["message"]["message"]
                    ], $getUserDetailsForReview["status"]);
                }
                $partialRefundResponse = [];
                foreach ($refunds as $refund) {

                    $flag = true;
                    if ($refund->type == "minus") {
                        $flag = false;
                    }
                    $amount = ($flag) ? $refund->amount : -1 * $refund->amount;
                    $total += $amount;
                    $price = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $refund->amount);
                    $price = ($flag) ? $price : "-" . $price;
                    $refund = [
                        "id" => 0,
                        "adjustment_id" => $refund->id,
                        "order_id" => $refund->order_id,
//                            "user_id" => $refund->user_id,
                        "item_id" => 0,
//                        "id" => $refund->id,
                        "user_id" => $refund->user_id,
                        "food_name" => $refund->title,
                        "quantity" => 1,
                        "unit_price" => $amount,
                        "addons" => [],
                        "is_adjustment" => true,
                        "unit_price_currency" => $price,
                        "item_price_currency" => $price,
                        "item_price" => $amount,
                        "special_instruction" => "",
                        "food_size" => "",
                        "type" => $refund->type,
                        "vat" => $refund->vat,
                        "comment" => $refund->comment,
                        "food_image_url" => "null",
                        "size" => ""
                    ];
                    $refund["name"] = "";
                    foreach ($getUserDetailsForReview["message"]["data"] as $userDetails) {
                        if ($userDetails["id"] == $refund["user_id"]) {
                            $refund["name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($userDetails['first_name'], $userDetails['middle_name'], $userDetails['last_name']))));
                        }
                    }
                    $partialRefundResponse[] = $refund;

                }

//                $refunds->makeHidden(["user_id","comment"]);
                skipGettingUserInfo:
                $orderItemsToshow["items"] = array_merge($orderItemsToshow["items"], $partialRefundResponse);
//                $orderItemsToshow["partial_refund"] = $refunds;
                $orderItemsToshow["rejection_reason_id"] = $order->rejection_reason_id;
                $orderItemsToshow["rejection_reason"] = $order->rejection_reason;
                $orderItemsToshow["subtotal"] = $total;
                $orderItemsToshow["tax"] = round($order["tax"], 2);
                if(isset($order["user_type"]) &&  ($order["user_type"] == "restaurant-customer") && (0 == $order['is_normal'])){
                    $orderItemsToshow["delivery_charge"] = 0;
                    $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] , 2);
                }
                else {
                    $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
                    $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] + $order->delivery_fee, 2);
                }
                $orderItemsToshow["subtotal_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $total);
                $orderItemsToshow["tax_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItemsToshow["tax"]);
                $orderItemsToshow["delivery_charge_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"],  $orderItemsToshow["delivery_charge"]);
                $orderItemsToshow["total_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItemsToshow["total"]);
                return response()->json([
                    "status" => "200",
                    "data" => $orderItemsToshow
                ]);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Order Item not found"
                ], 404);
            }
        }
        elseif (RoleChecker::hasRole("restaurant-admin") || (RoleChecker::get('parentRole') == "restaurant-admin")) {
            $restaurantInfo = RoleChecker::getRestaurantInfo();
            if (!is_null($restaurantInfo) && ($restaurantInfo["is_restaurant_admin"] == true)) {
                if (!is_null($restaurantInfo["branch_id"])) {
                    try {
                        $branchOrder = $this->order->getSpecificOrder($id);
                    } catch (ModelNotFoundException $ex) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Order not found"
                        ], 404);
                    }
                    /**
                     * checks if restaurant id matches with the restaurant info which is returned from oauth server
                     */
                    $array = array_intersect([$branchOrder["restaurant_id"]], $restaurantInfo["branch_id"]);
                    if (empty($array)) {
                        return response()->json([
                            "status" => "403",
                            "message" => "You can view your restaurant order item only"
                        ], 403);
                    }
                    $order = $branchOrder;
                    $order->setDefaultLanguage = $lang;
                    $order->makeVisible("restaurant");
                    $restaurantData = unserialize($order->restaurant);
                    $order["order_date"] = Carbon::parse($order->created_at)->tz($restaurantData["timezone"]);
                    $order->makeVisible("delivery_time");
                    $delivery_time = $this->getDeliveryTime($order);
//                    unset( $order["delivery_time"]);
                    $order["delivery_time"] = $delivery_time;
                    $orderStatusLanguage = $this->getOrderStatusTransaction($order["order_status"], $lang);
                    $orderStatusSlugLanguage = $this->getOrderStatusSlugTransaction($order["order_status"]);
                    if ($orderStatusLanguage) {
                        $order["order_status"] = $orderStatusLanguage;
                        $order["order_status_slug"] = $orderStatusSlugLanguage;
                    }
                    $paymentStatusLanguage = $this->getPaymentStatusTransaction($order["payment_status"], $lang);
                    $paymentStatusSlugLanguage = $this->getPaymentStatusSlugTransaction($order["payment_status"]);

                    if ($paymentStatusLanguage) {
                        $order["payment_status"] = $paymentStatusLanguage;
                        $order["payment_status_slug"] = $paymentStatusSlugLanguage;
                    }
                    $deliveryStatusLanguage = $this->getDeliveryStatusTransaction($order["delivery_status"], $lang);
                    $deliveryStatusSlugLanguage = $this->getDeliveryStatusSlugTransaction($order["delivery_status"]);

                    if ($deliveryStatusLanguage) {
                        $order["delivery_status"] = $deliveryStatusLanguage;
                        $order["delivery_status_slug"] = $deliveryStatusSlugLanguage;
                    }
                    $paymentMethod = $this->getPaymentMethodTranslation($order["payment_method"], $lang);
                    if ($paymentMethod) {
                        $order["payment_method"] = $paymentMethod;
                    }
                    $shippingMethod = $this->getDeliverytMethodTranslation($order["delivery_type"], $lang);
                    if ($shippingMethod) {
                        $order["delivery_type"] = $shippingMethod;
                    }
                    $orderItems = $order->orderItem()->get();
                    $orderItemsCount = $orderItems->count();
                    $total = 0;
                    if ($orderItemsCount == 0) {
                        return response()->json([
                            "status" => "404",
                            "message" => "Empty Order Item"
                        ], 404);
                    }
                    foreach ($orderItems as $orderItem) {
                        $itemTotal = 0;
                        $addonTotal = 0;
                        /**
                         * gets order Item addon
                         */

                        $orderItemsToshow["restaurant_name"] = '';
                        $orderItemsToshow["phone_no"] = null;
                        $orderItemsToshow["mobile_no"] = null;
                        $orderItemsToshow["order"] = $order;
//                        $orderItemsToshow["unit_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItem["unit_price"]);

                        /**
                         * get restaurant base url from Config/config.php
                         */
                        $restaurantId = $order->restaurant_id;
                        $restaurantBaseUrl = Config::get("config.restaurant_base_url");
                        $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$restaurantId/detail?lang=" . $lang;

                        /**
                         *calling restaurant service to check the restaurant exist or not if exist fetch the data
                         */

                        $restaurantData = RemoteCall::getSpecific($restaurantFullUrl);


                        if ($restaurantData['status'] != 200) {
                            $order->makeVisible("restaurant");
                            $restaurantData = unserialize($order["restaurant"]);
                            if ($restaurantData || is_null($restaurantData)) {
                                $orderItemsToshow["restaurant_name"] = $this->restaurantTranslate($restaurantData["translation"], $lang);;
                                $orderItemsToshow["phone_no"] = $restaurantData["phone_no"];
                                $orderItemsToshow["mobile_no"] = $restaurantData["mobile_no"];
                                $orderItemsToshow["address"] = $restaurantData["address"] ?: null;
                            }
                            $order->makeHidden("restaurant", "extra_info");
                        } else {
                            $orderItemsToshow["restaurant_name"] = $restaurantData["message"]["data"]["name"];
                            $orderItemsToshow["phone_no"] = $restaurantData["message"]["data"]["phone_no"];
                            $orderItemsToshow["mobile_no"] = $restaurantData["message"]["data"]["mobile_no"];
                            $orderItemsToshow["address"] = $restaurantData["message"]["data"]["address"] ?: null;
                        }
                        /**
                         *calling location service to get shipping address data
                         */
                        $locationBaseUrl = Config::get("config.location_base_url");
                        $locationFullUrl = "$locationBaseUrl/public/shipping/address/" . $order->shipping_id . "?lang=" . $lang;
                        $locationData = RemoteCall::getSpecific($locationFullUrl);
                        if ($locationData['status'] != 200) {
                            $orderItemsToshow["shipping_address"] = null;
                        } else {
                            $shippingList['address_name'] = $locationData['message']['data']['address_name'];
                            $shippingList['street'] = $locationData['message']['data']['street'];
                            $shippingList['longitude'] = $locationData['message']['data']['longitude'];
                            $shippingList['latitude'] = $locationData['message']['data']['latitude'];
                            $shippingList['extra_direction'] = $locationData['message']['data']['extra_direction'];
                            $shippingList['type'] = $locationData['message']['data']['type'];
                            $shippingList['zip_code'] = $locationData['message']['data']['zip_code'];
                            $shippingList['floor_number'] = $locationData['message']['data']['floor_number'];
                            $shippingList['office_name'] = $locationData['message']['data']['office_name'];
                            $shippingList['district_name'] = $locationData['message']['data']['district_name'];
                            $shippingList['city_name'] = $locationData['message']['data']['city_name'];
                            $shippingList['country_name'] = $locationData['message']['data']['country_name'];
                            $orderItemsToshow["shipping_address"] = $shippingList;
                        }

                        /**
                         *calling oauth service to get shipping address data
                         */
                        $oauth_base_url = Config::get("config.oauth_base_url");
                        $userFullUrl = "$oauth_base_url/machine/user/" . $order->customer_id . "/detail";
                        $userData = RemoteCall::getSpecific($userFullUrl);
                        if ($userData['status'] != 200) {
                            $orderItemsToshow["user_detail"] = null;
                        } else {
                            $orderItemsToshow['user_detail'] = $userData['message']['data'];
                        }

                        $orderItemAddons = $orderItem->OrderItemAddon()->get();
                        $tempArray = [];
                        foreach ($orderItemAddons as $orderItemAddon) {

                            $addonName = unserialize($orderItemAddon["addon_name"]);
                            $orderItemAddon["addon_name"] = $this->restaurantTranslate($addonName, $lang);
                            $tempArray[] = $orderItemAddon;
                            $addonTotal += $orderItemAddon["unit_price"];
                        }
                        $orderItem["addons"] = $tempArray;
                        $orderItemsToshow["item_count"] += $orderItem["quantity"];
                        $itemTotal = $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                        $orderItem["item_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $itemTotal);
                        $orderItem["unit_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItem["unit_price"]);
                        $orderItem["item_price"] = $itemTotal;
                        $foodName = unserialize($orderItem["food_name"]);
                        $orderItem["food_name"] = $this->restaurantTranslate($foodName, $lang);
                        $orderItem["is_adjustment"] = false;
                        $foodDetails = unserialize($orderItem->food_raw_data);
                        $orderItem["special_instruction"] = (isset($foodDetails["special_instruction"])) ? $foodDetails["special_instruction"] : '';
                        $orderItem["food_size"] = (isset($foodDetails["size_title"])) ? $this->restaurantTranslate($foodDetails["size_title"], $lang) : "";
                        $orderItemsToshow["items"][] = $orderItem;
                        $total += $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                    }


//                    if(isset($order["tax_type"]) && $order["tax_type"] != 'exclusive'){
//                        $orderItemsToshow["tax"] = round(($order["delivery_fee"] + $total) * $order->site_tax_rate * 0.01,2);
//                        $deliveryFeeTax = round(($order["delivery_fee"]) * $order->site_tax_rate * 0.01,2);
//                        $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
//                        $orderItemsToshow["subtotal"] = $total * (1 - $order->site_tax_rate * 0.01);;
//                        $orderItemsToshow["total"] = round($total + $deliveryFeeTax + $order->delivery_fee ,2);
//                    }
//                    else{

//                    }
                    $refunds = $order->partialRefunds;
                    $partialRefundResponse = [];
                    foreach ($refunds as $refund) {
                        $flag = true;
                        if ($refund->type == "minus") {
                            $flag = false;
                        }
                        $amount = ($flag) ? $refund->amount : -1 * $refund->amount;
                        $total += $amount;
                        $price = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $refund->amount);
                        $price = ($flag) ? $price : "-" . $price;
                        $refund = [
                            "id" => 0,
                            "adjustment_id" => $refund->id,
                            "order_id" => $refund->order_id,
//                            "user_id" => $refund->user_id,
                            "item_id" => 0,
                            "food_name" => $refund->title,
                            "quantity" => 1,
                            "unit_price" => $amount,
                            "addons" => [],
                            "is_adjustment" => true,
                            "unit_price_currency" => $price,
                            "item_price_currency" => $price,
                            "item_price" => $amount,
                            "special_instruction" => "",
                            "food_size" => "",
                            "type" => $refund->type,
                            "vat" => $refund->vat,
                            "food_image_url" => "null",
                            "size" => ""
//                            "comment" => $refund->comment
                        ];
                        $partialRefundResponse[] = $refund;

                    }
                    $refunds->makeHidden(["user_id", "comment"]);
                    $orderItemsToshow["items"] = array_merge($orderItemsToshow["items"], $partialRefundResponse);
//                    $orderItemsToshow["partial_refund"] = $refunds;
                    $orderItemsToshow["rejection_reason_id"] = $order->rejection_reason_id;
                    $orderItemsToshow["rejection_reason"] = $order->rejection_reason;
                    $orderItemsToshow["subtotal"] = $total;
                    $orderItemsToshow["tax"] = round($order["tax"], 2);
                    if(isset($order["user_type"]) &&  ($order["user_type"] == "restaurant-customer")  && (0 == $order['is_normal'])){
                        $orderItemsToshow["delivery_charge"] = 0;
                        $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] , 2);
                    }
                    else {
                        $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
                        $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] + $order->delivery_fee, 2);
                    }
                    $orderItemsToshow["subtotal_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $total);
                    $orderItemsToshow["tax_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItemsToshow["tax"]);
                    $orderItemsToshow["delivery_charge_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"],  $orderItemsToshow["delivery_charge"]);
                    $orderItemsToshow["total_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItemsToshow["total"]);
                    return response()->json([
                        "status" => "200",
                        "data" => $orderItemsToshow
                    ]);


                }
            } else {
                /**
                 * this means there is some modification done in oauth restaurant_meta_table .
                 * and  is_restaurant_admin is false
                 * */
                return response()->json([
                    "status" => "403",
                    "message" => "Forbidden"
                ], 403);
            }
        }
        elseif (RoleChecker::hasRole("branch-admin") || RoleChecker::hasRole("branch-operator") || (RoleChecker::get('parentRole') == "branch-admin" || RoleChecker::get('parentRole') == "branch-operator")) {
            $restaurantInfo = RoleChecker::getRestaurantInfo();
            if (!is_null($restaurantInfo) && !is_null($restaurantInfo["branch_id"]) && ($restaurantInfo["is_restaurant_admin"] == false)) {
                /*
                 * gets order based on restaurant id and order id
                 */
                if (RoleChecker::hasRole("branch-operator") || RoleChecker::get('parentRole') == "branch-operator") {
                    $showCategoryFlag = true;
                }

                $order = $this->order->getOrdersByRestaurantAndOrderId($restaurantInfo["branch_id"], $id);
                if (count($order) == 0) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Empty Order Item"
                    ], 404);
                }
                $order->setDefaultLanguage = $lang;
                $order->makeVisible("restaurant");
                $restaurantData = unserialize($order->restaurant);
                $order["order_date"] = Carbon::parse($order->created_at)->tz($restaurantData["timezone"]);
                $order->makeVisible("delivery_time");
                $delivery_time = $this->getDeliveryTime($order);
//                    unset( $order["delivery_time"]);
                $order["delivery_time"] = $delivery_time;
                //TODO
                $orderStatusLanguage = $this->getOrderStatusTransaction($order["order_status"], $lang);
                $orderStatusSlugLanguage = $this->getOrderStatusSlugTransaction($order["order_status"]);
                if ($orderStatusLanguage) {
                    $order["order_status"] = $orderStatusLanguage;
                    $order["order_status_slug"] = $orderStatusSlugLanguage;
                }
                $paymentStatusLanguage = $this->getPaymentStatusTransaction($order["payment_status"], $lang);
                $paymentStatusSlugLanguage = $this->getPaymentStatusSlugTransaction($order["payment_status"]);

                if ($paymentStatusLanguage) {
                    $order["payment_status"] = $paymentStatusLanguage;
                    $order["payment_status_slug"] = $paymentStatusSlugLanguage;
                }
                $deliveryStatusLanguage = $this->getDeliveryStatusTransaction($order["delivery_status"], $lang);
                $deliveryStatusSlugLanguage = $this->getDeliveryStatusSlugTransaction($order["delivery_status"]);

                if ($deliveryStatusLanguage) {
                    $order["delivery_status"] = $deliveryStatusLanguage;
                    $order["delivery_status_slug"] = $deliveryStatusSlugLanguage;
                }
                $paymentMethod = $this->getPaymentMethodTranslation($order["payment_method"], $lang);
                $order["payment_method_slug"] = $order["payment_method"];
                if ($paymentMethod) {
                    $order["payment_method"] = $paymentMethod;
                }
                $shippingMethod = $this->getDeliverytMethodTranslation($order["delivery_type"], $lang);
                if ($shippingMethod) {
                    $order["delivery_type"] = $shippingMethod;
                }
                if (!$order) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Order not found"
                    ], 404);
                }
                $orderItems = $order->orderItem()->get();
                $orderItemsCount = $orderItems->count();
                $total = 0;
                if ($orderItemsCount == 0) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Empty Order Item"
                    ], 404);
                }
                foreach ($orderItems as $orderItem) {
                    $itemTotal = 0;
                    $addonTotal = 0;
                    /**
                     * gets order Item addon
                     */

                    $orderItemsToshow["restaurant_name"] = '';
                    $orderItemsToshow["phone_no"] = null;
                    $orderItemsToshow["mobile_no"] = null;
                    $orderItemsToshow["order"] = $order;
//                    $orderItemsToshow["unit_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItem["unit_price"]);

                    /**
                     * get restaurant base url from Config/config.php
                     */
                    $restaurantId = $order->restaurant_id;
                    $restaurantBaseUrl = Config::get("config.restaurant_base_url");
                    $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$restaurantId/detail?lang=" . $lang;

                    /**
                     *calling restaurant service to check the restaurant exist or not if exist fetch the data
                     */

                    $restaurantData = RemoteCall::getSpecific($restaurantFullUrl);


                    if ($restaurantData['status'] != 200) {
                        $order->makeVisible("restaurant");
                        $restaurantData = unserialize($order["restaurant"]);
                        if ($restaurantData || is_null($restaurantData)) {
                            $orderItemsToshow["restaurant_name"] = $this->restaurantTranslate($restaurantData["translation"], $lang);;
                            $orderItemsToshow["phone_no"] = $restaurantData["phone_no"];
                            $orderItemsToshow["mobile_no"] = $restaurantData["mobile_no"];
                            $orderItemsToshow["address"] = $restaurantData["address"] ?: null;
                        }
                        $order->makeHidden("restaurant", "extra_info");
                    } else {
                        $orderItemsToshow["restaurant_name"] = $restaurantData["message"]["data"]["name"];
                        $orderItemsToshow["phone_no"] = $restaurantData["message"]["data"]["phone_no"];
                        $orderItemsToshow["mobile_no"] = $restaurantData["message"]["data"]["mobile_no"];
                        $orderItemsToshow["address"] = $restaurantData["message"]["data"]["address"] ?: null;
                    }
                    /**
                     *calling location service to get shipping address data
                     */
                    $locationBaseUrl = Config::get("config.location_base_url");
                    $locationFullUrl = "$locationBaseUrl/public/shipping/address/" . $order->shipping_id . "?lang=" . $lang;
                    $locationData = RemoteCall::getSpecific($locationFullUrl);
                    if ($locationData['status'] != 200) {
                        $orderItemsToshow["shipping_address"] = null;
                    } else {
                        $shippingList['address_name'] = $locationData['message']['data']['address_name'];
                        $shippingList['street'] = $locationData['message']['data']['street'];
                        $shippingList['longitude'] = $locationData['message']['data']['longitude'];
                        $shippingList['latitude'] = $locationData['message']['data']['latitude'];
                        $shippingList['extra_direction'] = $locationData['message']['data']['extra_direction'];
                        $shippingList['type'] = $locationData['message']['data']['type'];
                        $shippingList['zip_code'] = $locationData['message']['data']['zip_code'];
                        $shippingList['floor_number'] = $locationData['message']['data']['floor_number'];
                        $shippingList['office_name'] = $locationData['message']['data']['office_name'];
                        $shippingList['district_name'] = $locationData['message']['data']['district_name'];
                        $shippingList['city_name'] = $locationData['message']['data']['city_name'];
                        $shippingList['country_name'] = $locationData['message']['data']['country_name'];
                        $orderItemsToshow["shipping_address"] = $shippingList;
                    }

                    /**
                     *calling oauth service to get shipping address data
                     */
                    $oauth_base_url = Config::get("config.oauth_base_url");
                    $userFullUrl = "$oauth_base_url/machine/user/" . $order->customer_id . "/detail";
                    $userData = RemoteCall::getSpecific($userFullUrl);
                    if ($userData['status'] != 200) {
                        $orderItemsToshow["user_detail"] = null;
                    } else {
                        $orderItemsToshow['user_detail'] = $userData['message']['data'];
                    }
                    $orderItemAddons = $orderItem->OrderItemAddon()->get();
                    $tempArray = [];
                    foreach ($orderItemAddons as $orderItemAddon) {

                        $addonName = unserialize($orderItemAddon["addon_name"]);
                        $orderItemAddon["addon_name"] = $this->restaurantTranslate($addonName, $lang);
                        $tempArray[] = $orderItemAddon;
                        $addonTotal += $orderItemAddon["unit_price"];
                    }
                    $orderItem["addons"] = $tempArray;
                    $orderItemsToshow["item_count"] += $orderItem["quantity"];
                    $itemTotal = $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                    $orderItem["item_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $itemTotal);
                    $orderItem["unit_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItem["unit_price"]);
                    $orderItem["item_price"] = $itemTotal;
                    $foodName = unserialize($orderItem["food_name"]);
                    $orderItem["food_name"] = $this->restaurantTranslate($foodName, $lang);
                    $orderItem["is_adjustment"] = false;
                    $foodDetails = unserialize($orderItem->food_raw_data);
                    $orderItem["food_size"] = (isset($foodDetails["size_title"])) ? $this->restaurantTranslate($foodDetails["size_title"], $lang) : "";
                    $orderItem["special_instruction"] = (isset($foodDetails["special_instruction"])) ? $foodDetails["special_instruction"] : '';
//                    $orderItem["addons"] = $orderItemAddons;
                    $orderItemsToshow["items"][] = $orderItem;
                    $total += $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                }


//                if(isset($order["tax_type"]) && $order["tax_type"] != 'exclusive'){
//                    $orderItemsToshow["tax"] = round(($order["delivery_fee"] + $total) * $order->site_tax_rate * 0.01,2);
//                    $deliveryFeeTax = round(($order["delivery_fee"]) * $order->site_tax_rate * 0.01,2);
//                    $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
//                    $orderItemsToshow["subtotal"] = $total * (1 - $order->site_tax_rate * 0.01);;
//                    $orderItemsToshow["total"] = round($total + $deliveryFeeTax + $order->delivery_fee ,2);
//                }
//                else{
//                }
                $partialRefundResponse = [];
                $refunds = $order->partialRefunds;
                foreach ($refunds as $refund) {
                    $flag = true;
                    if ($refund->type == "minus") {
                        $flag = false;
                    }
                    $price = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $refund->amount);
                    $amount = ($flag) ? $refund->amount : -1 * $refund->amount;
                    $total += $amount;
                    $price = ($flag) ? $price : "-" . $price;
                    $refund = [
                        "id" => 0,
                        "adjustment_id" => $refund->id,
                        "order_id" => $refund->order_id,
//                            "user_id" => $refund->user_id,
                        "item_id" => 0,
                        "food_name" => $refund->title,
                        "quantity" => 1,
                        "unit_price" => $amount,
                        "addons" => [],
                        "is_adjustment" => true,
                        "unit_price_currency" => $price,
                        "item_price_currency" => $price,
                        "item_price" => $amount,
                        "special_instruction" => "",
                        "food_size" => "",
                        "type" => $refund->type,
                        "vat" => $refund->vat,
                        "food_image_url" => "null",
                        "size" => ""
//                            "comment" => $refund->comment
                    ];
                    $partialRefundResponse[] = $refund;

                }
                $refunds->makeHidden(["user_id", "comment"]);
                if (!$showCategoryFlag) {
                    goto skipOrderingByCategories;
                }
                $foodIdList["food_ids"] = collect($orderItemsToshow["items"])->pluck("item_id")->unique()->all();
                //caling restaurant service for getting categories

                $getFoodWithCategories = RemoteCall::storeWithoutOauth($restaurantBaseUrl . "/foods/category", $foodIdList);
                if ($getFoodWithCategories["status"] != "200") {
                    return response()->json([
                        "status" => $getFoodWithCategories["status"],
                        "message" => $getFoodWithCategories["message"]["message"]
                    ], $getFoodWithCategories["status"]);
                }
                $orderItemsToshowTempValue["items"] = array_merge($orderItemsToshow["items"], $partialRefundResponse);
                $orderItemsToshow["items"] = [];
                foreach ($orderItemsToshowTempValue["items"] as $item) {
                    if ($item["is_adjustment"]) {
                        $orderItemsToshow["items"]["adjustment"][] = $item;
                    } else {
                        foreach ($getFoodWithCategories["message"]["data"] as $food) {
                            if ($food["id"] == $item["item_id"]) {
                                $orderItemsToshow["items"][$food["name"]][] = $item;
                            }
                        }
                    }
                }
//                    $orderItemsToshow["partial_refund"] = $refunds;
                skipOrderingByCategories:
                $orderItemsToshow["rejection_reason_id"] = $order->rejection_reason_id;
                $orderItemsToshow["rejection_reason"] = $order->rejection_reason;
                $orderItemsToshow["subtotal"] = $total;
                $orderItemsToshow["tax"] = round($order["tax"], 2);
                if(isset($order["user_type"]) &&  ($order["user_type"] == "restaurant-customer")  && (0 == $order['is_normal'])){
                    $orderItemsToshow["delivery_charge"] = 0;
                    $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] , 2);
                }
                else {
                    $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
                    $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] + $order->delivery_fee, 2);
                }
                $orderItemsToshow["subtotal_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $total);
                $orderItemsToshow["tax_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItemsToshow["tax"]);
                $orderItemsToshow["delivery_charge_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"],  $orderItemsToshow["delivery_charge"]);
                $orderItemsToshow["total_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $orderItemsToshow["total"]);
                return response()->json([
                    "status" => "200",
                    "data" => $orderItemsToshow
                ]);

            } else {
                /**
                 * this means there is some modification done in oauth restaurant_meta_table .
                 * and  is_restaurant_admin is true if you are branch-admin
                 * */
                return response()->json([
                    "status" => "403",
                    "message" => "Forbidden."
                ], 403);
            }
        }
        elseif (RoleChecker::hasRole("customer") || RoleChecker::hasRole("restaurant-customer")) {
            try {
                $order = $this->order->getSpecificOrder($id);
                $order->setDefaultLanguage = $lang;
                $order->makeVisible("restaurant");
                $restaurantData = unserialize($order->restaurant);
                $order["order_date"] = Carbon::parse($order->created_at)->tz($restaurantData["timezone"]);
                $order->makeVisible("delivery_time");
                $delivery_time = $this->getDeliveryTime($order);
                $orderItemsToshow["delivery_time"] = $delivery_time;
                $orderItemsToshow["pre_order"] = $order->pre_order;
                $extraInfo = unserialize($order["extra_info"]);
                $orderStatusLanguage = $this->getOrderStatusTransaction($order["order_status"], $lang);
                $orderStatusSlugLanguage = $this->getOrderStatusSlugTransaction($order["order_status"]);
                $customerOrder["order_status"] = $order["order_status"];
                $customerOrder["payment_status"] = $order["payment_status"];

                if ($orderStatusLanguage) {
                    $customerOrder["order_status"] = $orderStatusLanguage;
                }
                if ($orderStatusSlugLanguage) {
                    $customerOrder["order_status_slug"] = $orderStatusSlugLanguage;
                }
                $paymentStatusLanguage = $this->getPaymentStatusTransaction($order["payment_status"], $lang);
                $paymentStatusSlugLanguage = $this->getPaymentStatusSlugTransaction($order["payment_status"]);
                if ($paymentStatusLanguage) {
                    $customerOrder["payment_status"] = $paymentStatusLanguage;
                }
                if ($paymentStatusSlugLanguage) {
                    $customerOrder["payment_status_slug"] = $paymentStatusSlugLanguage;
                }
                $customerOrder["delivery_status"] = $order["delivery_status"];
                $deliveryStatusLanguage = $this->getDeliveryStatusTransaction($order["delivery_status"], $lang);
                $deliveryStatusSlugLanguage = $this->getDeliveryStatusSlugTransaction($order["delivery_status"]);
                if ($deliveryStatusLanguage) {
                    $customerOrder["delivery_status"] = $deliveryStatusLanguage;
                }
                if ($deliveryStatusSlugLanguage) {
                    $customerOrder["delivery_status_slug"] = $deliveryStatusSlugLanguage;
                }
                $customerOrder["delivery_method"] = $order["delivery_type"];
                $customerOrder["delivery_method_name"] = $this->getDeliverytMethodTranslation($order["delivery_type"], $lang);

                $customerOrder["payment_method"] = $order["payment_method"];
                $customerOrder["payment_method_name"] = $this->getPaymentMethodTranslation($order["payment_method"], $lang);
                /**
                 * checks if the order belongs to logged in user or not
                 */
                if ($order->customer_id != RoleChecker::getuser()) {
                    return response()->json([
                        "status" => "403",
                        "message" => "You are only allowed to view your order item only"
                    ], 403);
                }
            } catch (ModelNotFoundException $exception) {
                return response()->json([
                    "status" => "404",
                    "message" => "Order not found "
                ], 404);
            }
            $orderItems = $order->orderItem()->get();
            $orderItemsCount = $orderItems->count();
            $total = 0;
            if ($orderItemsCount == 0) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty Order Item"
                ], 404);
            }
            $orderItemsToshow["item_count"] = 0;
            foreach ($orderItems as $orderItemKey => $orderItem) {
                $itemTotal = 0;
                $addonTotal = 0;
                /**
                 * gets order Item addon
                 */

                $orderItemsToshow["restaurant_name"] = '';
                $orderItemsToshow["phone_no"] = null;
                $orderItemsToshow["mobile_no"] = null;


                /**
                 * get restaurant base url from Config/config.php
                 */
                $restaurantId = $order->restaurant_id;
                $restaurantBaseUrl = Config::get("config.restaurant_base_url");
                $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$restaurantId/detail?lang=" . $lang;

                /**
                 *calling restaurant service to check the restaurant exist or not if exist fetch the data
                 */

                $restaurantData = RemoteCall::getSpecific($restaurantFullUrl);

                if ($restaurantData['status'] != 200) {
                    $order->makeVisible("restaurant");
                    $restaurantData = unserialize($order["restaurant"]);
                    if ($restaurantData || is_null($restaurantData)) {
                        $orderItemsToshow["restaurant_name"] = $this->restaurantTranslate($restaurantData["translation"], $lang);;
                        $orderItemsToshow["phone_no"] = $restaurantData["phone_no"];
                        $orderItemsToshow["mobile_no"] = $restaurantData["mobile_no"];
                        $orderItemsToshow["restaurant_slug"] = $restaurantData["slug"];
                        $orderItemsToshow["address"] = $restaurantData["address"] ?: null;
                        $orderItemsToshow['created_at'] = Carbon::parse($order["created_at"])->tz($restaurantData["timezone"])->format("Y-m-d h:i A");
                    }
                    $order->makeHidden("restaurant", "extra_info");
                } else {
                    $orderItemsToshow["restaurant_name"] = $restaurantData["message"]["data"]["name"];
                    $orderItemsToshow["phone_no"] = $restaurantData["message"]["data"]["phone_no"];
                    $orderItemsToshow["mobile_no"] = $restaurantData["message"]["data"]["mobile_no"];
                    $orderItemsToshow["address"] = $restaurantData["message"]["data"]["address"] ?: null;
                    $orderItemsToshow["restaurant_slug"] = $restaurantData["message"]["data"]["slug"];
//                    return $order["created_at"];
                    $orderItemsToshow['created_at'] = Carbon::parse($order["created_at"])->tz($restaurantData["message"]["data"]["timezone"])->format("Y-m-d h:i A");;

                }

                /**
                 *calling location service to get shipping address data
                 */
                $locationBaseUrl = Config::get("config.location_base_url");
                $locationFullUrl = "$locationBaseUrl/public/shipping/address/" . $order->shipping_id . "?lang=" . $lang;
                $locationData = RemoteCall::getSpecific($locationFullUrl);
                if ($locationData['status'] != 200) {
                    $orderItemsToshow["shipping_address"] = null;
                } else {
                    $shippingList['address_name'] = $locationData['message']['data']['address_name'];
                    $shippingList['street'] = $locationData['message']['data']['street'];
                    $shippingList['longitude'] = $locationData['message']['data']['longitude'];
                    $shippingList['latitude'] = $locationData['message']['data']['latitude'];
                    $shippingList['extra_direction'] = $locationData['message']['data']['extra_direction'];
                    $shippingList['type'] = $locationData['message']['data']['type'];
                    $shippingList['zip_code'] = $locationData['message']['data']['zip_code'];
                    $shippingList['floor_number'] = $locationData['message']['data']['floor_number'];
                    $shippingList['office_name'] = $locationData['message']['data']['office_name'];
                    $shippingList['district_name'] = $locationData['message']['data']['district_name'];
                    $shippingList['city_name'] = $locationData['message']['data']['city_name'];
                    $shippingList['country_name'] = $locationData['message']['data']['country_name'];
                    $orderItemsToshow["shipping_address"] = $shippingList;
                }

                /**
                 *calling oauth service to get shipping address data
                 */
                $oauth_base_url = Config::get("config.oauth_base_url");
                $userFullUrl = "$oauth_base_url/machine/user/" . $order->customer_id . "/detail";
                $userData = RemoteCall::getSpecific($userFullUrl);
                if ($userData['status'] != 200) {
                    $orderItemsToshow["user_detail"] = null;
                } else {
                    $orderItemsToshow['user_detail'] = $userData['message']['data'];
                }

                $orderItemAddons = $orderItem->OrderItemAddon()->get();
                $tempArray = [];

                foreach ($orderItemAddons as $addonKey => $orderItemAddon) {

                    $addonName = unserialize($orderItemAddon["addon_name"]);
                    if (isset($extraInfo["items"][$orderItemKey]["addons"][$addonKey])) {
                        $orderItemAddon["category_id"] = (isset($extraInfo["items"][$orderItemKey]["addons"][$addonKey]["category_id"])) ? $extraInfo["items"][$orderItemKey]["addons"][$addonKey]["category_id"] : "";
                        if (isset($extraInfo["items"][$orderItemKey]["addons"][$addonKey]["category_name"])) {
                            $orderItemAddon["category_name"] = $this->restaurantTranslate($extraInfo["items"][$orderItemKey]["addons"][$addonKey]["category_name"], $lang);
                        } else {
                            $orderItemAddon["category_name"] = $orderItemAddon["category_name"] = "";
                        }

                    } else {
                        $orderItemAddon["category_id"] = $orderItemAddon["category_name"] = "";
                    }
                    $orderItemAddon["addon_name"] = $this->restaurantTranslate($addonName, $lang);
                    $tempArray[] = $orderItemAddon;
                    $addonTotal += $orderItemAddon["unit_price"];
                }
                $orderItem["addons"] = $tempArray;
                $orderItemsToshow["item_count"] += $orderItem["quantity"];
                $itemTotal = $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
                $orderItem["item_price_currency"] = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $itemTotal);
                $orderItem["item_price"] = $itemTotal;
                $foodName = unserialize($orderItem["food_name"]);
                $orderItem["food_name"] = $this->restaurantTranslate($foodName, $lang);
                $orderItem["is_adjustment"] = false;
                $orderItem["food_image_url"] = "";
                $foodDetails = unserialize($orderItem->food_raw_data);
                $orderItem["food_size"] = (isset($foodDetails["size_title"])) ? $this->restaurantTranslate($foodDetails["size_title"], $lang) : "";
                $orderItem["special_instruction"] = (isset($foodDetails["special_instruction"])) ? $foodDetails["special_instruction"] : '';
                $orderItem["size"] = (isset($foodDetails["size_title"])) ? $this->restaurantTranslate($foodDetails["size_title"], $lang) : '';
                if (isset($foodDetails["image_url"]))
                    $orderItem["food_image_url"] = $foodDetails["image_url"];
//                $orderItem["addons"] = $orderItemAddons;
                $orderItemsToshow["items"][] = $orderItem;

                $total += $orderItem["quantity"] * ($orderItem["unit_price"] + $addonTotal);
            }


//            if(isset($order["tax_type"]) && $order["tax_type"] != 'exclusive'){
//                $orderItemsToshow["tax"] = round(($order["delivery_fee"] + $total) * $order->site_tax_rate * 0.01,2);
//                $deliveryFeeTax = round(($order["delivery_fee"]) * $order->site_tax_rate * 0.01,2);
//                $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
//                $orderItemsToshow["subtotal"] = $total * (1 - $order->site_tax_rate * 0.01);;
//                $orderItemsToshow["total"] = round($total + $deliveryFeeTax + $order->delivery_fee ,2);
//            }
//            else{

//            }
            $refunds = $order->partialRefunds;
            $partialRefundResponse = [];
            foreach ($refunds as $refund) {
                $flag = true;
                if ($refund->type == "minus") {
                    $flag = false;
                }
                $price = $this->currencyAttachHelper->attachCurrencyInAmount($order["currency"], $refund->amount);
                $amount = ($flag) ? $refund->amount : -1 * $refund->amount;
                $total += $amount;
                $price = ($flag) ? $price : "-" . $price;
                $refund = [
                    "id" => 0,
                    "adjustment_id" => $refund->id,
                    "order_id" => $refund->order_id,
//                            "user_id" => $refund->user_id,
                    "item_id" => 0,
                    "food_name" => $refund->title,
                    "quantity" => 1,
                    "unit_price" => $amount,
                    "addons" => [],
                    "is_adjustment" => true,
                    "unit_price_currency" => $price,
                    "item_price_currency" => $price,
                    "item_price" => $amount,
                    "special_instruction" => "",
                    "food_size" => "",
                    "type" => $refund->type,
                    "vat" => $refund->vat,
                    "food_image_url" => "null",
                    "size" => ""
//                            "comment" => $refund->comment
                ];
                $partialRefundResponse[] = $refund;

            }
            $refunds->makeHidden(["user_id", "comment"]);
            $orderItemsToshow["items"] = array_merge($orderItemsToshow["items"], $partialRefundResponse);
            $orderItemsToshow["rejection_reason"] = $order->rejection_reason;
//                    $orderItemsToshow["partial_refund"] = $refunds;
            $orderItemsToshow["subtotal"] = $total;
            $orderItemsToshow["currency"] = $order["currency"];
            $orderItemsToshow["tax"] = round($order["tax"], 2);

            if(RoleChecker::hasRole("restaurant-customer") && (0 == $order['is_normal'])){
                $orderItemsToshow["delivery_charge"] = 0;
                $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] , 2);
            }
            else {
                $orderItemsToshow["delivery_charge"] = $order->delivery_fee;
                $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] + $order->delivery_fee, 2);
            }
//            $orderItemsToshow["total"] = round($total + $orderItemsToshow["tax"] + $order->delivery_fee, 2);
            $orderItemsToshow["order_status"] = $customerOrder["order_status"];
            $orderItemsToshow["payment_status"] = $customerOrder["payment_status"];
            $orderItemsToshow["delivery_status"] = $customerOrder["delivery_status"];
            $orderItemsToshow["order_status_slug"] = $customerOrder["order_status_slug"];
            $orderItemsToshow["payment_status_slug"] = $customerOrder["payment_status_slug"];
            $orderItemsToshow["delivery_status_slug"] = $customerOrder["delivery_status_slug"];
            $orderItemsToshow["delivery_method_slug"] = $customerOrder["delivery_method"];
            $orderItemsToshow["delivery_method_name"] = ($customerOrder["delivery_method_name"]) ? $customerOrder["delivery_method_name"] : "";

            $orderItemsToshow["payment_method_slug"] = $customerOrder["payment_method"];
            $orderItemsToshow["payment_method_name"] = ($customerOrder["payment_method_name"]) ? $customerOrder["payment_method_name"] : "";
            return response()->json([
                "status" => "200",
                "data" => $orderItemsToshow
            ]);
        } else {
            return response()->json([
                "status" => "403",
                "message" => "You are not allowed to view order item"
            ], 403);
        }
    }


    private function getOrderStatusTransaction($orderStatus, $lang)
    {
        /**
         * gets order status from order status table based on slug order_status from order table
         */
        $orderStatus = $this->orderStatus->getOrderStatusBasedOnSlug($orderStatus);
        /*
         * if order_status is not present in order_status table slug then the value stored in order.order_status column in displayed
         */
        if (!$orderStatus) return false;
        $orderLanguage = $orderStatus->orderStatusTranslations()->where("lang_code", $lang)->first();
        /**
         *
         */
        if (!$orderLanguage) {
            $orderLanguage = $orderStatus->orderStatusTranslations()->where("lang_code", "en")->first();
            if (!$orderLanguage) return false;
        }

        return $orderLanguage["name"];

    }

    private function getOrderStatusSlugTransaction($orderStatus)
    {
        /**
         * gets order status from order status table based on slug order_status from order table
         */
        $orderStatus = $this->orderStatus->getOrderStatusBasedOnSlug($orderStatus);
        if (!$orderStatus) return false;
        return $orderStatus->slug;
    }

    private function getPaymentStatusTransaction($paymentStatus, $lang)
    {
        /**
         * gets payment status from payment status table based on slug payment_status from payment table
         */
        $paymentStatus = $this->paymentStatus->getPaymentStatusBasedOnSlug($paymentStatus);
        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (!$paymentStatus) return false;
        $paymentLanguage = $paymentStatus->paymentStatusTranslations()->where("lang_code", $lang)->first();
        /**
         *
         */
        if (!$paymentLanguage) {
            $paymentLanguage = $paymentStatus->paymentStatusTranslations()->where("lang_code", "en")->first();
            if (!$paymentLanguage) return false;
        }
        return $paymentLanguage["name"];

    }

    private function getPaymentStatusSlugTransaction($paymentStatus)
    {
        /**
         * gets payment status from payment status table based on slug payment_status from payment table
         */
        $paymentStatus = $this->paymentStatus->getPaymentStatusBasedOnSlug($paymentStatus);
        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (!$paymentStatus) return false;
        return $paymentStatus->slug;


    }

    private function getDeliveryStatusTransaction($deliveryStatus, $lang)
    {
        /**
         * gets payment status from payment status table based on slug payment_status from payment table
         */
        $deliveryStatus = $this->deliveryStatus->getDeliveryStatusBasedOnSlug($deliveryStatus);
        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (!$deliveryStatus) return false;
        $deliveryLanguage = $deliveryStatus->deliveryStatusTranslations()->where("lang_code", $lang)->first();
        /**
         *
         */
        if (!$deliveryLanguage) {
            $deliveryLanguage = $deliveryStatus->deliveryStatusTranslations()->where("lang_code", "en")->first();
            if (!$deliveryLanguage) return false;
        }
        return $deliveryLanguage["name"];

    }

    private function getDeliveryStatusSlugTransaction($deliveryStatus)
    {
        /**
         * gets payment status from payment status table based on slug payment_status from payment table
         */
        $deliveryStatus = $this->deliveryStatus->getDeliveryStatusBasedOnSlug($deliveryStatus);
        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (!$deliveryStatus) return false;
        return $deliveryStatus->slug;
    }

    /**
     * Translate restaurant name into passed param $lang
     * @param $resData
     * @param $lang
     * @return string
     */
    private function restaurantTranslate($resData, $lang)
    {
        if (isset($resData['en'])) {
            $resName = isset($resData[$lang]) ? $resData[$lang] : $resData['en'];
        } else {
            $resName = '';
        }
        return $resName;
    }


    private function getDeliveryTime($order)
    {
//        if(empty($order["delivery_time"])) return "haha";
        return (empty($order["delivery_time"])) ? "" : Carbon::parse($order["delivery_time"])->format("Y-m-d h:i A");
//
//        if ($order["pre_order"] == 1) {
//            return Carbon::parse($order["delivery_or_pickup_time"])->format("Y-m-d h:i A");
//        } else {
//            return Carbon::parse($order["api_delivery_time"])->format("Y-m-d h:i A");
//        }
    }

    private function getPaymentMethodTranslation($paymentMethod, $lang)
    {
        /**
         * gets payment status from payment status table based on slug payment_status from payment table
         */
        $deliveryStatus = $this->paymentMethod->getSpecificPaymentMethodBySlug($paymentMethod);
        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (!$deliveryStatus) return false;
        $deliveryLanguage = $deliveryStatus->paymentMethodTranslation()->where("lang", $lang)->first();
        /**
         *
         */
        if (!$deliveryLanguage) {
            $deliveryLanguage = $deliveryStatus->paymentMethodTranslation()->where("lang", "en")->first();
            if (!$deliveryLanguage) return false;
        }
        return $deliveryLanguage["name"];
    }

    private function getDeliverytMethodTranslation($deliveryMethod, $lang)
    {
        /**
         * gets payment status from payment status table based on slug payment_status from payment table
         */

        $deliveryStatus = $this->shippingMethod->getSpecificShippingMethodBySlug($deliveryMethod);

        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (count($deliveryStatus) == 0) return false;

        $deliveryLanguage = $this->shippingMethodTranslation->getAllShippingMethodTranslationByLang($deliveryStatus[0]['id'], $lang);;

        /**
         *
         */
        if (count($deliveryLanguage) == 0) return false;
        return $deliveryLanguage[0]["name"];
    }
}