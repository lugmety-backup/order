<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 6/14/2017
 * Time: 7:32 AM
 */

namespace App\Http\Controllers;

use App\Repo\OrderInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Repo\OrderCommentInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use LogStoreHelper;
use Mockery\Exception;
use RoleChecker;
use Illuminate\Support\Facades\Input;
use Validator;

/**
 * Class OrderCommentController
 * @package App\Http\Controllers
 */
class OrderCommentController extends Controller
{
    /**
     * @var OrderCommentInterface
     */
    protected $orderComment;
    /**
     * @var LogStoreHelper
     */
    protected $logStoreHelper;

    /**
     * @var OrderInterface
     */
    protected $order;

    /**
     * OrderCommentController constructor.
     * @param $orderComment
     * @param $logStoreHelper
     * @param $order
     */
    public function __construct(OrderCommentInterface $orderComment ,LogStoreHelper $logStoreHelper,OrderInterface $order)
    {
        $this->orderComment = $orderComment;
        $this->logStoreHelper = $logStoreHelper;
        $this->order = $order;
    }

    /**
     * Displays all order comments based on order_id.Only visible to admin or super-admin
     * @param $order_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function viewAllOrderCommentByOrder($order_id){
        try {
            $request['limit']=Input::get("limit",10);
            /**
             * if limit param is other than integer and validation fails then limit is set to 10
             */
            if (!is_null($request['limit'])) {
                $rules = [
                    "limit"=>"numeric|min:1"];
                $validator = Validator::make($request, $rules);

                if ($validator->fails()) {
                    $request['limit']=10;
                }
            }
            $orderComments = $this->orderComment->getAllCommentsByOrderId($order_id,$request["limit"]);
            if(count($orderComments)==0){
                throw new Exception();
            }
            $userData =[];
            foreach ($orderComments as $comment){
                $userData[] = $comment["user_id"];
            }
            $userDatas["users"] = array_unique($userData);
            $oauthUrl = Config::get('config.oauth_base_url')."/review/users/list";
            $getUserDetailsForReview =  \RemoteCall::storeWithoutOauth($oauthUrl,$userDatas);
            if($getUserDetailsForReview["status"] != "200"){
                throw new \Exception();
            }
            $userDatasFromOauth = $getUserDetailsForReview["message"]["data"];
            if(empty($userDatasFromOauth)){
                throw new \Exception();
            }
            foreach ($orderComments as $key =>$orderComment){

                $orderComment["comment_at"] = Carbon::createFromFormat('Y-m-d H:i:s', $orderComment["updated_at"])->diffForHumans();
                $orderComment->makeHidden(["created_at","updated_at"]);
                foreach ($userDatasFromOauth as $userData){
                    if($orderComment->user_id !== $userData["id"]) continue;
                    $orderComment["name"] = implode(" ", array($userData["first_name"], $userData["middle_name"], $userData["last_name"]));
                }
                if(!isset($orderComment["name"])) unset($orderComments[$key]);
            }
            return response()->json([
                "status" => "200",
                "data" => $orderComments->appends(["limit" => $request["limit"]])->withPath("/order/$order_id/comment")
            ]);

          }catch (\Exception $ex){
            return response()->json([
                "status" => "404",
                "message" => "Empty Record"
            ],404);
        }
    }

    /**
     * Create Order Comment Only permitted user can create order comment
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createComment(Request $request){
        try {
            $this->validate($request, [
                "order_id" => "required|integer|min:1",
                "comment" => "required",
            ]);

        }catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try {
            DB::beginTransaction();
            $request["user_id"] = RoleChecker::getUser();
            try{
                $checkOrder = $this->order->getSpecificOrder($request["order_id"]);
            }
            catch (ModelNotFoundException $ex){
                $this->logStoreHelper->storeLogError(array("Order ",[
                    "status" => "404",
                    "message" => "Requested Order  not found"
                ]));
                return response()->json([
                    "status" => "404",
                    "message" => "Requested Order  not found"
                ],404);
            }

            $orderComment = $this->orderComment->createComment($request->all());
            $this->logStoreHelper->storeLogInfo(array("Order Comment Creation",[
                "status" => "200",
                "message" => "Order Comment created successfully",
                "data" => $orderComment
            ]));
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Order Comment created successfully"
            ]);
        }catch (\Exception $ex){

            $this->logStoreHelper->storeLogError(array("Order Comment Creation",[
                "status" => "500",
                "message" => "Error Creating Order"
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error Creating Order"
            ],500);
        }
    }

    /**
     * Update Order Comment Only permitted user can update order comment
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateComment($id,Request $request){
        try {
            $this->validate($request, [
                "comment" => "required",
            ]);

        }catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try {
            $userId = RoleChecker::getUser();
            DB::beginTransaction();
            /**
             * checks if user_id matches with the order comment to be updated
             */
            if($userId != $this->orderComment->getSpecificComment($id)->user_id){

                $this->logStoreHelper->storeLogError(array("Order Comment Creation",[
                    "status" => "403",
                    "message" => "You can update your comment only"
                ]));
                return response()->json([
                    "status" => "403",
                    "message" => "You can update your comment only"
                ],403);
            }
            $orderComment = $this->orderComment->updateComment($id,$request->all());
            $this->logStoreHelper->storeLogInfo(array("Order Comment Creation",[
                "status" => "200",
                "message" => "Order Comment update successfully",
                "data" => $orderComment
            ]));
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Order Comment updated successfully"
            ]);
        }
        catch (ModelNotFoundException $exception){
            $this->logStoreHelper->storeLogError(array("Order Comment Creation",[
                "status" => "404",
                "message" => "Requested Order Comment not found"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Requested Order Comment not found"
            ],404);
        }
        catch (\Exception $ex){

            $this->logStoreHelper->storeLogError(array("Order Comment Creation",[
                "status" => "500",
                "message" => "Error Updating Order"
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error Updating Order"
            ],500);
        }
    }

    public function deleteOrderComment($id){
        try{
            DB::beginTransaction();
            $userId = RoleChecker::getUser();
            $order = $this->orderComment->getSpecificComment($id);

            /**
             * checks if user_id matches with the order comment to be deleted
             */

            if($userId != $order->user_id){
                $this->logStoreHelper->storeLogError(array("Order Comment Creation",[
                    "status" => "403",
                    "message" => "You can delete your comment only"
                ]));
                return response()->json([
                    "status" => "403",
                    "message" => "You can delete your comment only"
                ],403);
            }
            //deletes the order comment
            $this->orderComment->deleteComment($order);
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Order Deleted Successfully"
            ],200);
        }
        catch (ModelNotFoundException $ex){
            $this->logStoreHelper->storeLogError(array("Error in query",[
                "status" => "404",
                "message" => "Requested Order Comment not found"
            ]));

            return response()->json([
                "status" => "404",
                "message" => "Requested Order Comment not found"
            ],404);
        }
        catch (\Exception $ex){

            $this->logStoreHelper->storeLogError(array("Error in query",[
                "status" => "404",
                "message" => "Requested Order Comment not found"
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Error Deleting order comment"
            ],500);
        }
    }



}