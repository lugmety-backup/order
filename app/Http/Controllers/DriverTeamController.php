<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/2/2018
 * Time: 12:41 PM
 */

namespace App\Http\Controllers;


use App\Events\SendMessage;
use App\Http\Helpers\ExcelExportHelper;
use App\Repo\DriverCollectionInterface;
use App\Repo\DriverTeamInterface;
use App\Repo\DriverTeamMemberInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use LogStoreHelper;
use RoleChecker;
use Settings;
class DriverTeamController extends Controller
{
    protected $log;
    protected $driver_team;
    protected $driver_member;
    protected $driver_collection;
    protected $excel;
    public function __construct(LogStoreHelper $log, DriverTeamInterface $driver_team, DriverTeamMemberInterface $driver_member, DriverCollectionInterface $driverCollection,  ExcelExportHelper $excel)
    {
        $this->log = $log;
        $this->driver_member = $driver_member;
        $this->driver_team = $driver_team;
        $this->driver_collection = $driverCollection;
        $this->excel = $excel;
    }

    /**
     * Get specific team details including active as well as inactive members
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificTeam($id){
        try{
            $teamDetail = $this->driver_team->getSpecificDataByColumnActiveInActive("id",$id);
            if(!$teamDetail){
                throw new ModelNotFoundException();
            }
            $members = $teamDetail->driverTeamMembers()->withTrashed()->get();
            $team["users"] = collect($members)->pluck("driver_id")->toArray();
            $team["users"][count($team["users"])] = $teamDetail->created_by;
            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list?error=true",$team);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }
            $userDetails = collect($getUsersDetail["message"]["data"]);

            $teamDetail["admin_detail"] = $this->getUserFilteredData($userDetails->where("id",$teamDetail->created_by)->first());
            $teamDetail["captain_detail"] =  $this->getUserFilteredData($userDetails->where("id",$teamDetail->captain_id)->first());
            $count = 0;
            $membersDetail = [];
            foreach ($userDetails as $user){
                foreach ($members as $teamMember){
                    if($teamMember["driver_id"] == $user["id"]){

                        $membersDetail[$count] = $this->getUserFilteredData($user);
                        $membersDetail[$count]["driver_id"] = $teamMember["driver_id"];
                        $membersDetail[$count]["status"] = $teamMember["status"];
                        $count = $count +1;
                    }
                }
            }
            $teamDetail["members"] = $membersDetail;
            return response()->json([
                "status" => "200",
                "data" => $teamDetail
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError([
                "error updating driver team info",[
                    "id" => $id,
                    "message" => 'Team could not be found'
                ]
            ]);
            return response()->json([
                "status" => "404",
                "message" => "Team does not exist or not active."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error viewing specific team details ",[
                    "data" => ["id" => $id],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error viewing team detail."
            ],500);
        }
    }

    /**
     * get assigned drivers including captain driver
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAssignedDrivers(Request $request){
        try {
            if($request->has("code")){
                if(!Hash::check("Alice123$", $request->code)){
                    return response()->json([
                        "status" => "403",
                        "message" => "Forbidden"
                    ],403);
                }
            }
            else{
                return response()->json([
                    "status" => "403",
                    "message" => "Forbidden"
                ],403);
            }
            $getAssignedDrivers = $this->driver_member->getAllTeamMembers();
            $getDrivers = collect($getAssignedDrivers)->pluck("driver_id")->values()->all();
            $allTeams =  $this->driver_team->getALlTeamsForAdmin()->pluck("captain_id")->values()->all();
            $drivers["members"] = array_values(array_diff($getDrivers,$allTeams));
            $drivers["captains"] = $allTeams;
            return response()->json([
                "status" => "200",
                "data" => $drivers
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error adding new drivers ",[
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error viewing assigned driver team member(s)."
            ],500);
        }

    }

    /**
     * This endpoint will list all the teams with their members count.
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllTeamInfo(Request $request){

        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1",
                "city_id" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try{
            $this->validate($request,[
                "limit" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            $request->merge([
                "limit" => 10
            ]);
        }
        try{

            $allTeams =  $this->driver_team->getAllTeamForAdmin($request->all());
            $userData = $allTeams->items();
            if(empty($userData) || count($userData) == 0) {
                return response()->json([
                    "status" => '200',
                    "data" => $allTeams
                ]);
            }
            $teamCaptainIds["users"] = collect($userData)->pluck("captain_id")->values()->all();
            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list",$teamCaptainIds);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }

            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/country/$request->country_id");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];
            $timezone = $getCountryDetail["message"]["data"]["timezone"];
            foreach ($allTeams->items() as $team){
                $dateConverter = $this->convertUtcTime($team, $timezone);
                if(isset($dateConverter["created_at"])){
                    $info["created_at"] = $dateConverter["created_at"];
                }
                if(isset($dateConverter["admin_received_at"])){
                    $info["admin_received_at"] = $dateConverter["admin_received_at"];
                }
                if(isset($dateConverter["leader_received_at"])){
                    $info["leader_received_at"] = $dateConverter["leader_received_at"];
                }

                $team["captain_name"] = "";
                $team["collectable_amount"] = $currency . number_format($team["collectable_amount"],2);
                foreach ($getUsersDetail["message"]["data"] as $users){
                    if($team["captain_id"] == $users["id"] ){
                        $team["captain_name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($users['first_name'], $users['middle_name'], $users['last_name']))));
                    }
                }
            }
            return response()->json([
                "status" => "200",
                "data" => $allTeams
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error listing driver teams",[
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing driver teams"
            ],500);
        }
    }

    /**
     * get individual lifetime dashboard stats
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTeamDashboardStats($id){

        try {
            $getTeamDashboard = $this->driver_team->getIndividualTeamDashboardStats($id);
            if(count($getTeamDashboard) == 0){
                return response()->json([
                    "status" => "404",
                    "message" => "Team could not be found."
                ],404);
            }
            $country_id = $getTeamDashboard->country_id;
            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/country/$country_id");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];
//            $timezone = $getCountryDetail["message"]["data"]["timezone"];
            $getTeamDashboard["currency"] =  $currency;

            return response()->json([
                'status' => '200',
                'data' => $getTeamDashboard
            ]) ;
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error listing team dashboard stats",[
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing team dashboard statistics."
            ],500);
        }
    }

    /**
     * get all cash orders for each teams
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCashOrders(Request $request){
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1",
                "city_id" => "required|integer|min:1",
                "start_date" => "date_format:Y-m-d",
                "end_date" => "date_format:Y-m-d"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try{
            $this->validate($request,[
                "limit" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            $request->merge([
                "limit" => 10
            ]);
        }
        try {
            $originalRequest = $request->all();

            $getSettingDayLimit = Settings::getSettings('day-limit-for-viewing-captain-record');
            if ($getSettingDayLimit["status"] == "200") {
                $daylimit = (int)$getSettingDayLimit["message"]["data"]["value"];
            } else {
                $daylimit =7;
            }
            if($request->has("start_date") && !$request->has("end_date") ){
                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::now('utc')->toDateTimeString()
                ]);
            }
            elseif (!$request->has("start_date") && $request->has("end_date")){

                $request->merge([
                    "start_date" =>Carbon::parse($request->end_date)->tz('utc')->subDays($daylimit)->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            }
            elseif ($request->has("start_date") && $request->has("end_date")){

                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            }
            else{
                $start_date = Carbon::now("utc")->subDays($daylimit)->startOfDay()->toDateTimeString();
                $end_date =  Carbon::now("utc")->endOfDay()->toDateTimeString();
                $request->merge([
                    "start_date" => $start_date,
                    "end_date" => $end_date
                ]);
            }

            /**
             * gets cash orders records ,original request is used to display in pagination url
             */
            $getAllCashOrders = $this->driver_team->getAllTeamForCashOrdersAdmin($request->all(),$originalRequest);
            $getRecordIndex = $getAllCashOrders["records"]->items();
            if(empty($getRecordIndex)) goto skipForEmptyRecords;
            $teamCaptainIds["users"] = collect($getRecordIndex)->pluck("captain_id")->values()->all();
            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list",$teamCaptainIds);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }
            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/country/$request->country_id");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];
            $timezone = $getCountryDetail["message"]["data"]["timezone"];
            $getAllCashOrders["summary"]["total_receivable_amount"] = $currency. $getAllCashOrders["summary"]["total_receivable_amount"] ;
            $getAllCashOrders["summary"]["total_collected_amount"] = $currency. $getAllCashOrders["summary"]["total_collected_amount"] ;
            foreach ($getRecordIndex as $team){

                $dateConverter = $this->convertUtcTime($team, $timezone);
                if(isset($dateConverter["created_at"])){
                    $info["created_at"] = $dateConverter["created_at"];
                }
                if(isset($dateConverter["admin_received_at"])){
                    $info["admin_received_at"] = $dateConverter["admin_received_at"];
                }
                if(isset($dateConverter["leader_received_at"])){
                    $info["leader_received_at"] = $dateConverter["leader_received_at"];
                }
                $team["captain_name"] = "";
                $team["collectable_amount_from_captain"] = $currency . number_format($team["collectable_amount_from_captain"],2);
                $team["collected_amount_from_driver"] = $currency . number_format($team["collected_amount_from_driver"],2);
                foreach ($getUsersDetail["message"]["data"] as $users){
                    if($team["captain_id"] == $users["id"] ){
                        $team["captain_name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($users['first_name'], $users['middle_name'], $users['last_name']))));
                    }
                }
            }
            skipForEmptyRecords:
            $finalAllRecords = collect($getAllCashOrders["summary"])->merge($getAllCashOrders["records"]);
            return response()->json([
                "status" => "200",
                "data" => $finalAllRecords ,
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error listing cash orders",[
                    "data" => $request->all(),
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing cash orders"
            ],500);
        }

    }

    /**
     * get specific member of a team order history
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function  getAllDriverHistoryOfTeam(Request $request){
        try{
            $this->validate($request,[
                "team_id" => "required|integer|min:1",
                "driver_id" => "sometimes|integer|min:1",
                "start_date" => "date_format:Y-m-d",
                "end_date" => "date_format:Y-m-d",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try{
            $this->validate($request,[
                "limit" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            $request->merge([
                "limit" => 10
            ]);
        }
        try {
            $originalRequest = $request->all();
            $getSettingDayLimit = Settings::getSettings('day-limit-for-viewing-captain-record');
            if ($getSettingDayLimit["status"] == "200") {
                $daylimit = (int)$getSettingDayLimit["message"]["data"]["value"];
            } else {
                $daylimit =7;
            }
            if ($request->has("start_date") && !$request->has("end_date")) {
                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::now('utc')->toDateTimeString()
                ]);
            } elseif (!$request->has("start_date") && $request->has("end_date")) {

                $request->merge([
                    "start_date" => Carbon::parse($request->end_date)->tz('utc')->subDays($daylimit)->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            } elseif ($request->has("start_date") && $request->has("end_date")) {

                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            } else {
                $start_date = Carbon::now("utc")->subDays($daylimit)->startOfDay()->toDateTimeString();
                $end_date = Carbon::now("utc")->endOfDay()->toDateTimeString();
                $request->merge([
                    "start_date" => $start_date,
                    "end_date" => $end_date
                ]);
            }
            $getTeamDriversInfo = $this->driver_collection->getSpecificTeamMemberOrderHistory($request->all(),$originalRequest);
            $getPaginationData = $getTeamDriversInfo->items();
            if(count($getPaginationData) == 0 ){
                return response()->json([
                    "status" => "200",
                    "data" => $getTeamDriversInfo
                ]);
            }
            $driverIds["users"] = collect($getPaginationData)->pluck("driver_id")->values()->toArray();
            $admins =  collect($getPaginationData)->pluck("admin_id")->values()->toArray();
            $driverIds["users"] = array_values(array_unique(array_merge($driverIds["users"] ,$admins)));
            $countryId = collect($getPaginationData)->pluck("country_id")->first();
            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list",$driverIds);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }
            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/public/country/$countryId");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }

            $userDetails = $getUsersDetail["message"]["data"];
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];
            $timezone =  $getCountryDetail["message"]["data"]["timezone"];
            foreach ($getTeamDriversInfo as $info){
                $dateConverter = $this->convertUtcTime($info, $timezone);
                if(isset($dateConverter["created_at"])){
                    $info["created_at"] = $dateConverter["created_at"];
                }
                if(isset($dateConverter["admin_received_at"])){
                    $info["admin_received_at"] = $dateConverter["admin_received_at"];
                }
                if(isset($dateConverter["leader_received_at"])){
                    $info["leader_received_at"] = $dateConverter["leader_received_at"];
                }

                $info["driver_name"] = "";
                $info["status"] = "Collected By Admin";
                $info["driver_name"] = "";
                if(!empty($info["admin_id"]) && ($info["admin_id"] > 0)){
                    $info["status"] = "Collected By Admin";
                }
                elseif(is_null($info["leader_received_at"])){
                    $info["status"] = "Pending Collection By Captain";
                }
                else{
                    $info["status"] = "Collected By Captain";
                }
                foreach ($userDetails as $user){
                    if($info["driver_id"] == $user["id"]){
                        $info["driver_name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($user['first_name'], $user['middle_name'], $user['last_name']))));
                        $info["amount"] = $currency. number_format($info["amount"],2);
                    }
                }
            }

            return response()->json([
                "status" => "200",
                "data" => $getTeamDriversInfo
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error listing team member history",[
                    "data" => $request->all(),
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing team member history"
            ],500);
        }
    }

    /**
     * gets specified team driver and driver_id admin collected order history
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function collectionHistoryTeamDetail(Request $request){
        try{
            $this->validate($request,[
                "team_id" => "required|integer|min:1",
                "driver_id" => "sometimes|integer|min:1",
                "start_date" => "date_format:Y-m-d",
                "end_date" => "date_format:Y-m-d",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try{
            $this->validate($request,[
                "limit" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            $request->merge([
                "limit" => 10
            ]);
        }
        try {
            $originalRequest = $request->all();
            $getSettingDayLimit = Settings::getSettings('day-limit-for-viewing-captain-record');
            if ($getSettingDayLimit["status"] == "200") {
                $daylimit = (int)$getSettingDayLimit["message"]["data"]["value"];
            } else {
                $daylimit =7;
            }
            if ($request->has("start_date") && !$request->has("end_date")) {
                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::now('utc')->toDateTimeString()
                ]);
            } elseif (!$request->has("start_date") && $request->has("end_date")) {

                $request->merge([
                    "start_date" => Carbon::parse($request->end_date)->tz('utc')->subDays($daylimit)->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            } elseif ($request->has("start_date") && $request->has("end_date")) {

                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            } else {
                $start_date = Carbon::now("utc")->subDays($daylimit)->startOfDay()->toDateTimeString();
                $end_date = Carbon::now("utc")->endOfDay()->toDateTimeString();
                $request->merge([
                    "start_date" => $start_date,
                    "end_date" => $end_date
                ]);
            }
            $getTeamDriversInfo = $this->driver_collection->getSpecificDriverCollectedCollectionHistoryDetailBasedOnTeamIdForDriverForAdmin($request->all(),$originalRequest);
            $getPaginationData = $getTeamDriversInfo->items();
            if(count($getPaginationData) == 0 ){
                return response()->json([
                    "status" => "200",
                    "data" => $getTeamDriversInfo
                ]);
            }
            $driverIds["users"] = collect($getPaginationData)->pluck("driver_id")->values()->toArray();
            $admins =  collect($getPaginationData)->pluck("admin_id")->values()->toArray();
            $driverIds["users"] = array_values(array_unique(array_merge($driverIds["users"] ,$admins)));
            $countryId = collect($getPaginationData)->pluck("country_id")->first();
            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list",$driverIds);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }
            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/public/country/$countryId");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }
            $userDetails = $getUsersDetail["message"]["data"];
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];
            $timezone = $getCountryDetail["message"]["data"]["timezone"];

            foreach ($getTeamDriversInfo as $info){
                $info["driver_name"] = "";
                $info["status"] = "Collected By Admin";
                foreach ($userDetails as $adminUser) {
                    if($adminUser["id"] == $info["admin_id"]){
                        $adminName = preg_replace('/\s+/', ' ', trim(implode(" ", array($adminUser['first_name'], $adminUser['middle_name'], $adminUser['last_name']))));
                        $info["status"] = "Collected By Admin $adminName";
                        break;
                    }
                }
                $dateConverter = $this->convertUtcTime($info, $timezone);
                if(isset($dateConverter["created_at"])){
                    $info["created_at"] = $dateConverter["created_at"];
                }
                if(isset($dateConverter["admin_received_at"])){
                    $info["admin_received_at"] = $dateConverter["admin_received_at"];
                }
                if(isset($dateConverter["leader_received_at"])){
                    $info["leader_received_at"] = $dateConverter["leader_received_at"];
                }

                foreach ($userDetails as $user){
                    if($info["driver_id"] == $user["id"]){
                        $info["driver_name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($user['first_name'], $user['middle_name'], $user['last_name']))));
                        $info["amount"] = $currency. number_format($info["amount"],2);
                    }
                }
            }

            return response()->json([
                "status" => "200",
                "data" => $getTeamDriversInfo
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error listing team collection history",[
                    "data" => $request->all(),
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing team collection history"
            ],500);
        }
    }

    /**
     * gets the specific team members cash order history based on the passed driverId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTeamCashOrderDetail(Request $request){
        try{
            $this->validate($request,[
                "team_id" => "required|integer|min:1",
                "driver_id" => "sometimes|integer|min:1",
                "start_date" => "date_format:Y-m-d",
                "end_date" => "date_format:Y-m-d",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try{
            $this->validate($request,[
                "limit" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            $request->merge([
                "limit" => 10
            ]);
        }
        try {
            $originalRequest = $request->all();
            $getSettingDayLimit = Settings::getSettings('day-limit-for-viewing-captain-record');
            if ($getSettingDayLimit["status"] == "200") {
                $daylimit = (int)$getSettingDayLimit["message"]["data"]["value"];
            } else {
                $daylimit =7;
            }
            if ($request->has("start_date") && !$request->has("end_date")) {
                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::now('utc')->toDateTimeString()
                ]);
            } elseif (!$request->has("start_date") && $request->has("end_date")) {

                $request->merge([
                    "start_date" => Carbon::parse($request->end_date)->tz('utc')->subDays($daylimit)->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            } elseif ($request->has("start_date") && $request->has("end_date")) {

                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            } else {
                $start_date = Carbon::now("utc")->subDays($daylimit)->startOfDay()->toDateTimeString();
                $end_date = Carbon::now("utc")->endOfDay()->toDateTimeString();
                $request->merge([
                    "start_date" => $start_date,
                    "end_date" => $end_date
                ]);
            }
            $getTeamDriversInfo = $this->driver_collection->getSpecificDriverCollectedCollectionBasedOnTeamIdForDriverForAdmin($request->all(),$originalRequest);
            $getPaginationData = $getTeamDriversInfo->items();
            if(count($getPaginationData) == 0 ){
                return response()->json([
                    "status" => "200",
                    "data" => $getTeamDriversInfo
                ]);
            }
            $driverIds["users"] = collect($getPaginationData)->pluck("driver_id")->values()->toArray();
            $countryId = collect($getPaginationData)->pluck("country_id")->first();
            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list",$driverIds);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }
            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/public/country/$countryId");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }
            $userDetails = $getUsersDetail["message"]["data"];
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];
            $timezone = $getCountryDetail["message"]["data"]["timezone"];
            foreach ($getTeamDriversInfo as $info){
                $info["driver_name"] = "";
                if(is_null($info["leader_received_at"])){
                    $info["status"] = "Pending Collection By Captain";
                }
                else{
                    $info["status"] = "Collected By Captain";
                }
                $dateConverter = $this->convertUtcTime($info, $timezone);
                if(isset($dateConverter["created_at"])){
                    $info["created_at"] = $dateConverter["created_at"];
                }
                if(isset($dateConverter["admin_received_at"])){
                    $info["admin_received_at"] = $dateConverter["admin_received_at"];
                }
                if(isset($dateConverter["leader_received_at"])){
                    $info["leader_received_at"] = $dateConverter["leader_received_at"];
                }

                foreach ($userDetails as $user){
                    if($info["driver_id"] == $user["id"]){
                        $info["driver_name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($user['first_name'], $user['middle_name'], $user['last_name']))));
                        $info["amount"] = $currency. number_format($info["amount"],2);
                    }
                }

            }

            return response()->json([
                "status" => "200",
                "data" => $getTeamDriversInfo
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error listing team cash orders history",[
                    "data" => $request->all(),
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing team cash orders history"
            ],500);
        }
    }

    public function exportTeamsCollectionHistory(Request $request){
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1",
                'city_id' => "required|integer|min:1",
                "start_date" => "date_format:Y-m-d",
                "end_date" => "date_format:Y-m-d",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try{
            $this->validate($request,[
                "limit" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            $request->merge([
                "limit" => 10
            ]);
        }

        try{
            $getSettingDayLimit = Settings::getSettings('day-limit-for-viewing-captain-record');
            if ($getSettingDayLimit["status"] == "200") {
                $daylimit = (int)$getSettingDayLimit["message"]["data"]["value"];
            } else {
                $daylimit =7;
            }
            $originalRequest = $request->all();
            if($request->has("start_date") && !$request->has("end_date") ){
                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::now('utc')->toDateTimeString()
                ]);
            }
            elseif (!$request->has("start_date") && $request->has("end_date")){

                $request->merge([
                    "start_date" =>Carbon::parse($request->end_date)->tz('utc')->subDays($daylimit)->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            }
            elseif ($request->has("start_date") && $request->has("end_date")){

                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            }
            else{
                $start_date = Carbon::now("utc")->subDays($daylimit)->startOfDay()->toDateTimeString();
                $end_date =  Carbon::now("utc")->endOfDay()->toDateTimeString();
                $request->merge([
                    "start_date" => $start_date,
                    "end_date" => $end_date
                ]);
            }
            $getCollectionHistory = $this->driver_team->getAllTeamCollectionHistoryForAdmin($request->all(),$originalRequest);
            $items = $getCollectionHistory->items();
            if(empty($items)){
                return response()->json([
                    "status" => "200",
                    "data" => $getCollectionHistory
                ]);
            }
            //calling oauth service to get captain ids

            $driversId["users"] = collect($items)->pluck("captain_id")->values()->all();

            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list",$driversId);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }
//calling location service to get currency
            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/public/country/$request->country_id");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];

            foreach ($getCollectionHistory as $history){
                $history["captain_name"] = "";
                $history["collected_amount"] = $currency.number_format($history["collected_amount"]);
                foreach ($getUsersDetail["message"]["data"] as $user){
                    if($user["id"] = $history["captain_id"]){
                        $history["captain_name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($user['first_name'], $user['middle_name'], $user['last_name']))));
                    }
                }
            }
            //todo exports
            return $getCollectionHistory->items();

        }
        catch (\Exception $ex){

            $this->log->storeLogError([
                "error viewing collecting chistory",[
                    "data" => [$request->all()],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => '200',
                'message' => 'Error viewing team collection history.'
            ]);
        }
    }


    /**
     * List all the active or non active members based on the team id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllDriversListOfTeam($id){
        try{
            $getDriversList = $this->driver_member->getAllMembersByTeamIdSoftDeletedAsWell($id);
            $driverIds["users"] = $getDriversList;
            if(count($getDriversList) == 0){
                return response()->json([
                    'status' => '404',
                    "message" => "Empty team members."
                ],404);
            }
            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list",$driverIds);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }
            return response()->json([
                "status" => "200",
                "data" => $getUsersDetail["message"]["data"]
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error listing team memebers ",[
                    "data" => ["team_id" => $id],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing team members"
            ],500);
        }
    }

    /**
     * This function is used to collect driver collection amount from driver captain
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function receiveCollectionByAdmin($id){
        DB::beginTransaction();
        try{
            $getCollection = $this->driver_collection->getSpecificCollection($id);

            if(is_null($getCollection->leader_received_at)){
                return response()->json([
                    'status' => '403',
                    'message' => 'Driver Captain has not collected order amount from driver'
                ],403);
            }
            elseif (!is_null($getCollection->admin_received_at)){
                return response()->json([
                    'status' => '403',
                    'message' => 'Collection amount was already received.'
                ],403);
            }
            $data["admin_id"] = RoleChecker::getUser();
            $data["admin_received_at"] = Carbon::now('utc')->format("Y-m-d H:i:s");
            $this->driver_collection->updateCollection($id,$data);//insert the admin id with received date .
            //get team captain id
            $getTeamInfo = $this->driver_team->getAllRecordsByColumn('id',$getCollection->team_id);
            $captainId = $getTeamInfo[0]->captain_id;
            $array = [
                'service' => 'order service',
                'message' => 'order collected by admin',
                'data' => [
                    'user_details' => ['id' => $captainId],
                    'driver_id' => $getCollection->driver_id,
                    'order_id' => $getCollection->order_id,
                    'country_id' => $getCollection->country_id
                ]
            ];

            event(new SendMessage($array));
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => 'Successfully received collection amount.'
            ],200);
        }

        catch (ModelNotFoundException $ex){

            $this->log->storeLogError([
                "error updating driver collection",[
                    "data" =>["id" => $id],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                'status' => '404',
                'message' => 'Driver Collection could not be found'
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error updating driver collection",[
                    "data" =>["id" => $id],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error updating driver collection"
            ],500);
        }
    }


    /**
     * receives all the order amounts collected by driver captain for a specific @param $teamId
     * @param Request $request
     * @param $teamId
     * @return \Illuminate\Http\JsonResponse
     */
    public function receiveAllCollectionByAdmin(Request $request, $teamId){
        try{
            $this->validate($request,[
                "start_date" => "date_format:Y-m-d",
                "end_date" => "date_format:Y-m-d",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try {
            /**
             * receive the day limit from setting if any error is obtained then default date is set to 7
             */
            $getSettingDayLimit = Settings::getSettings('day-limit-for-viewing-captain-record');
            if ($getSettingDayLimit["status"] == "200") {
                $daylimit = (int)$getSettingDayLimit["message"]["data"]["value"];
            } else {
                $daylimit = 7;
            }

            if ($request->has("start_date") && !$request->has("end_date")) {
                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::now('utc')->toDateTimeString()
                ]);
            } elseif (!$request->has("start_date") && $request->has("end_date")) {

                $request->merge([
                    "start_date" => Carbon::parse($request->end_date)->tz('utc')->subDays($daylimit)->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            } elseif ($request->has("start_date") && $request->has("end_date")) {

                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            } else {
                $start_date = Carbon::now("utc")->subDays($daylimit)->startOfDay()->toDateTimeString();
                $end_date = Carbon::now("utc")->endOfDay()->toDateTimeString();
                $request->merge([
                    "start_date" => $start_date,
                    "end_date" => $end_date
                ]);
            }
            $request->merge([
                'admin_received_at' => Carbon::now('utc'),
                'admin_id' => RoleChecker::getUser()
            ]);
            /**
             * updates all the captain collection
             */
            $receiveAllCaptainCollectedOrder = $this->driver_collection->updateALLDriverCaptainCollectedCollectionBasedOnTeamIdForAdmin($request->all(),$teamId);
            DB::commit();
            return response()->json([
                'status' => '200',
                'message' => "Successfully received all collection amount for date interval $request->start_date and $request->end_date."
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error updating all driver collection",[
                    "data" =>["teamId" => $teamId],
                    "request" => $request->all(),
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error updating driver collection"
            ],500);
        }
    }

    /**
     * list the drivers by driver captain
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDriversInfoForCaptain(Request $request){

        try{
            $this->validate($request,[
                "start_date" => "date_format:Y-m-d",
                "end_date" => "date_format:Y-m-d",
                "country_id" => "required|integer:min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            if(!RoleChecker::hasRole('driver-captain')){
                return response()->json([
                    "status" => "403",
                    "message" => "Only driver captain has access."
                ]);
            }

//todo get day limit from setting
            $getSettingDayLimit = Settings::getSettings('day-limit-for-viewing-captain-record');
            if ($getSettingDayLimit["status"] == "200") {
                $daylimit = (int)$getSettingDayLimit["message"]["data"]["value"];
            } else {
                $daylimit =7;
            }
            if($request->has("start_date") && !$request->has("end_date") ){
                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::now('utc')->toDateTimeString()
                ]);
            }
            elseif (!$request->has("start_date") && $request->has("end_date")){

                $request->merge([
                    "start_date" =>Carbon::parse($request->end_date)->tz('utc')->subDays($daylimit)->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            }
            elseif ($request->has("start_date") && $request->has("end_date")){

                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            }
            else{
                $start_date = Carbon::now("utc")->subDays($daylimit)->startOfDay()->toDateTimeString();
                $end_date =  Carbon::now("utc")->endOfDay()->toDateTimeString();
                $request->merge([
                    "start_date" => $start_date,
                    "end_date" => $end_date
                ]);
            }

            $request->merge([
                "captain_id" => RoleChecker::getUser()
            ]);
            $teamMembersInfo =  $this->driver_member->getDriversInfoForCaptain($request->all());
            if(count($teamMembersInfo["list"]) == 0) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty driver collection"
                ]);
            }
            $teamMembersInfo["summary"]["days_limit"] = $daylimit;
            $driversId["users"] = collect($teamMembersInfo["list"])->pluck("driver_id")->values()->all();

            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list",$driversId);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }
            $countryId = $teamMembersInfo["list"][0]["country_id"];
            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/public/country/$countryId");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];

            foreach ($teamMembersInfo["list"] as $team){
                $team["is_yourself"] = false;
                $team["driver_name"] = "";
                $team["mobile"] = "";
                $team["currency"] = $currency;
                $team["collectable_amount_form_driver"] =  number_format($team["collectable_amount_form_driver"],2);
                foreach ($getUsersDetail["message"]["data"] as $users){
                    if($team["driver_id"] == $users["id"] ){
                        if($team["driver_id"] == $request->captain_id){
                            $team["is_yourself"] = true;
                        }
                        $team["mobile"] = preg_replace('/\s+/', '', trim(implode(" ", array($users['country_code'], $users['mobile']))));;
                        $team["driver_name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($users['first_name'], $users['middle_name'], $users['last_name']))));
                    }
                }
            }

            $teamMembersInfo["summary"]["currency"] = $currency;
            return response()->json([
                "status" => "200",
                "data" => $teamMembersInfo
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error listing drivers info for captain",[
                    "data" => $request->all(),
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing drivers info"
            ],500);
        }
    }

    /**
     * get specific driver to be received order collection by driver captain
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificDriverCollectionDetail( Request $request, $driverId){


        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try{

            $this->validate($request,[
                "limit" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            $request->merge([
                "limit" => 10
            ]);
        }
        try{
            if(!RoleChecker::hasRole('driver-captain')){
                return response()->json([
                    "status" => "403",
                    "message" => "Only driver captain has access."
                ]);
            }
            $getCaptainId = RoleChecker::getUser();
            $getTeamDetail = $this->driver_team->getSpecificDataByColumn("captain_id",$getCaptainId);

            if(!$getTeamDetail){
                throw new ModelNotFoundException();
            }
            $request->merge([
                "team_id" => $getTeamDetail->id

            ]);
            $getSpecificTeamDetail = $this->driver_collection->getSpecificDriverCollectionBasedOnTeamId($request->all(),$driverId);

            $drivers["users"] = [$driverId];
            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list",$drivers);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }
            $users = $getUsersDetail["message"]["data"][0];
            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/public/country/$request->country_id");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];
            $userInfo["summary"] = [
                "mobile" => preg_replace('/\s+/', '', trim(implode(" ", array($users['country_code'], $users['mobile'])))),
                "name" => preg_replace('/\s+/', ' ', trim(implode(" ", array($users['first_name'], $users['middle_name'], $users['last_name'])))),
//                "address" => preg_replace('/\s+/', ' ', trim(implode(" ", array($users['address_lin'], $users['middle_name'], $users['last_name'])))),
                "total_collection_amount" => $getSpecificTeamDetail["summary"]["summary"]["total_collection_amount"],
                "currency" => $currency
            ];
            $getSpecificTeamDetails = collect($userInfo)->merge($getSpecificTeamDetail["info"]);

            return response()->json([
                "status" => "200",
                "data" => $getSpecificTeamDetails
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError([
                "error listing  specific drivers remaining collection detail list",[
                    "data" => [$request->all(),"driver_id" => $driverId],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "404",
                "message" => "You are not assigned to any team."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error listing  specific drivers remaining collection detail list",[
                    "data" => [$request->all(),"driver_id" => $driverId],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing drivers receivable collection detail list"
            ],500);
        }
    }

    /**
     * get list of collected amount from driver members list by driver captain
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDriverCollectionCollectedHistory(Request $request){
        try{
            $this->validate($request,[
                "start_date" => "date_format:Y-m-d",
                "end_date" => "date_format:Y-m-d",
                "country_id" => "required|integer:min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            if(!RoleChecker::hasRole('driver-captain')){
                return response()->json([
                    "status" => "403",
                    "message" => "Only driver captain has access."
                ]);
            }


            $getSettingDayLimit = Settings::getSettings('day-limit-for-viewing-captain-record');
            if ($getSettingDayLimit["status"] == "200") {
                $daylimit = (int)$getSettingDayLimit["message"]["data"]["value"];
            } else {
                $daylimit =7;
            }
            Log::info("day-limit-from-setting",[$daylimit]);
            if($request->has("start_date") && !$request->has("end_date") ){
                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::now('utc')->toDateTimeString()
                ]);
            }
            elseif (!$request->has("start_date") && $request->has("end_date")){

                $request->merge([
                    "start_date" =>Carbon::parse($request->end_date)->tz('utc')->subDays($daylimit)->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            }
            elseif ($request->has("start_date") && $request->has("end_date")){

                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            }
            else{
                $start_date = Carbon::now("utc")->subDays($daylimit)->startOfDay()->toDateTimeString();
                $end_date =  Carbon::now("utc")->endOfDay()->toDateTimeString();
                $request->merge([
                    "start_date" => $start_date,
                    "end_date" => $end_date
                ]);
            }

            $request->merge([
                "captain_id" => RoleChecker::getUser()
            ]);
            $teamMembersInfo =  $this->driver_member->getDriverCashCollectionHistoryByCaptain($request->all());
            if(count($teamMembersInfo["list"]) == 0) {
                return response()->json([
                    "status" => "404",
                    "message" => "Empty driver collection"
                ],404);
            }
            $teamMembersInfo["summary"]["days_limit"] = $daylimit;
            $driversId["users"] = collect($teamMembersInfo["list"])->pluck("driver_id")->values()->all();

            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list",$driversId);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }
            $countryId = $teamMembersInfo["list"][0]["country_id"];
            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/public/country/$countryId");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];

            foreach ($teamMembersInfo["list"] as $team){
                $team["is_yourself"] = false;
                $team["driver_name"] = "";
                $team["mobile"] = "";
                $team["currency"] = $currency;
                $team["collected_amount_form_driver"] =  number_format($team["collected_amount_form_driver"],2);
                foreach ($getUsersDetail["message"]["data"] as $users){
                    if($team["driver_id"] == $users["id"] ){
                        if($team["driver_id"] == $request->captain_id){
                            $team["is_yourself"] = true;
                        }
                        $team["mobile"] = preg_replace('/\s+/', '', trim(implode(" ", array($users['country_code'], $users['mobile']))));;
                        $team["driver_name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($users['first_name'], $users['middle_name'], $users['last_name']))));
                    }
                }
            }

            return response()->json([
                "status" => "200",
                "data" => $teamMembersInfo["list"]
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error listing drivers collected info for captain",[
                    "data" => $request->all(),
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing drivers info"
            ],500);
        }

    }


    /**
     * get specific driver to received order collection by driver captain
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificDriverCollectionCollectedDetail( Request $request, $driverId){


        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try{

            $this->validate($request,[
                "limit" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            $request->merge([
                "limit" => 10
            ]);
        }
        try{
            if(!RoleChecker::hasRole('driver-captain')){
                return response()->json([
                    "status" => "403",
                    "message" => "Only driver captain has access."
                ]);
            }
            $getCaptainId = RoleChecker::getUser();
            $getTeamDetail = $this->driver_team->getSpecificDataByColumn("captain_id",$getCaptainId);

            if(!$getTeamDetail){
                throw new ModelNotFoundException();
            }
            $request->merge([
                "team_id" => $getTeamDetail->id

            ]);
            $getSpecificTeamDetail = $this->driver_collection->getSpecificDriverCollectedCollectionBasedOnTeamId($request->all(),$driverId);

            $drivers["users"] = [$driverId];
            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list",$drivers);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }
            $users = $getUsersDetail["message"]["data"][0];
            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/public/country/$request->country_id");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];
            $userInfo["summary"] = [
                "mobile" => preg_replace('/\s+/', '', trim(implode(" ", array($users['country_code'], $users['mobile'])))),
                "name" => preg_replace('/\s+/', ' ', trim(implode(" ", array($users['first_name'], $users['middle_name'], $users['last_name'])))),
//                "address" => preg_replace('/\s+/', ' ', trim(implode(" ", array($users['address_lin'], $users['middle_name'], $users['last_name'])))),
                "total_collection_amount" => $getSpecificTeamDetail["summary"]["summary"]["total_collection_amount"],
                "currency" => $currency
            ];
            $getSpecificTeamDetails = collect($userInfo)->merge($getSpecificTeamDetail["info"]);

            return response()->json([
                "status" => "200",
                "data" => $getSpecificTeamDetails
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError([
                "error listing  specific drivers collected collection detail list",[
                    "data" => [$request->all(),"driver_id" => $driverId],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "404",
                "message" => "You are not assigned to any team."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error listing  specific drivers collected collection detail list",[
                    "data" => [$request->all(),"driver_id" => $driverId],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing drivers received collection detail list"
            ],500);
        }
    }

    /**
     * create new drivers team
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTeam(Request $request){
        try{
            $this->validate($request,[
                "captain_id" => "required|integer|min:1",
                "country_id" => "required|integer|min:1",
                "city_id" => "required|integer|min:1",
                "name" => "required|string|max:255",
                "description" => "string",
//                "drivers_id" => "required|array",
//                "drivers_id.*" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        DB::beginTransaction();
        try{
            /**
             * checks if the passed driver is assigned to other team
             * @returns number of count
             */
            $request->merge([
                "created_by" => 1//RoleChecker::getUser()
            ]);
//            $driversId = array_unique(array_values($request->drivers_id));
            $driverAndCaptainList = [
                "captain_id" => $request->captain_id,
                "drivers_id" =>  []
            ];

            $fullCountryUrl = Config::get("config.location_base_url")."/check/country/$request->country_id/city/$request->city_id";
            $checkCountryAndCity = \RemoteCall::getSpecific($fullCountryUrl);
            if($checkCountryAndCity["status"] != "200"){
                if($checkCountryAndCity["status"] == "503"){
                    return response()->json([
                        "status" => "503",
                        "message" => $checkCountryAndCity["message"]
                    ],503);
                }
                else{
                    return response()->json([
                        "status" => (string)$checkCountryAndCity["status"],
                        "message" => $checkCountryAndCity["message"]["message"]
                    ],$checkCountryAndCity["status"]);
                }
            }

            /**
             * calling oauth service to check captain id and drivers id
             */
            $endpoint = Config::get("config.oauth_base_url")."/check/captain/driver";
            $checkCapainAndDrivers = \RemoteCall::store($endpoint,$driverAndCaptainList);

            if($checkCapainAndDrivers["status"] != "200"){
                if($checkCapainAndDrivers["status"] == "503"){
                    return response()->json([
                        "status" => "503",
                        "message" => $checkCapainAndDrivers["message"]
                    ],503);
                }
                else{
                    return response()->json([
                        "status" => (string)$checkCapainAndDrivers["status"],
                        "message" => $checkCapainAndDrivers["message"]
                    ],$checkCapainAndDrivers["status"]);
                }
            }
            $driversId[] = $request->captain_id;
            $request->merge([
                "drivers_id" => array_unique(array_values($driversId))
            ]);
            $checkDriverIsAssignedToOtherTeam = $this->driver_team->checkDriverIsAssignedToOtherTeam($request->captain_id,$request->drivers_id);
            if($checkDriverIsAssignedToOtherTeam ){
                return response()->json([
                    "status" => "403",
                    "message" => "Driver is assigned to $checkDriverIsAssignedToOtherTeam->name."
                ],403);
            }
            $createTeam = $this->driver_team->createNewRecord($request->all());
            $this->log->storeLogInfo(["creation of driver team",[
                "data" => $request->all()
            ]]);
            DB::commit();
            return response()->json([
                "status" => "200",
                'message' => 'Driver team created successfully.'
            ]);

        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error creating driver team",[
                    "data" => $request->all(),
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error creating team"
            ],500);
        }
    }

    /**
     * collect cash from driver by captain id by driver captain
     * @param Request $request
     * @param $collectionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function collectAmountByCaptainDriver(Request $request,$collectionId){
        DB::beginTransaction();
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        try{
            if(!RoleChecker::hasRole('driver-captain')){
                return response()->json([
                    "status" => "403",
                    "message" => "Only driver captain has access."
                ]);
            }
            $getTeamInfo = $this->driver_team->getCurrentActiveTeamId(RoleChecker::getUser());
            if(!$getTeamInfo){
                return response()->json([
                    "status" => "404",
                    "message" => "Team could not be found."
                ],404);
            }
            $checkCollectionExistInCountry = $this->driver_collection->getSpecificCollectionByTeamIDAndCountryId($collectionId,$getTeamInfo->id,$request->country_id);
            $data["leader_received_at"] = Carbon::now('utc');
            $this->driver_collection->updateCollection($collectionId,$data);

            $array = [
                'service' => 'order service',
                'message' => 'order collected by captain',
                'data' => [
                    'user_details' => ['id' => RoleChecker::getUser()],
                    'driver_id' => $checkCollectionExistInCountry->driver_id,
                    'order_id' => $checkCollectionExistInCountry->order_id,
                    'country_id' => $checkCollectionExistInCountry->country_id
                ]
            ];

            event(new SendMessage($array));
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "You have successfully collected the cash."
            ]);
        }
        catch (ModelNotFoundException $ex){

            $this->log->storeLogError([
                "error collecting cash from driver by captain",[
                    "data" => [$request->all(),"id" => $collectionId],
                    "message" => $ex->getMessage()
                ]
            ]);

            return response()->json([
                "status" => "404",
                "message" => "Driver Collection could not be found."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error collecting cash from driver by captain",[
                    "data" => [$request->all(),"id" => $collectionId],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ],500);
        }

    }

    public function getTeamsCollectionHistory(Request $request){
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1",
                'city_id' => "required|integer|min:1",
                "start_date" => "date_format:Y-m-d",
                "end_date" => "date_format:Y-m-d",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try{
            $this->validate($request,[
                "limit" => "required|integer|min:1"
            ]);
        }
        catch (\Exception $ex){
            $request->merge([
                "limit" => 10
            ]);
        }

        try{
            $getSettingDayLimit = Settings::getSettings('day-limit-for-viewing-captain-record');
            if ($getSettingDayLimit["status"] == "200") {
                $daylimit = (int)$getSettingDayLimit["message"]["data"]["value"];
            } else {
                $daylimit =7;
            }
            $originalRequest = $request->all();
            if($request->has("start_date") && !$request->has("end_date") ){
                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::now('utc')->toDateTimeString()
                ]);
            }
            elseif (!$request->has("start_date") && $request->has("end_date")){

                $request->merge([
                    "start_date" =>Carbon::parse($request->end_date)->tz('utc')->subDays($daylimit)->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            }
            elseif ($request->has("start_date") && $request->has("end_date")){

                $request->merge([
                    "start_date" => Carbon::parse($request->start_date)->tz('utc')->startOfDay()->toDateTimeString(),
                    "end_date" => Carbon::parse($request->end_date)->tz('utc')->endOfDay()->toDateTimeString()
                ]);
            }
            else{
                $start_date = Carbon::now("utc")->subDays($daylimit)->startOfDay()->toDateTimeString();
                $end_date =  Carbon::now("utc")->endOfDay()->toDateTimeString();
                $request->merge([
                    "start_date" => $start_date,
                    "end_date" => $end_date
                ]);
            }
//todo admin/teams/collection/history
            $getCollectionHistory = $this->driver_team->getAllTeamCollectionHistoryForAdmin($request->all(),$originalRequest);
            $items = $getCollectionHistory->items();
            if(empty($items)){
                return response()->json([
                    "status" => "200",
                    "data" => $getCollectionHistory
                ]);
            }
            //calling oauth service to get captain ids

            $driversId["users"] = collect($items)->pluck("captain_id")->values()->all();

            $getUsersDetail = \RemoteCall::store(Config::get("config.oauth_base_url")."/review/users/list",$driversId);
            if($getUsersDetail["status"] != "200"){
                if($getUsersDetail["status"] == "503"){
                    return response()->json($getUsersDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getUsersDetail["status"],
                        "message" => $getUsersDetail["message"]
                    ],$getUsersDetail["status"]);
                }
            }
//calling location service to get currency
            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/public/country/$request->country_id");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];
//return $getCollectionHistory;
            foreach ($getCollectionHistory as $history){
                $history["captain_name"] = "";
                $history["collected_amount"] = $currency.number_format($history["collected_amount"]);
                foreach ($getUsersDetail["message"]["data"] as $user){
                    if($user["id"] == $history["captain_id"]){
                        $history["captain_name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($user['first_name'], $user['middle_name'], $user['last_name']))));
                    }
                }
            }
            return response()->json([
                'status' => '200',
                'data' => $getCollectionHistory
            ]);

        }
        catch (\Exception $ex){

            $this->log->storeLogError([
                "error viewing collecting chistory",[
                    "data" => [$request->all()],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => '200',
                'message' => 'Error viewing team collection history.'
            ]);
        }
    }

    /**
     * add new members
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addDrivers($id ,Request $request){
        try{
            $this->validate($request,[
//                "captain_id" => "required|integer|min:1",
//                "name" => "required|string|max:255",
//                "description" => "string",
                "drivers_id" => "required|array",
                "drivers_id.*" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
            $request->merge([
                "drivers_id" => array_values(array_unique($request->drivers_id))
            ]);
            $checkTeamIdExist = $this->driver_team->getSpecificDataByColumn("id",$id);
            if(!$checkTeamIdExist){
                throw new ModelNotFoundException();
            }
            $checkDriverIsAssignedToOtherTeam = $this->driver_team->checkDriverIsAssignedToOtherTeam($checkTeamIdExist->captain_id,$request->drivers_id);
            if($checkDriverIsAssignedToOtherTeam){
                return response()->json([
                    "status" => "403",
                    "message" => "Driver is assigned to $checkDriverIsAssignedToOtherTeam->name."
                ],403);
            }

            $requestDrivers = $request->drivers_id;
            $driverAndCaptainList = [
                "captain_id" => 0,
                "drivers_id" =>  $requestDrivers
            ];
            $endpoint = Config::get("config.oauth_base_url")."/check/captain/driver";
            $checkCapainAndDrivers = \RemoteCall::store($endpoint,$driverAndCaptainList);
            if($checkCapainAndDrivers["status"] != "200"){
                if($checkCapainAndDrivers["status"] == "503"){
                    return response()->json([
                        "status" => "503",
                        "message" => $checkCapainAndDrivers["message"]
                    ],503);
                }
                else{
                    return response()->json([
                        "status" => (string)$checkCapainAndDrivers["status"],
                        "message" => $checkCapainAndDrivers["message"]["message"]
                    ],$checkCapainAndDrivers["status"]);
                }
            }
            $getTeamDrivers = $checkTeamIdExist->driverTeamMembers()->pluck("driver_id")->toArray();
            $getDeletedDrivers = $checkTeamIdExist->driverTeamMembers()->onlyTrashed()->pluck("driver_id")->values()->all();
            $uniqueDeletedDriver = array_values(array_diff($requestDrivers,$getDeletedDrivers));
            if(count($uniqueDeletedDriver) !== count($requestDrivers )){
                return response()->json([
                    "status" => "403",
                    "message" => "Deleted member(s) cannot be added directly from here.Please restore the member."
                ],403);
            }
            $uniqueDriver = array_values(array_diff($requestDrivers,$getTeamDrivers));
            $request->merge([
                "drivers_id" => $uniqueDriver
            ]);
            $updateTeam = $this->driver_team->updateTeamMember($id,$request->all());
            $this->log->storeLogInfo(["adding new driver team member",[
                "data" => $request->all(),
                "id" => $id
            ]]);
            DB::commit();
            return response()->json([
                "status" => "200",
                'message' => 'Driver team member updated successfully.'
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError([
                "error updating driver team",[
                    "data" => $request->all(),
                    "id" => $id,
                    "message" => 'Team could not be found'
                ]
            ]);
            return response()->json([
                "status" => "404",
                "message" => "Team does not exist."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error adding new drivers ",[
                    "data" => $request->all(),
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error adding new driver team member(s)."
            ],500);
        }




    }

    /**
     * Update team basic info like name and description but wont change the captain id
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTeamInfo($id ,Request $request){
        try{
            $this->validate($request,[
//                "captain_id" => "required|integer|min:1",
                "name" => "required|string|max:255",
                "description" => "string",
//                "status" => "required|integer|min:0|max:1",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
            /**
             * check if the team id exists or not in database
             */
            $checkTeamIdExist = $this->driver_team->getSpecificDataByColumnActiveInActive("id",$id);
            if(!$checkTeamIdExist){
                throw new ModelNotFoundException();
            }
            /**
             * updates the team info except captain id
             */
            $updateTeam = $this->driver_team->updateTeamRecord($id,$request->except("captain_id"));
            $this->log->storeLogInfo(["updating driver team info",[
                "data" => $request->all(),
                "id" => $id
            ]]);
            DB::commit();
            return response()->json([
                "status" => "200",
                'message' => 'Driver team info updated successfully.'
            ]);
            return response()->json([
                "status" => "200",
                "message" => "Team updated info successfully."
            ],200);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError([
                "error updating driver team info",[
                    "data" => $request->all(),
                    "id" => $id,
                    "message" => 'Team could not be found'
                ]
            ]);
            return response()->json([
                "status" => "404",
                "message" => "Team does not exist."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error updating driver team info",[
                    "data" => $request->all(),
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error updating team info."
            ],500);
        }
    }

    /**
     * Delete the team
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTeam($id){
        DB::beginTransaction();
        try{
            $getTeamInfo = $this->driver_team->getSpecificDataByColumn("id",$id);
            if(!$getTeamInfo){
                throw new ModelNotFoundException();
            }
            $this->driver_team->softDeleteById($id);
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Team removed successfully."
            ]);

        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError([
                "error deleting driver team member",[
                    "id" => $id,
                    "message" => 'Team could not be found'
                ]
            ]);
            return response()->json([
                "status" => "404",
                "message" => "Team does not exist."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error deleting driver team member",[
                    "data" => [
                        "team_id" => $id
                    ],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error creating team"
            ],500);
        }
    }

    /**
     * restore
     * @param $id
     * @param $driverId
     * @return \Illuminate\Http\JsonResponse
     */
    public function restoreTeamMember($id,$driverId){
        DB::beginTransaction();
        try{
            $getTeamDetail = $this->driver_team->getSpecificDataByColumn("id",$id);
            if(!$getTeamDetail){
                throw new ModelNotFoundException();
            }

            $checkDriverIsAssignedToOtherTeam = $this->driver_team->checkDriverIsAssignedToOtherTeam($getTeamDetail->captain_id,[$driverId]);
            if($checkDriverIsAssignedToOtherTeam){
                return response()->json([
                    "status" => "403",
                    "message" => "Driver is assigned to $checkDriverIsAssignedToOtherTeam->name."
                ],403);
            }
            /**
             * gets soft deleted team members
             */
            $checkDriverExists = $getTeamDetail->driverTeamMembers()->onlyTrashed()->where("driver_id",$driverId)->first();
            if(!$checkDriverExists){
                return response()->json([
                    "status" => "403",
                    "message" => "Driver member does not belongs to the current team or the member is currently active."
                ],403);
            }
            //restore from soft deleting
            $restoreDeletedMembers = $checkDriverExists->restore();
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Team member restored successfully."
            ],200);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError([
                "error restoring driver team member",[
                    "id" => $id,
                    "message" => 'Team could not be found'
                ]
            ]);
            return response()->json([
                "status" => "404",
                "message" => "Team does not exist."
            ],404);
        }

        catch (\Exception $ex){
            $this->log->storeLogError([
                "error restoring driver team member",[
                    "id" => $id,
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error restoring team member"
            ],500);
        }
    }

    /**
     * Remove one or more drivers from the team , if the team doesnot belongs to current team then respective error message will be displayed
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeDrivers($id ,Request $request){
        try{
            $this->validate($request,[
//                "captain_id" => "required|integer|min:1",
//                "name" => "required|string|max:255",
//                "description" => "string",
                "drivers_id" => "required|array",
                "drivers_id.*" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
            $request->merge([
                "drivers_id" => array_values(array_unique($request->drivers_id))
            ]);
            $checkTeamIdExist = $this->driver_team->getSpecificDataByColumn("id",$id);
            if(!$checkTeamIdExist){
                throw new ModelNotFoundException();
            }

            /**
             * checks if the one or more requested driver is assigned to another team or not
             */
            $checkDriverIsAssignedToOtherTeam = $this->driver_team->checkDriverIsAssignedToOtherTeam($checkTeamIdExist->captain_id,$request->drivers_id);
            if($checkDriverIsAssignedToOtherTeam){
                return response()->json([
                    "status" => "403",
                    "message" => "Driver is assigned to $checkDriverIsAssignedToOtherTeam->name."
                ],403);
            }

            $requestDrivers = $request->drivers_id;

            /**
             * checking if the main driver i.e captain driver is removed or not
             */
            foreach ($requestDrivers as $driver){
                if($driver == $checkTeamIdExist->captain_id){
                    return response()->json([
                        "status" => "403",
                        "message" => "Team captain is not allowed to remove from here."
                    ],403);
                }
            }
            $getTeamDrivers = $checkTeamIdExist->driverTeamMembers()->pluck("driver_id")->toArray();
            $uniqueDriver = array_values(array_diff($requestDrivers,$getTeamDrivers));
            /**
             * compares the requested driver ids with the assigned driver ids.if passed drivers id doesnot belongs to the current team then error message will be displayed
             */
            if(count($uniqueDriver) != 0){
                return response()->json([
                    "status" => "403",
                    "message" => "One or more driver does not belongs to current team."
                ],403);
            }
            $request->merge([
                "drivers_id" => $requestDrivers
            ]);
            /**
             * if above conditions are satisfied then proceed to delete the team member
             */
            $removeTeam = $this->driver_member->deleteSpecificTeamMembers($id,$request->drivers_id);
            $this->log->storeLogInfo(["removing  driver team member",[
                "data" => $request->all(),
                "id" => $id
            ]]);
            DB::commit();
            return response()->json([
                "status" => "200",
                'message' => 'Driver team member(s) removed successfully.'
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError([
                "error deleting driver team member",[
                    "data" => $request->all(),
                    "id" => $id,
                    "message" => 'Team could not be found'
                ]
            ]);
            return response()->json([
                "status" => "404",
                "message" => "Team does not exist."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error deleting driver team member",[
                    "data" => $request->all(),
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error deleting team members."
            ],500);
        }




    }

    private function getUserFilteredData($userData){
        $teamDetail = [];
        if(!isset($userData['email'])){
            return $teamDetail;
        }
        $teamDetail["name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($userData['first_name'], $userData['middle_name'], $userData['last_name']))));
        $teamDetail["email"] = $userData["email"];
        $teamDetail["mobile"] = preg_replace('/\s+/', ' ', trim(implode("", array($userData['country_code'], $userData['mobile']))));
        return $teamDetail;
    }

    private function convertUtcTime($info,$timezone){
        if(isset($info["created_at"]) && !is_null($info["created_at"])){
            $info["created_at"] = Carbon::parse($info["created_at"])->timezone($timezone)->toDateTimeString();
        }
        if(isset($info["leader_received_at"]) && !is_null($info["leader_received_at"])){
            $info["leader_received_at"] = Carbon::parse($info["leader_received_at"])->timezone($timezone)->toDateTimeString();
        }
        if(isset($info["admin_received_at"]) && !is_null($info["admin_received_at"])){
            $info["admin_received_at"] = Carbon::parse($info["admin_received_at"])->timezone($timezone)->toDateTimeString();
        }
        return $info;
    }








}
