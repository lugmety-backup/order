<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 7/25/2017
 * Time: 12:08 PM
 */

namespace App\Http\Controllers;

use App\Events\SendMessage;
use App\Exceptions\DuplicateRecordException;
use App\Http\Helpers\ExcelExportHelper;
use App\Repo\DeliveryStatusInterface;
use App\Repo\DriverCollectionInterface;
use App\Repo\DriverEarningInterface;
use App\Repo\DriverEarningWithdrawnInterface;
use App\Repo\DriverExperiencePointInterface;
use App\Repo\DriverOrderLocationInterface;
use App\Repo\DriverTeamInterface;
use App\Repo\EarningRuleInterface;
use App\Repo\ExperiencePointRulesInterface;
use App\Repo\OrderItemAddonInterface;
use App\Repo\OrderItemInterface;
use App\Repo\OrderSettingInterface;
use App\Repo\ShippingMethodInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Repo\OrderInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\In;
use LogStoreHelper;
use Illuminate\Support\Facades\Config;
use RemoteCall;
use Illuminate\Support\Facades\Input;
use Validator;
use RoleChecker;
use App\Repo\PaymentStatusInterface;
use App\Repo\OrderStatusInterface;
use CurrencyAttachHelper;
use App\Repo\DriverOrderInterface;
use App\Repo\PaymentMethodInterface;
use Settings;
use Mpdf\Mpdf;
use Chumper\Zipper\Zipper;
use Cocur\Slugify\Slugify;

class DriverController extends Controller
{

    /**
     * @var OrderInterface
     */
    protected $order;
    /**
     * @var LogStoreHelper
     */
    protected $log;

    protected $currencyAttachHelper;

    protected $driverOrder;

    protected $xpRule;

    protected $earningRule;

    protected $driverXp;

    protected $driverEarning;

    protected $slugify;

    protected $driverOrderLocation;
    protected $paymentMethod;
    protected $deliveryStatus;
    protected $driverEarningWithdrawl;
    protected $item;
    protected $allDeliveryStatus;
    protected $driverTeam;
    protected $driverCollection;
    protected $order_setting;
    protected  $excel;

    public function __construct(OrderInterface $order,
                                LogStoreHelper $log,
                                DriverEarningWithdrawnInterface $driverEarningWithdrawn,
                                CurrencyAttachHelper $currencyAttachHelper,
                                PaymentMethodInterface $paymentMethod,
                                DriverEarningInterface $driverEarning,
                                DriverExperiencePointInterface $driverExperiencePoint,
                                ExperiencePointRulesInterface $xpRule,
                                ShippingMethodInterface $shippingMethod,
                                EarningRuleInterface $earningRule,
                                DeliveryStatusInterface $deliveryStatus,
                                DriverOrderLocationInterface $driverOrderLocation,
                                OrderItemInterface $item,
                                DriverOrderInterface $driverOrder,
                                DriverTeamInterface $driverTeam,
                                DriverCollectionInterface $driverCollection,
                                Slugify $slugify,
                                ExcelExportHelper $excelExportHelper,
                                OrderSettingInterface $orderSetting
    )
    {
        $this->order = $order;
        $this->log = $log;
        $this->currencyAttachHelper = $currencyAttachHelper;
        $this->driverOrder = $driverOrder;
        $this->earningRule = $earningRule;
        $this->xpRule = $xpRule;
        $this->deliveryStatus = $deliveryStatus;
        $this->driverEarning = $driverEarning;
        $this->driverXp = $driverExperiencePoint;
        $this->paymentMethod = $paymentMethod;
        $this->driverOrderLocation = $driverOrderLocation;
        $this->driverEarningWithdrawl = $driverEarningWithdrawn;
        $this->item = $item;
        $this->driverTeam = $driverTeam;
        $this->driverCollection = $driverCollection;
        $this->slugify = $slugify;
        $this->order_setting = $orderSetting;
        $this->excel = $excelExportHelper;
    }


    public function driverDashBoard(Request $request)
    {
//       $time1 = round(microtime(true));
        if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
            return response()->json([
                "status" => "403",
                "message" => "Only Driver has access"
            ], 403);
        }
        $driverUrlInput["latitude"] = Input::get("latitude");
        $driverUrlInput["longitude"] = Input::get("longitude");
        $driverUrlInput["country_id"] = Input::get("country_id");
//        $distanceLimit = 1000;// distance limit this will shows restauants nearby driver
        $getSetting = Settings::getSettings('distance-limit');
        if ($getSetting["status"] == "200") {
            $distanceLimit = (int)$getSetting["message"]["data"]["value"];
        } else {
            $distanceLimit = 1.5;
        }
        $lang = $driverUrlInput["lang"] = Input::get("lang", "en");
        $rules = [
            "latitude" => "required|numeric",
            "longitude" => "required|numeric",
            "country_id" => "required|integer|min:1"
        ];
        $validator = Validator::make($driverUrlInput, $rules);
        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->json([
                "status" => "422",
                "message" => $error
            ], 422
            );
        }

        try {

            $user_id = RoleChecker::getUser();
            /**
             * get location service base url from Config/config.php
             */
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$request->country_id&lang=$lang";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if ($countryInfo['status'] != 503) {
                } else {
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails = $countryInfo["message"]["data"];
            $currency = $shippingDetails["currency_symbol"];
            $time1 = microtime(true) ;
//            $getTodayCompletedRecord = $this->driverOrder->getTodayCompletedOrder($user_id, $driverUrlInput["country_id"]);
            $getWeeklyRecord = $this->getSevendaysRecord($user_id, $driverUrlInput["country_id"]);

            $getTodayCompletedRecord = $getWeeklyRecord['today'];
            unset($getWeeklyRecord['today']);
            $restaurantDatas = $this->order->getAllRestaurantForDriver($driverUrlInput["latitude"], $driverUrlInput["longitude"], $driverUrlInput["country_id"], $distanceLimit, 1);
            $totalRestaurant = $restaurantDatas["count"][0]->count;//total number of record obtained from query
            $completedOrder = $this->getCompletedOrder($user_id, $driverUrlInput["country_id"]);
            $getCompletedOrder = $completedOrder["count"];
            $getTotalXp = $this->getTotalXp($user_id, $driverUrlInput["country_id"]);
            $getTotalEarning = $this->getTotalEarning($user_id, $request->country_id);
            $dashboardResponse["completed_order_count"] = $getCompletedOrder;
            $dashboardResponse["total_xp"] = round($getTotalXp,2);

            $dashboardResponse["currency"] = $currency;

            $dashboardResponse["current_balance"] = round($getTotalEarning);
            $dashboardResponse["restaurant_count"] = $totalRestaurant;
            $dashboardResponse["weekly_order_count"] = $getWeeklyRecord;
            $dashboardResponse["today_order_count"] = $getTodayCompletedRecord;
            $time2 = microtime(true);

//            $time2 = round(microtime(true));
            $dashboardResponse["execution_time"] = $time2 - $time1;
            return response()->json([
                "status" => "200",
                "data" => $dashboardResponse
            ]);


        } catch (\Exception $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('order-error-getting-data',
                "Error getting data.", 500, $lang);
        }
    }

    /**
     * get all nearby restaurant for drivers
     * @return \Illuminate\Http\JsonResponse
     */
    public function restaurantOrderListForDriver()
    {
        if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
            return response()->json([
                "status" => "403",
                "message" => "Only Driver has access"
            ], 403);
        }
        $driverUrlInput["latitude"] = Input::get("latitude");
        $driverUrlInput["longitude"] = Input::get("longitude");
        $driverUrlInput["country_id"] = Input::get("country_id");
        $limit = $driverUrlInput["limit"] = Input::get("limit", 20);
//        $distanceLimit = 1000;// distance limit this will shows restauants nearby driver
        $getSetting = Settings::getSettings('distance-limit');
        if ($getSetting["status"] == "200") {
            $distanceLimit = (int)$getSetting["message"]["data"]["value"];
        } else {
            $distanceLimit = 1.5;
        }
        $lang = $driverUrlInput["lang"] = Input::get("lang", "en");
        $rules = [
            "latitude" => "required|numeric",
            "longitude" => "required|numeric",
            "country_id" => "required|integer|min:1"
        ];
        $validator = Validator::make($driverUrlInput, $rules);
        if ($validator->fails()) {
            $error = $validator->errors();
            return response()->json([
                "status" => "422",
                "message" => $error
            ], 422
            );
        }
        $rules = [
            "limit" => "required|integer|min:1"
        ];
        $validator = Validator::make($driverUrlInput, $rules);
        if ($validator->fails()) {
            $limit = 20;//if limit validation fails then the limit is set to 20
        }
        /*
         * gets nearby restaurants which has pending orders to be delivered
         */
        try {
            $user_id = RoleChecker::getUser();
            $restaurantDatas = $this->order->getAllRestaurantForDriver($driverUrlInput["latitude"], $driverUrlInput["longitude"], $driverUrlInput["country_id"], $distanceLimit, $limit);
            $totalRecord = $restaurantDatas["count"][0]->count;//total number of record obtained from query
            unset($restaurantDatas["count"]);
            if (count($restaurantDatas) == 0) {
                return \Settings::getErrorMessageOrDefaultMessage('order-nearby-restaurant-orders-could-not-be-found',
                    "Nearby Restaurant Orders could not be found.", 404, $lang);
            }
//            $restaurantDatas->withpath("/restaurants/nearby")->appends(["latitude" => $driverUrlInput["latitude"], "longitude" => $driverUrlInput["latitude"], "country_id" => $driverUrlInput["country_id"], 'lang' => $lang, "limit" => $limit]);
            foreach ($restaurantDatas as $restaurant) {
                $restaurantdata["restaurant_ids"][] = $restaurant->restaurant_id;
            }
            $restaurantdata["lang"] = $driverUrlInput["lang"];
            //calling restaurant service to get restaurant name list
            $restaurantBaseUrl = Config::get('config.restaurant_base_url');
            $restaurantDetails = RemoteCall::store($restaurantBaseUrl . '/public/branch?lang=' . $lang, $restaurantdata);
            if ($restaurantDetails["status"] != 200) {
                return response()->json([
                    "status" => $restaurantDetails["status"],
                    "message" => $restaurantDetails["message"]["message"]
                ], $restaurantDetails["status"]);
            }
            $restaurantDatasFromRestautant = $restaurantDetails["message"]["data"];
            /**
             * assigns order count to restaurant detail
             */
            $restaurantDataFilter = [];
            foreach ($restaurantDatas as $key => $data) {
                foreach ($restaurantDatasFromRestautant as $restaurant) {
                    if ($data->restaurant_id == $restaurant["id"]) {
                        if( 80 == $restaurant['id'] && 2608 != $user_id ) continue;
                        $restaurantDatas[$key]->restaurant_details = $restaurant;
                        $restaurantDataFilter[] = $restaurantDatas[$key];
                        break;
                    }

                }

            }
            if (count($restaurantDataFilter) == 0) throw new \Exception();
            $restaurantDatas = $this->manualPagination($limit, array_values($restaurantDataFilter), $totalRecord);
//        foreach ($restaurantDatasFromRestautant as $key => $restaurant){
//            foreach ($restaurantDatas as $data){
//                if($data->restaurant_id == $restaurant["id"]){
//                    $restaurantDatasFromRestautant[$key]["order_count"] = $data->count;
//                    break;
//                }
//            }
//        }

            $this->log->storeLogInfo(array("Resturant order driver list", [
                "status" => "200",
                "data" => $restaurantDatas
            ]
            ));
            return response()->json([
                "status" => "200",
                "data" => $restaurantDatas
            ]);
        } catch (\Exception $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('order-nearby-restaurant-orders-could-not-be-found',
                "Nearby Restaurant Orders could not be found.", 404, $lang);
        }
    }

    /**
     * displays the list of orders based on @param $id i.e restaurnt branch id
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function orderListForDriver($id, Request $request)
    {
        try {
            $lang = Input::get('lang', 'en');
            if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
                return \Settings::getErrorMessageOrDefaultMessage('order-only-driver-has-access',
                    "Only Driver has access.", 403, $lang);
            }
            $driverUrlInput["country_id"] = Input::get("country_id");
            $limit = $driverUrlInput["limit"] = Input::get("limit", 20);
            $rules = [
                "country_id" => "required|integer|min:1"
            ];
            $validator = Validator::make($driverUrlInput, $rules);
            if ($validator->fails()) {
                $error = $validator->errors();
                return response()->json([
                    "status" => "422",
                    "message" => $error
                ], 422
                );
            }
            $rules = [
                "limit" => "required|integer|min:1"
            ];
            $validator = Validator::make($driverUrlInput, $rules);
            if ($validator->fails()) {
                $limit = 20;//if limit validation fails then the limit is set to 20
            }
            $lang = $driverUrlInput["lang"] = Input::get("lang", "en");

            /**
             * gets pending list of orders  to be delivered by driver based on restaurant id
             */

            $ordersDatas = $this->order->getAllOrdersForDriver($id, $driverUrlInput["country_id"], $limit);
//            $ordersDatas->withpath("/restaurant/$id/orders")->appends(["country_id" =>$driverUrlInput["country_id"],'lang'=> $lang,"limit" => $limit, ]);
            $totalRecord = $ordersDatas["count"][0]->total;//total number of record obtained from query
            unset($ordersDatas["count"]);

            /*
             * checks if the orders is empty
             */
            if (count($ordersDatas) == 0) {
                return \Settings::getErrorMessageOrDefaultMessage('order-restaurant-orders-could-not-be-found',
                    "Restaurant Orders could not be found.", 403, $lang);
            }
            $shippingList = [];
            $userList = [];
            /*
             * listing user id and shipping id
             */
            foreach ($ordersDatas as $order) {
                $userList[] = $order->customer_id;
                $shippingList[] = $order->shipping_id;
            }
            $userDatas["users"] = array_values(array_unique($userList, SORT_REGULAR));
            $shippingList["address"] = array_values(array_unique($shippingList, SORT_REGULAR));
            /*
             * calling location service to get shipping address based on the array of shipping id
             *
             */
            $locationUrl = Config::get('config.location_base_url') . "/shipping/address/list?lang=$lang";
            $getShippingDetails = RemoteCall::storeWithoutOauth($locationUrl, $shippingList);
            if ($getShippingDetails["status"] != "200") {
                throw new \Exception();
            }
            $getShippingDetailsFromLocation = $getShippingDetails["message"]["data"];
            if (empty($getShippingDetailsFromLocation)) {
                throw new \Exception();
            }

            /**
             * calling oauth service to get user lists
             */
            $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list?lang=$lang";
            $getUserDetailsForReview = RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);
            if ($getUserDetailsForReview["status"] != "200") {
                throw new \Exception();
            }
            $userDatasFromOauth = $getUserDetailsForReview["message"]["data"];
            if (empty($userDatasFromOauth)) {
                throw new \Exception();
            }
            /*
             * ignoring the order which doesnot have user details or shipping address
             */
            $filteredOrderDatas = [];
            $timezone = $ordersDatas[0]->time_zone_utc;
            foreach ($ordersDatas as $key => $order) {
//              dd(Carbon::createFromFormat("Y-m-d H:i:s",$order->delivery_or_pickup_time,"Asia/Riyadh")->tz('utc')->diffForHumans());
                if (is_null($order->api_delivery_time) || empty($order->api_delivery_time) || $order->api_delivery_time == "") continue;
                $carbon = new Carbon($order->created_at);
                $order->created_at = $carbon->diffForHumans();//human readable form

                foreach ($userDatasFromOauth as $userData) {
                    if ($userData["id"] !== $order->customer_id) continue;
//                    $order->user_name = $userData["first_name"];
//                    if(!empty($userData["middle_name"])) $order->user_name .= " ".$userData["middle_name"];
                    $order->user_name = implode(" ", array($userData["first_name"], $userData["middle_name"], $userData["last_name"]));
                    $order->mobile = $userData["country_code"] . $userData["mobile"];

                }
                foreach ($getShippingDetailsFromLocation as $address) {
                    if ($order->shipping_id !== $address["id"]) continue;
                    $order->address = $address["address"];
                }

                if (!isset($order->address) || !isset($order->user_name)) {
                    unset($ordersDatas[$key]);
                } else {
                    $filteredOrderData["created_at"] = $order->created_at;
                    $filteredOrderData["user_name"] = $order->user_name;
                    $filteredOrderData["address"] = $order->address;
//                    $filteredOrderDatas["delivery_time"] = $order->api_delivery_time;
                    $filteredOrderData["delivery_time"] = $this->getTime($order)->toDateTimeString();
                    $filteredOrderData["mobile"] = $order->mobile;
                    $filteredOrderData["id"] = $order->id;
                    $filteredOrderData["customer_priority"] = $order->customer_priority;
                    $filteredOrderDatas[] = $filteredOrderData;

                }
            }


            $driverOrderPriorityCriticalLimit  = \Settings::getSettings('driver-order-listing-critical-time-limit');
            if ($driverOrderPriorityCriticalLimit["status"] != 200) {
                return response()->json(
                    $driverOrderPriorityCriticalLimit["message"]
                    , $driverOrderPriorityCriticalLimit["status"]);
            }

            $time_limit = $driverOrderPriorityCriticalLimit["message"]["data"]["value"];
            $date = Carbon::now($timezone)->addMinutes($time_limit);// subtract the current time from the setting minutes limit
            $currentDate = Carbon::now($timezone); // get current time
            $getCriticalRecords = collect($filteredOrderDatas)->filter(function ($item) use ($date,$currentDate) { // selects the orders between the current time and date , to group the higb priority order
                return (data_get($item, 'delivery_time') <= $date) && (data_get($item, 'delivery_time') >= $currentDate);
            })->sortByDesc('delivery_time');

            $getCriticalRecords = $getCriticalRecords->map(function ($item){ // add new field order_priority and set critical
                $item["order_priority"] = "critical";
                return $item;
            });
            $getCriticalRecordsIds= collect($getCriticalRecords)->pluck('id')->values()->all();//get all the critical  orders id
            $getVipRecords = collect($filteredOrderDatas)->where('customer_priority',10)->whereNotIn('id',$getCriticalRecordsIds)->sortByDesc('delivery_time')->values()->all(); // fetch all the vip orders which is not critical
            $getVipRecords = collect($getVipRecords)->map(function ($item)  { //assign order_priority priotity for all the vip records
                $item["order_priority"] = "high";
                return $item;
            });

            $combineCriticalAndVipRecords = $getCriticalRecords->merge($getVipRecords)->values()->all();// merge the critical reords and vip record and sort by latest delivery_time


            $getcombineCriticalAndVipRecordsIds = collect($combineCriticalAndVipRecords)->pluck('id')->values()->all();//gets critical and vip orders IDS
            $getNewUserOrders = collect($filteredOrderDatas)->whereNotIn('id',$getcombineCriticalAndVipRecordsIds)->where('customer_priority',20)->sortByDesc('delivery_time')->values()->all(); // fetch the records which are not vip and critical and whose customer_priority is 20 i.e new user
            $getNewUserOrders = collect($getNewUserOrders)->map(function ($item){ // set new user order priority to medium
                $item["order_priority"] = "medium";
                return $item;
            });
            $getNormalUserRecord = collect($filteredOrderDatas)->whereNotIn('id',$getcombineCriticalAndVipRecordsIds)->where('customer_priority',30)->sortByDesc('delivery_time')->values()->all(); // fetch the orders whose priority is 30 and is not vip , critical and new user
            $getNormalUserRecord = collect($getNormalUserRecord)->map(function ($item){ // set order priority to low
                $item["order_priority"] = "low";
                return $item;
            });

            $filteredOrderDatas = collect($combineCriticalAndVipRecords)->merge($getNewUserOrders)->merge($getNormalUserRecord)->values()->all();
            $orderFromPagination = $this->manualPagination($limit, array_values($filteredOrderDatas), $totalRecord);
            $this->log->storeLogInfo(array("Restaurant order driver list", [
                "status" => "200",
                "data" => $orderFromPagination
            ]));
            return response()->json([
                "status" => "200",
                "data" => $orderFromPagination->withpath("/restaurant/$id/orders")->appends(["country_id" => $driverUrlInput["country_id"], 'lang' => $lang, "limit" => $limit])
            ]);

        } catch (\Exception $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('order-restaurant-orders-could-not-be-found',
                "Restaurant Orders could not be found.", 404, $lang);
        }
    }

    /**
     * get all order collection history of logged in driver collected by captain driver
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificDriverCollectionDetail(Request $request){
        try{
            $this->validate($request,[
                "country_id" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ],422);
        }

        try{

            $this->validate($request,[
                "limit" => "required|integer|min:1",
            ]);
        }
        catch (\Exception $ex){
            $request->merge([
                "limit" => 10
            ]);
        }
        try{
            if(!(RoleChecker::hasRole('driver-captain') || RoleChecker::hasRole('driver'))){
                return response()->json([
                    "status" => "403",
                    "message" => "Only driver has access."
                ]);
            }
            $driverId = RoleChecker::getUser();

            $getCountryDetail = \RemoteCall::getSpecific(Config::get("config.location_base_url")."/public/country/$request->country_id");
            if($getCountryDetail["status"] != "200"){
                if($getCountryDetail["status"] == "503"){
                    return response()->json($getCountryDetail,503);
                }
                else{
                    return response()->json([
                        "status" => (string)$getCountryDetail["status"],
                        "message" => $getCountryDetail["message"]["message"]
                    ],$getCountryDetail["status"]);
                }
            }
            $currency = $getCountryDetail["message"]["data"]["currency_symbol"];

            $getSpecificTeamDetail = $this->driverCollection->getSpecificDriverCollectedCollectionBasedOnTeamIdForDriver($request->all(),$driverId);
            $getSpecificTeamDetail->map(function ($post) use ($currency) {
                $post['currency'] = $currency;
                return $post;
            });
            return response()->json([
                "status" => "200",
                "data" => $getSpecificTeamDetail
            ]);
        }
        catch (ModelNotFoundException $ex){
            $this->log->storeLogError([
                "error listing  specific drivers collected collection detail list",[
                    "data" => [$request->all(),"driver_id" => $driverId],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "404",
                "message" => "You are not assigned to any team."
            ],404);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                "error listing  specific driver collected collection detail list",[
                    "data" => [$request->all(),"driver_id" => $driverId],
                    "message" => $ex->getMessage()
                ]
            ]);
            return response()->json([
                "status" => "500",
                "message" => "Error listing driver collected collection detail list"
            ],500);
        }
    }

    /**
     * get all driver order history , if status paramater is passed record will be returned based on status , only accessible to those who has permission
     * @return mixed
     */
    public function getListForDriverOrderForAdmin(Request $request)
    {
        $lang = Input::get('lang', 'en');
        try {
            $this->validate($request, [
                "country_id" => "required|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $orderLists = $this->driverOrder->getAllDriverOrderForAdmin(Input::get("status"));
            $orderLists->makeHidden(["order", "restaurant"]);
            $orderLists = collect($orderLists)->where("country_id", $request->country_id);
            /**
             * get location service base url from Config/config.php
             */
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/1/shipping/1?country_id=$request->country_id";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::getSpecific($countryFullUrl);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if ($countryInfo['status'] != 503) {
                } else {
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }

            $shippingDetails = $countryInfo["message"]["data"];
            /**
             * calling oauth service to get user list
             */
            $userDatas["users"] = array_unique($orderLists->pluck("user_id")->toArray());
            $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list";
            $getUserDetailsForReview = RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);
            if ($getUserDetailsForReview["status"] != "200") {
                if ($getUserDetailsForReview["status"] == "503") {
                    return response()->json([
                        "status" => $getUserDetailsForReview["status"],
                        "message" => $getUserDetailsForReview["message"]
                    ], $getUserDetailsForReview["status"]);
                }
                return response()->json($getUserDetailsForReview["message"], $getUserDetailsForReview["status"]);

            }
            $userLists = $getUserDetailsForReview["message"]["data"];
            foreach ($orderLists as $key => $orderlist) {
                $orderlist["price"] = $shippingDetails["currency_symbol"] . $orderlist["price"];
                $flag = false;
                foreach ($userLists as $userlist) {
                    if ($orderlist->user_id == $userlist["id"]) {
                        $flag = true;
                        $orderlist["name"] = implode(" ", array($userlist['first_name'], $userlist['middle_name'], $userlist['last_name']));
                        $orderlist["mobile"] = implode("", array($userlist['country_code'], $userlist["mobile"]));
                        $orderlist["email"] = $userlist['email'];
                        $orderlist["first_name"] = $userlist['first_name'];
                        $orderlist["last_name"] = $userlist['last_name'];
                        break;
                    }
                }
                if (!$flag) unset($orderLists[$key]);
            }
            return \Datatables::of($orderLists->values())->make(true);
        } catch (\Exception $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('order-error-getting-data',
                "Error getting data.", 500, $lang);
        }

    }

    /**
     * get assigned order based on @param $id , only logged in driver can view his data only
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderSpecificForDriver($id, Request $request)
    {
        $lang = Input::get('lang', 'en');
        if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
            return \Settings::getErrorMessageOrDefaultMessage('order-only-driver-has-access',
                "Only Driver has access.", 403, $lang);
        }
        try {
            $userId = RoleChecker::getUser();
            $order = $this->driverOrder->getDriverOrderBasedOnIdAndUserId($id, $userId);
            $order->makeHidden("extra_info");
            $realOrder = $this->order->getSpecificOrder($order->order_id);
            $order["item_count"] = $this->item->countOrderItem($order->order_id);
            $restaurantBaseUrl = Config::get("config.restaurant_base_url");
            $restaurantFullUrl = "$restaurantBaseUrl/public/restaurant/branch/$realOrder->restaurant_id/detail?lang=$lang";

            /**
             *calling restaurant service to check the restaurant exist or not if exist fetch the data
             */
            $restaurantdata = \RemoteCall::getSpecific($restaurantFullUrl);
            if ($restaurantdata['status'] != 200) {
                if ($restaurantdata['status'] == 503) {
                    return response()->json(
                        $restaurantdata, $restaurantdata['status']);
                }
                return response()->json(
                    $restaurantdata["message"], $restaurantdata['status']);
            }
            $restaurantDetails = $restaurantdata["message"]["data"];
            $order["restaurant_latitude"] = $restaurantDetails["latitude"];
            $order["restaurant_longitude"] = $restaurantDetails["longitude"];
            $shippingId = $realOrder["shipping_id"];
            $customerId = $realOrder["customer_id"];
            $restaurantCountryId = $restaurantDetails["country_id"];
            /**
             * get location service base url from Config/config.php
             */
            $countryBaseUrl = Config::get("config.location_base_url");

            //todo get restaurant country location

            $countryFullUrl = "$countryBaseUrl/user/$customerId/shipping/$shippingId?restaurant_country=$restaurantCountryId&lang=$lang";
            /**
             * calling to location service
             */
            $countryInfo = RemoteCall::getSpecificWithoutToken($countryFullUrl);
            if ($countryInfo["status"] != "200") {
                if ($countryInfo["status"] == "503") {
                    $status = $countryInfo["status"];
                    return response()->json([
                        "status" => $status,
                        "message" => $countryInfo["message"]
                    ], $status);
                }
                return response()->json($countryInfo["message"], $countryInfo["status"]);
            }

            /**
             * calling oauth service to get user lists
             */
            $userDatas["users"] = [$customerId];
            $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list?lang=$lang";
            $getUserDetailsForReview = RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);
            if ($getUserDetailsForReview["status"] != "200") {
                if ($getUserDetailsForReview["status"] == "503") {
                    $status = $countryInfo["status"];
                    return response()->json([
                        "status" => $status,
                        "message" => $getUserDetailsForReview["message"]
                    ], $status);
                }
                return response()->json($getUserDetailsForReview["message"], $getUserDetailsForReview["status"]);

            }

            $order->makeHidden(["restaurant", "order"]);
            $order = $order->toArray();
            $customer = $getUserDetailsForReview["message"]["data"][0];
            $shippingDetails = $countryInfo["message"]["data"];
            $countryFirstName = explode(" ", $shippingDetails["translation"]["name"]);
            if (strpos(strtolower($restaurantDetails["address"]), strtolower($countryFirstName[0])) !== false) {
                $order["restaurant_information"]["address"] = $restaurantDetails["address"];
            } else {
                $order["restaurant_information"]["address"] = implode(",", [$restaurantDetails["address"], $shippingDetails["translation"]["name"]]);
            }


            $timezone = $shippingDetails["country"]["timezone"];
            $order["customer_information"]["name"] = implode(" ", array($customer['first_name'], $customer['middle_name'], $customer['last_name']));
            $order["customer_information"]["mobile"] = implode("", array($customer['country_code'], $customer["mobile"]));
            $order["customer_information"]["house_no"] = $shippingDetails["house_number"];
            $order["customer_information"]["address"] = implode(",", array($shippingDetails['address_name'], $shippingDetails['street'], $shippingDetails['extra_direction'], $shippingDetails["district_name"]["name"], $shippingDetails["city_name"]["name"], $shippingDetails["translation"]["name"]));
            $order["customer_information"]["zip_code"] = $shippingDetails["zip_code"];
            $order["restaurant_information"]["name"] = $restaurantDetails["name"];

            $order["restaurant_information"]["phone_number"] = $restaurantDetails["phone_no"];
            $order["payment_method"] = $this->getPaymentMethodTranslation($realOrder["payment_method"], "en");
            if(isset($realOrder["user_type"]) && ($realOrder["user_type"] == "restaurant-customer") && (0 == $realOrder['is_normal'])){
                $order["amount"] = $realOrder->amount - $realOrder->delivery_fee;
            }
            else{
                $order["amount"] = $realOrder->amount;
            }

            $order["currency"] = ($shippingDetails["country"]["currency_symbol"] == $realOrder->currency) ? $realOrder->currency : $shippingDetails["country"]["currency_symbol"];
            $getDriverEarning = $this->driverEarning->viewSpecificEarningByOrderIdAndUserId($realOrder->id, $userId, $order["country_id"]);
            $order["earned_amount"] = $getDriverEarning;

//            $order["mobile"] = implode("", array($userInfo['country_code'], $userInfo['mobile']));
//            $order["name"] = implode(" ", array($userInfo['first_name'], $userInfo['middle_name'], $userInfo['last_name']));
//            $order["address"] = implode(",", array($addressInfo['address_name'], $addressInfo['street'], $addressInfo['house_number']));
            $order["shipping_latitude"] = $countryInfo["message"]["data"]["latitude"];
            $order["shipping_longitude"] = $countryInfo["message"]["data"]["longitude"];
//            $order["time_remaining"] = Carbon::parse($order["pickup_time"])->diffForHumans();
            $order["time_remaining"] = $order["pickup_time"];
            $deliveryStatus = $this->getDeliveryStatusTransaction($realOrder->delivery_status, $lang);
            if ($realOrder->delivery_status == "delivered") {
                $order["delivery_information"]["delivery_status"] = $deliveryStatus;
                $order["delivery_information"]["is_completed"] = true;
                $convertToCountryTimeZone = Carbon::parse($getDriverEarning["created_at"])->timezone($timezone);
                $order["delivery_information"]["time"] = $convertToCountryTimeZone->format("h:i A");
                $order["delivery_information"]["date"] = $convertToCountryTimeZone->format("Y-m-d");
            } else {
                $order["delivery_information"]["delivery_status"] = $deliveryStatus;
                $order["delivery_information"]["is_completed"] = false;
            }
            return response()->json([
                "status" => "200",
                "data" => $order
            ]);
        } catch (\Exception $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('order-your-assigned-order-could-not-be-found',
                "Your assigned order could not be found.", 404, $lang);
        }
    }

    /**
     * get specific assigned order of driver to admin
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificDetailOfDriverOrderForAdmin($id)
    {
        try {
            $assignedOrder = $this->driverOrder->getDriverOrderBasedOnId($id)->makeHidden(["restaurant", "order"]);
            /**
             * calling oauth service to get user list
             */
            $userDatas["users"] = [$assignedOrder->user_id];
            $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list";
            $getUserDetailsForReview = RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);
            if ($getUserDetailsForReview["status"] != "200") {
                if ($getUserDetailsForReview["status"] == "503") {
                    return response()->json([
                        "status" => $getUserDetailsForReview["status"],
                        "message" => $getUserDetailsForReview["message"]
                    ], $getUserDetailsForReview["status"]);
                }
                return response()->json($getUserDetailsForReview["message"], $getUserDetailsForReview["status"]);

            }
            $userlist = $getUserDetailsForReview["message"]["data"][0];
            $assignedOrder["name"] = preg_replace('/\s+/', ' ', trim(implode(" ", array($userlist['first_name'], $userlist['middle_name'], $userlist['last_name']))));
            $assignedOrder["mobile"] = implode("", array($userlist['country_code'], $userlist["mobile"]));
            $assignedOrder["email"] = $userlist['email'];
            $assignedOrder["first_name"] = $userlist['first_name'];
            $assignedOrder["last_name"] = $userlist['last_name'];
            return response()->json([
                "status" => "200",
                "data" => $assignedOrder
            ]);
        } catch (ModelNotFoundException $ex) {
            return response()->json([
                "status" => "404",
                "message" => "Driver Order not be found"
            ], 404);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => "Error geting data"
            ], 500);
        }

    }

    /**
     * gets all list of order history for the logged in driver
     * @return mixed
     */
    public function getListForDriver(Request $request)
    {
        $lang = Input::get('lang', 'en');
        if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
            return \Settings::getErrorMessageOrDefaultMessage('order-only-driver-has-access',
                "Only Driver has access.", 403, $lang);
        }
        try {
            $this->validate($request, [
                "country_id" => "required|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);

        }
        $limit["limit"] = Input::get("limit", 20);
        $validator = Validator::make($limit, [
            "limit" => "integer|min:1"
        ]);
        if ($validator->fails()) {
            $limit["limit"] = 20;
        }
        $userId = RoleChecker::getUser();
        $country_id = $request->country_id;
        $this->allDeliveryStatus = $this->deliveryStatus->getAllDeliveryStatusIncludingTranslation();
        $driverOrdersHistory = $this->driverOrder->getAllDriverOrderByUserId($userId, $country_id, $limit["limit"]);
        foreach ($driverOrdersHistory as $history){
            $deliveryStatusLanguage = $this->getDeliveryStatusTransactionModified($history["status"], $this->allDeliveryStatus, $lang);
            if ($deliveryStatusLanguage) {
                $history["type_label"] = $deliveryStatusLanguage;
            }
            else{
                $history["type_label"] = $history["status"];
            }
        }
        return $driverOrdersHistory->withPath('/driver/order/history')->appends(['limit' => $limit["limit"]]);

    }

    /**
     * returns driver list based on order id.Only user which has permission can view
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function driverSpecificOrderListById($id)
    {
        $lang = Input::get('lang', 'en');
        try {
            $getDriverList = $this->driverOrder->getDriverBasedOnOrderId($id, $type = "admin");
            if (count($getDriverList) == 0) goto skipCallingOauth;
            $order = $this->order->getSpecificOrder($id);
            if (!$order) goto skipCallingOauth;
            $restaurant = unserialize($order["restaurant"]);
            $timezone = $restaurant["timezone"];
            $userDatas["users"] = array_unique($getDriverList->pluck("user_id")->toArray());
            $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list?lang=$lang";
            $getUserDetailsForReview = \RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);
            if ($getUserDetailsForReview["status"] != "200") {
                if ($getUserDetailsForReview['status'] == 503) {
                    return response()->json(
                        $getUserDetailsForReview, $getUserDetailsForReview['status']);
                }
                return response()->json(
                    $getUserDetailsForReview["message"], $getUserDetailsForReview['status']);
            }
            $userDatasFromOauth = $getUserDetailsForReview["message"]["data"];

            foreach ($getDriverList as $driverList) {
                $driverList["order_history"] = unserialize($driverList["order_history"]);
                foreach ($userDatasFromOauth as $user) {
                    if ($driverList["user_id"] == $user["id"]) {
//                        $driverList["user"] = $user;
                        $driverList["name"] = implode(" ", array($user["first_name"], $user["middle_name"], $user["last_name"]));
                        $driverList["mobile"] = implode("", array($user["country_code"], $user["mobile"]));
                        break;
                    }
                }
            }
            skipCallingOauth:
            $getDriverList = new Collection($getDriverList);
            return \Datatables::of($getDriverList)->make(true);

        } catch (\Exception $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('order-error-getting-data',
                "Error getting data.", 500, $lang);
        }
    }

    /**
     * download the driver invoice report in csv and pdf
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function getSpecificDriverInvoiceReport(Request $request){
        try{
            $this->validate($request,[
                'date' => 'sometimes|date_format:Y-m',
                'driver_id' => 'required|integer|min:1',
                'country_id' => 'required|integer|min:1',
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                'status' => '422',
                'message' => $ex->response->original
            ],422);
        }
        DB::beginTransaction();
        try{
            if($request->has('date')){
                $date = Carbon::parse($request->date)->tz('utc');

            }
            else{
                $date = Carbon::now('utc')->subMonth(1);
            }

            $bankInfoEnglish = \Settings::getSettings('driver-invoice-bank-info-english');
            if ($bankInfoEnglish["status"] != 200) {
                return response()->json(
                    $bankInfoEnglish["message"]
                    , $bankInfoEnglish["status"]);
            }
            $bankInfoArabic = \Settings::getSettings('driver-invoice-bank-info-arabic');
            if ($bankInfoArabic["status"] != 200) {
                return response()->json(
                    $bankInfoArabic["message"]
                    , $bankInfoArabic["status"]);
            }

            $tinNumberFromSetting = \Settings::getSettings('invoice-lugmety-tin');
            if ($tinNumberFromSetting["status"] != 200) {
                return response()->json(
                    $tinNumberFromSetting["message"]
                    , $tinNumberFromSetting["status"]);
            }
            $phoneNumber = \Settings::getSettings('invoice-lugmety-phone-number');
            if ($phoneNumber["status"] != 200) {
                return response()->json(
                    $phoneNumber["message"]
                    , $phoneNumber["status"]);
            }

            $getCaptainSalary = \Settings::getSettings('driver-invoice-captain-salary');
            if ($getCaptainSalary["status"] != 200) {
                return response()->json(
                    $getCaptainSalary["message"]
                    , $getCaptainSalary["status"]);
            }

            $getCommissionRateForDriver = \Settings::getSettings('driver-invoice-commission-rate');
            if ($getCommissionRateForDriver["status"] != 200) {
                return response()->json(
                    $getCommissionRateForDriver["message"]
                    , $getCommissionRateForDriver["status"]);
            }
            //calling oauth service to get driver information

            $oauthUrl = Config::get('config.oauth_base_url') . "/driver/$request->driver_id/info";
            $getUserDetailsForReview = \RemoteCall::getSpecificWithoutToken($oauthUrl);

            if ($getUserDetailsForReview["status"] != "200") {
                if ($getUserDetailsForReview['status'] == 503) {
                    return response()->json(
                        $getUserDetailsForReview, $getUserDetailsForReview['status']);
                }
                return response()->json(
                    $getUserDetailsForReview["message"], $getUserDetailsForReview['status']);
            }


            //calling location service to get country info

            $userInfo = $getUserDetailsForReview["message"]["data"];
            $datesForInvoice = $date->format('Y-m');
            $invoice = "D-$request->driver_id-$datesForInvoice";
            /**
             * gets driver name
             */
            $bill_to = preg_replace('/\s+/', ' ', trim(implode(" ", array($userInfo['first_name'], $userInfo['middle_name'], $userInfo['last_name']))));
//            $filename = $bill_to;
            $filename = "$bill_to-$invoice";

            $fileNameInSlug = $this->slugify->slugify($filename);

            $getSettings = $this->order_setting->getSettingForInvoice('driver',$datesForInvoice);
            if(!$getSettings){
                return response()->json([
                    'status' => '404',
                    'message' => 'Settings could not be found'
                ],404);
            }
            $getSettingData = unserialize($getSettings->data);
            /**
             * if commission rate, captain salary are not found in order setting then take the latest setting commission rate and captain salary
             */
            $commission_order_rate = isset($getSettingData['driver-invoice-commission-rate']) ? $getSettingData['driver-invoice-commission-rate'] : $getCommissionRateForDriver["message"]["data"]["value"] ;
            $captain_salary = isset($getSettingData['driver-invoice-captain-salary']) ? $getSettingData['driver-invoice-captain-salary'] : $getCaptainSalary["message"]["data"]["value"];
            $getSpecificDateSetting = $this->order_setting->getSetting('driver',$datesForInvoice);//fetch the specific date setting based on type
            /**
             * if found the update the settings if the count is not equals to 2
             */
            if($getSpecificDateSetting){
                $settingData = unserialize($getSpecificDateSetting['data']);
//                dd(count($settingData));
                if(count($settingData) != 2){
                    if(!isset($settingData['driver-invoice-commission-rate'])){
                        $settingData['driver-invoice-commission-rate'] =$commission_order_rate;
                    }
                    elseif(!isset($settingData['driver-invoice-captain-salary'])){
                        $settingData['driver-invoice-captain-salary'] = $captain_salary;
                    }
                    $this->order_setting->updateSetting($getSpecificDateSetting,[
                        'data' => $settingData
                    ]);
                }

            }
            /**
             * create the new settings for the passed date for history reference
             */
            else{
                $order_setting['type'] = 'driver';
                $order_setting['date_for'] = $datesForInvoice;
                $data['driver-invoice-commission-rate'] = $commission_order_rate;
                $data['driver-invoice-captain-salary'] = $captain_salary;
                $order_setting["data"] =[
                    'driver-invoice-commission-rate' => $commission_order_rate,
                    'driver-invoice-captain-salary' => $captain_salary,
                ];
                $this->order_setting->createSetting($order_setting);
            }

            //get tax of the provided country
            $callLocationService = RemoteCall::getSpecific(Config::get('config.location_base_url')."/country/$request->country_id");

            if($callLocationService['status'] != '200'){
                return response()->json([
                    'status' => $callLocationService['status'],
                    'message' => $callLocationService['message']['message']
                ],$callLocationService['status']);
            }

            $commission_vat_rate = $callLocationService['message']['data']['tax_percentage'];
            $currency = $callLocationService['message']['data']['currency_symbol'];
            $date_range['start_date'] = $date->startOfMonth()->startOfDay()->format('Y-m-d H:i:s');
            $date_range['end_date'] = $date->endOfMonth()->endOfDay()->format('Y-m-d H:i:s');
            /**
             * gets total earning for the provided date
             */
            $getEarningBasedOnTime = $this->driverEarning->getSpecificDriverEarning($request->country_id,$request->driver_id,$date_range);

            /**
             * get earning whose order is not null
             */
            $get_driver_earnings = collect($getEarningBasedOnTime)->where('order_id','!=',null)->sum('amount');


            /**
             * gets total amount deducted for the provided date
             */
            $getDeductionEarningBasedOnTime =  $this->driverEarning->getSpecificDriverEarningDeduction($request->country_id,$request->driver_id,$date_range);
            /**
             * calculate the total earning amount
             */
            $total_earning = collect($getEarningBasedOnTime)->sum('amount');
            /**
             * calculate the total orders
             */
            $total_order = collect($getEarningBasedOnTime)->unique('order_id')->count();

            /**
             * calculate the total deduction amount
             */

            $total_deduction = collect($getDeductionEarningBasedOnTime)->sum(['amount']);
            /**
             * calculation of subtotal
             */
            $total_final_earning = $subtotal = round($total_earning - $total_deduction ,2);

            $lugmety_commission = round($get_driver_earnings * $commission_order_rate ,2);

            $lugmety_commission_vat_amount = round($lugmety_commission * $commission_vat_rate * 0.01,2);

            $getAllEarningsHistory = $this->driverEarning->getEarningHistoryOfDriver($request->driver_id,$request->country_id,$date_range);



            /**
             * checks if the driver has the role of driver captain or driver. If the driver is the driver captain then add the captain salary
             */
            if($userInfo["role"] === 'driver-captain'){
                $is_captain = true;
                $grand_total = $total_final_earning -  $lugmety_commission - $lugmety_commission_vat_amount + $captain_salary;
            }
            else{
                $is_captain = false;
                $grand_total = $total_final_earning -  $lugmety_commission - $lugmety_commission_vat_amount ;
            }


            if(abs($grand_total) !== $grand_total){
                $grand_total = abs($grand_total);
                $grand_total_en = '- '.$currency .  number_format($grand_total,2);
            }
            else{
                $grand_total_en = $currency .  number_format($grand_total,2);
            }
            $total_final_earning = number_format($total_final_earning,2);
            $lugmety_commission = number_format($lugmety_commission,2);
            $lugmety_commission_vat_amount = number_format($lugmety_commission_vat_amount,2);
            $data = [
                'invoice' => $invoice,
                'lugmety_phone_number' => $phoneNumber["message"]["data"]["value"],
                "bill_to" => "$bill_to",
                'bill_for_en' => 'Online Delivery Service',
                "bank_info_english" => $bankInfoEnglish["message"]["data"]["value"],
                "bank_info_arabic" => $bankInfoArabic["message"]["data"]["value"],
                "tin_from_setting" => $tinNumberFromSetting["message"]["data"]["value"],
                'total_order_en' => "$total_order Orders",
                'total_earning_en' => $currency .$total_final_earning,
                'subtotal_en' => $currency . $subtotal,
                'commission_rate_en' => $currency."$commission_order_rate/order",
                'commission_amount_en' => $currency . $lugmety_commission,
                'commission_vat_en' => "$commission_vat_rate %",
                'commission_vat_amount_en' => $currency .$lugmety_commission_vat_amount,
                'is_captain_driver' => $is_captain,
                'captain_salary_en' => $currency .$captain_salary,
                'grand_total_en' => $grand_total_en


            ];
            /**
             * create the csv file
             */
            $this->processHistortToCsv($getAllEarningsHistory,$commission_order_rate,$commission_vat_rate,$is_captain,$captain_salary,$fileNameInSlug,$currency);
            ob_clean();
            $path = storage_path() . "/exports/driver";
            if (!file_exists($path)) {
                if (!mkdir($path, 0777, true) && !is_dir($path)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $path));
                }
            }
            $html2pdf = new Mpdf(['tempDir' => storage_path() . "/exports/driver"]);
            $content = view("invoice-driver-en")->with(["data" => $data]);
            $html2pdf->autoScriptToLang = true;
            $html2pdf->autoLangToFont = true;
            $html2pdf->WriteHTML($content);
            $html2pdf->output(storage_path() . "/exports/driver/driver-invoice-en" . ".pdf");
//            ob_clean();
//            $html2pdf1 = new Mpdf(['tempDir' => storage_path() . "/exports"]);
//            $html2pdf1->autoScriptToLang = true;
//            $html2pdf1->autoLangToFont = true;
//            $contentInArabic = view("invoice-ar")->with(["data" => $data]);
//            $html2pdf1->writeHTML($contentInArabic);
//
//            $html2pdf1->output(storage_path() . "/exports/driver/driver-invoice-ar" . ".pdf");
            if (is_dir(storage_path() . '/exports/driver/ttfontdata')) {
                array_map('unlink', glob(storage_path() . '/exports/driver/ttfontdata/*'));
                rmdir(storage_path() . '/exports/driver/ttfontdata');
            }
            $zipper = new Zipper();
            $files = glob(storage_path() . '/exports/driver/*');
            $zipper->make(storage_path("/exports/driver/$fileNameInSlug.zip"))->add($files)->close();
            DB::commit();
            if (file_exists(storage_path("/exports/driver/$fileNameInSlug.zip"))) {
                array_map('unlink', glob(storage_path() . '/exports/driver/*.pdf'));
                array_map('unlink', glob(storage_path() . '/exports/driver/*.csv'));
                return response()->download(storage_path("/exports/driver/$fileNameInSlug.zip"))->deleteFileAfterSend(true);
            }



        }
        catch (\Exception $ex){
            $this->log->storeLogError(['error processing driver report',[
                'request' => $request->all(),
                'message' => $ex->getMessage()
            ]]);

            return response()->json([
                'status' => '500',
                'message' => 'Error processing driver invoice report'
            ],500);
        }

    }


    public function driverSpecificOrderListByIdForUser($id, Request $request)
    {
        $lang = Input::get('lang', 'en');
        try {
            if (!RoleChecker::hasRole("customer")) {
                return \Settings::getErrorMessageOrDefaultMessage('order-only-customer-can-view-driver-detail',
                    "Only customer can view driver detail.", 403, $lang);
            }
            $getDriverList = $this->driverOrder->getDriverBasedOnOrderId($id, $type = "customer");
            if (count($getDriverList) == 0) {
                return \Settings::getErrorMessageOrDefaultMessage('order-empty-driver-list',
                    "Empty driver list.", 404, $lang);
            }
            try {
                $order = $this->order->getSpecificOrder($id);
                if ($order->customer_id != RoleChecker::getUser()) {
                    return \Settings::getErrorMessageOrDefaultMessage('order-you-can-only-view-your-order',
                        "You can only view your order.", 403, $lang);
                }
            } catch (ModelNotFoundException $ex) {
                return \Settings::getErrorMessageOrDefaultMessage('order-order-could-not-be-found',
                    "Order could not be found.", 404, $lang);
            }
            $userDatas["users"] = array_unique($getDriverList->pluck("user_id")->toArray());
            $oauthUrl = Config::get('config.oauth_base_url') . "/review/users/list?lang=$lang";
            $getUserDetailsForReview = \RemoteCall::storeWithoutOauth($oauthUrl, $userDatas);
            if ($getUserDetailsForReview["status"] != "200") {
                if ($getUserDetailsForReview['status'] == 503) {
                    return response()->json(
                        $getUserDetailsForReview, $getUserDetailsForReview['status']);
                }
                return response()->json(
                    $getUserDetailsForReview["message"], $getUserDetailsForReview['status']);
            }
            $userDatasFromOauth = $getUserDetailsForReview["message"]["data"][0];
            $getDriverList["name"] = implode(" ", array($userDatasFromOauth["first_name"], $userDatasFromOauth["middle_name"], $userDatasFromOauth["last_name"]));
            $getDriverList["mobile"] = implode("", array($userDatasFromOauth["country_code"], $userDatasFromOauth["mobile"]));
            return response()->json([
                "status" => "200",
                "data" => $getDriverList
            ]);
        } catch (\Exception $ex) {
            return \Settings::getErrorMessageOrDefaultMessage('order-error-getting-data',
                "Error getting data.", 500, $lang);
        }

    }

    /**
     * returns all the driver whose pending order are to be delivered
     * @param Request $request
     * @return mixed
     */
    public function getAllAssignedDriver(Request $request){
//        $countryId = $request->get('country_id',52);
        try {
            $pendingDriver = $this->driverOrder->getAllAssignedDriver();

            return response()->json([
                'status' => '200',
                'data' => $pendingDriver
            ]);
        }
        catch (\Exception $ex){
            $this->log->storeLogError([
                'error listing assigned drivers',[
                    'message' => $ex->getMessage()
                ]
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Error listing assigned drivers'
            ]);
        }

    }


    /**
     * assign order to driver based on driver selected order
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignOrderToDriver(Request $request)
    {
        Log::info('requests',$request->all());
        $lang = Input::get('lang', 'en');
        if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
            return \Settings::getErrorMessageOrDefaultMessage('order-only-driver-has-access',
                "Only Driver has access.", 403, $lang);
        }



        try {
            $message = [
                "status_validation" => "Input status must have value assigned",
                "unique" => "Order is assigned to other driver"
            ];
            $this->validate($request, [
                "restaurant_id" => 'required|integer|min:1',
                "order_id" => 'required|integer|min:1',
                "status" => 'required|status_validation'
            ], $message);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        DB::beginTransaction();
        try {
            //checks if the order status is assigned , order is seen , payment type is not cash on pickup
            $checkOrder = $this->order->getOrdersByRestaurantAndOrderIdAndValidate($request["restaurant_id"], $request["order_id"]);
            if (!$checkOrder) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            $this->log->storeLogError(array("Restaurant order driver list", [
                "status" => "404",
                "message" => "Order is being assigned to another driver.Please try another order."
            ]));
            return \Settings::getErrorMessageOrDefaultMessage('order-orders-could-not-be-found-based-on-passed',
                "Order is being assigned to another driver.Please try another order.", 404, $lang);
        }

        try {
            $status = $request['status'];
            $request = $request->all();
            $request["user_id"] = RoleChecker::getUser();
            /*
            * checks if the passed driver  has previous order to be delivered
            */
            $checkUser = $this->driverOrder->checkUser($request["user_id"]);
            if ($checkUser) {
                return \Settings::getErrorMessageOrDefaultMessage('order-requested-user-id-has-pending-order-to-be-delivered',
                    "Requested user_id has pending order to be delivered.", 403, $lang);
            }
            $checksIfOrderIsassignedToAnotherDriver = $this->driverOrder->checkAnotherDriverIsAssigned($request["user_id"],$request["restaurant_id"], $request["order_id"]);
            if($checksIfOrderIsassignedToAnotherDriver){
                return response()->json([
                    "status" => "403",
                    "message" => "Order is assigned to another driver."
                ],403);
            }
            $request["country_id"] = $checkOrder->country_id;
            $request["city_id"] = $checkOrder->city_id;
            $request["source_district_id"] = $checkOrder->district_id;
            $request["destination_district_id"] = $checkOrder->shipping_district_id;
            $request["distance"] = $checkOrder->distance;
            $request["price"] = $checkOrder->amount;
            $request["pickup_time"] = $this->getTime($checkOrder)->toDateTimeString();
            $request["order"] = serialize($checkOrder->toArray());
            $request["restaurant"] = $checkOrder->restaurant;
            $restaurant = unserialize($checkOrder->restaurant);
            $data["$status"] = [
                "status" => $status,
                "date" => Carbon::now('utc')->tz($restaurant["timezone"])->format("d M Y h:i A")
            ];
            $request["order_history"] = serialize($data);

            $is_driver_available = RoleChecker::get('is_available');
            if($is_driver_available || $is_driver_available == 0 ){
                $oauthUrl = Config::get('config.oauth_base_url').'/driver/availability';
                $updateDriverAvailability = RemoteCall::store($oauthUrl,[
                    'availability' => 1
                ]);

                if ($updateDriverAvailability["status"] != 200) {
                    return response()->json([
                        $updateDriverAvailability["message"]
                    ], $updateDriverAvailability["status"]);
                }
            }

            $assignOrderToDriver = $this->driverOrder->createDriverOrder($request);
            $checkOrder->update(["delivery_status" => $request["status"]]);
            $order = $checkOrder->toArray();
            $order['restaurant'] = unserialize($checkOrder->restaurant);
            $order['extra_info'] = unserialize($order['extra_info']);


            $array = [
                'service' => 'order service',
                'message' => 'order accepted by driver',
                'data' => [
                    'user_details' => ['id' => $checkOrder->customer_id],
                    'driver_id' => $request["user_id"],
                    'order' => $order,
                    'restaurant_id' => $checkOrder->restaurant_id,
                    'parent_id' => $order['restaurant']['parent_id'],
                ]
            ];
            try {
                event(new SendMessage($array));
            } catch (\Exception $ex) {
                $this->log->storeLogError(array("Restaurant order driver list", [
                    "status" => "500",
                    "message" => $ex->getMessage()
                ]));
                return \Settings::getErrorMessageOrDefaultMessage('order-order-could-not-be-assigned-to-driver',
                    "Order could not be assigned to driver.", 500, $lang);
            }
            DB::commit();
            $this->log->storeLogInfo(array("Restaurant order driver list", [
                "status" => "200",
                "message" => "Order Successfully assigned to driver",
                "data" => $assignOrderToDriver
            ]));
            $payload['id'] = $assignOrderToDriver->id;
            return \Settings::getErrorMessageOrDefaultMessageWithPayload('order-order-assigned-successfully',
                "Order assigned successfully.", 200, $lang, $payload);
        }
        catch (DuplicateRecordException $ex){
            $this->log->storeLogError(array("Assign driver to order error", [
                "status" => "500",
                'data' => $request,
                "message" => $ex->getMessage()
            ]));
            return response()->json([
                'status' => '403',
                'message' => 'Order is being assigned to another driver.Please try another order.'
            ],403);
        }
        catch (\Exception $ex) {
            $this->log->storeLogError(array("Restaurant order driver list", [
                "status" => "500",
                "message" => $ex->getMessage()
            ]));
            return \Settings::getErrorMessageOrDefaultMessage('order-order-could-not-be-assigned-to-driver',
                "Order could not be assigned to driver.", 500, $lang);
        }
    }

    /**
     * assign order to new driver by admin
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignedOrderToDriverByAdmin(Request $request)
    {
        try {
            $message = [
                "status_validation" => "Input status must have value assigned",
                "unique" => "Order is assigned to other driver"
            ];
            $this->validate($request, [
                "restaurant_id" => 'required|integer|min:0',
                "order_id" => 'required|integer|min:1',
                "user_id" => 'required|integer|min:1',
                "status" => 'required|status_validation'
            ], $message);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        DB::beginTransaction();

        try {
            //checks if the order status is assigned , order is seen , payment type is not cash on pickup
            $checkOrder = $this->order->getOrdersByRestaurantAndOrderIdAndValidate($request["restaurant_id"], $request["order_id"]);
            if (!$checkOrder) {
                throw new \Exception();
            }
        } catch (\Exception $ex) {
            $this->log->storeLogError(array("Restaurant order driver list", [
                "status" => "404",
                "message" => "Order is not allowed to assign.Please accept the order first."
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Order is already assigned to driver.Please try loading the page."
            ], 404);
        }
        try {
            $status = $request["status"];
            $request = $request->all();
            /*
            * checks if the passed driver  has previous order to be delivered
            */
            $userId = $request["user_id"];
            $checkUser = $this->driverOrder->checkUser($userId);

            if ($checkUser) {
                return response()->json([
                    "status" => "403",
                    "message" => "Requested driver has pending order to be delivered"
                ], 403);
            }
            $request["country_id"] = $checkOrder->country_id;
            $request["city_id"] = $checkOrder->city_id;
            $request["source_district_id"] = $checkOrder->district_id;
            $request["destination_district_id"] = $checkOrder->shipping_district_id;
            $request["distance"] = $checkOrder->distance;
            $request["price"] = $checkOrder->amount;
            $request["pickup_time"] = $this->getTime($checkOrder)->toDateTimeString();
            $request["order"] = serialize($checkOrder->toArray());
            $request["restaurant"] = $checkOrder->restaurant;
            $restaurant = unserialize($request["restaurant"]);
            $data["$status"] = [
                "status" => $status,
                "date" => Carbon::now('utc')->tz($restaurant['timezone'])->format("d M Y h:i A")
            ];
            $request["order_history"] = serialize($data);

            //calling oauth service to check user has role driver
            $baseUrlForOauth = Config::get('config.oauth_base_url');
            $checkUserRoleIsDriver = RemoteCall::getSpecific($baseUrlForOauth . "/user/$userId/driver");
            if ($checkUserRoleIsDriver["status"] != 200) {
                return response()->json([
                    $checkUserRoleIsDriver["message"]
                ], $checkUserRoleIsDriver["status"]);
            }
            $assignOrderToDriver = $this->driverOrder->createDriverOrder($request);
            $checkOrder->update(["delivery_status" => $request["status"]]);
            $order = unserialize($assignOrderToDriver['order']);
            $order['restaurant'] = unserialize($assignOrderToDriver['restaurant']);
            $order['extra_info'] = unserialize($order['extra_info']);

            $this->log->storeLogInfo(array("Restaurant order driver list", [
                "status" => "200",
                "message" => "Order Successfully assigned to driver",
                "data" => $assignOrderToDriver
            ]));
            $array = [
                'service' => 'order service',
                'message' => 'order accepted by driver',
                'data' => [
                    'user_details' => ['id' => $order['customer_id']],
                    'driver_id' => $userId,
                    'order' => $order,
                    'restaurant_id' => $order['restaurant_id'],
                    'parent_id' => $order['restaurant']['parent_id']
                ]
            ];

            event(new SendMessage($array));
            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Order Successfully assigned to driver"
            ]);
        }
        catch (DuplicateRecordException $ex){
            $this->log->storeLogError(array("Admin Assign driver to order error", [
                "status" => "500",
                'data' => $request,
                "message" => $ex->getMessage()
            ]));
            return response()->json([
                'status' => '403',
                'message' => 'Order is being assigned to another driver.Please try another order.'
            ],403);
        }

        catch (\Exception $ex) {
            $this->log->storeLogError(array("Restaurant order driver list", [
                "status" => "500",
                "message" => $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Order could not be assigned to driver"
            ], 500);
        }
    }

    /**
     * updated assigned order by driver, only assigned driver can update it where the status is assigned
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAssignedOrderToDriver($id, Request $request)
    {
        $lang = Input::get('lang', 'en');
        if (!(RoleChecker::hasRole('driver') || RoleChecker::hasRole('driver-captain'))) {
            return \Settings::getErrorMessageOrDefaultMessage('order-only-driver-has-access',
                "Only Driver has access.", 403, $lang);
        }
        DB::beginTransaction();
        try {
            $message = [
                "status_validation_driver_update" => "Input status must have value completed or picked",
            ];
            $this->validate($request, [
                "status" => 'required|status_validation_driver_update'
            ], $message);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $userId = RoleChecker::getUser();

            $is_driver_available = RoleChecker::get('is_available');

            if($is_driver_available || $is_driver_available == 0 ){
                $oauthUrl = Config::get('config.oauth_base_url').'/driver/availability';
                $updateDriverAvailability = RemoteCall::store($oauthUrl,[
                    'availability' => 1
                ]);

                if ($updateDriverAvailability["status"] != 200) {
                    return response()->json([
                        $updateDriverAvailability["message"]
                    ], $updateDriverAvailability["status"]);
                }
            }

            $assignedOrder = $this->driverOrder->getDriverOrderBasedOnId($id);
            /**
             * checks if the user who created can update or not , assigned order has status assigned
             */
            if (!(($assignedOrder->user_id == $userId) || ($assignedOrder->status == "assigned") || ($assignedOrder->status == "picked"))) {
                return \Settings::getErrorMessageOrDefaultMessage('order-you-can-update-your-assigned-or-picked-order-only',
                    "You can update your assigned or picked order only.", 403, $lang);
            }
            if (($assignedOrder->status == "assigned")) {
                if (($request->status == "picked")) goto updateDriver;
                else {
                    return \Settings::getErrorMessageOrDefaultMessage('order-request-status-must-be-pending-in-case-of-order-status-is-assigned',
                        "Request status must be pending in case of order status is assigned.", 403, $lang);
                }
            } else {
                if (($request->status == "completed")) goto updateDriver;
                else {
                    return \Settings::getErrorMessageOrDefaultMessage('order-request-status-must-be-completed-in-case-of-order-status-is-pickup',
                        "Request status must be completed in case of order status is pickup.", 403, $lang);
                }
            }
            updateDriver:
            if ($request->status == "completed") {
                $delivery_status = "delivered";
            } else {
                $delivery_status = $request->status;
            }
            $restaurant = unserialize($assignedOrder["restaurant"]);
            $timezone = $restaurant["timezone"];
            $orderHistory = [];
            if (isset($assignedOrder["order_history"]) && !empty($assignedOrder["order_history"])) {
                $orderHistory = unserialize($assignedOrder["order_history"]);
            }
            $now = Carbon::now('utc');
            $orderHistory["$delivery_status"] = [
                "status" => $delivery_status,
                "date" => $now->tz($timezone)->format("d M Y h:i A")
            ];
            $request->merge([
                "order_history" => serialize($orderHistory)
            ]);

            $order = $this->order->getSpecificOrder($assignedOrder->order_id);

            if ($order['payment_method'] == "cash-on-delivery" && $request->status == "completed") {
                $order->update([
                    "delivery_status" => $delivery_status,
                    "payment_status" => "completed"
                ]);
            } elseif ($delivery_status == "picked") {
                $request->merge([
                    "driver_picked_time" => $now->tz('utc')
                ]);
                $order->update([
                    "delivery_status" => $delivery_status
                ]);
            } else {
                $order->update([
                    "delivery_status" => $delivery_status
                ]);
            }
            /**
             * calls exp and earning calculation function
             */
            if ($request->status == "completed") {
                $this->log->storeLogInfo([
                    'creating_earning',[
                        'id' => $assignedOrder->id
                    ]
                ]);
                $calculateXpAndEarning = $this->calculateXpAndEarning($assignedOrder, $order, false);
            }
            $originalStatus = $request->status;
            $request->merge(["status" => $delivery_status]);
            if ($delivery_status == "picked") {
                $updateDriverOrder = $this->driverOrder->updateDriverOrder($assignedOrder, $request->only('status', "order_history", "driver_picked_time"));
            } else {
                $updateDriverOrder = $this->driverOrder->updateDriverOrder($assignedOrder, $request->only('status', "order_history"));

            }

            $order = unserialize($assignedOrder['order']);
            $order['restaurant'] = unserialize($assignedOrder['restaurant']);
            $order['extra_info'] = unserialize($order['extra_info']);
            $array = [
                'service' => 'order service',
                'message' => 'order ' . $originalStatus,
                'data' => [
                    'user_details' => ['id' => $order['customer_id']],
                    'driver_id' => $userId,
                    'order' => $order,
                    'restaurant_id' => $order['restaurant_id'],
                    'parent_id' => $order['restaurant']['parent_id']
                ]
            ];
            // return $array;

            event(new SendMessage($array));
            DB::commit();
            return \Settings::getErrorMessageOrDefaultMessage('order-assigned-driver-order-updated-successfully',
                "Assigned driver order updated successfully.", 200, $lang);
        } catch (ModelNotFoundException $ex) {
            $this->log->storeLogError(array("Restaurant order driver list", [
                "status" => "404",
                "message" => "Driver order could not be found"
            ]));
            return \Settings::getErrorMessageOrDefaultMessage('order-driver-order-could-not-be-found',
                "Driver order could not be found.", 404, $lang);
        } catch (\Exception $ex) {
            $this->log->storeLogError(array("update assign order", [
                "status" => "500",
                "message" => $ex->getMessage()
            ]));
            return \Settings::getErrorMessageOrDefaultMessage('order-order-could-not-be-assigned-to-driver',
                "Order could not be assigned to driver.", 500, $lang);
        }
    }

    public function importEarning(){
        set_time_limit(0);
        $todays_driver_order = $this->driverOrder->getTodayDeliveredOrder();

        $list_order_ids = $todays_driver_order->pluck('order_id')->values()->all();
        $list_order_ids_earning = $this->driverEarning->getTodayEarning($list_order_ids)->values()->all();
        $array_diff = array_values(array_unique(array_diff($list_order_ids,$list_order_ids_earning)));
        $not_created_driver_earning = collect($todays_driver_order)->whereIn('order_id',$array_diff)->all();
        $current_date = Carbon::now('utc')->format('Y-m-d H:i:s');
        foreach ($not_created_driver_earning as $driver_order){
            $order_detail = unserialize($driver_order['order']);
            if(!isset($order_detail['delivery_fee'])) continue;
            $this->driverEarning->createDriverEarning([
                'user_id' => $driver_order['user_id'],
                'order_id' => $driver_order['order_id'],
                'type' => 'normal',
                'date_for' => $current_date,
                'amount' => $order_detail['delivery_fee'],
                'country_id' => $order_detail['country_id']

            ]);
            sleep(1);
        }

        return 'sucess';

    }

    /**
     * updated assigned order by driver, only assigned driver can update it where the status is assigned
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAssignedOrderToDriverByAdmin($id, Request $request)
    {
        DB::beginTransaction();
        try {
            $message = [
                "status_validation_driver_update" => "Input status must have value completed or picked",
            ];
            $this->validate($request, [
                "status" => 'required|status_validation_driver_update'
            ], $message);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {

            $assignedOrder = $this->driverOrder->getAssignedDriverBasedOnOrderId($id);
            /**
             * checks if the user who created can update or not , assigned order has status assigned or pickup
             */
            if (!($assignedOrder->status == "assigned" || $assignedOrder->status == "picked")) {
                return response()->json([
                    "status" => "403",
                    "message" => "You can update either assigned or picked order only."
                ], 403);
            }

            if (($assignedOrder->status == "assigned")) {
                if (($request->status == "picked")) goto updateDriver;
                else {
                    return response()->json([
                        "status" => "403",
                        "message" => "Request status must be pending in case of order status is assigned."
                    ], 403);
                }
            } else {
                if (($request->status == "completed")) goto updateDriver;
                else {
                    return response()->json([
                        "status" => "403",
                        "message" => "Request status must be completed in case of order status is pickup."
                    ], 403);
                }
            }
            updateDriver:
            $currentTime = Carbon::now('utc');
            $order = $this->order->getSpecificOrder($assignedOrder->order_id);
            if ($request->status == "completed") {
                $delivery_status = "delivered";
            } else {
                $delivery_status = $request->status;
            }
            if ($order['payment_method'] == "cash-on-delivery" && $request->status == "completed") {
                $order->update([
                    "delivery_status" => "delivered",
                    "payment_status" => $delivery_status,
                    "is_seen" => "1"
                ]);
            } elseif ($delivery_status == "picked") {
                $request->merge([
                    "driver_picked_time" => $currentTime->tz('utc')
                ]);
                $order->update([
                    "delivery_status" => $delivery_status,
                    "is_seen" => "1"
                ]);
            } else {

                $order->update([
                    "delivery_status" => $delivery_status,
                    "is_seen" => "1"
                ]);
            }
            /**
             * calls exp and earning calculation function
             */

            if ($request->status == "completed") $calculateXpAndEarning = $this->calculateXpAndEarning($assignedOrder, $order,true);

            $originalStatus = $request->status;
            $request->merge(["status" => $delivery_status]);
            $restaurant = unserialize($assignedOrder["restaurant"]);
            $timezone = $restaurant["timezone"];
            $orderHistory = [];
            if (isset($assignedOrder["order_history"]) && !empty($assignedOrder["order_history"])) {
                $orderHistory = unserialize($assignedOrder["order_history"]);
            }

            $orderHistory["$delivery_status"] = [
                "status" => $delivery_status,
                "date" => $currentTime->tz($timezone)->format("d M Y h:i A")
            ];
            $request->merge([
                "order_history" => serialize($orderHistory)
            ]);
            if ($delivery_status == "picked") {
                $updateDriverOrder = $this->driverOrder->updateDriverOrder($assignedOrder, $request->only('status', "order_history", "driver_picked_time"));
            } else {
                $updateDriverOrder = $this->driverOrder->updateDriverOrder($assignedOrder, $request->only('status', "order_history"));

            }
            $order = unserialize($assignedOrder['order']);
            $order['restaurant'] = unserialize($assignedOrder['restaurant']);
            $order['extra_info'] = unserialize($order['extra_info']);
            $array = [
                'service' => 'order service',
                'message' => 'order completed by admin',
                'data' => [
                    'user_details' => ['id' => $order['customer_id']],
                    'driver_id' => $assignedOrder->user_id,
                    'order' => $order,
                    'restaurant_id' => $order['restaurant_id'],
                    'parent_id' => $order['restaurant']['parent_id']
                ]
            ];
            if ($originalStatus == "completed") event(new SendMessage($array));

            DB::commit();
            return response()->json([
                "status" => "200",
                "message" => "Assigned driver order updated successfully"
            ]);
        } catch (ModelNotFoundException $ex) {
            $this->log->storeLogError(array("Restaurant order driver list", [
                "status" => "404",
                "message" => "Driver order could not be found"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Driver order could not be found"
            ], 404);
        } catch (\Exception $ex) {
            $this->log->storeLogError(array("update assign order", [
                "status" => "500",
                "message" => $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Order could not be assigned to driver"
            ], 500);
        }
    }

    /**
     * this endpoint is used to stop assigning order from driver
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function stopAssigningOrderFromDriver($id,Request $request){
        DB::beginTransaction();
        try {
            $checkOrder = $this->driverOrder->getAssignedDriverBasedOnOrderId($id);

        } catch (ModelNotFoundException $ex) {
            $this->log->storeLogError(array("Restaurant order driver list", [
                "status" => "404",
                "message" => "Driver order could not be found"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Driver order could not be found"
            ], 404);
        }
        try {

            /*
             * updates the order delivery status to pending
             */
            $realOrder = $this->order->getSpecificOrder($checkOrder->order_id);
            $realOrder->update([
                "delivery_status" => "pending"
            ]);
            $restaurant = unserialize($checkOrder["restaurant"]);
            $timezone = $restaurant["timezone"];
            $historyOfPreviousDriver = [];
            if (isset($checkOrder["order_history"]) && !empty($checkOrder["order_history"])) {
                $historyOfPreviousDriver = unserialize($checkOrder["order_history"]);
            }
            $request->merge([
                'status' => 'reassigned'
            ]);
            $status = $request->status;
            $historyOfPreviousDriver["$status"] = [
                "status" => "$status",
                "date" => Carbon::now('utc')->tz($timezone)->format("d M Y h:i A")
            ];
            $request->merge(["order_history" => serialize($historyOfPreviousDriver)]);
            /**
             * updates the status of previously assigned order driver to reassigned
             */
            $updatePreviousUserStatus = $checkOrder->update($request->only("status", "order_history"));

            DB::commit();

            $this->log->storeLogInfo(array("pause assigned order task", [
                "status" => "200",
                "message" => "Order Successfully unassigned",
                "data" => $request->all()
            ]));
            DB::commit();
            return response()->json([
                "status" => 200,
                "message" => "Order Successfully unassigned"
            ], 200);
        } catch (\Exception $ex) {
            $this->log->storeLogError(array("order unassigned", [
                "status" => "500",
                "message" => $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Order could not be unassigned from driver"
            ], 500);
        }
    }

    /**
     * assign previously assigned order to new driver by admin
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAssignedOrderToOtherDriverByAdmin($id, Request $request)
    {
        DB::beginTransaction();
        try {
            $message = [
                "status_validation_driver_update_by_admin" => "Input status must have value reassigned",
            ];
            $this->validate($request, [
                "user_id" => "required|integer|min:1",
                "status" => 'required|status_validation_driver_update_by_admin'
            ], $message);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }

        try {
            $checkOrder =$this->driverOrder->getAssignedDriverBasedOnOrderId($id);       //$checkOrder =$this->driverOrder-> $this->driverOrder->getDriverOrderBasedOnId($id);
            if ($checkOrder->status == "completed") {
                return response()->json([
                    "status" => "403",
                    "message" => "Completed Order can not be assigned to another driver"
                ], 403);
            }

        } catch (ModelNotFoundException $ex) {
            $this->log->storeLogError(array("Restaurant order driver list", [
                "status" => "404",
                "message" => "Driver order could not be found"
            ]));
            return response()->json([
                "status" => "404",
                "message" => "Driver order could not be found"
            ], 404);
        }
        try {
            $userId = $request["user_id"];
            /*
             * checks if the passed driver  has previous order to be delivered
             */
            $checkUser = $this->driverOrder->checkUser($userId);

            if ($checkUser) {
                return response()->json([
                    "status" => "403",
                    "message" => "Requested user_id has pending order to be delivered"
                ], 403);
            }

            //calling oauth service to check user has role driver
            $baseUrlForOauth = Config::get('config.oauth_base_url');
            $checkUserRoleIsDriver = RemoteCall::getSpecific($baseUrlForOauth . "/user/$userId/driver");
            if ($checkUserRoleIsDriver["status"] != 200) {
                return response()->json([
                    $checkUserRoleIsDriver["message"]
                ], $checkUserRoleIsDriver["status"]);
            }
            /**
             * updates the status of previously assigned order driver to reassigned
             */
            $realOrder = $this->order->getSpecificOrder($checkOrder->order_id);
            $realOrder->update([
                "delivery_status" => "assigned"
            ]);
            $restaurant = unserialize($checkOrder["restaurant"]);
            $timezone = $restaurant["timezone"];
            $coppyOrderDetailsToReassignedUser = $checkOrder;
            $historyOfPreviousDriver = [];
            if (isset($checkOrder["order_history"]) && !empty($checkOrder["order_history"])) {
                $historyOfPreviousDriver = unserialize($checkOrder["order_history"]);
            }
            $status = $request->status;
            $historyOfPreviousDriver["$status"] = [
                "status" => "$status",
                "date" => Carbon::now('utc')->tz($timezone)->format("d M Y h:i A")
            ];
            $request->merge(["order_history" => serialize($historyOfPreviousDriver)]);
            $updatePreviousUserStatus = $checkOrder->update($request->only("status", "order_history"));
            $coppyOrderDetailsToReassignedUser["user_id"] = $userId;
            $coppyOrderDetailsToReassignedUser["status"] = 'assigned';

            $history["assigned"] = [
                "status" => "assigned",
                "date" => Carbon::now('utc')->tz($timezone)->format("d M Y h:i A")
            ];
            $coppyOrderDetailsToReassignedUser["order_history"] = serialize($history);
            $coppyOrderDetailsToReassignedUser = $coppyOrderDetailsToReassignedUser->toArray();
            $reassignOrderToNewDriver = $this->driverOrder->createDriverOrder($coppyOrderDetailsToReassignedUser);
            $order = unserialize($reassignOrderToNewDriver['order']);
            $order['restaurant'] = unserialize($reassignOrderToNewDriver['restaurant']);
            $order['extra_info'] = unserialize($order['extra_info']);

            $array = [
                'service' => 'order service',
                'message' => 'order reassign',
                'data' => [
                    'user_details' => ['id' => $order['customer_id']],
                    'order' => $order,
                    'restaurant_id' => $order['restaurant_id'],
                    'parent_id' => $order['restaurant']['parent_id'],
                    'driver_id' => $userId,
                ]
            ];

            event(new SendMessage($array));

            DB::commit();

            $this->log->storeLogInfo(array("Restaurant order driver list", [
                "status" => "200",
                "message" => "Order Successfully reassigned",
                "data" => $reassignOrderToNewDriver
            ]));
            DB::commit();
            return response()->json([
                "status" => 200,
                "message" => "Order Successfully reassigned"
            ], 200);
        } catch (\Exception $ex) {
            $this->log->storeLogError(array("order ressign", [
                "status" => "500",
                "message" => $ex->getMessage()
            ]));
            return response()->json([
                "status" => "500",
                "message" => "Order could not be reassigned to driver"
            ], 500);
        }

    }

    public function trackYourOrder($id)
    {
        try {
            try {
                $order = $this->order->getSpecificOrder($id);
            } catch (\Exception $ex) {
                return response()->json([
                    "status" => "404",
                    "message" => "Order could not be found."
                ], 404);
            }
            if (is_null($order->shipping_id) || $order->shipping_id < 0 || empty($order->shipping_id)) {
                return response()->json([
                    "status" => "403",
                    "message" => "Delivery type should either be home delivery or cash on delivery"
                ], 403);
            }

            /**
             * get location service base url from Config/config.php
             */
            $customerId = $order->customer_id;
            $shippingId = $order->shipping_id;
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/user/$customerId/shipping/$shippingId";
            /**
             * calling to location service
             */
            $countryInfo = RemoteCall::getSpecificWithoutToken($countryFullUrl);
            if ($countryInfo["status"] != "200") {
                if ($countryInfo["status"] == "503") {
                    $status = $countryInfo["status"];
                    return response()->json([
                        "status" => $status,
                        "message" => $countryInfo["message"]
                    ], $status);
                }
                return response()->json($countryInfo["message"], $countryInfo["status"]);
            }
            $trackOrder["shipping_latitude"] = $countryInfo["message"]["data"]["latitude"];
            $trackOrder["shipping_longitude"] = $countryInfo["message"]["data"]["longitude"];
            $restaurantdata["restaurant_ids"] = [$order->restaurant_id];
            $restaurantdata["lang"] = "en";//$driverUrlInput["lang"];
            //calling restaurant service to get restaurant name list
            $restaurantBaseUrl = Config::get('config.restaurant_base_url');
            $restaurantDetails = RemoteCall::store($restaurantBaseUrl . '/public/branch', $restaurantdata);
            if ($restaurantDetails["status"] != 200) {
                return response()->json([
                    "status" => $restaurantDetails["status"],
                    "message" => $restaurantDetails["message"]["message"]
                ], $restaurantDetails["status"]);
            }
            $restaurantDatasFromRestautant = $restaurantDetails["message"]["data"][0];
            $trackOrder["restaurant_latitude"] = $restaurantDatasFromRestautant["latitude"];
            $trackOrder["restaurant_longitude"] = $restaurantDatasFromRestautant["longitude"];

            /**
             * getting driver location
             */
            $trackOrder["driver_location"] = false;

            if ($order->delivery_status == "picked") {
                $driverOrder = $this->driverOrder->getDriverOrderByOrderId($id);
                if (!$driverOrder) {
                    return response()->json([
                        "status" => "404",
                        "message" => "Driver location could not be found."
                    ], 404);
                }
                $driverOrderLocation = $this->driverOrderLocation->getDriverOrderLocationByDriverOrderId($driverOrder->order_id);
                $driverOrderLocation = collect($driverOrderLocation)->where("driver_id", $driverOrder->user_id);
                if (count($driverOrderLocation) != 0) {
                    $trackOrder["driver_location"] = true;
                    $trackOrder["driver_latitude"] = $driverOrderLocation[0]["latitude"];
                    $trackOrder["driver_longitude"] = $driverOrderLocation[0]["longitude"];
                }
            }
            return response()->json([
                "status" => "200",
                "data" => $trackOrder
            ], 200);

        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ], 500);
        }
    }

    /** get overall earning xp and payout summery of passed driver
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSummaryForAdmin(Request $request)
    {
        try {
            $this->validate($request, [
                "user_id" => "required|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $CountryEarningList = array_unique(collect($this->driverEarning->getAllDriverEarningForAdmin($request->user_id))->where("country_id", "!=", null)->pluck("country_id")->toArray());
            $countryXpList = array_unique(collect($this->driverXp->getAllDriverXpForSummary($request->user_id))->where("country_id", "!=", null)->pluck("country_id")->toArray());
            $countryPayoutRequest = array_unique(collect($this->driverEarningWithdrawl->getAllDriverWithdrawnRequest($request->user_id))->where("country_id", "!=", null)->pluck("country_id")->toArray());
            $countryLists = ["ids" => array_values(array_unique(array_merge($CountryEarningList, $countryXpList, $countryPayoutRequest)))];
            if (empty($countryLists["ids"])) {
                return response()->json([
                    "status" => "404",
                    "message" => "Given user record could not be found."
                ], 404);
            }

            /**
             * get location service base url from Config/config.php
             */
            $countryBaseUrl = Config::get("config.location_base_url");
            $countryFullUrl = "$countryBaseUrl/country/list";

            /**
             * calling to location service
             */
            $countryInfo = \RemoteCall::storeWithoutOauth($countryFullUrl, $countryLists);
            /**
             * if location service gives status message other than zero then display the message
             */

            if ($countryInfo['status'] != 200) {
                if ($countryInfo['status'] != 503) {
                } else {
                    return response()->json(
                        $countryInfo, $countryInfo['status']);
                }
                return response()->json(
                    $countryInfo["message"], $countryInfo['status']);
            }
            $countryLists = $countryInfo["message"]["data"];
            $summary = [];
            foreach ($countryLists as $key => $list) {
                $summary[$key]["country_name"] = $list["name"];
                $summary[$key]["currency"] = $list["currency_symbol"];
                $summary[$key]["id"] = $list["id"];
                $summary[$key]["earning"]["available_earning"] = $this->getTotalEarning($request->user_id, $list["id"]);
                $summary[$key]["earning"]["total_earning"] = $this->driverEarning->getAllDriverEarningNormal($request->user_id, $list["id"]);
                $getUserStatusInfo = $this->driverEarningWithdrawl->getAllStatusdata($request->user_id, $list["id"]);
                $summary[$key]["payouts"]["paid"] = $getUserStatusInfo["paid"];
                $summary[$key]["payouts"]["pending"] = $getUserStatusInfo["pending"];
                $summary[$key]["payouts"]["rejected"] = $getUserStatusInfo["rejected"];
                $summary[$key]["xp"]["available_xp"] = $this->getTotalXp($request->user_id, $list["id"]);
                $summary[$key]["xp"]["total_xp"] = $this->driverXp->getAllDriverXpNormal($request->user_id, $list["id"]);
            }
            return response()->json([
                "status" => "200",
                "data" => $summary
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => "Error getting data"
            ], 500);
        }

    }

    /**
     * get country earning rule to calculate price
     * @param $countryId
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEarningRule($countryId, Request $request)
    {
        try {
            $this->validate($request, [
                "timezone" => "required|string"
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }
        try {
            $currentDate = Carbon::now('utc')->tz($request->timezone);
            $earningRules = $this->earningRule->getAllRules($countryId);
            $selectedEarningRule = $this->getSelectedRule($earningRules, $currentDate, $request->timezone);
            if (is_null($selectedEarningRule)) {
                return response()->json([
                    "status" => '404',
                    "message" => "Earning rule doesnot exist."
                ], 404);
            }
            return response()->json([
                "status" => "200",
                "data" => $selectedEarningRule
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                "status" => "500",
                "message" => "Error getting rules"
            ]);
        }
    }

    private function manualPagination($limit, $data, $count)
    {
        $page = Input::get('page', 1);
        $paginate = $limit;
        $offSet = ($page * $paginate) - $paginate;
//        dd($data);
        $itemsForCurrentPage = array_slice($data, $offSet, $paginate, false);
        return new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, $count, $paginate, $page);

    }


    private function getTimeForDriver($checkOrder)
    {
        $timezone = unserialize($checkOrder->restaurant)["timezone"];
        if ($checkOrder->pre_order == "1") {

            $time = Carbon::createFromFormat("Y-m-d H:i:s", $checkOrder->delivery_or_pickup_time, $timezone)->tz("utc");
//            $time = Carbon::parse($checkOrder->delivery_or_pickup_time);
            return $time;
        }
        $diffFromAPi = 0;
        if (!is_null($checkOrder->threshold_delivery_time) && !is_null($checkOrder->api_delivery_time)) {

            $thresholdDeliveryTime = Carbon::parse($checkOrder->threshold_delivery_time);
            $apiDeliveryTime = Carbon::parse($checkOrder->api_delivery_time);
            $diffInminutes = $thresholdDeliveryTime->diffInMinutes($apiDeliveryTime);
            if ($thresholdDeliveryTime->gte($apiDeliveryTime)) {
                $diffFromAPi = $thresholdDeliveryTime->addMinutes($diffInminutes);
            } else {
                $diffFromAPi = $apiDeliveryTime->addMinutes($diffInminutes);
            }

        } else if (!is_null($checkOrder->threshold_delivery_time)) {
            $diffFromAPi = $thresholdDeliveryTime = Carbon::parse($checkOrder->threshold_delivery_time);

        } else {
            $apiDeliveryTime = Carbon::parse($checkOrder->api_delivery_time);
            $diffFromAPi = $apiDeliveryTime;
        }


        $time = Carbon::createFromFormat("Y-m-d H:i:s", $diffFromAPi, $timezone)->tz("utc");

        return $time;

    }

    /**
     * returns  delivery time for driver
     * @param $checkOrder
     * @return static
     */
    private function getTime($checkOrder)
    {
        $timezone = unserialize($checkOrder->restaurant)["timezone"];
//        if ($checkOrder->pre_order == "1") {
//
//        $time = Carbon::createFromFormat("Y-m-d H:i:s",$checkOrder->delivery_or_pickup_time,$timezone)->tz("utc");
////            $time = Carbon::parse($checkOrder->delivery_or_pickup_time);
//            return $time;
//        }
//        $diffFromAPi =0;
//        if (!is_null($checkOrder->threshold_delivery_time) && !is_null($checkOrder->api_delivery_time)) {
//
//            $thresholdDeliveryTime = Carbon::parse($checkOrder->threshold_delivery_time);
//            $apiDeliveryTime = Carbon::parse($checkOrder->api_delivery_time);
//            $diffInminutes = $thresholdDeliveryTime->diffInMinutes($apiDeliveryTime);
//            if ($thresholdDeliveryTime->gte($apiDeliveryTime)) {
//                $diffFromAPi = $thresholdDeliveryTime->addMinutes($diffInminutes);
//            } else {
//                $diffFromAPi = $apiDeliveryTime->addMinutes($diffInminutes);
//            }
//
//        } else if (!is_null($checkOrder->threshold_delivery_time)) {
//            $diffFromAPi = $thresholdDeliveryTime = Carbon::parse($checkOrder->threshold_delivery_time);
//
//        } else {
//            $apiDeliveryTime = Carbon::parse($checkOrder->api_delivery_time);
//            $diffFromAPi = $apiDeliveryTime;
//        }


        $time = Carbon::createFromFormat("Y-m-d H:i:s", $checkOrder->api_delivery_time, $timezone);

        return $time;

    }

    /**
     * calculate xp and earning rule and store it
     * @param $assignedOrder
     */
    private function calculateXpAndEarning($assignedOrder, $order,$isAdmin = false )
    {
        $restaurant = unserialize($assignedOrder["restaurant"]);
        $timeZone = $restaurant["timezone"];
        $currentDate = Carbon::now("utc")->tz($timeZone);
        $xpRules = $this->xpRule->getAllRules($assignedOrder->country_id);

        $selectedXpRule = $this->getSelectedRule($xpRules, $currentDate, $timeZone);
        $this->log->storeLogInfo([
            'xp_rules',[
                'rules' => $selectedXpRule->toArray()
            ]
        ]);
        $earningRules = $this->earningRule->getAllRules($assignedOrder->country_id);
        $selectedEarningRule = $this->getSelectedRule($earningRules, $currentDate, $timeZone);

        $this->log->storeLogInfo([
            'earning_rules',[
                'rules' => $selectedEarningRule->toArray()
            ]
        ]);

        if (is_null($selectedXpRule) || is_null($selectedEarningRule)) return false;
        $storeEarning = $this->calculateEarningAndStore($assignedOrder, $selectedEarningRule, $order,$isAdmin);
        $storexp = $this->calculateXpAndStore($assignedOrder, $selectedXpRule);
        $data["earning"] = $storeEarning;
        $data["xp"] = $storexp;
        return $data;

    }

    /**
     * select the rule
     * @param $rules
     * @param $currentDate
     * @return array
     */
    private function getSelectedRule($rules, $currentDate, $timezone)
    {
        $selectedRule = null;
        foreach ($rules as $rule) {
            if ($rule->start_date == null && $rule->is_default == 1) $selectedRule = $rule;
            else {
                $startDate = Carbon::createFromFormat("Y-m-d H:i:s", $rule->start_date, $timezone);
                $endDate = Carbon::createFromFormat("Y-m-d H:i:s", $rule->end_date, $timezone);
                if ($currentDate->gte($startDate) && $currentDate->lte($endDate)) {
                    $selectedRule = $rule;
                    break;
                }
            }
        }
        return $selectedRule;
    }

    private function calculateXpAndStore($assignedOrder, $rule)
    {
        $xp = 0;
        if ($assignedOrder->distance <= $rule->min_distance) {
            $xp = $rule->min_xp;
        } else {
            $xp = $rule->min_xp + ($assignedOrder->distance - $rule->min_distance) * $rule->xp_per_km;
        }
        try {
            $this->driverXp->createDriverXp([
                "user_id" => $assignedOrder->user_id,
                "order_id" => $assignedOrder->order_id,
                "experience_point" => $xp,
                "type" => "normal",
                "country_id" => $assignedOrder->country_id
            ]);
            $this->log->storeLogInfo(['success creating xp',[
                'id' => $assignedOrder['id']
            ]]);
            return true;
        } catch (\Exception $ex) {

            $this->log->storeLogError(['error creating xp',[
                'message' => $ex->getMessage(),
                'id' => $assignedOrder['id']
            ]]);
            return $ex->getMessage();
        }
    }

    private function calculateEarningAndStore($assignedOrder, $rule, $order,$isAdmin=false)
    {
        $earning = 0;
        if(isset($order["user_type"]) && ($order["user_type"] == "restaurant-customer") &&  (0 == $order['is_normal'])){
            $earning = $order["delivery_fee"];
            goto skipCalculationDeliveryFeeBasedOnDistanceForRestaurantAdmin;
        }
        if ($assignedOrder->distance <= $rule->min_distance) {
            $earning = $rule->min_earning;
        } else {
            $earning = $rule->min_earning + ($assignedOrder->distance - $rule->min_distance) * $rule->earning_per_km;
        }
        /**
         * If the delivery fee of order is less than the earning calculated here .. the earning is equals to driver earning
         */
        if ($earning > $order['delivery_fee']) {
            $earning = $order['delivery_fee'];
        }
        skipCalculationDeliveryFeeBasedOnDistanceForRestaurantAdmin:
        try {
            $this->driverEarning->createDriverEarning([
                "user_id" => $assignedOrder->user_id,
                "order_id" => $assignedOrder->order_id,
                "amount" => $earning,
                "type" => "normal",
                'date_for' => Carbon::now('utc'),
                "country_id" => $assignedOrder->country_id
            ]);

            $this->log->storeLogInfo(['success creating earning',[
                'id' => $assignedOrder['id']
            ]]);
            /*
             * checks if the payment method is cash on delivery, if it is cash on delivery then proceeds for creating driver collection
             */
            if($order['payment_method'] == "cash-on-delivery"){
                /**
                 * checks if the driver is assigned with any of the team
                 */
                $getDriverTeam =  $this->driverTeam->getTeamIdBasedOnForDriver($assignedOrder->country_id, $assignedOrder->user_id);
                //
                if(isset($order["user_type"]) &&  ($order["user_type"] == "restaurant-customer")  && (0 == $order['is_normal'])){
                    $amount = $order['amount'] - $order['delivery_fee'];
                }
                else{
                    $amount = $order['amount'] ;
                }
                if($getDriverTeam){
                    $driverCollectionData =[
                        "order_id" => $assignedOrder->order_id ,
                        "driver_id" => $assignedOrder->user_id ,
                        "team_id" => $getDriverTeam->id ,
//                       "admin_id" => ,
//                       "leader_received_at" ,
//                       "admin_received_at" ,
                        "amount" => $amount,
                        "country_id" => $order->country_id ,
                        "city_id" => $order->city_id
                    ];
                    $this->driverCollection->createCollection($driverCollectionData);

                }
            }
            return true;
        } catch (\Exception $ex) {
            $this->log->storeLogError(['error creating earning',[
                'message' => $ex->getMessage(),
                'id' => $assignedOrder['id']
            ]]);
            return $ex->getMessage();
        }
    }

    /**
     * get total completed order count
     * @param $userId
     * @return int
     */
    private function getCompletedOrder($userId, $countryId)
    {
        $completedOrder = $this->driverOrder->getCompletedOrder($userId, $countryId);
        return ["count" => $completedOrder];

    }

    private function getTotalXp($user_id, $countryId)
    {
        $normalXp = $this->driverXp->getAllDriverXpNormal($user_id, $countryId);
        $deductedXp = $this->driverXp->getAllDriverDeducted($user_id, $countryId);
        return $normalXp - $deductedXp;

    }

    private function getTotalEarning($user_id, $countryId)
    {
//        return $this->driverEarning->getCurrentEarning($user_id, $countryId);
        $normalEarning = $this->driverEarning->getAllDriverEarningNormal($user_id, $countryId);
        $deductedEarning = $this->driverEarning->getAllDriverEarningDeducted($user_id, $countryId);
        return $normalEarning - $deductedEarning;

    }

    private function getSevendaysRecord($userId, $countryId)
    {
        $now = Carbon::now()->endOfDay();
        $days[6] = $now->subDay(1)->toDateString();
        $days[5] = $now->subDay(1)->toDateString();
        $days[4] = $now->subDay(1)->toDateString();
        $days[3] = $now->subDay(1)->toDateString();
        $days[2] = $now->subDay(1)->toDateString();
        $days[1] = $now->subDay(1)->toDateString();
        $days[0] = $now->subDay(1)->toDateString();
        $days = array_reverse($days);
        $datas = [];
        $today = 0;
//        return $days;
        $weklyRecord = $this->driverOrder->getLastSevenDaysRecordsIncludingCurrentDate($userId,$countryId);

        foreach ($days as $day) {
            foreach ($weklyRecord as  $record){
                $getDay = Carbon::parse($day);
                $getDayInFormat = $getDay->format('Y-m-d');
                if($getDayInFormat != $record->dt) continue;
                if($now->format('Y-m-d') == $record->dt){
                    $today = (int)$record->order_count ; continue;
                }
                $weekDay = $getDay->format("l");
                $datas[] = ["$weekDay" => (int) $record->order_count];
            }

        }
        $datas['today'] = $today;
        return $datas;
        return array_reverse($datas);
    }


    private function getPaymentMethodTranslation($paymentMethod, $lang)
    {
        /**
         * gets payment status from payment status table based on slug payment_status from payment table
         */
        $deliveryStatus = $this->paymentMethod->getSpecificPaymentMethodBySlug($paymentMethod);
        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (!$deliveryStatus) return false;
        $deliveryLanguage = $deliveryStatus->paymentMethodTranslation()->where("lang", $lang)->first();
        /**
         *
         */
        if (!$deliveryLanguage) {
            $deliveryLanguage = $deliveryStatus->paymentMethodTranslation()->where("lang", "en")->first();
            if (!$deliveryLanguage) return false;
        }
        return $deliveryLanguage["name"];
    }

    private function getDeliveryStatusTransaction($deliveryStatus, $lang)
    {

        /**
         * gets payment status from payment status table based on slug payment_status from payment table
         */
        $deliveryStatus = $this->deliveryStatus->getDeliveryStatusBasedOnSlug($deliveryStatus);
        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (!$deliveryStatus) return false;
        $deliveryLanguage = $deliveryStatus->deliveryStatusTranslations()->where("lang_code", $lang)->first();
        /**
         *
         */
        if (!$deliveryLanguage) {
            $deliveryLanguage = $deliveryStatus->deliveryStatusTranslations()->where("lang_code", "en")->first();
            if (!$deliveryLanguage) return false;
        }
        return $deliveryLanguage["name"];

    }

    private function getDeliveryStatusTransactionModified($deliveryStatus, $allStatus, $lang)
    {
        /**
         * gets payment status based on slug payment_status from payment table
         */
        $deliveryStatus = collect($allStatus)->where("slug", $deliveryStatus)->first();
        /*
         * if payment_status is not present in payment_status table slug then the value stored in payment.payment_status column in displayed
         */
        if (!$deliveryStatus) return false;
        $deliveryStatus = $deliveryStatus->toArray();
        $language = collect($deliveryStatus["delivery_status_translations"]);
        $deliveryLanguage = $language->where("lang_code", $lang)->first();
        /**
         *
         */
        if (!$deliveryLanguage) {
            $deliveryLanguage = $language->where("lang_code", "en")->first();
            if (!$deliveryLanguage) return false;
        }
        return $deliveryLanguage["name"];

    }

    /**
     * process the earning history and save to csv file
     * @param $getAllEarningsHistory
     * @param $commission_order_rate
     * @param $commission_vat_rate
     * @param $is_captain
     * @param $captain_salary
     * @param $fileName
     * @param $currency
     */
    private function processHistortToCsv($getAllEarningsHistory,$commission_order_rate,$commission_vat_rate,$is_captain,$captain_salary,$fileName,$currency){
        $total_amount = 0;
        $total_commission_amount = 0;
        $total_vat_amount = 0;
        $total_grand_total = 0;

        foreach ($getAllEarningsHistory as $history){
            $commission = 0;
            $grand_total = 0;
            $commission_vat = 0;
            /**
             * deduction means money is paid to hence substracted
             */
            if($history['type'] == 'deduction'){
                /**
                 * for deduction case there wont be any commission amount and commission vat amount since its already being paid
                 */
                $history->commission_amount = 0;
                $history->commission_tax_amount = 0;
                /**
                 * as it is deduction negate the amount
                 */
                $grand_total = $history->grand_total = $history->amount = (- $history->amount);
                $total_grand_total += $grand_total;
                $total_amount += $grand_total;

            }

            else{
                /**
                 * commission is amount * order rate(the commission rate for driver) * 0.01
                 */
                $commission = $history->commission_amount = $history->amount * $commission_order_rate * 0.01;
                /**
                 * commission vat  is $commission * order commission_amount(the commission rate for driver) * 0.01
                 */
                $commission_vat = $history->commission_tax_amount = $history->commission_amount * $commission_vat_rate * 0.01;
                /**
                 * grand total is the amount obtained by deducting commission and commission vat amount
                 */
                $grand_total = $history->grand_total = $history->amount -  $commission - $commission_vat;
                $total_amount += $history->amount;
                $total_vat_amount += $commission_vat;
                $total_commission_amount += $commission;
                $total_grand_total += $grand_total;
                $history->commission_amount = round($commission,2);
                $history->commission_tax_amount = round($commission_vat,2);
                $history->grand_total = round($grand_total,2);
            }
        }
        /**
         * if the driver is captain then add the captain salary to the grand total
         */
        if($is_captain){
            $total_grand_total += $captain_salary;
        }
        $total =  [
            'total_amount' => round($total_amount,2),
            'total_commission_amount' => round($total_commission_amount,2),
            'total_vat_amount' => round($total_vat_amount,2),
            'total_grand_total' => round($total_grand_total,2),
            'currency' => $currency,
            'captain_salary' => $captain_salary,
            'is_captain' => $is_captain
        ];
        return $this->excel->createDriverInvoiceReport($getAllEarningsHistory,$total,$fileName);

    }

}
