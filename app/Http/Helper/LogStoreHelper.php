<?php

/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 4/25/2017
 * Time: 3:05 PM
 */
use Illuminate\Support\Facades\Log;
use Monolog\Logger;
use CodeInternetApplications\MonologStackdriver\StackdriverHandler;
/**
 * Class LogStoreHelper
 */
class LogStoreHelper
{
    /**
     * LogStoreHelper constructor.
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->user_id = RoleChecker::getUser();
    }

    /**
     * @param $data
     */
    public function storeLogInfo($data){
        if(ENV("LOG_STATUS") =="enable"){

            $data[1]['user_id']=$this->user_id;
            $this->googleStackDriver($data,'info');
            Log::info($data[0], $data[1]);
        }

    }

    /**
     * @param $data
     */
    public function storeLogError($data){
        if(ENV("LOG_STATUS")=="enable"){

            $data[1]['user_id']=$this->user_id;
            $this->googleStackDriver($data,'error');
            Log::error($data[0], $data[1]);
        }

    }

    private function googleStackDriver($data ,$type){
        return true;
        $projectId = 'lugmety-staging';

// See Google\Cloud\Logging\LoggingClient::__construct
        $loggingClientOptions = [
            'keyFilePath' => storage_path().'/stackdriver.json'
        ];

// init handler
        $stackdriverHandler = new StackdriverHandler(
            $projectId,
            $loggingClientOptions
        );

// init logger with StackdriverHandler
        $logger = new Logger('order service', [$stackdriverHandler]);

// basic info log with contextual data
        if($type == 'info'){
            $logger->info($data[0],$data);
        }
        else{
            $logger->error($data[0],$data);
        }

        return true;
    }

}