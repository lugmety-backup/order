<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 3/14/2018
 * Time: 12:49 PM
 */

namespace App\Http\Helper;


class OrderFilterHelper
{
    public function checkForOrderPermission($order,$requestOrder){


        if($order["delivery_type"] == "home-delivery"){
            if((in_array($requestOrder["order_status"],["pending","rejected","accepted"]) && ($order["order_status"] == "pending")) || (($requestOrder["order_status"] == $order["order_status"] )) ){
                return true;
            }
            else{
                return false;
            }
        }


        else{

            if((in_array($requestOrder["order_status"],["pending","rejected","accepted"]) && ($order["order_status"] == "pending")) || (($requestOrder["order_status"] == $order["order_status"] ) ) ){

                if(($order["payment_status"] == $requestOrder["payment_status"]) || ($order["payment_status"] == "pending" && in_array($requestOrder["payment_status"],["pending","completed"]))){
                    return true;
                }
                else{
                    return false;
                }

            }
            else{
                return false;
            }
        }




    }

}