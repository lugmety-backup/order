<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 6/14/17
 * Time: 4:14 PM
 */

namespace App\Http\Facades;

use Illuminate\Support\Facades\Facade;

class FoodCheckerFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return "foodData";
    }
}