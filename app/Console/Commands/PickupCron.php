<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 4/3/2018
 * Time: 12:28 PM
 */

namespace App\Console\Commands;


use App\Models\Orders;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PickupCron extends Command
{
    protected  $order;
    public function __construct(Orders $order)
    {
        parent::__construct();
        $this->order = $order;
    }

    protected $name = 'order:ModifyPickup';
    public function handle()
    {
        DB::beginTransaction();
        try {
            set_time_limit(0);
            $getAllNonDeliveredPickupOrder = $this->order
                ->where([
                    ["order_status", "accepted"],
                    ["delivery_type", "pickup"],
                    ["payment_method", "cash-on-pickup"],
                    ["api_delivery_time", "!=", ""],
                    ["delivery_status","!=","delivered"],
                ])
                ->orWhere([
                    ["order_status", "accepted"],
                    ["delivery_type", "pickup"],
                    ["api_delivery_time", "!=", ""],
                    ["payment_method", "creditcard"],
                    ["payment_status", "completed"],
                    ["delivery_status","!=","delivered"],
                ])->orderBy("id","desc")->get();

            Log::info("Pickup cron",[
                "total_order" => count($getAllNonDeliveredPickupOrder)
            ]);

            foreach ($getAllNonDeliveredPickupOrder as $order) {
                if ($order->payment_method == "creditcard") {
                    $apiDeliveryTime = Carbon::parse($order->api_delivery_time, $order->time_zone_utc);
                    $currentTime = Carbon::now($order->time_zone_utc);
                    $timeDifference = $apiDeliveryTime->diffInMinutes($currentTime);
                    if ($timeDifference >= 10) {
                        $order->update([
                            "delivery_status" => "delivered"
                        ]);
                    }
                } else {
                    $apiDeliveryTime = Carbon::parse($order->api_delivery_time, $order->time_zone_utc);
                    $currentTime = Carbon::now($order->time_zone_utc);
                    $timeDifference = $apiDeliveryTime->diffInMinutes($currentTime);
                    if ($timeDifference >= 10) {
                        $order->update([
                            "delivery_status" => "delivered",
                            "payment_status" => "completed"
                        ]);
                    }
                }
            }


            DB::commit();
            dd("successfully completed pickup cron");
        }
        catch (\Exception $ex){
            Log::error("Pickup cron error",[
                "total_order" => count($getAllNonDeliveredPickupOrder),
                "message" => $ex->getMessage()
            ]);
        }

    }

}