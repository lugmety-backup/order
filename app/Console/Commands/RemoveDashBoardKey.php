<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 7/24/2018
 * Time: 12:01 PM
 */

namespace App\Console\Commands;


use Illuminate\Console\Command;

class RemoveDashBoardKey extends  Command
{
    protected $name = 'flush:dashboard-key';

    public function handle()
    {
        try {
            if(\Redis::exists('admin-dashboard-stats')){
                \Redis::del('admin-dashboard-stats');
            }
            dd('successfully deleted dashboard key ');
        }
        catch (\Exception $ex){
            Log::error("flush dashboard key  cron error",[
                "message" => $ex->getMessage()
            ]);
        }

    }
}