<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 2/25/2018
 * Time: 10:42 AM
 */

namespace App\Console\Commands;


use App\Events\SendInvoiceMessage;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Orders;
use Illuminate\Support\Facades\DB;
use App\Models\CronInvoice;
use Illuminate\Support\Facades\Log;
use App\Repo\CronInvoiceInterface;
class RestaurantInvoice extends Command
{
    protected  $cronInvoice;
    public function __construct(CronInvoiceInterface $cronInvoice)
    {
        parent::__construct();
        $this->cronInvoice = $cronInvoice;
    }

    protected $name = 'invoice:sendRestaurantDetail';
    public function handle(){
        $currentDate = Carbon::parse("2018-03-12");
//        $currentDate = Carbon::now("utc");
        $previousMonth = $currentDate->subMonth();
//        dd($previousMonth->format("Y-m"));
        $endOfTheMonth= $previousMonth->endOfMonth()->toDateTimeString();
        $startDate = $previousMonth->startOfMonth()->toDateTimeString();
        $getOrders = Orders::where("order_status", "accepted")->whereBetween("created_at", [
            $startDate,
            $endOfTheMonth
        ])->groupBy("country_id")->orderBy("country_id","asc")->get();
        $neworganizedOrders = [];
        foreach ($getOrders as $order){
            $neworganizedOrders[$order->country_id][] = $order->toArray();
        }
        foreach ($neworganizedOrders as $key => $neworganizedOrder){
            $restaurantId = [];
            $restaurantId["list"] =array_unique(array_values(collect($neworganizedOrder)->pluck("restaurant_id")->all()));
            $cronInvoice["restaurant_lists"] = serialize($restaurantId["list"]);
            $cronInvoice["last_date"] = $previousMonth->format("Y-m");
            $cronInvoice["country_id"] = $key;
            $storeOrUpdateInvoice = $this->cronInvoice->createInvoice($cronInvoice);
            $checkInvoiceToBeSendOrNot = $this->cronInvoice->checkMonthIsChanged($key,$cronInvoice["last_date"]);
            if(count($checkInvoiceToBeSendOrNot) == 1){
                $restaurantId["date"] = [
                    "start_date" => $startDate,
                    "end_date" => $endOfTheMonth
                ];
                event(new SendInvoiceMessage($restaurantId));
                $cronInvoice["is_send_invoice"] = 1;
                $this->cronInvoice->updateInvoiceInfo($checkInvoiceToBeSendOrNot,$cronInvoice,true);
            }

        }







//        $data = [
//          "service" => "order",
//          "message"  => "invoice",
//          "data" =>[
//              "restaurant_id" => $restaurantId
//          ]
//        ];




    }

}